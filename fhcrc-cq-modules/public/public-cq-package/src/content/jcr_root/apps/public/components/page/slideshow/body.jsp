<%@include file="/libs/foundation/global.jsp"%>
<%
	StringBuffer cls = new StringBuffer();
    for (String c: componentContext.getCssClassNames()) {
        cls.append(c).append(" ");
    }
%>


<body class="<%= cls %>">

	<cq:include script="header.jsp" />
	<cq:include script="titlebar.jsp" />
	<cq:include script="content.jsp" />
	<cq:include script="footer.jsp" />
	<cq:include script="pixels.jsp" />

</body>