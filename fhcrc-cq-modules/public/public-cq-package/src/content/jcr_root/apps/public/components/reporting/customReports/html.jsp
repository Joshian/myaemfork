<%@include file="/libs/foundation/global.jsp"%>

<head>
    <cq:includeClientLib css="reporting.components.customReports" />
    <script src="/libs/cq/ui/resources/cq-ui.js" type="text/javascript"></script>
</head>
<body>
<%
String reportType = slingRequest.getParameter("reportType");
if(reportType == null) {
%>
<h1>Custom Reports</h1>
<form action="<%= currentPage.getPath() %>.html" method="get">
<div id="formContainer">
<select name="reportType" id="reportType">
    <option value="">--Select a report Type--</option>
    <option value="recentlyCreated">Recently Created Pages</option>
    <option value="recentlyModified">Recently Modified Pages</option>
    <option value="staleContent">Aging Content</option>
    <option value="findComponents">Find Components</option>
    <option value="newUsers">New Users</option>
</select>
</div>
<input id="customReportSubmit" type="submit" value="Submit" disabled="disabled"></input>
</form>
<%
} else {
%>
<span class="backLink"><a href="<%= currentPage.getPath() %>.html">Reset and create new report</a></span>
<%
    if(reportType.equals("recentlyCreated")) {
%>
<h1>Recently Created Pages</h1>
<cq:include script="recentlyCreated.jsp" />
<%
    } else if(reportType.equals("staleContent")) {
%>
<h1>Aging Content</h1>
<cq:include script="staleContent.jsp" />
<%
    } else if(reportType.equals("findComponents")) {
%>
<h1>Find Components</h1>
<cq:include script="findComponents.jsp" />
<%
    } else if(reportType.equals("newUsers")) {
%>
<h1>New Users</h1>
<cq:include script="newUsers.jsp" />
<%
    } else if(reportType.equals("recentlyModified")) {
%>
<h1>Recently Modified Pages</h1>
<cq:include script="recentlyModified.jsp" />
<%
    }
}
%>
<cq:includeClientLib js="reporting.components.customReports" />
</body>