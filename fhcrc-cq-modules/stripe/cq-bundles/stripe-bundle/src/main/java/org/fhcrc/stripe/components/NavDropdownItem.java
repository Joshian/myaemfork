/**
 * NavDropdownItem class is in charge of handling NavDropdownItem properties and logic
 *  
 */
package org.fhcrc.stripe.components;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import com.day.cq.wcm.api.Page;
import javax.jcr.Node;

import java.util.Iterator;
import org.slf4j.*;

public class NavDropdownItem 
{

	private final Logger log = LoggerFactory.getLogger(NavDropdownItem.class);
	private final static String ITEM_LABEL = "itemlabel";
	private final static String ITEM_PATH = "itempath";
	private final static String SUB_ITEM_PATH = "subitempath";
	private final static String SUB_ITEM_LABEL= "subitemlabel";
	private final static String IS_EXTERNAL_LINK = "isExternalLink";
	private final static String WIDTH = "width";
	
	private final static String DEFAULT_ITEM_LABEL = "New Nav Item";
	private final static String DEFAULT_SUBITEM_LABEL = "New Nav Sub Item";


	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	private Page currentPage;
	private Resource resource;
	
	private String itemlabel;
	private String itempath;

	private String subitempath;
	private String subitemlabel;
	private boolean isExternalLink;
	private String width;

	/** Constructor of the class
	 * @param properties the page properties
	 * @param resourceResolver the resource resolver
	 */
	public NavDropdownItem() {
		
	}

	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
        this.itemlabel = properties.get(ITEM_LABEL,DEFAULT_ITEM_LABEL);
        this.itempath = properties.get(ITEM_PATH, "#");
        this.subitempath = properties.get(SUB_ITEM_PATH, "#");
        this.subitemlabel = properties.get(SUB_ITEM_LABEL, DEFAULT_SUBITEM_LABEL);
        this.isExternalLink = properties.get(IS_EXTERNAL_LINK, false);
        this.width = properties.get(WIDTH, "");
        
	}

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public String getItemlabel() {
		return itemlabel;
	}

	public void setItemlabel(String itemlabel) {
		this.itemlabel = itemlabel;
	}

	public Page getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public boolean isSelected(){
		boolean match = false;
		String subitempath = "";
		if (itempath != null && itempath.equals(currentPage.getPath())){
			match = true;
		}
		if (!match)
		{
			try
			{
				Resource subparsys = resourceResolver.getResource(resource.getPath()+"/navsubitems");
				if (subparsys != null){
					Iterator<Resource> subitems = resourceResolver.listChildren(subparsys);
					while (subitems.hasNext() && !match){
						Node item = subitems.next().adaptTo(Node.class);
						subitempath = (String)item.getProperty("subitempath").getString();
						if (subitempath != null && subitempath.equals(currentPage.getPath()))
						{
							match = true;
						}
					}
				}
			}
			catch(Exception exception)
			{
				log.error("Error finding selected tab : " + exception);
			}
		}
		return match;
	}

	public String getSubitempath() {
		return subitempath;
	}

	public void setSubitempath(String subitempath) {
		this.subitempath = subitempath;
	}

	public String getSubitemlabel() {
		return subitemlabel;
	}

	public void setSubitemlabel(String subitemlabel) {
		this.subitemlabel = subitemlabel;
	}
	
	public String getItempath() {
		return itempath;
	}

	public void setItempath(String itempath) {
		this.itempath = itempath;
	}
	
	public boolean isExternalLink() {
		return isExternalLink;
	}

	public void setExternalLink(boolean isExternalLink) {
		this.isExternalLink = isExternalLink;
	}
	
	/**
	 * @return the width
	 */
	public String getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(String width) {
		this.width = width;
	}
}