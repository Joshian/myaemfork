<%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default head script.

  Draws the HTML head with some default content:
  - includes the WCML init script
  - includes the head libs script
  - includes the favicons
  - sets the HTML title
  - sets some meta data

  ==============================================================================

--%><%@include file="/libs/foundation/global.jsp" %><%
%><%@ page import="com.day.cq.commons.Doctype,
                   org.apache.commons.lang.StringEscapeUtils,
                 com.day.cq.security.User,
                 com.day.cq.security.UserManager,
                 com.day.cq.security.UserManagerFactory,
                 org.apache.sling.jcr.api.SlingRepository,
                 com.day.cq.wcm.api.WCMMode,
                 com.day.cq.wcm.api.components.DropTarget,
                 com.day.cq.security.Group,
                 com.day.cq.i18n.I18n,
                 java.util.ResourceBundle,
                 org.apache.sling.settings.SlingSettingsService,
				 java.util.Set" %><%
    String xs = Doctype.isXHTML(request) ? "/" : "";
    String favIcon = currentDesign.getPath() + "/img/favicon.ico";
    if (resourceResolver.getResource(favIcon) == null) {
        favIcon = null;
    }
%><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="keywords" content="<%= StringEscapeUtils.escapeHtml(WCMUtils.getKeywords(currentPage, false)) %>" />
    <meta name="description" content="<%= StringEscapeUtils.escapeHtml(properties.get("jcr:description", "")) %>" />
    <meta name="robots" content="all" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="width" />     

    <link rel="image_src" href="http://www.womenscanceradvocacy.net<%=currentDesign.getPath()%>/img/facebook_share.png" />
    <meta property="og:image" content="http://www.womenscanceradvocacy.net<%=currentDesign.getPath()%>/img/facebook_share.png" />
    <meta property="og:title" content="<%= StringEscapeUtils.escapeHtml(currentPage.getTitle() == null ? currentPage.getName() : currentPage.getTitle()) %>" />
    <meta property="og:description" content="<%= StringEscapeUtils.escapeHtml(properties.get("jcr:description", "")) %>" /> 
    
    <%-- do not launch the sidekick when the author is in wecan-reviewer group --%>
    <%
       Session adminSession = null;
       try {
         // only want to do this when we are in author, not publish:
         Set<String> runModes =  sling.getService(SlingSettingsService.class).getRunModes();
		 log.debug("RUNMODES = " + runModes);
		 if (runModes.contains("author")){
            boolean isReviewer = false;
            User currentUser = resourceResolver.adaptTo(User.class); 
            SlingRepository repo = sling.getService(SlingRepository.class);
            adminSession = repo.loginAdministrative(null);
            UserManager userManager = sling.getService(UserManagerFactory.class).createUserManager(adminSession);
            Group group = null;
            if(userManager.hasAuthorizable("wecan-reviewer")){
              group = (Group) userManager.get("wecan-reviewer"); 
              isReviewer = group.isMember(currentUser); 
              log.info("WE-CAN reviewer group exists. Current user " + (isReviewer?"IS":"IS NOT") + " a member of " + group.getID());
            } else {
              log.info("WE-CAN reviewer group does NOT exist. Check to be sure this is correct.");
            }
            //-------------------------------------------------------
            // If user is NOT a member of the reviewer group,  
            // permit launch of the sidekick.  
            //-------------------------------------------------------
            if(!isReviewer){%>
                <cq:include script="/libs/wcm/core/components/init/init.jsp"/>
            <%}
         }
       } catch (Exception ex) {
           log.error("Exception", ex);
       } finally {
            if (adminSession != null) {
                adminSession.logout();
            }          
       }
    %>

    <% if (favIcon != null) { %>
    <link rel="SHORTCUT ICON" type="image/x-icon" href="<%= favIcon %>" />
    <link rel="ICON" type="image/x-icon" href="<%= favIcon %>" />
    <% } %>
    <title><%= currentPage.getTitle() == null ? currentPage.getName() : currentPage.getTitle() %></title>

    <cq:includeClientLib css="apps.wecan.main"/>
    
    <% if (currentPage.getDepth() == 3){ %>
    <cq:includeClientLib css="wecan.libs.royalslider" />
    <% } %> 
    
    <!--[if gte IE 9]>
      <style type="text/css">
        /* http://www.colorzilla.com/gradient-editor/ */
        .gradient {
           filter: none;
        }
      </style>
    <![endif]-->

    <!--[if lte IE 8]>
    <link rel="stylesheet" type="text/css" href="<%=currentDesign.getPath()%>/clientlibs/css/ie8.css" />
    <![endif]-->    
    
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="<%=currentDesign.getPath()%>/clientlibs/css/ie7.css" />
    <![endif]-->        
    
    <cq:includeClientLib js="apps.wecan.main"/>
    
    <!--[if lte IE 9]>
    <![endif]-->      

    
	<script type="text/javascript">
	<!--
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-528883-26']);
	  _gaq.push(['_trackPageview']);

	  if(location.hostname=='www.womenscanceradvocacy.net' || location.hostname=='womenscanceradvocacy.net'){	
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
	  } 
	//-->
 	</script> 
 	
 	<%-- using typekit to get custom webfonts. adding these lines here load synchronously, blocks page render until complete. --%>
    <%-- typekit interferes with author in firefox browsers, so disable while we figure this out. --%>
    <%-- String runModes = System.getProperty("sling.run.modes");
       if (!(runModes.toLowerCase().contains("author"))){ --%>
    <script type="text/javascript" src="//use.typekit.net/ibk3vwp.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <%-- } --%>

</head>
