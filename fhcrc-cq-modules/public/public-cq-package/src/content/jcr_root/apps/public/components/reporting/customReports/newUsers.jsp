<%@include file="/libs/foundation/global.jsp"%>
<%@page import="com.day.text.Text, 
                 java.util.*, 
                 org.apache.commons.lang.StringEscapeUtils, 
                 com.day.cq.search.QueryBuilder, 
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.result.Hit,
                 java.text.SimpleDateFormat,
                 org.apache.jackrabbit.api.security.user.*" %>
<%
Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);
HashMap<String, String> map = new HashMap<String, String>();
String timeInt = slingRequest.getParameter("timeInt");
String timeIncrement = slingRequest.getParameter("timeIncrement");

map.put("type","rep:User");
map.put("path","/home/users");
map.put("relativedaterange.property","jcr:created");
if(timeInt != null && timeIncrement != null && !timeInt.trim().equals("")) {
    map.put("relativedaterange.lowerBound","-" + timeInt + timeIncrement);
} else {
	map.put("relativedaterange.lowerBound","-1w");
}
map.put("orderby","@jcr:created");
map.put("orderby.sort","desc");
map.put("p.limit","1000");

List<Hit> hitList = builder.createQuery(PredicateGroup.create(map), session).getResult().getHits();
ArrayList<Node> NodeList = new ArrayList<Node>();

if(hitList.size() == 0){
%>
    <cq:include script="empty.jsp"/>
<%
} else {
	for(int i = 0; i < hitList.size(); i++){
	      Node testNode = hitList.get(i).getResource().adaptTo(Node.class);
	      NodeList.add(testNode);
	}
	Iterator<Node> NodeIterator = NodeList.iterator();
    if(!NodeIterator.hasNext()){
%>  
    	    <cq:include script="empty.jsp"/>
<%
    } else {
%>
<!-- Table of results -->
<table id="reportResults" class="sortable">
    <tr>
        <th>User name</th>
        <th>Created on</th>
    </tr>


<%
    	while(NodeIterator.hasNext()) {
    		Node nextNode = NodeIterator.next();
    		SimpleDateFormat f = new SimpleDateFormat("MMM dd, k:mm");
    		String creationDate = "Unknown", name = "Unknown", memberOf = "Unknown";
    		
    		if(nextNode.hasProperty("profile/givenName")) {
    			name = nextNode.getProperty("profile/givenName").getString();
    		} else if(nextNode.hasProperty("rep:principalName")) {
    			name = nextNode.getProperty("rep:principalName").getString();
    		}
    		
    		if(nextNode.hasProperty("jcr:created")) {
    			creationDate = f.format(nextNode.getProperty("jcr:created").getDate().getTime());
    		}
%>
    <tr>
        <td><%= name %></td>
        <td><%= creationDate %></td>
    </tr>
<%
    	}
    }
}
%>
</table>
<script type="text/javascript">
var numResults = document.getElementById('reportResults').rows.length;
numResults -= 1;
var theTable = document.getElementById("reportResults");
var returnedResults = document.createElement("span");
returnedResults.setAttribute("id","numResults");
returnedResults.appendChild(document.createTextNode("Returned " + numResults + " results"));
theTable.parentNode.insertBefore(returnedResults,theTable);
</script>