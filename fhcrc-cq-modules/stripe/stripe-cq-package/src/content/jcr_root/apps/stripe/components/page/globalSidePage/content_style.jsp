<%@include file="/apps/stripe/global.jsp" %>
<%@page import="com.day.cq.wcm.api.Page,
                org.fhcrc.stripe.templates.general.Colors" %>
<%
     String logoTopColor = Colors.getColorProperty(currentPage,"logo_top");
     String logoBottomColor = Colors.getColorProperty(currentPage,"logo_bottom");
     
     String labTopColor = Colors.getColorProperty(currentPage,"lab_top");
     String labBottomColor = Colors.getColorProperty(currentPage,"lab_bottom");
     String labtextColor = Colors.getColorProperty(currentPage,"lab_text_color");
     
     String hyperlinkActiveColor = Colors.getColorProperty(currentPage,"hyperlink_active_color"); 
     String hyperlinkColor = Colors.getColorProperty(currentPage,"hyperlink_color");
     
     String navActiveTextColor = Colors.getColorProperty(currentPage,"sidenav_active_text"); 
     String navBackgroundColor = Colors.getColorProperty(currentPage,"sidenav_background");
     String navSelectedSubMenuColor = Colors.getColorProperty(currentPage,"sidenav_selected_sub_menu");
     String navSelectedMenuColor = Colors.getColorProperty(currentPage,"sidenav_selected_menu");
     String navTextColor = Colors.getColorProperty(currentPage,"sidenav_text");
     String carouselCurrentIconColor = Colors.getColorProperty(currentPage,"carousel_current_icon");
     String carouselDescriptionBoxColor = Colors.getColorProperty(currentPage,"carousel_description");
     
     
%>
<style type="text/css">


/*----------general start ------------*/
a{
    color: #<%=hyperlinkColor%>;
}

a:hover{
    color: #<%=hyperlinkActiveColor%>;
}
/*----------general end ------------*/

/*---------- lab colorbar start------------ */
.navbar-inverse .lab-title{
    height: 50px;
    color: #<%=labtextColor%>;
    background-color: #<%=labBottomColor%>;
    background-image: -moz-linear-gradient(top, #<%=labTopColor%>, #<%=labBottomColor%>);
    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#<%=labTopColor%>), to(#<%=labBottomColor%>));
    background-image: -webkit-linear-gradient(top, #<%=labTopColor%>, #<%=labBottomColor%>);
    background-image: -o-linear-gradient(top, #<%=labTopColor%>, #<%=labBottomColor%>);
    background-image: linear-gradient(to bottom, #<%=labTopColor%>, #<%=labBottomColor%>);
    filter: progid:dximagetransform.microsoft.gradient(startColorstr='#ff<%=labTopColor%>', endColorstr='#ff<%=labBottomColor%>', GradientType=0);
}

.labtitle h1 {
    color: #<%=labtextColor%>;
}
/*---------- lab colorbar end------------ */

/*---------- logobar start ----------*/
.logobar{
  height: 65px;
  border-width: 0 0 1px;
  background-image: none;
  background-color: #<%=logoBottomColor%>;
}

.navbar-inverse .logobar{
  background-color: #<%=logoBottomColor%>;
  background-image: -moz-linear-gradient(top, #<%=logoTopColor%>, #<%=logoBottomColor%>);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#<%=logoTopColor%>), to(#<%=logoBottomColor%>));
  background-image: -webkit-linear-gradient(top, #<%=logoTopColor%>, #<%=logoBottomColor%>);
  background-image: -o-linear-gradient(top, #<%=logoTopColor%>, #<%=logoBottomColor%>);
  background-image: linear-gradient(to bottom, #<%=logoTopColor%>, #<%=logoBottomColor%>);
  background-repeat: repeat-x;
  border-color: #252525;
  filter: progid:dximagetransform.microsoft.gradient(startColorstr='#ff<%=logoTopColor%>', endColorstr='#ff<%=logoBottomColor%>', GradientType=0);
}
/*---------- logobar end ----------*/

/*-------- container ----------------*/
.container-aside {
    background-color: #<%=navBackgroundColor%>;
}

/* -------------- Navigation Start --------------- */

.dropdown-toggle p{
    color: #EEEEEE; 
}

.dropdown-menu {
  background-color: #<%=navBackgroundColor%>;
}

.dropdown-menu a {
    color: #<%=navActiveTextColor%>; 
}

.dropdown-menu li  a:hover,
.dropdown-menu li  a:focus,
.dropdown-submenu:hover  a {
  color: #<%=navActiveTextColor%>;  
  background-color: #<%=navSelectedMenuColor%>;
  background-image: -moz-linear-gradient(top, #<%=navSelectedSubMenuColor%>, #<%=navSelectedSubMenuColor%>);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#<%=navSelectedSubMenuColor%>), to(#<%=navSelectedSubMenuColor%>));
  background-image: -webkit-linear-gradient(top, #<%=navSelectedSubMenuColor%>, #<%=navSelectedSubMenuColor%>);
  background-image: -o-linear-gradient(top, #<%=navSelectedSubMenuColor%>, #<%=navSelectedSubMenuColor%>);
  background-image: linear-gradient(to bottom, #<%=navSelectedSubMenuColor%>, #<%=navSelectedSubMenuColor%>);
  filter: progid:dximagetransform.microsoft.gradient(startColorstr='#ff<%=navSelectedSubMenuColor%>', endColorstr='#ff<%=navSelectedSubMenuColor%>', GradientType=0);
}

.nav-tabs.nav-stacked li > a {
    color: #<%=navTextColor%>; 
}

.nav li.dropdown.open  .dropdown-toggle,
.nav li.dropdown.active  .dropdown-toggle,
.nav li.dropdown.open.active  .dropdown-toggle {
  color: #<%=navActiveTextColor%>; 
  background-color: #<%=navSelectedMenuColor%>;
}

.nav li.dropdown  .dropdown-toggle:hover{
  color: #<%=navActiveTextColor%>; 
  background-color: #<%=navSelectedMenuColor%>;
}

.nav  li  a {
    color: #<%=navActiveTextColor%>; 
}

.nav  li  a:hover {
    color: #<%=navActiveTextColor%>;  
    background-color: #<%=navSelectedMenuColor%>;
}
.nav  li  .dropdown-menu:after {
  border-top: 6px solid #<%=navBackgroundColor%>;
}

.nav li.dropdown.selected{
  color: #<%=navActiveTextColor%>; 
  background-color: #<%=navSelectedMenuColor%>;
}

/* -------------- Navigation End --------------- */

/** ---- carousel start -----*/ 

.carousel-icons.current{
    background-color: #<%=carouselCurrentIconColor%>;
}
.bottom_description{  
    background-color: #<%=carouselDescriptionBoxColor%>; 
}
.bottom_description p{
    color: #<%=navTextColor%>;
}

/** ---- carousel end -----*/

/*-------- page ----------*/

.page_description{  
    background-color: #<%=carouselDescriptionBoxColor%>; 
    color: #<%=navTextColor%>;
}

/*----------general ------------*/

.btn-side:hover {
    color: #<%=navTextColor%> !important;
}

.btn-login:hover{
    color: #<%=navTextColor%> !important;
}
</style>
<script type='text/javascript'>
jQuery(document).ready(function(){
     jQuery('th').css('background-color', '#<%=labBottomColor%>');
     jQuery('th').css('color', '#<%=labtextColor%>');
     jQuery('th').find('p').css('color', '#<%=labtextColor%>');
  });   
</script>