<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text, 
                 java.util.*,
                 com.day.cq.search.QueryBuilder, 
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.result.Hit,
                 com.day.cq.tagging.Tag,
                 com.day.cq.tagging.TagManager,
                 java.text.SimpleDateFormat,
                 java.text.DateFormat,
                 com.day.cq.wcm.api.WCMMode,
                 org.fhcrc.publicsite.constants.Constants,
                 org.fhcrc.tools.FHUtilityFunctions,
                 com.day.cq.wcm.api.components.IncludeOptions,
                 com.day.cq.wcm.foundation.Paragraph,
                 com.day.cq.wcm.foundation.ParagraphSystem,
                 com.day.cq.i18n.I18n,
                 org.fhcrc.tools.FieldUtility,
                 org.apache.commons.lang.StringEscapeUtils,
                 com.day.cq.wcm.foundation.Image,
                 java.net.URLEncoder,
                 org.apache.commons.lang.StringUtils" %><%


    // This routine was created as an offshoot of Ian's newsFinder component.
    // I've edited to be used as a "News roll". Characteristics of a news roll:
    //
    // News sources: you can choose among Center News, News Releases and/or Hutch Magazine
    // Filtering: able to include/exclude by tag
    // "Pin" articles to the top of the newsroll
    // Set the paging you want
    // Limit to certain date range
    // Format the display to your liking (show/hide date, description, source, etc.)
    //


    // -------------------------------------
    // Defaults and properties
    // -------------------------------------

long startTime = System.nanoTime();
long endTime, duration;

Date defaultDate = new Date();

Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);
TagManager tagManager = resourceResolver.adaptTo(TagManager.class);

String [] selectors = slingRequest.getRequestPathInfo().getSelectors();

boolean omitSource = properties.get("omitSource",Boolean.FALSE);
boolean omitDate = properties.get("omitDate",Boolean.FALSE);
boolean showDescription = properties.get("showDescription",Boolean.FALSE);
Integer maxnum = properties.get("maxnum",3);
String newsrollTitle = properties.get("defaultTitle","Recent Stories");

/* searchTerm is the name of the variable that should be passed to the component via the URL */
String queryString = slingRequest.getParameter("searchTerm");


/* limit to pages that have certain tags on them */
String[] tags = properties.get("searchTags", new String[0]);

/* exclude stories that have certain tags on them */
String[] excludeTagStrings = properties.get("excludeTags", new String[0]);
Tag[] excludeTags = new Tag[excludeTagStrings.length];
if(excludeTags.length > 0) {
    for(int i=0; i<excludeTags.length; i++){
        excludeTags[i] = tagManager.resolve(excludeTagStrings[i]); // note excludeTags CAN BE NULL if the tag is missing
        if(excludeTags[i]==null) log.warn("Excluding by a tag that is missing on this instance: " + excludeTagStrings[i]);
    }
}

/* set the display mode. the default is to show latest stories by reverse chron. if you're not
   in this mode, then you might be displaying by tag or by date. */
Boolean default_mode = true; // we'll assume it's a default view (latest news) unless they've requested by tag or by date.
Boolean date_mode = false; // becomes true when the "date" selector is being used

// these are articles they want to "pin" to the top of the default newsroll:
String[] mustDisplay = properties.get("include",String[].class);


// USER-REQUESTED, LIST BY TAG 
// the user wants to display only articles that have a certain tag on them 
/* if the user is sending valid tag(s) as selectors, then increase the tagset by those tags */
String [] newtags; 
//out.println(selectors[1].replace("`","/"));
if(selectors.length>=2 && selectors[0].equals("tag") && tagManager.resolve(selectors[1].replace("`","/"))!=null ){

  newtags = increaseArray( tags, 1 );
  newtags[ newtags.length-1 ] = selectors[1].replace("`","/"); // because %2F (/) does not pass correctly, I use %60 (`) in the encoding
  newsrollTitle = "Stories tagged '"+ tagManager.resolve(selectors[1].replace("`","/")).getTitle() +"'";
  default_mode = false;
} else {
  newtags = tags;
    //out.println("No tags");
}


// USER-REQUESTED PAGING
// if the user is asking for skip starting, their request is in the LAST selector to the page. That integer will
// be the Nth page to display. 0=first page (same as no selector); 1=second page; etc.
// this is only in play for components where a max is set by the user (maxnum)
   int pageNum = 0;
   if(selectors.length>0 && maxnum>0){        
        try {
            pageNum = Integer.parseInt( selectors[ selectors.length-1 ] ); // page numbering is always the last selector
        } catch (Exception e) {
            pageNum = 0;
        }  // out.print(pageNum);
    }



/* Begin building the predicateGroup for the search */ 
HashMap<String, String> map = new HashMap<String, String>();



// ----------------
// SEARCH OVER PATH
// ----------------

Boolean paths_filter = false; // flag to tell us whether we need to post-filter the found set by path. post-filtering is needed when they choose none OR more than 1 news source.
List<String> paths = new ArrayList<String>(); // determine over which path(s) to perform the search
if(properties.get("centernews","false").equals("true"))    {paths.add( Constants.PATH_TO_CENTERNEWS_FOLDER     ); }
if(properties.get("pressreleases","false").equals("true")) {paths.add( Constants.PATH_TO_NEWSRELEASES_FOLDER   ); }
if(properties.get("hutchMagazine","false").equals("true")) {paths.add( Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER ); }

/* if no path was selected, that's the same as all of them being selected from my perspective, and we'll need to do post-query filtering over all three: */
if( paths.isEmpty() ){
	paths.add( Constants.PATH_TO_CENTERNEWS_FOLDER   );
    paths.add( Constants.PATH_TO_NEWSRELEASES_FOLDER );
    paths.add( Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER        ); 
}

/* if there's exactly one path selected, we are golden: just query over that path, no post-query filtering is needed... */
if( paths.size()==1 ){
	map.put("1_group.1_path", paths.get(0));
    paths_filter = false;
} else { /* ...otherwise do a search over the news folder and post-filter the results */
    map.put("1_group.1_path", Constants.PATH_TO_NEWS_FOLDER);
    paths_filter = true;
}

/* construct the path filtering regex, for use later */
String paths_filter_regex = "(" + StringUtils.join( paths, "|") + ").*"; //out.println(paths_filter_regex);


// -----------------
// (2) SEARCH BY TAG
// -----------------

/* If we have any search tags, add them to the search IN THEIR OWN GROUP, 2_group */
if(newtags.length > 0){
    if("true".equals(properties.get("tagsAnd","false"))){
        map.put("2_group.p.and","true");
    } else {
        map.put("2_group.p.or","true");
    }
    for(int i=1; i<=newtags.length; i++) {
        map.put("2_group."+Integer.toString(i)+"_tagid",newtags[i-1]);
        map.put("2_group."+Integer.toString(i)+"_tagid.property","jcr:content/cq:tags");
    }
}


// ---------------------
// (3) LIMIT BY TEMPLATE
// ---------------------

/* We only care about pages that are News Page templates */
map.put("type","cq:Page");
map.put("property","jcr:content/cq:template");
map.put("property.value","/apps/public/templates/news");



// -------------------------------------------
// (4) ADD A LIMIT TO THE QUERY FOR EFFICIENCY
// -------------------------------------------

// Add a limit to the number of articles in the found set. Calculated:
//
//   1. maxnum provided by the user in this component (0=all)
//   2. the page of the request if not the first page. e.g., if it's page 3, no need to retrieve all 100 pages.
//   3. buffer to handle the exclusion of certain articles AFTER the query has run (based on tags)
//
// If there is no "maxnum" then you don't want to put a limit on the number of query results (because the user wants them all)
// If you are doing post-query path filtering, then you also don't want a limit at this point

if(maxnum>0 && paths_filter==false){

   int limit = maxnum;
   limit += pageNum * maxnum;
   limit += 1; // because I always want 1 more than the requested found set, so the newsroll knows there's at least one more article and will show 'load more'

   // buffer the limit by the number of articles excluded by tags (if any)
   if(excludeTags.length>0){
         for(Tag t : excludeTags){
             if( !(t==null) ){
             	limit += ( t.getCount()>0 ? t.getCount(): 0 ); // will overestimate by the # pages outside the news branch, but that's OK for this purpose.
             }
         }    
   }

   map.put("p.limit",Integer.toString( limit )); log.debug("Limit = " + limit); //out.println("limit = "+limit);
}

else {

    map.put("p.limit","100000"); // it seems the default limit is 10, so let's set this really high to avoid that.

}

// -------------------------
// OPTIMIZE QUERYBUILDER
// -------------------------

map.put("p.guessTotal","true"); // Added to fix Oak 1.0.18 problem


// --------------------------
// (5) LIMIT TO CERTAIN DATES
// --------------------------

/* Handle any date filters on the newsroll itself. Mar/2012 */
Date startdate = properties.get("startdate",Date.class);
Date enddate = properties.get("enddate",Date.class);

// check if the user is sending in a valid date selector, and if so, change startdate to that selector
// you can send in date in one of two ways:
//		news.date.YYYY-MM-DD.html
//      news.date.YYYY-MM.html
// there are slight differences in what you get for each of these ways.
if(selectors.length>=2 && selectors[0].equals("date") ){
  if(selectors[1].matches("(1|2)\\d\\d\\d-(0|1)\\d-(0|1|2|3)\\d")){
    String archive_date = selectors[1]; //"2012-02-03"
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdf2= new SimpleDateFormat("MMMM d', 'yyyy");
	Date d = sdf.parse( archive_date );
    if(d!=null){
      if(startdate==null || (d.getTime() >= startdate.getTime())  ) { 
          startdate = d;
      }
      date_mode = true;
      default_mode = false;
      newsrollTitle = "Archive starting "+ StringEscapeUtils.escapeHtml( sdf2.format(startdate)) +"";        
    }

  } else if(selectors[1].matches("(1|2)\\d\\d\\d-(0|1)\\d")){
    String archive_date = selectors[1]; //"2012-02"
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
	SimpleDateFormat sdf2= new SimpleDateFormat("MMMM', 'yyyy");
	Date d = sdf.parse( archive_date );
    if(d!=null){
      if(startdate==null || (d.getTime() >= startdate.getTime())  ) { 
          startdate = d;
      }
      date_mode = true;
      default_mode = false;
      newsrollTitle = "Archive for "+ StringEscapeUtils.escapeHtml( sdf2.format(startdate)) +"";        

    }
  } else {
      // the user sent a date selector that doesn't make sense.
      log.warn("Date selector sent in the request is unparsable (could be hacking): " + selectors[1]);
      //response.sendError(404);
      //return;
  }
}

if(startdate!=null){
    DateFormat f1= new SimpleDateFormat("yyyy-MM-dd'T00:00:00.000-06:00'"); // assets will have daylight savings time
    map.put("4_group.1_daterange.lowerBound",f1.format(startdate)); //f1.format(startdate)//Integer.toString(startdate.getYear())+'-'+Integer.toString(startdate.getMonth())+'-'+Integer.toString(startdate.getDay())
    map.put("4_group.1_daterange.lowerOperation",">=");
    map.put("4_group.1_daterange.property","@jcr:content/pubDate/date");
    log.debug("start date = " + f1.format(startdate));
}
if(enddate!=null){
    DateFormat f2= new SimpleDateFormat("yyyy-MM-dd'T23:59:59.999-09:00'"); //   built in...so cover the worst case scenario.
    map.put("4_group.2_daterange.upperBound",f2.format(enddate)); //f1.format(enddate)//Integer.toString(startdate.getYear())+'-'+Integer.toString(startdate.getMonth())+'-'+Integer.toString(startdate.getDay())
    map.put("4_group.2_daterange.upperOperation","<=");
    map.put("4_group.2_daterange.property","@jcr:content/pubDate/date");
    log.debug("stop date = " + f2.format(enddate));
}
//map.put("4_group.p.or","false");
// http://dev.day.com/discussion-groups/content/lists/cq-google/2010-11/2010-11-30__day_communique____operator_in_daterange_query_sharat.html/2?q=Querybuilder

// if there is NO selector, then the request is for the "default" newsroll shown on the News landing page. to speed returns
//   add a relative date on this query. you just have to cover the timerange for the most recent handful of articles
//   that appear in the first page of the newsroll, because once they Load More Stories they are using selectors and this
//   accelerator is no longer in play. the relative value you use here can be -1M, -15d or something like that.
if(selectors.length==0){
    map.put("4_group.1_relativedaterange.lowerBound","-21d"); //f1.format(startdate)//Integer.toString(startdate.getYear())+'-'+Integer.toString(startdate.getMonth())+'-'+Integer.toString(startdate.getDay())
    //map.put("4_group.1_relativedaterange.lowerOperation",">=");
    map.put("4_group.1_relativedaterange.property","@jcr:content/pubDate/date");
    log.debug("using relative-date accelerator for this query");
}



// --------------------
// (6) SORTING OPTIONS
// --------------------


/* the default is to sort by reverse chron... */
//map.put("orderby","@pubDate/date"); // changed to date sorting, sld.
map.put("orderby", "@jcr:content/pubDate/date"); // testing this out June 6th. Sept 30th.
if(date_mode){
  // stick with default sorting (ascending) in date mode...
} else {
  map.put("orderby.sort","desc"); // ...but reverse chron for all requests other than when in date mode.
}
map.put("orderby.index","true");

/* ...unless the user has expressed a choice: */
String orderby = properties.get("orderBy",""); log.debug("order by = " + orderby);
if(orderby.equals("alpha")){
    map.put("orderby","@jcr:content/jcr:title"); 
    map.put("orderby.sort","asc"); 
    map.put("orderby.index","true");    
}

/* but if there is a query string, then do a fulltext search on that string and order results by score */   
if(queryString!=null && !queryString.trim().equals("")){ 
    //queryString = queryString.replaceAll(" ", " OR ");
    map.put("3_group.p.or","true");
    map.put("3_group.1_fulltext",queryString);
    map.put("3_group.1_fulltext.relPath", "jcr:content"); // query appears to work whether or not you include this line.
    //map.put("3_group.2_fulltext",queryString); // you already do a fulltext over jcr:content, above...is this cq:tag necessary?
    //map.put("3_group.2_fulltext.relPath", "@cq:tags");
    map.put("orderby","@jcr:score"); /* override whatever defaults or choices were made */
    map.put("orderby.sort","desc");
    default_mode = false;
    newsrollTitle = "Search results for '"+ StringEscapeUtils.escapeHtml(queryString) +"'";
} 



// -----------------
// PERFORM THE QUERY
// -----------------

	endTime = System.nanoTime();
	duration = endTime - startTime;
    log.debug("Timer: Ready to perform the query: " + (float)duration/1000000000);


List<Hit> hitList = builder.createQuery(PredicateGroup.create(map), session).getResult().getHits();
List<Page> pageList = new ArrayList<Page>();


	endTime = System.nanoTime();
	duration = endTime - startTime;
    log.debug("Timer: Just performed the query: " + (float)duration/1000000000);

%>

<div class="newsrollcontainer">
  <%-- optional header widget. note this precludes you from programmatically hooking up the more button to a specific search. --%>
  <%-- <cq:include resourceType="/apps/public/components/content/header" path="includedHeaderWidget" /> --%>
      <h2 class="newsrolltitle underline"><%=newsrollTitle%></h2>

<% if(properties.get("searchBox","false").equals("true")){%>
      <cq:include resourceType="/apps/public/components/content/searchWidget" path="includedSearchBox" />
<%}%>

<%
    if(hitList.size() == 0){
%>  
    <cq:include script="empty.jsp"/>
<%  
} else {
    for(int i = 0; i < hitList.size(); i++){
      Page testPage = hitList.get(i).getResource().adaptTo(Page.class);
      Boolean skip = false;
      // omit the import pages themselves (which are news pages too)
      if(testPage.getPath().equals(Constants.PATH_TO_SCIENCESPOTLIGHTIMPORTER_FOLDER)||
         testPage.getPath().equals(Constants.PATH_TO_PETRIDISHIMPORTER_FOLDER)||
         testPage.getPath().equals(Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER)){ /* there has to be a better way.... */
        skip=true;
      }
      // if we are doing post-query path filtering, do that now so we only retain items from paths we want
      if( paths_filter ){ //.*(/content/public/en/news/center-news|/content/public/en/news/releases|/content/public/en/news/hutch-magazine).*
          if( !(testPage.getPath().matches( paths_filter_regex )) ){ 
				skip = true;
                } 

          // i tested this simpler equivalent approach but it has no time advantage over the regex:
          /*boolean flag = false;
          for( int j=0; j<paths.size(); j++ ){
              if( testPage.getPath().startsWith( paths.get(j) )){
				  flag=true;
                  break;
              }
          }
          if(flag==false){ skip=true; }
          */

      }
      if(!skip && excludeTags.length > 0){ //Check to see if the page we are looking at should not be added to the list
          Tag[] pageTags = testPage.getTags();
          for(Tag s : pageTags){
              for(Tag t : excludeTags){
                 if(!(t==null) && t.equals(s)){
                    skip = true;
                    break; // No need to continue the loop if we get a match
                 }
              }
              if(skip == true){
                  break; // No need to continue the loop if we get a match
              }
          }
      }
      if(skip == false){
          pageList.add(testPage); 
      }
    }

    // in default mode, prepend any "pinned" articles here
    int count_pinned = 0;
    if(default_mode){
        if(mustDisplay != null){
            for( int i=mustDisplay.length-1; i>-1; i-- ){
				Page thePage = resourceResolver.getResource(mustDisplay[i]).adaptTo(Page.class);
                if( thePage!=null ){
                    if(pageList.contains(thePage)){
						pageList.remove(thePage);
                    }
                    pageList.add(0, thePage);
                    count_pinned++;
                }
            }
        }
    }


    // USER-REQUESTED PAGING
    // if the user is asking for skip starting, their request is in the LAST selector to the page. That integer will
    // be the Nth page to display. 0=first page (same as no selector); 1=second page; etc.
    // this is only in play for components where a max is set by the user (maxnum)
    int startAt = 0; // option base 0, 0 means start with the first element
    if(selectors.length>0 && maxnum>0){        
        if(pageNum>0){
            startAt = pageNum * maxnum; // + count_pinned ;
            //if(startAt>pageList.size()-1) startAt=pageList.size()-1; // never start beyond the maxnum of elements in the list, set to the last element in the list
            if(startAt>pageList.size()-1) startAt=pageList.size(); // never allow them to ask for more articles than I have.
            if(startAt<0) startAt=0; // because I'm a cautious guy.
        }
    }



    // -------------------------
    // ITERATE OVER THE RESULTS
    // -------------------------


    Iterator<Page> pageIterator = pageList.listIterator(startAt); // startAt = number of the element you want to start with (option base 0), default is 0.

    if(!pageIterator.hasNext()){
%>  
    <cq:include script="empty.jsp"/>
<%      
    } else {
%>
<div class="newsroll-content">
<%

Integer ct = 0;
while(pageIterator.hasNext() && (maxnum.equals(0)||(ct++<maxnum))){
    Page nextPage = pageIterator.next();
    String pageDescription = nextPage.getDescription();

    String source="";
    if(nextPage.getPath().contains(Constants.PATH_TO_CENTERNEWS_FOLDER)){ source = " in <a href='"+Constants.PATH_TO_NEWS_FOLDER+".html'>Hutch News</a>"; }
    if(nextPage.getPath().contains(Constants.PATH_TO_PETRIDISHIMPORTER_FOLDER)){ source = " in <a href='http://questmagazine.wordpress.com' target='_blank'>Petri Dish</a>"; } 
    if(nextPage.getPath().contains(Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER)){ source = " in <a href='"+ Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER+".html'>Hutch Magazine</a>"; }
    if(nextPage.getPath().contains(Constants.PATH_TO_NEWSRELEASES_FOLDER)){ source = " in <a href='"+ Constants.PATH_TO_NEWSRELEASES_FOLDER+".html'>News Releases</a>"; } 
    if(nextPage.getPath().contains(Constants.PATH_TO_SCIENCESPOTLIGHT_FOLDER)){ source = " in <a href='"+ Constants.PATH_TO_SCIENCESPOTLIGHT_FOLDER+".html'>Science Spotlight</a>"; } 
    if(nextPage.getPath().contains(Constants.PATH_TO_VIDD_NEWS_FOLDER)){ source = " in <a href='"+ Constants.PATH_TO_VIDD_NEWS_FOLDER+".html'>VIDD News</a>"; } 

    Date pubdate = nextPage.getProperties().get("pubDate/date", nextPage.getProperties().get("cq:lastModified", Date.class));    

    String description = nextPage.getProperties().get("jcr:description","");

    String byline = nextPage.getProperties().get("byline/text", "");
    String subtitle = nextPage.getProperties().get("newssubtitle/text", "");

    // title should match that shown on the actual news page. note this uses the Title foundational component which differs from our displayTitle().
    String articletitle = nextPage.getProperties().get("title/"+NameConstants.PN_TITLE, String.class); //String articletitle = nextPage.getProperties().get("title/jcr:title", "");
    if( articletitle==null || articletitle.equals("") ) {
		articletitle = nextPage.getPageTitle();
    }
    if( articletitle==null || articletitle.equals("") ) {
		articletitle = nextPage.getTitle();
    }
    if( articletitle==null || articletitle.equals("") ) {
		articletitle = nextPage.getName();
        }

    String item_index = Integer.toString( ct + startAt ); // integer 1..N used for various classes and social media inits
%>

  <div class="newsroll-item item<%=item_index%>">
    <span class="social_target"><%=item_index%></span><%-- this is used to identify the social share element to init upon Load More stories --%>

      <h2 class="news-title"><a href="<%= nextPage.getPath() %>.html"><%= xssAPI.filterHTML(articletitle) %></a></h2> <% // was: xssAPI.filterHTML(FHUtilityFunctions.displayTitle(nextPage)) %>
    <% if(!subtitle.equals("")){ %> <h4 class="news-subtitle"><%=xssAPI.filterHTML(subtitle)%></h4><% } %>   
    <% if(showDescription && !"".equals(description)){ %><span class="news-description"><%=description%></span><br /><% } %>
    <% if(!omitDate){ %><span class="news-date"><%=FHUtilityFunctions.APStylize(pubdate)%></span><% } %>
    <% if(!omitSource){ %><span class="news-source"><%=source%></span><% } %>
    <% if(!byline.equals("")){ %> | <span class="news-source"><%=byline%></span><% } %>

      <p></p>  

    <% // print out the article's body.
       //
       // 1) in default mode (latest news) you want to dive into the article body and pull back everything up
       // to the first Read More component (if missing, you'll get the whole article body).
       //
       // 2) in non-default mode (e.g., by tag or by date), you are grabbing the description meta


    if(default_mode){

        // we are in "Latest News" format, so make it a rich dive into the article's body.
        // note that imagery, if any, comes from the article body, not from a thumbnail, in this mode
   		ParagraphSystem parsys = nextPage.getContentResource("articletext") != null ?
          new ParagraphSystem(nextPage.getContentResource("articletext")) : null;
    	String text = "There is no text for this item."; //nextPage.getText();
		if (parsys == null) {
            %><p class="cq-edit-only"><%= xssAPI.filterHTML(text) %></p><%
	    } else if(nextPage.getPath().contains(Constants.PATH_TO_HUTCH_MAG_IMPORTER_FOLDER)) { 
            %><p class="cq-edit-only"><%= xssAPI.filterHTML("Omitting description for Hutch Magazine articles imported from Serena.") %></p><%	    
	    }
	    else {

               for (Paragraph par : parsys.paragraphs()) {
                   //out.write(par.getResourceType());
                    if (par.getResourceType().endsWith("newsbreak")) {
                        Node node = par.adaptTo(Node.class);
                        String readmorelink = "Continue reading";
                        Property p = null;
                        if(node.hasProperty("text")){ p = node.getProperty("text"); }
                        if(p!=null) { readmorelink = p.getString(); }
                        %><p class="read_more"><a href="<%= xssAPI.getValidHref(nextPage.getPath()+".html") %>" <%
                            %>title="<%= xssAPI.encodeForHTMLAttr(nextPage.getTitle()) %>" <%
                            %>onclick="blogSearchTrackClick('<%= xssAPI.encodeForJSString(nextPage.getTitle()) %>')"<%
                            %>><%=readmorelink%></a> &gt;</p><%
                        break;
                    }
                   // this next statement takes away the ability to EDIT the included components in this newsroll:
                   IncludeOptions.getOptions(request, true).forceSameContext(true);
                    %><sling:include resource="<%= par %>"/><%
                }
	    } 


    } else { 

        // we are in "compact mode" e.g., list by tag or by date, so grab the thumb from the servlet
        // and the article text from the description meta. if the asset does not have a thumb, then omit the image part.
        Boolean hasimage = false;
        Resource r = nextPage.getContentResource("image");
        if(r!=null){
           Image imageCheck = new Image(r);
           if(imageCheck!=null && imageCheck.hasContent()){
			 hasimage = true;
           }
        }
        // Hutch Magazine articles imported by the importer have the entire body of the article in their jcr:description (why we did that is beyond me). we don't want to print that out.
        String desc = StringEscapeUtils.escapeHtml(nextPage.getProperties().get("jcr:description", ""));
        if(nextPage.getPath().contains(Constants.PATH_TO_HUTCH_MAG_IMPORTER_FOLDER)) { desc=""; }
        
        if(hasimage){%>
          <div class="compact">
            <div class="thumbnail">
                <a href="<%=nextPage.getPath()+".html"%>"><img class="frame" alt="" src="<%=nextPage.getPath()+".pagethumb.180.135.fit.png"%>"></a>
        	</div>
        	<div class="description">
            	<%= desc %>
			</div>
          </div>
        <% } else { %>
            <div class="compact">
            	<%= desc %>
			</div>
		<% } %>

      <p class="view_story"><a href="<%=nextPage.getPath()+".html"%>">View story</a> &gt;</p>


    <% } %>


    <!-- social share -->
    <% IncludeOptions.getOptions(request, true).forceSameContext(true); %>
    <%--<cq:include path="newsSocialShare" resourceType="public/components/content/socialshare"/>--%>


	<!--  SOCIAL -->
	<div class="newsSocialShare socialshare">
		<div class="share_options">
	  		<span class="a2a_kit_manual_<%=item_index%> a2a_default_style">
                <a class="a2a_dd" href="http://www.addtoany.com/share_save">
                   <img src="<%= currentDesign.getPath() %>/img/buttons/plus_icon_static.png" class="rolloverImage" data-hover="<%= currentDesign.getPath() %>/img/buttons/plus_icon_hover.png"><span class="share_link_text">Share</span>
                </a>
  			</span>
		</div>
		<script>
  			if( typeof a2a == 'object' ) { 
	        	a2a_config.linkname="<%= xssAPI.filterHTML(FHUtilityFunctions.displayTitle(nextPage)) %>";
    	    	a2a_config.linkurl="http://www.fredhutch.org<%= resourceResolver.map(nextPage.getPath()) %>.html";
        		//a2a_config.target = ".a2a_dd_manual";
    			a2a_config.target = ".a2a_kit_manual_<%=item_index%>";
    			a2a.init('page'); 
  			}
		</script>
	</div>      






<!-- tags -->
<%  
    String location = nextPage.getPath() + "/jcr:content/taglist";
    // this never worked: IncludeOptions.getOptions(request, true).forceCurrentPage(nextPage); //out.write(nextPage.getPath()); 
    request.setAttribute("targetPageForTagList", nextPage.getPath()); // https://daycare.day.com/home/fhcrc/fhcrc_us/customer_services/59092.html
    IncludeOptions.getOptions(request, true).forceSameContext(true); 
    %>
    <!--tags should be from nextPage, not from this page: -->
<div class="taglist">
    <cq:include path="<%= location %>" resourceType="public/components/content/newsplus/taglist"/>
</div>


</div><!-- /newsroll-item -->


    <hr class="story_divider" />



<%
}

        // ------------------------
        // LOAD MORE STORIES button
        // ------------------------

    	// if there are more articles to read than were shown, print out a READ MORE button
        // and hook it up to retrieve the "next" page of results. you have to preserve any selectors
        // that were there. TODO: make this replace the div rather that write into it.
        // note that because we use .load() with a selector, any javascript in that selected content will NOT be executed. http://api.jquery.com/load/
    	if(pageIterator.hasNext()){
            String npage = currentPage.getName() + "." + Integer.toString(pageNum+1) + ".html";    // next page (for paging and for google rel link)
            String ppage;																		   // previous page (for google rel link)
            if(pageNum==1){
                ppage = currentPage.getName() + ".html";  
            } else {
                ppage = currentPage.getName() + "." + Integer.toString(pageNum-1) + ".html";  
            }

            // preserve the selectors if there are any
            if(selectors.length>1 && selectors[0].equals("tag")){
                // TODO: Harden this
				npage = currentPage.getName() + ".tag." + URLEncoder.encode(selectors[1],"UTF-8").replace("+","%20") + "." + Integer.toString(pageNum+1) + ".html";
                if(pageNum==1){
                    ppage = currentPage.getName() + ".tag." + URLEncoder.encode(selectors[1],"UTF-8").replace("+","%20") + ".html";
                } else {
                    ppage = currentPage.getName() + ".tag." + URLEncoder.encode(selectors[1],"UTF-8").replace("+","%20") + "." + Integer.toString(pageNum-1) + ".html";
                }
            }
            if(selectors.length>1 && selectors[0].equals("date")){
                // TODO: Harden this
				npage = currentPage.getName() + ".date." + URLEncoder.encode(selectors[1],"UTF-8").replace("+","%20") + "." + Integer.toString(pageNum+1) + ".html";
                if(pageNum==1){
                    ppage = currentPage.getName() + ".date." + URLEncoder.encode(selectors[1],"UTF-8").replace("+","%20") + ".html";
                } else {
                    ppage = currentPage.getName() + ".date." + URLEncoder.encode(selectors[1],"UTF-8").replace("+","%20") + "." + Integer.toString(pageNum-1) + ".html";
                }
            }
            //out.write("<div style='padding:15px;background-color:#0659a1;font-size:170%;font-family:'franklin-gothic-urw-n5','franklin-gothic-urw','Franklin Gothic Medium',Arial,Sans-serif;font-weight:500;text-align:center' onclick=\"$(this).attr('style','').html('Loading...').attr('onclick','').load('"+npage+" div.newsroll-content',function(){ if(typeof a2a=='object'){  setTimeout( function(){a2a.init('page');$('.share_options').show();}, 500 )  } });return false;\"><a x-cq-linkchecker='skip' href=\""+npage+"\">LOAD MORE STORIES</a> &darr;</div>");
            //out.write("<div class='loadmore' style=\"padding:15px;color:#fff;background-color:#0659a1;font-size:150%;font-family:'franklin-gothic-urw-n5','franklin-gothic-urw','Franklin Gothic Medium',Arial,Sans-serif;font-weight:500;text-align:center\" onclick=\"$(this).attr('style','').html('Loading...').attr('onclick','').load('"+npage+" div.newsroll-content',function(){ if(typeof a2a=='object'){  a2a_config.target='.a2a_kit_manual'; setTimeout( function(){a2a.init('page');$('.share_options').show();}, 500 )  } });return false;\"><a x-cq-linkchecker='skip' href=\""+npage+"\" style='color:white;'>LOAD MORE STORIES</a> &darr;</div>");
            //fail: out.write("<div class='loadmore' style=\"padding:15px;color:#fff;background-color:#0659a1;font-size:150%;font-family:'franklin-gothic-urw-n5','franklin-gothic-urw','Franklin Gothic Medium',Arial,Sans-serif;font-weight:500;text-align:center\" onclick=\"$(this).attr('style','').html('Loading...').attr('onclick','').load('"+npage+" div.newsroll-content',function(){ if(typeof a2a=='object'){ function(){ init_unloaded_share_elements(); }  } });return false;\"><a x-cq-linkchecker='skip' href=\""+npage+"\" style='color:white;'>LOAD MORE STORIES</a> &darr;</div>");
            out.write("<div class='loadmore' onclick=\"$(this).removeClass('loadmore').html('Loading...').attr('onclick','').load('"+npage+" div.newsroll-content',function(){ if(typeof a2a=='object'){  init_unloaded_share_elements();  } });return false;\"><a x-cq-linkchecker='skip' href=\""+npage+"\">LOAD MORE STORIES</a></div>");

            // i am WELL AWARE that the next lines should appear in <head> but that's getting tricky. let's add here and see what happens.
            // https://support.google.com/webmasters/answer/1663744?hl=en
            out.write("\n\n<link rel='next' value='" + npage + "' />\n");
            if(pageNum>0){
				out.write("\n<link rel='prev' value='" + ppage + "' />\n");
            }

        }


%>

<%-- anchor to determine if "load more" is visible (above me) --%>
<div id="loadmoreanchor"></div>



<script type='text/javascript'>
    // loop through the page and locate all uninitialized social share elements.
    // you find those because they have href ending in "share_save" (they haven't yet gotten their #url=... via the a2a_init()).
    // for those, grab the title, url, a2a target classname, and init them
    function init_unloaded_share_elements(){
       $(".newsroll-item").has("a.a2a_dd[href$='share_save']").each( function(){ a2a_config.linkname=$(this).find(".news-title").text(); a2a_config.linkurl='http://www.fredhutch.org'+$(this).find(".news-title a").attr('href'); a2a_config.target=".a2a_kit_manual_"+$(this).find(".social_target").text(); a2a_init('page');    } );
       $('.share_options').show();		
	}


    // utility function to determine if a particular elements has scrolled into view    
    // returns true or false
    function isScrolledIntoView(elem) {
	    var docViewTop = $(window).scrollTop();
	    var docViewBottom = docViewTop + $(window).height();

	    var elemTop = $(elem).offset().top;
	    var elemBottom = elemTop + $(elem).height();

	    var shim = .30 * $(window).height(); // make this a percentage of the visible window (viewport). fine tune to your liking.

	    return ((elemBottom >= docViewTop - shim) && (elemTop <= docViewBottom - shim)
    	  && (elemBottom <= docViewBottom - shim) &&  (elemTop >= docViewTop - shim) );
	}

    $(window).scroll(function(){//alert('scrolled');
        if( isScrolledIntoView($("div#loadmoreanchor")) ){ //alert('yes, in view');
            $("div.loadmore").last().click();
        }
    });


</script>


</div>




<%



    }
}
%>


    <% 	endTime = System.nanoTime();
	    duration = endTime - startTime;
        log.debug("Timer: Complete: " + (float)duration/1000000000);
     %>

</div>



<%!
private String[] increaseArray(String[] theArray, int increaseBy)  
{  
    int i = theArray.length;  
    int n = ++i;  
    String[] newArray = new String[n];  
    for(int cnt=0;cnt<theArray.length;cnt++)
    {  
        newArray[cnt] = theArray[cnt];  
    }  
    return newArray;  
}  

    %>