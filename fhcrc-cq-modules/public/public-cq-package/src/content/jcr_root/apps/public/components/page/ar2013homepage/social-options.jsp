<%@include file="/libs/foundation/global.jsp" %><%
%><%@ page import="java.net.URLEncoder" %>

<% // setup the linkurl and the linkname that addtoany wants to use.
   // there is a linknote sent (empty), not sure what that's for or if we'll use it
   String linkurl = "http://www.fredhutch.org" + resourceResolver.map(currentPage.getPath())+".html";
   String linkname = properties.get("maintitle/jcr:title","");   

   if(linkname.equals("")){
	   linkname = currentPage.getPageTitle();
   }
   if(linkname==null || linkname.equals("")){
       linkname = currentPage.getTitle();
   }
   if(linkname==null || linkname.equals("")){
       linkname = currentPage.getNavigationTitle();
   }
   if(linkname==null || linkname.equals("")){
       linkname = currentPage.getName();
   }   
   
   // the homepage is special
   if(currentPage.getProperties().get("cq:template","").contains("ar2013homepage")){
      //linkname = "Ending Cancer Together"; // commenting this out, because we now have multiple annual reports based on this one template.
      //linkname = "Fred Hutch Annual Report";	   
   } else {
   	  linkname = "Fred Hutch Annual Report: " + linkname;
   }
   
   linkurl = URLEncoder.encode( linkurl );
   linkname = URLEncoder.encode( linkname );

   
   // switcher to locate the targets of the download/donate buttons. on content pages you have to go up one level
   String social_target_path = "";
   if(currentPage.getProperties().get("cq:template","").contains("ar2013homepage")){
		social_target_path = currentPage.getPath();
   } else {
   		social_target_path = currentPage.getParent().getPath();
   }   
   
%>

<!-- social element -->
<div class="social_options">
  <ul>
    <li><a class="a2a_button_facebook" target="_blank" href="http://www.addtoany.com/add_to/facebook?linkurl=<%=linkurl%>&amp;type=page&amp;linkname=<%=linkname%>&amp;linknote="><img src="<%= currentDesign.getPath() %>/img/icons/ar_fb.png" alt="Facebook icon"><br/>Share</a></li>
    <li><a class="a2a_button_twitter" target="_blank" href="http://www.addtoany.com/add_to/twitter?linkurl=<%=linkurl%>&amp;type=page&amp;linkname=<%=linkname%>&amp;linknote="><img src="<%= currentDesign.getPath() %>/img/icons/ar_tw.png" alt="Twitter icon"><br/>Tweet</a></li>
    <li><a class="a2a_button_email" target="_blank" href="http://www.addtoany.com/add_to/email?linkurl=<%=linkurl%>&amp;type=page&amp;linkname=<%=linkname%>&amp;linknote="><img src="<%= currentDesign.getPath() %>/img/icons/ar_email.png" alt="Email icon"><br/>Email</a></li>
    <li><a target="_blank" href="<%= social_target_path %>/download.html"><img src="<%= currentDesign.getPath() %>/img/icons/ar_pdf.png" alt="PDF icon"><br/><span class="droptext">Download Annual Report</span> PDF</a></li>
    <li class="donate"><div class="CSSButton"><a target="_blank" class="button colorTheme0 designTheme0" href="<%= social_target_path %>/donate.html">DONATE NOW</a></div></li>
  </ul>
</div><!-- /social_options -->      

<!-- donate now button that appears for mobile form factor -->
<div class="mobile_donate_now">
  <div class="CSSButton"><a href="<%= social_target_path %>/donate.html" class="button colorTheme0 designTheme0">DONATE NOW</a></div>        
</div>
