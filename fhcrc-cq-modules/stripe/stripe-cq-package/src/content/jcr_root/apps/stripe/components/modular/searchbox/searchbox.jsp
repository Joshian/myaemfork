<%@include file="/libs/foundation/global.jsp"%><%
String searchPath = currentPage.getAbsoluteParent(2).getPath() + "/en/search.html";
log.info("searchPath = " + searchPath);
%>
<form id="utility_search" class="utility_search" action="<%= searchPath %>">
	<input type="text" name="q" value="Search..." class="search_term"></input>
	<input type="image" class="search_button" src="/etc/designs/stripedesigns/stripe/clientlibs/img/magnify-glass.gif" alt="GO"></input>
</form>
<script type="text/javascript">
  $("#utility_search input.search_term").focus( function(){ if(this.value=="Search..."){ this.value=""; } } );
  $("#utility_search input.search_term").blur( function(){ if($.trim(this.value)==""){ this.value="Search..."; } } );
</script>