<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.text.SimpleDateFormat,
                 java.text.DateFormat,
                 java.util.Date" %>
<% 
Date targetDate = properties.get("targetDate", Date.class);
long longDate;

if(targetDate != null){
	longDate = targetDate.getTime();
} else {
%>
<cq:include script="empty.jsp" />
<%	
return;
}
%>
<div class="countdownContainer">
<table class="hideMe">
<tr>
<td class="quarter">Days</td>
<td class="quarter">Hours</td>
<td class="quarter">Minutes</td>
<td class="quarter">Seconds</td>
</tr>
<tr>
<td id="cntdwn" colspan="4">&nbsp;</td>
</tr>
</table>
</div>

<script type="text/javascript">
<!--
//Variable declaration for the countdown clock
var CDC_TargetDate = <%= longDate %>;
var CDC_BackColor = "white";
var CDC_ForeColor = "navy";
var CDC_CountActive = true;
var CDC_CountStepper = -1;
var CDC_LeadingZero = true;
var CDC_DisplayFormat = "&nbsp;&nbsp;&nbsp;%%D%%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%%H%%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%%M%%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%%S%%";
var CDC_FinishMessage = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0";

$('.countdownContainer table').css({'width':'240px','margin-bottom':'10px'});
$('.countdownContainer td.quarter').css({'width':'25%','text-align':'center'});
$('.countdownContainer td#cntdwn').css({
    'background-image':'url("<%= currentDesign.getPath() %>/img/backgrounds/eventdate-back.jpg")',
    'background-repeat':'no-repeat',
    'font-family':'Arial, Helvetica, sans-serif',
    'font-size':'18px',
    'color':'#126BBE',
    'width':'100%',
    'height':'34px',
    'background-position':'top left',
    'padding-top':'5px'});


//Author: Robert Hashemian
//http://www.hashemian.com/

//You can use this code in any manner so long as the author's
//name, Web address and this disclaimer is kept intact.

function CDC_calcage(secs, num1, num2) {
  var s = ((Math.floor(secs/num1))%num2).toString();
  if (CDC_LeadingZero && s.length < 2)
    s = "0" + s;
  return "<b>" + s + "</b>";
}

function CDC_CountBack(secs) {
  if (secs < 0) {
    $("#cntdwn").html(CDC_FinishMessage);
    return;
  }
  var DisplayStr = CDC_DisplayFormat.replace(/%%D%%/g, CDC_calcage(secs,86400,100000));
  DisplayStr = DisplayStr.replace(/%%H%%/g, CDC_calcage(secs,3600,24));
  DisplayStr = DisplayStr.replace(/%%M%%/g, CDC_calcage(secs,60,60));
  DisplayStr = DisplayStr.replace(/%%S%%/g, CDC_calcage(secs,1,60));

  $("#cntdwn").html(DisplayStr);
  if (CDC_CountActive)
    setTimeout("CDC_CountBack(" + (secs+CDC_CountStepper) + ")", CDC_SetTimeOutPeriod);
}
/*
function putspan(backcolor, forecolor) {
 document.write("<span id='cntdwn' style='background-color:" + backcolor + 
                "; color:" + forecolor + "'></span>");
}
*/
if (typeof(CDC_BackColor)=="undefined")
  CDC_BackColor = "white";
if (typeof(CDC_ForeColor)=="undefined")
  CDC_ForeColor= "black";
if (typeof(CDC_TargetDate)=="undefined")
  CDC_TargetDate = "12/31/2020 5:00 AM";
if (typeof(CDC_DisplayFormat)=="undefined")
  CDC_DisplayFormat = "%%D%% Days, %%H%% Hours, %%M%% Minutes, %%S%% Seconds.";
if (typeof(CDC_CountActive)=="undefined")
  CDC_CountActive = true;
if (typeof(CDC_FinishMessage)=="undefined")
  CDC_FinishMessage = "";
if (typeof(CDC_CountStepper)!="number")
  CDC_CountStepper = -1;
if (typeof(CDC_LeadingZero)=="undefined")
  CDC_LeadingZero = true;

CDC_CountStepper = Math.ceil(CDC_CountStepper);
if (CDC_CountStepper == 0)
	CDC_CountActive = false;
var CDC_SetTimeOutPeriod = (Math.abs(CDC_CountStepper)-1)*1000 + 990;
//putspan(BackColor, ForeColor);
var CDC_dthen = new Date(CDC_TargetDate);
var CDC_dnow = new Date();
if(CDC_CountStepper>0)
  var CDC_ddiff = new Date(CDC_dnow-CDC_dthen);
else
  var CDC_ddiff = new Date(CDC_dthen-CDC_dnow);
var CDC_gsecs = Math.floor(CDC_ddiff.getTime()/1000);                                                                      
CDC_CountBack(CDC_gsecs);
//-->
</script>