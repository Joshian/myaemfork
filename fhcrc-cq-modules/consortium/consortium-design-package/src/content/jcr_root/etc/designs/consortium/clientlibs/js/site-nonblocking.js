/* non-blocking javascript for www.cancerconsortium.org */

/* this element is called last in the dom, so ideally
   nothing herein prevents the design from rendering. */
   
   
/* Last modified: Mar 28, 2013
   Change log:
     Jan 19, 2013 - created
     Mar 14, 2013 - adds handlers for search icons/links
     Mar 28, 2013 - comments out the closeMobileMenus on resize, that wasn't working for mobile devices.
     
 */


/* scroll the webpage to hide the address bar */
consortiumApp.addEvent(window,"load",function(){setTimeout(function(){window.scrollTo(0,0);})});

/* fine-tune the position of the body content (breadcrumbs, h1 and content */
consortiumApp.addEvent(window,"load", function(){ consortiumApp.adjustBodyPosition(); });
consortiumApp.addEvent(window,"resize",function(){ /*consortiumApp.closeMobileMenus();*/ consortiumApp.adjustBodyPosition(); });

/* set up click events for the mobile menu button */
consortiumApp.addEvent(window,"load",function(){
  var o_menu = document.getElementById('pushdown_menu');
  var o_button = document.getElementById('mobile_menu_button');
  if( o_menu && o_button ){
	  consortiumApp.addEvent(o_button,"click",function(){
      if(o_menu.style.display=='block') o_menu.style.display='none';
      else o_menu.style.display='block';
      consortiumApp.adjustBodyPosition();
    });
  }
});

/* set up click events for the utility nav search button */
consortiumApp.addEvent(window,"load",function(){
  var o_search = document.getElementById('utility_search');
  var o_button = document.getElementById('utility_nav_search_icon');
  var o_form   = document.getElementById('utility_search_form');
  if( o_search && o_button && o_form ){
	consortiumApp.addEvent(o_button,"click",function(e){
      if(o_search.style.display=='block') {
        if(o_form.q && o_form.q.value){
          o_form.submit();
        }
        o_search.style.display='none';
      } else o_search.style.display='block';
      consortiumApp.adjustBodyPosition();
      var evt = e ? e:window.event;
      if (evt.preventDefault) evt.preventDefault();
      evt.returnValue = false;
      return false;
    });
  }
});

/* set up click events for the mobile search button */
consortiumApp.addEvent(window,"load",function(){
  var o_search = document.getElementById('pushdown_search');
  var o_button = document.getElementById('mobile_search_button');
  if( o_search && o_button ){
	consortiumApp.addEvent(o_button,"click",function(){
      if(o_search.style.display=='block') o_search.style.display='none';
      else o_search.style.display='block';
      consortiumApp.adjustBodyPosition();
    });
  }
});





