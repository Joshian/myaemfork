<%@include file="/libs/foundation/global.jsp"%><%
%>

  <!-- body contents -->
  <div class="body" id="body_element">
    <div class="body_contents">
      <cq:include script="breadcrumbs.jsp" />
      <cq:include script="clusterTitle.jsp" />      
      <div class="page_content" id="page_content">      
        <div class="main_column">
          <% if(!properties.get("isClusterHead","false").equals("true")) { // disappears when currentPage is the cluster head %>
             <cq:include path="maintitle" resourceType="foundation/components/title"/> 
          <% } %>
          <cq:include path="mainpar" resourceType="foundation/components/parsys"/>
          <!-- remote-wrap-api-marker -->
        </div>   
        <% if(properties.get("isRightCol","false").equals("true")) { %>
        <div class="right_column">
          <div class="right_column_contents">
            <cq:include path="rightcolpar" resourceType="foundation/components/parsys"/>
          </div>
          <div class="right_column_fade"></div>
        </div>  
        <% } %>
        <div class="clear"></div>       
      </div>

      <!-- sidebar holds navigation and other elements -->
      <% if(properties.get("omitSidebar","false").equals("false")) { %>
      <div class="sidebar">
        <div class="sidebar_contents">
          <% if(properties.get("omitSectionNav","false").equals("false")) { %>
          <cq:include script="section-navigation.jsp"/>
          <% } %>
          <!-- related content, if any: -->
          <div class="related">
            <cq:include path="relatedpar" resourceType="foundation/components/parsys"/>
          </div><!-- /related -->
          <div class="sidebar_fade"></div>
        </div>
      </div>
      <% } %>

      <div class="clear"></div>
    </div>
  </div>