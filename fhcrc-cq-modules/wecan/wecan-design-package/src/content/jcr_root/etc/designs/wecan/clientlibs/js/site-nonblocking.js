weCanApp.addEvent(window, "load", function() {
	
	weCanApp.adjustBodyPosition();
	weCanApp.adjustFooterHeight();
	
	$("#globalNav>ul li").hoverIntent({
		over: function(){weCanApp.globalNav.activateItem(this)},
		out: function(){weCanApp.globalNav.activateItem(this)},
		timeout: 500
	});
	$("#mobileMenuButton").click(function() {
		weCanApp.globalNav.toggleMobileNav();
	});
	$("#mobileSearchButton").click(function() {
		weCanApp.globalNav.toggleMobileSearch();
	});
	$(window).resize(function() {
		weCanApp.adjustBodyPosition();
		weCanApp.adjustFooterHeight();
	});
	
});