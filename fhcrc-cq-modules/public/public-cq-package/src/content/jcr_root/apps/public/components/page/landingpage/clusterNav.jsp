<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.commons.Doctype,
        java.util.ArrayList,
        com.day.cq.wcm.api.PageFilter,
        com.day.cq.wcm.foundation.Navigation,
        com.day.text.Text" %>
<%!
public void recursiveNav(Page current, PageFilter filter, Page finalPage, ArrayList trail, JspWriter out) throws Exception {
    Navigation nav = new Navigation(current, current.getDepth() - 1, filter, 1);
    for (Navigation.Element e: nav){
        switch(e.getType()) {
        case NODE_OPEN:
            out.println("<ul>");
            break;
        case ITEM_BEGIN:
            out.println("<li><a href=\"" + e.getPath() +".html\">" + e.getTitle() + "</a>");
            Page next = e.getPage();
            for(int i=0; i<trail.size(); i++) {
            	if(next.getPath().equals(((Page) trail.get(i)).getPath())) {
                    out.println("<ul>");
                    recursiveNav(next, filter, finalPage, trail, out);
                    out.println("</ul>");
                    break;
            	}
            }
            break;
        case ITEM_END:
            out.println("</li>");
            break;
        case NODE_CLOSE:
           out.println("</ul>");
           break;
        }
    }
}
%>
<%
Page clusterHead = currentPage;
String test = clusterHead.getProperties().get("isClusterHead", "false");
ArrayList<Page> al = new ArrayList<Page>();
al.add(clusterHead);
while(!"true".equals(test)){
    if(clusterHead.getParent() == null){
        // If we have hit the top of the tree, then use the branch leader as the clusterHead
        clusterHead = currentPage.getAbsoluteParent(3);
        break;
    }	
	clusterHead = clusterHead.getParent();
	test = clusterHead.getProperties().get("isClusterHead", "false");
	al.add(clusterHead);
}
PageFilter filter = new PageFilter(request);

%>
<!-- left column (sidebar) -->
  <div id="fh_sidebar" class="fh_sidebar"><div id="fh_sidebar_content" class="fh_sidebar_content">
    <a name="section-navigation" class="clear"></a>
    <div class="section-nav"><ul>
    
<%

recursiveNav(clusterHead, filter, currentPage, al, out);

%>
</ul></div><!-- /section-nav -->   