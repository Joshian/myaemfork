<%@include file="/libs/foundation/global.jsp" %><%
    StringBuffer cls = new StringBuffer();
    for (String c: componentContext.getCssClassNames()) {
        cls.append(c).append(" ");
    }
%><body class="<%= cls %>">
    <cq:include script="header.jsp"/> 
    <cq:include script="bodyContent.jsp"/>
    <cq:include script="footer.jsp"/>

</body>