package org.fhcrc.general.search.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SearchCacheData {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    private Date lastUpdate;
    private Long count;

    public SearchCacheData(Long count, Date lastUpdate) {
        this.count = count;
        this.lastUpdate = lastUpdate;
    }
    
    public Long getCount() {
        return count;
    }
    
    public Date getLastUpdate() {
        return lastUpdate;
    }
    
    public boolean isOutOfDate(int ttlMinutes) {
        Date ttlDate = addMinutesToDate(ttlMinutes, lastUpdate);
        if (ttlDate.after(new Date())) {
            return false;
        } else {
            log.info("Cache is expired");
            return true;
        }
    }
    
    private static Date addMinutesToDate(int minutes, Date beforeTime){
        final long ONE_MINUTE_IN_MILLIS = 60000;//millisecs

        long curTimeInMs = beforeTime.getTime();
        Date afterAddingMins = new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
        return afterAddingMins;
    }
}
