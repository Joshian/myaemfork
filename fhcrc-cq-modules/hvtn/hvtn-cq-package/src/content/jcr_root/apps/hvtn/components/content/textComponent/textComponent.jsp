<%--
  Copyright 1997-2009 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Text component

  Draws text.
  
  COLOR THEME:
  colorTheme0, colorTheme1, etc. are controlled
  via design dialog. In the design dialog configure
  the allowed set of text & background colors like this:

     0<tab>Normal (default)
     1<tab>Black text on white background
     2<tab>White text on yellow background
     #<tab>Text the user will see in the dropdown
     etc.

  you do not have to go sequentially, and can use multiple digits if you want.
  now the user will be able to select a background color of your choosing.
  this component places a class, colorTheme#, around the text div.
  add the appropriate styling to your css for the colorTheme# choices you provide.


  DESIGN THEME:
  for complete generality, we add the ability to select a second class.
  you can use this however you want. e.g., 

     0<tab>None (default)
     1<tab>Narrow margins
     2<tab>Blinking text
     #<tab>Text the user will see in the dropdown
     etc.
  
--%>
<%@include file="/libs/foundation/global.jsp"%>
<jsp:useBean id="textBean" scope="request" class="org.fhcrc.hvtn.components.TextComponent" />
<jsp:setProperty name="textBean" property="componentProperties" value="<%= properties %>"/>

<cq:text property="text" tagClass="${ textBean.colorTheme } ${ textBean.designTheme }" />