<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.api.WCMMode,
                 org.fhcrc.tools.FHUtilityFunctions" %>
<%
String underline = properties.get("omitUnderline", "false");
String padding = properties.get("omitPadding", "false");
String classText = "headerwidget";
String linkTarget = properties.get("linkTarget","");
Resource r; 
Boolean isInternal = false;
if(!linkTarget.equals("")){
    linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
}
if(underline.equals("true")){
    classText += " nounderline";
}
if(padding.equals("true")){
    classText += " flushbottom";
}
%>
<div class="<%= classText %>">
    <cq:text property="text" tagName="<%= properties.get("headerSize","h2") %>" tagClass="title" />
<%
if(!properties.get("linkText","").equals("") && !properties.get("linkTarget","").equals("")){
%>    
    <span class="more"><a href="<%= linkTarget %>" <%= isInternal ? "target=\"_blank\"" : "" %>><cq:text property="linkText" /></a><span class="raquo">&gt;</span></span>
<%
}
%>
</div>

<% if(WCMMode.fromRequest(request) == WCMMode.EDIT){
   // trigger a reset of header widgets on this page if we are in edit mode. this moves the "more" link without the user having to refresh. 
   // this doesn't affect rendering on publish or preview.
%>
<script type="text/javascript">
  initHeaderWidgets();
</script>
<% } %>