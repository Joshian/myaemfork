<%--

  XMLSitemap component HTML view.

--%><%@include file="/libs/foundation/global.jsp"%><%--
--%><%@include file="/apps/public/components/page/xmlsitemap/xmlsitemap.class.jsp"%><%--
--%><%@page import="org.apache.commons.lang.StringEscapeUtils"%><%--
--%><%@page session="false"%><%--
--%><%
String rootPath = properties.get("rootPath", "");
Page rootPage = slingRequest.getResourceResolver().adaptTo(PageManager.class).getPage(rootPath);
XMLSitemapFactory xmlSitemapFactory = new XMLSitemapFactory();
XMLSitemap xmlSitemap = xmlSitemapFactory.buildXMLSitemap(rootPage, resourceResolver, request, "news");
List<String> tableHeaders = xmlSitemap.getTableHeaders();
List<Map<String, String>> tableValues = xmlSitemap.getMapValues();
String header;
int i;
%><!DOCTYPE HTML>
<html>
  <head>
    <style type="text/css">
      body {
        font-family: 'Trebuchet MS', Helvetica, sans-serif;
        font-size: 75%;
      }
    </style>
    <cq:includeClientLib css="apps.shared.tablesorter"/>
  </head>
  <body>
    <table>
      <thead><%
      for (i = 0; i < tableHeaders.size(); i++) {
        header = tableHeaders.get(i);
%>
        <th><%= header %></th><%
      }
%>
      </thead>
      <tbody><%
      for (i = 0; i < tableValues.size(); i++) {
        Map<String, String> row = tableValues.get(i);
%>
        <tr>
          <td><%= xmlSitemap.writeLink(row.get("loc")) %></td>
          <td><%= row.get("name") %></td>
          <td><%= row.get("language") %></td>
          <td><%= row.get("genres") %></td>
          <td><%= row.get("pubDate") %></td>
          <td><%= StringEscapeUtils.escapeHtml(row.get("title")) %></td>
          <td><%= row.get("keywords") %></td>
        </tr><%
      }
%>
      </tbody>
    </table>
    <cq:includeClientLib js="cq.jquery"/>
    <script type="text/javascript">
      (function($) {
          $(window).load(function() {
              if ($('h1').length) { return; };
              var $table = $('table'),
                  count = $table.find('tr:visible').length - 1,
                  file = window.location.href.replace('html', 'xml');
              $table.before('<h1>Sitemap file: ' + file + '</h1><p><strong>Number of URLs in this sitemap: ' + count + '</strong></p>');
          });
      })(jQuery);
    </script>
    <cq:includeClientLib js="apps.shared.tablesorter"/>
    <script type="text/javascript">
      (function($) {
        $(document).ready(function() {
          var $table = $('table');
          $table.addClass('tablesorter-blue');
          if ($().tablesorter) {
            $table.tablesorter();
          }
        });
      })(jQuery);
    </script>
  </body>
</html>