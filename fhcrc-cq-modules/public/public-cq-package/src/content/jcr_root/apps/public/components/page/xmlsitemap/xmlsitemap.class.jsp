<%/*
 @TODO Create page property settings for priority and change frequency.
 @TODO Create xmlsitemaps from page template.
 @TODO Detect xmlsitemaps for dynamic index file.
 @TODO Add image and video content.
 @TODO Convert to a bundle.
 @TODO Ensure there are no char encoding/security issues.
 @TODO Finish javadoc documentation.
*/%><%--
--%><%@ page import="org.apache.jackrabbit.util.Text,
                 org.apache.sling.api.resource.ResourceResolver,
                 org.apache.commons.lang.StringEscapeUtils,
                 com.day.cq.commons.date.DateUtil,
                 com.day.cq.wcm.foundation.Sitemap,
                 com.day.cq.wcm.api.PageFilter,
                 com.day.cq.wcm.api.PageManager,
                 com.day.cq.tagging.*,
				 java.lang.IllegalArgumentException,
                 java.net.*,
                 java.util.LinkedList,
                 java.util.ArrayList,
                 java.util.List,
                 java.util.Date,
                 java.util.Calendar,
				 java.util.GregorianCalendar,
                 java.util.HashMap,
                 java.util.Map"%><%--
--%><%@ include file="/libs/foundation/global.jsp" %><%--
--%><%
class XMLSitemapConstants {
    public static final String NEWS_GENRE_PATH = "content/public/en/news/releases";
    public static final String NEWS_NAME = "Fred Hutchinson Cancer Research Center";
    // While the value is -3, this removes pages that are older than 2 days, as time is not a factor.
    public static final int NEWS_DAYS_TILL_PURGE = -3;

    private XMLSitemapConstants() {
        throw new AssertionError();
    }
}

/**
 * Provides a structure and foundation for outputting sitemaps as XML. The class
 * contains a List of Page objects from the root Page defined when the
 * sitemap is created and a List of map objects containing the
 * data for the sitemap.
 */
abstract class XMLSitemap extends Sitemap {
    protected final ResourceResolver resourceResolver;
    protected final ServletRequest request;
    protected List<Page> pages = new ArrayList<Page>();
    protected List<Map<String, String>> mapValues = new ArrayList<Map<String, String>>();
    // @TODO Part of the sitemap index file creation.
    //protected List<Map> sitemaps = new ArrayList<Map>();
    // protected String outputXML;
    // protected String outputHTML;
    // For debugging.
    public String out = "";

    /**
     * Constructor. Populates the pages arraylist with the pages under the root page.
     *
     * @param   rootPage            The starting page for the sitemap tree.
     * @param   resourceResolver    The resourceResolver from the calling .jsp.
     */
    public XMLSitemap(Page rootPage, ResourceResolver resourceResolver, ServletRequest request) {
        super(rootPage);
        this.resourceResolver = resourceResolver;
        this.request = request;
        getPages();
    }

    public String absolutePath(String path) {
        URI uri = URI.create(rootPath() + path + ".html");
        return uri.toString();
    }

    /**
     * Create an anchor from a URL.
     *
     * @param   path    A path to a Page.
     *
     * @return  String  The absolute URL.
     */
    protected String writeLink(String path) {
        String absPath = absolutePath(path);

        return "<a href='" + absPath + "'>" + absPath + "</a>";
    }

    /**
     * Format the last modified date according to http://www.w3.org/TR/NOTE-datetime
     *
     * @param   lastModified    The last modification date as a Calendar.
     *
     * @return  lastModified    The last modification date as a ISO8601 format String.
     */
    protected String formatLastModified(Calendar lastModified) {
        return DateUtil.getISO8601Date(lastModified);
    }

    /**
     * @TODO Externalizer may be an option for handling the
     *   scheme/domain detection. Check if n number of domains can be mapped. Also, is
     *   Externalizer handling the security stuff?
     *
     * @return  rootPath    The scheme/hostname.
     */
    protected String rootPath() {
        //return request.getScheme() + "://" + request.getServerName();
        return "http://" + request.getServerName();
    }

    /**
     * Populates the class variable, pages, by calling the super class Sitemap's
     * getLinks() method and loading the links as Pages into a List.
     */
    protected void getPages() {
        LinkedList<Sitemap.Link> links = getLinks();
        Page page;
        Sitemap.Link link;
        String loc;
        for (int i = 0; i < links.size(); i++) {
            link = links.get(i);
            loc = link.getPath();
            page = resourceResolver.adaptTo(PageManager.class).getPage(loc);
            pages.add(page);
        }
    }

    public List<Map<String, String>> getMapValues() {
        return mapValues;
    }

    protected String getPageTitle(Page page) {
        String title = page.getProperties().get("title/jcr:title", String.class);
        if (title == null || title.equals("")) {
            title = page.getPageTitle();
        }
        if (title == null || title.equals("")) {
            title = page.getTitle();
        }
        if (title == null || title.equals("")) {
            title = page.getName();
        }
        return title;
    }

    /**
     * Since this class doesn't create anything on it's own, the generate
     * method must be implemented by subclasses.
     */
    abstract public void generate();

    abstract public List<String> getTableHeaders();

    // protected String writeXMLVersion() {
    //   return "<?xml version='1.0' encoding='UTF-8'?>";
    // }

    // protected String writeSitemapIndexOpen() {
    //     return "<sitemapindex xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>";
    // }

    // protected String writeSitemapIndexClose() {
    //     return "</sitemapindex>";
    // }

    // protected String writeURLSetOpen() {
    //     return "<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>";
    // }

    // protected String writeURLSetClose() {
    //     return "</urlset>";
    // }

    /**
     * @TODO Replace hardcoded placeholder.
     * This should render the data in List sitemaps as XML.
     */
    // protected String writeIndexFiles() {
    //     return "<sitemap>" +
    //                 "<loc>http://www.fhcrc.org/sitemap.xml</loc>" +
    //                 "<lastmod>2014-05-01T18:23:17+00:00</lastmod>" +
    //            "</sitemap>" +
    //            "<sitemap>" +
    //                 "<loc>http://www.fhcrc.org/en/news/sitemap-news.xml</loc>" +
    //                 "<lastmod>2014-05-01T18:23:17+00:00</lastmod>" +
    //            "</sitemap>";
    // }

    /**
     * @TODO Replace hardcoded placeholder.
     * This should render the data in list sitemaps as HTML
     */
    // @TODO This is just a place holder.
    // protected String writeIndexFilesHTML() {
    //     return "<tr>" +
    //                 "<td><a href='http://www.fhcrc.org/sitemap.xml'>http://www.fhcrc.org/sitemap.xml</td>" +
    //                 "<td>2014-05-01T18:23:17+00:00</td>" +
    //             "</tr>" +
    //             "<tr>" +
    //                 "<td><a href='http://www.fhcrc.org/en/news/sitemap.xml'>http://www.fhcrc.org/en/news/sitemap.xml</a></td>" +
    //                 "<td>2014-05-01T18:23:17+00:00</td>" +
    //             "</tr>";
    // }

    // protected String writeURLOpen() {
    //     return "<url>";
    // }

    // protected String writeURLClose() {
    //     return "</url>";
    // }

    // protected String writeLoc(String loc) {
    //     URI uri = URI.create(rootPath() + loc + ".html");
    //     String fullPath = uri.toString();
    //     return "<loc>" + fullPath + "</loc>";
    // }

    // protected String writeLastModified(String lastModified) {
    //     return "<lastmod>" + StringEscapeUtils.escapeHtml(lastModified) + "</lastmod>";
    // }

    // protected String writeChangeFreq(String changeFreq) {
    //     return "<changefreq>" + StringEscapeUtils.escapeHtml(changeFreq) + "</changefreq>";
    // }

    // protected String writePriority(String priority) {
    //     return "<priority>" + StringEscapeUtils.escapeHtml(priority) + "</priority>";
    // }

    // protected String writeHTMLOpen() {
    //     return "<!DOCTYPE HTML><html>";
    // }

    // protected String writeHTMLClose() {
    //     return "</html>";
    // }

    // protected String writeHeadOpen() {
    //     return "<head>";
    // }

    // protected String writeHeadClose() {
    //     return "</head>";
    // }

    // protected String writeCSS() {
    //     return "<style type='text/css'>" +
    //                 "body {" +
    //                     "font-family: 'Trebuchet MS', Helvetica, sans-serif;" +
    //                     "font-size: 75%;" +
    //                 "}" +
    //             "</style>";
    // }

    /**
     * Include CSS required for the "blue" theme.
     *
     * @return      CSS include hosted by free service.
     */
    // protected String writeTableSorterCSS() {
    //     return "<link rel='stylesheet' type='text/css'  href='//cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.17.0/css/theme.blue.css' />";
    // }

    /**
     * Include the jQuery TableSorter lib.
     *
     * @return      JS include hosted by free service.
     */
    // @TODO Maybe put these two JS libraries into CQ clientLibraryFolders
    // protected String writeTableSorterJS() {
    //     return "<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.17.0/jquery.tablesorter.min.js'></script>";
    // }

    // protected String writeJqueryJS() {
    //     return "<script src='//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>";
    // }

    /**
     * Initialize the tablesorter lib and add a class to activate the theme.
     *
     * @return      The TableSorter Javascript init script.
     */
    // protected String writeInitTableSorterJS() {
    //     return "<script type='text/javascript'>" +
    //                 "(function($) {" +
    //                     "$(document).ready(function() {" +
    //                         "var $table = $('table');" +
    //                         "$table.addClass('tablesorter-blue');" +
    //                         "if ($().tablesorter) {" +
    //                             "$table.tablesorter();" +
    //                         "}" +
    //                     "});" +
    //                 "})(jQuery);" +
    //             "</script>";
    // }

    /**
     * Create the header HTML.
     *
     * @return      The Javascript needed to create the header HTML (sitemap file path/name and number of URLs contained).
     */
    // protected String writeHeaderJS() {
    //     // Stuff TableSorter is doing causes this to be run twice (at least).
    //     // So if this exists (it has been run before), don't do it again.
    //     return "<script type='text/javascript'>" +
    //                 "(function($) {" +
    //                     "$(window).load(function() {" +
    //                         "if ($('h1').length) { return; };" +
    //                         "var $table = $('table'), count = $table.find('tr:visible').length - 1; file = window.location.href.replace('html', 'xml');" +
    //                         "$table.before('<h1>Sitemap file: ' + file + '</h1><p><strong>Number of URLs in this sitemap: ' + count + '</strong></p>');" +
    //                     "});" +
    //                 "})(jQuery);" +
    //             "</script>";
    // }

    // protected String writeBodyOpen() {
    //     return "<body>";
    // }

    // protected String writeBodyClose() {
    //     return "</body>";
    // }

    // protected String writeTableOpen() {
    //     return "<table>";
    // }

    // protected String writeTableClose() {
    //     return "</table>";
    // }

    /**
     * Creates the <thead><tr><th></th></tr></thead> HTML.
     *
     * @param   headers    List of column headers.
     *
     * @return  thead      Rendered column headers.
     */
    // protected String writeTableHead(List<String> headers) {
    //     String header;
    //     String thead = "<thead><tr>";
    //     for (int i = 0; i < headers.size(); i++) {
    //         header = headers.get(i);
    //         thead += "<th>" + header + "</th>";
    //     }
    //     thead += "</tr></thead>";

    //     return thead;
    // }

    // protected String writeTableBodyOpen() {
    //     return "<tbody>";
    // }

    // protected String writeTableBodyClose() {
    //     return "</tbody>";
    // }

    // protected String writeTableRow(List<String> values) {
    //     String value;
    //     String row = "<tr>";
    //     for (int i = 0; i < values.size(); i++) {
    //         value = values.get(i);
    //         row += "<td>" + value + "</td>";
    //     }
    //     row += "</tr>";

    //     return row;
    // }

    /**
     * @return  outputXML   The rendered sitemap XML.
     */
    // public String getXML() {
    //     return outputXML;
    // }

    /**
     * @return  outputHTML  The rendered sitemap HTML.
     */
    // public String getHTML() {
    //     return outputHTML;
    // }

    /*
     * This method should be able to identify the sitemaps pages and last
     * modified date and store them in a map object for rendering.
     */
    //protected void getXMLSitemaps() {}
}

/**
 * Creates the global (sitewide) sitemap XML/HTML.
 */
// class GlobalSitemap extends XMLSitemap {
//     public GlobalSitemap(Page rootPage, ResourceResolver resourceResolver, ServletRequest request) {
//         super(rootPage, resourceResolver, request);
//         generate();
//     }

//     /**
//      * @TODO Store the change frequency and priority as page properties.
//      */
//     public void generate() {
//         Page page;
//         // @TODO Make these properties of the page.
//         String changeFreq = "daily";
//         String priority = "0.5";
//         String lastModified;
//         String path;
//         for (int i = 0; i < pages.size(); i++) {
//             Map<String, String> data = new HashMap<String, String>();
//             page = pages.get(i);
//             lastModified = page.getLastModified() == null ? "" : formatLastModified(page.getLastModified());
//             path = page.getPath();
//             data.put("loc", resourceResolver.map(path));
//             data.put("lastModified", lastModified);
//             data.put("changeFreq", changeFreq);
//             data.put("priority", priority);
//             mapValues.add(data);
//         }

//         draw();
//     }

//     public List<String> getTableHeaders() {
//         List<String> headers = new ArrayList<String>();
//         headers.add("URL");
//         headers.add("Name");
//         headers.add("Language");
//         headers.add("Genres");
//         headers.add("Publication Date");
//         headers.add("Title");
//         headers.add("Keywords");
//         return headers;
//     }

//     /**
//      * Render the data as XML and HTML.
//      */
//     public void draw() {
//         Map<String, String> values;
//         String loc;
//         String lastModified;
//         String changeFreq;
//         String priority;
//         StringBuilder outputXML = new StringBuilder();
//         StringBuilder outputHTML = new StringBuilder();
//         List<String> headers = new ArrayList<String>();
//         List<String> row = new ArrayList<String>();
//         headers.add("URL");
//         headers.add("Last Modification Date");
//         headers.add("Change Frequency");
//         headers.add("Priority");

//         outputHTML.append(writeXMLVersion());

//         outputHTML.append(writeHTMLOpen());
//         outputHTML.append(writeHeadOpen());
//         outputHTML.append(writeCSS());
//         outputHTML.append(writeTableSorterCSS());
//         outputHTML.append(writeJqueryJS());
//         outputHTML.append(writeTableSorterJS());
//         outputHTML.append(writeInitTableSorterJS());
//         outputHTML.append(writeHeaderJS());
//         outputHTML.append(writeHeadClose());
//         outputHTML.append(writeBodyOpen());
//         outputHTML.append(writeTableOpen());
//         outputHTML.append(writeTableHead(headers));
//         outputHTML.append(writeTableBodyOpen());

//         for (int i = 0; i < mapValues.size(); i++) {
//             values = mapValues.get(i);
//             loc = values.get("loc");
//             lastModified = values.get("lastModified");
//             changeFreq = values.get("changeFreq");
//             priority = values.get("priority");
//             outputXML.append(writeURLOpen());
//             outputXML.append(writeLoc(loc));
//             outputXML.append(writeLastModified(lastModified));
//             outputXML.append(writeChangeFreq(changeFreq));
//             outputXML.append(writePriority(priority));
//             outputXML.append(writeURLClose());

//             row.add(writeLink(loc));
//             row.add(lastModified);
//             row.add(changeFreq);
//             row.add(priority);
//             outputXML.append(writeTableRow(row));
//             row = new ArrayList<String>();
//         }
//         outputXML.append(writeURLSetClose());
//         this.outputXML = outputXML.toString();

//         outputHTML.append(writeTableBodyClose());
//         outputHTML.append(writeTableClose());
//         outputHTML.append(writeBodyClose());
//         outputHTML.append(writeHTMLClose());
//         this.outputHTML = outputHTML.toString();
//     }
// }

/**
 * Create the News content XML sitemap.
 */
class NewsSitemap extends XMLSitemap {
    public NewsSitemap(Page rootPage, ResourceResolver resourceResolver, ServletRequest request) {
        super(rootPage, resourceResolver, request);
        generate();
    }

    // protected String writeNewsURLSetOpen() {
    //   return "<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9' " +
    //              "xmlns:news='http://www.google.com/schemas/sitemap-news/0.9'>";
    // }

    // protected String writeNewsURLSetClose() {
    //     return "</urlset>";
    // }

    // protected String writeNewsOpen() {
    //     return "<news:news>";
    // }

    // protected String writeNewsClose() {
    //     return "</news:news>";
    // }

    // protected String writeNewsPubOpen() {
    //     return "<news:publication>";
    // }

    // protected String writeNewsPubClose() {
    //     return "</news:publication>";
    // }

    // protected String writeNewsName(String name) {
    //     return "<news:name>" + StringEscapeUtils.escapeXml(name) + "</news:name>";
    // }

    // protected String writeNewsLanguage(String language) {
    //     return "<news:language>" + language + "</news:language>";
    // }

    // protected String writeNewsAccess(String access) {
    //     return "<news:access>" + access + "</news:access>";
    // }

    // // Comma-seprated values. Should I accept a string array or expected it already concatenated?
    // protected String writeNewsGenres(String genres) {
    //     return "<news:genres>" + StringEscapeUtils.escapeXml(genres) + "</news:genres>";
    // }

    // // YYYY-MM-DD
    // protected String writeNewsPubDate(String date) {
    //     return "<news:publication_date>" + date + "</news:publication_date>";
    // }

    // protected String writeNewsTitle(String title) {
    //     return "<news:title>" + StringEscapeUtils.escapeXml(title) + "</news:title>";
    // }

    // protected String writeNewsKeywords(String keywords) {
    //     return "<news:keywords>" + StringEscapeUtils.escapeXml(keywords) + "</news:keywords>";
    // }

    /**
     * Format the last published date according to http://www.w3.org/TR/NOTE-datetime
     *
     * @param   pubDate     The publication date as a Date.
     *
     * @return  pubDate     The publication date as an ISO8601 formatted date without time.
     */
    protected String formatPubDate(Date pubDate) {
    	return DateUtil.getISO8601DateNoTime(pubDate);
    }

    /**
     * Convert array of Tag objects to comma delimited string.
     *
     * @param   tagArray    Array of Tag objects to turn into CSV.
     *
     * @return  tagsCSV     Tag titles as CSV string.
     */
    protected String tagsToCSV(Tag[] tagArray) {
        String tagsCSV = "";
        Tag t;
        for (int i = 0; i < tagArray.length; i++) {
            t = tagArray[i];
            tagsCSV += t.getTitle();
            if (i != tagArray.length - 1) {
                tagsCSV += ", ";
            }
        }

        return tagsCSV;
    }

    /**
     * Set the genres for pages.
     *
     * @param   path    URL.
     *
     * @return  genres  Content categories.
     */
    protected String newsGenres(String path) {
        String newsRelease = XMLSitemapConstants.NEWS_GENRE_PATH;
        String genres = "";
        if (path.contains(newsRelease)) {
            genres = "PressRelease";
        }

        return genres;
    }

    /**
     * Populate the class variable mapValues with the sitemap data and render it.
     */
    public void generate() {
        Page page;
        String path;
        String title;
        String tags;
        for (int i = 0; i < pages.size(); i++) {
            Map<String, String> data = new HashMap<String, String>();
            page = pages.get(i);
            if (!isIncludeWorthy(page)) {
                continue;
            }
            title = getPageTitle(page);
            path = page.getPath();
            Tag[] tagArray = page.getTags();
            tags = "";
            if (tagArray != null && tagArray.length > 0) {
                tags = tagsToCSV(tagArray);
            }
            data.put("keywords", tags);
            // @TODO Links don't work outside of http://www.fhcrc.org.
            data.put("loc", resourceResolver.map(path));
            data.put("title", title);
            // @TODO Get from system var?
            data.put("name", XMLSitemapConstants.NEWS_NAME);
            data.put("pubDate", formatPubDate(page.getProperties().get("pubDate/date", Date.class)));
            data.put("genres", newsGenres(path));
            data.put("language", "en");
            mapValues.add(data);
      }

        //draw();
    }

    /**
     * Determine if the page should be included in the sitemap according to
     * https://support.google.com/news/publisher/answer/74288?hl=en
     *
     * This method allows pages to be included that:
     *   - Have a publish date.
     *   - Have been published within two days of the current page load. While time is
     *      automatically entered when the content is created, it is not factor in this
     *      calculation, since the time field is not being considered by authors. 
     *	 - Have a time value that is not in the future. While time is not factored into the
     *      two day requirement. I have accounted for consideration of the time field for future values. 
     *      If an author deliberately sets the time field to a time in the future, the page will not
     *      be included until the current time is >= the publish time. 
     *
     * @param   page        The Page object.
     *
     * @return  boolean     Whether or not to include this page in the sitemap.
     */
    protected boolean isIncludeWorthy(Page page) {
        Date pubDate = page.getProperties().get("pubDate/date", Date.class);
        Calendar nowCal = new DateUtil().getNow();
        Calendar purgeDay = new DateUtil().getNow();
        // Set the date for determining if a publication date is outside the required range.
        purgeDay.add(Calendar.DAY_OF_MONTH, XMLSitemapConstants.NEWS_DAYS_TILL_PURGE);

        // Pub date is required. Disqualify pages without a pub date.
        if (pubDate == null) {
            return false;
        }
        Calendar pubCal = new GregorianCalendar();
        pubCal.setTime(pubDate);

		// Disqualify pages that will be published in the future.
        if (pubCal.after(nowCal)) {
			return false;
        }

        // Set the publication date's time values to the current time. This way the publication time is not a factor.
        pubCal.set(Calendar.HOUR_OF_DAY, nowCal.get(Calendar.HOUR_OF_DAY));
        pubCal.set(Calendar.MINUTE, nowCal.get(Calendar.MINUTE));
        pubCal.set(Calendar.SECOND, nowCal.get(Calendar.SECOND));

		// Disqualify pages that are older than 2 days.
        if (pubCal.before(purgeDay)) {
            return false;
        }

        return true;
    }

    public List<String> getTableHeaders() {
        List<String> headers = new ArrayList<String>();
        headers.add("URL");
        headers.add("Name");
        headers.add("Language");
        headers.add("Genres");
        headers.add("Publication Date");
        headers.add("Title");
        headers.add("Keywords");

        return headers;
    }

    /**
     * Render the data as XML and HTML.
     */
//     public void draw() {
//         StringBuilder outputXML = new StringBuilder();
//         StringBuilder outputHTML = new StringBuilder();
//         String loc;
//         String name;
//         String language;
//         String genres;
//         String pubDate;
//         String title;
//         String keywords;
//         List<String> headers = new ArrayList<String>();
//         List<String> row = new ArrayList<String>();
//         headers.add("URL");
//         headers.add("Name");
//         headers.add("Language");
//         headers.add("Genres");
//         headers.add("Publication Date");
//         headers.add("Title");
//         headers.add("Keywords");

//         outputXML.append(writeXMLVersion());
//         outputXML.append(writeNewsURLSetOpen());

//         outputHTML.append(writeHTMLOpen());
//         outputHTML.append(writeHeadOpen());
//         outputHTML.append(writeCSS());
//         outputHTML.append(writeTableSorterCSS());

//         outputHTML.append(writeHeadClose());
//         outputHTML.append(writeBodyOpen());
//         outputHTML.append(writeTableOpen());
//         outputHTML.append(writeTableHead(headers));
//         outputHTML.append(writeTableBodyOpen());
//         for (int i = 0; i < mapValues.size(); i++) {
//             Map<String, String> values = mapValues.get(i);
//             loc = values.get("loc");
//             name = values.get("name");
//             language = values.get("language");
//             genres = values.get("genres");
//             pubDate = values.get("pubDate");
//             title = values.get("title");
//             keywords = values.get("keywords");

//             outputXML.append(writeURLOpen());
//             outputXML.append(writeLoc(loc));
//             outputXML.append(writeNewsOpen());
//             outputXML.append(writeNewsPubOpen());
//             outputXML.append(writeNewsName(name));
//             outputXML.append(writeNewsLanguage(language));
//             outputXML.append(writeNewsPubClose());
//             outputXML.append(writeNewsGenres(genres));
//             outputXML.append(writeNewsPubDate(pubDate));
//             outputXML.append(writeNewsTitle(title));
//             outputXML.append(writeNewsKeywords(keywords));
//             outputXML.append(writeNewsClose());
//             outputXML.append(writeURLClose());

//             row = new ArrayList<String>();
//             row.add(writeLink(loc));
//             row.add(name);
//             row.add(language);
//             row.add(genres);
//             row.add(pubDate);
//             row.add(title);
//             row.add(keywords);
//             outputHTML.append(writeTableRow(row));
//         }
//         outputXML.append(writeNewsURLSetClose());

//         outputHTML.append(writeTableBodyClose());
//         outputHTML.append(writeTableClose());
//         outputHTML.append(writeJqueryJS());
//         outputHTML.append(writeTableSorterJS());
//         outputHTML.append(writeInitTableSorterJS());
//         outputHTML.append(writeHeaderJS());
//         outputHTML.append(writeBodyClose());
//         outputHTML.append(writeHTMLClose());

//         this.outputXML = outputXML.toString();
//         this.outputHTML = outputHTML.toString();
//     }
}

/**
 * Create the Sitemap index.
 */
// class SitemapIndex extends XMLSitemap {
//     public SitemapIndex(Page rootPage, ResourceResolver resourceResolver, ServletRequest request) {
//         super(rootPage, resourceResolver, request);
//         generate();
//     }

//     @Override public void generate() {
//         draw();
//     }

//     public List<String> getTableHeaders() {
//         List<String> headers = new ArrayList<String>();
//         headers.add("URL");
//         headers.add("Name");
//         headers.add("Language");
//         headers.add("Genres");
//         headers.add("Publication Date");
//         headers.add("Title");
//         headers.add("Keywords");
//         return headers;
//     }

//     /**
//      * The original writeHeaderJS() doesn't make sense in the context of the
//      * index file. Change some of the wording.
//      *
//      * @return  String  The Javascript that creates the page header.
//      */
//     @Override protected String writeHeaderJS() {
//         return "<script type='text/javascript'>" +
//                     "(function($) {" +
//                         "$(window).load(function() {" +
//                             "if ($('h1').length) { return; }" +
//                             "var $table = $('table'), count = $table.find('tr:visible').length - 1, file = window.location.href.replace('html', 'xml');" +
//                             "$table.before('<h1>Sitemap Index file: ' + file + '</h1><p><strong>Number of sitemaps in this index: ' + count + '</strong></p>');" +
//                         "});" +
//                     "})(jQuery);" +
//                 "</script>";
//     }

//     public void draw() {
//         StringBuilder outputXML = new StringBuilder();
//         StringBuilder outputHTML = new StringBuilder();
//         List<String> headers = new ArrayList<String>();

//         headers.add("URL");
//         headers.add("Last Modification Date");

//         outputXML.append(writeXMLVersion());
//         outputXML.append(writeIndexFiles());
//         this.outputXML = outputXML.toString();

//         outputHTML.append(writeHTMLOpen());
//         outputHTML.append(writeHeadOpen());
//         outputHTML.append(writeCSS());
//         outputHTML.append(writeTableSorterCSS());
//         outputHTML.append(writeHeadClose());
//         outputHTML.append(writeBodyOpen());
//         outputHTML.append(writeTableOpen());
//         outputHTML.append(writeTableHead(headers));
//         outputHTML.append(writeTableBodyOpen());
//         outputHTML.append(writeIndexFilesHTML());
//         outputHTML.append(writeTableBodyClose());
//         outputHTML.append(writeTableClose());
//         outputHTML.append(writeJqueryJS());
//         outputHTML.append(writeTableSorterJS());
//         outputHTML.append(writeInitTableSorterJS());
//         outputHTML.append(writeHeaderJS());
//         outputHTML.append(writeBodyClose());
//         outputHTML.append(writeHTMLClose());
//         this.outputHTML = outputHTML.toString();
//     }
// }

/*
public enum XMLSitemapType {
    NEWS, GLOBAL, INDEX
}

public enum XMLSitemapFormatOutput {
    XML, HTML
}
*/

class XMLSitemapFactory {
    public XMLSitemap buildXMLSitemap(Page rootPage, ResourceResolver resourceResolver, ServletRequest request, String type) {
        XMLSitemap xmlSitemap = null;
        if (type.equals("news")) {
            xmlSitemap = new NewsSitemap(rootPage, resourceResolver, request);
        }
        else if (type.equals("global")) {
            //xmlSitemap = new GlobalSitemap(rootPage, resourceResolver, request);
        }
        else if (type.equals("index")) {
            //xmlSitemap = new SitemapIndex(rootPage, resourceResolver, request);
        }
        else {
            throw new IllegalArgumentException("The XMLSitemap type '" + type + "' has not been defined.");
        }
        return xmlSitemap;
    }
    /*
    public XMLSitemap getXMLSitemap(Page rootPage, ResourceResolver resourceResolver, ServletRequest request, XMLSitemapType type) {
        XMLSitemap xmlSitemap = null;
        switch (type) {
            case NEWS:
                xmlSitemap = new News(rootPage, resourceResolver, request);
                break;
            case GLOBAL:
                xmlSitemap = new Global(rootPage, resourceResolver, request);
                break;
            case INDEX:
                xmlsitemap = new Index(rootPage, resourceResolver, request);
                break;
            default:
            break;
        }
        return xmlSitemap;
    }
    */
}
%>