var componentApp = (function(componentApp) {
	
	/*---------------- SLIDER COMPONENT ----------------*/
	/* Publicly accessible methods:
	 * 
	 * None
	 */
	
	componentApp.sliderComponent = (function(sliderComponent) {
		
		/* Some helper functions */
		
		var createStructure = function(el) {
			
			var numSlides = el.querySelectorAll(".sliderItem").length;
			var theStructure = document.createElement("div");
			var theContainer = document.createElement("div");
			var theHTML = "";
			
			theStructure.classList.add("locationIndicator");
			theContainer.classList.add("indexContainer");
			
			/* Since the indexIndicator divs are inline-block, I am using innerHTML to make sure
			 * there are no spaces in between the divs, as that would create extra space */
			
			for (var i = 0; i < numSlides; i++) {
				
				theHTML += "<div class=\"indexIndicator\"></div>";
				
			}
			
			theContainer.innerHTML = theHTML;
			theStructure.appendChild(theContainer);
			
			return theStructure;
			
		}

		var appendStructure = function(el, struct) {
			
			var slides = el.querySelectorAll(".sliderItem");
			var duplicateNode = struct.cloneNode(true);
			
			for (var i = 0; i < slides.length; i++) {
				
				var duplicateNode = struct.cloneNode(true);
				
				var theSlide = slides[i];
				var theText = slides[i].querySelector(".textContainer");
				theSlide.insertBefore(duplicateNode, theText);
			}
			
		}

		var addActiveClasses = function(el) {
			
			var slides = el.querySelectorAll(".sliderItem");
			
			for (var i = 0; i < slides.length; i++) {
				
				var indicators = slides[i].querySelectorAll(".indexIndicator");
				indicators[i].classList.add("active");
				
			}
			
		}
		
		var addIndicators = function(el) {
			
			var struct = createStructure(el);
			appendStructure(el, struct);
			addActiveClasses(el);
			
		}
		
		/* Check to see if the Swipe object exists and if so, extend it with
		 * a few functions. */
		
		if(Swipe) {
			
			Swipe.prototype.toggleText = function() {
				
				var el = componentApp.utilityFunctions.lookUpForClassName(this, "slideContainer");
				
				var theTexts = el.querySelectorAll(".textContainer .theText"),
					theButton;
					
				for (var i = 0; i < theTexts.length; i++) {
					
					theTexts[i].classList.toggle("open");
					theButton = theTexts[i].parentNode.querySelector(".expandButton");
					if (theButton.innerHTML === "+") {
						
						theButton.innerHTML = "&ndash;";
						
					} else {
						
						theButton.innerHTML = "+";
						
					}
					
				}
				
			};

			Swipe.prototype.setupMobileSlider = function() {
				
				var expanderButtons = this.element.querySelectorAll(".expandButton");

				for (var i = 0; i < expanderButtons.length; i++) {
					
					expanderButtons[i].addEventListener("click", this.toggleText, false);
					
				}
				
				addIndicators(this.element);
				
			};
			
		}
		
		return sliderComponent;
	
	})(componentApp.sliderComponent || {});

	
	return componentApp;
	
})(componentApp || {});