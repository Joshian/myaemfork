<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text, 
                 java.util.*, 
                 org.apache.commons.lang.StringEscapeUtils, 
                 com.day.cq.tagging.Tag,
                 com.day.cq.tagging.TagManager,
                 com.day.cq.commons.RangeIterator,
                 org.fhcrc.publicsite.constants.Constants,
                 org.fhcrc.tools.FHUtilityFunctions" %>

<h2 class="underline">Related Content</h2>

<%
TagManager tagManager = resourceResolver.adaptTo(TagManager.class);

/* Check to see if the user has used the override tags feature */
String[] overrideTags = properties.get("overrideTags",String[].class);
Tag[] allTags;
/* If not, get all the tags that are currently on this page */
if(overrideTags == null || overrideTags.length == 0) {
    allTags = tagManager.getTags(resourceResolver.getResource(currentPage.getPath() + "/jcr:content"));
/* If they have, use all the tags in that array as the tags to search on */
} else {
    allTags = new Tag[overrideTags.length];
    for(int i = 0; i < allTags.length; i++) {
        allTags[i] = tagManager.resolve(overrideTags[i]);
    }
}

/* This is the maximum number of articles to display at the end, default is 3 */
int maxNumArticles = properties.get("maxNum",3);
/* If there are any manual overrides, get them */
String[] mustDisplay = properties.get("include",String[].class);
String[] dontDisplay = properties.get("exclude",String[].class);
/* If there are no tags on this page and also no manual overrides, then this component is meaningless. Alert the user */
if(allTags.length == 0 && (mustDisplay == null || mustDisplay.length == 0)){
%>
<cq:include script="empty.jsp"/>
<%  
} else {

/* Since all I really need are the Tag IDs, create an array of string to hold those */
String[] tagIDs = new String[allTags.length];
for(int i = 0; i < allTags.length; i++) {
    tagIDs[i] = allTags[i].getTagID();
}
/* A TreeSet to allow me to order the returned news articles by date */
TreeSet<Page> articleTree = new TreeSet<Page>(new Comparator<Page>(){
    public int compare(Page a, Page b){
        Date dateA = a.getProperties().get("pubDate/date", Date.class);
        Date dateB = b.getProperties().get("pubDate/date", Date.class);
        if(dateA==null){
            dateA = new Date();
        }
        if(dateB==null){
            dateB = new Date();
        }
        if(dateA.equals(dateB)){
            return b.getPath().compareTo(a.getPath());
        }
        return dateA.compareTo(dateB);
    }
});

%>
<ul class="flush nobullets">
<%
/* Add the mustDisplay articles to the list first if they exist */
if(mustDisplay != null){
    for(String displayPath : mustDisplay) {
       if(maxNumArticles <= 0) {
           break;
       } else {
            Page thePage = resourceResolver.getResource(displayPath).adaptTo(Page.class);
            Date pubDate = thePage.getProperties().get("pubDate/date", thePage.getProperties().get("cq:lastModified", Date.class));
%>
    <li><a href="<%= thePage.getPath() %>.html"><%= thePage.getTitle() != null ? thePage.getTitle() : thePage.getName() %></a><br />
    <span class="news-date"><%= FHUtilityFunctions.APStylize(pubDate) %></span></li>
<%      
            maxNumArticles--;
       }
    }
}
/* Now, find all the news pages that contain at least one of the tags that is on this page
   Of course, if all the slots are taken up by mustDisplay articles or there are no tags on the page, there's no need to do the search */
if(maxNumArticles > 0 && allTags != null && allTags.length > 0) {
    RangeIterator<Resource> theResources = tagManager.find(Constants.PATH_TO_NEWS_FOLDER, tagIDs, true);
    while(theResources.hasNext()) {
        Resource nextResource = theResources.next().getParent(); // Get parent, since the jcr:content node is the tagged node and I want the page
        if(nextResource.isResourceType("cq:Page")) { // If the resource is a Page...
            Page nextPage = resourceResolver.getResource(nextResource.getPath()).adaptTo(Page.class);
            if(nextPage.getProperties().get("cq:template","").equals("/apps/public/templates/news")) { // ...and has the news template...
                if(!nextPage.getPath().equals(currentPage.getPath())) { // ...and it isn't the page we are on currently...
                    articleTree.add(nextPage); // ...add it to our TreeSet
                }
            }
        }
    }
    Iterator<Page> thePages = articleTree.descendingIterator();
    while(thePages.hasNext() && maxNumArticles > 0){
        Page nextPage = thePages.next();
/* Check to see if a page I am about to display is one the user mandated I should not. If so, continue the loop */        
        if(dontDisplay != null && dontDisplay.length > 0) {
            Boolean skip = false;
            for(String path : dontDisplay) {
                if(path.equals(nextPage.getPath())){
                    skip = true;
                    break;
                }
            }
            if(skip == true) {
                continue;
            }
        }
        Date pubDate = nextPage.getProperties().get("pubDate/date", nextPage.getProperties().get("cq:lastModified", Date.class));
%>
<li><a href="<%= nextPage.getPath() %>.html"><%= nextPage.getTitle() != null ? nextPage.getTitle() : nextPage.getName() %></a><br />
<span class="news-date"><%= FHUtilityFunctions.APStylize(pubDate) %></span></li>
<%
        maxNumArticles--;
    }
}
%>
</ul>
<%
}
%>