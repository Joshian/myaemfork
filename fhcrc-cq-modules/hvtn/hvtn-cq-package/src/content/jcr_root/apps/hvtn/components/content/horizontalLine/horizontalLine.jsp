<%@include file="/libs/foundation/global.jsp"%>
<jsp:useBean id="horizontalLineBean" scope="request" class="org.fhcrc.hvtn.components.HorizontalLine" />
<jsp:setProperty name="horizontalLineBean" property="componentProperties" value="<%= properties %>"/>

<div class="${ horizontalLineBean.colorTheme } ${ horizontalLineBean.designTheme }">

	<hr>

</div>