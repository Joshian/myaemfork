package org.fhcrc.hvtn.components;

import javax.servlet.ServletRequest;

import java.io.IOException;
import java.io.Writer;

import com.day.cq.wcm.api.components.DropTarget;
import com.day.cq.commons.Doctype;
import com.day.cq.wcm.foundation.Image;
import com.day.cq.wcm.api.WCMMode;
import com.day.cq.wcm.api.designer.*;

import org.apache.sling.api.resource.*;
import org.fhcrc.tools.FHUtilityFunctions;

public class Bucket {
	
	private final String IMAGE_CONTAINER_CLASS = "bucketImageContainer";
	private final String BORDER_CLASS = "frame";
	private final String HIDE_AT_MOBILE_CLASS = "hideAtMobile";
	private final String VISUALLY_HIDDEN_CLASS = "visuallyHidden";
	
	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	private Resource resource;
	private Style currentStyle;
	private Design currentDesign;
	private Design resourceDesign;
	private ServletRequest request;
	private Writer writer;
	
	private Image image;
	
	private String linkTarget;
	private String imageContainerClass;
	
	private boolean externalLink;
	private boolean bordered;
	private boolean hideAtMobile;
	
	public Bucket() {
		
	}
	
	public ValueMap getComponentProperties() {
		return componentProperties;
	}
	
	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
		this.externalLink = properties.get("externalLink",false);
		this.bordered = properties.get("hasBorder",false);
		this.hideAtMobile = properties.get("hideAtMobile",false);
		this.image = new Image(this.resource, "image");
		setImage(image);
		setLinkTarget(properties.get("linkTarget",""));
		setImageContainerClass(IMAGE_CONTAINER_CLASS);
	}
	
	public Image getImage() {
		return image;
	}

	@SuppressWarnings("deprecation")
	public void setImage(Image image) {
		this.image = image;
		this.image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
		this.image.loadStyleData(currentStyle);
	    this.image.setSelector(".img"); // use image script
	    this.image.setDoctype(Doctype.fromRequest(request));
	    // add design information if not default (i.e. for reference paras)
	    if (!currentDesign.equals(resourceDesign)) {
	        this.image.setSuffix(currentDesign.getId());
	    }
	    if (bordered) {
	    	this.image.addCssClass(BORDER_CLASS);
	    }		
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}
	
	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}
	
	public Resource getResource() {
		return resource;
	}
	
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	
	public Style getCurrentStyle() {
		return currentStyle;
	}
	
	public void setCurrentStyle(Style currentStyle) {
		this.currentStyle = currentStyle;
	}
	
	public Design getCurrentDesign() {
		return currentDesign;
	}
	
	public void setCurrentDesign(Design currentDesign) {
		this.currentDesign = currentDesign;
	}
	
	public Design getResourceDesign() {
		return resourceDesign;
	}
	
	public void setResourceDesign(Design resourceDesign) {
		this.resourceDesign = resourceDesign;
	}
	
	public ServletRequest getRequest() {
		return request;
	}
	
	public void setRequest(ServletRequest request) {
		this.request = request;
	}
	
	public Writer getWriter() {
		return writer;
	}
	
	public void setWriter(Writer writer) {
		this.writer = writer;
	}
	
	public String getLinkTarget() {
		return linkTarget;
	}
	
	public void setLinkTarget(String linkTarget) {
		if(!linkTarget.equals("")){
	        this.linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
	    } else {
	    	this.linkTarget = "";
	    }
	}
	
	public String getImageContainerClass() {
		return imageContainerClass;
	}
	
	public void setImageContainerClass(String imageContainerClass) {
		this.imageContainerClass = imageContainerClass;
		if (hideAtMobile) {
			this.imageContainerClass = this.imageContainerClass.concat(" ").concat(HIDE_AT_MOBILE_CLASS);
		}
		if (!image.hasContent() && !(WCMMode.fromRequest(request) == WCMMode.EDIT)) {
			this.imageContainerClass = this.imageContainerClass.concat(" ").concat(VISUALLY_HIDDEN_CLASS);
		}
	}
	
	public boolean isExternalLink() {
		return externalLink;
	}
	
	public void setExternalLink(boolean externalLink) {
		this.externalLink = externalLink;
	}
	
	public boolean isBordered() {
		return bordered;
	}
	
	public void setBordered(boolean bordered) {
		this.bordered = bordered;
	}
	
	public boolean isHideAtMobile() {
		return hideAtMobile;
	}
	
	public void setHideAtMobile(boolean hideAtMobile) {
		this.hideAtMobile = hideAtMobile;
	}
	
	public void drawImage() {
		try {
			image.draw(writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}