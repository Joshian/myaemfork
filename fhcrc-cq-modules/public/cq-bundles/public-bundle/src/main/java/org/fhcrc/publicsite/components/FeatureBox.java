package org.fhcrc.publicsite.components;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.fhcrc.tools.FHUtilityFunctions;
import java.io.UnsupportedEncodingException;

import com.day.cq.commons.Doctype;
import com.day.cq.wcm.api.components.DropTarget;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.foundation.Image;



public class FeatureBox {
	
	/* CONSTANTS */

	private final String BORDER_CLASS = "frame";
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	/* PRIVATE VARIABLES */

	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	private Resource resource;
	private Style currentStyle;
	private Design currentDesign;
	private Design resourceDesign;
	private ServletRequest request;
	private Writer writer;
	
	private Image image;
	
	private String linkTarget;
	private String seeMoreLinkTarget;
	private String seeMoreLinkText;
	private String colorTheme;
	private String designTheme;
	
	private boolean externalLink;
	private boolean bordered;
	private boolean link;
	private boolean seeMoreLink;
	
	/* GETTERS AND SETTERS */
	
	public ValueMap getComponentProperties() {
		return componentProperties;
	}
	public void setComponentProperties(ValueMap componentProperties) {
		this.componentProperties = componentProperties;
		this.externalLink = componentProperties.get("externalLink",false);
		this.bordered = componentProperties.get("bordered",false);
		this.image = new Image(this.resource, "image");
		setImage(image);
		setLinkTarget(componentProperties.get("linkTarget",""));
		setSeeMoreLinkText(componentProperties.get("seeMoreLinkText",""));
		setSeeMoreLinkTarget(componentProperties.get("seeMoreLinkTarget",""));
		setColorTheme(componentProperties.get("colortheme","0"));
		
	}
	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}
	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}
	public Resource getResource() {
		return resource;
	}
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	public Style getCurrentStyle() {
		return currentStyle;
	}
	public void setCurrentStyle(Style currentStyle) {
		this.currentStyle = currentStyle;
	}
	public Design getCurrentDesign() {
		return currentDesign;
	}
	public void setCurrentDesign(Design currentDesign) {
		this.currentDesign = currentDesign;
	}
	public Design getResourceDesign() {
		return resourceDesign;
	}
	public void setResourceDesign(Design resourceDesign) {
		this.resourceDesign = resourceDesign;
	}
	public ServletRequest getRequest() {
		return request;
	}
	public void setRequest(ServletRequest request) {
		this.request = request;
	}
	public Writer getWriter() {
		return writer;
	}
	public void setWriter(Writer writer) {
		this.writer = writer;
	}
	public Image getImage() {
		return image;
	}
	@SuppressWarnings("deprecation")
	public void setImage(Image image) {
		this.image = image;
		if (this.image.hasContent()) {
			try {
				this.image = FHUtilityFunctions.nukeURLPrefix(this.image, resourceResolver);
			} catch(UnsupportedEncodingException e) {
				log.error("ERROR: Unsupported Encoding Exception - ".concat(e.getMessage()));
			}
		}
		this.image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
		this.image.loadStyleData(currentStyle);
	    this.image.setSelector(".img"); // use image script
	    this.image.setDoctype(Doctype.fromRequest(request));
	    // add design information if not default (i.e. for reference paras)
	    if (!currentDesign.equals(resourceDesign)) {
	        this.image.setSuffix(currentDesign.getId());
	    }
	    if (bordered) {
	    	this.image.addCssClass(BORDER_CLASS);
	    }
	}
	public String getLinkTarget() {
		return linkTarget;
	}
	public void setLinkTarget(String linkTarget) {
		if(!linkTarget.equals("")){
	        this.linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
	        this.link = true;
	    } else {
	    	this.linkTarget = "";
	    	this.link = false;
	    }
	}

	public String getSeeMoreLinkTarget() {
		return seeMoreLinkTarget;
	}
	public void setSeeMoreLinkTarget(String seeMoreLinkTarget) {
		if(!seeMoreLinkTarget.equals("")){
	        this.seeMoreLinkTarget = FHUtilityFunctions.cleanLink(seeMoreLinkTarget, resourceResolver);
	    } else {
	    	this.seeMoreLinkTarget = "";
	    }
	}
	public String getSeeMoreLinkText() {
		return seeMoreLinkText;
	}
	public void setSeeMoreLinkText(String seeMoreLinkText) {
		this.seeMoreLinkText = seeMoreLinkText;
		if (seeMoreLinkText.trim().equals("")) {
			this.seeMoreLink = false;
		} else {
			this.seeMoreLink = true;
		}
	}
	public String getColorTheme() {
		return colorTheme;
	}
	public void setColorTheme(String colorTheme) {
		this.colorTheme = colorTheme;
	}
	public String getDesignTheme() {
		return designTheme;
	}
	public void setDesignTheme(String designTheme) {
		this.designTheme = designTheme;
	}
	public boolean isExternalLink() {
		return externalLink;
	}
	public void setExternalLink(boolean externalLink) {
		this.externalLink = externalLink;
	}
	public boolean isBordered() {
		return bordered;
	}
	public void setBordered(boolean bordered) {
		this.bordered = bordered;
	}
	public boolean isLink() {
		return link;
	}
	public void setLink(boolean link) {
		this.link = link;
	}
	public boolean isSeeMoreLink() {
		return seeMoreLink;
	}
	public void setSeeMoreLink(boolean seeMoreLink) {
		this.seeMoreLink = seeMoreLink;
	}
	
	/* OTHER PUBLIC METHODS */
	
	public void drawImage() {
		try {
			image.draw(writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}