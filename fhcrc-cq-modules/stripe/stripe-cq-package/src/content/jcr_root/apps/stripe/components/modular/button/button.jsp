<%@include file="/apps/stripe/global.jsp"%>
<%@page import="com.day.cq.dam.api.Asset,
                org.fhcrc.stripe.utilities.AssetUtil,
                org.apache.commons.lang.StringEscapeUtils,
                org.fhcrc.stripe.functions.FHUtilityFunctions"%>
<%
     String buttonText = properties.get("buttonText","Default");
     String buttonAlign = properties.get("align","left");
     String buttonTopColor = properties.get("button_top","ffffff");
     String buttonBottomColor = properties.get("button_bottom","e6e6e6");
     String buttonTextColor = properties.get("button_text_color", "333333");
     String buttonHoverTextColor = properties.get("button_hover_text_color", "333333");
//     boolean buttonExternalLink = properties.get("isExternalLink",false);
     String link = properties.get("link","");
     link = FHUtilityFunctions.cleanLink(link,resourceResolver);
     
//     request.setAttribute("buttonExternalLink",buttonExternalLink);
     request.setAttribute("link",link);
     
%>



<div style="text-align: <%=buttonAlign%>;">
  <form action="<%=link%>" style="display:inline-block;" method="get" target='_blank'>
    <input style="white-space:normal" type="submit" class="stripebtn" value="<%=StringEscapeUtils.escapeHtml(buttonText)%>"/>
  </form>
</div>


<%--
 <div style="text-align: <%=buttonAlign%>;">
    <p>
        <c:choose>
            <c:when test="${buttonExternalLink}">
               <a href="<%=link%>" target="_blank">
                <button class="stripebtn" type="button" onclick="<%=link%>" target="_blank"><%=buttonText%></button>
                </a>
            </c:when>
            <c:otherwise>
               <a href="<%=link%>.html">
                <button class="stripebtn" type="button" onclick="<%=link%>"><%=buttonText%></button>
                </a>
            </c:otherwise>
        </c:choose>
    </p>
</div>
--%>

<style type="text/css">

/** Button **/

.stripebtn {
  white-space:normal; /* new, sld jul/2013 */
  display: inline-block;
  padding: 4px 14px;
  margin-bottom: 0;
  font-size: 14px;
  line-height: 20px;
  color: #<%=buttonTextColor%>;
  text-align: center;
  /*text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);*/
  vertical-align: middle;
  cursor: pointer;
  /*
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#<buttonTopColor>), to(#<buttonBottomColor%>)) !important;
  background-image: -webkit-linear-gradient(top, #<buttonTopColor>, #<buttonBottomColor%>) !important;
  background-image: -o-linear-gradient(top, #<buttonTopColor%>, #<buttonBottomColor%>) !important;
  filter: progid:dximagetransform.microsoft.gradient(enabled=false);
  */
  background-image: -moz-linear-gradient(center top, #<%=buttonTopColor%>, #<%=buttonBottomColor%>) !important;
  filter: progid:dximagetransform.microsoft.gradient(startColorstr='#<%=buttonTopColor%>', endColorstr='#<%=buttonBottomColor%>', GradientType=0) !important;
  background-image: linear-gradient(to bottom, #<%=buttonTopColor%>, #<%=buttonBottomColor%>) !important;
  background-repeat: repeat-x;
  border: 1px solid #bbbbbb;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  -webkit-border-radius: 4px;
     -moz-border-radius: 4px;
          border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
     -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
          box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
}

.stripebtn:hover,
.stripebtn:active,
.stripebtn.active,
.stripebtn.disabled,
.stripebtn[disabled] {
  color: #333333;
  background-color: #<%=buttonBottomColor%>;
}

.stripebtn:active,
.stripebtn.active {
  background-color: #cccccc;
}

.stripebtn:hover {
  color: #<%=buttonHoverTextColor%>;
  text-decoration: none;
  background-color: #<%=buttonBottomColor%>;
  /* Buttons in IE7 don't get borders, so darken on hover */

  background-position: 0 -15px;
  -webkit-transition: background-position 0.1s linear;
     -moz-transition: background-position 0.1s linear;
       -o-transition: background-position 0.1s linear;
          transition: background-position 0.1s linear;
}

.stripebtn:focus {
  outline: thin dotted #333;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px;
}

.stripebtn.active,
.stripebtn:active {
  background-color: #<%=buttonBottomColor%>;
  background-image: none;
  outline: 0;
  -webkit-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
     -moz-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
          box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
}

.stripebtn.disabled,
.stripebtn[disabled] {
  cursor: default;
  background-color: #<%=buttonBottomColor%>;
  background-image: none;
  opacity: 0.65;
  filter: alpha(opacity=65);
  -webkit-box-shadow: none;
     -moz-box-shadow: none;
          box-shadow: none;
}

</style>