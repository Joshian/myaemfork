var componentApp = (function(componentApp) {
	
	componentApp.searchWidget = (function(searchWidget) {
		
		searchWidget.clearDefaultText = function(){
			defaultText = "Search by keyword..."; //Change this if the default value of the text input changes
		/* Case 1: clicking on a search button clears the default text (this does it from all search boxes on the page, 
		 * but since we have clicked and are leaving the page, it shouldn't matter. */	
			if($(this).hasClass("search_button")){
				$("input.search_term").each(function(){
					if(this.value == defaultText){
						this.value= "";
					}
				});
			}
		/* Case 2: text input box gaining focus clears the default text */	
			if(this.value == defaultText){
				this.value = "";
			}
		};
		
		$(function() {
			$("input.search_term").focus(searchWidget.clearDefaultText);
			$("input.search_button").click(searchWidget.clearDefaultText);
		});
		
		return searchWidget;
		
	})(componentApp.searchWidget || {});
	
	return componentApp;
	
})(componentApp || {});