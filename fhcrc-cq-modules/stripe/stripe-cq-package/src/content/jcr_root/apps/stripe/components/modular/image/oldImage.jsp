<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Image component

  Draws an image. 
    
    # Sridhar Bojja 1/16/2013
    # Extended the image component functionality for use in the stripe project

--%><%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,com.day.cq.commons.ImageResource" %>
    <%@include file="/apps/stripe/global.jsp" %>
    <%@include file="/libs/foundation/global.jsp"%>
    <%
    Image image = new Image(resource);
    //drop target css class = dd prefix + name of the drop target in the edit config
    image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
    image.loadStyleData(currentStyle);
    image.setSelector(".img"); // use image script
    image.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        image.setSuffix(currentDesign.getId());
    }
    
    boolean hasContent = image.hasContent();
    
       String divStyle="";
       String imgStyle="";
	   String imageWidth = image.get("width");
	   String imageHeight = image.get("height");
	   boolean userColumnWidth = Boolean.parseBoolean(image.get("useColumnWidth"));
	   boolean uselightBox = Boolean.parseBoolean(image.get("lightBox"));
       String imageLargeWidth = image.get("largewidth");
       String imageLargeHeight = image.get("largeheight");
       String styleClass = "cq-dd-image";
	   String imageAlign = image.get("alignImage");
	   String shadowAffect = image.get("shadowAffect");
	   
	   if(shadowAffect.equals(""))
	   {
		   shadowAffect = "outer";
	   }
	   
	   String altTxt = "";
	   String customTitle = "";
	   boolean isLinkTo = false;
	   
	   if(image.get("showShadow").equals("true") && image.get("shadowAffect").equals("outer"))
	   {
		   
		   divStyle = "box-shadow: 0 0 5px #888; display:block; width:" + imageWidth + "px; height:" + imageHeight +"px;";
	   }
	   else if(image.get("showShadow").equals("true") && image.get("shadowAffect").equals("inner"))
	   {
		   //style = "box-shadow: inset -5px -5px 5px 5px #888; -moz-box-shadow: inset -5px -5px 5px 5px #888; -webkit-box-shadow: inset -5px -5px 5px 5px#888; display:block;";
		   divStyle = "box-shadow: inset 0px 0px 10px rgba(0,0,0,0.9); display:block; width:" + imageWidth + "px; height:" + imageHeight +"px;";
		   imgStyle = "position: relative; z-index: -2;";
	   }
	   else
	   {
		   divStyle = "display:block;";
	   }
	   
	   if(image.get(ImageResource.PN_LINK_URL) != null && !image.get(ImageResource.PN_LINK_URL).equals(""))
	   {
		   styleClass = "cq-dd-image-link";
		   isLinkTo = true;
	   }
	   
	   if(image.get(ImageResource.PN_ALT) != null && !image.get(ImageResource.PN_ALT).equals(""))
	   {
		   altTxt = image.get(ImageResource.PN_ALT);
	   }
	   else
	   {
		   altTxt = image.get("jcr:title");
	   }
	   
	   customTitle = image.get("jcr:title");
	   request.setAttribute("customTitle", customTitle);
	   
	   String imageName = image.getFileName(); 
	  
	   String id = imageName.replaceAll("[^a-zA-Z0-9]+","");
	   
	   String link = image.get(ImageResource.PN_LINK_URL);
       boolean externalLink = Boolean.parseBoolean(image.get("externalLink"));

	   if(!externalLink)
	   {
		   link = link.concat(".html");
	   }
	%>
<div style="margin-top: <%=image.get("marginTop")%>px; margin-bottom: <%=image.get("marginBottom")%>px;">
        <div align="<%=imageAlign%>">
	        <% 
	             if(!customTitle.equals(""))
	             {
	        %>
	             <strong><%=customTitle%></strong><br/><br/>
	        <% 
	             }
	        %>
	        <div class="aldiv<%=id%><%= hasContent ? "" : " cq-edit-only" %>" style="<%=divStyle%>">
	        <% 
	        /* TESTING TO SEE IF I CAN GET THE PLACEHOLDER IN THERE */
	        	if(!hasContent) {
	        
	        %>
	        
	        	<img class="<%= styleClass.concat(" cq-image-placeholder") %>" src="/etc/designs/default/0.gif">	
	        	
	        <%
	        
	        	} else {
	        
	        /* END TESTING */
	             if(isLinkTo)
	             {
	        %>
	                <a href="<%=link%>" <%if(externalLink) { %>target="_blank" <%}%>>
	                    <img class="<%=styleClass%>" alt="<%=altTxt%>" title="<%=altTxt%>" src="<%=image.getFileReference()%>" 
	                       fileName="<%=image.getFileName()%>" width="<%=imageWidth%>px;" 
	                       height="<%=imageHeight%>px;" style="<%=imgStyle%>"
	                       uselightbox="<%=uselightBox%>" imageLargeWidth="<%=imageLargeWidth%>" imageLargeHeight="<%=imageLargeHeight%>"/><br/>
	                </a>
	        <%
	             }
	             else
	             {
	        %>
	                <img class="<%=styleClass%>" alt="<%=altTxt%>" title="<%=altTxt%>" src="<%=image.getFileReference()%>" 
	                       fileName="<%=image.getFileName()%>"  width="<%=imageWidth%>px;" 
	                       height="<%=imageHeight%>px;" style="<%=imgStyle%>"
	                       uselightbox="<%=uselightBox%>" imageLargeWidth="<%=imageLargeWidth%>" imageLargeHeight="<%=imageLargeHeight%>"/><br/>
	        <%
	             } }
	        %>
	        </div>
            <cq:text property="jcr:description" placeholder="" tagName="small"/>
        </div>
        
</div>

<script type='text/javascript'>

jQuery(window).load(function()
{
	var innerDiv = '.aldiv<%=id%> .cq-dd-image';
	$(innerDiv).each(function() 
    {    
		var $this = $(this);   
		var imageWidth = $this.width();
		var imageHeight = $this.height();
		if(imageWidth != 0)
		{
	        jQuery('.aldiv<%=id%>').css({'width': imageWidth}); 
		}
		if(imageHeight != 0)
		{
	        jQuery('.aldiv<%=id%>').css({'height': imageHeight}); 
		}
	}); 
 

jQuery('.body_content .cq-dd-image').click(function()
{
	var uselightbox = jQuery(this).attr('uselightbox');
    if(uselightbox == 'true')
    {
      jQuery(this).css('cursor','pointer');

      var imgname="";
      var imgpath="";
      var imageLargeWidth=0;
      var imageLargeHeight=0;

      imgname = jQuery(this).attr('fileName');
      imgpath = jQuery(this).attr('src');
      imageLargeWidth = jQuery(this).attr('imagelargewidth');
      imageLargeHeight = jQuery(this).attr('imagelargeheight');
      jQuery('#test_modal').find('#pictureLocation').attr('src',imgpath); 
      

      // WHEN THE IMAGE WIDTH IS GIVEN 
      if(imageLargeWidth != "")
      {
    	  //jQuery('#test_modal').find('#pictureLocation').attr('width',imageLargeWidth);
    	  //jQuery('#test_modal').find('#pictureLocation').attr('height',imageLargeHeight);
    	  
    	  if(imageLargeWidth < 1000)
          {
              jQuery('#test_modal').css('width', imageLargeWidth);         
              jQuery('#test_modal').find('#pictureLocation').css('width',imageLargeWidth);
          }
          else if(imageLargeWidth >= 1000)
          {
              jQuery('#test_modal').css('width', 1000);
              jQuery('#test_modal').find('#pictureLocation').css('width',1000);
          }
      }

      // WHEN THE IMAGE WIDTH IS NOT GIVEN 
      if(imageLargeWidth == "")
      {
        var img = new Image();
        img.src = imgpath;
        var imgWidth = img.width;
        var imgHeight = img.height;

        if(imgWidth < 560)
        {
            jQuery('#modal_body').css('width', imgWidth);
            jQuery('#test_modal').css('width', imgWidth+30);
            jQuery('#test_modal').css('overflow','hidden');
        }
        else if(imgWidth >= 1000)
        {
            jQuery('#test_modal').css('width', 1000);
            jQuery('#test_modal').css('width', 1000+30);
            jQuery('#test_modal').css('overflow-x','auto');
        }
        else if (imgWidth >= 560 && imgWidth < 1000 )
        {
            jQuery('#test_modal').css('width', imgWidth);
            jQuery('#test_modal').css('width', imgWidth+30);
            jQuery('#test_modal').css('overflow-x','hidden');
        }
      }

      // WHEN THE IMAGE HEIGHT IS GIVEN
      if(imageLargeHeight != "")
      {
          jQuery('#test_modal').find('#pictureLocation').css('height',imageLargeHeight);
          
	      if(imageLargeHeight > jQuery(window).height())
          {
              var ret = 0;
              ret=parseFloat(jQuery('#test_modal').css('height')); 
              ret = ret-100;
              jQuery('#test_modal').css('top', '5%');

              var windowHeight = jQuery(window).height()-70;
              windowHeight = windowHeight - 70;

              var windowMaxHeight = jQuery(window).height();
              windowMaxHeight = windowMaxHeight - 100;
              
              jQuery('#test_modal').css('height', windowHeight);
              jQuery('#modal_body').css('max-height',windowMaxHeight);
              jQuery('#modal_body').css('height',ret);
              jQuery('#modal_body').css('overflow-y','scroll');
          }
          else
          {
              var ret = 0;
              ret=parseFloat(jQuery('#test_modal').css('height')); 
              ret = ret-100;

              var windowMaxHeight = jQuery(window).height();
              windowMaxHeight = windowMaxHeight - 150;
              
              var windowHeight = jQuery(window).height()-70;
              windowHeight = windowHeight - 70;

              jQuery('#test_modal').css('top', '20%');

              if(windowMaxHeight >= 650)
              {
            	  jQuery('#test_modal').css('height', 650);
              }
              else
              {
            	  jQuery('#test_modal').css('height', windowMaxHeight);
              }
              
              jQuery('#modal_body').css('max-height',windowMaxHeight);
              jQuery('#modal_body').css('height',ret);
          }
      }

      // WHEN THE IMAGE HEIGHT IS NOT GIVEN
      if(imageLargeHeight == "")
      {
    	  var img = new Image();
          img.src = imgpath;
          var imgHeight = img.height;
          if(imgHeight > jQuery(window).height())
          {
              var ret = 0;
              ret=parseFloat(jQuery('#test_modal').css('height')); 
              ret = ret-100;

              var windowHeight = jQuery(window).height()-70
              windowHeight = windowHeight - 70;

              var windowMaxHeight = jQuery(window).height();
              windowMaxHeight = windowMaxHeight - 100;
              
              jQuery('#test_modal').css('height', windowHeight);
              jQuery('#modal_body').css('max-height',windowMaxHeight);
              jQuery('#modal_body').css('height',ret);                      
              jQuery('#modal_body').css('overflow-y','scroll');

              jQuery('#test_modal').css('top', '5%');

          }
          else
          {
        	  var ret = 0;
              ret=parseFloat(jQuery('#test_modal').css('height')); 
              ret = ret-100;

              var windowMaxHeight = jQuery(window).height();
              windowMaxHeight = windowMaxHeight - 150;
              jQuery('#test_modal').css('top', '20%');
              jQuery('#test_modal').css('height', imgHeight+75);
              jQuery('#modal_body').css('max-height',imgHeight);
              //jQuery('#modal_body').css('height',imgHeight);  
              jQuery('#test_modal').find('#pictureLocation').css('height',imgHeight);

              
              jQuery('#modal_body').css('overflow-y','hidden');
              jQuery('#test_modal').css('overflow-y','hidden');
          }
      }
      jQuery('#test_modal').modal('show');
    }
});
});

</script>
