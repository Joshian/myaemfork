<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.foundation.Image,com.day.cq.commons.Doctype" %>
<%

final String SEE_MORE_LINK_TEXT = "See more researchers";
final String SEE_MORE_LINK_LOCATION = "/content/public/en/diseases/featured-researchers.html";

// while this is an array, we only display the top item. this is for future-proofing this component.
String[] researchers = properties.get("researchers",String[].class);

// we only show the first item. this may change.
int max=1;
		
String colorthemes = properties.get("colortheme","0"); // default is theme 0
//out.write(colorthemes);
String[] c = colorthemes.split("\t");
String color_theme_number = c[0];
String colorTheme = "";
if(!color_theme_number.isEmpty()){
	colorTheme = "colorTheme" + color_theme_number ;
}

String designthemes = properties.get("designtheme","0"); // default is theme 0
//out.write(designthemes);
c = designthemes.split("\t");
String design_theme_number = c[0];
String designTheme = "";
if(!design_theme_number.isEmpty()){
designTheme = "designTheme" + design_theme_number ;
}

if(null==researchers){ %>
  <cq:include script="empty.jsp" />
<% } else { %>

<div class="featuredresearchers <%= colorTheme %> clearfix<%= properties.get("omitImage","false").equals("true") ? " omitfeaturedimage" : "" %>">

  <% for(int i = 0; i < max; i++){
       Page thePage = resourceResolver.getResource(researchers[i]).adaptTo(Page.class);
       
       String name_display = thePage.getProperties().get("nameDisplay","").trim();
       String business_title = thePage.getProperties().get("businessRole","").trim();
       boolean omitImage = properties.get("omitImage",Boolean.FALSE);
       boolean omitMoreLink = properties.get("omitMoreLink",Boolean.FALSE);
       boolean omitBusinessTitle = properties.get("omitBusinessTitle",Boolean.FALSE);

	   Resource r = thePage.getContentResource("image");
	   if (r != null && !omitImage) {
	      String img;
		  Image image = new Image(r);
		  img = thePage.getPath() + ".img.png" + image.getSuffix(); // e.g.,: /content/public/en/events/events/hutch-award.img.png/1323194909406.gif
		  // prepend context path to img
	      img = request.getContextPath() + img;
		  img = resourceResolver.map(img);
		  out.print("<div class='mainimage'><a href='"+ thePage.getPath() +".html'>");
		  out.print("<img src='"+img+"' alt='" + name_display + "' title='" + name_display + "'>");
		  out.print(" </a></div>");
	   } %>
	   
	<div class="text_container">
          
        <h2 class="category">Featured Researcher</h2>
            
        <a href="<%= thePage.getPath() %>.html"><h3 class="name"><%= name_display != null ? name_display : ( thePage.getTitle() != null ? thePage.getTitle() : thePage.getName() ) %></h3></a>

	<%
    if (!omitBusinessTitle) {
    %>
    
        <div class="research_interest"><%= business_title %></div>

	<%
    }
    %> 

        <div class="description_text"><%= thePage.getDescription() != null ? thePage.getDescription() : "" %></div>
		
		<div class="link_container">
			<a href="<%= SEE_MORE_LINK_LOCATION %>"><%= SEE_MORE_LINK_TEXT %></a>
		</div>
		
	</div>

   <% } %>  
     
</div>

<% } %>