<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.util.Iterator,
        com.day.cq.wcm.api.PageFilter,
        org.fhcrc.tools.FHUtilityFunctions,
        org.fhcrc.publicsite.constants.Constants" %>

<%
Page homepage = currentPage.getAbsoluteParent(2);
PageFilter filter = new PageFilter();
Iterator<Page> navElements = homepage.listChildren(filter);
%>



<!--  Ian, the first ul below can build from content tree: -->

    <nav class="mobileNav" id="mobileNav">
    <!-- mirror the global nav elements: -->
        <ul>
<%

while (navElements.hasNext()) {
    
    Page child = navElements.next();
%>
          <li><a href="<%= child.getPath() %>.html"><%= FHUtilityFunctions.displayTitle(child) %></a></li>
<%
}
%>
        </ul>
    </nav>