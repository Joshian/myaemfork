<%--

  Tagboard component.



--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %><%
%><%
	String tagboardID = properties.get("tagboardID", "");
%>
<div>
  <div id="tagboard-embed"></div>
  <script> var tagboardOptions = { tagboard:"<%= tagboardID %>" }; </script>
  <script src="//tagboard.com/public/js/embed.js"></script>
</div>