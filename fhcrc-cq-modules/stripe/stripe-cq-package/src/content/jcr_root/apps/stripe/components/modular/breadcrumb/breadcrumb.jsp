<%@include file="/apps/stripe/global.jsp"%><%
%><%@ page import="java.util.Iterator,
        com.day.cq.wcm.api.PageFilter,
        com.day.cq.wcm.api.NameConstants,
        com.day.cq.wcm.api.Page,
        org.fhcrc.stripe.utilities.PageUtil" %><%

    // get starting point of trail
    long level = currentStyle.get("absParent", 2L);
    long endLevel = currentStyle.get("relParent", 1L);
    String delimStr = currentStyle.get("delim", "&nbsp;&gt;&nbsp;");
    String trailStr = currentStyle.get("trail", "");
    int currentLevel = currentPage.getDepth();
    String delim = "";
    Page trail;
    String title;
    while (level < currentLevel - endLevel) {
        trail = currentPage.getAbsoluteParent((int) level);
        if (trail == null) {
            break;
        }
        title = PageUtil.getPageTitle(trail,"");
        %><%= xssAPI.filterHTML(delim) %><%
        %><a href="<%= xssAPI.getValidHref(trail.getPath()+".html") %>"><%
        %><%= xssAPI.encodeForHTML(title) %><%
        %></a><%

        delim = delimStr;
        level++;
    }
    trail = currentPage;
    title = PageUtil.getPageTitle(trail,"");
    %><%= xssAPI.filterHTML(delim) %><%
    %><%= title %><%
    if (trailStr.length() > 0) {
        %><%= xssAPI.filterHTML(trailStr) %><%
    }

%>
