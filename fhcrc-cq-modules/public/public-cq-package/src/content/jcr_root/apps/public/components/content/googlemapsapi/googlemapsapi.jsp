<%@include file="/libs/foundation/global.jsp"%><%
 int hashCode = resource.hashCode();
 String mapWidth = properties.get("mapWidth","");
 String mapHeight = properties.get("mapHeight","400");
 String linkTarget = "";
 
 if(properties.get("directions","basic").equals("basic")){
     linkTarget = "http://g.co/maps/ty32r";
 } else {
     linkTarget = "https://maps.google.com/maps/ms?msid=212457292778404937524.0004aea5cd161df69022f&msa=0&ll=47.627136,-122.330967&spn=0.002983,0.003846";
 }

%>
<%-- use linkTarget as a global javascript variable that will be used in clientlibs.js --%>
<script type="text/javascript">
var FHGoogleMaps_linkTarget = "<%= linkTarget %>";
</script>
<%-- all the important script content is located in a co-located js file: --%>
<cq:includeClientLib categories="public.components.googlemapsapi" />

<%-- this is the element that holds the map  --%>
<div id="map_canvas<%=hashCode%>" class="googlemapsapielement"></div>

<%-- identify the target div, and initialize the google map: --%>
<script type="text/javascript">  
  <%-- set up the id of the map area (referenced in the clientlib (js)) --%>
  var googleMapID = 'map_canvas<%=hashCode %>'; 
  <%-- the location of the assets that go with this component, used in the clientlib (js), e.g., /apps/public/components/content/googlemapsapi/resources/ --%>
  var googleMapRes = "<%=component.getPath()%>/resources/"; //alert(googleMapRes);
  googleMapRes = "/etc/designs/public/clientlibs/js/components/googleMapsAPI/resources/";
  <%-- if they set up height/width params, honor those here --%>
  <% if(!"".equals(mapWidth)||!"".equals(mapHeight)) { %>
      $(document).ready(        
        function(){
          <% if(!"".equals(mapWidth)){ %>  
          $('#'+googleMapID).css('width','<%=mapWidth%>px');
          <% } %>
          <% if(!"".equals(mapHeight)){ %>  
          $('#'+googleMapID).css('height','<%=mapHeight%>px');
          <% } %>
        }
      )
  <%} %>
</script>