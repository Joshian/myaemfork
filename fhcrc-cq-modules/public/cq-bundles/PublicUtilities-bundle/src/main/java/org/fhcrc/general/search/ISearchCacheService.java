package org.fhcrc.general.search;

import javax.jcr.Session;
import com.day.cq.search.QueryBuilder;

/**
 * The Interface ISearchCacheService.
 */
public interface ISearchCacheService {

    public Long getPageCountbyPathAndTag(Session session, QueryBuilder builder, String path, String tagId) throws Exception;
}
