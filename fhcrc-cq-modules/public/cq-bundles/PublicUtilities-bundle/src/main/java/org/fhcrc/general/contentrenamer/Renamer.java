package org.fhcrc.general.contentrenamer;

import java.util.List;

public interface Renamer {

	public List<String> rename(String startPath, String findNodeName, String replacementName, boolean isTest);
}
