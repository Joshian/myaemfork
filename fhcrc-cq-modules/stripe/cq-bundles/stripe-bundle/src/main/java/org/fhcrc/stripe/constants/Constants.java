package org.fhcrc.stripe.constants;

public class Constants {
	
    /* URL path to the folder to pick up panorama images. omit trailing slash. used in public/components/page/foundation/panorama.jsp */
	public final static String PATH_TO_PANORAMA_FOLDER = "/content/dam/public/wrapper/panoramas";
	
    /* folders related to news. no trailing slash */
	public final static String PATH_TO_NEWS_FOLDER              = "/content/public/en/news";
    public final static String PATH_TO_NEWSRELEASES_FOLDER      = "/content/public/en/news/releases";
    public final static String PATH_TO_CENTERNEWS_FOLDER        = "/content/public/en/news/center-news";
    public final static String PATH_TO_HUTCH_MAGAZINE_FOLDER    = "/content/public/en/news/hutch-magazine";
    public final static String PATH_TO_HUTCH_MAG_IMPORTER_FOLDER     = "/content/public/en/news/hutch-magazine/imports";
    public final static String PATH_TO_PETRIDISHIMPORTER_FOLDER = "/content/public/en/news/petridish-import";
    public final static String PATH_TO_SCIENCESPOTLIGHT_FOLDER  = "/content/public/en/news/spotlight";
    public final static String PATH_TO_SCIENCESPOTLIGHTIMPORTER_FOLDER  = "/content/public/en/news/spotlight/imports";
    
    
    /* folders related to labs. no trailing slash */
    public final static String PATH_TO_LABSIMPORTER_FOLDER = "/content/public/en/labs/labs-import";
    
    /* 3rd-level domain part of the regex that allows certain URLs to be exposed via ProxyServlet. see PublicUtilities > org.fhcrc.general.proxy.ProxyServlet.java */
    public final static String PROXYSERVLET_REGEX = "(extranet|is-ext|quest)"; //.fhcrc.org
    
	
}
