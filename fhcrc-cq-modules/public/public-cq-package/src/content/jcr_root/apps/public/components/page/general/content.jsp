<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text, java.util.Iterator" %>
 <!-- page content -->
 <div id="fh_body" class="fh_body container_12 clearfix"><a name="content"></a>

  <div class="grid_12">

    <!-- breadcrumbs appear in the content flow in case they wrap to two lines -->
    <cq:include script="breadcrumbs.jsp"/>
    
    <cq:include script="clusterTitle.jsp" /> 

  </div>  
    
  <!-- content column -->
  <% if("true".equals(properties.get("hasRightColumn","false"))){ %>

	  <div id="fh_body_content" class="fh_body_content fh_rightcol_container clearfix grid_9 push_3">  
	    <div class="grid_5 fh_body_column alpha">
		    <% // checks whether this page is an event subpage, and if so, insert the subpage banner and themecolor %>
		    <cq:include script="eventsubpage.jsp"/>
		    <% //log.info(currentPage.getProperties().get("./isClusterHead","--not set--"));
		       // if this is a cluster head, then do not display a field for title, since the cluster title will take on that role
		       if(!properties.get("isClusterHead",Boolean.FALSE)){ %>
		       <cq:include path="title" resourceType="foundation/components/title"/>
		    <% } %>
		    <cq:include path="par" resourceType="foundation/components/parsys"/>
		    <!-- remote-wrap-api-marker -->
		</div>
		<div class="grid_4 fh_rightcol_column omega"><div class="fh_rightcol_content"><cq:include path="parcolright" resourceType="foundation/components/parsys"/></div></div>
	  </div>
	  <div style="padding-bottom:0px;">&nbsp;</div>

  <% } else { %>

	  <div id="fh_body_content" class="fh_body_content grid_9 push_3">  
	    <% // checks whether this page is an event subpage, and if so, insert the subpage banner and themecolor %>
	    <cq:include script="eventsubpage.jsp"/>
	    <% //log.info(currentPage.getProperties().get("./isClusterHead","--not set--"));
	       // if this is a cluster head, then do not display a field for title, since the cluster title will take on that role
	       if(!properties.get("isClusterHead",Boolean.FALSE)){ %>
	       <cq:include path="title" resourceType="foundation/components/title"/>
	    <% } %>
	    <cq:include path="par" resourceType="foundation/components/parsys"/>
	    <!-- remote-wrap-api-marker -->
	  </div>

  <% } %>
  <!-- /content column -->

    

  <!-- left column (sidebar) -->
  <div id="fh_sidebar" class="fh_sidebar grid_3 pull_9"><div id="fh_sidebar_content" class="fh_sidebar_content">
    
    <cq:include script="clusterNav_page.jsp" />    
    
    <!-- protected parsys: -->
    <div class="relatedcontentarea">
      <cq:include path="parRelated" resourceType="public/components/content/protectedparsys"/>
    </div>
    
    <cq:include script="related-content.jsp" />
  
   </div><!-- /fh_sidebar_content -->
  
  </div>
  <!-- /left column -->
 
 </div><!-- /fh_body -->
