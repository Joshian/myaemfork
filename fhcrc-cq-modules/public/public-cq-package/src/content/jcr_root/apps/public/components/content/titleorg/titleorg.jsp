<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.api.WCMMode,org.fhcrc.tools.FHUtilityFunctions" %><%

   String org = "";

   String title = properties.get("title", "");
   if (!title.isEmpty()) {
%>
    <dt><%= title %></dt>
    <dd>
    <%
       String linkType = properties.get("linkType", "");
       if (!linkType.isEmpty()) {
           if ("internal".equals(linkType)) {
               String internalPagePath = properties.get("internalPagePath", "");
               Page internalPage = pageManager.getPage(internalPagePath);
               if (internalPage != null) { 
               %>
                 <a href="<%= internalPage.getPath() + ".html" %>"><%= FHUtilityFunctions.displayTitle(internalPage) %></a>  
               <% }
           } else {
               if ("external".equals(linkType)) {
                   String linkTitle = properties.get("linkTitle", "");
                   String url = properties.get("url", "");
    %>             <a href="<%= url %>"><%= linkTitle %></a>  <%
               }
           }
       }
%>     
    </dd>
<%

   } else {
        WCMMode mode = WCMMode.fromRequest(request);
        if (mode == WCMMode.EDIT) {
            %><cq:include script="empty.jsp"/><%
        }
   }
%>
