<%@include file="/apps/stripe/global.jsp" %>
<%@page import="com.day.cq.dam.api.Asset,
                org.fhcrc.stripe.utilities.AssetUtil"%>
<%@page import="org.fhcrc.stripe.components.DivisionTitle"%>
<%@page import="org.fhcrc.stripe.components.DivisionTitle.DivisionTitleDetails"%>
<jsp:useBean id="divisionTitleBean" scope="request" class="org.fhcrc.stripe.components.DivisionTitle" />
<jsp:setProperty name="divisionTitleBean" property="componentProperties" value="<%= properties %>"/>
<jsp:setProperty name="divisionTitleBean" property="resourceResolver" value="<%= resourceResolver %>"/>

<%
      List<DivisionTitle.DivisionTitleDetails> divisionTitle = properties.get("division_title", divisionTitleBean.getInheritedProperty(currentPage,"divisiontitle"));
      request.setAttribute("divisionTitle",divisionTitle);
%>


<div class="divisiontitle">
    <c:set var="counter" value="0" scope="request"/>
    <c:forEach items="${divisionTitle}" var="divisionTitles" varStatus="loop">
          <c:set var="counter" value="${counter+1}" scope="request"/>
          <c:set var="totalWidth" value="980" scope="request"/>
          <c:set var="numberOfDivisionTitles" value="${fn:length(divisionTitleBean.divisionTitles)}" scope="request"/>
          <c:set var="titleWidth" value="${(totalWidth/(numberOfDivisionTitles))}" scope="request"/>
          <c:set var="size" value="5" scope="request"/>
          
          <%--
          <c:if test="${numberOfDivisionTitles == 1}">
                <c:set var="titleWidth" value="900" scope="request"/>
          </c:if>

          <c:if test="${numberOfDivisionTitles == 2}">
               <c:set var="size" value="4" scope="request"/>
                <c:set var="division1Width" value="446"/>
                <c:set var="division2Width" value="346"/>
          </c:if>
          
          <c:if test="${numberOfDivisionTitles == 3}">
                <c:set var="size" value="3" scope="request"/>
                <c:set var="division1Width" value="375"/>
                <c:set var="division2Width" value="300"/>
          </c:if>
          <c:if test="${numberOfDivisionTitles == 4}">
                <c:set var="size" value="1" scope="request"/>
                <c:set var="division1Width" value="305"/>
                <c:set var="division2Width" value="215"/>
          </c:if>
          <c:if test="${numberOfDivisionTitles == 5}">
                <c:set var="size" value="0.5" scope="request"/>
                <c:set var="division1Width" value="245"/>
                <c:set var="division2Width" value="175"/>
          </c:if>
          <c:if test="${numberOfDivisionTitles == 6}">
                <c:set var="size" value="1" scope="request"/>
                <c:set var="division1Width" value="340"/>
                <c:set var="division2Width" value="245"/>
          </c:if>
                    
          <c:if test="${divisionTitles.divisionTitle == 'VACCINE AND INFECTIOUS DISEASE'}">
              <div style="width:${division1Width}px; float: right">
                 <a href="${divisionTitles.divisionLink}" style="text-decoration: none">
                     <p style="text-align: right;"><font size="${size}px;"><c:out value="${divisionTitles.divisionTitle}"/></font></p>
                 </a>
              </div>
          </c:if>          


          <c:if test="${divisionTitles.divisionTitle == 'PUBLIC HEALTH SCIENCES'}">
                <div style="width:${division2Width}px; float: right">
                 <a href="${divisionTitles.divisionLink}" style="text-decoration: none">
                     <p style="text-align: right;"><font size="${size}px;"><c:out value="${divisionTitles.divisionTitle}"/></font></p>
                 </a>
              </div>
          </c:if>          

          <c:if test="${divisionTitles.divisionTitle == 'CLINICAL RESEARCH' ||  
                    divisionTitles.divisionTitle == 'HUMAN BIOLOGY' ||
                    divisionTitles.divisionTitle == 'BASIC SCIENCES'}">
	                <div style="width:${titleWidth+5}px; float: right">
		                 <a href="${divisionTitles.divisionLink}" style="text-decoration: none">
		                     <p style="text-align: right;"><font size="${size}px;"><c:out value="${divisionTitles.divisionTitle}"/></font></p>
		                 </a>
	               </div>
          </c:if>          
          --%>
          
        <div style="float: right">
            <a href="${divisionTitles.divisionLink}" style="text-decoration: none">
                <p style="text-align: right;">
                    <font size="${size}px;">
                        <c:if test="${loop.last}">
                            <c:out value="${divisionTitles.faculty}"/>
                        </c:if>
                        <c:out value="${divisionTitles.divisionTitle}"/>
                    </font>
                </p>
            </a>
        </div>
          
          <c:if test="${not loop.last}">
              <div style="width:10px; float:right; padding-left:5px; padding-right:5px; padding-top:2px;">
                   <font size="6px;">.</font>    
              </div>          
          </c:if>
          
    </c:forEach>
    <c:if test="${empty divisionTitle}" >
        <div>
           Configure Division Titles
        </div>
    </c:if>
</div>
<%--
<%@ page import="org.fhcrc.stripe.utilities.PageUtil" %>
<jsp:useBean id="pageUtilBean" scope="request" class="org.fhcrc.stripe.utilities.PageUtil" />
<jsp:setProperty name="pageUtilBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<%
      //String divisionTitle = properties.get("division_title", pageUtilBean.getInheritedProperty(currentPage,"divisiontitle","division_title"));
      //String divisionLink = properties.get("link", pageUtilBean.getInheritedProperty(currentPage,"divisiontitle","link"));
      //String divisionTitle = properties.get("division_title", pageUtilBean.getInheritedProperty(currentPage,"divisiontitle"));
%>



<jsp:useBean id="pageUtilBean" scope="request" class="org.fhcrc.stripe.utilities.PageUtil" />
<jsp:setProperty name="pageUtilBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<%
      String divisionTitle = properties.get("division_title", pageUtilBean.getInheritedProperty(currentPage,"divisiontitle","division_title"));
      String divisionLink = properties.get("link", pageUtilBean.getInheritedProperty(currentPage,"divisiontitle","link"));
 
%>
<div class="divisiontitle">
    <a href="<%=divisionLink%>" style="text-decoration: none"><p><%=divisionTitle%></p></a>
</div>
 --%>

<%--
<c:choose>
    <c:when test="${loop.first && divisionTitles.divisionTitle == 'VACCINE AND INFECTIOUS DISEASES'}">
        <div style="width:${titleWidth+50}px; float: left;">
          <a href="${divisionTitles.divisionLink}" style="text-decoration: none">
              <p><font size="${size}px;"><c:out value="${divisionTitles.divisionTitle}"/></font></p>
          </a>
        </div>
    </c:when>
    <c:otherwise>
        <div style="width:${titleWidth}px; float: left;">
          <a href="${divisionTitles.divisionLink}" style="text-decoration: none">
              <p><font size="${size}px;"><c:out value="${divisionTitles.divisionTitle}"/></font></p>
          </a>
        </div>
    </c:otherwise>
</c:choose>
--%>