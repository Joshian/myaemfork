<%@include file="/apps/stripe/global.jsp" %>

<jsp:useBean id="nwBioFooterBean" scope="request" class="org.fhcrc.stripe.components.NWBioFooter" />
<jsp:setProperty name="nwBioFooterBean" property="page" value="<%= currentPage %>"/>
<jsp:setProperty name="nwBioFooterBean" property="componentProperties" value="<%= properties %>"/>
<jsp:setProperty name="nwBioFooterBean" property="designProperties" value="<%= currentStyle %>"/>
<jsp:setProperty name="nwBioFooterBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<%
  String contactus = properties.get("contactus", nwBioFooterBean.getInheritedProperty(currentPage,"nwbiofooter","contactus"));  
  String contactusurl = properties.get("contactusurl", nwBioFooterBean.getInheritedProperty(currentPage,"nwbiofooter","contactusurl"));
  String stafflogin = properties.get("stafflogin", nwBioFooterBean.getInheritedProperty(currentPage,"nwbiofooter","stafflogin"));
  String staffloginurl = properties.get("staffloginurl", nwBioFooterBean.getInheritedProperty(currentPage,"nwbiofooter","staffloginurl"));
  
  String schildrens = properties.get("schildrens", nwBioFooterBean.getInheritedProperty(currentPage,"nwbiofooter","schildrens"));
  String hutch = properties.get("hutch", nwBioFooterBean.getInheritedProperty(currentPage,"nwbiofooter","hutch"));
  String scca = properties.get("scca", nwBioFooterBean.getInheritedProperty(currentPage,"nwbiofooter","scca"));
  String uw = properties.get("uw", nwBioFooterBean.getInheritedProperty(currentPage,"nwbiofooter","uw"));
%>
<div class="footer" id="footer">
    <div class="container">
        <div class="row" style="height: 55px;">
            <div class="footer-text span2" style="padding-top: 16px;">
                <p>
                    <font class="footer-page-title" style="font-size: 10pt;"><a href="<%=contactusurl%>" style="text-decoration: none; color: #FFFFFF;"><%=contactus%></a></font>  &nbsp;&nbsp;&nbsp;          
                    <font class="footer-page-title" style="font-size: 10pt;"><a href="<%=staffloginurl%>" style="text-decoration: none; color: #FFFFFF;"><%=stafflogin%></a></font><br/>
                </p>
            </div>
            <div class="span10" style="text-align: right; padding-top: 8px;">
                <a href="<%=schildrens%>" target="_blank"><img src="/etc/designs/stripedesigns/nwbiotrust/clientlibs/img/Childrens-white-150x26.png"/></a>
                <a href="<%=scca%>" target="_blank"><img src="/etc/designs/stripedesigns/nwbiotrust/clientlibs/img/SCCA-100x35.png" style="padding-left: 20px;"/></a>
                <a href="<%=uw%>" target="_blank"><img src="/etc/designs/stripedesigns/nwbiotrust/clientlibs/img/UWMedicine-logo-160x29.png" style="padding-left: 20px;"/></a>
                <a href="<%=hutch%>" target="_blank"><img src="/etc/designs/stripedesigns/nwbiotrust/clientlibs/img/FRED_HUTCH_LOGO_footer.png" style="padding-left: 20px;"/></a>
                <a href="https://www.iths.org/" target="_blank"><img src="/etc/designs/stripedesigns/nwbiotrust/clientlibs/img/ITHSLogoBlock_BW_footer.gif" style="padding-left: 20px; width:100px; height:30px;"/></a>
            </div>
        </div>
    </div>
    <div class="container footer-colorbar">
        <br/>
    </div>
</div>