<%@include file="/apps/stripe/global.jsp" %>
<%@page import="com.day.cq.wcm.api.Page,
                org.fhcrc.stripe.templates.general.Colors, org.fhcrc.stripe.utilities.PageUtil" %>
<%
String home = Text.getAbsoluteParent(currentPage.getPath(), 3) + "/jcr:content";
String showSideButton = PageUtil.getSiteConfigProperty(slingRequest, home, "showSideButton"); 
if(showSideButton.equals(""))
{
	showSideButton = "false";
}
request.setAttribute("showSideButton",showSideButton);
String buttonText = PageUtil.getSiteConfigProperty(slingRequest, home, "button1_text"); 
if(buttonText.equals(""))
{
	buttonText = "DONATE";
}
String buttonUrl = PageUtil.getSiteConfigProperty(slingRequest, home, "button1_url"); 
if(buttonUrl.equals("") && buttonText.equalsIgnoreCase("DONATE"))
{
	buttonUrl = "http://getinvolved.fhcrc.org/site/Donation2?df_id=3440&3440.donation=form1";
}

String externalLink = PageUtil.getSiteConfigProperty(slingRequest, home, "externalLink");  

if(externalLink.equals(""))
{
	externalLink = "false";
}

%>
<div class="container">
    <section class="container-par">
        <cq:include script="center.jsp" />
    </section> 
    <aside id="sidenv" class="container-aside">
          <c:if test="${showSideButton eq 'true'}">
	          <div class="side-button" >
	              <a class="btn btn-login btn-large btn-side" href="<%=buttonUrl%>" <% if(externalLink.equals("true")) { %>target="_blank" <%} %>><%=buttonText%></a>
	          </div>
          </c:if>
          <div class="nav-collapse collapse">
            <ul class="nav nav-tabs nav-stacked">
                <cq:include path="navitemsiparsys" resourceType="foundation/components/iparsys" />
            </ul>
          </div><!--/.nav-collapse -->
    </aside>
</div>
<cq:include script="content_style.jsp"/>
