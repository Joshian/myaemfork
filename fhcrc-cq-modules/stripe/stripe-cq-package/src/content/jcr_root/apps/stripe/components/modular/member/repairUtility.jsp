<%@ page import="javax.jcr.Node,
				 javax.jcr.Property" %>
<%@include file="/libs/foundation/global.jsp"%>
<% 

log.info("REPAIR UTILITY: Begin");
final String[] AFFECTED_NODES = {"alt", "fileReference", "jcr:title", "imageRotate", "imageCrop"};
final String RESOURCE_TYPE = "foundation/components/image";
Node resourceNode = resource.adaptTo(Node.class);
Node imageNode = null;

if (resourceNode.hasNode("image")) {
	
	imageNode = resourceNode.getNode("image");
	log.info("REPAIR UTILITY - Image node exists, no repair necessary");
	
} else {
	
	log.info("REPAIR UTILITY - No image node, beginning repair");
	
	resourceNode.addNode("image");
	imageNode = resourceNode.getNode("image");
	
	if (imageNode != null) {
		
		for (String prop : AFFECTED_NODES) {
			
			if (resourceNode.hasProperty(prop)) {
				
				log.info("REPAIR UTILITY: Setting property ".concat(prop).concat(" to the value ").concat(resourceNode.getProperty(prop).getString()));
				imageNode.setProperty(prop, resourceNode.getProperty(prop).getString());
				
			}
			
		}
		
		imageNode.setProperty("sling:resourceType", RESOURCE_TYPE);
		
	}
	
	resourceNode.getSession().save();
	
}

log.info("REPAIR UTILITY: End");
%>