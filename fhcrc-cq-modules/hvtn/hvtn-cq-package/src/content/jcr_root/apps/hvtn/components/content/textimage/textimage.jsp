<%@include file="/libs/foundation/global.jsp"%>
<jsp:useBean id="textImageBean" scope="request" class="org.fhcrc.hvtn.components.TextImage" />
<jsp:setProperty name="textImageBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="textImageBean" property="resource" value="<%= resource %>"/>
<jsp:setProperty name="textImageBean" property="currentStyle" value="<%= currentStyle %>"/>
<jsp:setProperty name="textImageBean" property="currentDesign" value="<%= currentDesign %>"/>
<jsp:setProperty name="textImageBean" property="resourceDesign" value="<%= resourceDesign %>"/>
<jsp:setProperty name="textImageBean" property="request" value="<%= request %>"/>
<jsp:setProperty name="textImageBean" property="writer" value="<%= out %>"/>
<jsp:setProperty name="textImageBean" property="componentProperties" value="<%= properties %>"/>

<div class="${ textImageBean.wrapperClass }">

	<div class="${ textImageBean.imageContainerClass }">

		<div class="imageContainer">
		
			<c:choose>
				<c:when test="${!empty textImageBean.linkTarget }">
					<c:choose>
						<c:when test="${ textImageBean.externalLink }">
							<a href="${ textImageBean.linkTarget }" target="_blank">
						</c:when>
						<c:otherwise>
							<a href="${ textImageBean.linkTarget }">
						</c:otherwise>
					</c:choose>
					<% textImageBean.drawImage(); %></a>
				</c:when>
				<c:otherwise>
					<% textImageBean.drawImage(); %>
				</c:otherwise>
			</c:choose>
			
		</div>
		
		<c:choose>
			<c:when test="${!empty textImageBean.hiResSrc || textImageBean.captionOrCredit }">
		
				<div class="captionAndCredit">
				
					<cq:text property="jcr:caption" tagName="span" placeholder="" tagClass="caption" />
		       		<cq:text property="credit" tagName="span" placeholder="" tagClass="credit" />
					
					<c:if test="${!empty textImageBean.hiResSrc }">
						<span class="hiRes"><a href="${ textImageBean.hiResSrc }" target="_blank">Click for high-res version</a></span>
					</c:if>
					
				</div>	
		
			</c:when>
		</c:choose>
	
	</div>
	
	<cq:text property="text" tagName="div" tagClass="textContainer" />
	
	<div class="clear"></div>

</div>

<cq:includeClientLib js="apps.hvtn.textimage"/>