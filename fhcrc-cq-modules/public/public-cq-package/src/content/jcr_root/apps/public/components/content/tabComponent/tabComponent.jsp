<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text,
                 com.day.cq.wcm.api.WCMMode,
                 org.fhcrc.tools.FHUtilityFunctions" %>

<% 
String tabtype = properties.get("tabtype",""); 
Integer tabcount = 0; 
String randomID = FHUtilityFunctions.getRandomID();
%>

<a name="top-<%= randomID %>"></a>
<div class="tabsContainer clearfix <%= tabtype %>" id="tab-<%= randomID %>">
    <ul class="tabs">
<%
if(WCMMode.fromRequest(request) == WCMMode.EDIT || !properties.get("tabtitle1","").equals("")){
%>
    <li class="tab<%=++tabcount%>"><a href="#tabcontent-tab1-<%= randomID %>"><cq:text property="tabtitle1" placeholder="<span class='defaultTabText'>Tab 1</span>" /></a></li>
<%
}
if(!properties.get("tabtitle2","").equals("")){
%>
    <li class="tab<%=++tabcount%>"><a href="#tabcontent-tab2-<%= randomID %>"><cq:text property="tabtitle2" placeholder="<span class='defaultTabText'>Tab 2</span>" /></a></li>
<%
}
if(!properties.get("tabtitle3","").equals("")){
%>
    <li class="tab<%=++tabcount%>"><a href="#tabcontent-tab3-<%= randomID %>"><cq:text property="tabtitle3" placeholder="<span class='defaultTabText'>Tab 3</span>" /></a></li>
<%
}
if(!properties.get("tabtitle4","").equals("")){
%>
    <li class="tab<%=++tabcount%>"><a href="#tabcontent-tab4-<%= randomID %>"><cq:text property="tabtitle4" placeholder="<span class='defaultTabText'>Tab 4</span>" /></a></li>
<%
}
if(!properties.get("tabtitle5","").equals("")){
%>
    <li class="tab<%=++tabcount%>"><a href="#tabcontent-tab5-<%= randomID %>"><cq:text property="tabtitle5" placeholder="<span class='defaultTabText'>Tab 5</span>" /></a></li>
<%
}
if(!properties.get("tabtitle6","").equals("") && tabtype.equals("supertabs")){
%>
    <li class="tab<%=++tabcount%>"><a href="#tabcontent-tab6-<%= randomID %>"><cq:text property="tabtitle6" placeholder="<span class='defaultTabText'>Tab 6</span>" /></a></li>
<%
}
if(!properties.get("omitViewAll","false").equals("true")){
%>    
    <li class="viewAll"><a href="#">View all</a></li>
<%
}
%>    
    </ul>
    <div class="tabContentsContainer"><div class="tabContentsContainerBorder">
<%    
if(WCMMode.fromRequest(request) == WCMMode.EDIT || !properties.get("tabtitle1","").equals("")){    
%>
        <div class="tabContent">
            <a name="tabcontent-tab1-<%= randomID %>"></a>
            <cq:text property="tabtitle1" tagName="h2" tagClass="viewAllOnly" placeholder="Tab 1" />
            <cq:include path="tabbedpar1" resourceType="foundation/components/parsys"/>
            <cq:text tagName="p" tagClass="viewAllOnly backtotop" value="<%="[<a href='#top-" + randomID  + "'>back to top</a>]" %>"/>
        </div>        
<%
}
if(!properties.get("tabtitle2","").equals("")){
%>
        <div class="tabContent">
            <a name="tabcontent-tab2-<%= randomID %>"></a>
            <cq:text property="tabtitle2" tagName="h2" tagClass="viewAllOnly" placeholder="Tab 2" />
            <cq:include path="tabbedpar2" resourceType="foundation/components/parsys"/>    
            <cq:text tagName="p" tagClass="viewAllOnly backtotop" value="<%="[<a href='#top-" + randomID  + "'>back to top</a>]" %>"/>
        </div>
<%
}
if(!properties.get("tabtitle3","").equals("")){
%>
        <div class="tabContent">
            <a name="tabcontent-tab3-<%= randomID %>"></a>
            <cq:text property="tabtitle3" tagName="h2" tagClass="viewAllOnly" placeholder="Tab 3" />
            <cq:include path="tabbedpar3" resourceType="foundation/components/parsys"/>
            <cq:text tagName="p" tagClass="viewAllOnly backtotop" value="<%="[<a href='#top-" + randomID  + "'>back to top</a>]" %>"/>
        </div>
<%
}
if(!properties.get("tabtitle4","").equals("")){
%>
        <div class="tabContent">
            <a name="tabcontent-tab4-<%= randomID %>"></a>
            <cq:text property="tabtitle4" tagName="h2" tagClass="viewAllOnly" placeholder="Tab 4" />
            <cq:include path="tabbedpar4" resourceType="foundation/components/parsys"/>
            <cq:text tagName="p" tagClass="viewAllOnly backtotop" value="<%="[<a href='#top-" + randomID  + "'>back to top</a>]" %>"/>
        </div>     
<%
}
if(!properties.get("tabtitle5","").equals("")){
%>   
        <div class="tabContent">
            <a name="tabcontent-tab5-<%= randomID %>"></a>
            <cq:text property="tabtitle5" tagName="h2" tagClass="viewAllOnly" placeholder="Tab 5" />
            <cq:include path="tabbedpar5" resourceType="foundation/components/parsys"/>    
            <cq:text tagName="p" tagClass="viewAllOnly backtotop" value="<%="[<a href='#top-" + randomID  + "'>back to top</a>]" %>"/>
        </div>
<%
}
if(!properties.get("tabtitle6","").equals("") && tabtype.equals("supertabs")){
%>   
        <div class="tabContent">
            <a name="tabcontent-tab6-<%= randomID %>"></a>
            <cq:text property="tabtitle6" tagName="h2" tagClass="viewAllOnly" placeholder="Tab 6" />
            <cq:include path="tabbedpar6" resourceType="foundation/components/parsys"/>    
            <cq:text tagName="p" tagClass="viewAllOnly backtotop" value="<%="[<a href='#top-" + randomID  + "'>back to top</a>]" %>"/>
        </div>
<%
}
%>
    </div>
</div></div>
<% //if(WCMMode.fromRequest(request) == WCMMode.EDIT){
   // trigger a reset of tabbed widgets on the page if we are in Edit Mode
%>
<script type="text/javascript">
  initTabbedWidget( "tab-<%= randomID %>" , <%= (WCMMode.fromRequest(request) == WCMMode.EDIT ? "'editmode'" : "''") %> );
</script>
