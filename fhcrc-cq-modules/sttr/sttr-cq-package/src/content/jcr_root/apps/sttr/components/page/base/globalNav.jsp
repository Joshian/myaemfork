<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.util.Iterator,
        com.day.cq.wcm.api.PageFilter,
        org.fhcrc.tools.FHUtilityFunctions" %>

<%
Page homepage = resourceResolver.getResource(currentPage.getAbsoluteParent(2).getPath()).adaptTo(Page.class);
PageFilter filter = new PageFilter();
Iterator<Page> navElements = homepage.listChildren(filter);
%>


<ul class="core">            
        
<%

while (navElements.hasNext()) {
    
    Page child = navElements.next();
    String currentPagePath = currentPage.getPath();
    String navPath = child.getPath();
    Boolean isOnTrail = currentPagePath.contains(navPath);
    StringBuffer cls = new StringBuffer();
    
    /* Note that in the line of HTML below, the </li> is missing, as we first have to place
     * the dropdown menu with the Overview link and any other children. */
     if(isOnTrail){ cls.append("selected"); }
     if(!navElements.hasNext()){ if(cls.length()!=0){cls.append(" ");} cls.append("last"); }
%>
          <li<%= cls.length()!=0?" class='"+ cls +"'":"" %>><a href="<%= navPath %>.html"><%= FHUtilityFunctions.displayTitle(child) %></a>

            <ul class="dropdown_menu">
                <li><a href="<%= navPath %>.html">Overview</a></li>
<%

    if (child.listChildren(filter).hasNext()) {
        
        Iterator<Page> dropdownElements = child.listChildren(filter);        

        while (dropdownElements.hasNext()) {
            
            Page dropdownChild = dropdownElements.next();            
%>
                <li><a href="<%= dropdownChild.getPath() %>.html"><%= FHUtilityFunctions.displayTitle(dropdownChild) %></a></li>
<%          
        }

    } /* end if (child.listChildren().hasNext()) */
/* And here is the closing tag for the <ul> and <li> above. */
%>
                </ul>
            </li>
<%
} /* end while (navElements.next()) */
%>
</ul>
