<%@include file="/libs/foundation/global.jsp"%>
<%@page session="false" %><%

String appId = properties.get("appId","267354292492");
String sendButton = properties.get("sendButton","false");
String showFaces = properties.get("showFaces","false");
int width = properties.get("width",450);
String pageHref = "";

if (appId.equals("107459469324278")) {
    pageHref = "http://www.facebook.com/HutchinsonCenter";
} else if (appId.equals("139525287864")) {
    pageHref = "http://www.facebook.com/pages/Innovators-Network/";
}

%>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<%= appId %>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-like" <%= pageHref.equals("") ? "" : "data-href=\"".concat(pageHref).concat("\"") %> <%= sendButton.equals("true") ? "data-send=\"true\"" : "" %> data-width="<%= width %>" 
<%= showFaces.equals("true") ? "data-show-faces=\"true\"" : "data-show-faces=\"false\"" %> data-font="lucida grande"></div>