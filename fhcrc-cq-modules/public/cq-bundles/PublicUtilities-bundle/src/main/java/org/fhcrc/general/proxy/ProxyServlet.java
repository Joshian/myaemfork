/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhcrc.general.proxy;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletException;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.fhcrc.publicsite.constants.Constants;

@Component
@Service
@Properties( {
		@Property(name = "service.description", value = "Proxy Servlet"),
		@Property(name = "service.vendor", value = "FHCRC"),
		@Property(name = "sling.servlet.paths", value = "/libs/fhcrc/ProxyServlet"),
		@Property(name = "sling.servlet.extensions", value = "html") })
public class ProxyServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;
	private final Logger log = LoggerFactory.getLogger(getClass());
	private final int READ_BUFFER_SIZE = 1024;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException {
		try {
			String targetURL = request.getParameter("url");
			if (targetURL != null && targetURL.matches("|^https?://"+ Constants.PROXYSERVLET_REGEX +"\\.fhcrc\\.org.*|")) {
				URL url = new URL(targetURL);
	
				// test: does the url contain |^https?://[^\.]*\.fhcrc\.org|
				// fail: return blank (no data) and log exception
				
				URLConnection urlConnection = url.openConnection();
				response.setContentType(urlConnection.getContentType());
	
				InputStream ins = urlConnection.getInputStream();
				OutputStream outs = response.getOutputStream();
				byte[] buffer = new byte[READ_BUFFER_SIZE];
				int bytesRead = 0;
				while ((bytesRead = ins.read(buffer, 0, READ_BUFFER_SIZE)) != -1) {
					outs.write(buffer, 0, bytesRead);
				}
	
				log.debug("outs = " + outs);
				outs.flush();
				outs.close();
				ins.close();
			} else {
				log.error("Attempting an illegal URL request.  targetUrl = " + targetURL);
			}
		}

		catch (MalformedURLException e) {
			log.error("", e);
		} catch (IOException e) {
			log.error("", e);
		}
	}
}