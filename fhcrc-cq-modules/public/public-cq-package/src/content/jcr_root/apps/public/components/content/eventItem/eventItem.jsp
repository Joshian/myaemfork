<%@include file="/libs/foundation/global.jsp"%><%@ page import="com.day.cq.wcm.api.WCMMode, com.day.cq.wcm.foundation.Image, com.day.cq.commons.Doctype" %>
<div class="eventlistingitem clearfix">
<%
String targetPath = properties.get("eventTarget","");
if(targetPath.equals("") || resourceResolver.getResource(targetPath)==null){
%>
<cq:include script="empty.jsp" />
<%
} else {
	Page eventPage = resourceResolver.getResource(targetPath).adaptTo(Page.class);
	String eventTitle = eventPage.getTitle();
	if(eventTitle == null || eventTitle.trim().equals("")){
	    eventTitle = eventPage.getName();
	}
	String eventDate = eventPage.getProperties().get("dateDisplay","");
	eventDate = eventDate.replaceAll("\n", "<br />"); // Turn any newline characters into break tags
	String eventDescription = eventPage.getDescription();
	if(eventDescription == null){
	    eventDescription = "";
	}
%>
    <div class="eventimage">	
    <a href="<%= eventPage.getPath() + ".html" %>">
<%	

	Resource r = eventPage.getContentResource("calendarimage");
	if (r != null) {
	   String img;
	   Image image = new Image(r);
	   if(image.hasContent()){
		   img = eventPage.getPath() + "/_jcr_content/calendarimage.img.png" + image.getSuffix(); // e.g.,: /content/public/en/events/events/hutch-award.img.png/1323194909406.gif
		    // prepend context path to img
	       img = request.getContextPath() + img;
		   img = resourceResolver.map(img);
	       //out.print("<a href='"+ eventPage.getPath() +".html'>"); <-- commented out as we already print out the <a> tag above
	       out.print("<img src='"+img+"'>");
	       //out.print("</a>");
	   } else {
%>
		   <img src="<%= currentDesign.getPath() %>/img/defaults/eventicon.jpg" alt="Calendar with date circled" />
<%
	   }
/* 
  new code: <img src='/content/public/en/events/events/hutch-award.img.png/1323285695864.jpg' class='frame'>
  old code: <img title="Business Man Working on a Train" alt="Business Man Working on a Train" width="94px" class="frame" src="/content/public/en/events/events/hutch-award/_jcr_content/image.img.jpg/1323285695864.jpg"/>
*/	   
	   
    //Image eventImage = new Image(resourceResolver.getResource(targetPath+"/jcr:content/image"));
	/*Image eventImage = new Image(eventPage.getContentResource("image"));
    if(eventImage.hasContent()){
		eventImage.addCssClass("frame");
		eventImage.addAttribute("width","94px");
		eventImage.loadStyleData(currentStyle);
	    eventImage.setSelector(".img"); // use image script
	    eventImage.setDoctype(Doctype.fromRequest(request));
	    // add design information if not default (i.e. for reference paras)
	    if (!currentDesign.equals(resourceDesign)) {
	        eventImage.setSuffix(currentDesign.getId());
	    }
	    eventImage.draw(out);}*/
	    
	} else {
%>		
		<img src="<%= currentDesign.getPath() %>/img/defaults/eventicon.jpg" alt="Calendar with date circled" />
<%
	}
%>
    </a>
    </div>
    <div class="eventtext">
        <div class="title"><a href="<%= eventPage.getPath() + ".html" %>"><%= eventTitle %></a></div>
<%
    if(!eventDate.trim().equals("")){
%>
        <div class="date"><%= eventDate %></div>
<%
    }
    if(!eventDescription.trim().equals("")){
%>
        <div class="description"><%= eventDescription %></div>
<%
    }
%>        
    </div>
<%
}
%>
</div>