<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  List component sub-script

  Creates a {com.day.cq.wcm.foundation.List} list from the request and sets
  it as a request attribute.

--%><%
%><%@page import="com.day.cq.wcm.foundation.List, com.day.cq.wcm.api.PageFilter,java.util.Comparator, java.util.Date" %><%
%><%@include file="/libs/foundation/global.jsp"%><%
    
List list = new List(slingRequest, new PageFilter());


/*
// when running under pubDate/date sorting you actually
// want to reverse the order. i'm not sure why this works when
// i place this setOrderComparator here, in init.jsp, vs. in list.jsp.
if( properties.get("orderBy","").equals("xxxxpubDate/date") ){
    list.setOrderComparator(new Comparator<Page>(){
	    public int compare(Page a, Page b){
	        Date dateA = a.getProperties().get("pubDate/date", Date.class);
	        Date dateB = b.getProperties().get("pubDate/date", Date.class);
	        if(dateA==null){
	            dateA = new Date();
	        }
	        if(dateB==null){
	            dateB = new Date();
	        }
	        if(dateA.equals(dateB)){
	          return a.getTitle().compareTo(b.getTitle()); 
	        } else {
	          return dateB.compareTo(dateA);
	        }
	    }
	}); // set by reverse chron   
}*/

request.setAttribute("list", list);

%>