<%@include file="/libs/foundation/global.jsp"%><%
%>

<div id="hero_actionbar" class="hero_actionbar clearfix">
    <!-- donate button -->
    <div class="donate"><a href="http://getinvolved.fhcrc.org/site/Donation2?df_id=3440&3440.donation=form1" onclick="_gaq.push(['_trackEvent','Home Page Click Tracking','Action Bar (below hero)','Make a Donation Today']);"><img src="<%= currentDesign.getPath() %>/img/buttons/button_lg_make_donate_home_stat.gif" class="rolloverImage" data-hover="<%= currentDesign.getPath() %>/img/buttons/button_lg_make_donate_home_over.gif" alt="Make a donation today"></a></div>
    <!-- text items in this div -->
    <div class="textitems">
      <!-- info items -->
      <div class="item"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/about/mission.html" onclick="_gaq.push(['_trackEvent','Home Page Click Tracking','Action Bar (below hero)','Our Mission']);">Our Mission</a>&nbsp;&gt;</div>
      <div class="item"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/about/breakthroughs.html" onclick="_gaq.push(['_trackEvent','Home Page Click Tracking','Action Bar (below hero)','Our Breakthroughs']);">Our Breakthroughs</a>&nbsp;&gt;</div>    
      <!-- double-line help us find -->
      <div class="helpusfind">Help us find better ways to prevent, detect<br/> and treat cancer and related diseases.</div>
    </div>
</div>