<%--
  Copyright 1997-2009 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================


--%><%@ page session="false" import="com.day.cq.wcm.api.WCMMode"%><%
%><%@include file="/libs/foundation/global.jsp"%><%

      if (WCMMode.fromRequest(slingRequest) == WCMMode.EDIT){
      out.print("<p class='cq-edit-only'><strong>Donation Formlet misconfigured</strong>. There are one or more problems with this component. Solutions: Verify you are using the id of a valid donation form; verify that the donation form has an 'other' giving level; verify that you have a valid target URL. <i>(This message not displayed on the live site.)</i></p>");
      }

%>