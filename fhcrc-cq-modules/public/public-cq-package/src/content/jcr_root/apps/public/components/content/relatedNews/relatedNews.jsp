<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text, 
                 java.util.*, 
                 org.apache.commons.lang.StringEscapeUtils, 
                 com.day.cq.tagging.Tag,
                 com.day.cq.tagging.TagManager,
                 com.day.cq.commons.RangeIterator,
                 org.fhcrc.publicsite.constants.Constants,
                 org.fhcrc.tools.FHUtilityFunctions,
				 com.day.cq.wcm.foundation.Image,
				 com.day.cq.wcm.foundation.Search,org.fhcrc.tools.FieldUtility,
				 com.day.cq.search.QueryBuilder, 
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.result.Hit" %>



<%

   /*                

     RELATED NEWS COMPONENT

     Goal is to print out a list of news related to the page you are on.
     There are two ways for news articles to show up via this component:

       (1) author has specifically picked news article(s) to appear. these always appear first,
           in the order specified by the author;

       (2) algorithm that performs a QueryBuilder search based on tags.
           default: uses the tags that are applied to the current page as the tagset to search over
           override: user can override by providing their own set of tags to replace the default tag set

           the algorithm performs a QueryBuilder search, and sort orders the results based
           upon relevancy. (not date: a user might want to use this component in a scenario in which
           highly-relevant assets appear first, even if older)


   */

   // grab properties and instantiate objects:
   TagManager tagManager = resourceResolver.adaptTo(TagManager.class); 
   Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
   QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);

   Boolean disabled = properties.get("disableWidget", false);
   Boolean omitHeader = properties.get("omitHeader", false);
   Boolean omitUnderline = properties.get("omitUnderline", false);
   String titleText = properties.get("title","Similar stories");

   // maximum number of articles to display via the algorithm (does not include those picked by the author)
   int maxNumArticles = properties.get("maxNum",3);


   // An arraylist to hold the correctly-ordered list of articles to display. will also contain the pre-pended
   // must-display articles requested by the author
   ArrayList<Page> newsPages = new ArrayList<Page>();

   // A TreeSet to date order news articles, when needed.
   TreeSet<Page> articleTree = new TreeSet<Page>(new Comparator<Page>(){
      public int compare(Page a, Page b){
        Date dateA = a.getProperties().get("pubDate/date", Date.class);
        Date dateB = b.getProperties().get("pubDate/date", Date.class);
        if(dateA==null){
            dateA = new Date();
        }
        if(dateB==null){
            dateB = new Date();
        }
        if(dateA.equals(dateB)){
            return b.getPath().compareTo(a.getPath());
        }
        return dateA.compareTo(dateB);
      }
   });


   // if the user has disabled this widget, don't display anything (except a placeholder)
   if(disabled){ %><cq:include script="empty.jsp"/><% return; }
%>




<%

  // TAGSET for searching. Determines the set of tags over which to search.
  // allows a user to replace the target tagset with another of their choosing.

    Tag[] allTags;
    String[] overrideTags = properties.get("overrideTags",String[].class);

    if(overrideTags == null || overrideTags.length == 0) {
        //allTags = tagManager.getTags(resourceResolver.getResource(currentPage.getPath() + "/jcr:content"));
        allTags = tagManager.getTags( currentPage.getContentResource() );

    } else {
        allTags = new Tag[overrideTags.length];
        for(int i = 0; i < allTags.length; i++) {
          allTags[i] = tagManager.resolve(overrideTags[i]);
        }
    }

    // Since all I really need are the Tag IDs, create an array of string to hold those 
       String[] tagIDs = new String[allTags.length];
       for(int i = 0; i < allTags.length; i++) {
          tagIDs[i] = allTags[i].getTagID();
       }



   // MUST-DISPLAY OR DO-NOT-DISPLAY options. Allow author to pick news items they WANT to appear (in the pre-pended list),
   // and news items they FORBID to appear (in the algorithm list).

     String[] mustDisplay = properties.get("include",String[].class);
     String[] dontDisplay = properties.get("exclude",String[].class);

     // pre-pend the must-display articles, if any
     if(mustDisplay!=null){
         for( int i=0; i<mustDisplay.length; i++ ){
             if( dontDisplay==null || !Arrays.asList(dontDisplay).contains( mustDisplay[i] ) ){
                 Resource r = resourceResolver.getResource(mustDisplay[i]);
                 if(r!=null){
                     Page thePage = r.adaptTo(Page.class);
               		 if(thePage!=null){
                        newsPages.add(thePage);
                     }
                 } else {
                     log.info("Article was not found: " + mustDisplay[i]);
                 }
             }
         }
     }


   // STATUS CHECK. If there are no tags on this page and also no manual overrides, then this component is meaningless. Alert the author. 

     if(allTags.length == 0 && (newsPages == null || newsPages.size() == 0)){%><cq:include script="empty.jsp"/><% return; } 



   // PERFORM A TAGSET SEARCH TO DETERMINE ARTICLES RELATED TO THIS ONE. Add whatever you find to the list of newsPages.


   if( allTags.length>0 ){

      // let's try a cq-based search so we get relevancy out of it  
      HashMap<String, String> map = new HashMap<String, String>();

      // type:
      map.put("type", "cq:Page");

      // path:
      //map.put("path",Constants.PATH_TO_NEWS_FOLDER);
      // limit to the big three paths:
      map.put("group.1_path",Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER);
      map.put("group.2_path",Constants.PATH_TO_CENTERNEWS_FOLDER);
      //map.put("group.3_path",Constants.PATH_TO_NEWSRELEASES_FOLDER);
      map.put("group.p.or", "true");

      // template:
      map.put("1_property", "jcr:content/cq:template");
      map.put("1_property.value", "/apps/public/templates/news");

      // tags:
      map.put("2_property", "jcr:content/cq:tags");
      map.put("2_property.p.or", "true");
      for( int i = 0; i< tagIDs.length; i++ ){
         map.put("2_property." + i + "_value", tagIDs[i]);
      }

      // ordering
      map.put("orderby.index","true");
      map.put("orderby", "@jcr:score");
      map.put("orderby.sort", "desc");

      // limits
      map.put("p.limit", Integer.toString( maxNumArticles+ (dontDisplay==null?0:dontDisplay.length)+ 1 )); // add a few more if author is omitting some, and add +1 because you'll probably exclude the page you're on from the found set

      // optimization
      map.put("p.guessTotal","true"); // Added to fix Oak 1.0.18 problem

      List<Hit> hitList = builder.createQuery(PredicateGroup.create(map), session).getResult().getHits();


      if(hitList.size() > 0){
        for( int i=0; i<hitList.size(); i++){
           Page p = hitList.get(i).getResource().adaptTo(Page.class);
           if( dontDisplay==null || !Arrays.asList(dontDisplay).contains(p.getPath()) ){
              newsPages.add(p);
           }
        }
      }

      //out.println(newsPages.size());

      // omit the current page because it surely is related to itself (smile)
      if(newsPages.contains(currentPage)) { newsPages.remove(currentPage); }

   }


  // FINALLY! If you have anything in newsPages, print'em out. 
  // EVerything in the list is eligible to be seen (already filtered out dont-displays), but check that you don't go over the requested maxnum
  // because the list might to too large (we padded a bit during the tagset search).

  if(newsPages.size()>0){
  
    out.println("<div class='relatednews_container'>");

    if (!omitHeader) {
	    out.println("<h3".concat(omitUnderline ? ">" : " class='underline'>").concat(titleText).concat("</h3>"));
    }
    out.println("<ul>");


    String newstitle, newssubtitle, imagesrc, imagehtml, pageurl;
    Date pubDate;
    for( int i=0; i<newsPages.size() && i<maxNumArticles; i++){ 

        Page p = newsPages.get(i);

        // get the title, subtitle and publication date
        newstitle = p.getTitle();
        newssubtitle = p.getProperties().get("newssubtitle/text","");
        pubDate = p.getProperties().get("pubDate/date", p.getProperties().get("cq:lastModified", Date.class));

        // get the thumbnail src attribute, if any
        imagesrc = ""; imagehtml = "";

        //Resource r = p.getContentResource("image");
        //if(r!=null){
        //  Image imageCheck = new Image(r);
        //  if(imageCheck!=null && imageCheck.hasContent()){
        //     imagesrc = request.getContextPath() + p.getPath() + ".img.png" + imageCheck.getSuffix();
        //     imagehtml = "<img src='"+imagesrc+"' class='frame' alt='' title='' style='max-width:125px;float:left;margin-bottom:1rem;margin-right:1rem'>";
        //
        //  }
        //}

        // update: use the image thumb servlet. if you end up using this approach, comment out the stuff above me.        
        imagehtml = "<img src='"+ resourceResolver.map(p.getPath()) +".pagethumb.140.105.fit.png' class='thumbnail frame' alt='' title=''>";


        // get the URL for the page
        pageurl = p.getPath() + ".html";


        // display this entry
        %>

             <li class="item">
                 <a href="<%=pageurl%>">
                     <div><%=imagehtml%></div>
                     <div class="news-title"><%=FieldUtility.cleanField(newstitle)%></div>
                     <div class="news-subtitle"><%=FieldUtility.cleanField(newssubtitle)%></div>
                     <div class="news-date"><%=FHUtilityFunctions.APStylize(pubDate)%></div>
                 </a>

             </li>



        <%



    }

    out.println("</ul>");
    out.println("<div class='clear'></div>");

    out.println("</div>");


  }

%>


