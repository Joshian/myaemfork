function appendFormFields() {
	if(document.getElementById("dataCollectionContainer")) {
		var toRemove = document.getElementById("dataCollectionContainer");
		toRemove.parentNode.removeChild(toRemove);
	}
	var toAppend = document.getElementById("formContainer");
	switch(document.getElementById("reportType").value) {
		case "recentlyCreated" :
			var theContainer = document.createElement("div");
			theContainer.setAttribute("id", "dataCollectionContainer");
			var theLabel = document.createElement("label");
			theLabel.setAttribute("for","timeInt");
			theLabel.appendChild(document.createTextNode("How far back would you like to look?"));
			theLabel.appendChild(document.createElement("br"));
			var small = document.createElement("small");
			small.appendChild(document.createTextNode("(default is one week)"));
			theLabel.appendChild(small);
			var timeInt = document.createElement("input");
			timeInt.setAttribute("type","num");
			timeInt.setAttribute("id","timeInt");
			timeInt.setAttribute("name","timeInt");
			var dropdown = document.createElement("select");
			dropdown.setAttribute("name","timeIncrement");
			var days = document.createElement("option");
			days.setAttribute("value","d");
			days.appendChild(document.createTextNode("days"));
			var weeks = document.createElement("option");
			weeks.setAttribute("value","w");
			weeks.appendChild(document.createTextNode("weeks"));
			var months = document.createElement("option");
			months.setAttribute("value","M");
			months.appendChild(document.createTextNode("months"));
			dropdown.appendChild(days);
			dropdown.appendChild(weeks);
			dropdown.appendChild(months);
			theContainer.appendChild(theLabel);
			theContainer.appendChild(timeInt);
			theContainer.appendChild(dropdown);
			toAppend.appendChild(theContainer);
			document.getElementById("customReportSubmit").removeAttribute("disabled");
			break;
        case "recentlyModified" :
			var theContainer = document.createElement("div");
			theContainer.setAttribute("id", "dataCollectionContainer");
			var theLabel = document.createElement("label");
			theLabel.setAttribute("for","timeInt");
			theLabel.appendChild(document.createTextNode("Find pages that have been modified in the last:"));
			theLabel.appendChild(document.createElement("br"));
			var small = document.createElement("small");
			small.appendChild(document.createTextNode("(default is one week)"));
			theLabel.appendChild(small);
			var timeInt = document.createElement("input");
			timeInt.setAttribute("type","num");
			timeInt.setAttribute("id","timeInt");
			timeInt.setAttribute("name","timeInt");
			var dropdown = document.createElement("select");
			dropdown.setAttribute("name","timeIncrement");
			var days = document.createElement("option");
			days.setAttribute("value","d");
			days.appendChild(document.createTextNode("days"));
			var weeks = document.createElement("option");
			weeks.setAttribute("value","w");
			weeks.appendChild(document.createTextNode("weeks"));
			var months = document.createElement("option");
			months.setAttribute("value","M");
			months.appendChild(document.createTextNode("months"));
			dropdown.appendChild(days);
			dropdown.appendChild(weeks);
			dropdown.appendChild(months);
			theContainer.appendChild(theLabel);
			theContainer.appendChild(timeInt);
			theContainer.appendChild(dropdown);
			toAppend.appendChild(theContainer);
			document.getElementById("customReportSubmit").removeAttribute("disabled");
			break;
		case "staleContent" :
			var theContainer = document.createElement("div");
			theContainer.setAttribute("id", "dataCollectionContainer");
			var theLabel = document.createElement("label");
			theLabel.setAttribute("for","timeInt");
			theLabel.appendChild(document.createTextNode("Find content that has not been edited in how long?"));
			theLabel.appendChild(document.createElement("br"));
			var small = document.createElement("small");
			small.appendChild(document.createTextNode("(default is 3 months)"));
			theLabel.appendChild(small);
			var timeInt = document.createElement("input");
			timeInt.setAttribute("type","num");
			timeInt.setAttribute("id","timeInt");
			timeInt.setAttribute("name","timeInt");
			var dropdown = document.createElement("select");
			dropdown.setAttribute("name","timeIncrement");
			var days = document.createElement("option");
			days.setAttribute("value","d");
			days.appendChild(document.createTextNode("days"));
			var weeks = document.createElement("option");
			weeks.setAttribute("value","w");
			weeks.appendChild(document.createTextNode("weeks"));
			var months = document.createElement("option");
			months.setAttribute("value","M");
			months.appendChild(document.createTextNode("months"));
			var years = document.createElement("option");
			years.setAttribute("value","y");
			years.appendChild(document.createTextNode("years"));
			dropdown.appendChild(days);
			dropdown.appendChild(weeks);
			dropdown.appendChild(months);
			dropdown.appendChild(years);
			theContainer.appendChild(theLabel);
			theContainer.appendChild(timeInt);
			theContainer.appendChild(dropdown);
			toAppend.appendChild(theContainer);
			document.getElementById("customReportSubmit").removeAttribute("disabled");
			break;
		case "findComponents" :
			var theContainer = document.createElement("div");
			theContainer.setAttribute("id", "dataCollectionContainer");
			var theLabel = document.createElement("label");
			theLabel.setAttribute("for","componentType");
			theLabel.appendChild(document.createTextNode("Which component type are you looking for?"));
			theLabel.appendChild(document.createElement("br"));
			var small = document.createElement("small");
			small.appendChild(document.createTextNode("(default is Embedded Video)"));
			theLabel.appendChild(small);

            var dropdown = document.createElement("select");
			dropdown.setAttribute("name","componentType");
			dropdown.setAttribute("id","componentType");

            var embeddedVideo = document.createElement("option");
			embeddedVideo.setAttribute("value","videoEmbed");
			embeddedVideo.appendChild(document.createTextNode("Embedded Video"));

            var download = document.createElement("option");
			download.setAttribute("value","download");
			download.appendChild(document.createTextNode("Download Component"));

            var horizontalImage = document.createElement("option");
			horizontalImage.setAttribute("value","horizontalImage");
			horizontalImage.appendChild(document.createTextNode("Horizontal Image Component"));

            var multipurposeBox = document.createElement("option");
			multipurposeBox.setAttribute("value","multipurposeBox");
			multipurposeBox.appendChild(document.createTextNode("Multipurpose Box Component"));

            var featuredArticle = document.createElement("option");
			featuredArticle.setAttribute("value","featuredArticle");
			featuredArticle.appendChild(document.createTextNode("Featured Article Component"));

            var featuredResearcher = document.createElement("option");
			featuredResearcher.setAttribute("value","featuredResearcher");
			featuredResearcher.appendChild(document.createTextNode("Featured Researcher Component"));

            var featuredEvents = document.createElement("option");
			featuredEvents.setAttribute("value","featuredEvents");
			featuredEvents.appendChild(document.createTextNode("Featured Events Component"));

            var reference = document.createElement("option");
			reference.setAttribute("value","reference");
			reference.appendChild(document.createTextNode("Reference Component"));

            var highlightsBox = document.createElement("option");
			highlightsBox.setAttribute("value","highlightsBox");
			highlightsBox.appendChild(document.createTextNode("Highlights Box Component"));

            var projectFinder = document.createElement("option");
			projectFinder.setAttribute("value","projectFinder");
			projectFinder.appendChild(document.createTextNode("Project Finder Component"));

            var relatedContent = document.createElement("option");
			relatedContent.setAttribute("value","relatedContent");
			relatedContent.appendChild(document.createTextNode("Related Content Component"));

            var superTabs = document.createElement("option");
			superTabs.setAttribute("value","superTabs");
			superTabs.appendChild(document.createTextNode("Super Tabs Component"));

            var tabFacade = document.createElement("option");
			tabFacade.setAttribute("value","tabFacade");
			tabFacade.appendChild(document.createTextNode("Tab Facade Component"));

            var textCallout = document.createElement("option");
			textCallout.setAttribute("value","textCallout");
			textCallout.appendChild(document.createTextNode("Text and Callout Box Component"));

            var titleOrganization = document.createElement("option");
			titleOrganization.setAttribute("value","titleOrganization");
			titleOrganization.appendChild(document.createTextNode("Title and Organization Component"));

            var list = document.createElement("option");
			list.setAttribute("value","list");
			list.appendChild(document.createTextNode("List Component"));
			
			var bulletedPicklist = document.createElement("option");
			bulletedPicklist.setAttribute("value","bulletedPicklist");
			bulletedPicklist.appendChild(document.createTextNode("Bulleted Picklist Component"));
			
			var calendarRSSReader = document.createElement("option");
			calendarRSSReader.setAttribute("value","calendarRSSReader");
			calendarRSSReader.appendChild(document.createTextNode("Calendar RSS Reader Component"));
			
			var divisionEvent = document.createElement("option");
			divisionEvent.setAttribute("value","divisionEvent");
			divisionEvent.appendChild(document.createTextNode("Division Event Item Component"));
			
			var divisionEventLister = document.createElement("option");
			divisionEventLister.setAttribute("value","divisionEventLister");
			divisionEventLister.appendChild(document.createTextNode("Division Event Lister Component"));
			
			var eventInfoBlock = document.createElement("option");
			eventInfoBlock.setAttribute("value","eventInfoBlock");
			eventInfoBlock.appendChild(document.createTextNode("Event Info Block Component"));
			
			var inPageNav = document.createElement("option");
			inPageNav.setAttribute("value","inPageNav");
			inPageNav.appendChild(document.createTextNode("In-Page Navigation Component"));
			
			var mediaCoverageList = document.createElement("option");
			mediaCoverageList.setAttribute("value","mediaCoverageList");
			mediaCoverageList.appendChild(document.createTextNode("Media Coverage Lister Component"));
			
			var newsFinder = document.createElement("option");
			newsFinder.setAttribute("value","newsFinder");
			newsFinder.appendChild(document.createTextNode("News Finder Component"));
			
			var newsLister = document.createElement("option");
			newsLister.setAttribute("value","newsLister");
			newsLister.appendChild(document.createTextNode("News Lister Component"));
			
			var directorySearchBox = document.createElement("option");
			directorySearchBox.setAttribute("value","directorySearchBox");
			directorySearchBox.appendChild(document.createTextNode("Directory Search Box Component"));
			
			var directorySearchResults = document.createElement("option");
			directorySearchResults.setAttribute("value","directorySearchResults");
			directorySearchResults.appendChild(document.createTextNode("Directory Search Results Component"));
			
			var eventItem = document.createElement("option");
			eventItem.setAttribute("value","eventItem");
			eventItem.appendChild(document.createTextNode("Event Item Component"));
			
			var INslideshow = document.createElement("option");
			INslideshow.setAttribute("value","INslideshow");
			INslideshow.appendChild(document.createTextNode("Slideshow with Title Card Component"));
			
			var phoneNumber = document.createElement("option");
			phoneNumber.setAttribute("value","phoneNumber");
			phoneNumber.appendChild(document.createTextNode("Phone Number Component"));
			
			var profileFinder = document.createElement("option");
			profileFinder.setAttribute("value","profileFinder");
			profileFinder.appendChild(document.createTextNode("Profile Finder Component"));
			
			var rssFeedLister = document.createElement("option");
			rssFeedLister.setAttribute("value","rssFeedLister");
			rssFeedLister.appendChild(document.createTextNode("RSS Feed Lister Component"));
			
			var searchbox = document.createElement("option");
			searchbox.setAttribute("value","searchbox");
			searchbox.appendChild(document.createTextNode("Google Search Box Component"));
			
			var searchResults = document.createElement("option");
			searchResults.setAttribute("value","searchResults");
			searchResults.appendChild(document.createTextNode("Search Results Component"));
			
			var table = document.createElement("option");
			table.setAttribute("value","table");
			table.appendChild(document.createTextNode("Table Component"));
			
			var fb = document.createElement("option");
			fb.setAttribute("value","facebookLike");
			fb.appendChild(document.createTextNode("Facebook Like Component"));
			
			var videoSlideshow = document.createElement("option");
			videoSlideshow.setAttribute("value","videoSlideshow");
			videoSlideshow.appendChild(document.createTextNode("Video Slideshow Component"));
			
			var repairUtility = document.createElement("option");
			repairUtility.setAttribute("value","repairUtility");
			repairUtility.appendChild(document.createTextNode("Repair Utility"));
			
			var featureBox = document.createElement("option");
			featureBox.setAttribute("value","featureBox");
			featureBox.appendChild(document.createTextNode("Feature Box"));
			
			var donationFormlet = document.createElement("option");
			donationFormlet.setAttribute("value","donationFormlet");
			donationFormlet.appendChild(document.createTextNode("Donation Formlet"));
            
            var homepageSlider = document.createElement("option");
			homepageSlider.setAttribute("value","homepageSlider");
			homepageSlider.appendChild(document.createTextNode("Slider (Large Images)"));
            
            var standardSlider = document.createElement("option");
			standardSlider.setAttribute("value","standardSlider");
			standardSlider.appendChild(document.createTextNode("Slider (Full-width)"));
            
            var donateButton = document.createElement("option");
			donateButton.setAttribute("value","donateButton");
			donateButton.appendChild(document.createTextNode("Donate Button"));
            
            var forms = document.createElement("option");
			forms.setAttribute("value","forms");
			forms.appendChild(document.createTextNode("Forms"));

			
			dropdown.appendChild(bulletedPicklist);
			dropdown.appendChild(calendarRSSReader);
			dropdown.appendChild(directorySearchBox);
            dropdown.appendChild(directorySearchResults);
			dropdown.appendChild(divisionEvent);
            dropdown.appendChild(divisionEventLister);
            dropdown.appendChild(donateButton);
            dropdown.appendChild(donationFormlet);
			dropdown.appendChild(download);
			dropdown.appendChild(embeddedVideo);
			dropdown.appendChild(eventInfoBlock);
			dropdown.appendChild(eventItem);
			dropdown.appendChild(fb);
			dropdown.appendChild(featureBox);
			dropdown.appendChild(featuredArticle);
			dropdown.appendChild(featuredEvents);
            dropdown.appendChild(featuredResearcher);
            dropdown.appendChild(forms);
            dropdown.appendChild(searchbox);
            dropdown.appendChild(highlightsBox);
            dropdown.appendChild(horizontalImage);
            dropdown.appendChild(inPageNav);
            dropdown.appendChild(list);
            dropdown.appendChild(mediaCoverageList);
            dropdown.appendChild(multipurposeBox);
            dropdown.appendChild(newsFinder);
            dropdown.appendChild(newsLister);
            dropdown.appendChild(phoneNumber);
            dropdown.appendChild(profileFinder);
            dropdown.appendChild(projectFinder);            
            dropdown.appendChild(reference);
            dropdown.appendChild(relatedContent);
            dropdown.appendChild(rssFeedLister);
            dropdown.appendChild(searchResults);
            dropdown.appendChild(homepageSlider);
            dropdown.appendChild(standardSlider);
            dropdown.appendChild(INslideshow);
            dropdown.appendChild(superTabs);
            dropdown.appendChild(tabFacade);
            dropdown.appendChild(table);
            dropdown.appendChild(textCallout);
            dropdown.appendChild(titleOrganization);
            dropdown.appendChild(videoSlideshow);
            dropdown.appendChild(repairUtility);

			theContainer.appendChild(theLabel);
			theContainer.appendChild(dropdown);
			toAppend.appendChild(theContainer);
			document.getElementById("customReportSubmit").removeAttribute("disabled");
			break;
		case "newUsers" :
			var theContainer = document.createElement("div");
			theContainer.setAttribute("id", "dataCollectionContainer");
			var theLabel = document.createElement("label");
			theLabel.setAttribute("for","timeInt");
			theLabel.appendChild(document.createTextNode("How far back would you like to look?"));
			theLabel.appendChild(document.createElement("br"));
			var small = document.createElement("small");
			small.appendChild(document.createTextNode("(default is one week)"));
			theLabel.appendChild(small);
			var timeInt = document.createElement("input");
			timeInt.setAttribute("type","num");
			timeInt.setAttribute("id","timeInt");
			timeInt.setAttribute("name","timeInt");
			var dropdown = document.createElement("select");
			dropdown.setAttribute("name","timeIncrement");
			var days = document.createElement("option");
			days.setAttribute("value","d");
			days.appendChild(document.createTextNode("days"));
			var weeks = document.createElement("option");
			weeks.setAttribute("value","w");
			weeks.appendChild(document.createTextNode("weeks"));
			var months = document.createElement("option");
			months.setAttribute("value","M");
			months.appendChild(document.createTextNode("months"));
			dropdown.appendChild(days);
			dropdown.appendChild(weeks);
			dropdown.appendChild(months);
			theContainer.appendChild(theLabel);
			theContainer.appendChild(timeInt);
			theContainer.appendChild(dropdown);
			toAppend.appendChild(theContainer);
			document.getElementById("customReportSubmit").removeAttribute("disabled");
			break;
		default :
			document.getElementById("customReportSubmit").setAttribute("disabled", "disabled");
			break;
	}
}

if(document.getElementById("reportType")) {
	document.addEventListener ? 
			document.getElementById("reportType").addEventListener("change",appendFormFields, false) 
			: document.getElementById("reportType").attachEvent("onchange",appendFormFields);
}