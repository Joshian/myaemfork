<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text,
                 com.day.cq.wcm.api.WCMMode,
                 org.fhcrc.tools.FHUtilityFunctions" %>

<% 
String tabtype = properties.get("tabtype","grid12_col2");
String cssClass = properties.get("cssClass","");
String randomID = FHUtilityFunctions.getRandomID();
%>


<div class="tabsContainer tabfacade clearfix <%= tabtype %> <%= cssClass %>" x-style="z-index:1" id="tab-<%= randomID %>">
    <ul class="tabs">
<%
if(WCMMode.fromRequest(request) == WCMMode.EDIT || !properties.get("tabtitle1","").equals("")){
%>
    <li class="tabtitle1 <%= properties.get("tabactive1","").equals("true")?"active":"" %>"><a href="<%= properties.get("taburl1","#") %>.html"><cq:text property="tabtitle1" placeholder="<span class='defaultTabText'>Tab 1</span>" /><br/>&nbsp;</a></li>
<%
}
if(!properties.get("tabtitle2","").equals("")){
%>
    <li class="tabtitle2 <%= properties.get("tabactive2","").equals("true")?"active":"" %>"><a href="<%= properties.get("taburl2","#") %>.html"><cq:text property="tabtitle2" placeholder="<span class='defaultTabText'>Tab 2</span>" /><br/>&nbsp;</a></li>
<%
}
%>

<%-- automatically omit some tabs if we are using a particular tabtype --%>
<% if(!cssClass.equals("jobstabs")){ %>

    <% 
    if(!properties.get("tabtitle3","").equals("")){
    %>
        <li class="tabtitle3 <%= properties.get("tabactive3","").equals("true")?"active":"" %>"><a href="<%= properties.get("taburl3","#") %>"><cq:text property="tabtitle3" placeholder="<span class='defaultTabText'>Tab 3</span>" /><br/>&nbsp;</a></li>
    <%
    }
    if(!properties.get("tabtitle4","").equals("")){
    %>
        <li class="tabtitle4 <%= properties.get("tabactive4","").equals("true")?"active":"" %>"><a href="<%= properties.get("taburl4","#") %>"><cq:text property="tabtitle4" placeholder="<span class='defaultTabText'>Tab 4</span>" /><br/>&nbsp;</a></li>
    <%
    }
    if(!properties.get("tabtitle5","").equals("")){
    %>
        <li class="tabtitle5 <%= properties.get("tabactive5","").equals("true")?"active":"" %>"><a href="<%= properties.get("taburl5","#") %>"><cq:text property="tabtitle5" placeholder="<span class='defaultTabText'>Tab 5</span>" /><br/>&nbsp;</a></li>
    <% } %>   

<% } %>


    </ul>
    <div class="tabContentsContainer"><div class="tabContentsContainerBorder"><div class="shim"></div>

    <%-- the layout of the tab area depends upon the tabtype property --%>
    
    <% if( tabtype.equals("grid12_col2")){ %>
        <div class="tabContent">
          <div class="grid_8 alpha omega">
            <cq:include path="tabfacadepar1" resourceType="foundation/components/parsys"/>
          </div>
          <div class="grid_4 alpha omega"><div class="fh_rightcol_content">
            <cq:include path="tabfacadepar2" resourceType="foundation/components/parsys"/>
          </div></div>            
        </div>    
    <% } %>           

    </div></div></div>
<% //if(WCMMode.fromRequest(request) == WCMMode.EDIT){
   // trigger a reset of tabbed widgets on the page if we are in Edit Mode
%>
<script type="text/javascript">
  initTabFacadeWidget( "tab-<%= randomID  %>" ); 
</script>
