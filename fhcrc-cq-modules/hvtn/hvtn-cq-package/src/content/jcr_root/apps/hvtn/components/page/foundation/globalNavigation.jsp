<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.util.Iterator,
        com.day.cq.wcm.api.PageFilter,
        org.fhcrc.tools.FHUtilityFunctions" %>

<%
Page homepage = resourceResolver.getResource(currentPage.getAbsoluteParent(2).getPath()).adaptTo(Page.class);
PageFilter filter = new PageFilter();
Iterator<Page> navElements = homepage.listChildren(filter);
%>

<nav>

    <div id="navigation-main-wrapper" class="black-alpha60 noprint">
        <ul id="navigation-main">
        
<%

while (navElements.hasNext()) {
    
    Page child = navElements.next();
    String currentPagePath = currentPage.getPath();
    String navPath = child.getPath();
    Boolean isOnTrail = currentPagePath.contains(navPath);
    
/* Note that in the line of HTML below, the </li> is missing, as we first have to check to see if
 * there are children that need to go into the dropdown */
%>
          <li class="navigation-main-item">
          	<a class="navigation-main-link<%= isOnTrail ? " currentSection" : "" %>" href="<%= navPath %>.html"><%= FHUtilityFunctions.displayTitle(child) %></a>
<%

    if (child.listChildren(filter).hasNext()) {
        
        Iterator<Page> dropdownElements = child.listChildren(filter);
        
%>
            <ul class="navigation-sub">
<%
        while (dropdownElements.hasNext()) {
            
            Page dropdownChild = dropdownElements.next();
            
%>
                <li class="navigation-sub-item"><a href="<%= dropdownChild.getPath() %>.html"><%= FHUtilityFunctions.displayTitle(dropdownChild) %></a></li>
<%          
        }

%>

            </ul><!-- end dropdown_menu -->

<%
    } /* end if (child.listChildren().hasNext()) */
/* And here is the closing tag for the <li> above. */
%>
            </li>
<%
} /* end while (navElements.next()) */
%>
        </ul><!-- end global_nav ul -->
    </div>
</nav>