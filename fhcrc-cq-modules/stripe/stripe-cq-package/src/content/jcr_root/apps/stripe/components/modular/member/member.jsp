<%@include file="/apps/stripe/global.jsp"%>
<jsp:useBean id="memberBean" scope="request" class="org.fhcrc.stripe.components.Member" />
<jsp:setProperty name="memberBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="memberBean" property="resource" value="<%= resource %>"/>
<jsp:setProperty name="memberBean" property="currentStyle" value="<%= currentStyle %>"/>
<jsp:setProperty name="memberBean" property="currentDesign" value="<%= currentDesign %>"/>
<jsp:setProperty name="memberBean" property="resourceDesign" value="<%= resourceDesign %>"/>
<jsp:setProperty name="memberBean" property="request" value="<%= request %>"/>
<jsp:setProperty name="memberBean" property="writer" value="<%= out %>"/>
<jsp:setProperty name="memberBean" property="componentProperties" value="<%= properties %>"/>

<%--
Removing this as it has served its purpose. ICS 6/24/15
<cq:include script="repairUtility.jsp" />
--%>

<%
       boolean externalLink = memberBean.isExternalLink();
       String memberPage = memberBean.getMemberPage();

%>       
<div class="member" style="height:${memberBean.height}px;">
    <c:choose>
        <c:when test="${!empty memberBean.image}">
           <c:choose>
             <c:when test="${!empty memberBean.memberPage}">
                   <a <%= externalLink ? "target=\"_blank\" " : "" %>href="<%=memberPage%>"><% memberBean.drawImage(); %></a>
                </c:when>
                <c:otherwise>
                    <% memberBean.drawImage(); %>
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:otherwise>
             <img src="http://placehold.it/230x230"></img>
        </c:otherwise>
    </c:choose>
    <p class="name" >${memberBean.name}</p> 
    <c:if test="${!empty memberBean.title}">
        ${memberBean.title} </br>
    </c:if>
    <c:if test="${!empty memberBean.dates}">
        ${memberBean.dates} </br>
    </c:if>
    <c:if test="${!empty memberBean.career}"> 
	    <c:forEach items="${memberBean.career}" var="career" varStatus="loop">
	            ${career}</br>
	    </c:forEach>
    </c:if>
    <c:if test="${!empty memberBean.email}">
        <a class="email" href="mailto:${memberBean.email}" >${memberBean.email}</a></br>
    </c:if>
    <c:if test="${!empty memberBean.phone}">
        ${memberBean.phone}</br>
    </c:if>
   <c:if test="${!empty memberBean.other}">
        ${memberBean.other}<br>
    </c:if>
    <input type="hidden" id="memberPage" name="memberPage" value="${memberBean.memberPage}"/>
</div>
