<%@include file="/libs/foundation/global.jsp"%>
<jsp:useBean id="slideBean" scope="request" class="org.fhcrc.publicsite.pageComponents.Slide" />
<jsp:setProperty name="slideBean" property="currentPage" value="<%= currentPage %>"/>
<jsp:setProperty name="slideBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="slideBean" property="resource" value="<%= resource %>"/>
<jsp:setProperty name="slideBean" property="currentStyle" value="<%= currentStyle %>"/>
<jsp:setProperty name="slideBean" property="currentDesign" value="<%= currentDesign %>"/>
<jsp:setProperty name="slideBean" property="resourceDesign" value="<%= resourceDesign %>"/>
<jsp:setProperty name="slideBean" property="request" value="<%= request %>"/>
<jsp:setProperty name="slideBean" property="writer" value="<%= out %>"/>
<jsp:setProperty name="slideBean" property="componentProperties" value="<%= properties %>"/>

<!-- CONTENT AREA -->

	<div class="content_container outer_grid_container">
	
	<!-- PRIMARY CONTENT AREA -->
	  
	  	<div class="primary_content_container inner_grid_container">
	  	
		  	<c:choose>
		
				<c:when test="${ slideBean.photoContent }">
			    
			    	<div class="slide_photos_container">
			      
			      		<div class="slide_photo active">
			      
				        	<% slideBean.drawSlideImage(); %>
				        
				    	</div>
			      
			      	</div>
			      
			    </c:when>
			
				<c:otherwise>
			
					<cq:include script="empty.jsp" />
				
				</c:otherwise>
			
			</c:choose>
		      
		</div>
    
	<!-- SIDEBAR / SECONDARY CONTENT AREA -->
  
	  	<div class="secondary_content_container inner_grid_container">
	    
	    <!-- THUMBNAIL NAVIGATION -->
	    <!-- 
	    	<div class="thumbnail_navigation_container">
	      
		      	<div class="thumbnail_navigation_control">
		        
		        	<img id="thumbnail_left_arrow" src="<%= currentDesign.getPath() %>/img/slideshow/thumbnail_left_arrow_blk.png">
		        
		        </div>
		        
		        <div class="thumbnail_viewport">
		        
		          <div class="thumbnail_container">
		          
		          	<div class="thumbnail_image thumbnail_placeholder">
		              <img class="thumbnail" src="img/empty_thumbnail.jpg">
		              <div class="thumbnail_filter"></div>
		            </div>
		          
		            <div class="thumbnail_image active_thumbnail">
		              <img class="thumbnail" src="">
		              <div class="thumbnail_filter"></div>
		            </div>
		
					<div class="thumbnail_image thumbnail_placeholder">
		              <img class="thumbnail" src="img/empty_thumbnail.jpg">
		              <div class="thumbnail_filter"></div>
		            </div>
		          
		          </div>
		          
		        </div>
	        
	        	<div class="thumbnail_navigation_control">
	        
	        		<img id="thumbnail_right_arrow" src="<%= currentDesign.getPath() %>/img/slideshow/thumbnail_right_arrow_blk.png">
	        
	        	</div>
	        	
	        </div>
	    -->  
	    <!-- END THUMBNAIL NAVIGATION -->
	    
		    <div class="slide_information_container">
		      
		      	<div class="slide_information active">
		      
			    <!-- HEADLINE -->
			    
			    	<div class="headline_container">
			    
			    		<cq:text property="headline" tagName="h2" />
			        
			    	</div>
			      
			    <!-- END HEADLINE -->
			      
			    <!-- CAPTION -->
			    
			    	<div class="caption_container">
			      
			      		<cq:text property="caption" />
			        
			      	</div>
			      
			    <!-- END CAPTION -->
			      
			    <!-- PHOTO CREDIT -->
			    
			    	<div class="photo_credit_container">
			    	
			    		<cq:text property="photoCredit" tagName="span" tagClass="photo_credit" />
			      
			    	</div>
			      
			    <!-- END PHOTO CREDIT -->
			    
			    <c:if test="${ slideBean.related }">
			    
			    <!-- RELATED CONTENT -->
			    
			    	<div class="related_content_container">
			      
			      		<h3 class="related_content_header">Related</h3>
			        
			        	<% slideBean.drawRelatedContent(); %>
			      
			    	</div>
			      
			    <!-- END RELATED CONTENT -->
			    
			    </c:if>
			    
			    </div>
		    
		    </div>
		    
		</div>
    
	<!-- END SECONDARY CONTENT -->

		<div class="clear"></div>
  
	</div>
  
<!-- END CONTENT AREA -->
