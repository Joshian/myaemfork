<%@include file="/libs/foundation/global.jsp" %>
<!-- utility nav background is outside the positioning context -->
<div id="fh_utility_nav"><div id="utility_nav" class="utility_nav"><cq:include path="searchBox" resourceType="public/components/content/searchbox" /><ul>
   <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/careers.html">Careers</a></li> 
   <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/news.html">News</a></li> 
   <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/contact-us.html">Contact Us</a></li> 
   <li><a id="utility_search_link" href="<%= currentPage.getAbsoluteParent(2).getPath() %>/util/search.html">Search</a></li> 
   <li class="donate-now"><% 
   
       // the homepage has slightly different tracking subsrc value than all interior pages:
       
       if(currentPage.getProperties().get("cq:template","").equals("/apps/public/templates/homepage")) {

         request.setAttribute("donateButtonTargetURL","https://secure2.convio.net/fhcrc/site/Donation2?df_id=5980&5980.donation=form1&s_src=web&s_subsrc=aeguh0");

       } else {

         //request.setAttribute("donateButtonTargetURL","https://secure2.convio.net/fhcrc/site/Donation2?df_id=5980&5980.donation=form1&s_src=web&s_subsrc=aegu0");
         // temporarily set for the fall DNA campaign:
         //request.setAttribute("donateButtonTargetURL","https://secure2.convio.net/fhcrc/site/Donation2?df_id=7343&7343.donation=form1&s_src=DNA16&s_subsrc=aegeo16112");  
         // responsive donation form:
         request.setAttribute("donateButtonTargetURL","https://secure2.convio.net/fhcrc/site/SPageServer?pagename=adf_agiv&experience=dna16&s_src=DNA16&s_subsrc=aegeo16112");     
       
       } 
       
       %><cq:include path="utility_donate_button" resourceType="/apps/public/components/content/donateButton" /></li>
   <%--    <li class="donate-now"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/redirect-stubs/donate.html">Donate Now</a></li> --%>
  </ul></div></div>
