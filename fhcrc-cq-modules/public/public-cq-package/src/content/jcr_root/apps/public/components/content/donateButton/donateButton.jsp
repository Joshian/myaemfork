<%--

  Donation Button component
  
  A simple component to assure that all Donation Buttons on our site are identical. Has only one dialog item:
  Donation Link location, a URL to where the button will link to.
  
  Pass in the default target URL:
  If you want your script to pass in the value of the href, just include this in your calling .jsp:
  
  request.setAttribute("donateButtonTargetURL", "..value of the href...");
  
  

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %>

<jsp:useBean id="donateBean" scope="request" class="org.fhcrc.publicsite.components.DonateButton" />
<jsp:setProperty name="donateBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="donateBean" property="componentProperties" value="<%= properties %>"/>

<% // check whether we are being sent the target URL and use that if so...
   String target_url = (String)request.getAttribute("donateButtonTargetURL"); 
   
   // ...except if the user has specifically set the linklocation of this component instance
   if( properties.get("linkLocation") != null ){
       target_url = null;
   }
      
%>
      
   <a class="donate_button" href="<%= xssAPI.filterHTML( target_url !=null ? target_url : donateBean.getDonationLink() ) %>">Donate Now</a>