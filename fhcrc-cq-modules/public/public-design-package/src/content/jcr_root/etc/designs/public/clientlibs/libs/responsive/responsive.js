/* responsive.js for responsive page types */

/* set up click events on the mobile menu's activating button */
$(document).ready(function(){
  $("#mobile_menu .mobile_menu_button img").click(function(){
    toggleMobileDropdownMenu();
  });
});

function toggleMobileDropdownMenu(){
  if( $("#mobile_menu").hasClass("engaged") ){
    $("#mobile_menu .mobile_menu_contents").slideToggle('fast',function(){
      $("#mobile_menu").toggleClass('engaged');
    });
  } else {
    $("#mobile_menu").toggleClass('engaged');
    $("#mobile_menu .mobile_menu_contents").slideToggle('fast',function(){});
  }
}

