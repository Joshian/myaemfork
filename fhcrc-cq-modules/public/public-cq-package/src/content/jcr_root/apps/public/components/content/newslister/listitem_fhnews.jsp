<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  List component sub-script

  Draws a list item as a news headline.
                                               r
  request attributes:
  - {com.day.cq.wcm.foundation.List} list The list
  - {com.day.cq.wcm.api.Page} listitem The list item as a page

--%><%
%><%@ page session="false"
           import="java.text.DateFormat,
                   java.util.Date,
                   com.day.cq.wcm.api.Page,
                   java.text.SimpleDateFormat;"%><%

    Page listItem = (Page)request.getAttribute("listitem");
    String title = listItem.getTitle() != null ? listItem.getTitle() : listItem.getName();
    String description = listItem.getDescription() != null ? listItem.getDescription() : "";
    Date defaultDate = new Date();
    //Date date = listItem.getProperties().get("date", listItem.getProperties().get("cq:lastModified", defaultDate));
    Date date = listItem.getProperties().get("pubDate/date", listItem.getProperties().get("cq:lastModified", defaultDate));
    //Date date2 = listItem.getProperties().get("pubDate",defaultDate);
    //Date date2 = listItem.getProperties().get("pubDate", defaultDate);
    //String date3 = listItem.getProperties().get("pubDate","");
    //Date date4 = listItem.getProperties().get("pubDate", listItem.getProperties().get("cq:lastModified", defaultDate));


    %><li><p><a href="<%= listItem.getPath() %>.html" title="<%= title %>"><span class="news-title"><%= title %></span></a>
    
    
    
    
    <%
    if (defaultDate.getTime() != date.getTime()) {
    	DateFormat f = new SimpleDateFormat("MMM d, yyyy");
        %><br/><span class="news-date"><%= f.format(date) %></span><%
    }
    %>
    
    in <a href="/content/public/en/news/press-releases.html">Press Releases</a>
    
    
    <%
    if (!"".equals(description)) {
        %><br/><span class="news-description"><%= description %></span><%
    }
%></p></li>