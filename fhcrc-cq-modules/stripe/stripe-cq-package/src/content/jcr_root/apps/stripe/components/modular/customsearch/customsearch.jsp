<%@include file="/libs/foundation/global.jsp" %> 
<%@ page import="com.day.cq.search.result.*,
                org.fhcrc.stripe.utilities.SearchUtil,
                org.apache.commons.httpclient.*, 
                org.apache.commons.httpclient.methods.*,
                org.apache.commons.httpclient.params.HttpMethodParams,
                java.io.IOException,
                org.fhcrc.stripe.utilities.PageUtil,
                org.fhcrc.tools.FHUtilityFunctions,
                java.util.Date,
                java.net.URLEncoder,
                org.apache.commons.lang.StringEscapeUtils"%>
<cq:setContentBundle/>
<%
      String searchCriteria = request.getParameter("q");

		if (searchCriteria == null) {
			searchCriteria = "";
		}

      SearchResult stripeResult = null;
      SearchResult publicResult = null;
      
      if(searchCriteria != null && searchCriteria.length()>0)
      {
      
          // Search under /content/stripe
          stripeResult = SearchUtil.getSearchResults(slingRequest, resource, searchCriteria , "/content/stripe");
          // Search under /content/public      
          publicResult = SearchUtil.getSearchResults(slingRequest, resource, searchCriteria , "/content/public");
      }
      request.setAttribute("searchCriteria",searchCriteria);
      request.setAttribute("stripeResult",stripeResult);
      request.setAttribute("publicResult",publicResult);
      
      String searchAPILink = properties.get("./searchAPILink", "http://is.fhcrc.org/sites/public/apis/directory-api/directory.php");
      
      String queryString = "q=" + URLEncoder.encode(searchCriteria,"UTF-8") + "&format=json&short=true&type=people";
      searchAPILink = searchAPILink + "?" + queryString;
      log.info("Directory Search url:  " + searchAPILink);
      
      HttpClient client = new HttpClient();
      HttpMethod method = new GetMethod(searchAPILink);
      try {
          int statusCode = client.executeMethod(method);
          if (statusCode != HttpStatus.SC_OK) {
            log.error("Method failed: " + method.getStatusLine());
          }
          
          String searchResponse = method.getResponseBodyAsString();
          
          String q_encoded = "";
          if(request.getParameter("q")!=null) { q_encoded = URLEncoder.encode(request.getParameter("q"),"UTF-8"); }
          

%>
<div style="border-bottom:1px silver solid;font-size:16px;/*background-color: #636267; padding: 10px 10px 10px 10px*/">
    Search for: <c:if test="${searchCriteria != null}"><%=StringEscapeUtils.escapeHtml(searchCriteria)%></c:if>
</div>    

<script>
var directory_results = <%=searchResponse%> 
</script>
<div id="directory_results_api_marker">
</div>
<div id="googleapisearchresults">
<script type="text/javascript">
            if(directory_results&&directory_results.people_found&&directory_results.people_fields){
                var msg = "";                
                msg+= "<h5 class='underline facultystaff'>Faculty &amp; Staff Directory Results</h5>";
                msg+= "<table cellspacing='0' width='100%'>";
                msg+= "<tr><th scope='col' align='left'>Name</th><th scope='col' align='left'>Department</th><th scope='col' align='left'>Phone</th><th scope='col' align='left'>Email</th><th scope='col' align='left'></th></tr>";
                $.each( directory_results.people, function(k,v){
                      msg+= "<tr>";
                      msg+= "<td>" + v[directory_results.people_fields.Name] + "</td>";
                      msg+= "<td>" + (v[directory_results.people_fields.Department]? v[directory_results.people_fields.Department] : "") + "</td>";
                      msg+= "<td>" + (v[directory_results.people_fields.Phone]?v[directory_results.people_fields.Phone]:"") + "</td>";
                      msg+= "<td>" + (v[directory_results.people_fields.Email]?"<a href='mailto:"+v[directory_results.people_fields.Email]+"'>"+v[directory_results.people_fields.Email]+"</a>":"") + "</td>";
                      msg+= "<td class='spacer'>&nbsp;</td>";
                      msg+= "</tr>";
                    } );
                msg+= "</table>";
                var prefix = location.host.substring(0,2)=='cq' ? '/content/public' : ''; // when in the cms need this extra pathing
                if( directory_results.people.length < directory_results.people_found ){
                    msg+= "<a href='http://www.fredhutch.org/en/util/directory.html?q=<%= q_encoded %>&amp;short=true' title='Our full directory has more information and detail' target='_blank'>View " + (directory_results.people_found - directory_results.people.length) + " more result" + ((directory_results.people_found - directory_results.people.length)==1?'':'s')  + " and more details &raquo;</a>";
                } else {
                    msg+= "<a href='http://www.fredhutch.org/en/util/directory.html?q=<%= q_encoded %>&amp;short=true' title='Our full directory has more information and detail' target='_blank'>View more details in our full directory &raquo;</a>";
                }
                $("#directory_results_api_marker").html( msg );
            }
       </script>
    </div>
<%
} catch (HttpException e) {
    log.error("Fatal protocol violation: " + e.getMessage());
    response.setStatus(404);
  } catch (IOException e) {
    log.error("Fatal transport error: " + e.getMessage());
    response.setStatus(404);
  } finally {
    method.releaseConnection();
  }  

%>





<p><br></p>


<!-- <p>
<c:if test="${stripeResult != null}">
<br />-Total matches from <strong>research.fhcrc.org: </strong><c:out value="${fn:length(stripeResult.hits)}"/>.
</c:if>
<c:if test="${publicResult != null}">
<br />-Total matches from  <strong>www.fhcrc.org: </strong><c:out value="${fn:length(publicResult.hits)}"/>.
</c:if>
</p>-->

 
<ul class="nav nav-tabs" id="myTab">
    <li class="active">
        <a href="#stripe" id="str" data-toggle="tab" style="color: #8E8E8E">Results from Research.fhcrc.org (${fn:length(stripeResult.hits)})</a>
    </li>
    <li>
        <a href="#public" id="pub" data-toggle="tab" style="color: #8E8E8E">Results from www.fredhutch.org (${fn:length(publicResult.hits)})</a>
    </li>
</ul>
</div> 
    <div class="tab-content">
    <div class="tab-pane" id="public">
      <%
      long numberOfPages1 = 0;
      int counter1 = 0;
      Boolean publicTruncated = false;
      int maxpublic = 100; // be sure to make this a multiple of # items per page (10)
      Boolean publicpageIsOpen = false; // safety check to see if we need to close (</div>) the current page.
                                        // i noticed that sometimes you do not iterate over every last element of the found set.

                if(publicResult != null)
                {
                int pageNum1 = 0;
                long totalMatches1 = publicResult.getTotalMatches();
                numberOfPages1 = (totalMatches1 / 10)+1;
                boolean publicflag = true;
                for (Hit hit : publicResult.getHits())
                {   
                Page stripePage = hit.getResource().getParent().adaptTo(Page.class);
                Date pubdate = null;
                if(stripePage != null)
                {
                    pubdate = stripePage.getProperties().get("pubDate/date", stripePage.getProperties().get("cq:lastModified", Date.class));    
                }
                counter1 = counter1 + 1;
                if(counter1>maxpublic){ // do not display more than maxpublic results
                    publicTruncated = true;
                    numberOfPages1 = (maxpublic / 10) + 0;
                    //continue;
                }

                
                if(publicflag)
                //if(counter1%10 == 0 || counter1==1)                	
                {
                    pageNum1 = pageNum1 + 1;
            %>
                    <div id="public<%=pageNum1%>" class="publiccontent">
             <%
                    publicflag = false;
                    publicpageIsOpen = true;
                }
             %>

             <% if(!publicTruncated){ %>
               <div><%=hit.getTitle()%></div>
               <div><%=SearchUtil.processExcerpt(hit.getExcerpt()) %></strong></div>
               <div>
                <a href="<%= PageUtil.getLink(slingRequest, hit.getPath(), "public")%>">
                    <%= PageUtil.getLink(slingRequest, hit.getPath(),"public") %>
                </a>
                <% if(pubdate != null) { %>
                    <span><%=FHUtilityFunctions.APStylize(pubdate)%></span>
                <% } %>
                </div>
                <div style="margin-top: 10px;"></div>
             <% } %>

             <%  //out.print("<!--Counter1 = "+counter1+" and totalMatches1 = "+totalMatches1+"-->");
                 if(counter1%10 == 0 || totalMatches1 == counter1 )
                 {                	 
                	if(counter1==maxpublic){
                		out.print("<div><a style='text-decoration:underline' href='http://www.fredhutch.org/en/util/search.html?q="+ URLEncoder.encode(searchCriteria,"UTF-8") +"'><strong>View "+ (totalMatches1-maxpublic) + " more results</strong> at www.fredhutch.org</a> &raquo;</div>"); 
                	}
             %>
                    </div>
             <%
                 publicflag = true;
                 publicpageIsOpen = false;
                 }
              }
             }
             if(publicpageIsOpen){ out.print("</div>"); } //this is the cheat: for some reason the last hit might not equal the total number of hits on publisher. 
             if(counter1 == 0) 
             {
            %>
            <div>
            Your search - <%=StringEscapeUtils.escapeHtml(searchCriteria)%> - did not match any documents.<br>
            No pages were found containing "<%=StringEscapeUtils.escapeHtml(searchCriteria)%>". 
            </div>
            <%
             }
            %>
            <div id="public-page-selection"></div>
        </div>
        <div class="tab-pane active" id="stripe">
              <%
              long numberOfPages = 0;
              int counter = 0;

              if(stripeResult != null)
              {
                  int pageNum = 0;
                  // paging metadata
                  long totalMatches = stripeResult.getTotalMatches();
                  numberOfPages = (totalMatches / 10)+1;
                  boolean flag = true;
                  for (Hit hit : stripeResult.getHits())
                  {  
                  Page stripePage = hit.getResource().getParent().adaptTo(Page.class);
                  Date pubdate = null;
                  if(stripePage != null)
                  {
                      pubdate = stripePage.getProperties().get("pubDate/date", stripePage.getProperties().get("cq:lastModified", Date.class));    
                  }

                  //iterating over the results 
                  counter = counter + 1;
                  if(flag)
                  {
                        pageNum = pageNum + 1;
              %>
                      <div id="content<%=pageNum%>" class="stripecontent">
               <%
                       flag = false;
                  }
               %>
                      <div><%=hit.getTitle()%></div>
                      <div><%=SearchUtil.processExcerpt(hit.getExcerpt())%></strong></div>
                      <div>
                        <a href="<%= PageUtil.getLink(slingRequest, hit.getPath(), "stripe")%>">
                            <%= PageUtil.getLink(slingRequest, hit.getPath(),"stripe") %>
                        </a>
                        <% if(pubdate != null) { %>
                        <span><%=FHUtilityFunctions.APStylize(pubdate)%></span>
                        <% } %>
                      </div>
                      <div style="margin-top: 10px;"></div>
               <% 
                   if(counter%10 == 0 || totalMatches == counter )
                   {
               %>
                      </div>
              <%
                   flag = true;
                   }
               }
              }
              if(counter == 0)
              {
              %>
              <div>
                Your search - <%=StringEscapeUtils.escapeHtml(searchCriteria)%> - did not match any documents.<br>
                No pages were found containing "<%=StringEscapeUtils.escapeHtml(searchCriteria)%>". 
              </div>
              <%
              }
              %>
              <div id="page-selection"></div>
          </div>
<script>
jQuery(document).ready(function() {

    /**
    $('.peoplematch').hide();
    $('.deptmatch').hide();
    $("td:contains(Employee listing) ").each(function(){
        var newText = $(this).html().replace("Employee listing", "");
        $(this).html(newText);
        });

    $("td:contains(Related Departments)").each(function(){
        var newText = $(this).html().replace("Related Departments", "");
        $(this).html(newText);
        });
    $('.email:first').hide();
    
   */
    
    jQuery(function () {
        jQuery('div[class^="publiccontent"]').hide();
        jQuery('div[class^="stripecontent"]').hide();
        //jQuery('div[id^="content1"]').show(); // was matching the 10th, 11th, etc. pages.
        jQuery('div[id="content1"]').show(); // show the first page only, sld sep/01/2013
    })
    
    
    $('#myTab li a').each(function () {
        $(this).click(function (e) {
        var id = $(this).attr('id')
        if(id=='str')
        {
            jQuery('div[class^="publiccontent"]').hide();
            jQuery('div[class^="stripecontent"]').hide();
            jQuery("#public").attr("class", "tab-pane");
            jQuery("#stripe").attr("class", "tab-pane active");
            //jQuery('div[id^="content1"]').show();
            jQuery('div[id="content1"]').show(); // show the first page only, sld sep/01/2013
        }
        if(id=='pub')
        {

            jQuery("#stripe").attr("class", "tab-pane");
            jQuery("#public").attr("class", "tab-pane active");
            jQuery('div[class^="publiccontent"]').hide();
            jQuery('div[class^="stripecontent"]').hide();
            //jQuery('div[id^="public1"]').show();
            jQuery('div[id="public1"]').show(); // show just the first page. sld Sep/01/2013
        }
        });
    });

    $('#page-selection').bootpag({
            total: <%=numberOfPages%>
      }).on("page", function(event, /* page number here */ num){
          //jQuery('div[id^="content"]').hide();
          jQuery('.stripecontent').hide();
          jQuery('#content'+num).show();
      });

    $('#public-page-selection').bootpag({
            total: <%=numberOfPages1%>
      }).on("page", function(event, /* page number here */ num){
          //jQuery('div[id^="content"]').hide();
          jQuery('.publiccontent').hide();
          jQuery('#public'+num).show();
          //jQuery('div#public'+num+".publiccontent").show();
      });
    });  
</script>