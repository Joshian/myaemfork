<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.api.WCMMode,
                 com.day.cq.wcm.foundation.Image,
                 com.day.cq.commons.Doctype" %>
<%
String brandColor = properties.get("color","");
Image image = new Image(resource, "image");
String linkTarget = properties.get("linkTarget","");
Boolean hasLink = !linkTarget.equals("");

Resource r;
if(!linkTarget.equals("")){
    if(!linkTarget.matches("https?:.*")){
        if(linkTarget.matches("^/.*")){
            r = resourceResolver.getResource(linkTarget); //log.info(r.getResourceType());
            if(r!=null && r.isResourceType("cq:Page")){ // added, 2/06/2012 sld
              linkTarget += ".html";
            }
        } else {
            linkTarget = ("http://").concat(linkTarget);
        }
    }
}

//remove title attribute unless there is something explicitly in the dialog
if(properties.get("image/jcr:title","").trim().equals("")) {
    image.setTitle(" ");
}

image.loadStyleData(currentStyle);
image.setSelector(".img"); // use image script
image.setDoctype(Doctype.fromRequest(request));
// add design information if not default (i.e. for reference paras)
if (!currentDesign.equals(resourceDesign)) {
    image.setSuffix(currentDesign.getId());
}

%>

<div class="featureItemContainer<%= brandColor.equals("") ? "" : " ".concat(brandColor) %>">

    <cq:text property="title" tagName="h2" />

    <div class="featureItem_image">
    
<%

    if(hasLink){ %>
        <a href="<%= linkTarget %>" <%= properties.get("externalLink","false").equals("true") ? "target=\"_blank\"" : "" %>>
    <% }
    image.draw(out);
    if(hasLink){ %></a><% }

%>    
    
    </div>
    
    <div class="featureItem_text">
    
        <cq:text property="text" tagClass="featureItem_text_container" />
    
    </div>

</div>