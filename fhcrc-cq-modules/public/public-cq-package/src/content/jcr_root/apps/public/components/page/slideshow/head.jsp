<%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default head script.

  Draws the HTML head with some default content:
  - includes the WCML init script
  - includes the head libs script
  - includes the favicons
  - sets the HTML title
  - sets some meta data

  ==============================================================================

--%><%@include file="/libs/foundation/global.jsp" %><%
%><%@ page import="com.day.cq.commons.Doctype,
                   org.apache.commons.lang.StringEscapeUtils,
                 com.day.cq.security.User,
                 com.day.cq.security.UserManager,
                 com.day.cq.security.UserManagerFactory,
                 org.apache.sling.jcr.api.SlingRepository,
                 com.day.cq.wcm.api.WCMMode,
                 com.day.cq.wcm.api.components.DropTarget,
                 com.day.cq.security.Group,
                 com.day.cq.i18n.I18n,
                 java.util.ResourceBundle,
                 org.apache.sling.settings.SlingSettingsService,
				 java.util.Set" %><%
    String xs = Doctype.isXHTML(request) ? "/" : "";
    String favIcon = currentDesign.getPath() + "/img/favicon.ico";
    if (resourceResolver.getResource(favIcon) == null) {
        favIcon = null;
    }
    /* Set meta robots options. Current options are follow/nofollow and index/noindex */
    String robotsOptions;
    if (properties.get("noindex","false").equals("true")) {
    	
    	if (properties.get("nofollow","false").equals("true")) {
    		
    		robotsOptions = "noindex, nofollow";
    		
    	} else {
    		
    		robotsOptions = "noindex, follow";
    		
    	}
    	
    } else if (properties.get("nofollow","false").equals("true")) {
    	
    	robotsOptions = "index, nofollow";
    	
    } else {
    	
    	robotsOptions = "all";
    	
    }
    
%><head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"<%=xs%>>
    <meta name="keywords" content="<%= StringEscapeUtils.escapeHtml(WCMUtils.getKeywords(currentPage, false)) %>"<%=xs%>>
    <meta name="description" content="<%= StringEscapeUtils.escapeHtml(properties.get("jcr:description", "")) %>"<%=xs%>>
    <meta name="robots" content="<%= robotsOptions %>"<%=xs%>>
    
    <!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <%-- headlibs.jsp provides four items:
         
         <script type="text/javascript" src="/etc/clientlibs/foundation/jquery.js"></script>
         <script type="text/javascript" src="/etc/clientlibs/foundation/main.js"></script>
         <link rel="stylesheet" href="/etc/clientlibs/foundation/main.css" type="text/css"/>
         <link href="/etc/designs/public.css" rel="stylesheet" type="text/css"/>
         
         however we don't need all four. therefore i'll comment out headlibs.jsp 
         and just include what i want.
         
         ok, i realize now that cq's query.js (1.4) is necessary because their entire
         authoring is based on it...so let's just leave this as-is.    --%>
    <cq:include script="headlibs.jsp"/>
    <%--<script type="text/javascript" src="/etc/clientlibs/foundation/main.js"></script>
    <link rel="stylesheet" href="/etc/clientlibs/foundation/main.css" type="text/css"/>--%>
     
    <cq:includeClientLib css="apps.public.slideshow"/>

    <%-- webfonts must include their licensing within the css comments, so factor those out separately: --%>
    <link rel="stylesheet" href="<%=currentDesign.getPath()%>/clientlibs/css/webfonts-geogrotesque.css"/>

    <%-- do not launch the sidekick when the author is in public-reviewer group --%>
    <%
       Session adminSession = null;
       try {
   	     // only want to do this when we are in author, not publish:
   	     Set<String> runModes =  sling.getService(SlingSettingsService.class).getRunModes();
		 log.debug("RUNMODES = " + runModes);
		 if (runModes.contains("author")){
            boolean isReviewer = false;
            User currentUser = resourceResolver.adaptTo(User.class); 
            SlingRepository repo = sling.getService(SlingRepository.class);
            adminSession = repo.loginAdministrative(null);
            UserManager userManager = sling.getService(UserManagerFactory.class).createUserManager(adminSession);
            Group group = null;
            if(userManager.hasAuthorizable("public-reviewer")){
              group = (Group) userManager.get("public-reviewer"); 
              isReviewer = group.isMember(currentUser); 
              log.info("Public reviewer group exists. Current user " + (isReviewer?"IS":"IS NOT") + " a member of " + group.getID());
            } else {
              log.info("Public reviewer group does NOT exist. Check to be sure this is correct.");
            }
            //-------------------------------------------------------
            // If user is NOT a member of the reviewer group,  
            // permit launch of the sidekick.  
            //-------------------------------------------------------
            if(!isReviewer){%>
                <cq:include script="/libs/wcm/core/components/init/init.jsp"/>
            <%}
    	 }
       } catch (Exception ex) {
    	   log.error("Exception", ex);
       } finally {
    	    if (adminSession != null) {
    	        adminSession.logout();
    	    }    	   
       }
    %>
    
    <%-- cq stats are included by this next statement. this adds google analytics, click tracking
         and mvt impression tracking, so re-enable when/if you add in those features: --%>
    <%--<cq:include script="stats.jsp"/>--%>
    
    <%-- until we choose to go back to stats.jsp, use our own custom google analytics code.
         note that you may wish to continue to use our own tracking, because it includes the
         ability to send user session to secure2.convio.net for cross-domain tracking. --%>
    <cq:include script="google-analytics.jsp"/>

    <% if (favIcon != null) { %>
    <link rel="icon" type="image/vnd.microsoft.icon" href="<%= favIcon %>"<%=xs%>>
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<%= favIcon %>"<%=xs%>>
    <% } %>
    <title><%= currentPage.getTitle() == null ? currentPage.getName() : currentPage.getTitle() %></title>

</head>