/**
 * SearchUtil class has helper methods to put the search results in the respective buckets.
 * @author sbojja
 * 
 */
package org.fhcrc.stripe.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.foundation.Search;
import com.day.cq.wcm.foundation.Search.Hit;
import com.day.cq.wcm.foundation.Search.Result;

/**
 * @author sbojja
 *
 */
public class SearchUtil{
	
	private static final Logger log = LoggerFactory.getLogger(SearchUtil.class);
	
	private List<Hit> publicResults = new ArrayList<Hit>();
	private List<Hit> labResults = new ArrayList<Hit>();
	private List<Result> labPaging = new ArrayList<Result>();
	public List<Result> getLabPaging() {
		return labPaging;
	}

	public void setLabPaging(List<Result> labPaging) {
		this.labPaging = labPaging;
	}

	public List<Result> getPublicPaging() {
		return publicPaging;
	}

	public void setPublicPaging(List<Result> publicPaging) {
		this.publicPaging = publicPaging;
	}


	private List<Result> publicPaging = new ArrayList<Result>();

	
	public List<Hit> getPublicResults() {
		return publicResults;
	}       

	public void setPublicResults(List<Hit> publicResults) {
		this.publicResults = publicResults;
	}

	public List<Hit> getLabResults() {
		return labResults;
	}

	public void setLabResults(List<Hit> labResults) {
		this.labResults = labResults;
	}


	public void setResultsInBuckets(Search search)
	{
		try
		{
			//Result searchResults
			//if(searchResults != null && searchResults.size()>0)
			//{
				//for(Result result: searchResults)
				//{
					int counter = 0;					
					for(Hit hit: search.getResult().getHits())
					{
						counter = counter + 1;
						if(hit.getURL().contains("/content/public"))
						{
							publicResults.add(hit);
							publicPaging.add(search.getResult());
						}
						else if(hit.getURL().contains("/content/stripe"))
						{
							labResults.add(hit);
							publicPaging.add(search.getResult());
						}
						else
						{
							log.info("There are other results that are not part of Public and Lab related");
						}
					}
					
				//}
			//}
		}
		catch (Exception exception) 
		{
			log.error("There is an expcetion in setResultsInBuckets method :" + exception);
		}
	}
	
	public static SearchResult getSearchResults(SlingHttpServletRequest slingRequest, 
			Resource resource, String searchCriteria, String searchPath) throws RepositoryException
	{
	    SearchResult result =null;    
		try
		{
		    Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
		    QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);
		    Query query =null;   
			
			Map<String, String> map = new HashMap<String, String>();
	
			map.put("path", searchPath);
			map.put("type", "nt:hierarchyNode");
		    // QUERY IN THE LAB
			map.put("group.p.or", "true"); // combine this group with OR
			
			
			//map.put("group.1_path", searchPath);
			//map.put("group.2_path", "/dam/public");
			map.put("group.0_fulltext", searchCriteria);
			map.put("group.0_fulltext.relPath", "jcr:content");
			map.put("group.1_fulltext", searchCriteria);
			map.put("group.1_fulltext.relPath", "jcr:content/@jcr:title");
			map.put("group.2_fulltext", searchCriteria);
			map.put("group.2_fulltext.relPath", "jcr:content/@jcr:description");
		    map.put("group.3_fulltext",searchCriteria);
		    map.put("group.3_fulltext.relPath", "@cq:tags");
		    map.put("orderby", "@jcr:score");    
		    map.put("orderby.sort", "desc");
		    

	
		    //map.put("tagsid.property", "jcr:content/cq:tags");
		    //map.put("tagsid.tagid", searchCriteria);
	
		    
		      map.put("p.offset", "0"); // same as query.setStart(0) below
		      // Get everything since the QueryBuilder doesn't support QueryBuilder 
		      map.put("p.limit", "10000"); 
		        
		      query = builder.createQuery(PredicateGroup.create(map),session);
		      result = query.getResult();//QUERYING JCR
		}
		catch (Exception exception) 
		{
			log.error("There is an issue while searching..." + searchCriteria + "...."
					+ searchPath + "...." + exception);
		}
	      return result;
	}
	
	public static String processExcerpt(String excerpt)
	{
		String processedString = excerpt;
		if(excerpt !=null && excerpt.length()>1)
		{
			if(excerpt.contains("<a"))
			{
				processedString = excerpt.substring(0, excerpt.indexOf("<a"));	
			}
		}
		return processedString;
	}
	
}