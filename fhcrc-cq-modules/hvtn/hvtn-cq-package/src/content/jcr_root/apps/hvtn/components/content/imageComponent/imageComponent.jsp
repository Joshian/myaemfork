<%@include file="/libs/foundation/global.jsp"%>
<jsp:useBean id="imageBean" scope="request" class="org.fhcrc.hvtn.components.ImageComponent" />
<jsp:setProperty name="imageBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="imageBean" property="resource" value="<%= resource %>"/>
<jsp:setProperty name="imageBean" property="currentStyle" value="<%= currentStyle %>"/>
<jsp:setProperty name="imageBean" property="currentDesign" value="<%= currentDesign %>"/>
<jsp:setProperty name="imageBean" property="resourceDesign" value="<%= resourceDesign %>"/>
<jsp:setProperty name="imageBean" property="request" value="<%= request %>"/>
<jsp:setProperty name="imageBean" property="writer" value="<%= out %>"/>
<jsp:setProperty name="imageBean" property="componentProperties" value="<%= properties %>"/>

<div class="${ imageBean.wrapperClass }">

	<div class="imageContainer">
	
		<c:choose>
			<c:when test="${!empty imageBean.linkTarget }">
				<c:choose>
					<c:when test="${ imageBean.externalLink }">
						<a href="${ imageBean.linkTarget }" target="_blank">
					</c:when>
					<c:otherwise>
						<a href="${ imageBean.linkTarget }">
					</c:otherwise>
				</c:choose>
				<% imageBean.drawImage(); %></a>
			</c:when>
			<c:otherwise>
				<% imageBean.drawImage(); %>
			</c:otherwise>
		</c:choose>
		
	</div>

	<c:choose>
		<c:when test="${!empty imageBean.hiResSrc || imageBean.captionOrCredit }">

			<div class="captionAndCredit">
			
				<cq:text property="jcr:caption" tagName="span" placeholder="" tagClass="caption" />
        		<cq:text property="credit" tagName="span" placeholder="" tagClass="credit" />
				
				<c:if test="${!empty imageBean.hiResSrc }">
					<span class="hiRes"><a href="${ imageBean.hiResSrc }" target="_blank">Click for high-res version</a></span>
				</c:if>
				
			</div>	
	
		</c:when>
	</c:choose>

</div>

<div class="clear"></div>

<cq:includeClientLib js="apps.hvtn.textimage"/>