<%@include file="/libs/foundation/global.jsp"%><%
    StringBuffer cls = new StringBuffer();
    for (String c: componentContext.getCssClassNames()) {
        cls.append(c).append(" ");
    }
    if(properties.get("isRightCol","false").equals("true")){
    	cls.append("with_right_column").append(" ");
    }
    if(properties.get("omitSidebar","false").equals("false")){
        cls.append("with_sidebar").append(" ");
    }
%>

<body class="<%= cls %>">
<cq:include script="redirect-notice.jsp"/>

  <a href="#page_content" title="Jump to content"></a>
  <a href="#global_nav" title="Jump to site-wide navigation"></a>
  <a href="#section_navigation" title="Jump to navigation for this section"></a>

  <cq:include script="content.jsp"/>
  <cq:include script="chrome.jsp"/>

  <!-- global non-blocking js last to avoid blocking -->
  <script src="<%= currentDesign.getPath() %>/clientlibs/js/site-nonblocking.js" type="text/javascript"></script>
  
</body>