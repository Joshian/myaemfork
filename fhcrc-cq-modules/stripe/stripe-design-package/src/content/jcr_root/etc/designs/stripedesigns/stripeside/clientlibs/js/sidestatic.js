
/** 
 * Custom JS code for the STRIPE project - Side Design
 */

/* ----------sticky footer ------------------*/

jQuery(document).ready(fixFooter());


function fixFooter() {
	   
	   var docHeight = jQuery(window).height();
	   var footerHeight = jQuery('#footer').height();
	   var footerTop = jQuery('#footer').position().top + footerHeight;
	   var sidebarHeight = jQuery('#sidenv').height();
	   //alert(sidebarHeight);
	   if (footerTop < docHeight) {
	    //jQuery('#footer').css('padding-top', 10 + (docHeight - footerTop) + 'px');
	    jQuery('#sidenv').css('height', sidebarHeight + 20 + (docHeight - footerTop) + 'px');
	   }
}
