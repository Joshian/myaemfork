<%@include file="/libs/foundation/global.jsp"%>
<jsp:useBean id="slideBean" scope="request" class="org.fhcrc.publicsite.pageComponents.Slide" />
<jsp:setProperty name="slideBean" property="currentPage" value="<%= currentPage %>"/>
<jsp:setProperty name="slideBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="slideBean" property="resource" value="<%= resource %>"/>
<jsp:setProperty name="slideBean" property="currentStyle" value="<%= currentStyle %>"/>
<jsp:setProperty name="slideBean" property="currentDesign" value="<%= currentDesign %>"/>
<jsp:setProperty name="slideBean" property="resourceDesign" value="<%= resourceDesign %>"/>
<jsp:setProperty name="slideBean" property="request" value="<%= request %>"/>
<jsp:setProperty name="slideBean" property="writer" value="<%= out %>"/>
<jsp:setProperty name="slideBean" property="componentProperties" value="<%= properties %>"/>

<!-- TITLE BAR -->

	<div class="titlebar">
  
  	<div class="titlebar_container outer_grid_container table">
    
    	<div class="table_row">
    
        <div class="title_container table_cell">
        
          <cq:include path="title" resourceType="foundation/components/title"/>
        
        </div>
        
        <div class="controls_container table">
        
        	<div class="table_row">
        
            <img id="left_arrow" class="control_arrow disabled table_cell" src="<%= currentDesign.getPath() %>/img/slideshow/slideshow_arrows_sm_left_gray.png">
            <span class="controls_text table_cell"><span id="current_slide">1</span>/<span id="total_slides">1</span></span>
            <img id="right_arrow" class="control_arrow table_cell" src="<%= currentDesign.getPath() %>/img/slideshow/slideshow_arrows_sm_right.png">
            
          </div>
        
        </div>
        
        <div class="sharing_container table_cell">
        
        <!-- Lockerz Share BEGIN -->
      	<span class="share_options">
        	<a class="a2a_dd" href="http://www.addtoany.com/share_save">
	    		<img src="<%= currentDesign.getPath() %>/img/buttons/plus_icon_static.png" class="rolloverImage" 
	    		data-hover="<%= currentDesign.getPath() %>/img/buttons/plus_icon_hover.png"><span class="share_link_text">Share</span>
	    	</a>
	    </span>
	    <script type="text/javascript">
	      <!--
	        a2a_config = {};
	        a2a_config.onclick=1;
	        a2a_config.show_title=0;
	        a2a_config.num_services=10;
	        /*a2a_config.linkname=document.title;
	        a2a_config.linkurl=location.href;*/
	        a2a_config.prioritize=["facebook","twitter","linkedin","digg","delicious","myspace","read_it_later","squidoo","technorati_favorites","care2_news"];
	        /*a2a_config.color_main="D7E5ED";*/
	        /*a2a_config.color_border="AECADB";*/
	        a2a_config.color_link_text="123054";
	        a2a_config.color_link_text_hover="24548d";
	      //-->
	    </script>
	    <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
	    <!-- Lockerz Share END -->   
        
        </div>
        
      </div>
    
    </div>
    
  </div>
  
<!-- END TITLE BAR -->