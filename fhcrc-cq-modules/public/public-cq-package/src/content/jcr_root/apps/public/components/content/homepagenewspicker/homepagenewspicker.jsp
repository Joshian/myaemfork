<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.fhcrc.publicsite.constants.Constants,com.day.cq.wcm.api.WCMMode,java.text.SimpleDateFormat,java.text.DateFormat,java.util.Date,
com.day.cq.commons.RangeIterator, org.apache.commons.lang.StringEscapeUtils, java.util.TreeSet,java.util.Comparator,java.util.Iterator, org.fhcrc.tools.FHUtilityFunctions" %>

<% // changed to pick two articles (was pick three) for be breakthrough home page change, Nov 2012.
   // if you want to return to three just uncomment two lines below, and create article3 element 
   // in the dialog. Sld Nov 2, 2012. 
%>

<h3 class="underline">News</h3> 

<% WCMMode mode = WCMMode.fromRequest(request);
      
   String internalPagePath1 = properties.get("article1", "");
   String internalPagePath2 = properties.get("article2", "");
   //String internalPagePath3 = properties.get("article3", "");
   
   String newsSource = "";
   String newsSourceLink = "";
   
   Date defaultDate = new Date();
   DateFormat f = new SimpleDateFormat("MMM d, yyyy");
   
   TreeSet<Page> newsTree = new TreeSet<Page>(new Comparator<Page>(){
        public int compare(Page a, Page b){
            Date dateA = a.getProperties().get("pubDate/date", Date.class);
            Date dateB = b.getProperties().get("pubDate/date", Date.class);
            //Date dateA=new Date();
            //Date dateB=new Date();
            if(dateA==null){
                dateA = new Date();
            }
            if(dateB==null){
                dateB = new Date();
            }
            if(dateA.equals(dateB)){
                return a.getPath().compareTo(b.getPath());
            }
            return dateA.compareTo(dateB);
        }
    });   
   
   
   //String[] newsPaths = {internalPagePath1,internalPagePath2,internalPagePath3};
   String[] newsPaths = {internalPagePath1,internalPagePath2};
   
   for(String s : newsPaths){      
       //Page newsPage = resourceResolver.getResource(s).adaptTo(Page.class);
       if(null!=resourceResolver.getResource(s)){
         Page newsPage = resourceResolver.getResource(s).adaptTo(Page.class);
         if(newsPage!=null && newsPage.getProperties().get("cq:template","").equals("/apps/public/templates/news")){ // I only want to list pages of the "News page" type
           newsTree.add(newsPage);
         }     
       }
   }
   
   Iterator<Page> printIterator = newsTree.descendingIterator(); // For reverse chron
   
   out.print("<div class='related-news'>");
   while(printIterator.hasNext()){
       Page printMe = printIterator.next();
       String pagePath = printMe.getPath();
       if(pagePath.contains(Constants.PATH_TO_NEWSRELEASES_FOLDER)) {
           newsSource = "News Releases";
           newsSourceLink = Constants.PATH_TO_NEWSRELEASES_FOLDER+".html";
       } else if(pagePath.contains(Constants.PATH_TO_CENTERNEWS_FOLDER)) {
           newsSource = "Hutch News";
           newsSourceLink = Constants.PATH_TO_NEWS_FOLDER+".html";
       } else if(pagePath.contains(Constants.PATH_TO_HUTCH_MAG_IMPORTER_FOLDER)) {
           newsSource = "Hutch Magazine";
           newsSourceLink = Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER+".html"; //"http://quest.fhcrc.org/";
       } else if(pagePath.contains(Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER)) {
           newsSource = "Hutch Magazine";
           newsSourceLink = Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER+".html";
       } else if(pagePath.contains(Constants.PATH_TO_PETRIDISHIMPORTER_FOLDER)) {
           newsSource = "Petri Dish";
           newsSourceLink = "http://questmagazine.wordpress.com/";
       } else {
           newsSource = "News";
           newsSourceLink = Constants.PATH_TO_NEWS_FOLDER+".html";
       }
       out.print("<p>");
       out.print("<div class='news-title'>");
       out.print("<a href=\"" + printMe.getPath() + ".html\">");
       out.print(FHUtilityFunctions.displayTitle(printMe));
       out.print("</a></div>");
       
       Date pubdate = printMe.getProperties().get("pubDate/date", printMe.getProperties().get("cq:lastModified", Date.class));
       //Date pubdate = new Date();
       out.print("<span class='news-date'>");
       out.print(FHUtilityFunctions.APStylize(pubdate));
       out.print("</span>");
       out.print("<span class='news-source'>");
       out.print(" in <a href='" + newsSourceLink + "'>" + newsSource + "</a>");
       out.print("</span>");
       
       out.print("</p>");
   }
   out.print("</div>");

   out.print("<p style='text-align:right;margin-right:20px;'><a href='"+Constants.PATH_TO_NEWS_FOLDER+".html'>More</a>&nbsp;&gt;</p>");
   
   
   
          


%>