<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode" %><%
%><%@include file="/apps/stripe/global.jsp"%><%
    Image image = new Image(resource);

    //drop target css class = dd prefix + name of the drop target in the edit config
    image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
    image.loadStyleData(currentStyle);
    image.setSelector(".img"); // use image script
    image.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        image.setSuffix(currentDesign.getId());
    }
    String divId = "cq-image-jsp-" + resource.getPath();
    pageContext.setAttribute("imagePath", (String)properties.get("fileReference",String.class));
    
    if(image.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT) {
    
    %><div id="<%= divId %>" class="herobanner-inner"><% image.draw(out); %></div><%
    
    }
    
    %>
    <jsp:useBean id="AssetUtil" scope="request" class="org.fhcrc.stripe.utilities.AssetUtil" />
    <c:set var="imageDescription" value="<%=AssetUtil.getAssetProperty(slingRequest,(String)pageContext.getAttribute("imagePath"),"dc:description")%>"/>    
    <c:if test="${imageDescription != ''}" >
        <!-- <div class="transparent_description">
            <p>                       
               <c:out value="${imageDescription}" />
            </p>
        </div>
         -->
    </c:if>
    
    <cq:text property="jcr:description" placeholder="" tagName="small" escapeXml="true"/>
    