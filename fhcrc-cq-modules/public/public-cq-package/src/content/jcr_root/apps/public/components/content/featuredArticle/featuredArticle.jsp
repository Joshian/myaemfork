<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.foundation.Image,
                 com.day.cq.commons.Doctype,
                 java.util.Date,
                 java.util.Calendar,
                 org.fhcrc.publicsite.constants.Constants,
                 org.fhcrc.tools.FHUtilityFunctions" %>
<%
String[] articles = properties.get("articles",String[].class);
String displayArticle = "";
if(articles != null && articles.length > 0){
/* Run through the list of articles and pull the first valid News Page to be displayed. 
A page is valid if it has content and the on/off time spans the current time */ 
    for(String a : articles){
    	
    	String templateName = resourceResolver.getResource(a).adaptTo(Page.class).getProperties().get("cq:template","");
    	
        if(resourceResolver.getResource(a).adaptTo(Page.class).isValid() && (templateName.contains("apps/public/templates/news") || templateName.contains("apps/public/templates/slideshow"))){
            displayArticle = a;
            break;
        }
    }
    if(!displayArticle.trim().equals("")){
        Page thePage = resourceResolver.getResource(displayArticle).adaptTo(Page.class);
        Resource r = thePage.getContentResource("image");
        boolean omitImage = properties.get("omitImage",Boolean.FALSE);
        if(r!=null){
            Image imageCheck = new Image(r);
            if(!imageCheck.hasContent()) {
                omitImage = true;
            }
        } else {
            omitImage = true;
        }
        String layout = properties.get("layout","standard");
%>
<div class="featuredArticleContainer<%= omitImage ? " noimage" : "" %><%= layout.equals("compact") ? " vertical" : "" %>">
    <!-- included header widget -->
    <cq:include path="includedHeader" resourceType="/apps/public/components/content/header" />
    <!-- end included header widget -->
    <div class="articlePhoto">
<%      
        if (r != null && !omitImage) {
            String img;
            Image image = new Image(r);
            img = thePage.getPath() + ".img.png" + image.getSuffix(); // e.g.,: /content/public/en/events/events/hutch-award.img.png/1323194909406.gif
            // prepend context path to img
            img = request.getContextPath() + img;
            img = resourceResolver.map(img);
            out.print("<a href='"+ thePage.getPath() +".html'>");
            out.print("<img src='"+img+"' alt=' ' title=' '>");
            out.print("</a>");
        }
%>        
    </div>
<%  
        String displayText = thePage.getDescription();
        String displayTitle = thePage.getPageTitle();
        Date displayDate = thePage.getProperties().get("pubDate/date",Date.class);
        
        if (displayDate == null) {
        	
        	displayDate = thePage.getLastModified().getTime();
        	
        	if (displayDate == null) {
        		
        		displayDate = Calendar.getInstance().getTime();
        		
        	}
        	
        }
        
        String newsSource = "";
        String pagePath = thePage.getPath();

/*      
        if(displayText==null || displayText.trim().equals("")){
            displayText = "THERE IS NO DESCRIPTION METADATA FOR THIS ARTICLE.";
        }
*/
        if(displayTitle==null || displayTitle.trim().equals("")){
            if(thePage.getTitle() != null && !thePage.getTitle().trim().equals("")){
                displayTitle = thePage.getTitle();
            } else {
                displayTitle = thePage.getName();
            }
        }
        if(pagePath.contains(Constants.PATH_TO_CENTERNEWS_FOLDER)){ newsSource = "<a href='"+Constants.PATH_TO_NEWS_FOLDER+".html'>Hutch News</a>"; }
        else if(pagePath.contains(Constants.PATH_TO_PETRIDISHIMPORTER_FOLDER)){ newsSource = "<a href='http://questmagazine.wordpress.com' target='_blank'>Petri Dish</a>"; } 
        else if(pagePath.contains(Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER)||pagePath.contains(Constants.PATH_TO_HUTCH_MAG_IMPORTER_FOLDER)){ newsSource = "<a href='"+ Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER+".html'>Hutch Magazine</a>"; } 
        else if(pagePath.contains(Constants.PATH_TO_NEWSRELEASES_FOLDER)){ newsSource = "<a href='"+ Constants.PATH_TO_NEWSRELEASES_FOLDER+".html'>News Releases</a>"; } 
        else if(pagePath.contains(Constants.PATH_TO_SCIENCESPOTLIGHT_FOLDER)){ newsSource = "<a href='"+ Constants.PATH_TO_SCIENCESPOTLIGHT_FOLDER+".html'>Science Spotlight</a>"; } 
        else {
        	
        	newsSource = "<a href='"+Constants.PATH_TO_NEWS_FOLDER+".html'>Hutch News</a>";
        	
        }

        boolean omitSource = properties.get("omitSource",Boolean.FALSE);
        
%>
        <div class="headline"><a href="<%= pagePath %>.html" class="articleTitle"><%= displayTitle %></a></div>
        <div class="abstract"><%= displayText==null ? "" : displayText %></div>
        <div class="related-news"><div class="news-date"><%= FHUtilityFunctions.APStylize(displayDate) %> <% if(!omitSource){ %>in <%= newsSource %><% } %></div></div>
    <div class="clear"></div>
</div>
<script type="text/javascript">
<!--
  $(document).ready( function(){
    // loop over all the other news items following a preset pattern on this page and hide if they point to the same URL
    $("div.related-news .news-title").filter( function(index){
      return $('a',this).attr('href')=="<%= pagePath %>.html";
    }).parent().hide(); 
  });
  //-->
 </script>
<%
    } else {
%>
    <div class="cq-edit-only">None of the <%= articles.length %> articles you selected is currently valid. Please add a currently valid article or modify the On/Off Times of a selected article.</div>
<%
    }
} else {
%>
<cq:include script="empty.jsp" />
<% } %>
