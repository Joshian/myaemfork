<%@include file="/apps/stripe/global.jsp" %>
<%@page import="com.day.cq.dam.api.Asset,
                org.fhcrc.stripe.utilities.AssetUtil"%>

<jsp:useBean id="numberedListBean" scope="request" class="org.fhcrc.stripe.components.NumberedList" />
<jsp:setProperty name="numberedListBean" property="componentProperties" value="<%= properties %>"/>

 <!-- Numbered List Items -->
 <div>
    <c:set var="counter" value="0"/>
    <c:forEach items="${numberedListBean.textItems}" var="textItems" varStatus="loop">
        <c:set var="orderedNumber" value="${numberedListBean.startNumber+counter}"/>   
        <div style="width: ${numberedListBean.columnWidth}px;">
            <div style="width: 15px; float: left;"><c:out value="${orderedNumber}"/>.</div>
            <div style="display: inline; width: ${numberedListBean.columnWidth-30}px; float: left; margin-left: 1em; padding-bottom:5px;">
                <c:out value="${textItems}"/>
            </div>
        </div>    
        <c:set var="counter" value="${counter+1}"/>
    </c:forEach>
</div>