<%@include file="/apps/stripe/global.jsp" %>

<jsp:useBean id="footerBean" scope="request" class="org.fhcrc.stripe.components.Footer" />
<jsp:setProperty name="footerBean" property="page" value="<%= currentPage %>"/>
<jsp:setProperty name="footerBean" property="componentProperties" value="<%= properties %>"/>
<jsp:setProperty name="footerBean" property="designProperties" value="<%= currentStyle %>"/>
<jsp:setProperty name="footerBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<%
      String title = properties.get("header", footerBean.getInheritedProperty(currentPage,"footer","header"));
      String location = properties.get("location", footerBean.getInheritedProperty(currentPage,"footer","location"));
//      String copyRight = footerBean.getInheritedProperty(currentPage,"footer","copyright");
      String button1_text = properties.get("button1_text", footerBean.getInheritedProperty(currentPage,"footer","button1_text"));
      String button2_text = properties.get("button2_text", footerBean.getInheritedProperty(currentPage,"footer","button2_text"));
      String button1URL = properties.get("button1URL", footerBean.getInheritedProperty(currentPage,"footer","button1URL"));
      String button2URL = properties.get("button2URL", footerBean.getInheritedProperty(currentPage,"footer","button2URL"));
      String showButton1 = properties.get("showButton1", footerBean.getInheritedProperty(currentPage,"footer","showButton1"));
      String showButton2 = properties.get("showButton2", footerBean.getInheritedProperty(currentPage,"footer","showButton2"));
      
      String facebook = properties.get("facebook", footerBean.getInheritedProperty(currentPage,"footer","facebook"));
      String twitter = properties.get("twitter", footerBean.getInheritedProperty(currentPage,"footer","twitter"));
      String youtube = properties.get("youtube", footerBean.getInheritedProperty(currentPage,"footer","youtube"));
      
      if(showButton1.isEmpty())
      {
    	  showButton1 = "DONATE";
      }
      
      if(showButton2.isEmpty())
      {
    	  showButton2 = "CenterNet";
      }
      
      if(facebook.isEmpty())
      {
    	  facebook = "https://www.facebook.com/HutchinsonCenter";
      }
      
      if(twitter.isEmpty())
      {
    	  twitter = "https://twitter.com/fredhutch";
      }
      
      if(youtube.isEmpty())
      {
    	  youtube = "https://www.youtube.com/channel/UCnWBh22UKMHF4VRKdsQvZqg/featured";
      }

      request.setAttribute("button1_text", button1_text);
      request.setAttribute("button2_text", button2_text);
      request.setAttribute("showButton1", showButton1);
      request.setAttribute("showButton2", showButton2);
%>
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="footer-text span7" >
                <p>
                    <font class="footer-page-title"><%=title%></font><br/>             
                    <font class="footer-location"><%=location%></font><br/>
                    <font class="footer-copyright"><c:out value="${ footerBean.copyright }"/></font>
                </p>
            </div>
            <div class="span5 footer-features" style="white-space: nowrap;">
                <div class="footer-logo"><img src="/etc/designs/stripedesigns/stripe/clientlibs/img/fhcrc-footer-logo.png" alt="" border="0" /></div>            
                <div class="footer-icons"> 
                    <ul class="socialicons color">
                        <li><a class="facebook" href="<%=facebook%>" target="_blank" alt="Facebook"></a></li>
                        <li><a class="twitter" href="<%=twitter%>" target="_blank" alt="Twitter"></a></li>
                        <li><a class="youtube" href="<%=youtube%>" target="_blank" alt="Youtube"></a></li>  
                    </ul>
                     <c:if test="${showButton1 eq 'true'}">
                        <a class="btn btn-login btn-large" href="<%=button1URL%>" target="_blank"><c:out value="${button1_text}"/></a>
                     </c:if>
                     <c:if test="${showButton2 eq 'true'}">
                        <a class="btn btn-login btn-large" href="<%=button2URL%>" target="_blank"><c:out value="${button2_text}"/></a>
                     </c:if>
                </div>
            </div>
        </div>
    </div>
    <div class="container footer-colorbar">
        <br/>
    </div>
</div>