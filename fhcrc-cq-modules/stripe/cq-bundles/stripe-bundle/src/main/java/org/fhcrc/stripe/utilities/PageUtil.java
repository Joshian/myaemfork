/**
 * PageUtil class is in charge of every common functionality related to pages
 *  
 */

package org.fhcrc.stripe.utilities;

import javax.jcr.Node;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;

public class PageUtil {
		
    private Page currentPage;
	private static final Logger log = LoggerFactory.getLogger(PageUtil.class);

	public static final String DEFAULT_TITLE = "ENGLISH";
	public static final String DEFAULT_VALUE = "";
	ResourceResolver resourceResolver;



	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}


	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}


	public PageUtil()
	{
	}
	
	
/** Getters and setters **/
    
	public Page getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}

	
/** Methods **/	

	public static String getPageTitle(Page page, String defaultValue){
		if (page == null){
			return defaultValue;
		}		
		String pageTitle = page.getPageTitle();
		if (pageTitle == null || pageTitle.equals("")){
			pageTitle = page.getTitle();
			if (pageTitle == null || pageTitle.equals("")){
				pageTitle = page.getNavigationTitle();
				if (pageTitle == null || pageTitle.equals("")){
					return defaultValue;
				}
			}
		}
		return pageTitle;
	}
	
	public String getCurrentPageDescription(){
		String desc = "";
		if (currentPage != null){
			desc = currentPage.getDescription();
		}
		return desc;
	}
	

	public String getInheritedProperty(Page page, String componentName, String propertyName)
	{
	     String value = "";
	     Page root = page;
	     while (value.equals(DEFAULT_VALUE)){
	         if (root.getParent()!=null){
	              root = root.getParent();
	              Resource parentResource = resourceResolver.getResource(root.getPath()+ "/jcr:content/" + componentName);
	              if(parentResource != null)
	              {
	            	  try{
		            	  Node parentNode = parentResource.adaptTo(Node.class);
		            	  value = (String)parentNode.getProperty(propertyName).getString();
		            	  break;
	            	  }
	            	  catch(Exception exception){
	    	              log.error("ERROR GETTING DIVISION TITLE []", exception);
	            	  }
	              }
	         }
	         else{
	        	 break;
	         }
	     }
	     if (value.equals("")){
	    	 value = getPageTitle(page,"DEFAULT TITLE");
	     }
	     return value;
	}

	/** 
	 * This method returns config property value that is set at the site level.
	 * @param request
	 * @param nodePath
	 * @param propertyName
	 * @return propertyValue
	 */
	public static String getSiteConfigProperty(SlingHttpServletRequest request,String nodePath, String propertyName)
	{
		 String propertyValue = DEFAULT_VALUE;
		 Resource res =  request.getResourceResolver().getResource(nodePath);
	     if(res != null)
	     {
	    	 try
	    	 {
	    		 Node parentNode = res.adaptTo(Node.class);
	    		 if( parentNode.hasProperty(propertyName) ){ // added, sld Nov 11, 2013
	    		   propertyValue = parentNode.getProperty(propertyName).getString();
	    		 } else {
	    			 log.warn("Property missing or null. Author has not set site config property for: " + propertyName + ". Using default.");
	    		 }
	    	 }
	    	 catch(Exception exception)
	    	 {
	              log.error("ERROR GETTING SITE CONFIG PROPERTY : ", propertyName, exception);
	    	 }
         }
	     return propertyValue;
	}
	
	/**
	 * This method build's the url for based on the environment. 
	 * @param request
	 * @param link
	 * @return
	 */
	public static String getNewsLink(SlingHttpServletRequest request, String link)
	{
		String newsUrl = "";
		// We need /content/public when running tests on localhost
		String editedLink = link.replace("/content/public", "");
		if(request.getServerName().contains("localhost"))
		{
			newsUrl = "http://localhost".concat(":").concat(String.valueOf(request.getServerPort())).concat(link).concat(".html");
		}
		else if(request.getServerName().contains("sandbox"))
		{
			newsUrl = "https://research-sandbox.fhcrc.org".concat(editedLink).concat(".html");
		}
		else if(request.getServerName().contains("qa"))
		{
			newsUrl = "https://www-qa.fhcrc.org".concat(editedLink).concat(".html");
		}
		else 
		{
			newsUrl = "https://www.fredhutch.org".concat(editedLink).concat(".html");
		}
		return newsUrl;
	}
	
	
	/**
	 * This method build's the url for based on the environment. 
	 * @param request
	 * @param link
	 * @return
	 */
	public static String getLink(SlingHttpServletRequest request, String link, String linkType)
	{
		String newsUrl = "";
		// We need /content/public when running tests on localhost
		String editedLink = ""; 
		if(linkType.equals("public"))
		{
			editedLink = link.replace("/content/public", "");
		}
		else if(linkType.equals("stripe"))
		{
			editedLink = link.replace("/content/stripe", "");
		}
		
		if(request.getServerName().contains("localhost"))
		{
			newsUrl = "http://localhost".concat(":").concat(String.valueOf(request.getServerPort())).concat(link).concat(".html");
		}
		else if(request.getServerName().contains("sandbox"))
		{
			if(linkType.equals("public")){ newsUrl = "http://www-sandbox.fhcrc.org".concat(editedLink).concat(".html"); }
			if(linkType.equals("stripe")){ newsUrl = "http://research-sandbox.fhcrc.org".concat(editedLink).concat(".html"); }
		}
		else if(request.getServerName().contains("qa"))
		{
			if(linkType.equals("public")){ newsUrl = "http://www-qa.fhcrc.org".concat(editedLink).concat(".html"); }
			if(linkType.equals("stripe")){ newsUrl = "http://research-qa.fhcrc.org".concat(editedLink).concat(".html"); }
			//newsUrl = "https://www-qa.fhcrc.org".concat(editedLink).concat(".html");
		}
		else 
		{
			if(linkType.equals("public")){ newsUrl = "http://www.fredhutch.org".concat(editedLink).concat(".html"); }
			if(linkType.equals("stripe")){ newsUrl = "http://research.fhcrc.org".concat(editedLink).concat(".html"); }
			//newsUrl = "https://www.fhcrc.org".concat(editedLink).concat(".html");
		}
		return newsUrl;
	}
}