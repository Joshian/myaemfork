/* js loaded in the <head> section */

// general-use function to add handlers. JS Bible 6th ed.
// examples:
//      addEvent(document.getElementById('ehs.form'), 'click', handleRadioClick);
//      addEvent(window,"load",omitSlideoutMenu);
function addEvent(elem,evtType,func){
  if(elem.addEventListener){ elem.addEventListener(evtType,func,false); }
  else if(elem.attachEvent){ elem.attachEvent("on"+evtType, func); }
  else { elem["on"+evtType] = func; }
}
