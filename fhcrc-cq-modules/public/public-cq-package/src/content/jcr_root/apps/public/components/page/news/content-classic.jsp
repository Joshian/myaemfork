<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text, java.util.Iterator, com.day.cq.wcm.api.WCMMode, org.fhcrc.publicsite.constants.Constants" %>

 <!-- page content -->
 <div id="fh_body" class="fh_body container_12 clearfix"><a name="content"></a>

  <div class="grid_12">

    <!-- breadcrumbs appear in the content flow in case they wrap to two lines -->
    <cq:include script="breadcrumbs.jsp"/>

    <cq:include script="clusterTitle.jsp" /> 
    
  </div>  
    
  <!-- content column -->
  <div id="fh_body_content" class="fh_body_content newsclassic grid_9 push_3">  

    <!--  TITLE -->
    <cq:include path="title" resourceType="foundation/components/title"/>
    
    <!--  SUBTITLE -->
    <% String subtitle = properties.get("newssubtitle/text",""); %>
    <% if( "".equals(subtitle) && WCMMode.fromRequest(request) == WCMMode.EDIT){%>
      <div class="cq-edit-only" style="font-style:italic">Subtitle (if any):</div>
    <% } %>
    <h3><cq:include path="newssubtitle" resourceType="public/components/content/textarea"/></h3>
    
    <!--  PUBLICATION DATE -->
    <%  Boolean CNW = currentPage.getPath().contains(Constants.PATH_TO_CENTERNEWS_FOLDER) ? Boolean.TRUE : Boolean.FALSE; 
        Boolean SPOT = currentPage.getPath().contains(Constants.PATH_TO_SCIENCESPOTLIGHT_FOLDER) ? Boolean.TRUE : Boolean.FALSE;
        if(WCMMode.fromRequest(request) == WCMMode.EDIT){
        String tooltip = (CNW||SPOT) ? "Publication date: " : "Publication date, required (used for sorting, it is not displayed):";
    %>
      <div class="cq-edit-only"><i><%= tooltip %></i></div>
    <% } %>
    <% if( CNW || SPOT || WCMMode.fromRequest(request) == WCMMode.EDIT ){ %>
       <div class="newspage-date <%= (!(CNW||SPOT)?"cq-edit-only":"") %>"><cq:include path="pubDate" resourceType="public/components/content/date"/></div>
       <!-- Pub date is currently: <cq:text property="pubDate/date"/> -->
    <% } %>
    
    <!--  BYLINE -->
    <% if(WCMMode.fromRequest(request) == WCMMode.EDIT){%>
      <div class="cq-edit-only" style="margin-top:12px; font-style:italic">Byline (if any):</div>
    <% } %>
    <div class="newspage-byline"><cq:include path="byline" resourceType="public/components/content/textarea"/></div>        

    
    <!-- TEXT -->        
    <% if( WCMMode.fromRequest(request) == WCMMode.EDIT ){%>
    <p class="cq-edit-only" style="font-style:italic">Story text:</p>
    <% } %>
    <cq:include path="articletext" resourceType="foundation/components/parsys"/>
    
    <!-- FEATURES -->  
    <% /* some news types have the "traditional" news layout
          but they also sport the newsplus-type "features" of the modern age. 
          choose those types here. (At least supply the additional 2nd social widget.) */

          String path = currentPage.getPath();
          if(path.contains(Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER)){%>

              <cq:include script="news-features.jsp"/>

          <% } else { %>

              <cq:include path="newsSocialShare" resourceType="public/components/content/socialshare"/>

          <% } %>

  </div>
  <!-- /content column -->

    

  <!-- left column (sidebar) -->
  <div id="fh_sidebar" class="fh_sidebar grid_3 pull_9"><div id="fh_sidebar_content" class="fh_sidebar_content">

    <%  if( currentPage.getPath().contains(Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER.concat("/")) ){
        //if( currentPage.getPath().contains("/content/public/en/news/hutch-magazine/") ){    
     %>
       <cq:include script="clusterNav_page.jsp" />
    <% } %>
    
    <!-- protected parsys: -->
    <div class="relatedcontentarea">
      <cq:include path="parRelated" resourceType="public/components/content/protectedparsys"/>
    </div>

    <cq:include script="related-content.jsp" />
  
   </div><!-- /fh_sidebar_content -->
  
  </div>
  <!-- /left column -->
 
 </div><!-- /fh_body -->
