package org.fhcrc.general.search.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.fhcrc.general.search.ISearchCacheService;
import org.apache.commons.io.IOUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.osgi.PropertiesUtil;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.result.SearchResult;
import javax.jcr.Session;

@Component(label = "FHCRC - SearchCacheService", description = "Search Cache", metatype = true, immediate = true, enabled = true)
@Service(ISearchCacheService.class)
@Properties({ @Property(name = "service.description", value = "Search Cache"), @Property(name = "service.vendor", value = "Fred Hutch") })
public class SearchCacheService implements ISearchCacheService {

    private static final int DEFAULT_TTL_MINUTES = 60;
    private int ttlMinutes = DEFAULT_TTL_MINUTES;
    @Property(label = "TTL in minutes",
            description = "Time that tag cache lives. [ Default: 60 minutes ]",
            intValue = DEFAULT_TTL_MINUTES)
    public static final String PROP_TTL_MINUTES = "ttl-minutes";
    
    private static HashMap<String, SearchCacheData> pathMap = new HashMap<String, SearchCacheData>();
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    
    public Long getPageCountbyPathAndTag(Session session, QueryBuilder builder, String path, String tagId) throws Exception {
        String mapKey = path + ";" + tagId;
        SearchCacheData cachedData = pathMap.get(mapKey);
        if (cachedData != null && !cachedData.isOutOfDate(ttlMinutes)) {
            log.debug("Found path + tagId in cache.");
            return cachedData.getCount();
        } else {
            // --------------------------------------------------------------------
            // Cache doesn't hold this value. Search it and populate the cache.
            // --------------------------------------------------------------------
            log.debug("Did not find path + tagId in cache.");
            cachedData = searchPageCountByPathAndTag(session, builder, path, tagId);
            pathMap.put(mapKey, cachedData);
            log.debug("Added to cache : key = " + mapKey + " count = " + cachedData.getCount());
            return cachedData.getCount();
        }
    }

    private SearchCacheData searchPageCountByPathAndTag(Session session, QueryBuilder builder, String path, String tagId) throws Exception {
        /* Begin building the predicateGroup for the search */
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("type", "cq:PageContent");
		map.put("property", "cq:tags");
		map.put("property.1_value", tagId);
		map.put("group.1_path", path);
		SearchResult result = builder.createQuery(PredicateGroup.create(map), session).getResult();
		log.warn("Center News (New Way) = " + result.getTotalMatches());
		Long totalMatches = result.getTotalMatches();	
		return new SearchCacheData(totalMatches, new Date());		
    }

    @Activate
    protected final void activate(final Map<String, String> config) {
        ttlMinutes = PropertiesUtil.toInteger(config.get(PROP_TTL_MINUTES), DEFAULT_TTL_MINUTES);
    }

}
