<%@include file="/libs/foundation/global.jsp" %><%
%><%
    final StringBuffer cls = new StringBuffer();
    for (String c : componentContext.getCssClassNames()) {
        cls.append(c).append(" ");
    }
%>
<body>
<div id="wrapper" class="<%= cls %>">
  <a href="#jump_content" title="Jump to page content"></a>
  <a name="top"></a>
 

  <cq:include script="mobile-header.jsp"/>
  <cq:include path="sectionIdNavigation" resourceType="public_mobile/components/content/sectionIdNavigation"/>
  <cq:include script="content.jsp"/>
  <cq:include script="mobile-footer.jsp"/>
 
  <cq:include script="menu-system.jsp"/>

</div>
</body>
