<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.fhcrc.tools.FHUtilityFunctions" %>
<%@page session="false" %><%

String title = properties.get("title","");
String text = properties.get("text","");
String link = properties.get("linkTarget","");

Boolean hasLink = !link.trim().equals("");
Boolean hasText = !text.trim().equals("");

if (hasLink) {
	
	link = FHUtilityFunctions.cleanLink(link, resourceResolver);
	
}

%>

<cq:text tagName="h2" property="title"/>

<div<%= hasLink ? " data-link=\"".concat(link).concat("\"") : "" %>>
	<span title="<%= title %>"<%= hasText ? "" : " class=\"cq-text-placeholder\"" %>><%= hasText ? text : "&para;" %></span>
</div>