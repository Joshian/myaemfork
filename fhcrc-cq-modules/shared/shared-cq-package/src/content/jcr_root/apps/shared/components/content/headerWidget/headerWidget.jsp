<%--
  Copyright 1997-2009 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Header component

  Places author text into a header tag, the order of which (h2, h3, etc) is also chosen by the user.

--%><%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.fhcrc.tools.FHUtilityFunctions" %><%

// COLOR THEME:
// colorTheme0, colorTheme1, etc. are controlled
// via design dialog. In the design dialog configure
// the allowed set of text & background colors like this:
//
//       0<tab>Normal (default)
//       1<tab>Black text on white background
//       2<tab>White text on yellow background
//       #<tab>Text the user will see in the dropdown
//       etc.
//
// you do not have to go sequentially, and can use multiple digits if you want.
// now the user will be able to select a background color of your choosing.
// this component places a class, colorTheme#, around the text div.
// add the appropriate styling to your css for the colorTheme# choices you provide.


// DESIGN THEME:
// for complete generality, we add the ability to select a second class.
// you can use this however you want. e.g., 
//
//       0<tab>None (default)
//       1<tab>Narrow margins
//       2<tab>Blinking text
//       #<tab>Text the user will see in the dropdown
//       etc.

String colorthemes = properties.get("colortheme","0"); // default is theme 0
//out.write(colorthemes);
String[] c = colorthemes.split("\t");
String color_theme_number = c[0];
String colorTheme = "";
if(!color_theme_number.isEmpty()){
	colorTheme = "colorTheme" + color_theme_number ;
}

String designthemes = properties.get("designtheme","0"); // default is theme 0
//out.write(designthemes);
c = designthemes.split("\t");
String design_theme_number = c[0];
String designTheme = "";
if(!design_theme_number.isEmpty()){
  designTheme = "designTheme" + design_theme_number ;
}

String linkTarget = properties.get("linkTarget","");
Boolean hasLink = !(linkTarget.equals(""));
if (hasLink) {
    linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
%>
<a href="<%= linkTarget %>"<%= properties.get("externalLink","false").equals("true") ? " target=\"_blank\"" : "" %>>
<%
}
%>
<cq:text property="text" tagName="<%= properties.get("headerSize","h2") %>" tagClass="<%= colorTheme + " " + designTheme %>" />
<%
if (hasLink) {
%>
</a>
<%
}
%>