<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text, 
                 java.util.*,
                 com.day.cq.wcm.foundation.Image,
                 com.day.cq.search.QueryBuilder, 
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.result.Hit,
                 java.text.SimpleDateFormat,
                 java.text.DateFormat,
                 com.day.cq.wcm.api.WCMMode,
                 org.fhcrc.publicsite.constants.Constants,
                 org.fhcrc.tools.FHUtilityFunctions" %>
<%
Date defaultDate = new Date();

Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);

/* searchTerm is the name of the variable that should be passed to the component via the URL */
String queryString = slingRequest.getParameter("searchTerm");
/* This could be pulled out into the Constants file if desired */
String newsPath = "/content/consortium/en/about/news";

/* Begin building the predicateGroup for the search */ 
HashMap<String, String> map = new HashMap<String, String>();

map.put("path", newsPath);
/* We only care about jcr:content nodes */
//map.put("type","cq:Page");
map.put("property","cq:template");
map.put("property.value","/apps/consortium/templates/general");
map.put("p.limit","100000"); //Temp fix to display all results. Change when/if pagination is implemented.

/* Handle any date filters. Mar/2012 */
Date startdate = properties.get("startdate",Date.class);
Date enddate = properties.get("enddate",Date.class);
if(startdate!=null){
    DateFormat f1= new SimpleDateFormat("yyyy-MM-dd'T00:00:00.000-06:00'"); // assets will have daylight savings time
    map.put("4_group.1_daterange.lowerBound",f1.format(startdate)); //f1.format(startdate)//Integer.toString(startdate.getYear())+'-'+Integer.toString(startdate.getMonth())+'-'+Integer.toString(startdate.getDay())
    map.put("4_group.1_daterange.lowerOperation",">=");
    map.put("4_group.1_daterange.property","jcr:created");
    log.debug("start date = " + f1.format(startdate));
}
if(enddate!=null){
    DateFormat f2= new SimpleDateFormat("yyyy-MM-dd'T23:59:59.999-09:00'"); //   built in...so cover the worst case scenario.
    map.put("4_group.2_daterange.upperBound",f2.format(enddate)); //f1.format(enddate)//Integer.toString(startdate.getYear())+'-'+Integer.toString(startdate.getMonth())+'-'+Integer.toString(startdate.getDay())
    map.put("4_group.2_daterange.upperOperation","<=");
    map.put("4_group.2_daterange.property","jcr:created");
    log.debug("stop date = " + f2.format(enddate));
}
//map.put("4_group.p.or","false");
// http://dev.day.com/discussion-groups/content/lists/cq-google/2010-11/2010-11-30__day_communique____operator_in_daterange_query_sharat.html/2?q=Querybuilder



/* the default is to sort by reverse chron... */
map.put("orderby","@jcr:created"); // changed to date sorting, sld.
map.put("orderby.sort","desc"); // added reverse chron, sld.
map.put("orderby.index","true");

/* but if there is a query string, then do a fulltext search on that string and order results by score */   
if(queryString!=null && !queryString.trim().equals("")){
    queryString = queryString.replaceAll(" ", " OR ");
    map.put("3_group.p.or","true");
    map.put("3_group.1_fulltext",queryString);
//    map.put("3_group.1_fulltext.relPath", "jcr:content");
    map.put("3_group.2_fulltext",queryString);
    map.put("3_group.2_fulltext.relPath", "@cq:tags");
    map.put("orderby","@jcr:score"); /* override whatever defaults or choices were made */
    map.put("orderby.sort","desc");
} 

%>

<div class="newsfindercontainer">

<%

List<Hit> hitList = builder.createQuery(PredicateGroup.create(map), session).getResult().getHits();
List<Page> pageList = new ArrayList<Page>();
if(properties.get("searchBox","false").equals("true")){
    %>
        <cq:include resourceType="/apps/public/components/content/searchWidget" path="includedSearchBox" />
    <%
}
if(hitList.size() == 0){
%>  
    <cq:include script="empty.jsp"/>
<%  
} else {
    
    //We may need to skip the page if it is the news page itself. Keep that in mind.
    
    for(int i = 0; i < hitList.size(); i++){
      Page testPage = hitList.get(i).getResource().getParent().adaptTo(Page.class);
      Boolean skip = false;
      // omit the landing page
      if(testPage.getPath().equals(newsPath)){ 
        skip=true;
      }
      
      if(skip == false){
          pageList.add(testPage); 
      }
    }

    Iterator<Page> pageIterator = pageList.iterator();

    if(!pageIterator.hasNext()){
%>  
    <cq:include script="empty.jsp"/>
<%      
    } else {
%>
<div class="related-news">
<%

boolean showDescription = properties.get("showDescription",Boolean.FALSE);
boolean showIcon = properties.get("showIcon", Boolean.FALSE);
Integer maxnum = properties.get("maxnum",3);

Integer ct = 0;
while(pageIterator.hasNext() && (maxnum.equals(0)||(ct++ < maxnum))){
    Page nextPage = pageIterator.next();
    String pageDescription = nextPage.getDescription() != null ? nextPage.getDescription() : "";
    
    if(showIcon) {
        
        Resource r = nextPage.getContentResource("image");
        
        if (r != null) {
            String img;
            Image image = new Image(r);
            
            if(image.hasContent()) {
                img = nextPage.getPath() + ".img.png" + image.getSuffix(); // e.g.,: /content/public/en/events/events/hutch-award.img.png/1323194909406.gif
                // prepend context path to img
                img = request.getContextPath() + img;
        
%>
    <div class="announcementIcon">
        <img src="<%= img %>" alt=" " title=" ">
    </div>
<%
            }
        }
    }
%>
    <p>
     <span class="news-title"><a href="<%= nextPage.getPath() %>.html"><%= FHUtilityFunctions.displayTitle(nextPage) %></a></span><br />     
     <% if(showDescription && !"".equals(pageDescription)){ %><span class="news-description"><%=pageDescription%></span><br /><% } %>
    </p>
<%
}
%>
</div>
<%
}
}
%>

</div>