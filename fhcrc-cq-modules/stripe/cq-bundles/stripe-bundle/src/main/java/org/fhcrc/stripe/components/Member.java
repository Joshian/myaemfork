
/**
 * Member class is in charge of handling member component properties and logic
 *  
 */
package org.fhcrc.stripe.components;


import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletRequest;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.fhcrc.stripe.functions.FHUtilityFunctions;

import com.day.cq.commons.Doctype;
import com.day.cq.wcm.api.components.DropTarget;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.foundation.Image;

public class Member {
	
	private ValueMap componentProperties;
	
	private ResourceResolver resourceResolver;
	
	private Resource resource;
	private Style currentStyle;
	private Design currentDesign;
	private Design resourceDesign;
	private ServletRequest request;
	private Writer writer;
	
	private Image image;
	private String name;
	private String title;
	private String[] career;
	private String email;
	private String phone;
//	private String imagePath;
	private String height;
	private String dates;
	private String other;
	private String memberPage;
	private boolean externalLink;

	
	/** Constructor of the class
	 */
	public Member() {
	}	
	
	
	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;	
		this.name = componentProperties.get("name","");
		this.title= componentProperties.get("title","");
		this.dates= componentProperties.get("dates","");
		this.other= componentProperties.get("other","");
		this.career = componentProperties.get("career", String[].class);
		this.email = componentProperties.get("email","");
		this.phone = componentProperties.get("phone","");
		this.image = new Image(this.resource, "image");
		setImage(this.image);
//		this.imagePath = componentProperties.get("fileReference","");
		this.height = componentProperties.get("height","450");
		this.memberPage = componentProperties.get("memberpage","");
		setMemberPage(this.memberPage);
		this.externalLink = componentProperties.get("externalLink",false);
		
	}

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}
	
	public Image getImage() {
		return image;
	}


	@SuppressWarnings("deprecation")
	public void setImage(Image image) {
		this.image = image;
		this.image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
		this.image.loadStyleData(currentStyle);
	    this.image.setSelector(".img"); // use image script
	    this.image.setDoctype(Doctype.fromRequest(request));
	    // add design information if not default (i.e. for reference paras)
	    if (!currentDesign.equals(resourceDesign)) {
	        this.image.setSuffix(currentDesign.getId());
	    }
	}

	public Resource getResource() {
		return resource;
	}


	public void setResource(Resource resource) {
		this.resource = resource;
	}


	public Style getCurrentStyle() {
		return currentStyle;
	}


	public void setCurrentStyle(Style currentStyle) {
		this.currentStyle = currentStyle;
	}


	public Design getCurrentDesign() {
		return currentDesign;
	}


	public void setCurrentDesign(Design currentDesign) {
		this.currentDesign = currentDesign;
	}


	public Design getResourceDesign() {
		return resourceDesign;
	}


	public void setResourceDesign(Design resourceDesign) {
		this.resourceDesign = resourceDesign;
	}


	public ServletRequest getRequest() {
		return request;
	}


	public void setRequest(ServletRequest request) {
		this.request = request;
	}


	public Writer getWriter() {
		return writer;
	}


	public void setWriter(Writer writer) {
		this.writer = writer;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String[] getCareer() {
		return career;
	}

	public void setCareer(String[] career) {
		this.career = career;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}
/*
	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
*/
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhoneLink(){
		return this.phone.replaceAll("-", "");
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}
	
	public String getDates() {
		return dates;
	}


	public void setDates(String dates) {
		this.dates = dates;
	}
	
	public String getOther() {
		return other;
	}


	public void setOther(String other) {
		this.other = other;
	}
	
	public String getMemberPage() {
		return memberPage;
	}


	public void setMemberPage(String memberPage) {
		if(!memberPage.equals("")){
	        this.memberPage = FHUtilityFunctions.cleanLink(memberPage, resourceResolver);
	    } else {
	    	this.memberPage = "";
	    }
	}

	public boolean isExternalLink() {
		return externalLink;
	}

	public void setExternalLink(boolean externalLink) {
		this.externalLink = externalLink;
	}
	
	/* OTHER PUBLIC METHODS */
	
	public void drawImage() {
		try {
			this.image.draw(writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}