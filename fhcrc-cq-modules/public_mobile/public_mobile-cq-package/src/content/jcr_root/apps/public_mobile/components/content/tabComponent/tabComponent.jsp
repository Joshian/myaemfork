<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.WCMMode" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%

String[] tabTitles = {properties.get("tabtitle1",""), properties.get("tabtitle2",""), properties.get("tabtitle3",""), 
  properties.get("tabtitle4",""), properties.get("tabtitle5",""), properties.get("tabtitle6","")};
boolean firstTabReached = false;
String parName = "tabbedpar";

for (int tabNumber = 0; tabNumber < tabTitles.length; tabNumber++) {
	
	if (!tabTitles[tabNumber].equals("")) {
		
		String thisParName = parName.concat(Integer.toString(tabNumber + 1));
		
%>

<div class="expanderContainer" data-default-status="<%= firstTabReached ? "closed" : "open" %>">

    <div class="title">
    
        <div class="titleContainer tableCell">
        
            <h2><%= tabTitles[tabNumber] %></h2>
            
        </div>
        
        <div class="iconContainer tableCell">
        
            <span class="expanderIcon" data-status="<%= firstTabReached ? "closed" : "open" %>"><%= firstTabReached ? "+" : "&ndash;" %></span>
        
        </div>
        
    </div>
    
    <div class="textContainer">
    
        <cq:include path="<%= thisParName %>" resourceType="foundation/components/parsys"/>
    
    </div>
    
</div>
<%

        firstTabReached = true;
        
    }
    
}

%>
<cq:includeClientLib js="mobile.components.expander" />
<script type="text/javascript">
/* If we haven't already added the event listener to initialize the expander
 * components, switch the hasExpanders boolean and add the initialization
 * function to the DOMContentLoaded event.
 */
if (!componentApp.expanderComponent.hasExpanders) {

    componentApp.expanderComponent.hasExpanders = true;
    document.addEventListener("DOMContentLoaded", 
      componentApp.expanderComponent.expanderInit, false);
      
}
</script>