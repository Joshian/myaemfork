<%@include file="/libs/foundation/global.jsp"%>

<!-- FOOTER -->

	<div class="footer_container">
  
  	<div class="footer_items_container outer_grid_container">
  	  
  	  <div class="footer_item social_icons_container inner_grid_container">
      
      	<div class="social_icons">
	
	  	  <h3>Follow Us</h3>
 		  <img id="social_media_icons_footer" src="<%= currentDesign.getPath() %>/img/icons/socialmedia_footer.png" alt="Social media icons" usemap="#socialmap">
 		  <map name="socialmap">
     		<area shape="rect" coords="0,0,28,28" href="http://www.facebook.com/HutchinsonCenter" target="_blank" alt="Like Fred Hutch on Facebook"></area>
     		<area shape="rect" coords="28,0,56,28" href="http://twitter.com/fredhutch" target="_blank" alt="Follow Fred Hutch on Twitter"></area>
     		<area shape="rect" coords="56,0,83,28" href="https://www.youtube.com/channel/UCnWBh22UKMHF4VRKdsQvZqg/featured" target="_blank" alt="View the Fred Hutch YouTube channel"></area>
     		<area shape="rect" coords="83,0,111,28" href="http://www.pinterest.com/fredhutch/" target="_blank" alt="Pin from the Fred Hutch boards on Pinterest"></area>
     		<area shape="rect" coords="111,0,139,28" href="http://instagram.com/FredHutch" target="_blank" alt="Check out Fred Hutch photos on Instagram"></area>
    		<area shape="rect" coords="139,0,166,28" href="https://plus.google.com/116048577052683099154/posts" target="_blank" alt="Follow Fred Hutch on Google+"></area>
   		  </map>
  
  		</div>
  		
  	  </div>
  	  
      <div class="footer_item fred_hutch_description inner_grid_container">
      
        <p>Fred Hutch is a world-renowned nonprofit research organization working to improve the prevention, detection and treatment of cancer and related diseases. We are proud to be home to three Nobel laureates.</p>
      
      </div>
      
    </div>
  
  </div>

<!-- END FOOTER -->