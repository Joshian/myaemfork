var componentApp = (function(componentApp) {
	
	componentApp.homepageSlider = (function(homepageSlider) {
		
		homepageSlider.homepagePanel = 0;
		
		// Code based on a post found here: http://www.queness.com/post/923/create-a-simple-infinite-carousel-with-jquery
		homepageSlider.initHomepageSlider = function() {

			var allTheDots = $('.homepageDot');
			var totalPanels = $('.homepageFadeContainer').size();
			if (totalPanels === 1) {
			  $('homepageSliderDots').css({display:'none'});
			  return;
			}
			$('#homepageSliderDots').css({display : 'block'});
			$('img','.homepageDot:first').attr('src','/etc/designs/public/img/bullets/dot_white.png');
			var theInterval = window.setInterval(homepageSlider.advanceSlider, 6000);
			$('.homepageDot').click(function(){
				var newPanel = allTheDots.index(this);
				if(newPanel == homepageSlider.homepagePanel) { return; }
				else {
					$('.homepageFadeContainer').eq(homepageSlider.homepagePanel).fadeOut(1000).end().eq(newPanel).fadeIn(1500);
					$('.homepageDot img').attr('src','/etc/designs/public/img/bullets/dot_grey.png');
					$('img', this).attr('src','/etc/designs/public/img/bullets/dot_white.png');
					homepageSlider.homepagePanel = newPanel;
				}
			});
			$('#homepageSliderContainer').mouseenter(function() {
				window.clearInterval(theInterval);
			});
			$('#homepageSliderContainer').mouseleave(function() {
				theInterval = window.setInterval(homepageSlider.advanceSlider, 6000);  
			});
		};

		homepageSlider.advanceSlider = function() {
			var allTheDots = $('.homepageDot');
			var totalPanels = $('.homepagePhoto').size();
			if(homepageSlider.homepagePanel == totalPanels - 1){
				var newPanel = 0;
			} else {
				var newPanel = homepageSlider.homepagePanel+1;
			}
			
			$('.homepageFadeContainer').eq(homepageSlider.homepagePanel).fadeOut(1000).end().eq(newPanel).fadeIn(1500);
			$('.homepageDot img').attr('src','/etc/designs/public/img/bullets/dot_grey.png');
			$('img', allTheDots.eq(newPanel)).attr('src','/etc/designs/public/img/bullets/dot_white.png');
			homepageSlider.homepagePanel = newPanel;
		};
		
		$(document).ready(function() {
			homepageSlider.initHomepageSlider();
		});
				
		return homepageSlider;
		
	})(componentApp.homepageSlider || {});
	
	return componentApp;
	
})(componentApp || {});