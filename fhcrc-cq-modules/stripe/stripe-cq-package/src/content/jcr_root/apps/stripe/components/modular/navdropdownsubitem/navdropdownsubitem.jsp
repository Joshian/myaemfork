<%@include file="/apps/stripe/global.jsp" %>

<jsp:useBean id="navItemBean" scope="request" class="org.fhcrc.stripe.components.NavDropdownItem" />
<jsp:setProperty name="navItemBean" property="componentProperties" value="<%= properties %>"/>
<c:if test="${!empty navItemBean.subitemlabel}">
	<div class="clearfix"></div>
	<li style="padding-top: 5px;">
	    <a href="<c:out value="${navItemBean.subitempath}"/>.html"><c:out value="${navItemBean.subitemlabel}"/></a>
	</li>
</c:if>
