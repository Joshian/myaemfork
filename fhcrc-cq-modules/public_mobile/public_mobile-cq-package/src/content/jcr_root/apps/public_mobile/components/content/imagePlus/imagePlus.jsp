<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode,
    org.fhcrc.tools.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
    Image image = new Image(resource, "image");
//    Image hires = new Image(resource, "hi-res");
//    Image mouseover = new Image(resource, "mouseover");
//    String hiresSrc = "";
//    String mouseoverSrc = "";
    String linkTarget = properties.get("linkTarget",""),
      caption = properties.get("jcr:caption",""),
      credit = properties.get("credit",""),
      classString = "bottomBar";
    Boolean hasCaption = false, hasCredit = false;
    
    // Check to see if we do have caption/credit. This is important for HTML
    // class assignment later
    
    if (!caption.trim().equals("")) {
        
        hasCaption = true;
        
    }
    
    if (!credit.trim().equals("")) {
        
        hasCredit = true;
        
    }
    
    if (hasCaption) {
        
        if (hasCredit) {
            
            classString = "captionCreditContainer";
            
        } else {
            
            classString = "captionContainer";
            
        }
        
    } else {
        
        if (hasCredit) {
            
            classString = "creditContainer";
            
        }
    }

    // If the linkTarget is an internal page, we need to concat a ".html"
    
    Resource r;     
    if(!linkTarget.equals("")){
        linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
    }
    
/*    
    if(hires.hasContent()){
        hiresSrc = hires.getSrc();
    
        // If the hi-res image is not in the dam, we need to mess with the Src string to get hi-res.img.<file extension> rather than hi-res/file.<file extension>
        if(hiresSrc.contains("hi-res/file")){
            hiresSrc = hiresSrc.replaceAll("hi-res/file\\.", "hi-res.img.");
        }
    }
    if(mouseover.hasContent()){
        mouseoverSrc = mouseover.getSrc();
    
        // If the mouseover image is not in the dam, we need to mess with the Src string to get mouseover.img.<file extension> rather than mouseover/file.<file extension>
        if(mouseoverSrc.contains("mouseover/file")){
            mouseoverSrc = mouseoverSrc.replaceAll("mouseover/file\\.", "mouseover.img.");
        }
    }
*/

    //drop target css class = dd prefix + name of the drop target in the edit config
    image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");

/*
    if(!"true".equals(properties.get("noBorder","false"))) {
        image.addCssClass("frame"); // Add the frame class if requested
    } else {
        image.addCssClass("noborder"); 
    }
*/
/*    
    if(mouseover.hasContent()){
        image.addAttribute("data-hover", mouseoverSrc);
        image.addCssClass("rolloverImage");
    }
*/

    //remove title attribute unless there is something explicitly in the dialog

    if(properties.get("image/jcr:title","").trim().equals("")) {
        image.setTitle(" ");
    }

    image.loadStyleData(currentStyle);
    image.setSelector(".img"); // use image script
    image.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        image.setSuffix(currentDesign.getId());
    }
    
%>

<% 
    if(image.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT){ 
    
        // Open the div tag, adding the cq-edit-only class if there is no image
    %>
      
        <div class="imageContainer <%= image.hasContent() ? "" : "cq-edit-only" %>">
            <div class="topBar">Top Bar</div>
        <% 
        // If there is a link, add in the <a> tag (with target="blank" if necessary
                
        if(!linkTarget.equals("")){ 
        %>
        <a href="<%= linkTarget %>" <%= properties.get("externalLink","false").equals("true") ? "target=\"_blank\"" : "" %>>
        <% 
          image.draw(out);
        %>
        </a>
        <%
        } else {
            // If there was no link, just draw the image
            
            image.draw(out);
        }
        
        // Next is the caption/credit container. Add the classString to its class attribute
        %>
        
        <div class="<%= classString %>">
        <% 
        if (hasCaption) {
        %>
          <cq:text property="jcr:caption" tagName="span" tagClass="imageCaption" placeholder="" />
        <%
          if (hasCredit) {
        %>
          <i><span class="imageCredit">(<%= credit %>)</span></i>
        <%
          }
        } else {
            if (hasCredit) {
        %>
          <i><cq:text property="imageCredit" tagName="span" tagClass="caption" /></i>
        <%
            }
        }
        %>
        </div>
      </div>
    <%
    }
    %>

