<%@include file="/apps/stripe/global.jsp" %>
<%@ page import="org.fhcrc.stripe.utilities.PageUtil" %>
<%
      String paddingTop = properties.get("paddingtop", "25");
      String paddingBottom = properties.get("paddingbottom", "25");
      String hr_color = properties.get("hr_color", "c0c0c0");
      String weight = properties.get("weight", "2");
      String hr_style="border-top:" + weight + "px solid #" + hr_color +"!important; margin:0 !important; clear:both !important;";
%>
 <div style="padding-top: <%=paddingTop%>px; padding-bottom: <%=paddingBottom%>px;">
    <hr style="<%=hr_style%>">
 </div>