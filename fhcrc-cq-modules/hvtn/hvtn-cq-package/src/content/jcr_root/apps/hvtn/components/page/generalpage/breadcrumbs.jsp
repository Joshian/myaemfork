<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text,
        com.day.cq.wcm.api.PageFilter,
        com.day.cq.wcm.api.NameConstants,
        org.apache.commons.lang.StringEscapeUtils,
        com.day.cq.wcm.api.Page,
        org.fhcrc.tools.FHUtilityFunctions" %>
<%

final int STARTING_LEVEL = 3;
final String CURRENT_PAGE_TITLE = FHUtilityFunctions.displayTitle(currentPage);

%>
        
<div id="breadcrumbs" class="breadcrumbs noprint">
	<ul>
		<li class="firstTwoBreadcrumbs"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>.html">Home</a></li>
<%
int level = STARTING_LEVEL;
int currentLevel = currentPage.getDepth() - 1;
String title, cls;

while (level < currentLevel) {
    Page trail = currentPage.getAbsoluteParent(level);
    if (trail == null) {
        break;
    }
    title = FHUtilityFunctions.displayTitle(trail);
    cls = (level == STARTING_LEVEL) ? " class=\"firstTwoBreadcrumbs\"" : "";
    %><li<%=cls%>><a href="<%= Text.escape(trail.getPath(), '%', true) %>.html"><%
    %><%= StringEscapeUtils.escapeHtml(title) %><%
    %></a></li><%

    level++;
}
%>
          
		<li class="currentPageBreadcrumb"><%= CURRENT_PAGE_TITLE %></li>
	</ul>
</div>