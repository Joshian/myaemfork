<%@include file="/apps/stripe/global.jsp"%>

<%@page import="com.day.cq.dam.api.Asset,
                org.fhcrc.stripe.utilities.AssetUtil"%>
<%
String disabled = ((editMode.equals("true"))?"":"disabled"); 
%>

<jsp:useBean id="dynamicBean" scope="request" class="org.fhcrc.stripe.components.Dynamic" />
<jsp:setProperty name="dynamicBean" property="componentProperties" value="<%= properties %>"/>
<jsp:setProperty name="dynamicBean" property="resourceResolver" value="<%= resourceResolver %>"/>

   
<div id="dynamic-carousel" class="dynamic-container" >
    <div class="slides" >   
		<div id="myCarousel2" class="carousel slide carousel-fade">	    
		 <!-- Carousel items -->
		 <div class="carousel-inner">		    
		        <c:forEach items="${dynamicBean.sections}" var="section" varStatus="loop">
		            <c:choose>
		                <c:when test="${loop.count == 1}">
		                    <div id="img${loop.count}"   class="active item">
		                </c:when>
		                <c:otherwise>
		                    <div id="img${loop.count}"  class="item">
		                </c:otherwise>
		            </c:choose>   
		                     <img src="${section.imagePath}"/>
		                     <c:set var="carouselImage" value="${section.imagePath}" scope="request"/>
		                     <c:set var="imageDescription" value="<%=AssetUtil.getAssetProperty(slingRequest,(String)request.getAttribute("carouselImage"),"dc:description")%>"/>
		                       <c:if test="${imageDescription != ''}" >
		                            <div class="dynamic_description">
		                                <p>
		                                    <c:out value="${imageDescription}" />
		                                </p>
		                            </div>
		                       </c:if>
	                      </div>
		        </c:forEach>
         </div>		     
	        <c:if test="${empty dynamicBean.sections}" >
	            <div class="active item">
	                <img src="http://placehold.it/980x360" />
	            </div>
	        </c:if>
	
		</div>
	</div>
	<div class="slider-thumbs" >
	   <div class="inside">
	       <span id="thumbs-pointer" style="left: ${dynamicBean.pointerPos}px"></span>
	       <div class="thumbs-container">       
	            <ul>            
	                <c:forEach items="${dynamicBean.sections}" var="section" varStatus="loop">
	                    <c:choose>
	                        <c:when test="${loop.count == 1}">
	                            <li id="thumb${loop.count}" pos="${loop.count}" class="slide-thumb thumbs${dynamicBean.slideNumber} current">
	                        </c:when>
	                        <c:otherwise>
	                            <li id="thumb${loop.count}" pos="${loop.count}" class="slide-thumb thumbs${dynamicBean.slideNumber}">
	                        </c:otherwise>
	                    </c:choose> 
				                    <a href="#">
				                        <div class="thumb-image" >
				                            <c:choose>
				                             <c:when test="${!empty section.imageThumbnail}">
				                             <img class="squareImg" src="${section.imageThumbnail}" />
				                          </c:when>
				                         <c:otherwise>
				                            <img src="${section.imagePath}" />
				                         </c:otherwise>
				                         </c:choose>
				                        </div>
				                        <p class="thumb-label">${section.imageLabel}</p>
				                    </a>                           
	                            </li>
	                </c:forEach>
                </ul>
            </div>
	   </div>
	</div>
    <div class="clearfix"> 
    </div>
	<div class="dynamic-content" >
        <c:forEach items="${dynamicBean.sections}" var="section" varStatus="loop">
            <c:choose>
              <c:when test="${loop.count == 1}">
                    <div id="contentdiv${loop.count}" class="dynamic-div">
              </c:when>
              <c:otherwise>
                    <div id="contentdiv${loop.count}" class="dynamic-div <%=disabled%>">
              </c:otherwise>
            </c:choose> 
                        <div>
                            <%--
                            <cq:include path="title${loop.count}" resourceType="stripe/components/modular/sectiontitle" />
                             --%>
                            <div class="clearfix" >
                            </div>
                        </div>
		                <div>
		                    <cq:include path="parsys${loop.count}" resourceType="foundation/components/parsys"/>
		                    <div class="clearfix" >
                            </div>
		                </div>
                    </div>
        </c:forEach>
	</div>	
</div>

