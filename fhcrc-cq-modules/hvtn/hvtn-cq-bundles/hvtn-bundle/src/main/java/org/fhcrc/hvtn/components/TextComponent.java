package org.fhcrc.hvtn.components;

import org.apache.sling.api.resource.ValueMap;

public class TextComponent {
	
	private ValueMap componentProperties;
	
	private String colorTheme;
	private String designTheme;
	
	public TextComponent() {
		
	}

	/**
	 * @return the componentProperties
	 */
	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	/**
	 * @param componentProperties the componentProperties to set
	 */
	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
		setColorTheme(properties.get("colortheme","0"));
		setDesignTheme(properties.get("designtheme","0"));
	}

	public void setColorTheme(String colorString) {
		 String[] c = colorString.split("\t");
		 String color_theme_number = c[0];
		 this.colorTheme = "";
		 if(!color_theme_number.isEmpty()){
		     this.colorTheme = "colorTheme" + color_theme_number ;
		 }
	}
	
	public String getColorTheme() {
		return colorTheme;
	}
	
	public void setDesignTheme(String designString) {
		String[] c = designString.split("\t");
		String design_theme_number = c[0];
		this.designTheme = "";
		if(!design_theme_number.isEmpty()){
			this.designTheme = "designTheme" + design_theme_number ;
		}
	}
	
	public String getDesignTheme() {
		return designTheme;
	}

}