<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.api.WCMMode,
                 java.util.Date,
                 com.day.cq.commons.RangeIterator, 
                 java.util.TreeSet,
                 java.util.Comparator,
                 java.util.Iterator,
                 com.day.cq.wcm.foundation.List, 
                 com.day.cq.wcm.api.PageFilter, 
                 org.fhcrc.tools.FHUtilityFunctions" %>
<%

/* UPDATE 10/02/14 - 
   We no longer want this component to automatically show Petri Dish or Hutch Magazine
   articles. We only want an author to pick from news and display those.
   I'll leave in but comment out the code that used to handle Petri & Hutch Magazine */

/* this component creates a list of news items drawn from several sources.
   1 - the most recent Hutch Magazine feature article (from RSS import)
   2,3 - the two most recent Petri Dish articles (from RSS import)
   4,5 - editorially-chosen articles from Center News Weekly
   
   once the collection of five items is created, it will be sorted by 
   reverse chron (see TreeMap).
   
   this component is designed to be dropped into the News & Publications
   landing page, but it might exist anywhere you want this kind of 
   "recent news" plus editorial approach.
   
 */
 
 /* UPDATE 2014-01-23. Hutch Magazine and Petri Dish are old news and Joe
                       wants these all to be editorially chosen. 
                       I am omitting the auto-picks of Hutch Magazine and of
                       Petri Dish. Also opening up the pick list (see dialog.xml
                       so editor can select news from the entire news branch (was just CNW). */
 
   WCMMode mode = WCMMode.fromRequest(request);   
      
   String pathToPetridish = "/content/public/en/news/petridish-import";
   String pathToHutchMagazine = "/content/public/en/news/hutch-magazine/imports";

   /* let's omit these pulls by just dev/null-ing these paths. this'll make it easier to 
      bring back this functionality later. */
   pathToPetridish = "/dev/null";
   pathToHutchMagazine = "/dev/null";

   Date defaultDate = new Date();

   boolean omitSource = properties.get("omitSource",Boolean.FALSE);
   boolean omitDate = properties.get("omitDate",Boolean.FALSE);


   // optional title of this widget
   String widget_title = properties.get("title","");
   
   // here is the TreeMap that will contain the basket of articles
   TreeSet<Page> newsTree = new TreeSet<Page>(new Comparator<Page>(){
        public int compare(Page a, Page b){
            Date dateA = a.getProperties().get("pubDate/date", Date.class);
            Date dateB = b.getProperties().get("pubDate/date", Date.class);
            if(dateA==null){
                dateA = new Date();
            }
            if(dateB==null){
                dateB = new Date();
            }
            
            if(dateA.equals(dateB)){
                return a.getPath().compareTo(b.getPath());
            }
            return dateA.compareTo(dateB);
        }
    });  
   
   // it will be convenient to re-use another TreeSet for sorting
      TreeSet<Page> tmp = new TreeSet<Page>(new Comparator<Page>(){
        public int compare(Page a, Page b){
            Date dateA = a.getProperties().get("pubDate/date", Date.class);
            Date dateB = b.getProperties().get("pubDate/date", Date.class);
            if(dateA==null){
                dateA = new Date();
            }
            if(dateB==null){
                dateB = new Date();
            }
            if(dateA.equals(dateB)){
                return a.getPath().compareTo(b.getPath());
            }
            return dateA.compareTo(dateB);
        }
    });

   // 1. step through the top two ./pages in this component. note that this
   //    automatically disqualifies embargoed content, because when this runs
   //    on publisher the embargoed content doesn't exist.
   List list = new List(slingRequest, new PageFilter()); // not sure if we want the PageFilter here?
   if (!list.isEmpty()) {
       Iterator<Page> items = list.getPages();
       //while ( items.hasNext() && newsTree.size()<2 ) {
       while ( items.hasNext()  ) { // pick them all up, not just the first two in the list (that was a mistake)
           Page p = items.next();
           if(p.getProperties().get("cq:template","").equals("/apps/public/templates/news")){
               newsTree.add(p);
           }
           
       }
   }
   //  at this point you have zero, one or two CNW articles in the basket.
   //  UPDATE: You have zero to 1000 articles at this point. Since you are printing out the most recent five, do the truncation down lower.
   
   // 2. locate the two most recent Petri Dish articles. these articles
   //    are rss ingested into a known location, and they are all at the
   //    same heirarchical level, so no need to descend.
   //tmp.clear();

   //Page petridish;
   //if(null!=resourceResolver.getResource(pathToPetridish)){
   //    petridish = resourceResolver.getResource(pathToPetridish).adaptTo(Page.class);   
   //    Iterator<Page> children = petridish.listChildren();
   //    while(children.hasNext()){
   //        Page p = children.next();
   //        if(p.getProperties().get("cq:template","").equals("/apps/public/templates/news")){
   //            tmp.add(p);
   //        }
   //    }
   //    Iterator<Page> it = tmp.descendingIterator(); // For reverse chron
   //    int ct = 0;
   //    while ( it.hasNext() && ct<2 ) {
   //       Page p = it.next();
   //        newsTree.add(p);
   //        ct++;
   //    }  
   //} else {
   //    log.warn("recentarticlespicklist.jsp: Unable to locate Petri Dish content. Looked for: " + pathToPetridish);
   //}
   
   // at this point you have 0-2 CNW, plus 0, 1 or 2 Petri Dish articles
   

   // 3. locate the most recent Quest article. quest articles
   //    are rss ingested into a known location, and they are all at the
   //    same heirarchical level, so no need to descend.
   tmp.clear(); 

   //Page quest;
   //if(null!=resourceResolver.getResource(pathToHutchMagazine)){
   //    quest = resourceResolver.getResource(pathToHutchMagazine).adaptTo(Page.class);   
   //    Iterator<Page> children2 = quest.listChildren();
   //    while(children2.hasNext()){
   //        Page p = children2.next();
   //        if(p.getProperties().get("cq:template","").equals("/apps/public/templates/news")){
   //          tmp.add(p);
   //        }
   //    }   
   //    if(tmp.size()>0){ 
   //        newsTree.add(tmp.last()); 
   //    }
   //} else {
   //    log.warn("recentarticlespicklist.jsp: Unable to locate Quest content. Looked for: " + pathToHutchMagazine);
   //}
   
   // at this point you have 0-2 CNW, 0-2 Petri Dish articles, and 0 or 1 Quest article   
   
   
   
   
%>

<cq:include path="recentarticlesHeaderWidget" resourceType="public/components/content/header"/>

<div class="recentarticleslisting">

<% if(newsTree.size()<1) { %>
   <cq:include script="empty.jsp"/>
<% } %>

<%   
   // Finally, print this sucker out!
   Iterator<Page> printIterator = newsTree.descendingIterator(); // For reverse chron
   Integer j = 0; // new, Jan 23, 2014: print out the top 5 items because they are all selected editorially now and I have lots in the iterator
   
   out.print("<div class='related-news'>");
   while(printIterator.hasNext() && j<5 ){
       Page printMe = printIterator.next();
       out.print("<p>");
       out.print("<span class='news-title'>");
       out.print("<a href=\"" + printMe.getPath() + ".html\">");
       if(printMe.getTitle() == null){
           out.print(printMe.getName());
       } else {
           out.print(printMe.getTitle());
       }
       out.print("</a></span><br />");
       
       if(!omitDate){
         Date pubdate = printMe.getProperties().get("pubDate/date", printMe.getProperties().get("cq:lastModified", Date.class));
         out.print("<span class='news-date'>");
         out.print(FHUtilityFunctions.APStylize(pubdate));
         out.print("</span>");
       }
       if(!omitSource){
         String intext = omitDate ? "In " : " in ";
         out.print("<span class='news-source'>");
         String source="";
         if(printMe.getPath().contains("news/releases/")){ source = intext + "<a href='/content/public/en/news/releases.html'>News Releases</a>"; }
         if(printMe.getPath().contains("center-news/")){ source = intext + "<a href='/content/public/en/news.html'>Hutch News</a>"; }
         if(printMe.getPath().contains("petridish-import/")){ source = intext + "<a href='http://questmagazine.wordpress.com' target='_blank'>Petri Dish</a>"; } 
         if(printMe.getPath().contains("hutch-magazine/")){ source = intext + "<a href='/content/public/en/news/hutch-magazine.html'>Hutch Magazine</a>"; } 
         //if("".equals(source)){source=printMe.getPath();}
         out.print(source);
         out.print("</span>");
       }
       out.print("</p>");
       j++;
   }
   out.print("</div>");

   %>
   
   <% if(!properties.get("linkText","").equals("") && !properties.get("linkTarget","").equals("")){%>    
     <p class="more_link"><a href="<%= properties.get("linkTarget","") + ".html" %>"><cq:text property="linkText" /></a>&nbsp;<span class="raquo">&gt;</span></p>
   <%}%>   
   
   

</div>






