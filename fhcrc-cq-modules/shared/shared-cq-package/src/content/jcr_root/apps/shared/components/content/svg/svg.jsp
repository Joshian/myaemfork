<%@include file="/libs/foundation/global.jsp"%>
<jsp:useBean id="svgBean" scope="request" class="org.fhcrc.shared.components.Svg" />
<jsp:setProperty name="svgBean" property="resource" value="<%= resource %>"/>
<jsp:setProperty name="svgBean" property="currentStyle" value="<%= currentStyle %>"/>
<jsp:setProperty name="svgBean" property="currentDesign" value="<%= currentDesign %>"/>
<jsp:setProperty name="svgBean" property="resourceDesign" value="<%= resourceDesign %>"/>
<jsp:setProperty name="svgBean" property="request" value="<%= request %>"/>
<jsp:setProperty name="svgBean" property="writer" value="<%= out %>"/>
<jsp:setProperty name="svgBean" property="xssApi" value="<%= xssAPI %>"/>
<jsp:setProperty name="svgBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="svgBean" property="componentProperties" value="<%= properties %>"/>

<c:choose>

	<c:when test="${empty svgBean.svgHref }">
		<cq:include script="empty.jsp"/>
	</c:when>
	
	<c:otherwise>
		<object type="image/svg+xml" data="<%= svgBean.getSvgHref() %>">
			<% svgBean.drawImage(); %>
		</object>
	</c:otherwise>
	
</c:choose>
