
<%@ page import="org.apache.sling.api.*,
                 java.util.Date,com.day.cq.search.result.*,
                 com.day.cq.search.Query,
                 com.day.cq.search.QueryBuilder,
                 com.day.cq.search.eval.JcrPropertyPredicateEvaluator,
                 org.apache.sling.jcr.api.SlingRepository,
                 com.day.cq.search.PredicateGroup,
                 java.text.SimpleDateFormat,
                 java.util.Map,java.util.HashMap,
                 java.text.DateFormat,
                 javax.jcr.Session,
                 com.day.cq.wcm.api.WCMMode"%>
<%@include file="/libs/foundation/global.jsp"%><%
  try {
	Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
    QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);
    Query query =null;   
    SearchResult result =null;    
    Map<String, String> map =null;  
    String articleURL="";    
        map = new HashMap<String, String>();
        map.put("type", "cq:Page");
        map.put("group.p.or", "true");
        map.put("group.1_path", "/content/public/en/about");
        map.put("group.2_path", "/content/public/en/test");
        map.put("1_property","jcr:content/cq:template");
        map.put("1_property.value","/apps/public/templates/profile");
        query = builder.createQuery(PredicateGroup.create(map),session);
        result = query.getResult();//QUERYING JCR
        for (Hit hit : result.getHits()){   //iterating over the results 
            Page aPage = hit.getResource().adaptTo(Page.class);   
            %> - <%= aPage.getName() %><br><%
        	log.info("Page name = " + aPage.getName());
         }
  } catch (Exception ex) {
      log.error("Exception = " + ex);
  }
%>