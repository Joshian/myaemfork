<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.apache.commons.httpclient.*, 
                 org.apache.commons.httpclient.methods.*,
                 org.apache.commons.httpclient.params.HttpMethodParams,
                 java.io.IOException,
                 java.net.URLEncoder" %>
<%
//------------------------------------------------------
// Grab the query parameters froom the querystring sent
// in to this page.  Use the search API link provided.
// Make sure to attach the "key" to the querystring of
// the search API.  
//------------------------------------------------------
String queryString = request.getQueryString();
String searchAPILink = properties.get("./searchAPILink", "http://is.fhcrc.org/sites/public/apis/search-api/search-api.php");

boolean directory = properties.get("directory",Boolean.FALSE); // adds directory information to the search results.
String directory_num_hits = properties.get("dir_num_hits","3");

String searchKey = "key=public";
queryString = searchKey + "&" + queryString;
if(directory && request.getParameter("write_dir_info")==null){
  queryString = "write_dir_info=true&dir_type=people&dir_max=" + directory_num_hits + "&dir_format=json" + "&" + queryString;
}
searchAPILink = searchAPILink + "?" + queryString;
log.debug("Using Google Search Appliance with the following url:  " + searchAPILink);

HttpClient client = new HttpClient();
HttpMethod method = new GetMethod(searchAPILink);
try {
    int statusCode = client.executeMethod(method);
    if (statusCode != HttpStatus.SC_OK) {
      log.error("Method failed: " + method.getStatusLine());
    }

    String searchResponse = method.getResponseBodyAsString();
    //--------------------------------------------------------------------
    // Perform some string replacements on the results.  The result uses
    // a url with a path to the search results such as /search?q=cancer.  
    // We need it to be /search.html?q=cancer.
    // Also, on the advanced search page there are buttons with an action="search".
    // We need to make that action="search.html".
    //--------------------------------------------------------------------
    searchResponse = searchResponse.replaceAll("search\\?", "search.html?");
    searchResponse = searchResponse.replaceAll("action=\"search\"", "action=\"search.html\"");
    searchResponse = searchResponse.replaceAll("/user_help.html", currentPage.getPath() + "/user_help.html");
    searchResponse = searchResponse.replaceAll("<a href=", "<a x-cq-linkchecker=\"skip\" href="); // prevent linkchecker from checking google-provided links (128 max warning problem)
    log.debug("searchResponse = " + searchResponse);
    
    String q_encoded = "";
    if(request.getParameter("q")!=null) { q_encoded = URLEncoder.encode(request.getParameter("q"),"UTF-8"); }
%>
    <div id="googleapisearchresults">
       <%= searchResponse %>
       <% if(directory){ %>
       <script type="text/javascript">
         //addEvent(window,"load",function(){
            if(directory_results&&directory_results.people_found&&directory_results.people_fields){
                var msg = "";                
                msg+= "<h3 class='underline facultystaff'>Faculty &amp; Staff Directory Results</h3>";
                msg+= "<table cellspacing='0'>";
                msg+= "<tr><th scope='col'>Name</th><th scope='col'>Department</th><th scope='col'>Phone</th><th scope='col'>Email</th></tr>";
                $.each( directory_results.people, function(k,v){
                      msg+= "<tr>";
                      msg+= "<td>" + v[directory_results.people_fields.Name] + "</td>";
                      msg+= "<td>" + (v[directory_results.people_fields.Department]? v[directory_results.people_fields.Department] : "") + "</td>";
                      msg+= "<td>" + (v[directory_results.people_fields.Phone]?v[directory_results.people_fields.Phone]:"") + "</td>";
                      msg+= "<td>" + (v[directory_results.people_fields.Email]?"<a href='mailto:"+v[directory_results.people_fields.Email]+"'>"+v[directory_results.people_fields.Email]+"</a>":"") + "</td>";
                      msg+= "<td class='spacer'>&nbsp;</td>";
                      msg+= "</tr>";
                    } );
                msg+= "</table>";
                var prefix = location.host.substring(0,2)=='cq' ? '/content/public' : ''; // when in the cms need this extra pathing
                if( directory_results.people.length < directory_results.people_found ){
                    msg+= "<a href='"+ prefix +"/en/util/directory.html?q=<%= q_encoded %>&amp;short=true' title='Our full directory has more information and detail'>View " + (directory_results.people_found - directory_results.people.length) + " more result" + ((directory_results.people_found - directory_results.people.length)==1?'':'s')  + " and more details &gt;</a>";
                } else {
                    msg+= "<a href='"+ prefix +"/en/util/directory.html?q=<%= q_encoded %>&amp;short=true' title='Our full directory has more information and detail'>View more details in our full directory &gt;</a>";
                }
                msg+= "<h3 class='underline websiteresults'>Website Search Results</h3>";
                $("#directory_results_api_marker").html( msg );
            }
         //});
       </script>
       <% } %>
    </div>
<%
} catch (HttpException e) {
    log.error("Fatal protocol violation: " + e.getMessage());
    response.setStatus(404);
  } catch (IOException e) {
    log.error("Fatal transport error: " + e.getMessage());
    response.setStatus(404);
  } finally {
    method.releaseConnection();
  }  

%>