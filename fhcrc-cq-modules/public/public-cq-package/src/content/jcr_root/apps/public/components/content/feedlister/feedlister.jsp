<%@include file="/libs/foundation/global.jsp"%><% 
%>
<ul>
<%@ page import="java.util.Iterator, 
                 java.util.List,
                 java.util.ArrayList,
                 com.day.cq.wcm.api.PageFilter,
                 com.day.cq.wcm.api.WCMMode,
                 java.util.Date,
                 org.slf4j.Logger,
                 java.text.SimpleDateFormat" %><%
    
    WCMMode mode = WCMMode.fromRequest(request);
    SimpleDateFormat fmt = new SimpleDateFormat("MMMM d, yyyy");
    String startPath = properties.get("./startPath", "");
    List<Page> pageList = null;
    Page startPage = pageManager.getPage(startPath);
    if (startPage != null) {
    	pageList = showChildren(startPage, new ArrayList<Page>(), log);
    	for (Page aPage : pageList) {
            ValueMap pageProps = aPage.getProperties();
            String title = aPage.getTitle();
            String dateStr = "";
            Date date = pageProps.get("published", Date.class);
            if (date != null) {
            	dateStr = fmt.format(date);
            }
            String url = pageProps.get("id", String.class);
            String author = pageProps.get("author", String.class); 
            %>
            <li><a href="<%= url %>"><%= title %></a> : <%= dateStr %></li>
            <%       
        }
    }
    if (mode == WCMMode.EDIT && (pageList == null || pageList.isEmpty())) {
        %><cq:include script="empty.jsp"/><%
    }
%>
</ul>

<%!
   private List<Page> showChildren(Page page, List<Page> pageList, Logger log) throws Exception {
       log.debug("current page = " + page.getPath());
	   Iterator<Page> pageIt = page.listChildren();
       while (pageIt.hasNext()) {
         Page aPage = pageIt.next();
	     ValueMap pageProps = aPage.getProperties();
	     String title = aPage.getTitle();
	     Date date = pageProps.get("published", Date.class);
	     String url = pageProps.get("id", String.class);
	     String author = pageProps.get("author", String.class); 
	     if (url != null) {
	    	//-----------------------------------------------------
	    	// This is a valid feed page.
	    	//-----------------------------------------------------
	    	pageList.add(0, aPage);
	     }
	     pageList = showChildren(aPage, pageList, log);
      }
      return pageList;
   }
%>