<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.foundation.Image,
                 com.day.cq.commons.Doctype" %>

<!-- slideshow (homepage) or landingpage image (landing page).  -->
  <div id="slideshow">

      <!-- this div is nuked for general pages: -->
      <div class="slideshow-contents">
      
        <!-- this image represents either the slideshow or the landingpage image: -->
        
<%

Resource r = currentPage.getContentResource("image");
String imgSrc = "";

if (r != null) {
	Image image = new Image(r);
	if (image.hasContent()) {
		imgSrc = request.getContextPath().concat(currentPage.getPath()).concat(".img.png").concat(image.getSuffix());
%>
		<img src="<%= imgSrc %>">
<%
	}
}

%>

      </div>
        
  </div>