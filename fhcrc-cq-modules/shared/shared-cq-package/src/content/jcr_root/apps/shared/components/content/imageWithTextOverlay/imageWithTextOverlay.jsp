<%--

  Image with Text Overlay component.

  This component adds an image to the page that fades in some text on a semi-transparent colored
  background when hovered over.

--%>
<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode,
    org.fhcrc.tools.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%

    Boolean disabled = properties.get("disableWidget", false);

    // if the user has disabled this widget, don't display anything (except a placeholder)
    if(disabled){ %><cq:include script="empty.jsp"/><% return; }

    Image image = new Image(resource, "image");
    String linkTarget = properties.get("linkTarget","");
    
    String componentID = properties.get("componentId","");
    boolean hasID = componentID != null && !componentID.trim().equals("");
    String idString = "";
    if (hasID) {
        idString = "name=\"".concat(componentID).concat("\"");
    }

    // COLOR THEME:
    // colorTheme0, colorTheme1, etc. are controlled
    // via design dialog. In the design dialog configure
    // the allowed set of text & background colors like this:
    //
    //        0<tab>Normal (default)
    //        1<tab>Black text on white background
    //        2<tab>White text on yellow background
    //        #<tab>Text the user will see in the dropdown
    //        etc.
    //
    // you do not have to go sequentially, and can use multiple digits if you want.
    // now the user will be able to select a background color of your choosing.
    // this component places a class, colorTheme#, around the text div.
    // add the appropriate styling to your css for the colorTheme# choices you provide.
    
    
    // DESIGN THEME:
    // for complete generality, we add the ability to select a second class.
    // you can use this however you want. e.g., 
    //
    //        0<tab>None (default)
    //        1<tab>Narrow margins
    //        2<tab>Blinking text
    //        #<tab>Text the user will see in the dropdown
    //        etc.
    
     String colorthemes = properties.get("colortheme","0"); // default is theme 0
     //out.write(colorthemes);
     String[] c = colorthemes.split("\t");
     String color_theme_number = c[0];
     String colorTheme = "";
     if(!color_theme_number.isEmpty()){
         colorTheme = "colorTheme" + color_theme_number ;
     }
    
     String designthemes = properties.get("designtheme","0"); // default is theme 0
     //out.write(designthemes);
     c = designthemes.split("\t");
     String design_theme_number = c[0];
     String designTheme = "";
     if(!design_theme_number.isEmpty()){
       designTheme = "designTheme" + design_theme_number ;
     }    
     
     String decorations = properties.get("decorations",""); // default is blank
     //out.write(designthemes);
     c = decorations.split("\t");
     String decoration_code = c[0];
     if (decoration_code == null || decoration_code.trim().equals("")) {
         decoration_code = "";
     }
        
    Resource r;
    if(!linkTarget.trim().equals("")){
        linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
    }

    //remove title attribute unless there is something explicitly in the dialog
    if(properties.get("image/jcr:title","").trim().equals("")) {
        image.setTitle(" ");
    }
    
    image.loadStyleData(currentStyle);
    image.setSelector(".img"); // use image script
    image.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        image.setSuffix(currentDesign.getId());
    }

    if(!linkTarget.equals("")){ %>
    <a <%= idString %> href="<%= linkTarget %>" <%= properties.get("externalLink","false").equals("true") ? "target=\"_blank\"" : "" %>>
    <%
    }
    %>
       
    <div class="textOverlayContainer
        <%= colorTheme.equals("") ? "" : (" ").concat(colorTheme) %>
        <%= designTheme.equals("") ? "" : (" ").concat(designTheme) %>">
    
        <div class="textOverlayBackground">
        
            <div class="textContainer">
            
                <cq:text property="title" tagName="h3" tagClass="textOverlayTitle" />
                
                <cq:text property="text" tagName="span" tagClass="textOverlayDescription" />
                
<%-- Stopgap measure: hard coding this for now. It should be a design mode or dialog choice, but pressed for time --%>
<%

    if (false && !linkTarget.trim().equals("")) { // commenting this out for now, sld.
    
%>                
                <span class="linkText"><em>Click to read story</em>&nbsp;&gt;</span>
                
<%
    }
%>
<%-- End stopgap measure --%>                
            </div>
          
        </div>
            
        <%
        image.draw(out);
        %>
        
        <div class="mobileTitleContainer">
            <cq:text property="title" tagName="h3" tagClass="mobileTitle" placeholder="" />
        </div>
            
    </div>
    
    <%
    if(!linkTarget.equals("")){ %>
    </a>
    <% 
    }
    %>
    
    <cq:includeClientLib js="apps.shared.imageWithTextOverlay" />
