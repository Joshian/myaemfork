<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.WCMMode,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    org.fhcrc.tools.FHUtilityFunctions" %>
<%
String divWidth = properties.get("width", "full");
String underline = properties.get("isUnderlined", "false");
Image img = new Image(resource, "image");
Image mouseover = new Image(resource, "mouseover");
String mouseoverSrc = "";
String linkTarget = properties.get("linkTarget","");
Boolean isBordered = properties.get("bordered","false").equals("false") ? false : true;

if(!linkTarget.equals("")){
    linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
}

if(mouseover.hasContent()){
    mouseoverSrc = mouseover.getSrc();

    // If the mouseover image is not in the dam, we need to mess with the Src string to get mouseover.img.<file extension> rather than mouseover/file.<file extension>
    if(mouseoverSrc.contains("mouseover/file")){
        mouseoverSrc = mouseoverSrc.replaceAll("mouseover/file\\.", "mouseover.img.");
    }
}
%>
<div class="multipurpose clearfix<% 
if(properties.get("constrainHeight","false").equals("true")){
    out.print(" fiximageheight");
}
if(underline.equals("true")){ //Add the underline class if requested by the underline checkbox
    out.print(" underlinetitle");
}
if(divWidth.equals("full")){
    out.print(" unconstrained"); //Add the unconstrained class if the box is marked Full Width
}
if(!(img.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT)){ //Check to see if there is an image in the component and add the noimage class if there is not
    out.print(" noimage");
}
%>"><div<% 
if(divWidth.equals("wide")){
    out.print(" class=\"grid_8 alpha\"");
} else if(divWidth.equals("medium")){
    out.print(" class=\"grid_6 alpha\"");
} else if(divWidth.equals("narrow")){
    out.print(" class=\"grid_4 alpha\"");
}
%>><h3>
<cq:text property="title" placeholder="" />
</h3>
<%
if(img.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT){
    out.println("<div class=\"imagecolumn grid_2 alpha\">");
    img.loadStyleData(currentStyle);
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        img.setSuffix(currentDesign.getId());
    }
    
    if (img.hasContent()) {
	    img = FHUtilityFunctions.nukeURLPrefix(img, resourceResolver);
    }
    
    //drop target css class = dd prefix + name of the drop target in the edit config
    img.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
    if (isBordered) {
    	
    	img.addCssClass("frame");
    	
    }
    if(mouseover.hasContent()){
        img.addAttribute("data-hover", mouseoverSrc);
        img.addCssClass("rolloverImage");
    }
    //remove title attribute unless there is something explicitly in the dialog
    if(properties.get("image/jcr:title","").trim().equals("")) {
        img.setTitle(" ");
    }
    img.setSelector(".img");
    img.setDoctype(Doctype.fromRequest(request));
    // div around image for additional formatting
    out.println("<div class=\"image\">");
    // div for height constraint
    out.print("<div class=\"image_container\">");
    if(!linkTarget.equals("")){ %>
    <a href="<%= linkTarget %>" <%= properties.get("externalLink","false").equals("true") ? "target=\"_blank\"" : "" %>>
    <% }
    img.draw(out);
    if(!linkTarget.equals("")){ %></a><% }
    out.print("</div>");
    out.print("<p class=\"caption\">");
    %><cq:text property="caption" placeholder="" />
<%  out.print("</p></div></div>");
}
%>
<div class="textcolumn
<% 
if(img.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT){
    if(divWidth.equals("wide")){
        out.print(" grid_6 omega");
    } else if(divWidth.equals("medium")){
        out.print(" grid_4 omega");
    } else if(divWidth.equals("narrow")){
        out.print(" grid_2 omega");
    }
}
%>"><cq:text property="text" /></div></div></div>