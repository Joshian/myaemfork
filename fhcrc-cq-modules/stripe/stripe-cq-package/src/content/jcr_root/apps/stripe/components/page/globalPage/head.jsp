<%@include file="/apps/stripe/global.jsp" %>
<%@page import="com.day.cq.commons.Doctype,
                org.fhcrc.stripe.utilities.PageUtil,
                org.apache.commons.lang.StringEscapeUtils,
                com.day.cq.security.User,
                com.day.cq.security.UserManager,
                com.day.cq.security.UserManagerFactory,
                org.apache.sling.jcr.api.SlingRepository,
                com.day.cq.security.Group,
                org.apache.sling.settings.SlingSettingsService,
				java.util.Set" %>
<%
    String xs = Doctype.isXHTML(request) ? "/" : "";
    String favIcon = currentDesign.getPath() + "/clientlibs/img/favicon.ico";
    if (resourceResolver.getResource(favIcon) == null) {
           favIcon = null;
    }
   
%>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8"<%=xs%>>
<meta http-equiv="keywords" content="<%= StringEscapeUtils.escapeHtml(WCMUtils.getKeywords(currentPage, false)) %>"<%=xs%>>
<meta http-equiv="description" content="<%= StringEscapeUtils.escapeHtml(properties.get("jcr:description", "")) %>"<%=xs%>>
<meta name="viewport" content="width=device-width, initial-scale=1"<%=xs%>>
<!--[if lte IE 9]>
       <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

    <%-- do not launch the sidekick when the author is in public-reviewer group --%>
    <%
       Session adminSession = null;
       try {
         // only want to do this when we are in author, not publish:
         Set<String> runModes =  sling.getService(SlingSettingsService.class).getRunModes();
		 log.debug("RUNMODES = " + runModes);
		 if (runModes.contains("author")){
            boolean isReviewer = false;
            User currentUser = resourceResolver.adaptTo(User.class); 
            SlingRepository repo = sling.getService(SlingRepository.class);
            adminSession = repo.loginAdministrative(null);
            UserManager userManager = sling.getService(UserManagerFactory.class).createUserManager(adminSession);
            Group group = null;
            if(userManager.hasAuthorizable("public-reviewer")){
              group = (Group) userManager.get("public-reviewer"); 
              isReviewer = group.isMember(currentUser); 
              log.info("Public reviewer group exists. Current user " + (isReviewer?"IS":"IS NOT") + " a member of " + group.getID());
            } else {
              log.info("Public reviewer group does NOT exist. Check to be sure this is correct.");
            }
            //-------------------------------------------------------
            // If user is NOT a member of the reviewer group,  
            // permit launch of the sidekick.  
            //-------------------------------------------------------
            if(!isReviewer){%>
                <cq:include script="/libs/wcm/core/components/init/init.jsp"/>
            <%}
         }
       } catch (Exception ex) {
           log.error("Exception", ex);
       } finally {
            if (adminSession != null) {
                adminSession.logout();
            }          
       }
    %>

<cq:includeClientLib css="apps.stripetop" />
<cq:includeClientLib js="apps.stripetop" />
<link rel="stylesheet" href="/etc/designs/stripedesigns/stripe/clientlibs/css/print-friendly.css" media="print" />

<jsp:useBean id="pageUtilBean" scope="request" class="org.fhcrc.stripe.utilities.PageUtil" />
<jsp:setProperty name="pageUtilBean" property="resourceResolver" value="<%= resourceResolver %>"/>

<%
  String title = currentPage.getTitle();
  String nodePath = Text.getAbsoluteParent(currentPage.getPath(), 3);
  nodePath = nodePath.concat("/jcr:content/labtitle");
  String labTitle = pageUtilBean.getSiteConfigProperty(slingRequest, nodePath, "title");
  if(currentPage.getDepth() == 4)
  {
	  title = "";
  }
  else
  {
	  title = " -- " + title; 
  }
%>

<title><%=labTitle%><%=title%></title>

<% if (favIcon != null) { %>
    <link rel="icon" type="image/vnd.microsoft.icon" href="<%= favIcon%>"<%=xs%>>
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<%=favIcon %>"<%=xs%>>
<% } %>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-528883-5']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>