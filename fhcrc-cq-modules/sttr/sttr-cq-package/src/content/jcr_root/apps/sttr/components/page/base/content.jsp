<%@include file="/libs/foundation/global.jsp"%><%
%>

   <!--  body contents -->
   <div class="body_contents">

     <cq:include script="breadcrumbs.jsp" />

     <div class="page_content" id="page_content">
       <cq:include path="mainTitle" resourceType="foundation/components/title" />
       <cq:include path="mainPar" resourceType="foundation/components/parsys"/>
       <!-- remote-wrap-api-marker -->
     </div><!-- /page_content -->
     
     <nav><div class="sidebar">
       <div class="sidebar_contents">
          <cq:include script="section-navigation.jsp"/>
          <cq:include path="sidebarPar" resourceType="foundation/components/parsys"/>
       </div><!-- /sidebar_contents -->
     </div></nav><!-- /sidebar -->
     
     <div class="clear"></div>
     
   </div>
   <!-- /body_contents -->
   

