var componentApp = (function(componentApp) {
	
	componentApp.imageWithTextOverlayComponent = (function(imageWithTextOverlayComponent, $) {
		
		$(window).load(function(e) {
			
			imageWithTextOverlayComponent.alignTextCenter = function() {
				$(".imageWithTextOverlay").each(function(index, element) {
					var theText = $(this).find(".textContainer");
					var theMobileText = $(this).find(".mobileTitleContainer");
					var y1 = this.offsetHeight;
					var y2 = theText.outerHeight(false); //theText.height(); // want to include any padding that may be on the element, but not margin, sld.
					var y3 = theMobileText.outerHeight(false); //theMobileText.height();
					var theOffset = (y1 - y2) / 2;
					var theMobileOffset = (y1 - y3) / 2;
					theText.css("top", theOffset);
					theMobileText.css("top", theMobileOffset);	
				});
			}
			
            // adds some nodes as hooks that client can use to place an indicator on the panel
			imageWithTextOverlayComponent.addIndicator = function() {
				$(".imageWithTextOverlay .textOverlayContainer").after("<div class='indicator'><span></span></div>");
			}
			
			imageWithTextOverlayComponent.alignTextCenter();
//			$(window).resize(imageWithTextOverlayComponent.alignTextCenter);
			imageWithTextOverlayComponent.addIndicator();
			
			$(".textOverlayBackground").css('visibility','visible').hide(); // added .css() to reduce flash, c.f. css. sld.
			$(".imageWithTextOverlay").mouseenter(function() {
				if ($(".mobileTitleContainer").css("display") !== "none") {
					return;
				}
				$(this).find(".textOverlayBackground").stop().css('opacity','1').show(); //fadeIn(400);
			}).mouseleave(function() {
				if ($(".mobileTitleContainer").css("display") !== "none") {
					return;
				}
				$(this).find(".textOverlayBackground").stop().fadeOut(400);
			});
			
			
		});
		
		return imageWithTextOverlayComponent;

	})(componentApp.imageWithTextOverlayComponent || {}, jQuery);
	
	return componentApp;
	
})(componentApp || {});