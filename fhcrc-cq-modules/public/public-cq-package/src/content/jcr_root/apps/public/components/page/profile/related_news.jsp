<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text, 
                 java.util.Iterator, 
                 com.day.cq.tagging.TagManager, 
                 com.day.cq.tagging.Tag, 
                 com.day.cq.commons.RangeIterator, 
                 org.apache.commons.lang.StringEscapeUtils, 
                 java.util.TreeSet, 
                 java.util.Comparator, 
                 java.util.Date, 
                 com.day.cq.wcm.api.WCMMode, 
                 java.text.SimpleDateFormat,
                 java.text.DateFormat, 
                 org.fhcrc.publicsite.constants.Constants, 
                 org.fhcrc.tools.FHUtilityFunctions" %>
<%
/* Variable Declaration */
TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
Tag[] allTags = tagManager.getTags(resource);
Tag facultyTag = null;
int tagCount = 0;
String[] basePaths = {"/content/public/en/news"};
String newsSource = "";
String newsSourceLink = "";
/* This TreeSet will eventually contain all the pages I want to list in the UL tag, ordered by publication date */
TreeSet<Page> programsTree = new TreeSet<Page>(new Comparator<Page>(){
    public int compare(Page a, Page b){
        Date dateA = a.getProperties().get("pubDate/date", Date.class);
        Date dateB = b.getProperties().get("pubDate/date", Date.class);
        if(dateA==null){
            dateA = new Date();
        }
        if(dateB==null){
            dateB = new Date();
        }
        if(dateA.equals(dateB)){
            return b.getPath().compareTo(a.getPath());
        }
        return dateA.compareTo(dateB);
    }
});
/* I only care about tags in the web-faculty namespace, and I want to make sure there is one and only one */
for(Tag t : allTags){
    if(t != null){
    if(t.getNamespace().getName().equals("web-faculty")){
        tagCount++;
        facultyTag = t;
    }
    }
}

/* Don't print out anything if there is no Faculty tag on this page */
if(facultyTag != null || WCMMode.fromRequest(request) == WCMMode.EDIT){
%>
<h3 class="underline">Related News</h3>
<%
if(tagCount == 0){
    out.print("<p>Unable to determine related news.<br/><br/>Please add the faculty tag for this page using Page Properties. If the appropriate faculty tag is missing, contact webadm@fhcrc.org</p>");
} else if(tagCount == 1){
/* 
Here's the workhorse. I plow through all the resources under each of the basePaths, and grab all the ones that are tagged
with the same faculty tag as this page. Then, I add any of those resources that are Pages to the TreeSet.
*/
    String[] facultyTagArray = {facultyTag.getTagID()};
    for(String s : basePaths){
        RangeIterator<Resource> taggedProjects = tagManager.find(s, facultyTagArray, true);
        if(taggedProjects != null) {
            while(taggedProjects.hasNext()){
                Resource project = taggedProjects.next().getParent(); //Since the jcr:content is the thing that is tagged, and we want the Page
                if(project.isResourceType("cq:Page")){ // I only care about elements that are Pages
                    Page projectPage = resourceResolver.getResource(project.getPath()).adaptTo(Page.class);
                    if(projectPage.getProperties().get("cq:template","").equals("/apps/public/templates/news")){ // I only want to list pages of the "News page" type
                        programsTree.add(projectPage);
                    }
                }
            }
        }
    }
    Iterator<Page> printIterator = programsTree.descendingIterator(); // For reverse chron
%>
    <ul class="flush nobullets">
<%

    DateFormat f = new SimpleDateFormat("MMM d, yyyy");
    while(printIterator.hasNext()){
        Page printMe = printIterator.next();
/* Find the source of the news by using the page's path attribute */        
        String pagePath = printMe.getPath();
        if(pagePath.contains(Constants.PATH_TO_NEWSRELEASES_FOLDER)) {
            newsSource = "News Releases";
            newsSourceLink = Constants.PATH_TO_NEWSRELEASES_FOLDER+".html";
        } else if(pagePath.contains(Constants.PATH_TO_CENTERNEWS_FOLDER)) {
            newsSource = "Hutch News";
            newsSourceLink = Constants.PATH_TO_NEWS_FOLDER+".html";
        } else if(pagePath.contains(Constants.PATH_TO_HUTCH_MAG_IMPORTER_FOLDER) || pagePath.contains(Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER)) {
            newsSource = "Hutch Magazine";
            newsSourceLink = Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER+".html";
        } else if(pagePath.contains(Constants.PATH_TO_PETRIDISHIMPORTER_FOLDER)) {
            newsSource = "Petri Dish";
            newsSourceLink = "http://questmagazine.wordpress.com/";
        } else if(pagePath.contains(Constants.PATH_TO_SCIENCESPOTLIGHT_FOLDER)) {
            newsSource = "Science Spotlight";
            newsSourceLink = Constants.PATH_TO_SCIENCESPOTLIGHT_FOLDER+".html";
        } else {
            newsSource = "News";
            newsSourceLink = Constants.PATH_TO_NEWS_FOLDER+".html";
        }
        out.print("<li class='related-news'>");
        out.print("<div class='news-title'>");
        out.print("<a href=\"" + printMe.getPath() + ".html\">");
        out.print(FHUtilityFunctions.displayTitle(printMe));
        out.print("</a></div>");
        
        Date pubdate = printMe.getProperties().get("pubDate/date", printMe.getProperties().get("cq:lastModified", Date.class));
        out.print("<span class='news-date'>");
        //out.print(f.format(pubdate));
        out.print(FHUtilityFunctions.APStylize(pubdate));
        out.print("</span>");
        out.print("<span class='news-source'>");
        out.print(" in <a href='" + newsSourceLink + "'>" + newsSource + "</a>");
        out.print("</span>");
        
        out.print("</li>");
    }
%>    

      <li><a href="<%=Constants.PATH_TO_NEWS_FOLDER %>.html">View all news</a> &gt;</li>
    </ul>
<%  
} else {
    out.print("There are multiple faculty tags on this page.");
}
}
%>
    