<%@include file="/libs/foundation/global.jsp"%>
<header>

	<cq:include script="heroImage.jsp"/>

</header>

<section>

	<div id="wrapper-content-outer">
	            
		<div id="wrapper-content">
	
			<div id="content-primary" class="content-primary">
			
				<cq:include path="title" resourceType="foundation/components/title"/>
	               
				<cq:include path="par" resourceType="foundation/components/parsys"/>
	
				<div class="clear"></div>
				<!-- remote-wrap-api-marker -->
				
				<nav>
				
					<cq:include script="mobileSectionNav.jsp"/>
				
				</nav>
				              
			</div>
	            
		</div>
	    
	</div>

</section>