package org.fhcrc.stripe.components;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import org.apache.sling.api.SlingHttpServletRequest;
/**
 * @author mruival
 *
 */
public class Logo {

    private Style currentStyle;

    private Design currentDesign;

    private Page currentPage;

    /** default constructor
     * @param slingHttpServletRequest
     */
    public Logo(SlingHttpServletRequest slingHttpServletRequest ){}

    public Logo(Style currentStyle, Design currentDesign, Page currentPage){
    	this.currentStyle = currentStyle;
    	this.currentDesign = currentDesign;
    	this.currentPage = currentPage;
    }
    
    public Logo(){ 
    	
    }
    
    /**
     * @return the link of the logo
     */
    public String getLink(){
    	//long absParent = currentStyle.get("absParent", 0L);
        //String home = Text.getAbsoluteParent(currentPage.getPath(), (int) absParent);
        return (String) currentStyle.get("link","http://fredhutch.org");
    }

    /**
     * @return the path of the logo image
     */
    public String getSourceImage(){
        return (String) currentStyle.get("imageReference","/etc/designs/stripedesigns/stripe/clientlibs/img/Fred_Hutch_250x40.png");
    }

    /**
     * @return the alternative text of the logo image
     */
    public String getAltText(){
        return (String) currentStyle.get("alt_text","FHCRC logo");
    }    

    //default getters
    public Page getCurrentPage() {
		return currentPage;
	}

	public Style getCurrentStyle() {
        return currentStyle;
    }

    public Design getCurrentDesign() {
        return currentDesign;
    }

    //default setters
	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}

    public void setCurrentStyle(Style currentStyle) {
        this.currentStyle = currentStyle;
    }

    public void setCurrentDesign(Design currentDesign) {
        this.currentDesign = currentDesign;
    }
}