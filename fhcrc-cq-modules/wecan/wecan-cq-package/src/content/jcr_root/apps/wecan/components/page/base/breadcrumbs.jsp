<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text,
        org.apache.commons.lang.StringEscapeUtils,
        com.day.cq.wcm.api.Page,
        org.fhcrc.publicsite.constants.Constants,
        org.fhcrc.tools.FHUtilityFunctions" %>
        
      <nav class="breadcrumbs noprint" id="breadcrumbs">
        <ul>
          <li class="homeCrumb<%= (currentPage.getDepth()==4?" parent": "") %>"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>.html">Home</a></li>
<%
int level = 3;
int currentLevel = currentPage.getDepth() - 1;
String currentTitle = FHUtilityFunctions.displayTitle(currentPage);

while (level < currentLevel) {
    Page trail = currentPage.getAbsoluteParent(level);
    if (trail == null) {
        break;
    }
    String title = FHUtilityFunctions.displayTitle(trail);
    String cls = (currentLevel-level==1)? " class='parent'" : "";
    %><li<%=cls%>><a href="<%= Text.escape(trail.getPath(), '%', true) %>.html"><%
    %><%= StringEscapeUtils.escapeHtml(title) %><%
    %></a></li><%

    level++;
}
%>
          
          <li><%= currentTitle %></li>
        </ul>
      </nav>