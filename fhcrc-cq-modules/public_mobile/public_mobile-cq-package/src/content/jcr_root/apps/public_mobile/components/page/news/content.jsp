<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text, java.util.Iterator, com.day.cq.wcm.api.WCMMode, org.fhcrc.publicsite.constants.Constants" %>
   
   <div class="fh_content"><a name="jump_content"></a>
    <!-- content begins here: -->

    <cq:include path="title" resourceType="foundation/components/title"/>
    
    <!--  SUBTITLE -->
    <% String subtitle = properties.get("newssubtitle/text",""); %>
    <% if( !"".equals(subtitle) ){%>
    <h2><cq:include path="newssubtitle" resourceType="public/components/content/textarea"/></h2>
    <% } %>
    
    <!--  PUBLICATION DATE -->
    <!-- <p class="newspage-date"><cq:include path="pubDate" resourceType="public/components/content/date"/></p>-->
    
    <!--  BYLINE -->
    <% String byline = properties.get("byline/text",""); 
       if(!byline.equals("")){%>
       <p class="newspage-byline"><cq:text property="byline/text"/></p>
    <% } %>        
    
    <!-- TEXT -->        
    <cq:include path="articletext" resourceType="foundation/components/parsys"/>
    
    <cq:include path="socialshare_template" resourceType="public_mobile/components/content/socialshare"/>
    
    <!-- content ends here -->
    <!-- remote-wrap-api-marker -->
    
   </div><!-- /fh_content -->    
   
     