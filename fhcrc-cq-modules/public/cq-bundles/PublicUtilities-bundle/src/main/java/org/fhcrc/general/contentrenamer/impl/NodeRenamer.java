package org.fhcrc.general.contentrenamer.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.fhcrc.general.contentrenamer.Renamer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.PageManagerFactory;


@Component
@Service
public class NodeRenamer implements Renamer {

	
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Reference
	private ResourceResolverFactory resolverFactory;

    @Reference
    PageManagerFactory pageManagerFactory;
    
    ResourceResolver resolver = null;
    private Session adminSession = null;
    private PageManager pageManager = null;
    private List<String> results = null;
    
    public List<String> rename(String startPath, String findNodeName, String replacementName, boolean isTest) {
		results = new ArrayList<String>();
    	log.info("Renaming");
		try {
			 resolver = resolverFactory.getAdministrativeResourceResolver(null);
			 adminSession = resolver.adaptTo(Session.class);
			 pageManager = pageManagerFactory.getPageManager(resolver);
		     if (pageManager != null) {
	    	   Page startPage = pageManager.getPage(startPath);
	    	   rename(startPage, findNodeName, replacementName, isTest);
	         } else {
	    	   log.error("PageManager is null");
	         }
		} catch (LoginException e) {
			log.error("", e);
		} catch (PathNotFoundException e) {
			log.error("", e);
		} catch (RepositoryException e) {
			log.error("", e);
		} finally {
			if (adminSession != null) {
				adminSession.logout();
			}
		}
	    return results;
	}

    private void rename(Page page, String findNodeName, String replacementName, boolean isTest) throws RepositoryException {
    	if (page != null) {
 	       Resource targetResource = page.getContentResource().getChild(findNodeName);	
 	       if (targetResource != null) {
 	    	   log.info("Found Target Resource = " + targetResource.getPath());
     	 	   try {    
	    	       @SuppressWarnings("unused")
				   Node targetNode = adminSession.getNode(targetResource.getPath());
	    		   String newNodeName = targetResource.getParent().getPath() + "/" + replacementName;
	    		   if (isTest) {
	    			   results.add("Would have rename : " + targetResource.getPath() + " to " + newNodeName);
	    		   } else {
	    			   try {
						  adminSession.move(targetResource.getPath(), newNodeName);
						  adminSession.save();	    			   
		    			  results.add("Renamed " + targetResource.getPath() + " to " + newNodeName);
	    			   } catch (PathNotFoundException e) {
	     				  log.error("Resource exists but could not be renamed.", e);
		 			   } catch (RepositoryException e) {
		 				  log.error("Resource exists but could not be renamed.", e);
		 			   }
	    		   }
			   } catch (PathNotFoundException e) {
  				  log.info("Resource exists but it is not a node.  Skipping.");
	 		   }
 	       }
	       Iterator<Page> childrenIt = page.listChildren();
	       while (childrenIt.hasNext()) {
	   		   rename(childrenIt.next(), findNodeName, replacementName, isTest);
	       }
 	   } else {
 		   log.error("Start page not found.");
 	   }
    	
    }
}
