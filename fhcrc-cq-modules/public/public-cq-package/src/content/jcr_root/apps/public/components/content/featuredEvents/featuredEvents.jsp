<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.foundation.Image,
                 com.day.cq.wcm.api.WCMMode,
                 java.util.ArrayList,
                 java.util.List,
                 com.day.cq.commons.Doctype,
                 org.fhcrc.tools.FHUtilityFunctions" %>
<%

final String SEE_MORE_LINK_TEXT = "See all events";
final String SEE_MORE_LINK_LOCATION = "/content/public/en/events.html";
final String CATEGORY = "Upcoming Events";

String[] events = properties.get("events",String[].class);

String colorthemes = properties.get("colortheme","0"); // default is theme 0
//out.write(colorthemes);
String[] c = colorthemes.split("\t");
String color_theme_number = c[0];
String colorTheme = "";
if(!color_theme_number.isEmpty()){
	colorTheme = "colorTheme" + color_theme_number ;
}

String designthemes = properties.get("designtheme","0"); // default is theme 0
//out.write(designthemes);
c = designthemes.split("\t");
String design_theme_number = c[0];
String designTheme = "";
if(!design_theme_number.isEmpty()){
designTheme = "designTheme" + design_theme_number ;
}

if (events != null && events.length > 0) {
	List<String> list =  new ArrayList<String>();

	// iterate over each event
	for ( String event : events ) {
	    if (resourceResolver.getResource(event) != null) {
	        list.add(event);
	    } else {
            log.error("featuredEvents.jsp :: Event page (" + event + ") does not exist or is not activated.");
            
	    	if (WCMMode.fromRequest(slingRequest) == WCMMode.EDIT) {
	    		// TODO: add log statement here
	    %>
	    	<span class="error"><strong>Warning:</strong> <%=event %> is not yet activated.</span><br />
	    <%
	    	}
	    }
	}

	events = list.toArray(new String[list.size()]);
}

if(events != null && events.length > 0){
/* The limit of events to display is 3, but if they have selected fewer, that is fine, we just need to make note of it here */	
    int eventNumber = 3;
    if(events.length < eventNumber){
        eventNumber = events.length;
    }
%>
    <div class="featuredevents forceSquareBullet <%= colorTheme %> clearfix<%= properties.get("omitImage","false").equals("true") ? " omitfeaturedimage" : "" %>">
<%
/* Loop through the selected events */    
    for(int i = 0; i < eventNumber; i++) {
    	Page thePage = resourceResolver.getResource(events[i]).adaptTo(Page.class);
/* The first listed event gets special treatment */
    	if(i==0){
%>
            <div class="mainimage"><a href="<%= thePage.getPath() %>.html"><%

/* trying a new way, see carousel.jsp: */
   			Resource r = thePage.getContentResource("image");
   			String img;
   			if (r != null) {
			    
   				Image image = new Image(r);
			    img = thePage.getPath() + ".img.png" + image.getSuffix(); // e.g.,: /content/public/en/events/events/hutch-award.img.png/1323194909406.gif
			    // prepend context path to img
		        img = request.getContextPath() + img;
			    img = resourceResolver.map(img);
			    out.print("<img src='"+img+"'>");
		   
		    } else {
/* Default image (shouldn't happen, but just in case) */
%>
                <img src="<%= currentDesign.getPath() %>/img/defaults/eventicon_lg.jpg" alt="Calendar with date circled" />
<%
		    }
/* Print out the event title, date display (if requested), and description */
%>
                </a></div>

				<div class="text_container">
				
					<h2 class="category"><%= CATEGORY %></h2>
					<a href="<%= thePage.getPath() %>.html"><h3 class="title"><%= FHUtilityFunctions.displayTitle(thePage) %></h3></a>
					
					<div class="date_and_description">
<%
            if(properties.get("showDate","false").equals("true") && !thePage.getProperties().get("dateDisplay","").trim().equals("")){
            	String dateDisplay = thePage.getProperties().get("dateDisplay","");
            	dateDisplay = dateDisplay.replaceAll("\n", "<br />"); // Turn any newline characters into break tags
%>
                		<div class="date"><%= dateDisplay %></div>
<%
            }
%>					
						<div class="description"><%= thePage.getDescription() != null ? thePage.getDescription() : "" %></div>
					
					</div>
					
					<ul class="eventlist">

<%
    	} else {
/* Print out the remaining events, which just get title and link */
%>
			
            			<li><div class="title"><a href="<%= thePage.getPath() %>.html"><%= FHUtilityFunctions.displayTitle(thePage) %></a></div></li>
<%
    	}
    }
%>
    				</ul>
    
    				<div class="link_container">
						<a href="<%= SEE_MORE_LINK_LOCATION %>"><%= SEE_MORE_LINK_TEXT %></a>
					</div>
    				
    			</div>
<%   
    
} else {

%>
<cq:include script="empty.jsp" />
<% } %>
</div>