<%@include file="/libs/foundation/global.jsp"%><%
%>

  <!-- body contents -->
  <div class="body" id="body_element">
    <div class="slider">
      <div class="slider_contents">
        <cq:include script="slider.jsp"/>
      </div>
    </div>
    <div class="body_contents">
      <div class="page_content" id="page_content">      
        <div class="main_column">
          <cq:include path="mainpar" resourceType="foundation/components/parsys"/>
        </div>   
        <div class="clear"></div>       
      </div>
      <div class="clear"></div>
    </div>
  </div>