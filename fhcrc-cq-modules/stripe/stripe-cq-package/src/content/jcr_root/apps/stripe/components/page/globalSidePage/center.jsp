<%@include file="/apps/stripe/global.jsp" %>

<jsp:useBean id="pageUtil" scope="request" class="org.fhcrc.stripe.utilities.PageUtil" />
<jsp:setProperty name="pageUtil" property="currentPage" value="<%= currentPage %>"/>
<%--
  <c:if test="${!empty pageUtil.currentPageDescription}" >
	  <div  class="page_description">
            <c:out value="${pageUtil.currentPageDescription}" />
	  </div>
  </c:if>
 --%>  
  <cq:include script="subheader.jsp" />
  
  <div class="center-parsys body_content">
       <cq:include path="par" resourceType="foundation/components/parsys"/>
  </div>
 