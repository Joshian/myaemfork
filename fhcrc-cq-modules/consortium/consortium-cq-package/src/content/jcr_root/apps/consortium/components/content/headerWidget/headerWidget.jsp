<%@include file="/libs/foundation/global.jsp"%><%@ page import="com.day.cq.wcm.api.WCMMode" %>
<%
String underline = properties.get("omitUnderline", "false");
String classText = "headerContainer";
Resource r; 

if(underline.equals("true")){
	classText += " noUnderline";
}

%>
<div class="<%= classText %>">
    <cq:text property="text" tagName="<%= properties.get("headerSize","h2") %>" tagClass="title" />
</div>