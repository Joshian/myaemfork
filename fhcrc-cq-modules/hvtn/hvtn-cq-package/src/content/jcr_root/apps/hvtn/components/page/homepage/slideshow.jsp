<%@include file="/libs/foundation/global.jsp"%>

<!-- slideshow (homepage) or landingpage image (landing page).  -->
  <div id="slideshow">

      <!-- this div is nuked for general pages: -->
      <div class="slideshow-contents">
      
        <!-- this image represents either the slideshow or the landingpage image: -->
        
        <!-- here is a sample slideshow image for homepage: -->
        <!--<img src="http://is.fhcrc.org/users/ischempp/projects/HVTN/releases/8/images/placeholder-slideshow-01.jpg">-->
        
        <cq:include path="royalslider" resourceType="hvtn/components/content/royalslider"/> 
        
      </div>
        
  </div>