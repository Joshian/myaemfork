<%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default head script.

  Draws the HTML head with some default content:
  - includes the WCML init script
  - includes the head libs script
  - includes the favicons
  - sets the HTML title
  - sets some meta data

  ==============================================================================

--%><%@include file="/libs/foundation/global.jsp" %><%
%><%@ page import="com.day.cq.commons.Doctype,
                   org.apache.commons.lang.StringEscapeUtils,
                 com.day.cq.security.User,
                 com.day.cq.security.UserManager,
                 com.day.cq.security.UserManagerFactory,
                 org.apache.sling.jcr.api.SlingRepository,
                 com.day.cq.wcm.api.WCMMode,
                 com.day.cq.wcm.api.components.DropTarget,
                 com.day.cq.security.Group,
                 com.day.cq.i18n.I18n,
                 java.util.ResourceBundle,
                 com.day.cq.wcm.foundation.Image,
                 org.apache.sling.api.resource.*,
                 org.apache.sling.settings.SlingSettingsService,
        				 java.util.Set,
                 java.util.Date,
                 java.text.SimpleDateFormat,
                 java.text.FieldPosition" %><%
    String xs = Doctype.isXHTML(request) ? "/" : "";
    String favIcon = currentDesign.getPath() + "/img/favicon.ico";

    final String FACEBOOK_SITE_NAME = "Fred Hutch";
    final String FACEBOOK_PUBLISHER = "https://www.facebook.com/HutchinsonCenter";
    final String FACEBOOK_LOCALE = "en_US";
    final String FACEBOOK_APP_ID="10420423202";
    
    final String TWITTER_CARD_DEFINITION = "summary_large_image";
    final String TWITTER_SITE = "@fredhutch";

    Date pubdate = currentPage.getProperties().get("pubDate/date", currentPage.getProperties().get("cq:lastModified", Date.class)); 
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
    StringBuffer dateBuffer = new StringBuffer();
    FieldPosition fpos = new FieldPosition(1);
    dateBuffer = df.format(pubdate, dateBuffer, fpos);
    String dateString = dateBuffer.toString();
    
    String socialTitle = StringEscapeUtils.escapeHtml(currentPage.getTitle() == null ? currentPage.getName() : currentPage.getTitle());
    String socialDescription = StringEscapeUtils.escapeHtml(properties.get("jcr:description", ""));
    String socialImage = "";

    if (resourceResolver.getResource(favIcon) == null) {
        favIcon = null;
    }
    /* Set meta robots options. Current options are follow/nofollow and index/noindex */
    String robotsOptions;
    if (properties.get("noindex","false").equals("true")) {
    	
    	if (properties.get("nofollow","false").equals("true")) {
    		
    		robotsOptions = "noindex, nofollow";
    		
    	} else {
    		
    		robotsOptions = "noindex, follow";
    		
    	}
    	
    } else if (properties.get("nofollow","false").equals("true")) {
    	
    	robotsOptions = "index, nofollow";
    	
    } else {
    	
    	robotsOptions = "all";
    	
    }
    
    /* Check to see if the page has an image in the image tab, which we will use later as a Facebook og:image */
    
    Boolean hasImage = false;
    Resource imageResource = currentPage.getContentResource("image");
    
    if (imageResource != null) {
    	
    	Image image = new Image(imageResource);
    	
    	if (image.hasContent()) {
    		
    		hasImage = true;
    		
    	}
    	
    }
    
    // the news landing page (just that one page) has special robots rules depending upon what the newsroll is being asked for
    String [] selectors = slingRequest.getRequestPathInfo().getSelectors(); // if ANY selector is in place, do not index (and do not follow) (selectors are: by tag, by date, by page)
    if(  selectors.length>0 &&
         currentPage.getPath().equals("/content/public/en/news")         
     ){           
        robotsOptions = "noindex, nofollow";     
      }

    int hashCode = resource.hashCode();
    
%><head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"<%=xs%>>
    <meta name="keywords" content="<%= StringEscapeUtils.escapeHtml(WCMUtils.getKeywords(currentPage, false)) %>"<%=xs%>>
    <meta name="description" content="<%= StringEscapeUtils.escapeHtml(properties.get("jcr:description", "")) %>"<%=xs%>>
    <meta name="robots" content="<%= robotsOptions %>"<%=xs%>>
    
    <%-- Facebook image/title/description --%>
    <link rel="image_src" href="https://www.fredhutch.org<%=currentDesign.getPath()%>/img/facebook_share.png"<%=xs%>>
    
    <% if (hasImage) { 
    
        socialImage = currentPage.getPath() + ".pagethumb.resize.1200.630.rnd"+ hashCode +".png"; // https://developers.facebook.com/docs/sharing/best-practices#tags
        socialImage = resourceResolver.map(socialImage);
    
    %>
    <meta property="og:image" content="https://www.fredhutch.org<%= socialImage %>"<%=xs%>>
    <meta property="og:image:width" content="1200"<%=xs%>>
    <meta property="og:image:height" content="630"<%=xs%>>
    <% } else { %>    
    <meta property="og:image" content="https://www.fredhutch.org<%=currentDesign.getPath()%>/img/facebook_share.png"<%=xs%>>
    <% } %>
    <meta property="og:title" content="<%= socialTitle %>"<%=xs%>>
    <meta property="og:description" content="<%= socialDescription %>"<%=xs%>> 

    <%-- Extra Facebook information for news pages --%>

    <meta property="article:publisher" content="<%=FACEBOOK_PUBLISHER%>"<%=xs%>>
<% 
if (dateString != null && !dateString.trim().equals("")) {
%>
    <meta property="article:published_time" content="<%=dateString%>"<%=xs%>>
<%
}
%>

    <meta property="og:site_name" content="<%=FACEBOOK_SITE_NAME%>"<%=xs%>>
    <meta property="og:type" content="article"<%=xs%>>
    <meta property="og:locale" content="<%=FACEBOOK_LOCALE%>"<%=xs%>>
    <meta property="og:url" content="http://www.fredhutch.org<%=resourceResolver.map(currentPage.getPath())%>.html"<%=xs%>>

    <meta property="fb:admins" content="<%=FACEBOOK_APP_ID%>"<%=xs%>>
    <meta property="fb:pages" content="<%=FACEBOOK_APP_ID%>"<%=xs%>>
    
    <%-- Twitter Summary card information --%>
    <meta name="twitter:card" content="<%= TWITTER_CARD_DEFINITION %>">
    <meta name="twitter:site" content="<%= TWITTER_SITE %>">
    <meta name="twitter:title" content="<%= socialTitle %>">
    <meta name="twitter:description" content="<%= socialDescription %>">
    <% if (socialImage != null && !socialImage.trim().equals("")) { %>
    <meta name="twitter:image" content="https://www.fredhutch.org<%= socialImage %>">
    <% } %>
    
    <%-- This meta tag forces IE to use the most recent engine to render our pages --%>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
    <%-- headlibs.jsp provides four items:
         
         <script type="text/javascript" src="/etc/clientlibs/foundation/jquery.js"></script>
         <script type="text/javascript" src="/etc/clientlibs/foundation/main.js"></script>
         <link rel="stylesheet" href="/etc/clientlibs/foundation/main.css" type="text/css"/>
         <link href="/etc/designs/public.css" rel="stylesheet" type="text/css"/>
         
         however we don't need all four. therefore i'll comment out headlibs.jsp 
         and just include what i want.
         
         ok, i realize now that cq's query.js (1.4) is necessary because their entire
         authoring is based on it...so let's just leave this as-is.    --%>
    <cq:include script="headlibs.jsp"/>
    <%--<script type="text/javascript" src="/etc/clientlibs/foundation/main.js"></script>
    <link rel="stylesheet" href="/etc/clientlibs/foundation/main.css" type="text/css"/>--%>
     
    <cq:includeClientLib css="apps.public-main"/>

    <%-- webfonts must include their licensing within the css comments, so factor those out separately: --%>
    <link rel="stylesheet" href="<%=currentDesign.getPath()%>/clientlibs/css/webfonts-geogrotesque.css"/>
    
    <cq:includeClientLib js="apps.public-main"/>

    <%-- all pages except the homepage need a print.css --%>
    <% if(!currentPage.getProperties().get("cq:template","").equals("/apps/public/templates/homepage")){ %>
    <link rel="stylesheet" href="<%=currentDesign.getPath()%>/clientlibs/css/print.css" type="text/css" media="print" />
    <% } %>


    <!--[if gte IE 9]>
      <style type="text/css">
        /* http://www.colorzilla.com/gradient-editor/ */
        .gradient {
           filter: none;
        }
      </style>
    <![endif]-->

    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<%=currentDesign.getPath()%>/clientlibs/css/ie9.css" />
    <![endif]-->

    <!--[if lte IE 8]>
    <link rel="stylesheet" type="text/css" href="<%=currentDesign.getPath()%>/clientlibs/css/ie8.css" />
    <![endif]-->    
    
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="<%=currentDesign.getPath()%>/clientlibs/css/ie7.css" />
    <![endif]-->  


    <%-- do not launch the sidekick when the author is in public-reviewer group --%>
    <%
       Session adminSession = null;
       try {
   	     // only want to do this when we are in author, not publish:
   	     Set<String> runModes =  sling.getService(SlingSettingsService.class).getRunModes();
		 log.debug("RUNMODES = " + runModes);
		 if (runModes.contains("author")){
            boolean isReviewer = false;
            User currentUser = resourceResolver.adaptTo(User.class); 
            SlingRepository repo = sling.getService(SlingRepository.class);
            adminSession = repo.loginAdministrative(null);
            UserManager userManager = sling.getService(UserManagerFactory.class).createUserManager(adminSession);
            Group group = null;
            if(userManager.hasAuthorizable("public-reviewer")){
              group = (Group) userManager.get("public-reviewer"); 
              isReviewer = group.isMember(currentUser); 
              log.info("Public reviewer group exists. Current user " + (isReviewer?"IS":"IS NOT") + " a member of " + group.getID());
            } else {
              log.info("Public reviewer group does NOT exist. Check to be sure this is correct.");
            }
            //-------------------------------------------------------
            // If user is NOT a member of the reviewer group,  
            // permit launch of the sidekick.  
            //-------------------------------------------------------
            if(!isReviewer){%>
                <cq:include script="/libs/wcm/core/components/init/init.jsp"/>
            <%}
    	 }
       } catch (Exception ex) {
    	   log.error("Exception", ex);
       } finally {
    	    if (adminSession != null) {
    	        adminSession.logout();
    	    }    	   
       }
    %>
    
    <%-- cq stats are included by this next statement. this adds google analytics, click tracking
         and mvt impression tracking, so re-enable when/if you add in those features: --%>
    <%--<cq:include script="stats.jsp"/>--%>
    
    <%-- until we choose to go back to stats.jsp, use our own custom google analytics code.
         note that you may wish to continue to use our own tracking, because it includes the
         ability to send user session to secure2.convio.net for cross-domain tracking. --%>
    <cq:include script="google-analytics.jsp"/>

    <% if (favIcon != null) { %>
    <link rel="icon" type="image/vnd.microsoft.icon" href="<%= favIcon %>"<%=xs%>>
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<%= favIcon %>"<%=xs%>>
    <% } %>
    <title><%= currentPage.getTitle() == null ? currentPage.getName() : currentPage.getTitle() %></title>



</head>
