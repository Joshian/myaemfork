<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  List component sub-script

  Draws a list item as a news headline.
                                               r
  request attributes:
  - {com.day.cq.wcm.foundation.List} list The list
  - {com.day.cq.wcm.api.Page} listitem The list item as a page

--%><%
%><%@ page session="false"
           import="java.text.DateFormat,
                   java.util.Date,
                   com.day.cq.wcm.api.Page,
                   java.text.SimpleDateFormat,
                   org.fhcrc.publicsite.constants.Constants"%><%

    Page listItem = (Page)request.getAttribute("listitem");
    String title = listItem.getTitle() != null ? listItem.getTitle() : listItem.getName();
    String description = listItem.getDescription() != null ? listItem.getDescription() : "";
    Date defaultDate = new Date();
    Date date = listItem.getProperties().get("pubDate/date", listItem.getProperties().get("cq:lastModified", defaultDate));


    %><li class="newslister"><a href="<%= listItem.getPath() %>.html" title="<%= title %>"><span class="news-title"><%= title %></span></a>

    <% if (!"".equals(description)) { %>
      <div class="news-description"><%= description %></div>
    <% } %>

    <div>
       
	    <%
	    if (defaultDate.getTime() != date.getTime()) {
	    	DateFormat f = new SimpleDateFormat("MMM d, yyyy");
	        %><span class="news-date"><%= f.format(date) %></span><%
	    }
	    %>
	    
	    <% String source = null;
	       if(listItem.getPath().contains(Constants.PATH_TO_NEWSRELEASES_FOLDER)){ source="in <a href='"+Constants.PATH_TO_NEWSRELEASES_FOLDER+".html'>News Releases</a>" ; }
	       if(listItem.getPath().contains(Constants.PATH_TO_CENTERNEWS_FOLDER)){ source="in <a href='"+Constants.PATH_TO_NEWS_FOLDER+".html'>Hutch News</a>" ; }
           if(listItem.getPath().contains(Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER)||listItem.getPath().contains(Constants.PATH_TO_HUTCH_MAG_IMPORTER_FOLDER)){ source="in <a href='"+Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER+".html'>Hutch Magazine</a>" ; }
           if(listItem.getPath().contains(Constants.PATH_TO_PETRIDISHIMPORTER_FOLDER)){ source="in <a href='"+Constants.PATH_TO_PETRIDISHIMPORTER_FOLDER+".html'>Petri Dish</a>" ; }
	       if(null!=source){
	    %>
	    <span class="news-source"><%= source %></span>
	    <% } %>
    
    </div>
    
    
    </li>