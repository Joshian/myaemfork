<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode,
    org.fhcrc.tools.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
    Image image = new Image(resource, "image");
    Image hires = new Image(resource, "hi-res");
    Image mouseover = new Image(resource, "mouseover");
    String hiresSrc = "";
    String mouseoverSrc = "";
    String linkTarget = properties.get("linkTarget","");
    Boolean centered = properties.get("centerImage","false").equals("false") ? false : true;
    Boolean isBordered = properties.get("bordered","false").equals("false") ? false : true;
    
    Resource r;     
    if(!linkTarget.equals("")){
        linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
    }
    
    if(hires.hasContent()){
        hiresSrc = hires.getSrc();
    
        // If the hi-res image is not in the dam, we need to mess with the Src string to get hi-res.img.<file extension> rather than hi-res/file.<file extension>
        if(hiresSrc.contains("hi-res/file")){
            hiresSrc = hiresSrc.replaceAll("hi-res/file\\.", "hi-res.img.");
        }
    }
    if(mouseover.hasContent()){
        mouseoverSrc = mouseover.getSrc();
    
        // If the mouseover image is not in the dam, we need to mess with the Src string to get mouseover.img.<file extension> rather than mouseover/file.<file extension>
        if(mouseoverSrc.contains("mouseover/file")){
            mouseoverSrc = mouseoverSrc.replaceAll("mouseover/file\\.", "mouseover.img.");
        }
    }

    //drop target css class = dd prefix + name of the drop target in the edit config
    image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
    if(isBordered) {
        image.addCssClass("frame"); // Add the frame class if requested
    } else {
        image.addCssClass("noborder"); 
    }
    if(mouseover.hasContent()){
        image.addAttribute("data-hover", mouseoverSrc);
        image.addCssClass("rolloverImage");
    }
    //remove title attribute unless there is something explicitly in the dialog
	if(properties.get("image/jcr:title","").trim().equals("")) {
        image.setTitle(" ");
    }
    image.loadStyleData(currentStyle);
    image.setSelector(".img"); // use image script
    image.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        image.setSuffix(currentDesign.getId());
    }
    %>

    <% if(image.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT){ %>
        <div <%
        if(image.hasContent()){
        	
        	image = FHUtilityFunctions.nukeURLPrefix(image, resourceResolver);
        	
            out.print(" class='imageplus");
            if (centered) {
                out.print(" centered' ");
            } else {
                out.print("' ");
            }
            //--- RB ------------------------------------------------------------------------
            // Removed the original method of getting image width and replaced it with
            // the one in the out.print() line.  Seems to have improved performance greatly.
            //-------------------------------------------------------------------------------
            //int imgWidth = image.getLayer(true,true,true).getWidth();
            //out.print("style=\"width:" + Integer.toString(imgWidth) + "px\"");
            if(image.get(Image.PN_WIDTH) == null || image.get(Image.PN_WIDTH).trim().equals("")) {
                int styleWidth = image.getLayer(true,true,true).getWidth();
                if(isBordered) {
                    styleWidth += 2; //To accommodate for the border
                }
                out.print("style=\"width:" + styleWidth + "px\"");
            } else {
                int styleWidth = Integer.parseInt(image.get(Image.PN_WIDTH));
                if(isBordered) {
                    styleWidth += 2; //To accommodate for the border
                }
                out.print("style=\"width:" + styleWidth + "px\"");
            }
            log.info(image.get(Image.PN_WIDTH));
        } else {
            out.print(" class='imageplus cq-edit-only");
            if (centered) {
                out.print(" centered' ");
            } else {
                out.print("' ");
            }
        }
        %>><dl><dt>
        <% if(!linkTarget.equals("")){ %>
        <a href="<%= linkTarget %>" <%= properties.get("externalLink","false").equals("true") ? "target=\"_blank\"" : "" %>>
        <% } %>
        <% image.draw(out); %>
        <% if(!linkTarget.equals("")){ %></a><% } %></dt>
        <dd>
        <cq:text property="jcr:caption" tagName="p" tagClass="caption" placeholder="" /><%
        if(!properties.get("credit","").equals("")){ //If there is a photo credit, print it out
        %>
        <p class="credit"><cq:text property="credit" /></p><%
        } if(hires.hasContent()){ //If there is a high-resolution version of the photo, print out the link to it
        %>
        <p class="highres"><a href="<%= hiresSrc %>" target="_blank">Click for high-res version</a></p>
        <%
        }
        %></dd></dl></div>
    <% } %>