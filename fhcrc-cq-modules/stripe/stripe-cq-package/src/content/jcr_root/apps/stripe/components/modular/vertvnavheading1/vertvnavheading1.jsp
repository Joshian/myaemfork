<%@include file="/apps/stripe/global.jsp" %>
<%@ page import="org.fhcrc.stripe.utilities.PageUtil" %>
<%
    String title = properties.get("title", "Heading 1 Text Here");
    String linkURL = properties.get("linkURL", "");
    request.setAttribute("linkURL",linkURL);
    String target = "";
    boolean linkLocation = properties.get("linkLocation", false);
    if(linkLocation)
    {
        target = "_blank";
    }
%>
<div class="vertvnavheading1">
    <c:choose>
        <c:when test="${!empty linkURL}">
            <a href="<%=linkURL%>" target="<%=target%>"><h2><%=title%></h2></a>
         </c:when>
         <c:otherwise>
           <h2><%=title%></h2>
        </c:otherwise>
    </c:choose>
</div> 