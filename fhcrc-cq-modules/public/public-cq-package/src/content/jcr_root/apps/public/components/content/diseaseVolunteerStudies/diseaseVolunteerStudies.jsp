<%@include file="/libs/foundation/global.jsp"%><%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.foundation.Image" %>

<%
Image overrideImage = new Image(resource, "image");
%>

<div class="diseaserelated">

    <a href="/content/public/en/how-to-help/volunteer-your-time/volunteer-studies.html">
        <%
        if(overrideImage.hasContent()){
        	/* Design no longer has frame class as default, so removing */
//          overrideImage.addCssClass("frame");
            overrideImage.addAttribute("width","174px");
            overrideImage.loadStyleData(currentStyle);
            overrideImage.setSelector(".img"); // use image script
            overrideImage.setDoctype(Doctype.fromRequest(request));
            // add design information if not default (i.e. for reference paras)
            if (!currentDesign.equals(resourceDesign)) {
                overrideImage.setSuffix(currentDesign.getId());
            }
            overrideImage.draw(out);
        } else {
        %>    
            <img src="<%= currentDesign.getPath() %>/img/defaults/research_study.jpg" alt="Healthy volunteers" />
        <% } %>    
    </a>

    <p>Healthy volunteers are critical to prevention and early detection research.</p>
    <a href="/content/public/en/how-to-help/volunteer-your-time/volunteer-studies.html"><img class="rolloverImage" src="/etc/designs/public/img/buttons/button_sm_find_study_stat.gif" data-hover="/etc/designs/public/img/buttons/button_sm_find_study_over.gif" alt="Find a Study" /></a>
</div>