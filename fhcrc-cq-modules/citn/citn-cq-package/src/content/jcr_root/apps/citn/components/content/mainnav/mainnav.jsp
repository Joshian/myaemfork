<%--

  Main Navigation component.

  CITN Main Navigation component

--%><%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.commons.Doctype,
        java.util.ArrayList,
        java.util.Iterator,
        com.day.cq.wcm.api.PageFilter,
        org.apache.commons.lang.StringEscapeUtils,
        com.day.text.Text,
        com.day.cq.wcm.api.*,
        com.day.cq.wcm.api.Page,
        com.day.cq.wcm.api.PageManager " %>

<%      

    Page thisPage = currentPage;
    String handleString = currentStyle.get("parenthandle", "");
    Page handlePage;
    
    if("".equals(handleString)){
        handlePage = currentPage.getAbsoluteParent(2);
    }else{
        handlePage = pageManager.getPage(handleString);    
    }
    Iterator<Page> iter = handlePage.listChildren(); 

    String htmlOut = "";

    try {
        if (handlePage != null) {
            Iterator<Page> children = handlePage.listChildren();
            htmlOut += "<ul id=\"nav\">";
            htmlOut += "<li class=\"startHandle nav_border_r\"><a href=\"" + handleString + ".html\"> Home</a></li>";
            while (children.hasNext()) {
                Page child = children.next();
                Boolean onPath = thisPage.getPath().contains(child.getPath());
                if(!child.isHideInNav()){
                    htmlOut += "<li class=\"navitem nav_border_r nav_border_l ";
                    if (onPath) {
                    	htmlOut += "active ";
                    }
                    htmlOut += "\"><a href=\"" + child.getPath() + ".html\">" + child.getTitle() + "</a></li>";
                }
            }
            htmlOut += "</ul>";
        } 
    } catch(Exception e) {

    }       

    
    out.println(htmlOut); %>