<%@include file="/libs/foundation/global.jsp"%>
<%@page import="java.util.Date,
                java.text.*,
                org.fhcrc.tools.FHUtilityFunctions"%><%
Date date = properties.get("date", Date.class);
String displayformat = properties.get("displayformat", "MMM d, yyyy"); // set the default display format here http://download.oracle.com/javase/1.4.2/docs/api/java/text/SimpleDateFormat.html

if (date != null) {
%>
    
    <%

       SimpleDateFormat f = new SimpleDateFormat(displayformat);
       //f.applyPattern(displayformat);
       out.print(FHUtilityFunctions.APStylize(date));
 
    %>
<% 
} else {
%>
    <cq:text property="date"/>
<%
}
%>