<%@include file="/libs/foundation/global.jsp"%>
<section>

	<header>
	
		<cq:include script="slideshow.jsp"/>
	
	</header>
	
	<section>
	
		<div id="wrapper-content-outer">
		            
			<div id="wrapper-content">
		    
		    <header>
				<!-- homepage has this large banner graphic: -->          
				<div id="hp-content-banner">
					<img src="<%= currentDesign.getPath() %>/img/homepage/hp-banner-1.png" alt="An HIV Vaccine: The World's best long-term hope for ending AIDS.">
					<img src="<%= currentDesign.getPath() %>/img/homepage/hp-banner-2.png" alt="">
					<img src="<%= currentDesign.getPath() %>/img/homepage/hp-banner-3.png" alt="">
				</div> 
			</header>
			
			<section>	
			
				<div id="content-primary" class="content-primary">
		            
		            <section>
		            
						<cq:include path="par" resourceType="foundation/components/parsys"/>
						<div class="clear"></div>
	
					</section>
					
					<!-- remote-wrap-api-marker -->
		
					<!-- homepage in mobile has extra logos at the page bottom, only one is linked: -->
					<footer>
						<div id="hp-content-mobile-logos">
							<p><img src="<%= currentDesign.getPath() %>/img/logos/hvtn-logo-430-highres.png" alt="HIV Vaccine Trials Network" class="hvtn"></p>
							<p><a href="http://www.fredhutch.org" target="_blank"><img src="<%= currentDesign.getPath() %>/img/logos/logo-fred-hutch-cures-start-here.png" alt="Fred Hutchinson Cancer Research Center" border="0" class="fredhutch"></a></p>
						</div>
					</footer>
		              
				</div>
				
			</section>
		            
			</div>
		    
		</div>
		
	</section>

</section>