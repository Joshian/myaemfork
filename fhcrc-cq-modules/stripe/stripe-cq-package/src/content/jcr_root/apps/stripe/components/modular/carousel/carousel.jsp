<%@include file="/apps/stripe/global.jsp" %>
<%@page import="com.day.cq.dam.api.Asset,
                org.fhcrc.stripe.utilities.AssetUtil"%>

<jsp:useBean id="carouselBean" scope="request" class="org.fhcrc.stripe.components.Carousel" />
<jsp:setProperty name="carouselBean" property="componentProperties" value="<%= properties %>"/>

<div id="myCarousel" class="carousel slide carousel-fade">
 <!-- Carousel items -->
 <div class="carousel-inner">
  <%--
        String[] images = properties.get("images", String[].class);
        pageContext.setAttribute("images", images);
  --%>
        <c:set var="counter" value="0"/>
        <c:forEach items="${carouselBean.images}" var="carouselImg" varStatus="loop">
        <c:set var="counter" value="${counter+1}"/>
            <c:choose>
                <c:when test="${loop.count == 1}">
                    <div id="img${loop.count}" class="active item">               
                </c:when>
                <c:otherwise>
                    <div id="img${loop.count}" class="item">
                </c:otherwise>
            </c:choose>  
                    <img src="${carouselImg}"/>
                    <c:set var="carouselImage" value="${carouselImg}" scope="request"/>
                    <c:set var="imageDescription" value="<%=carouselBean.getImageDescription(slingRequest,(String)request.getAttribute("carouselImage"))%>"/>
                    <c:if test="${carouselBean.descriptionLocation == 'top' && !empty imageDescription}" >
                        <div class="transparent_description">
                            <p>
                               <c:out value="${imageDescription}" />
                            </p>
                        </div>
                    </c:if>
                    <c:if test="${carouselBean.descriptionLocation == 'bottom' && !empty imageDescription}">
                        <div class="bottom_description">
                            <p>
                              <c:out value="${imageDescription}" /> 
                            </p>
                        </div>
                    </c:if>
            </div>
        </c:forEach>
         </div>
        <c:forEach items="${carouselBean.images}" var="searchResult" varStatus="searchIndex" begin="${0}" end="${fn:length(carouselBean.images)}">
            <c:choose> 
                <c:when test="${searchIndex.count == 1}">
                    <div class="carousel-icons current img${searchIndex.count}" style="margin-right: ${(fn:length(carouselBean.images) - searchIndex.count) * 35}px" value="${searchIndex.count}">
                        <a  href="">
                            <img class="placeholder" alt="0" src="/etc/designs/default/0.gif"/>
                        </a>
                    </div> 
                </c:when>
                <c:otherwise>  
                    <div class="carousel-icons img${searchIndex.count}" style="margin-right: ${(fn:length(carouselBean.images) - searchIndex.count) * 35}px" value="${searchIndex.count}" >
                        <a  href="">
                            <img class="placeholder" alt="0" src="/etc/designs/default/0.gif"/>
                        </a>
                    </div>    
                </c:otherwise>
            </c:choose>  
        </c:forEach>
        <c:if test="${carouselBean.images == null}" >
            <div class="active item">
                <img src="http://placehold.it/980x360" />
            </div>
        </c:if>
<%--
 <!-- Carousel nav -->
 <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
 <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
  --%>
</div>