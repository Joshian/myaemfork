<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.util.Iterator,
        com.day.cq.wcm.api.PageFilter,
        org.fhcrc.tools.FHUtilityFunctions" %>
<%

PageFilter filter = new PageFilter();
Iterator<Page> navElements = currentPage.listChildren(filter);

if (navElements.hasNext()) {

%>
<nav>
	<div id="mobile-section-navigation">
		<ul>

<%

	while(navElements.hasNext()) {

		Page child = navElements.next();
	
%>

		<li><a href="<%= child.getPath() %>.html"><%= FHUtilityFunctions.displayTitle(child) %></a></li>

<%

	}

%>

		</ul>
	</div>
</nav>
<%

}

%>