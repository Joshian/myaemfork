var componentApp = (function(componentApp) {

	componentApp.calendarRSSReader = (function(calendarRSSReader) {
		
		calendarRSSReader.loadRSS = function(containerSet, eventType, maxNum) {
			
			var proxyLocation = '/libs/fhcrc/ProxyServlet.html';
			$.get(proxyLocation, "url=http://extranet.fhcrc.org/EN/util/science-events.xml", function(d) {
		/* First, get only the items whose category matches the requested event type */
				if(eventType == "All") {
					var firstPass = $(d).find('item');
				} else {
					var firstPass = $(d).find('item').filter(function(x){
						if($(this).find('category').text() == eventType){
							return true;
						} else {
							return false;
						}
					});
				}
		/* If there are no items in the specified category, print out a message saying as such */
				if(firstPass.size() == 0){
					var errorMsg = "<div class=\"eventContainer\">There are no upcoming " + eventType + " listed in the calendar.</div>";
					containerSet.find('div.rssTarget').append(errorMsg);
					return;
				}
				
		/* Next, if a maximum number of items was requested, chop off any items after the maximum */
				if(maxNum == 0 || maxNum > firstPass.size()){
					var secondPass = firstPass;
				} else {
					var secondPass = firstPass.slice(0, maxNum);
				}

		/* For each item left in the set, grab the appropriate data from within and append it to the target div */
				secondPass.each(function() {
					var eventDate = $(this).find('startdate').text();
					if($.trim($(this).find('stopdate').text()) != "") {
						eventDate += " - " + $(this).find('stopdate').text();
					}
					var eventTime = $(this).find('starttime').text();
					if($.trim($(this).find('stoptime').text()) != "") {
						eventTime += " - " + $(this).find('stoptime').text();
					}
					var eventLocation = $(this).find('location').text();
		/* Get rid of the <a> tags in the location, since they point to PDFs on CenterNet */			
					var regex = /<\/?a.*?>/g;
					eventLocation = eventLocation.replace(regex, "");
					var eventTitle = $(this).find('title').text();
					var eventSummary = $(this).find('abstract').text();
					var eventSpeaker = $(this).find('speaker').text();
					var eventSponsor = $(this).find('host').text();
					var eventContact = $(this).find('contact').text();
		/* Sometimes a rogue <p> tag sneaks into the event contact field, so get rid of it */
					var regexP = /<\/?p.*?>/g;
					eventContact = eventContact.replace(regexP, "");
					var html = "<div class=\"eventContainer\">";
					if(typeof eventTitle === "undefined" || $.trim(eventTitle) != ""){
						html += "<div class=\"eventTitle\">" + eventTitle + "</div>";
					}
					if(typeof eventDate === "undefined" || $.trim(eventDate) != ""){
						html += "<div class=\"eventDate\">" + eventDate + "</div>";
					}
					if(typeof eventTime === "undefined" || $.trim(eventTime) != ""){
						html += "<div class=\"eventTime\">" + eventTime + "</div>";
					}
					if(typeof eventLocation === "undefined" || $.trim(eventLocation) != ""){
						html += "<div class=\"eventLocation\">" + eventLocation + "</div>";
					}
					if(typeof eventLocation === "undefined" || $.trim(eventSummary) != ""){
						html += "<div class=\"eventSummary\">" + eventSummary + "</div>";
					}
					if(typeof eventSpeaker === "undefined" || $.trim(eventSpeaker) != ""){
						html += "<div class=\"eventSpeaker\">" + eventSpeaker + "</div>";
					}
					if(typeof eventSponsor === "undefined" || $.trim(eventSponsor) != ""){
						html += "<div class=\"eventSponsor\">Sponsor: " + eventSponsor + "</div>";
					}
					if(typeof eventContact === "undefined" || $.trim(eventContact) != ""){
						html += "<div class=\"eventContact\">Info: " + eventContact + "</div>";
					}
					html += "</div>";
					
					containerSet.find('div.rssTarget').append(html);
				});
				
			}, "xml");
			
		};
		
		return calendarRSSReader;
		
	})(componentApp.calendarRSSReader || {});

	return componentApp;
	
})(componentApp || {});