package org.fhcrc.tools;

import org.jsoup.*;
import org.jsoup.safety.Whitelist;

public class FieldUtility {
	
	public static String cleanField(String value) {
		return Jsoup.clean(value, Whitelist.simpleText());
	}

}
