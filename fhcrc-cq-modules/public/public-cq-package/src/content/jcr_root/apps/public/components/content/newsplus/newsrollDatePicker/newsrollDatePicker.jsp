<%--

 ADOBE CONFIDENTIAL
 __________________

  Copyright 2011 Adobe Systems Incorporated
  All Rights Reserved.

 NOTICE:  All information contained herein is, and remains
 the property of Adobe Systems Incorporated and its suppliers,
 if any.  The intellectual and technical concepts contained
 herein are proprietary to Adobe Systems Incorporated and its
 suppliers and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material
 is strictly forbidden unless prior written permission is obtained
 from Adobe Systems Incorporated.

  ==============================================================================

     Blog: Break component / repurposed for newsplus work, sld May/2014.

  If this component is encountered in a blog entry parsys, the entry list
  component will stop rendering the subsequent paragraphs and display a
  "read more" link instead.

  ==============================================================================

--%><%@ page session="false" import="com.adobe.cq.social.blog.Blog,
                     com.adobe.cq.social.blog.BlogManager,
                     com.day.cq.i18n.I18n,
                     com.day.cq.wcm.api.WCMMode, java.util.Calendar" %><%
%><%@include file="/libs/foundation/global.jsp" %><%

	String title = properties.get("titletext","Story Archive");

	String[] monthNames = {"Jan","Feb","March","April","May","June","July","Aug","Sept","Oct","Nov","Dec"};
	String[] monthIndexes = {"01","02","03","04","05","06","07","08","09","10","11","12"};

    String newsrollPath = properties.get("newsrollpath","/content/public/en/news");
	int newsStartYear = properties.get("newsrollstartyear", 1997); // year of the first news item, e.g., "when does the newsroll start out?"


	// get the current year
	Calendar theTime = Calendar.getInstance();
	int currentYear = theTime.get(Calendar.YEAR);
	int currentMonth = theTime.get(Calendar.MONTH); // option base 0, i.e., Jan = 1, Dec = 11.


%>

<h3><%=title%></h3>

<p class="datepicker_arrows"><a href="#" id="datepicker_up">&#x25B2;</a></p>

<div class="datepicker_container">
  <dl class="datepicker">
      <% for( int i=currentYear; i>=newsStartYear; i-- ){ %>
	      <dt><h4><%= i %></h4></dt>
    	  <dd>
			  <ul>
                  <% for( int j=1; j<=12; j++ ){ 
                             if( i==currentYear && (j-1 > currentMonth) ) {
                  %>
                  	  <li class="inactive"><%= monthNames[j-1] %></li>
                  <% } else { %>
                  	  <li><a href="<%=newsrollPath%>.date.<%=Integer.toString(i)%>-<%=monthIndexes[j-1]%>.html"><%= monthNames[j-1] %></a></li>
                  <% }} %>

              </ul>
              <div class="clear"></div>
      	  </dd>
      <% } %>
  </dl>
  <div class="clear"></div>
</div>

<p class="datepicker_arrows"><a href="#" id="datepicker_down">&#x25BC;</a></p>



<script type='text/javascript'>
$(window).load( function(){

  var naturaltop = $("dl.datepicker").position()['top']; //alert(naturaltop);

    $("#datepicker_down").click(function() { 
      var shim = $("dl.datepicker").position()['top'] - 100;
      $("dl.datepicker").animate({
          top: shim
      }, 500);
      return false;
  });

  $("#datepicker_up").click(function() { 
      var shim = $("dl.datepicker").position()['top'] + 100;
      if(shim > naturaltop) shim = naturaltop;
      $("dl.datepicker").animate({
          top: shim
      }, 500);
      //alert($("dl.datepicker").position()['top']);
      return false;
  });

});  
</script>

