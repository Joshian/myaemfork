package org.fhcrc.hvtn.components;

import org.apache.sling.api.resource.ValueMap;



public class HorizontalLine {
	
	private ValueMap componentProperties;
	
	private String colorTheme;
	private String designTheme;
	
	
	public HorizontalLine() {
		
	}


	public ValueMap getComponentProperties() {
		return componentProperties;
	}


	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
		setColorTheme(properties.get("colortheme","0"));
		setDesignTheme(properties.get("designtheme","0"));
	}


	public String getColorTheme() {
		return colorTheme;
	}


	public void setColorTheme(String colorTheme) {
		this.colorTheme = colorTheme;
		String[] c = colorTheme.split("\t");
		 String color_theme_number = c[0];
		 this.colorTheme = "";
		 if(!color_theme_number.isEmpty()){
		     this.colorTheme = "colorTheme" + color_theme_number ;
		 }
	}


	public String getDesignTheme() {
		return designTheme;
	}


	public void setDesignTheme(String designTheme) {
		this.designTheme = designTheme;
		String[] c = designTheme.split("\t");
		String design_theme_number = c[0];
		this.designTheme = "";
		if(!design_theme_number.isEmpty()){
			this.designTheme = "designTheme" + design_theme_number ;
		}
	}
	
	
}