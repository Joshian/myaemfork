<%@include file="/libs/foundation/global.jsp"%><%
String text = properties.get("text", "");
if (!text.isEmpty()) {
%>
    <div><%= text %></div>
<% 
} else {
%>

    <cq:text property="text"/>
    <!--<cq:include script="empty.jsp"/>-->
<%
}
%>