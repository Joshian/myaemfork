<%@include file="/apps/stripe/global.jsp" %>
<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.WCMMode,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image" %>
<%
      String title = properties.get("title", "Title");
      String content = properties.get("content", "");
      String id = properties.get("id", "");
      String heading_size = properties.get("heading_size","");
      String paddingTop = properties.get("paddingTop","5") + "px";
      String paddingBottom = properties.get("paddingBottom","5") + "px";
      String alignImage = properties.get("alignImage", "left");
      String[] imageLocations = properties.get("imageLocation", String[].class);
      
      Image img = new Image(resource, "image");

	    if (img.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT) 
	    {
	        img.loadStyleData(currentStyle);
	        // add design information if not default (i.e. for reference paras)
	        if (!currentDesign.equals(resourceDesign)) 
	        {
	            img.setSuffix(currentDesign.getId());
	        }
	        //drop target css class = dd prefix + name of the drop target in the edit config
	        img.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
	        img.setSelector(".img");
	        img.setDoctype(Doctype.fromRequest(request));
	        // div around image for additional formatting
	    }
%>
<div style="padding-bottom: <%=paddingBottom%>; padding-top: <%=paddingTop%>">
	<div id="expand<%=id%>">
	    <div style="float: left; cursor: pointer;"><img id="expandcollapse" src="/etc/designs/stripedesigns/stripe/clientlibs/img/expand.gif"></div>
	    <div style="float: left; font-size: <%=heading_size%>"><strong><%=title%></strong></div>
	</div><h1></h1>
	<br>
	<div id="content<%=id%>" style="display: block; text-align: left; padding-left: 15px; padding-top:0px;">
	    <%=content%>
        <%
            if(!img.getFileReference().equals(""))
            {
        %>
			   <div class="image" style="text-align: <%=alignImage%>; padding-top: 5px;">
			        <img src="<%=img.getFileReference()%>"/><br/>
			   </div>
	   <%
            }
	   %>
	   
        <%
            if(imageLocations != null)
            {
            	 for(int i = 0; i < imageLocations.length; i++)
                 {
        %>
	               <div class="image" style="text-align: <%=alignImage%>; padding-top: 5px;" >
	                    <img src="<%=imageLocations[i]%>"/><br/>
	               </div>
       <%
                 }
            }
       %>
    </div>
</div>
<script type='text/javascript'>
jQuery(document).ready(function(){
	$("#content<%=id%>").hide();
	jQuery("#expand<%=id%>").click(function(){
		    if(jQuery(this).find('#expandcollapse').attr('src').match('expand'))
		    {
		         jQuery('#expand<%=id%>').find('#expandcollapse').attr('src','/etc/designs/stripedesigns/stripe/clientlibs/img/collapse.gif');
		         jQuery('#content<%=id%>').show();  
		    }
		    else
		    {
                jQuery('#expand<%=id%>').find('#expandcollapse').attr('src','/etc/designs/stripedesigns/stripe/clientlibs/img/expand.gif');
                jQuery('#content<%=id%>').hide();
		    }
	  });
});	
</script>