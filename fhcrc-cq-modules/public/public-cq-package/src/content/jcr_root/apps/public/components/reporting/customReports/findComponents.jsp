<%@include file="/libs/foundation/global.jsp"%>
<%
String componentType = slingRequest.getParameter("componentType");
if(componentType == null || componentType.trim().equals("")) {
%>
<p>Temporary Error Message</p>
<!-- 
<cq:include script="componentFinders/videoEmbed.jsp" />
 -->
<%
} else if(componentType.equals("videoEmbed")) {
%>
<cq:include script="componentFinders/videoEmbed.jsp" />
<%
} else if(componentType.equals("download")) {
%>
<cq:include script="componentFinders/download.jsp" />
<%
} else if(componentType.equals("horizontalImage")) {
%>
<cq:include script="componentFinders/horizontalImage.jsp" />
<%
} else if(componentType.equals("multipurposeBox")) {
%>
<cq:include script="componentFinders/multipurposeBox.jsp" />
<%
} else if(componentType.equals("featuredArticle")) {
%>
<cq:include script="componentFinders/featuredArticle.jsp" />
<%
} else if(componentType.equals("featuredResearcher")) {
%>
<cq:include script="componentFinders/featuredResearcher.jsp" />
<%
} else if(componentType.equals("featuredEvents")) {
%>
<cq:include script="componentFinders/featuredEvents.jsp" />
<%
} else if(componentType.equals("reference")) {
%>
<cq:include script="componentFinders/reference.jsp" />
<%
} else if(componentType.equals("highlightsBox")) {
%>
<cq:include script="componentFinders/highlightsBox.jsp" />
<%
} else if(componentType.equals("projectFinder")) {
%>
<cq:include script="componentFinders/projectFinder.jsp" />
<%
} else if(componentType.equals("relatedContent")) {
%>
<cq:include script="componentFinders/relatedContent.jsp" />
<%
} else if(componentType.equals("superTabs")) {
%>
<cq:include script="componentFinders/superTabs.jsp" />
<%
} else if(componentType.equals("tabFacade")) {
%>
<cq:include script="componentFinders/tabFacade.jsp" />
<%
} else if(componentType.equals("textCallout")) {
%>
<cq:include script="componentFinders/textCallout.jsp" />
<%
} else if(componentType.equals("titleOrganization")) {
%>
<cq:include script="componentFinders/titleOrganization.jsp" />
<%
} else if(componentType.equals("list")) {
%>
<cq:include script="componentFinders/list.jsp" />
<%
} else if(componentType.equals("bulletedPicklist")) {
%>
<cq:include script="componentFinders/bulletedPicklist.jsp" />
<%
} else if(componentType.equals("calendarRSSReader")) {
%>
<cq:include script="componentFinders/calendarRSSReader.jsp" />
<%
} else if(componentType.equals("divisionEvent")) {
%>
<cq:include script="componentFinders/divisionEvent.jsp" />
<%
} else if(componentType.equals("divisionEventLister")) {
%>
<cq:include script="componentFinders/divisionEventLister.jsp" />
<%
} else if(componentType.equals("eventInfoBlock")) {
%>
<cq:include script="componentFinders/eventInfoBlock.jsp" />
<%
} else if(componentType.equals("inPageNav")) {
%>
<cq:include script="componentFinders/inPageNav.jsp" />
<%
} else if(componentType.equals("mediaCoverageList")) {
%>
<cq:include script="componentFinders/mediaCoverageList.jsp" />
<%
} else if(componentType.equals("newsFinder")) {
%>
<cq:include script="componentFinders/newsFinder.jsp" />
<%
} else if(componentType.equals("newsLister")) {
%>
<cq:include script="componentFinders/newsLister.jsp" />
<%
} else if(componentType.equals("directorySearchBox")) {
%>
<cq:include script="componentFinders/directorySearchBox.jsp" />
<%
}else if(componentType.equals("directorySearchResults")) {
%>
<cq:include script="componentFinders/directorySearchResults.jsp" />
<%
} else if(componentType.equals("eventItem")) {
%>
<cq:include script="componentFinders/eventItem.jsp" />
<%
} else if(componentType.equals("INslideshow")) {
%>
<cq:include script="componentFinders/INslideshow.jsp" />
<%
} else if(componentType.equals("phoneNumber")) {
%>
<cq:include script="componentFinders/phoneNumber.jsp" />
<%
} else if(componentType.equals("profileFinder")) {
%>
<cq:include script="componentFinders/profileFinder.jsp" />
<%
} else if(componentType.equals("rssFeedLister")) {
%>
<cq:include script="componentFinders/rssFeedLister.jsp" />
<%
} else if(componentType.equals("searchbox")) {
%>
<cq:include script="componentFinders/searchbox.jsp" />
<%
} else if(componentType.equals("searchResults")) {
%>
<cq:include script="componentFinders/searchResults.jsp" />
<%
} else if(componentType.equals("table")) {
%>
<cq:include script="componentFinders/table.jsp" />
<%
} else if(componentType.equals("facebookLike")) {
%>
<cq:include script="componentFinders/facebookLike.jsp" />
<%
} else if(componentType.equals("videoSlideshow")) {
%>
<cq:include script="componentFinders/videoSlideshow.jsp" />
<%
} else if(componentType.equals("repairUtility")) {
%>
<cq:include script="componentFinders/repairUtility.jsp" />
<%
} else if(componentType.equals("featureBox")) {
%>
<cq:include script="componentFinders/featureBox.jsp" />
<%
} else if(componentType.equals("donationFormlet")) {
%>
<cq:include script="componentFinders/donationFormlet.jsp" />
<%
} else if(componentType.equals("homepageSlider")) {
%>
<cq:include script="componentFinders/homepageSlider.jsp" />
<%
} else if(componentType.equals("standardSlider")) {
%>
<cq:include script="componentFinders/standardSlider.jsp" />
<%
} else if(componentType.equals("donateButton")) {
%>
<cq:include script="componentFinders/donateButton.jsp" />
<%
} else if(componentType.equals("forms")) {
%>
<cq:include script="componentFinders/forms.jsp" />
<%
}
%>
<script type="text/javascript">
var numResults = document.getElementById('reportResults').rows.length;
numResults -= 1;
var theTable = document.getElementById("reportResults");
var returnedResults = document.createElement("span");
returnedResults.setAttribute("id","numResults");
returnedResults.appendChild(document.createTextNode("Returned " + numResults + " results"));
theTable.parentNode.insertBefore(returnedResults,theTable);
</script>