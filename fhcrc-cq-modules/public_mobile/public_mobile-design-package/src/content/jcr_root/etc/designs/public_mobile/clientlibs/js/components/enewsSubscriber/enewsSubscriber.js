var componentApp = (function(componentApp) {
	
	componentApp.subscriptionComponent = (function(subscriptionComponent) {
		
		var PROXY_URL = "/libs/fhcrc/ProxyServlet.html?url=";
		var API_URL_BASE = "http://is-ext.fhcrc.org/open/mobile/enews/confirm.php";
		var apiUrl = API_URL_BASE;
		var theSubscriber = document.querySelector(".enewsSubscriber");
		var fname, lname, email;
		var isValid = true;
		
		var sendSubscribeRequest = function(e) {
			
			// Legacy IE preventDefault fix			
			if (e.preventDefault) {
				
				e.preventDefault();
				
			} else {
				
				e.returnValue = false;
				
			}
			
			// Function that handles the AJAX call later on
			var processSubscriptionRequest = function() {
			
				if (xhreq.readyState === 2) {
		
					if (theSubscriber.querySelector(".spinner").classList.contains("visuallyHidden")) {
						
						toggleSpinner();
					
					}
					
				} else if (xhreq.readyState === 4) {
				
					// Turn off the spinner when we get a response
					if (!theSubscriber.querySelector(".spinner").classList.contains("visuallyHidden")) {
						
						toggleSpinner();
					
					}
				
					if (xhreq.status === 200) {
					
						var xhReturn = xhreq.responseText;
							
						if (xhReturn === "OK") {
							
							displaySuccessMessage();
							
					
						} else if (xhReturn === "FAIL") {
					
							displayErrorMessage("FAIL");
							
						} else if (xhReturn === "MISSING VARIABLE") {
					
								displayErrorMessage("MISSING_VARS");
			
						} else {
							
							displayErrorMessage("OTHER");
							
						}
								
					} else {
						
						displayErrorMessage("NOT 200");
						
					}
				
				}
			
			};
			
			if (!Modernizr.input.required) {
				
				isValid = validateForm();
				
			}
			
			if (isValid) {
				
				/* HERE"S WHERE WE MAKE THE AJAX CALL TO THE CONVIO API */
				
				setSubscriptionVars();
				apiUrl = constructFullApiUrl(apiUrl);
				
				var xhreq = new XMLHttpRequest();
				xhreq.open("GET", apiUrl, true);
				xhreq.onreadystatechange = processSubscriptionRequest;
				xhreq.send(null);
				
			}
				
		};
		
		var resetForm = function(e) {
			
			// Legacy IE preventDefault fix			
			if (e.preventDefault) {
				
				e.preventDefault();
				
			} else {
				
				e.returnValue = false;
				
			}
			
			fname = lname = email = undefined;
			apiUrl = API_URL_BASE;
			
			var theFormInputs = theSubscriber.querySelectorAll(".js form input");
			
			for (var i = 0; i < theFormInputs.length; i++) {
				
				if (theFormInputs[i].type !== "submit") {
					
					theFormInputs[i].value = "";
					
				}
				
			}
			
			theSubscriber.querySelector(".successMessage").classList.add("visuallyHidden");
			theSubscriber.querySelector(".errorMessage").classList.add("visuallyHidden");
			theSubscriber.querySelector(".spinner").classList.add("visuallyHidden");
			theSubscriber.querySelector(".js").classList.remove("visuallyHidden");
			
		};
		
		var displayErrorMessage = function(errorCode) {
	
			var errorMessage = theSubscriber.querySelector(".errorMessage");
			var errorType = theSubscriber.querySelector(".errorType");
			var theForm = theSubscriber.querySelector(".js");
			var errorTypeHtml = "UNKNOWN ERROR";
		
			switch (errorCode) {
				
				case "FAIL":
					errorTypeHtml = "I could not send an email to the address you provided";
					break;
				case "MISSING_VARS":
					errorTypeHtml = "A required form value was either not provided or of an invalid type (e.g. not a valid email address)";
					break;
				case "NOT 200":
					errorTypeHtml = "There was an error with the confirmation script";
					break;
				default:
					break;
				
			}
			
			errorType.innerHTML = errorTypeHtml;
		
			if (!theForm.classList.contains("visuallyHidden")) {
		
				theForm.classList.add("visuallyHidden");
		
			}
			
			if (errorMessage.classList.contains("visuallyHidden")) {
		
				errorMessage.classList.remove("visuallyHidden");
		
			}
			
		};
		
		var displaySuccessMessage = function() {
			
			var successMessage = theSubscriber.querySelector(".successMessage");
			var theForm = theSubscriber.querySelector(".js");
			var theEmail = theSubscriber.querySelector(".successMessage .userEmail");
			
			/* Plus sign hack - return the plus sign to the email if it was there */
			
			email = email.replace(/%2B/g, "+");
			
			theEmail.innerHTML = email;
			
			if (!theForm.classList.contains("visuallyHidden")) {
		
				theForm.classList.add("visuallyHidden");
		
			}
			
			if (successMessage.classList.contains("visuallyHidden")) {
		
				successMessage.classList.remove("visuallyHidden");
		
			}
			
		};

		var constructFullApiUrl = function(apiUrl) {
			
			if (apiUrl === "" || apiUrl === undefined) {
				
				apiUrl = API_URL_BASE;
				
			}
			
			/* Plus sign hack - double-encode the plus sign*/
			
			if (email.indexOf("+") !== -1) {
				
				email = email.replace(/\+/g, "%2B");
				
			}
			
			apiUrl = apiUrl + "?fname=" + fname;
			apiUrl = apiUrl + "&lname=" + lname;
			apiUrl = apiUrl + "&email=" + email;
			
			apiUrl = encodeURIComponent(apiUrl);
			
			apiUrl = PROXY_URL + apiUrl;
			
			return apiUrl;
			
		};
		
		var setSubscriptionVars = function() {
			
			fname = document.getElementById("fname").value;
			lname = document.getElementById("lname").value;
			email = document.getElementById("email").value;
			
		};
		
		var toggleSpinner = function() {
			
			theSubscriber.querySelector(".js").classList.toggle("visuallyHidden");
			theSubscriber.querySelector(".spinner").classList.toggle("visuallyHidden");
			
		};
		
		var revealJSDiv = function() {
			
			theSubscriber.querySelector(".no-js").classList.add("visuallyHidden");
			theSubscriber.querySelector(".js").classList.remove("visuallyHidden");
			
		};
		
		var validateForm = function() {
			
			var firstName = document.getElementById("fname");
			var lastName = document.getElementById("lname");
			var emailAddress = document.getElementById("email");
			var requiredElements = [firstName, lastName, emailAddress];
			var isComplete = true;
			var isValidEmail = true;
			var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
			
			for (var i = 0; i < requiredElements.length; i++) {
				
				if (requiredElements[i].value === "") {
					
					requiredElements[i].style.backgroundColor = "yellow";
					isComplete = false;
					
				} else {
					
					requiredElements[i].style.backgroundColor = "white";
					
				}
				
				if (requiredElements[i] === emailAddress) {
					
					if (!emailRegEx.test(requiredElements[i].value)) {
						
						requiredElements[i].style.backgroundColor = "yellow";
						isValidEmail = false;
						
					} else {
						
						requiredElements[i].style.backgroundColor = "white";
						
					}
					
				}
				
			}
			
			if (!isComplete) {
				
				alert("There are required fields missing");
				
			} else if (!isValidEmail) {
				
				alert("Please enter a valid email");
				
			}
			
			return (isComplete && isValidEmail);
			
		};
		
		subscriptionComponent.initializeSubscription = function() {
			
			revealJSDiv();
			document.querySelector(".enewsSubscriber form").addEventListener("submit", sendSubscribeRequest, false);	
			theSubscriber.querySelector("a.formReset").addEventListener("click", resetForm, false);
			
		};
		
		return subscriptionComponent;

	})(componentApp.subscriptionComponent || {});
	
	return componentApp;
	
})(componentApp || {});