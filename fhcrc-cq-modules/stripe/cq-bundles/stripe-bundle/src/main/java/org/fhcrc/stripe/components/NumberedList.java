/**
 * Carousel class is in charge of handling Carousel properties and logic
 *  
 */
package org.fhcrc.stripe.components;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;



public class NumberedList 
{
	private final static String NUMBERED_ITEMS_PROPERTY = "textitems";
	private final static String STARTING_NUMBER = "startNumber";
	private final static String COLUMN_WIDTH= "columnWidth";
	//private final static String LINK_NAME = "linkName";
	//private final static String LINK_URL = "linkUrl"; 
	
	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	
		
	private String[] textItems;
	private int startNumber;
	private String columnWidth;
	//private String[] linkName;
	//private String[] linkUrl;
	
	//private HashMap<String, String> links = new HashMap<String, String>();
	
	
	/** Constructor of the class
	 * @param properties the page properties
	 * @param resourceResolver the resource resolver
	 */
	public NumberedList() {

	}	

	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
        this.textItems = properties.get(NUMBERED_ITEMS_PROPERTY, String[].class);
        this.startNumber = Integer.parseInt(properties.get(STARTING_NUMBER, "1"));
        this.columnWidth = properties.get(COLUMN_WIDTH, String.class);
        //this.linkName = properties.get(LINK_NAME, String[].class);
        //this.linkUrl = properties.get(LINK_URL, String[].class);
        //this.links.put(key, value)
	}

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}
	
	public String[] getTextItems() {
		return textItems;
	}

	public void setTextItems(String[] textItems) {
		this.textItems = textItems;
	}
	public int getStartNumber() {
		return startNumber;
	}

	public void setStartNumber(int startNumber) {
		this.startNumber = startNumber;
	}
	
	public String getColumnWidth() {
		return columnWidth;
	}

	public void setColumnWidth(String columnWidth) {
		this.columnWidth = columnWidth;
	}
	
//	public String[] getLinkName() {
//		return linkName;
//	}
//
//	public void setLinkName(String[] linkName) {
//		this.linkName = linkName;
//	}
//
//	public String[] getLinkUrl() {
//		return linkUrl;
//	}
//
//	public void setLinkUrl(String[] linkUrl) {
//		this.linkUrl = linkUrl;
//	}
//	
//	public HashMap<String, String> getLinks() {
//		return links;
//	}
//
//	public void setLinks(HashMap<String, String> links) {
//		this.links = links;
//	}
}