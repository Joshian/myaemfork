<%@include file="/libs/foundation/global.jsp" %><%


    /* 

    "newsplus" features that are refactored out here
    so you can include them in different news story layouts.

    provides:

    (1) 2nd social "share" icon
    (2) taglist, user can click to explore news by tag
    (3) related articles feature


    */
%>

        <!--  SOCIAL -->
        <div class="newsSocialShare socialshare">
            <cq:include path="newsSocialShare" resourceType="public/components/content/socialshare"/>
        </div>      

        <!-- TAGS -->
        <div style="margin:1.5rem 0;">
           <cq:include path="taglist" resourceType="public/components/content/newsplus/taglist"/>
        </div>

        <!-- RELATED ARTICLES -->
        <div>
         <cq:include path="relatedarticles" resourceType="public/components/content/relatedNews"/>
        </div>
        <div class="clear"></div>