<%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.
--%>
<%@include file="/libs/wcm/global.jsp"%>
<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="javax.mail.internet.InternetAddress"%>
<%@page import="com.day.cq.reporting.RequestLogAnalyzer"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.day.cq.search.Query"%>
<%@page import="com.day.cq.search.QueryBuilder"%>
<%@page import="com.day.cq.search.PredicateGroup"%>
<%@page import="com.day.cq.search.result.SearchResult"%>
<%@page import="com.day.cq.search.result.Hit"%>
<%@page import="com.day.cq.search.QueryBuilder"%>
<%@page import="com.day.cq.search.QueryBuilder"%>
<%@page import="org.apache.sling.jcr.api.SlingRepository" %> 
<%@page import="java.io.StringWriter"%>
<head>
    <script src="/libs/cq/ui/resources/cq-ui.js" type="text/javascript"></script>
</head>
<body>
<h2>Link checker</h2>
<div style="max-width: 470px">
<p>
Show pages where broken links reside.
</p>
<%
SlingRepository repo = sling.getService(SlingRepository.class);
Session jcrSession = repo.loginAdministrative(null); 
QueryBuilder builder = sling.getService(QueryBuilder.class);
Map map = new HashMap();
map.put("path", "/var/linkchecker");
map.put("property", "valid");
map.put("property.value", "false");
map.put("type", "sling:Folder");
Query query = builder.createQuery(PredicateGroup.create(map), jcrSession);
SearchResult result = query.getResult();

Iterator<Node> nodes = result.getNodes();
while (nodes.hasNext()) {
    String path = nodes.next().getPath().replaceFirst("/var/linkchecker/", "");
	%>
	<p>Linkchecker entry:  <%= path %>
	<%
	map = new HashMap();
	map.put("path", "/content");
	map.put("type", "cq:Page");
	map.put("fulltext", path);
	query = builder.createQuery(PredicateGroup.create(map), jcrSession);
    result = query.getResult();
    List<Hit> hits = result.getHits();
    for (Hit hit : hits) {
        %>
        <a href="<%= hit.getPath() %>"><%= hit.getTitle() %></a>
        <%
    }
    %></p><%
}
jcrSession.logout();
%>
</div>
