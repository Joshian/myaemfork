<%@include file="/libs/foundation/global.jsp"%><%@ page import="com.day.cq.wcm.api.WCMMode"%>
<div class="share_options">

    <a class="a2a_dd" href="http://www.addtoany.com/share_save">
    	<img src="<%= currentDesign.getPath() %>/img/buttons/plus_icon_static.png" class="rolloverImage" 
    	data-hover="<%= currentDesign.getPath() %>/img/buttons/plus_icon_hover.png"><span class="share_link_text">Share</span>
    </a>

</div>
<script>
  if( typeof a2a == 'object' ) { a2a.init('page'); }
  <%-- if you are in WCM.Edit mode, trigger a show on this element because when
       freshly inserted its display is none (static.js - initSocialShare()) --%>
  <% if(WCMMode.fromRequest(slingRequest) == WCMMode.EDIT){ %>
  $(".share_options").show();
  <% } %>
</script>
