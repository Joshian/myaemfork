<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.cq.wcm.foundation.Image,com.day.cq.wcm.api.WCMMode,
    com.day.cq.wcm.api.components.DropTarget,com.day.cq.commons.Doctype" %>

<% // the purpose of this file is to check whether the current page is a subpage of an event.
   // that is, it is an event "detail" page.
   // if it is, then the page needs to pick up the "banner" and the "themecolor" from page properties
   // of the event main page and print them out.
   // other than these special things, there's nothing particular exciting about event subpages.
%>

<% // if i am not a cluster head, and the cluster head for me is an eventmain page, then print out an event banner and set the themecolor
   Page clusterHead = (Page)request.getAttribute("clusterHead"); // refer to clusterTitle.jsp where this attribute is handily stored for our use here.
   //out.print("<!--The template is: " + clusterHead.getProperties().get("cq:template","") + "-->");  
   if(null!=clusterHead && !properties.get("isClusterHead",Boolean.FALSE) && clusterHead.getProperties().get("cq:template","").contains("eventmain")){
       
	   // YES, I am a subpage of an event, and therefore need to do some special things.

	   // 1. print out a banner that's stored on the event main page
	   //out.print("Insert banner here from " + clusterHead.getPath());      
       Image b = new Image(clusterHead.getContentResource(), "bannerimage" );
       if(b.hasContent()){
		    b.loadStyleData(currentStyle);
		    // add design information if not default (i.e. for reference paras)
		    if (!currentDesign.equals(resourceDesign)) {
		        b.setSuffix(currentDesign.getId());
		    }
		    //drop target css class = dd prefix + name of the drop target in the edit config
		    b.addCssClass(DropTarget.CSS_CLASS_PREFIX + "bannerimage");
		    /* Design no longer has bordered images as default */
//		    b.addCssClass("frame");
		    b.addCssClass("eventsubpage-banner");
		    b.setSelector(".img");
		    b.setDoctype(Doctype.fromRequest(request));
		    b.draw(out);
       }
	   
	   // 2. the theme color needs to adorn the <h1>. pick up the theme color from the event main page
	   //    since at this point we are before the existance of the <h1>, use an onload handler to effect this change
       String themecolor = clusterHead.getProperties().get("themecolor","");       
       if(!"".equals(themecolor)){%>
    	  <script type="text/javascript">
             $(document).ready(function(){$(".fh_body_content h1").css("color","#<%= themecolor %>");});  	    
    	  </script> 
       <% }
       
   }    
%>