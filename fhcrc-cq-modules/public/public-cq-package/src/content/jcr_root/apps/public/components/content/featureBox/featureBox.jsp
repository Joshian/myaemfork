<%@include file="/libs/foundation/global.jsp"%>
<jsp:useBean id="featureBoxBean" scope="request" class="org.fhcrc.publicsite.components.FeatureBox" />
<jsp:setProperty name="featureBoxBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="featureBoxBean" property="resource" value="<%= resource %>"/>
<jsp:setProperty name="featureBoxBean" property="currentStyle" value="<%= currentStyle %>"/>
<jsp:setProperty name="featureBoxBean" property="currentDesign" value="<%= currentDesign %>"/>
<jsp:setProperty name="featureBoxBean" property="resourceDesign" value="<%= resourceDesign %>"/>
<jsp:setProperty name="featureBoxBean" property="request" value="<%= request %>"/>
<jsp:setProperty name="featureBoxBean" property="writer" value="<%= out %>"/>
<jsp:setProperty name="featureBoxBean" property="componentProperties" value="<%= properties %>"/>

<div class="feature_box_container colorTheme<%= featureBoxBean.getColorTheme() %>">

	<div class="image_container">
	
		<c:choose>
			
			<c:when test="${ featureBoxBean.link }">
			
				<c:choose>
				
					<c:when test="${ featureBoxBean.externalLink }">
					
						<a href="<%= featureBoxBean.getLinkTarget() %>" target="_blank">
							<% featureBoxBean.drawImage(); %>
						</a>
						
					</c:when>
					
					<c:otherwise>
					
						<a href="<%= featureBoxBean.getLinkTarget() %>">
							<% featureBoxBean.drawImage(); %>
						</a>
						
					</c:otherwise>
					
				</c:choose>
				
			</c:when>
			
			<c:otherwise>
			
				<% featureBoxBean.drawImage(); %>
				
			</c:otherwise>
			
		</c:choose>
	     
	</div>
	     
	<div class="text_and_titles_container">
	     
		<cq:text tagName="h2" tagClass="category" placeholder="" property="category" />
		
		<c:choose>
			
			<c:when test="${ featureBoxBean.link }">
			
				<c:choose>
				
					<c:when test="${ featureBoxBean.externalLink }">
					
						<a class="title_link" href="<%= featureBoxBean.getLinkTarget() %>" target="_blank">
							<cq:text tagName="h3" tagClass="feature_box_title" property="title" />
						</a>
						
					</c:when>
					
					<c:otherwise>
					
						<a class="title_link" href="<%= featureBoxBean.getLinkTarget() %>">
							<cq:text tagName="h3" tagClass="feature_box_title" property="title" />
						</a>
						
					</c:otherwise>
					
				</c:choose>
				
			</c:when>
			
			<c:otherwise>
			
				<cq:text tagName="h3" tagClass="feature_box_title" property="title" />
				
			</c:otherwise>
			
		</c:choose>
		
		<cq:text property="text" tagName="div" tagClass="description_text" />
	
		<c:if test="${ featureBoxBean.seeMoreLink }">
	     
			<div class="link_container">
		     
		     	<c:choose>
		     	
		     		<c:when test="${ featureBoxBean.externalLink }">	     		
		     		
						<a href="<%= featureBoxBean.getSeeMoreLinkTarget() %>" target="_blank"><%= featureBoxBean.getSeeMoreLinkText() %></a>
						
					</c:when>
					
					<c:otherwise>
	
						<a href="<%= featureBoxBean.getSeeMoreLinkTarget() %>"><%= featureBoxBean.getSeeMoreLinkText() %></a>
					
					</c:otherwise>
	
				</c:choose>
		     
			</div>
	
		</c:if>
	
	     
	</div>

</div>