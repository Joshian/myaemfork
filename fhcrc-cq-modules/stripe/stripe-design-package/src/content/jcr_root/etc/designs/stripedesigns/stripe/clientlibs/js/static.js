/* Custom JS file for the STRIPE Project 
 * Sridhar Bojja
 * JQuery file for Lab's project
 */


/* ------Searchbox--------*/

$("#utility_search input.search_term").focus( function(){ if(this.value=="Search..."){ this.value=""; } } );
$("#utility_search input.search_term").blur( function(){ if(this.value==""){ this.value="Search..."; } } );


/* ----- Initialize Carousel -----*/

$(document).ready(function(){
	$(function() {
	    $('#myCarousel').carousel({
	    	interval: 5000
	    });
	});
});

/* ---- alternate row colors ----*/
/*
 * Removing this function for now, as it makes some tables unreadable. - IS, 1/23/15

jQuery(document).ready(function(){
	jQuery('tr:even').css('background-color', '#c6c6c6');
});  
*/

/* ------Carousel--------*/

$(document).ready(function(){
	jQuery('.carousel-icons').click(function(){
	    var imgname = jQuery(this).attr('value');
	    var plcid = jQuery('.active').attr('id');
	    var toHide = jQuery('.active');
	    var toShow = jQuery('#img'+imgname);
		var item = imgname-1;  
		
	    jQuery('.'+plcid).find('.placeholder').attr('src','/etc/designs/default/0.gif');  
	    jQuery('.'+plcid).removeClass('current');
	    jQuery(this).find('.placeholder').attr('src', '/etc/designs/default/0.gif' );
	    jQuery(this).addClass('current');
	    
	    //IE fix
	    //jQuery(toShow).css('filter', 'alpha(opacity=40)');
		jQuery('#myCarousel').carousel(item);
		//IE fix
		//jQuery(toShow).css('filter', 'alpha(opacity=100)');
		
	    return false; 
	});   


	jQuery('.carousel-control.right').click(function(){
	        // Get the active div and remove the active class also
	        // change the carousel icon to grey image
	        // var plcid = jQuery('.active').attr('id');
	        // jQuery('.'+plcid).find('.placeholder').attr('src','/etc/designs/stripe/clientlibs/img/circle_grey24x24.png');
	        // not required since jQuery('.active').removeClass('active');
	        // Now get the next image that needs to be painted and also
	        // set the current class on the carousel icon
	
	        var plcid = jQuery('.active').attr('id');
	        var numberOfImages = jQuery('.carousel-icons').children().size();
	        var currentImageNumber =  plcid.substring(3);
	        var nextImageNumber = eval(currentImageNumber) + 1;
	        if(nextImageNumber > numberOfImages)
	        {
	            nextImageNumber = 1;
	            plcid = "img"+numberOfImages;
	        }
	
	        if(nextImageNumber <= numberOfImages)
	        {
	             jQuery('.'+plcid).find('.placeholder').attr('src','/etc/designs/default/0.gif');
	             jQuery('.'+plcid).removeClass('current');
	             jQuery('.img'+nextImageNumber).find('.placeholder').attr('src', '/etc/designs/default/0.gif' );
	             jQuery('.img'+nextImageNumber).addClass('current');
	        }
	    });

	jQuery('.carousel-control.left').click(function(){
	     var plcid = jQuery('.active').attr('id');
	     var numberOfImages = jQuery('.carousel-icons').children().size();
	     var currentImageNumber =  plcid.substring(3);
	     var nextImageNumber = eval(currentImageNumber) - 1;
	     if(nextImageNumber == 0)
	     {
	         nextImageNumber = numberOfImages;
	         plcid = "img"+currentImageNumber;
	     }
	
	     if(nextImageNumber <= numberOfImages)
	     {
	          jQuery('.'+plcid).find('.placeholder').attr('src','/etc/designs/default/0.gif');
	          jQuery('.'+plcid).removeClass('current');
	          jQuery('.img'+nextImageNumber).find('.placeholder').attr('src', '/etc/designs/default/0.gif' );
	          jQuery('.img'+nextImageNumber).addClass('current');
	     }    
	});

	jQuery('#myCarousel').bind('slid', function() {
	     var plcid = jQuery('.active').attr('id');
	     var numberOfImages = jQuery('.carousel-icons').children().size();
	     var currentImageNumber =  plcid.substring(3);
	     var previousImageNumber = eval(currentImageNumber) - 1;
	     if(previousImageNumber == 0)
	     {
	         previousImageNumber = numberOfImages;
	         plcid = "img"+previousImageNumber;
	     }
	     else
	     {
	         plcid = "img"+previousImageNumber;
	     }
	
	     jQuery('.'+plcid).find('.placeholder').attr('src','/etc/designs/default/0.gif');
	     jQuery('.'+plcid).removeClass('current');
	     jQuery('.img'+currentImageNumber).find('.placeholder').attr('src', '/etc/designs/default/0.gif' );
	     jQuery('.img'+currentImageNumber).addClass('current');
	 });

});

jQuery(document).ready(function(){
    jQuery('.dropdown').hover(function(){
    	var navitem = this;
        jQuery('.dropdown').removeClass('open');
        var edit = jQuery(navitem).attr('editMode');
        if(edit == 'true' || jQuery(this).find('li').size() > 0)
        {
            jQuery(this).addClass('open');
        }
    },
    function() {
    	var navitem = this;
        var edit = jQuery(navitem).attr('editMode');  
        if(edit == 'false'){
            jQuery(this).removeClass('open');
        }
    });
});           
    


/* ---------------- Dynamic Component ---------------*/
jQuery(document).ready(function(){

jQuery(document).on('mouseleave','#myCarousel2', function(){
	  jQuery(this).carousel('pause');
});

jQuery('.slide-thumb').click(function(){
	if (jQuery('#thumbs-pointer').is(':animated')){
		return false;
	}
	else{
		var poscurrent = jQuery('.slide-thumb.current').position();
		var posnext = jQuery(this).position();
		var posthumb = jQuery('#thumbs-pointer').css('left');
		var nextdiv = '#contentdiv'+jQuery(this).closest('li').attr('pos');
		var currentdiv = '#contentdiv'+jQuery('.slide-thumb.current').closest('li').attr('pos');
		
		var mov = '';
		if (poscurrent.left < posnext.left){
			mov = '+='+(posnext.left - poscurrent.left);
		}
		else{
			mov = '-='+(poscurrent.left - posnext.left);
		}
	    jQuery('#thumbs-pointer').animate({
	        left: mov
	      }, 500, function() {
	        // Animation complete.
	    });
	    jQuery(currentdiv).addClass('disabled');
	    jQuery('.current').removeClass('current');	        
	    jQuery(nextdiv).removeClass('disabled');
	    jQuery(this).addClass('current');	 
	
	    //carousel
	    var imgname = jQuery(this).attr('pos');
	    var item = imgname -1;
	    jQuery('#myCarousel2').carousel(item);
	    
	    //footer
	    fixFooter();
	    
	    return false;
	}
	
	function fixFooter()
	{
		var docHeight = jQuery(window).height();
		var footerHeight = 100;//jQuery('#footer').height();
		var footerTop = jQuery('#footer').position().top + footerHeight;
		jQuery('#footer').css('margin-top', '0px');
		if (footerTop < docHeight) {
		    jQuery('#footer').css('margin-top', 15 + (docHeight - footerTop) + 'px');
		}
	}
	
});    
	
	
	/*

	jQuery('.member .member').click(function(){
		var memberPage = $(this).find('#memberPage').val();
		if(memberPage != '')
		{
			window.open(memberPage+'.html');	
		}
	});

	jQuery('.cq-dd-image').click(function(){
		type = $(this).attr('data-type');
		$('.overlay-container').fadeIn(function() { 
		    window.setTimeout(function(){  
		    $('.window-container.zoomout').addClass('window-container-visible');  
		    jQuery('.modal-body').find('#pictureLocation').attr('src','/content/dam/stripe/banners/test.jpg');  
		    }, 100);  
		});  
	}); 

	jQuery('.close').click(function(){
		alert('hello');
		$('.overlay-container').fadeOut().end().find('.window-container').removeClass('window-container-visible');
	});
	
	
	jQuery('.body_content .cq-dd-image').click(function()
	    {
	    	var uselightbox = jQuery(this).attr('uselightbox');
	    	if(uselightbox == 'true')
	    	{
		    	alert(uselightbox);
			    var imgname = jQuery(this).attr('fileName');
			    var imgpath = jQuery(this).attr('src');
			    var splitfirst = imgname.split("_");
			    var filename="";
			    for(var i=0; i<splitfirst.length; i++)
			    {
			        if(i!=(splitfirst.length-1))
			            filename = filename + splitfirst[i]+"_";
			    }
			    filename = filename +"large.jpg";
			    var splitimgpath = imgpath.split("/");
			    var newimgpath = "/content/dam/stripe/" + splitimgpath[4] + "/other/big/" + filename;
			    jQuery('#test_modal').find('#pictureLocation').attr('src',newimgpath);  
			
			    $('#test_modal').modal('show');
			
			    //alert("Inner Width " + window.innerWidth + "Modal width " + jQuery('#test_modal').width());
			    //alert("Inner Height " + window.innerHeight + "Modal height " + jQuery('#test_modal').height());
			    
			    if(jQuery('#test_modal').width() > window.innerWidth)
			    {
			        jQuery('#test_modal').css('width', window.innerWidth-500);
			    }
			    
			    if(jQuery('#test_modal').height() > window.innerHeight)
			    {
			        jQuery('#test_modal').css('max-height', window.innerHeight-100);
			    }
	    	}
	    });
	
	*/
	
	//jQuery('#myCarousel').bind('slide', function() {
	//alert("Slide");
 //});

/* --------------- Dropdown menu ---------------------*/
/*
jQuery('.dropdown').hover(
	function () {
		jQuery('.dropdown').removeClass('open');
    	$(this).addClass('open');
	}
);
*/

/**
jQuery('.dropdown').hover( function () {
			jQuery('.dropdown').removeClass('open');
			if(jQuery(this).find('li').size() > 0)
			{
				jQuery(this).addClass('open');
			}
		},
		function() {
			var dropdown = this;
			jQuery(dropdown).removeClass('open')
});**/

/**
jQuery('.dropdown-menu').hover( function () {
	jQuery('.dropdown-menu').removeClass('open');
	$(this).addClass('open');
	},
	function() {
		setTimeout(function(){$(this).removeClass('open');},1000);	
});
**/
});