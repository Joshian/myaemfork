<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text, java.util.Iterator, com.day.cq.wcm.api.WCMMode, com.day.cq.wcm.foundation.Image" %>
<%
String featuredPath = "/content/public/en/diseases/featured-researchers"; //This is where all the featured profiles live
String linkTarget = "";
log.debug("Current Page: " + currentPage.getName() + "\n");
Iterator<Page> pageIterator = resourceResolver.getResource(featuredPath).adaptTo(Page.class).listChildren();
while(pageIterator.hasNext()){
    Page nextPage = pageIterator.next();
    log.debug("Iterator page: " + nextPage.getName() + "\n");
/* Only care about Public Profile pages */    
    if(!nextPage.getProperties().get("cq:template","").contains("publicprofile")){
    	continue;
    }
/* Check to see if the name of the Public profile page is identical to the name of the current page and if so, set the linkTarget to that page */
    if(nextPage.getName().equals(currentPage.getName())){
        linkTarget = nextPage.getPath() + ".html";
        log.debug("Match Made!");
        break;
    }
}
%>
 <!-- page content -->
 <div id="fh_body" class="fh_body container_12 clearfix"><a name="content"></a>

  <div class="grid_12">

    <!-- breadcrumbs appear in the content flow in case they wrap to two lines -->
    <cq:include script="breadcrumbs.jsp"/>

    <cq:include script="clusterTitle.jsp" /> 
    
  </div>  
    
  <!-- content column -->
  <div id="fh_body_content" class="fh_body_content grid_9 push_3">  

    
    
    <% if(WCMMode.fromRequest(request) == WCMMode.EDIT){%>
    <span class="cq-edit-only" style="font-family:'Geogrotesque-RegularIt',Arial,Helvetica,Sans-serif;">Appointments. Use the first item for your primary appointment; use the rest of the items for your secondary appointments (if any):</span>
    <% } %>    
    <dl class="profile_appointments">
     <cq:include path="titleorg1" resourceType="public/components/content/titleorg"/>
     <cq:include path="titleorg2" resourceType="public/components/content/titleorg"/>
     <cq:include path="titleorg3" resourceType="public/components/content/titleorg"/>
     <cq:include path="titleorg4" resourceType="public/components/content/titleorg"/>
     <cq:include path="titleorg5" resourceType="public/components/content/titleorg"/>
    </dl> 
    
    <% if(WCMMode.fromRequest(request) == WCMMode.EDIT){%>
    <span class="cq-edit-only" style="font-family:'Geogrotesque-RegularIt',Arial,Helvetica,Sans-serif;"><br/>Biography:</span>
    <% } %>    
    <!-- reference-able by other assets: -->
    <cq:include path="bio" resourceType="foundation/components/parsys" />
      
    <br/><br/>

    <div class="grid_4 alpha">
      <!-- this is programmatically-created from all news items tagged with this profile: -->
      <cq:include script="related_news.jsp" />
    </div> 
    
    <div class="grid_4 prefix_1 omega">
 
      <!--  additional links, etc. (manual) -->
<%      
      if(!"".equals(properties.get("additionallinks/text","")) || !linkTarget.equals("") || WCMMode.fromRequest(request) == WCMMode.EDIT){%>
      <h3>Additional Links &amp; Information</h3>
      <div class="forceSquareBullet profile_additionallinks">
<%
        if(!linkTarget.equals("")){
%>
        <ul class="featuredresearcherlink" style="margin-bottom:0;padding-bottom:0;">
            <li><a href="<%= linkTarget %>">Featured Researcher Profile</a></li>
        </ul>
        <% } %>              
        <cq:include path="additionallinks" resourceType="public/components/content/text"/>
      </div>
      <% } %>
      
      <!-- Related Labs: this is programmatically-created from tags: -->
      <cq:include script="related_labs.jsp" />
    </div>
    
    
    
    
     
  </div>
  <!-- /content column -->

    

  <!-- left column (sidebar) -->
  <div id="fh_sidebar" class="fh_sidebar grid_3 pull_9"><div id="fh_sidebar_content" class="fh_sidebar_content">
    
    <!--  the image is a field: -->
    <% if(WCMMode.fromRequest(request) == WCMMode.EDIT){%>
    <div class="cq-edit-only" style="font-family:'Geogrotesque-RegularIt',Arial,Helvetica,Sans-serif; background-color:white"><br/>Profile image &amp; caption (opt.):</div>
    <% } %>
    <% Image image = new Image(resource, "profileImage"); 
       String addClass = image.hasContent() ? "" : " cq-edit-only";
       addClass = (!image.hasContent() && !(WCMMode.fromRequest(request) == WCMMode.EDIT)) ? " noimage" : ""; // add a class if there is no image, css can hide it.
    %>
    <div class="profile_image<%= addClass %>"><cq:include path="profileImage" resourceType="foundation/components/image"/><div class="caption"><cq:include path="profileImageCaption" resourceType="public/components/content/textarea"/></div></div>
    
    <% if(
    	 (!"".equals(properties.get("phone/text","")) ||
          !"".equals(properties.get("fax/text","")) ||
          !"".equals(properties.get("email/text","")) ||
          !"".equals(properties.get("alternate/text",""))) ||
          WCMMode.fromRequest(request) == WCMMode.EDIT          
         ) { %>       
    <div class="profile_contact_information">
     <h4>Contact Information</h4>   
     <!--  first three of these data should draw from SQL: --> 
     <dl>
      <% if(!"".equals(properties.get("phone/text","")) || WCMMode.fromRequest(request) == WCMMode.EDIT){%>
      <dt>Phone</dt>
      <dd><cq:include path="phone" resourceType="public/components/content/phonenumber" /></dd>
      <% } %>
      <% if(!"".equals(properties.get("fax/text","")) || WCMMode.fromRequest(request) == WCMMode.EDIT){%>
      <dt>Fax</dt>
      <dd><cq:include path="fax" resourceType="public/components/content/phonenumber" /></dd>
      <% } %>
      <% if(!"".equals(properties.get("email/text","")) || WCMMode.fromRequest(request) == WCMMode.EDIT){%>
      <dt>Email</dt>
      <dd><cq:include path="email" resourceType="public/components/content/emailaddress" /></dd>
      <% } %>
      <% if(!"".equals(properties.get("alternate/text","")) || WCMMode.fromRequest(request) == WCMMode.EDIT){%>      
      <dt>Additional contact</dt>
      <dd class="additional_contact"><% if("".equals(properties.get("alternate/text",""))){ %><span class="cq-edit-only" style="font-family:'Geogrotesque-RegularIt',Arial,Helvetica,Sans-serif; color:#4b4b4b">Additional contact information, if any:</span><% } %><cq:include path="alternate" resourceType="public/components/content/text" /></dd>
      <% } %>
     </dl>
    </div>
    <% } %>

    <!-- protected parsys: -->
    <div class="relatedcontentarea">
      <cq:include path="parRelated" resourceType="public/components/content/protectedparsys"/>
    </div>
    
    <cq:include script="related-content.jsp" />
  
   </div><!-- /fh_sidebar_content -->
  
  </div>
  <!-- /left column -->
 
 </div><!-- /fh_body -->
