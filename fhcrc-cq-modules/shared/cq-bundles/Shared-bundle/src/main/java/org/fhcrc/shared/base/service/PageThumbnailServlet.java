package org.fhcrc.shared.base.service;


import javax.servlet.ServletException;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.request.RequestPathInfo;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import com.day.cq.wcm.api.Page;
import java.util.Dictionary;
import javax.jcr.Node;
import com.day.cq.commons.ImageHelper;
import com.day.image.Layer;
import java.awt.Rectangle;
import org.apache.sling.api.servlets.HtmlResponse;
import com.day.cq.commons.servlets.HtmlStatusResponseHelper;
// SEAN: THESE HAVE BEEN REMOVED - LEFT ONLY FOR YOUR REFERENCE:
// SEAN: THESE HAVE BEEN REMOVED - LEFT ONLY FOR YOUR REFERENCE:
// SEAN: THESE HAVE BEEN REMOVED - LEFT ONLY FOR YOUR REFERENCE:
//import com.day.cq.dam.api.AssetManager;
//import com.day.cq.dam.api.Asset;
//import com.day.cq.dam.api.Rendition;

/**
 * The Class PageThumbnailServlet.
 *
 * @author 3|Share SnapBuild plugin
 */
@SlingServlet(
        resourceTypes     = "sling/servlet/default",
        selectors         = "pagethumb",
        methods           = "GET",
        extensions        = "png",
        generateService   = true,
        generateComponent = true,
        metatype          = true,
        label             = "Page Thumbnail Servlet"
)
public class PageThumbnailServlet extends SlingAllMethodsServlet {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The Constant log. */
    private static final Logger log      = LoggerFactory.getLogger(PageThumbnailServlet.class);
    /**
     * Activate.
     *
     * @param componentContext the component context
     */
    protected void activate(ComponentContext componentContext)
    {
        log.info("Activating PageThumbnailServlet");
        configure(componentContext.getProperties());
    }
    /**
      * Configure.
      *
      * @param properties the properties
      */
    protected void configure(Dictionary<?, ?> properties)
    {
        // Do nothing
    }
    
    /**
     * Process a SlingServletRequest for the ".views" selector
     *
     * @param request the request
     * @param response the response
     * @throws ServletException the servlet exception
     */
    @Override
    public final void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException
    {
        log.debug("PageThumbnailServlet results: initializing");
        try
        {
            // Example: http://localhost:4502/content/public/en/news/releases/2013/09/researchers-influenza-infect-cells.pagethumb.32.32.png?x=y
            /*
                1. Check if it is a page and get the resource
                2. Get the height and width selectors
                3. Get the type selector - fit/resize/crop
                4. Get the image:
                    A. See if the page has the thumbnail inline
                            - Load
                    B. See if the page has an image in the DAM
                            - Get as an asset...
            */
            int selectorNumber = 0; // We need to keep track in order to know which selector is which...
            boolean          first            = true;
            String           type             = "fit";
            RequestPathInfo  requestPathInfo  = request.getRequestPathInfo();
            String[]         requestSelectors = requestPathInfo.getSelectors();
            int              h                = 64;
            int              w                = 64;
            String           requestExtension = requestPathInfo.getExtension();
            log.debug("REQUEST EXTENSION: " + requestExtension);
            for(String requestSelector:requestSelectors)
            {
                log.debug("---> SELECTOR: " + (selectorNumber++) + " " + requestSelector);
                if(requestSelector.equals("")) {}
                else if(requestSelector.equals("pagethumb"))
                {
                    // That is us - do nothing...
                }
                else if(requestSelector.equals("fit") || requestSelector.equals("crop") || requestSelector.equals("resize"))
                {
                    type = requestSelector;
                }
                else
                {
                    // Try to get as long for limit:
                    try
                    {
                        int intValue = Integer.valueOf(requestSelector).intValue();
                        if(first)
                        {
                            w      = intValue;
                            first  = false;
                        }
                        else
                        {
                            h      = intValue;
                        }
                    }
                    catch(Exception e)
                    {
                        // Do nothing - bad parameter...
                    }
                }
            }
            log.debug("---> Height x Width: " + h + " x " + w);
            // Get the resource:
            ResourceResolver resourceResolver   = request.getResourceResolver();
            Resource         resource           = request.getResource();
            Page             thumbPage          = null;
            try
            {
                thumbPage = resource.adaptTo(Page.class);
            }
            catch(Exception e)
            {
                // not a page - do nothing
            }
            if(thumbPage == null)
            {
//                log.debug("NOT A PAGE");
                return;
            }
            else
            {
                log.debug("WAS A PAGE");
            }
            // OK, we have a page at this point:
            // Notes: 1. does image have a fileReference property?
            //        2. or does it have image/
            String imagePath  = null;
            Layer  imageLayer = null;
            try
            {
                Resource imageFileResource = thumbPage.getContentResource("image/file");
                // Notes: 1. does image have a fileReference property?
                //        2. Does it have image/file?
                imageLayer = ImageHelper.createLayer(imageFileResource);
                imagePath     = imageFileResource.getPath();
            }
            catch(Exception e12)
            {
            }
            if(imagePath == null)
            {
                // Ok, does it have an image file reference
                try
                {
                    // NOTE: DAM content paths are as below:
                    // content/dam/geometrixx-media/articles/ai.jpeg/jcr:content/renditions/original/jcr:content/jcr:data
                    Resource           imageResource = thumbPage.getContentResource("image");
                    if(imageResource != null)
                    {
                        String             imageDamPath  = ((Node)imageResource.adaptTo(Node.class)).getProperty("fileReference").getString()+"/jcr:content/renditions/original/jcr:content";
                        log.debug("---> IMAGE DAM PATH: " + imageDamPath);
                        Resource           imageDamResource = resourceResolver.getResource(imageDamPath); 
                        if(imageDamResource != null)
                        {
                            log.debug("imageDamResource was not null");
                            imageLayer = ImageHelper.createLayer(imageDamResource);
                            if(imageLayer != null)
                            {
                                imagePath     = imageDamPath;
                                log.debug("DAM imageLayer was not null");
                            }
                            else
                            {
                                log.debug("DAM imageLayer was null");
                            }
                        }
                        else
                        {
                            log.debug("imageDamResource was null");
                        }
                    }

// SEAN: THIS HAS BEEN REMOVED - LEFT ONLY FOR YOUR REFERENCE:
// SEAN: THIS HAS BEEN REMOVED - LEFT ONLY FOR YOUR REFERENCE:
// SEAN: THIS HAS BEEN REMOVED - LEFT ONLY FOR YOUR REFERENCE:
// Original DAM code:
/*
                    Resource           imageResource = thumbPage.getContentResource("image");
                    if(imageResource != null)
                    {
                        String             imageDamPath  = ((Node)imageResource.adaptTo(Node.class)).getProperty("fileReference").getString();
                        Resource           imageDamResource = resourceResolver.getResource(imageDamPath); 
                        log.debug("---> IMAGE DAM PATH: " + imageDamPath);
                        Asset              imageDamAsset = imageDamResource.adaptTo(Asset.class);
                        Rendition          imageDamAssetRendition = imageDamAsset.getOriginal();
                        imageLayer = ImageHelper.createLayer(imageDamAssetRendition);
                    }
*/
                }
                catch(Exception e98)
                {
                  log.debug("PageViews e98: " + e98.toString());
                }
            }
            log.debug("Image path: " + imagePath );
            log.debug("Image layer: " + ((imageLayer == null)?"NULL":"NOT NULL"));
            
            // Do we have an image - if not get the default one:
            if(imageLayer == null)
            {
                try
                {
                    // Note: /etc/designs/public/img/defaults/eventicon.jpg/jcr:content/jcr:data
                    String defaultImagePath = "/etc/designs/shared/img/pagethumbs/default-pagethumb.png/jcr:content";

                    // If this is the public site, choose the Public site's version of the default image.
                    if(thumbPage.getPath().startsWith("/content/public/")){ defaultImagePath = "/etc/designs/shared/img/pagethumbs/public-pagethumb.png/jcr:content"; }
                    
                    Resource imageFileResource = resourceResolver.getResource(defaultImagePath);
                    if(imageFileResource != null)
                    {
                        log.debug("imageFileResource was not null");
                        // Notes: 1. does image have a fileReference property?
                        //        2. Does it have image/file?
                        imageLayer = ImageHelper.createLayer(imageFileResource);
                        if(imageLayer != null)
                        {
                            imagePath     = imageFileResource.getPath();
                            log.debug("imageLayer was not null");
                        }
                        else
                        {
                            log.debug("imageLayer was null");
                        }
                    }
                    else
                    {
                        log.debug("imageFileResource was null");
                        return;
                    }
                }
                catch(Exception e55)
                {
                    log.debug("e55: " + e55.getMessage() + " " + e55.getCause().getMessage());
                }

            }
            
            if(type.equals("fit"))
            {
                final double width     = imageLayer.getWidth();
                final double height    = imageLayer.getHeight();
                      double newWidth  = width;
                      double newHeight = height;

                int rectX      = 0;
                int rectY      = 0;
                int rectWidth  = 0;
                int rectHeight = 0;
                double widthScale  = w/width;
                double heightScale = h/height;
                // Which is the larger scale to meet the matching new width/height
                if(widthScale > heightScale)
                {
                    // Width will be filled - height will be clipped
                    newWidth   = w;
                    newHeight  = height * widthScale;
                    rectX      = 0;
                    rectWidth  = w;
                    rectY      = 0; // Our clip rectangle starts halfway into the extra part
                    rectHeight = h;
                }
                else
                {
                    // Height will be filled - width will be clipped
                    newWidth   = width * heightScale;
                    newHeight  = h;
                    rectX      = 0; // Our clip rectangle starts halfway into the extra part
                    rectWidth  = w;
                    rectY      = 0;
                    rectHeight = h;
                }
                imageLayer.resize((int) newWidth, (int) newHeight);
                Rectangle rect = new Rectangle(rectX, rectY, rectWidth, rectHeight);
                imageLayer.crop(rect);
            }
            else if(type.equals("crop"))
            {
                double newWidth = imageLayer.getWidth();
                double newHeight = imageLayer.getHeight();
                int rectX = 0;
                int rectY = 0;
                if( newWidth > w)
                {
                    rectX = (int)(newWidth - w) / 2;  
                }
                if( newHeight > h)
                {
                    rectY = (int)(newHeight - h) / 2;  
                }
                newWidth = w;
                newHeight = h;
                Rectangle rect = new Rectangle(rectX, rectY, (int)newWidth, (int)newHeight);
                imageLayer.crop(rect);
                
            }
            else if(type.equals("resize"))
            {
                final double width = imageLayer.getWidth();
                final double height = imageLayer.getHeight();
                double newWidth = width;
                double newHeight = height;
                newWidth = w;
                newHeight = w * height / width;
                if (newHeight > h)
                {
                    newWidth = h * width / height;
                    newHeight = h;
                }
                imageLayer.resize((int) newWidth, (int) newHeight);
            }
            try {
                if(imageLayer!=null)
                {
                    response.setContentType("image/png");
                    imageLayer.write("image/png", 1, response.getOutputStream());
                    return;
                }
            } catch (IllegalArgumentException e) {
                final HtmlResponse htmlResponse =
                        HtmlStatusResponseHelper.createStatusResponse(false, "Error creating image [1]");
                htmlResponse.send(response, true);
                return;
            }
        }
        catch(Exception e)
        {
            log.debug("---- e: " + e);
        }
        finally
        {
            try
            { 
                final HtmlResponse htmlResponse =
                        HtmlStatusResponseHelper.createStatusResponse(false, "Error creating image [2]");
                htmlResponse.send(response, true);
            }
            catch(Exception e2)
            {
                log.debug("---- e2: " + e2);
            }
        }
    }
}
