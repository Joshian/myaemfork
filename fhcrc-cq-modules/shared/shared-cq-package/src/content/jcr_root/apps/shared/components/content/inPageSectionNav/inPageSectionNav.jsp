<%--

  In-Page Navigation
  
  This component adds a "Home | Previous | Next" option to the page. The buttons navigate you to the siblings of the page it is included on, taking the 
  order from the default order that CQ uses. The "Home" option looks up the tree to find the current page's cluster head.

--%><%
%><%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.api.PageFilter,
    java.util.Iterator,
    org.fhcrc.tools.FHUtilityFunctions" %><%
%><%@page session="false" %><%
%><%

Page clusterHead = currentPage.getAbsoluteParent(2), // default to the homepage
  previousPage = null,
  nextPage = null,
  parentPage = currentPage.getParent(),
  tempPage = null;
Boolean firstLoop = true,
  ignoresHideInNav = false,
  hasPrevious = true,
  hasNext = true;
String filters = properties.get("filters","");
PageFilter filter;

if(filters.contains("hide")) {
    ignoresHideInNav = true;
}

if (ignoresHideInNav) {
    filter = new PageFilter(false, true);
} else {
	filter = new PageFilter();
}


if(!(parentPage == null)) {
	/* Attempt to find the previous and next pages of the current page, if they exist. */
    Iterator<Page> children = parentPage.listChildren(filter);
    while(children.hasNext()) {
    	tempPage = children.next();
    	if (tempPage.equals(currentPage)) {
    		
    		if (firstLoop) {
    			hasPrevious = false;
    		}
    		if (!children.hasNext()) {
    			hasNext = false;
    		} else {
    			nextPage = children.next();
    		}
    		
    		break;
    		
    	} else {
    		
    		previousPage = tempPage;
    		firstLoop = false;
    		
    	}
    }
    
    /* Attempt to find the cluster head above the current page */
    
    tempPage = parentPage;
    String test = tempPage.getProperties().get("isClusterHead", "false");
    while(!"true".equals(test)){
        if(tempPage.getParent() == null){
            // If we have hit the top of the tree, then use the homepage as the clusterHead
            clusterHead = currentPage.getAbsoluteParent(2);
            break;
        }
        tempPage = tempPage.getParent();
        test = clusterHead.getProperties().get("isClusterHead", "false");
    }
    clusterHead = tempPage;
    
    %>
    <nav><div class="inPageNavContainer">
    <% if (clusterHead != null) { %>
        <span class="inPageNavButton"><a href="<%= FHUtilityFunctions.cleanLink(clusterHead.getPath(), resourceResolver) %>">Home</a></span>
    <% } if (hasPrevious && previousPage != null) { %>
        <span class="inPageNavButton"><a href="<%= FHUtilityFunctions.cleanLink(previousPage.getPath(), resourceResolver) %>">&lt;&nbsp;Prev<span class='droptext'>ious</span></a></span>
    <% } if (hasNext && nextPage != null) { %>
        <span class="inPageNavButton"><a href="<%= FHUtilityFunctions.cleanLink(nextPage.getPath(), resourceResolver) %>">Next&nbsp;&gt;</a></span>
    <% } %>    
    </div></nav>
    <%
    
} else {
	%>
	<p>The current page seems to have no parent, or at least no parent that is a Page.</p>
	<%
}

%>
