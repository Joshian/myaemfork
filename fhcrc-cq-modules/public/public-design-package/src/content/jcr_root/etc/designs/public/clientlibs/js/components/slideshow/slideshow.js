/* Global variable declarations */
var totalSlides;
var currentSlide = 0;
var largeWidth = 597; // width of large image div
var thumbHeight = 89;  // width of .thumbnail div + border width (x2) + margin between divs
var topThumb = 0;
var bottomThumb;

$(document).ready( function() {
	createSkeleton();
	moveContent();
	$('.slideshowSuperContainer').css({'display':'none'}); //Hide the old version
	slideshowInit();
});
/* Wait until window load to mess with images, since images have no css until they are loaded in Safari/Chrome/Opera */
$(window).load(function() {
	$('.largePhoto img').each( function(){
		$(this).css({'margin-top' : Math.floor((428 - $(this).height()) / 2)});
	});					
});

/* Create and attach the necessary div structure to insert the content into */
function createSkeleton() {
	$('<div></div>',
	  {
		  "class": 'slideshowContainer'
	  }).insertAfter('.slideshowSuperContainer');
	$('<div></div>',
	  {
		  "class": 'slideshowHeader'
	  }).appendTo('div.slideshowContainer');
	$('<div></div>',
	  {
		  "class": 'slideshowPhotoContainer'
	  }).appendTo('div.slideshowContainer');
	$('<div></div>',
	  {
		  "class": 'clear'
	  }).appendTo('div.slideshowContainer');
	$('<div></div>',
	  {
		  "class": 'textContainer'
	  }).appendTo('div.slideshowContainer');
	$('<div></div>',
	  {
		  "class": 'largePhotoContainer'
	  }).appendTo('div.slideshowPhotoContainer');
	$('<div></div>',
	  {
		  "class": 'thumbnailBar'
	  }).appendTo('div.slideshowPhotoContainer');
	$('<div></div>',
	  {
		  "class": 'control',
		  id: 'leftControl'
	  }).appendTo('div.largePhotoContainer');
	$('<div></div>',
	  {
		  "class": 'theSlides'
	  }).appendTo('div.largePhotoContainer');
	$('<div></div>',
	  {
		  "class": 'control',
		  id: 'rightControl'
	  }).appendTo('div.largePhotoContainer');
	$('<div></div>',
	  {
		  "class": 'controlImage',
		  id: 'leftControlImage'
	  }).text("Move left").appendTo('div#leftControl');
	$('<div></div>',
	  {
		  "class": 'controlImage',
		  id: 'rightControlImage'
	  }).text("Move right").appendTo('div#rightControl');
	$('<div></div>',
	  {
		  "class": 'thumbControl',
		  id: 'topThumbControl'
	  }).text("Move up").appendTo('div.thumbnailBar');
	$('<div></div>',
	  {
		  "class": 'overflowContainer'
	  }).appendTo('div.thumbnailBar');
	$('<div></div>',
	  {
		  "class": 'thumbControl',
		  id: 'bottomThumbControl'
	  }).text("Move down").appendTo('div.thumbnailBar');
	$('<div></div>',
	  {
		  "class": 'thumbnailContainer'
	  }).appendTo('div.overflowContainer');
}

/* Actually take the content from the old structure and put it in the new */
function moveContent() {
	$('.slideshowSuperContainer .slideTitle, .slideshowSuperContainer .instructions').clone().prependTo('div.slideshowHeader');
	$('.slideshowSuperContainer div.textimage_image img').clone().each( function(){
		$('<div></div>',{"class":'largePhoto'}).append(this).appendTo('div.theSlides');
	});
	$('.slideshowSuperContainer div.textimage_image img').clone().attr({'title':''}).each( function(){
		$('<div></div>',{"class":'thumbnail'}).append(this).appendTo('div.thumbnailContainer');
	});
	$('.slideshowSuperContainer div.text').clone().each( function(){
		$('<div></div>',{"class":'textSlide'}).append(this).appendTo('div.textContainer');
	});
}

function slideshowInit(){
/* Set values for some global variables */	
	totalSlides = $('.slideshowSuperContainer .textimage').size();
	if(totalSlides > 4) {
		bottomThumb = 3;
	} else {
		bottomThumb = totalSlides - 1;
		$('#bottomThumbControl').css({'display':'none'}); //Hide the bottom arrow, since there aren't enough thumbnails to warrant it
	}
/* Resize all the appropriate container divs according to the number of slides*/
	$('.theSlides').width(largeWidth * totalSlides);
	$('.thumbnailContainer').height(thumbHeight * totalSlides);
/* Add active classes to the first thumbnail and text slide */	
	$('.thumbnailContainer .thumbnail').first().addClass('activeThumb');
	$('.textContainer .textSlide').first().addClass('activeText');
/* Add events to the arrows and hide the left and top arrows, since we are at the first slide */
	bindEvents();
	$('#leftControl').css({'display':'none'});
	$('#topThumbControl').css({'display':'none'});
/* Add mouseenter and mouseleave events to the large photo container to show/hide the arrows */
	$('.largePhotoContainer').mouseenter(revealArrows).mouseleave(hideArrows);
	hideArrows();
/* Add the status div (useless if user has no javaScript */	
	$('<div id="slideStatus"><span id="current">1</span>/'+ totalSlides + '</div>').insertBefore('.textContainer');
/* Get rid of the frame class from all images */
	$('.slideshowContainer img').removeClass("frame");
}

/* This function progresses the slideshow one slide to the right */
function advanceSlide() {
	unbindEvents(); //Avoid additional animations until this one has completed
	var theSlides = $('.theSlides');
	var temp = theSlides.css('left')/*.replace("px","")*/;
	var leftIndent = parseInt(temp) - largeWidth;
	theSlides.animate({'left' : leftIndent}, 200, bindEvents); //rebind the click events after done animating
	currentSlide++; //Update which slide is the current one
	$('.thumbnailContainer .thumbnail').removeClass('activeThumb').eq(currentSlide).addClass('activeThumb');
	$('.textContainer .textSlide').removeClass('activeText').eq(currentSlide).addClass('activeText');
	checkThumbs();
	addRemoveArrows();
	updateCurrent();
}
/* This function progresses the slideshow one slide to the left */
function regressSlide() {
	unbindEvents(); //Avoid additional animations until this one has completed
	var theSlides = $('.theSlides');
	var leftIndent = parseInt(theSlides.css('left')) + largeWidth;
	theSlides.animate({'left' : leftIndent}, 200, bindEvents); //rebind the click events after done animating
	currentSlide--; //Update which slide is the current one
	$('.thumbnailContainer .thumbnail').removeClass('activeThumb').eq(currentSlide).addClass('activeThumb');
	$('.textContainer .textSlide').removeClass('activeText').eq(currentSlide).addClass('activeText');
	checkThumbs();
	addRemoveArrows();
	updateCurrent();
}
/* This function allows you to click on a thumbnail image and be taken directly to that slide */
function changeSlide() {
	var slideIndex = $(this).index(); // goes from 0 to (totalSlides - 1)
	if(slideIndex == currentSlide) return; // No need to do anything if they click on the active slide
	unbindEvents(); //Avoid additional animations until this one has completed
	var slideDiff = slideIndex - currentSlide;
	var theSlides = $('.theSlides');
	var leftIndent = parseInt(theSlides.css('left')) - (slideDiff * largeWidth);
	
	theSlides.animate({'left' : leftIndent}, 500, bindEvents);
	currentSlide = slideIndex; //Update which slide is the current one
/* Change the active thumbnail and text slide */
	$('.thumbnailContainer .thumbnail').removeClass('activeThumb').eq(currentSlide).addClass('activeThumb');
	$('.textContainer .textSlide').removeClass('activeText').eq(currentSlide).addClass('activeText');
	checkThumbs();
	addRemoveArrows();	
	updateCurrent();
}
/* This function slides the thumbnail images downwards*/
function thumbnailAdvance() {
	if(bottomThumb == totalSlides - 1) return;
	var toSlideTo, animationSpeed;
	var container = $('.thumbnailContainer');

	toSlideTo = bottomThumb + 3;
	animationSpeed = 500;
	if(toSlideTo > totalSlides - 1) {
		toSlideTo = totalSlides - 1;
	}
	var slideDiff = toSlideTo - bottomThumb;
	var topIndent = parseInt(container.css('top')) - (slideDiff * thumbHeight);
	
	unbindEvents();
	container.animate({'top' : topIndent}, animationSpeed, bindEvents);
	
	bottomThumb += slideDiff;
	topThumb += slideDiff;
	addRemoveArrows();
}
/* This function slides the thumbnail images upwards*/
function thumbnailRegress() {
	if(topThumb == 0) return;
	var toSlideTo, animationSpeed;
	var container = $('.thumbnailContainer');
	toSlideTo = topThumb - 3;
	animationSpeed = 500;
	if(toSlideTo < 0) {
		toSlideTo = 0;
	}
	var slideDiff = toSlideTo - topThumb;
	var topIndent = parseInt(container.css('top')) - (slideDiff * thumbHeight);
	
	unbindEvents();
	container.animate({'top' : topIndent}, animationSpeed, bindEvents);
	
	topThumb += slideDiff;
	bottomThumb += slideDiff;
	addRemoveArrows();
}
/* This function makes it so the activeThumb is never off screen after a transition */
function checkThumbs() {
	var activeThumb = $('.activeThumb').index();
	var container = $('.thumbnailContainer');
	if(activeThumb == topThumb){
		if(activeThumb == 0) return; //Don't do anything if we are already at the top
		container.animate({'top' : parseInt(container.css('top')) + thumbHeight}, 200);
		topThumb--;
		bottomThumb--;
	} else if(activeThumb == bottomThumb) {
		if(activeThumb == totalSlides - 1) return;	 //Don't do anything if we are already at the bottom
		container.animate({'top' : parseInt(container.css('top')) - thumbHeight}, 200);
		bottomThumb++;
		topThumb++;
	} else if(activeThumb < topThumb) {
		var slideDiff = (topThumb+1) - activeThumb;
		if(activeThumb == 0) slideDiff = topThumb;	
		container.animate({'top' : parseInt(container.css('top')) + (thumbHeight * slideDiff)}, 200);
		bottomThumb -= slideDiff;
		topThumb -= slideDiff;
	} else if(activeThumb > bottomThumb) {
		var slideDiff = activeThumb - (bottomThumb-1);
		if(activeThumb == totalSlides - 1) slideDiff = (totalSlides - 1) - bottomThumb;
		container.animate({'top' : parseInt(container.css('top')) - (thumbHeight * slideDiff)}, 200);
		bottomThumb += slideDiff;
		topThumb += slideDiff;
	}
}

/* This function removes the advance arrow if you are on the last slide, or the regress arrow if you are on the first.
It also removes the top and bottom thumbnail arrows if there are no further thumbnails above or below, respectively. */
function addRemoveArrows() {
	if(currentSlide == 0) {
		$('#leftControl').css({'display' : "none"});
	} else {
		$('#leftControl').css({'display' : "block"});
	}
	if(currentSlide == totalSlides - 1){
		$('#rightControl').css({'display' : "none"});
	} else {
		$('#rightControl').css({'display' : "block"});
	}
	if(topThumb == 0) {
		$('#topThumbControl').css({'display' : 'none'});
	} else {
		$('#topThumbControl').css({'display' : 'block'});
	}
	if(bottomThumb == totalSlides - 1) {
		$('#bottomThumbControl').css({'display' : 'none'});
	} else {
		$('#bottomThumbControl').css({'display' : 'block'});
	}
}
/* Unbind all click events associated with animating */
function unbindEvents() {
	$('#rightControl').unbind("click");
	$('#leftControl').unbind("click");
	$('.thumbnailContainer .thumbnail').unbind("click");
	$('#bottomThumbControl').unbind("click");
	$('#topThumbControl').unbind("click");
}
/* Bind the click events associated with animating to their respective elements */
function bindEvents() {
/* Add events to the arrows */
	$('#rightControl').click(advanceSlide);
	$('#leftControl').click(regressSlide);
/* Add events to all the thumbnails */
	$('.thumbnailContainer .thumbnail').click(changeSlide);
/* Add events to the thumbnail arrows */
	$('#bottomThumbControl').click(thumbnailAdvance);
	$('#topThumbControl').click(thumbnailRegress);
}
/* A function to update the numbers that let you know which slide you are on */
function updateCurrent() {
	$('#current').text(currentSlide + 1);	
}
/* A function to push the slideshow advance/regress arrows over the image when you are hovering over it */
function revealArrows() {
	$('#leftControlImage').show();
	$('#rightControlImage').show();
}
/* A function to push the slideshow advance/regress arrows off to the side when you are not hovering over the image */
function hideArrows() {
	$('#leftControlImage').hide();
	$('#rightControlImage').hide();
}