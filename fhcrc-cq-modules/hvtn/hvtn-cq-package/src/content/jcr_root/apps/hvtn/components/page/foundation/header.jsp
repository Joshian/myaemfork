<%@include file="/libs/foundation/global.jsp" %>

<header>

	<div id="wrapper-header-fixed">
	
		<div id="wrapper-header">
	        
			<div id="header">
	      
				<div id="logo-hvtn">
					<% if (currentPage.getDepth() == 3){ %><h1><% } %>
					  <a href="<%= currentPage.getAbsoluteParent(1).getPath() %>/en.html"><img src="<%= currentDesign.getPath() %>/img/logos/hvtn_logo_2014_hvtn_header.png" alt="HIV Vaccine Trials Network" border="0"></a>
                    <% if (currentPage.getDepth() == 3){ %></h1><% } %>
				</div>
	          
				<div id="header-right">
	          
					<div id="logo-fred-hutch">
						<a href="http://www.fredhutch.org" target="_blank"><img src="<%= currentDesign.getPath() %>/img/logos/fred_hutch_logo_2014_hvtn_header.png" alt="Fred Hutchinson Cancer Research Center" border="0"></a>
					</div>
	
				<div class="clear"></div>
	
	<!-- Utility Navigation -->
	            <nav>  
					<div id="navigation-utility">
						<p>
							<span class="homenav"><a href="<%= currentPage.getAbsoluteParent(1).getPath() %>/en.html">Home</a>&nbsp; |&nbsp;</span> 
							<a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/news.html">News</a>&nbsp; |&nbsp; 
							<a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/resources.html">Resources</a>&nbsp; |&nbsp; 
							<a href="https://members.hvtn.org/SitePages/Home.aspx" target="_blank">Members</a>&nbsp; |&nbsp; 
							<a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/contact-us.html">Contact Us</a>&nbsp; |&nbsp; 
							<a class="header-search" href="<%= currentPage.getAbsoluteParent(2).getPath() %>/search.html"><span class="navigation-utility-highlight">Search</span></a>
						</p>
					</div>
					<div class="clear"></div>
				</nav>
	          
	<!-- End Utility Navigation -->              
	<!-- Search -->
	
	          <div id="header-search">
	            <form action="<%= currentPage.getAbsoluteParent(2).getPath() %>/search.html" method="get" autocomplete="on">
	                <input name="q" type="text" class="input-text-standard" />
	                <input type="image" alt="Search" src="<%= currentDesign.getPath() %>/img/icons/icon-search.png" />
	            </form>
	          </div>
	
	<!-- End Search -->
	              
	        </div>
	        <div class="clear"></div>
	          
	      </div>
	
	  	</div>
	
	<!-- Mobile Menus -->
	       
	    <div id="header-mobile-menu">  
	    
	      <img id="fhcrc_banner" src="<%= currentDesign.getPath() %>/img/logos/fhcrc-banner-responsive-highres.png" alt="Fred Hutchinson Cancer Research Center"><br/>
	    
	      <span id="mobile-search-icon">SEARCH</span>
	      
	      <a href="<%= currentPage.getAbsoluteParent(1).getPath() %>/en.html"><img id="mobile-logo-header" src="<%= currentDesign.getPath() %>/img/logos/logo-hvtn-no-globe.png" alt="HIV Vaccine Trials Network"></a>
	      
	      <div id="header-mobile-menu-toggle">
	      
	      	Menu
	        <img id="mobile-menu-icon" src="<%= currentDesign.getPath() %>/img/icons/icon-menu-isClosed.png" alt="open menu">
	        
	      </div>
	    
	    </div>
	    
	    <div id="header-mobile-search">
	      <form action="<%= currentPage.getAbsoluteParent(2).getPath() %>/search.html" method="get">
	          <input name="q" type="text" class="input-text-standard" />
	          <input type="image" alt="Submit" src="<%= currentDesign.getPath() %>/img/icons/icon-search.png" />
	      </form>
	    </div>
	
	<!-- End Mobile Menus -->
	<!-- Main Navigation -->        
		
		<cq:include script="globalNavigation.jsp"/>
	
	<!-- End Main Navigation -->
	
	  </div>
</header>