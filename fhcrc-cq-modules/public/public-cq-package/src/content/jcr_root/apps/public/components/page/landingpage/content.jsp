<%@include file="/libs/foundation/global.jsp" %>
 <!-- page content -->
 <div id="fh_body" class="fh_body container_12 clearfix"><a name="content"></a>

  <div class="grid_12">

    <!-- breadcrumbs appear in the content flow in case they wrap to two lines -->
    <cq:include script="breadcrumbs.jsp"/>

    <!-- all pages have the cluster title -->
    <cq:include script="clusterTitle.jsp" /> 
  
    
  </div>

  <!-- content column -->
  <% if("true".equals(properties.get("hasRightColumn","false"))){ %>
    <div class="grid_12 fh_rightcol_container fh_body_content clearfix">
      <div class="grid_8 fh_body_column alpha"><cq:include path="par" resourceType="foundation/components/parsys"/><!-- remote-wrap-api-marker --></div>    
      <div class="grid_4 omega"><div class="fh_rightcol_content"><%

        // For the public site's news landing page, and ONLY that page, we need the right column par to have a specific path name
        // so that children news pages can inherit it via iparsys. We don't like this hack, but it works, so, hey, let's go with it.

        if(currentPage.getPath().equals("/content/public/en/news")){%><cq:include path="newsparcolright" resourceType="foundation/components/parsys"/><% } else { 
        %><cq:include path="parcolright" resourceType="foundation/components/parsys"/><% } %></div></div>
    </div>    
    <div style="padding-bottom:0px;">&nbsp;</div>
  <% } else { %>
    <div id="fh_body_content" class="fh_body_content grid_12"><cq:include path="par" resourceType="foundation/components/parsys"/><!-- remote-wrap-api-marker --></div>
  <% } %>
  <!-- /content column -->
    
  <br class="clear"/>
   
 </div><!-- /fh_body, page content -->
