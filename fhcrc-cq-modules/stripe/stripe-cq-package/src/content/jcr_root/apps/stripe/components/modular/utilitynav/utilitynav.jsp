<%@include file="/apps/stripe/global.jsp" %>

<!-- utility nav background is outside the positioning context -->
<div id="fh_utility_nav">
    <div id="utility_nav" class="utility_nav">
	   <cq:include path="searchBox" resourceType="stripe/components/modular/searchbox" />
	    <%--
	    <cq:include path="searchBox" resourceType="stripe/components/modular/search" />
	    --%>
	   <ul>
           <li><a href="http://getinvolved.fhcrc.org/site/Donation2?df_id=3440&3440.donation=form1">Donate to Fred Hutch</a></li> 
		   <li><a href="http://fhcrc.org/en/careers.html">Careers</a></li> 
		   <li><a href="http://www.fhcrc.org/en/events.html">Events</a></li> 
		   <li><a href="http://fhcrc.org/en/util/directory.html">Directory</a></li>
		   <li><a href="http://www.fhcrc.org/en/contact-us.html">Contact Us</a></li> 
		   <li><a href="http://fhcrc.org/en/employees.html">For Employees</a></li> 
	    </ul>
    </div>
</div>