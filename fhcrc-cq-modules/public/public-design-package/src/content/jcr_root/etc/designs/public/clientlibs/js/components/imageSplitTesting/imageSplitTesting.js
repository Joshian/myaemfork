(function($) {
  $(document).ready(function() {
    // Prevents breakage when console doesn't exist. Should probably remve all the console statements before using the code. And delete this var.
    //var hasConsole = typeof console !== 'undefined' ? true : false;
    var namespace = 'split-';
    var coin = function( label ) {
      var activeTest = Math.random() < 0.5 ? 'a' : 'b';
      document.cookie = escape( namespace + label ) + '=' + activeTest;

      return activeTest;
    }
    
    var isFirstLoad = function( label ) { 
      if ( document.cookie.indexOf( escape( namespace + label ) ) === -1 ) {
        return true; 
      }
      else { 
        return false; 
      }
    };
    
    var getActiveTest = function( label ) {
    
      if ( document.cookie.indexOf( escape( namespace + label ) + '=a' ) !== -1 ) {    	 
        return 'a';
      }
      else if ( document.cookie.indexOf( escape( namespace + label ) + '=b' ) !== -1 ) {    	  
        return 'b';
      }
      else {
        return coin( label );
      }
    }
    
    ///////////////////////////////
    // Option 1 - Self calculating.
    
    // Input to recieve from the dialog.
    // - label
    // - description
//    var $img        = $( 'div.imageplus a.twoImageLink[href^="http://getinvolved.fhcrc.org"] img' ),
//        img         = coin(),
//        category    = 'A/B Image Tracking vs 1.0 | Option 1',
//        label       = twoImageLabel || 'no label set',
//        gaLabel,
//        description = twoImageDescription || 'no description set',
//        value;
//    
//    gaLabel = label + ' | ' + description;
//    
//    // Explicitly indicate the active version. Nothing relies on this atm.
//    $img.attr( 'data-img-active', img );
//    
//    switch ( img ) {
//      case 'a': 
//        if ( hasConsole ) {
//          console.log( 'leave it.' );
//        }
//        value = 0;
//        break;
//        
//      case 'b': 
//        if ( hasConsole ) {
//          console.log( 'switch it.' );
//        }
//        $img.attr( 'src', $img.attr( 'data-img-b' ) );
//        value = 1;
//        break;
//    }
//    
//    $img.load(function() {
//      _gaq.push( ['_trackEvent', category, 'Impression', label, value] );
//    });   
//    $img.closest( 'a' ).click(function() {
//      _gaq.push( ['_trackEvent', category, 'Clickthrough', label, value] );
//    });
    
    
    //////////////////////////////
    // Option 2 - Unique labeling.
    
    // Input to recieve from the dialog.
    // - labelA
    // - labelB
    // - descriptionA
    // - descriptionB
    
    var CAMPAIGN_LABEL_ATTR = "data-campaign-label",
        IMAGE_A_LABEL_ATTR  = "data-image-a-label",
        IMAGE_B_LABEL_ATTR  = "data-image-b-label";
    
    var $img         = $( 'div.imageplus a.twoImageLink img' ),
        $link        = $img.closest('a'),
        label        = $img.attr(CAMPAIGN_LABEL_ATTR) || 'Campaign', 
        gaLabel,
        img          = isFirstLoad(label) ? coin(label) : getActiveTest(label),
        category     = 'A/B Image Tracking',
        descriptionA = $img.attr(IMAGE_A_LABEL_ATTR) || 'Image A', 
        descriptionB = $img.attr(IMAGE_B_LABEL_ATTR) || 'Image B';
    
//    if (hasConsole) {
//          
//          console.log("LABEL = " + label);
//          console.log("DESCRIPTION_A = " + descriptionA);
//          console.log("DESCRIPTION_B = " + descriptionB);
//          
//    }
    
    // Explicitly indicate the active version. Nothing relies on this atm.
    $img.attr( 'data-img-active', img );

    switch ( img ) {
      case 'a':
//        if ( hasConsole ) {
//          console.log( 'leave it.' );
//        }
        gaLabel = label + ' | ' + descriptionA;
        break;
        
      case 'b': 
//        if ( hasConsole ) {
//          console.log( 'switch it.' );
//        }
        $img.attr( 'src', $img.attr( 'data-img-b' ) );
        
        var secondLink = $link.attr( 'data-href-b') || "";
        if (secondLink !== "") {
            $link.attr( 'href', secondLink);
        }

        gaLabel = label + ' | ' + descriptionB;
        break;
    }
    
    _gaq.push( ['_trackEvent', category, 'Impression', gaLabel, 1, true] ); // non-interaction event (9/8/2016)

    $link.click(function() {
      _gaq.push( ['_trackEvent', category, 'Clickthrough', gaLabel, 1] );
    });
    
    
//////////////////////////    
// Look at Math.random().
    
//    var x = 1000000, i;
//    var avg = 0;
//    for (i = 0; i < x; ++i) {
//      avg += Math.random() < 0.5 ? 0 : 1;
//    }
//    var denominator = parseInt( x.toString().substr( 0, x.toString().length - 2 ), 10 );
//    console.log( ( avg / denominator ).toString() + '%' );
    
  });
})(jQuery);