<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode,
    java.lang.Integer,
    org.fhcrc.stripe.functions.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%>

<%--
Removing this as it has served its purpose. ICS 6/24/15
<cq:include script="repairUtility.jsp"/>
--%>
<%
    Image image = new Image(resource, "image");
	boolean hasContent = image.hasContent();

// 	String divStyle="";
// 	String imgStyle="";
// 	String styleClass = "cq-dd-image";	
// 	String imageWidth = properties.get("width","");
// 	String imageHeight = properties.get("height","");
 	String useLightbox = properties.get("lightBox","false");
 	String imageLargeWidth = properties.get("largewidth","");
 	String imageLargeHeight = properties.get("largeheight","");	
 	String imageAlign = properties.get("alignImage","left");
 	String shadowAffect = properties.get("shadowAffect","outer");
 	String customTitle = image.getTitle();
	Integer styleWidth = new Integer(0);
	Integer dataHash = new Integer((int)Math.floor(Math.random() * Integer.MAX_VALUE));
 	
 	String linkURL = properties.get("linkURL","");
 	boolean isExternal = Boolean.parseBoolean(properties.get("externalLink","false"));
	boolean hasLink = linkURL != null && !linkURL.trim().equals("");
 	
 	Integer marginTop = Integer.decode(properties.get("marginTop","5"));
 	Integer marginBottom = Integer.decode(properties.get("marginBottom","5"));
	
 	boolean hasShadow = Boolean.parseBoolean(properties.get("showShadow","false"));
	
	String shadowClass = "";
	if (hasShadow) {
		
		if(shadowAffect.equals("outer")) {
			
			shadowClass = "shadowed";
			
		} else {
			
			shadowClass = "inner_shadowed";
			
		}
		
	}
        
    if(hasLink){
        linkURL = FHUtilityFunctions.cleanLink(linkURL, resourceResolver);
    }

    image.loadStyleData(currentStyle);
    image.setSelector(".img"); // use image script
    image.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        image.setSuffix(currentDesign.getId());
    }
    
    %>

    <% 
    
    if(image.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT){ 
    
    	if (image.hasContent()) {
    		
    		if(image.get(Image.PN_WIDTH) == null || image.get(Image.PN_WIDTH).trim().equals("")) {
                styleWidth = new Integer(image.getLayer(true,true,true).getWidth());
                log.warn("No Image.PN_WIDTH. StyleWidth = " + Integer.toString(styleWidth));
            } else {
                styleWidth = new Integer(image.get(Image.PN_WIDTH));
                log.info("StyleWidth = " + Integer.toString(styleWidth));
            }
    		
    	}
    
    %>
    
<div style="margin-top: <%= marginTop.toString() %>px; margin-bottom: <%= marginBottom.toString() %>px">

	<div align="<%= imageAlign %>">
	
		<cq:text property="image/jcr:title" tagName="strong" tagClass="custom_image_title" placeholder="" />
	
		<div id="aldiv<%= dataHash.toString() %>" class="imageContainer <%= shadowClass %> <%= !hasContent ? "cq-edit-only" : "" %>" 
		style="width: <%= styleWidth != null && styleWidth.intValue() != 0 ? Integer.toString(styleWidth) : "" %>px"
		data-use-lightbox="<%= useLightbox %>" data-large-width="<%= imageLargeWidth %>" data-large-height="<%= imageLargeHeight %>">
   
   		<% if (hasLink) { %>
   		
   			<a href="<%= linkURL %>" <%= isExternal ? "target=\"blank\"" : "" %>>
   			
   		<% } 
   		
		   image.draw(out);
    
    	if (hasLink) { %>
    	
    		</a>
    		
    	<% } %>
    
    	</div>
    	
    	<cq:text property="jcr:caption" tagName="small" tagClass="image_caption" placeholder="" />
    	
    </div>
    
</div>

	<% } %>
	
<script type='text/javascript'>

jQuery(window).load(function()
{
	var innerDiv = '#aldiv<%= dataHash.toString() %>';
	$(innerDiv).each(function() 
    {    
		var $this = $(this);   
		var imageWidth = $this.width();
		var imageHeight = $this.height();
		if(imageWidth != 0)
		{
	        jQuery('#aldiv<%= dataHash.toString() %>').css({'width': imageWidth}); 
		}
		if(imageHeight != 0)
		{
	        jQuery('#aldiv<%= dataHash.toString() %>').css({'height': imageHeight}); 
		}
	}); 
 

jQuery('.body_content div.image div.imageContainer').click(function()
{
	var uselightbox = jQuery(this).data('useLightbox');
    if (uselightbox == true) {

      jQuery(this).css('cursor','pointer');

//      var imgname="";
      var imgpath="";
      var imageLargeWidth=0;
      var imageLargeHeight=0;

//      imgname = jQuery(this).attr('fileName');
      imgpath = jQuery(this).find("img").attr('src');
      imageLargeWidth = jQuery(this).data('largeWidth');
      imageLargeHeight = jQuery(this).data('largeHeight');
      jQuery('#test_modal').find('#pictureLocation').attr('src',imgpath); 

      // WHEN THE IMAGE WIDTH IS GIVEN 
      if(imageLargeWidth != "")
      {
    	  //jQuery('#test_modal').find('#pictureLocation').attr('width',imageLargeWidth);
    	  //jQuery('#test_modal').find('#pictureLocation').attr('height',imageLargeHeight);
    	  
    	  if(imageLargeWidth < 1000)
          {
              jQuery('#test_modal').css('width', imageLargeWidth);         
              jQuery('#test_modal').find('#pictureLocation').css('width',imageLargeWidth);
          }
          else if(imageLargeWidth >= 1000)
          {
              jQuery('#test_modal').css('width', 1000);
              jQuery('#test_modal').find('#pictureLocation').css('width',1000);
          }
      }

      // WHEN THE IMAGE WIDTH IS NOT GIVEN 
      if(imageLargeWidth == "")
      {
        var img = new Image();
        img.src = imgpath;
        var imgWidth = img.width;
        var imgHeight = img.height;

        if(imgWidth < 560)
        {
            jQuery('#modal_body').css('width', imgWidth);
            jQuery('#test_modal').css('width', imgWidth+30);
            jQuery('#test_modal').css('overflow','hidden');
        }
        else if(imgWidth >= 1000)
        {
            jQuery('#test_modal').css('width', 1000);
            jQuery('#test_modal').css('width', 1000+30);
            jQuery('#test_modal').css('overflow-x','auto');
        }
        else if (imgWidth >= 560 && imgWidth < 1000 )
        {
            jQuery('#test_modal').css('width', imgWidth);
            jQuery('#test_modal').css('width', imgWidth+30);
            jQuery('#test_modal').css('overflow-x','hidden');
        }
      }

      // WHEN THE IMAGE HEIGHT IS GIVEN
      if(imageLargeHeight != "")
      {
          jQuery('#test_modal').find('#pictureLocation').css('height',imageLargeHeight);
          
	      if(imageLargeHeight > jQuery(window).height())
          {
              var ret = 0;
              ret=parseFloat(jQuery('#test_modal').css('height')); 
              ret = ret-100;
              jQuery('#test_modal').css('top', '5%');

              var windowHeight = jQuery(window).height()-70;
              windowHeight = windowHeight - 70;

              var windowMaxHeight = jQuery(window).height();
              windowMaxHeight = windowMaxHeight - 100;
              
              jQuery('#test_modal').css('height', windowHeight);
              jQuery('#modal_body').css('max-height',windowMaxHeight);
              jQuery('#modal_body').css('height',ret);
              jQuery('#modal_body').css('overflow-y','scroll');
          }
          else
          {
              var ret = 0;
              ret=parseFloat(jQuery('#test_modal').css('height')); 
              ret = ret-100;

              var windowMaxHeight = jQuery(window).height();
              windowMaxHeight = windowMaxHeight - 150;
              
              var windowHeight = jQuery(window).height()-70;
              windowHeight = windowHeight - 70;

              jQuery('#test_modal').css('top', '20%');

              if(windowMaxHeight >= 650)
              {
            	  jQuery('#test_modal').css('height', 650);
              }
              else
              {
            	  jQuery('#test_modal').css('height', windowMaxHeight);
              }
              
              jQuery('#modal_body').css('max-height',windowMaxHeight);
              jQuery('#modal_body').css('height',ret);
          }
      }

      // WHEN THE IMAGE HEIGHT IS NOT GIVEN
      if(imageLargeHeight == "")
      {
    	  var img = new Image();
          img.src = imgpath;
          var imgHeight = img.height;
          if(imgHeight > jQuery(window).height())
          {
              var ret = 0;
              ret=parseFloat(jQuery('#test_modal').css('height')); 
              ret = ret-100;

              var windowHeight = jQuery(window).height()-70
              windowHeight = windowHeight - 70;

              var windowMaxHeight = jQuery(window).height();
              windowMaxHeight = windowMaxHeight - 100;
              
              jQuery('#test_modal').css('height', windowHeight);
              jQuery('#modal_body').css('max-height',windowMaxHeight);
              jQuery('#modal_body').css('height',ret);                      
              jQuery('#modal_body').css('overflow-y','scroll');

              jQuery('#test_modal').css('top', '5%');

          }
          else
          {
        	  var ret = 0;
              ret=parseFloat(jQuery('#test_modal').css('height')); 
              ret = ret-100;

              var windowMaxHeight = jQuery(window).height();
              windowMaxHeight = windowMaxHeight - 150;
              jQuery('#test_modal').css('top', '20%');
              jQuery('#test_modal').css('height', imgHeight+75);
              jQuery('#modal_body').css('max-height',imgHeight);
              //jQuery('#modal_body').css('height',imgHeight);  
              jQuery('#test_modal').find('#pictureLocation').css('height',imgHeight);

              
              jQuery('#modal_body').css('overflow-y','hidden');
              jQuery('#test_modal').css('overflow-y','hidden');
          }
      }
      jQuery('#test_modal').modal('show');
    }
});
});

</script>