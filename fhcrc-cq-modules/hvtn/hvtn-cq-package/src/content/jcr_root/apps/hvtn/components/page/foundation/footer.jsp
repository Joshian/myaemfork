<%@ page import="java.util.Calendar" %><%@include file="/libs/foundation/global.jsp" %><%

Calendar theTime = Calendar.getInstance();
Integer theYear = theTime.get(Calendar.YEAR);

%>
<footer>

	<div id="wrapper-footer-outer">
	
		<div id="footer">
	    	<nav>
				<div id="footer-mobile-util-nav">
		      
					<p><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/news.html">News</a> | <a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/resources.html">Resources</a> | <a href="https://members.hvtn.org/SitePages/Home.aspx" target="_blank">Members</a> | <a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/contact-us.html">Contact&nbsp;Us</a></p>
		      
				</div>
			</nav>
	  
	    	<div id="footer-social">
				<div class="footer-social-item"><a href="https://www.facebook.com/helpendhiv" target="_blank"><img src="<%= currentDesign.getPath() %>/img/icons/icon-facebook.png" alt="facebook icon"></a></div>
				<div class="footer-social-item"><a href="https://twitter.com/HelpEndHIV" target="_blank"><img src="<%= currentDesign.getPath() %>/img/icons/icon-twitter.png" alt="twitter icon"></a></div>
				<div class="footer-social-item"><a href="http://www.linkedin.com/company/fred-hutchinson-cancer-research-center" target="_blank"><img src="<%= currentDesign.getPath() %>/img/icons/icon-linkedin.png" alt="linkedin icon"></a></div>
				<div class="footer-social-item"><a href="https://www.youtube.com/channel/UCnWBh22UKMHF4VRKdsQvZqg/featured" target="_blank"><img src="<%= currentDesign.getPath() %>/img/icons/icon-youtube.png" alt="youtube icon"></a></div>
				<!--<div class="footer-social-item"><a href="#" target="_blank"><img src="<%= currentDesign.getPath() %>/img/icons/icon-vimeo.png" alt="vimeo icon"></a></div>-->
				<div class="clear"></div>    
			</div>
	      
			<div id="footer-text">
				
				<p class="affiliation">
					<span class="title">Fred Hutchinson Cancer Research Center</span><br />
					<span class="address">1100 Fairview Ave. N., P.O. Box 19024, Seattle, WA 98109<br /></span>
					<span class="copyright">&copy; <%= theYear %> <a href="http://www.fredhutch.org/">Fred Hutchinson Cancer Research Center</a><br /></span>
					<span class="nonprofit">A 501(c)3 Nonprofit Organization</span>
				</p>
	        
				<p class="privacy-links">
					<a href="http://www.fredhutch.org/en/util/terms-privacy.html" target="_blank">Terms of Use &amp; Privacy Policy</a> | 
					<a href="http://www.fredhutch.org/en/util/conflict-of-interest.html" target="_blank">Conflict of Interest</a>
				</p>    
	
				<dl class="itemlist">
					<dd>
						<ul>
							<li><a href="http://www.fredhutch.org/en.html" target="_blank">Fred Hutchinson Cancer Research Center</a></li> 
							<li><a href="http://lab.hvtn.org/SitePages/Home.aspx" target="_blank">HVTN Lab Portal</a></li>
						</ul>
					</dd>
				</dl>
				
				<p class="more-information">For more information contact <a href="mailto:info@hvtn.org">info@hvtn.org</a></p>
	            
			</div>
	      
		</div>
	      
	</div>
	
</footer>