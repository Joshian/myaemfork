/* js for the fhcrc mobile website */

/* scroll the webpage to hide the address bar */
addEvent(window,"load",function(){setTimeout(function(){window.scrollTo(0,1);})});

/* move the campus, menu and search drop downs into the correct position,
   and set up the click events that will trigger them.                     */
function initMenus(){ //return;
  var el,q;
  // position the overlay menus correctly.
  if(! (el = document.getElementById("menu_anchor")) ) { return; }
  if( q = document.getElementById("campus_menu")){
    el.appendChild(q.parentNode.removeChild(q));
  }
  if( q = document.getElementById("search_menu")){
    el.appendChild(q.parentNode.removeChild(q));
  }
  if( q = document.getElementById("menu_menu")){
    el.appendChild(q.parentNode.removeChild(q));
  }

  // 1) add a click event on the whole she-bang that closes the menus. this works
  // in conjunction with part 2) which prevents this from happening when you 
  // click the menu itself.
  var el = document.querySelector('.fh_content'); // we use the area from the content down, to avoid overloading the three menu buttons & topmost logo. 
  if(el) addEvent( el,"click",hideMenus );
  // 2) cancel click propagation on the three menus so clicks outside of them
  // can cause them to disappear (see 1)
  var els = Array('#search_menu','#menu_menu','#campus_menu');
  for( var m=0; m<els.length; m++ ){
    if( el = document.querySelector( els[m] ) ){
      addEvent( els[m], "click", function(e){ e.stopPropagation(); } );
    }
  }
  
  // set up onclick handlers to activate/deactivate the menus
  if( el = document.querySelector("div.sitewidenav ul li.campus") ){
    addEvent(el, "click", function(){ 
      if(!menuIsActive('campus')){ showMenu('campus') } else { hideMenus() }
    });
  } 
  if( el = document.querySelector("div.sitewidenav ul li.search") ){
    addEvent(el, "click", function(){ 
      if(!menuIsActive('search')){ showMenu('search') } else { hideMenus() }
    });
  }
  if( el = document.querySelector("div.sitewidenav ul li.menu") ){
    addEvent(el, "click", function(){ 
      if(!menuIsActive('menu')){ showMenu('menu') } else { hideMenus() }
    });
  }
  // disable the links within the three button menus, because we already add click handlers on their parent li
  if( el = document.querySelector("div.sitewidenav ul li.campus a") ){//alert(el);
    addEvent(el, "click", function(e){ e.preventDefault();return false; } ); 
  }
  if( el = document.querySelector("div.sitewidenav ul li.menu a") ){//alert(el);
    addEvent(el, "click", function(e){ e.preventDefault();return false; } ); 
  }
  if( el = document.querySelector("div.sitewidenav ul li.search a") ){//alert(el);
    addEvent(el, "click", function(e){ e.preventDefault();return false; } ); 
  }
  // create the little daggers
  // model: <div class="dagger"><img src="img/dagger-border.png" alt="" /></div>
  var els = Array('div.search_menu div.search_dropdown', 'div.campus_menu div.campus_dropdown', 'div.menu_menu div.menu_dropdown'); /* after which elements to insert each dagger */
  for( var m=0; m<els.length; m++ ) {
    if( el = document.querySelector( els[m] ) ){
      var q2 = document.createElement('img');q2.setAttribute('src', '/etc/designs/public_mobile/img/dagger-border.png');q2.setAttribute('alt', '');
      q = document.createElement('div'); q.setAttribute('class', 'dagger');
      q.appendChild(q2);
      el.appendChild(q);
    }
  }
  
  // the campus menu needs to make the li's clickable for every <li><a> within the ul
  var els = document.querySelectorAll("div.campus_dropdown ul li");
  for( var i=0; i<els.length; i++ ){
    var el = els[i]; //alert(el);
    addEvent( el,"click",function(e){
      var a = e.target.querySelector("a"); //alert(a.getAttribute('href'));
      hideMenus(); // iOS needs this for some reason
      if(a) { window.location.href = a.getAttribute('href'); } // any nicer way to do this?
    });
  }  
  
  return;
}
addEvent(window,"load",initMenus);

/* hide all of the menus */
function hideMenus(){ //alert('hiding');
  var el,m;
  var els = Array('campus', 'search', 'menu');
  /* hide all menus */
  for( m=0; m<els.length; m++ ){
    if( el = document.getElementById( els[m]+'_menu' ) ){
      el.style.display = 'none';
    }
  }
  // reset the active state to none
  if( el = document.querySelector("div.sitewidenav ul") ){
    el.className = '';
  }
  // and change the icons to the inactive ones
  for( var m=0; m<els.length; m++ ){
    if( el = document.querySelector("div.sitewidenav ul li."+ els[m] + " img" )){
      el.src = '/etc/designs/public_mobile/img/icon_'+ els[m] + '.png';
    }
  }
}

/* show one menu. pass in the name of the desired menu: campus, menu or search.
   will turn off all menus before showing the target menu.  */
function showMenu(m){ //alert(m);
  var el;
  hideMenus();
  if( el = document.getElementById(m+"_menu") ){
    el.style.display = 'block';
  }
  // alter the class on the ul so this menu is now active
  if( el = document.querySelector("div.sitewidenav ul") ){
    el.className = m;
  }  
  // and change the icon to the active one
  if( el = document.querySelector("div.sitewidenav ul li."+ m + " img" )){
    el.src = '/etc/designs/public_mobile/img/icon_'+m+'_over.png';
  }
}

/* determine the display state of the passed menu (search, menu or campus). the state is either "none"
   or "block". returns true if the menu is visible, false otherwise */
function menuIsActive(m){
  var el;
  if( el = document.getElementById(m+"_menu") ){
    if(el.style.display=='block') { return true; }
  }
  return false;
}
