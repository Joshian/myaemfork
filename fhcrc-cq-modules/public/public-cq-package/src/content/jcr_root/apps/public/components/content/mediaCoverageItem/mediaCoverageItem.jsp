<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.text.SimpleDateFormat,java.text.DateFormat,org.fhcrc.tools.FHUtilityFunctions" %>
<div class="mediacoveragecontainer">
<%
String linkTarget = properties.get("linkTarget","");
String pubDate = properties.get("date","");
String dateDisplay = properties.get("dateDisplay","");
Boolean isExternal = false;

if(dateDisplay.trim().equals("")){
    DateFormat g = new SimpleDateFormat("MM/dd/yy");
    if(!pubDate.equals("")){
        pubDate = FHUtilityFunctions.APStylize(g.parse(pubDate));
    }
} else {
    pubDate = dateDisplay;
}

linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
if(!linkTarget.matches("^/.*")){
    isExternal = true;
}

%>
<%-- headline/title --%>
<div class="item_headline">
<% if(!linkTarget.equals("")){ %><a href="<%= linkTarget %>" <%= isExternal ? " target=\"_blank\"" : "" %>><% } %><cq:text property="headline" /><% if(!linkTarget.equals("")){ %></a><% } %>
</div>

<%-- publication and date (two on this single line) --%>
<div class="item_attribution">
 <cq:text property="publication" tagName="span" tagClass="item_source" placeholder="" /><%= properties.get("publication","").trim().equals("") ? "" : ", " %><span class="item_date"><%= pubDate %></span>
</div>

<%-- abstract/synopsis --%>
<% if(!properties.get("additionalText","").trim().equals("")){ %>
<cq:text property="additionalText" tagName="div" tagClass="item_description" placeholder="" />
<% } %>

</div>