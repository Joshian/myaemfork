<%@include file="/apps/stripe/global.jsp" %>

<jsp:useBean id="navItemBean" scope="request" class="org.fhcrc.stripe.components.NavDropdownItem" />
<jsp:setProperty name="navItemBean" property="componentProperties" value="<%= properties %>"/>
<jsp:setProperty name="navItemBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="navItemBean" property="resource" value="<%= resource %>"/>
<jsp:setProperty name="navItemBean" property="currentPage" value="<%= currentPage %>"/>
<c:choose>
    <c:when test="${navItemBean.selected}">
        <li class="dropdown selected" editMode="<%=editMode%>" <% if(!navItemBean.getWidth().equals("")) { %> style="width: <%=navItemBean.getWidth()%>px;" <% }%>>
    </c:when>
    <c:otherwise>
        <li class="dropdown" editMode="<%=editMode%>" <% if(!navItemBean.getWidth().equals("")) { %> style="width: <%=navItemBean.getWidth()%>px;" <% }%>>
    </c:otherwise>
</c:choose>
		<c:choose>
		    <c:when test="${navItemBean.externalLink}"> 
		        <a href="${navItemBean.itempath}" class="dropdown-toggle" target="_blank"><p><c:out value="${navItemBean.itemlabel}"/></p></a>
		    </c:when>
		    <c:otherwise>
		        <a href="${navItemBean.itempath}.html" class="dropdown-toggle"><p><c:out value="${navItemBean.itemlabel}"/></p></a>
		    </c:otherwise>   
		</c:choose>
		<ul class="dropdown-menu">
		    <cq:include path="navsubitems" resourceType="foundation/components/parsys"/>
		</ul>
    </li> 
<div class="clearfix"></div>
              
              