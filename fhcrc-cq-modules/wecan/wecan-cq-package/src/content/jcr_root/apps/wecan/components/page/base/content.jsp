<%@include file="/libs/foundation/global.jsp"%><%
%>

  <div class="bodyContent" id="bodyContent">
    
    <cq:include script="breadcrumbs.jsp" /> 

    <div class="pageContent" id="pageContent">  
      
      <cq:include path="mainTitle" resourceType="foundation/components/title" />
        
      <cq:include path="editableContent" resourceType="foundation/components/parsys"/>
          
      <!-- remote-wrap-api-marker -->
    </div>   
      
  </div>