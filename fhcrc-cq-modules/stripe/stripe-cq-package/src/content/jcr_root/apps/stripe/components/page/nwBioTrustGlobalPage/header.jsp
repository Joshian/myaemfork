<%@include file="/apps/stripe/global.jsp" %>
<%
String edit = (editMode.equals("true"))?"editmode":"";
String home = Text.getAbsoluteParent(currentPage.getPath(), 2);
String reqSampleColl = home.concat("/researchers/request.html");
String prefixNukedReqSampleColl = resourceResolver.map(reqSampleColl); // handle author/publisher in the onclick handler of the button, below. sld july/3/2013.
%>
<jsp:useBean id="pageUtilBean" scope="request" class="org.fhcrc.stripe.utilities.PageUtil" />
<%--
<div class="container" id="utility_nav_container">
    <cq:include path="utilitynav" resourceType="stripe/components/modular/utilitynav"/>  
</div>
 --%>
<div class="navbar navbar-inverse navbar-nwbiotrust-top">
  <div>
      <div class="navbar-nwbiotrust-top-bar">
        <div class="container">
          <div class="row">
            <span class="span12">
            </span>
          </div>
        </div>
      </div>
      
      <div class="nwbiotrust-logobar">
        <div class="container">
          <div class="row">
            <span class="span12">
               <%--
               <cq:include path="fhcrclogo" resourceType="stripe/components/modular/logo"/>
                --%>
                <div style="float: left;">
                    <a href="<%=home%>.html"><img src="/etc/designs/stripedesigns/nwbiotrust/clientlibs/img/NWBioTrustlogo.jpg"/></a>
                </div>
                <div style="float: left; padding-left: 545px; width: 150px; padding-top: 18px;">
                    <a href="https://research.nwbiotrust.org/" target="_blank">
                        <button class="nwbiobtn" type="button">&nbsp;&nbsp;Request&nbsp;&nbsp; Samples</button>
                    </a>
                </div>
            </span>
          </div>
        </div>
      </div>
      <div class="lab-title">
        <div class="container" style="height: 10px;">
          <div class="row">
            <span class="span12">
                <%--
                <cq:include path="labtitle" resourceType="stripe/components/modular/title" />
                 NOTHING IN THIS TOO --%>
            </span>
          </div>
        </div>
      </div>
      <div class="navitems">
        <%-- <div class="clearfix"></div> --%>
        <div class="container">
          <div class="row">
              <div class="span12">
                  <div class="home">
                        <a href="<%=home%>.html"><img src="/content/dam/nwbiotrust/logos/Home_35x35.png"></a>
                  </div>
                  <div class="nav-collapse collapse" style="float:left; padding-left:85px;" >
                    <ul class="nav">
                        <cq:include path="navitemsiparsys" resourceType="foundation/components/iparsys" />
                    </ul>
                  </div><!--/.nav-collapse -->
              </div>
          </div>
        </div>
      </div>
    </div>
</div>
<cq:include script="subheader.jsp" />
<cq:include script="header_style.jsp"/>
<style type="text/css">

/** Button **/

.nwbiobtn {
  display: inline-block;
  padding: 4px 14px;
  margin-bottom: 0;
  font-size: 14px;
  line-height: 20px;
  color: #FFFFFF;
  text-align: center;
  /*text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);*/
  vertical-align: middle;
  cursor: pointer;
  background-color: #0971ce; /* added 9/6/13 for safari sld */
  background-repeat: repeat-x;
  border: 1px solid #bbbbbb;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  background-image: -moz-linear-gradient(center top , #0971ce, #0971ce);
  filter: progid:dximagetransform.microsoft.gradient(startColorstr='#0971ce', endColorstr='#0971ce', GradientType=0) !important;
  background-image: linear-gradient(to bottom, #0971ce, #0971ce) !important;
  border-radius: 4px  !important;
  -webkit-border-radius: 4px;
     -moz-border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
     -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
          box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
}

.nwbiobtn:hover {
   font-weight: bold;
}
</style>