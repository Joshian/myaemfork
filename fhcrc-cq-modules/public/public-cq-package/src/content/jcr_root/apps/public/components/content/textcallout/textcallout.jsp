<%@include file="/libs/foundation/global.jsp"%><%@ page import="com.day.cq.wcm.api.WCMMode"%><%
%>

    <% // options are accounted for using classes
       String styleClasses = "";    
       styleClasses += (properties.get("noBorder","").equals("true")) ? "calloutnoborder " : "";
       styleClasses += (properties.get("calloutWidth","").equals("wide")) ? "calloutwide " : "";
       styleClasses += (properties.get("calloutAlign","").equals("left")) ? "calloutleft " : "";
       styleClasses += (properties.get("squareBullets","").equals("true")) ? "calloutsquarebullets " : "";
       
       // hide the calloutbox when nothing in it
       styleClasses += (properties.get("calloutText","").equals("") && !(WCMMode.fromRequest(request) == WCMMode.EDIT)) ? "hidecallout " : "";
       
     %>

    <div class="textcallout clearfix <%= styleClasses %>">
      <% if(properties.get("calloutText","").equals("")){ %>
        <div class="cq-edit-only"><cq:text property="calloutText" tagClass="callout" tagName="div"/></div>
      <% } else { %>
        <cq:text property="calloutText" tagClass="callout" tagName="div"/>
      <% } %>
      <cq:text property="text" tagName="div" tagClass="text" /></div>