<%@include file="/apps/stripe/global.jsp" %>
<%@ page import="org.fhcrc.stripe.utilities.PageUtil" %>
<%
	String title = properties.get("title", "Heading 3 Text Here");
	String linkURL = properties.get("linkURL", "");
	request.setAttribute("linkURL",linkURL);
	String target = "";
	boolean linkLocation = properties.get("linkLocation", false);
	if(linkLocation)
	{
	    target = "_blank";
	}
%>
<div class="vertvnavheading3">
    <c:choose>
        <c:when test="${!empty linkURL}">
            <a href="<%=linkURL%>" target="<%=target%>"><h4><%=title%></h4></a>
         </c:when>
         <c:otherwise>
           <h4><%=title%></h4>
        </c:otherwise>
    </c:choose>
</div> 