<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.WCMMode,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    java.lang.Integer,
    org.apache.commons.lang.StringEscapeUtils,
    java.util.ArrayList,
    java.util.TreeSet,
    java.util.Comparator,
    java.util.Iterator,
    java.util.Arrays,
    org.fhcrc.tools.FHUtilityFunctions" %>
<%

/* The arrays are of length 2: x[0] is the order of the slide (as given by user input)
 * x[1] is the number of the slide (1-6). If the order is not given by the user, the slide is
 * given order = 100. If two slides have the same order, the tie is decided by the number
 * of the slide (x[1]).
 */

Comparator<Float[]> arrayCompare = new Comparator<Float[]>() {
    public int compare(Float[] a, Float[] b){
        Float orderA = a[0];
        Float orderB = b[0];
        if(orderA==null){
            orderA = (float)100;
        }
        if(orderB==null){
            orderB = (float)100;
        }
    
        if(orderA.equals(orderB)){
            return a[1].compareTo(b[1]);
        }
        return orderA.compareTo(orderB);
    }
};

/* The metadata is a series of Float arrays, 6 arrays that each contain 2 points of data. */
Float[][] slideMetadata = new Float[6][2];
Float tmpOrder;

for(int metacounter = 0; metacounter < 6; metacounter++) {
	/* If the user doesn't enter an order, set the order at 100 (reasonably large enough that it should come last) */
	tmpOrder = Float.parseFloat(properties.get("highlight_"+(metacounter+1)+"/order","100"));
	slideMetadata[metacounter][0] = tmpOrder;
	slideMetadata[metacounter][1] = (float)(metacounter+1);
}

Arrays.sort(slideMetadata, arrayCompare);


String[] linkTargets = new String[6];
Boolean[] externalLink = new Boolean[6];
String[] textContent = new String[6];
String[] titles = new String[6];
//String[] linkTexts = new String[6];
String temp = "";
String textProperty = "";
//String linkTextProperty  = "";
String speed = properties.get("speed","0");
Image[] imageArray = new Image[6];
Boolean[] invalidArray = new Boolean[6];
int slideNumber;
Boolean hasTheFirstSlideBeenPrinted = false;

for(int i = 0; i < 6; i++) {
	
	slideNumber = slideMetadata[i][1].intValue();
	/* put the linkTargets into their array */
	temp = properties.get("highlight_"+(slideNumber)+"/linkTarget","");
	if (temp.trim().equals("")) {
		temp = currentPage.getPath(); /* If there is no link, make the link the current page */
	}
	temp = FHUtilityFunctions.cleanLink(temp, resourceResolver);
    linkTargets[i] = temp;
    
    /* Whether or not the links are external */
    if(properties.get("highlight_"+(slideNumber)+"/externalLink","false").equals("true")) {
        externalLink[i] = true;
    } else {
        externalLink[i] = false;
    }
    
    /* Next is the textContent */
    textContent[i] = properties.get("highlight_"+(slideNumber)+"/text","");
	
    /* The titles */
    titles[i] = properties.get("highlight_"+(slideNumber)+"/title","");
    
    /* The link texts */
    /*
    linkTexts[i] = properties.get("highlight_"+(slideNumber)+"/linkText","");
    */
    
    /* The images */
    imageArray[i] = new Image(resource, "image"+(slideNumber));
    
    /* Whether or not the slides are invalid */
    if(properties.get("highlight_"+(slideNumber)+"/isInvalid","false").equals("true")) {
    	invalidArray[i] = true;
    } else {
    	invalidArray[i] = false;
    }
    
}

%>
<div id="homepageSliderContainer">
<%        
    String tabnum = "";
    int tab = 0;

    for(int i = 0; i < imageArray.length; i++) {
      	if(imageArray[i].hasContent()){
/* If the current highlight has been marked invalid, skip it */
      		if(invalidArray[i] == true && WCMMode.fromRequest(request) != WCMMode.EDIT){
      			continue;
      		}

            tab++;
            tabnum = Integer.toString(tab); // a counter that's used with Google Analytics event tracking.
            

      		/* Set the image src attribute to avoid 301 redirects */
      		if (imageArray[i].hasContent()) {
	        	imageArray[i] = FHUtilityFunctions.nukeURLPrefix(imageArray[i], resourceResolver);
      		}

       		imageArray[i].loadStyleData(currentStyle);
       		imageArray[i].setSelector(".img"); // use image script
       	    imageArray[i].setDoctype(Doctype.fromRequest(request));
       	    // add design information if not default (i.e. for reference paras)
            if (!currentDesign.equals(resourceDesign)) {
                imageArray[i].setSuffix(currentDesign.getId());
            }
/* If the image title is just spaces, set the title to the empty string */       	    
/* 
			if(properties.get("image"+(slideMetadata[i][1])+"/jcr:title","").trim().equals("")) {
       	        imageArray[i].setTitle("");
       	    }
*/
%>
    <div 
    
<% 
	if (!hasTheFirstSlideBeenPrinted) {
		out.print("id=\"firstContainer\" ");
		hasTheFirstSlideBeenPrinted = true;
	}
%>
		class="homepageFadeContainer">
        <div class="homepagePhoto">
            <a href="<%= linkTargets[i] != null && !linkTargets[i].trim().equals("") ? linkTargets[i] : "#" %>" 
            onclick="_gaq.push(['_trackEvent','Large-Image Slider','Slider Image', $('h2',$(this).closest('.homepageFadeContainer')).text(), <%=tabnum%>]);"
            <%= externalLink[i] == true ? "target=\"_blank\"" : "" %>>
                <% imageArray[i].draw(out); %>
            </a>
   	    </div>
   	    <div class="homepageText">
   	        <h2>
				<a href="<%= linkTargets[i] != null && !linkTargets[i].trim().equals("") ? linkTargets[i] : "#" %>" 
					onclick="_gaq.push(['_trackEvent','Large-Image Slider','Slider Title', $(this).text(), <%=tabnum%>]);"
				    <%= externalLink[i] == true ? "target=\"_blank\"" : "" %>>
   	        			<%= titles[i] != null && !titles[i].trim().equals("") ? titles[i] : "MISSING TITLE TEXT" %>
   	         	</a>
   	        </h2>
           	<div class="textline">
           		<a href="<%= linkTargets[i] != null && !linkTargets[i].trim().equals("") ? linkTargets[i] : "#" %>" 
           		onclick="_gaq.push(['_trackEvent','Large-Image Slider','Slider Text', $('h2',$(this).closest('.homepageFadeContainer')).text(), <%=tabnum%>]);" 
           		<%= externalLink[i] == true ? "target=\"_blank\"" : "" %>>
           			<%= textContent[i] != null && !textContent[i].trim().equals("") ? textContent[i] : "" %>
           		</a>
           	</div>

        </div>

    </div>
<%
        }
    }
%>
    <div id="homepageSliderDots">
<%
    for(int i = 0; i < imageArray.length; i++) {
/* Don't want to display the dot if it doesn't have a matching image */
        if(imageArray[i].hasContent()){ 
/* If the current highlight has been marked invalid, we don't want a dot either */
            if(invalidArray[i] == true && WCMMode.fromRequest(request) != WCMMode.EDIT){
                continue;
            }
%>	            	
	    <div class="homepageDot">
	        <img src="<%= currentDesign.getPath() %>/img/bullets/dot_grey.png" alt="" />
	    </div>
<%	            	
	    }
    }
%>
    </div>
</div>
<cq:includeClientLib js="public.components.homepageSlider" />