<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="java.util.Iterator,
    com.day.cq.wcm.api.PageFilter,
    org.fhcrc.tools.FHUtilityFunctions" %>

<%-- Ian, this can build according to all the direct
     children of the cluster head. --%>
     
<%
Page clusterHead = currentPage;
PageFilter filter = new PageFilter();
String test = clusterHead.getProperties().get("isClusterHead", "false");
while(!"true".equals(test)){
    if(clusterHead.getParent() == null){
        // If we have hit the top of the tree, then use the home page as the clusterHead
        clusterHead = currentPage.getAbsoluteParent(2);
        break;
    }
    clusterHead = clusterHead.getParent();
    test = clusterHead.getProperties().get("isClusterHead", "false");
}
Iterator<Page> menuItems = clusterHead.listChildren(filter);
%>
  <!-- mobile menu -->
  <div id="mobile_menu" class="mobile_menu">
    <!-- activator button -->
    <div class="mobile_menu_button">
      <img src="<%=currentDesign.getPath()%>/img/buttons/mobile_menu_button.png" alt="Menu" />
    </div>
    <div class="mobile_menu_contents">
      <ul>
        <li><a href="<%= FHUtilityFunctions.cleanLink(clusterHead.getPath(), resourceResolver) %>"><%= FHUtilityFunctions.displayTitle(clusterHead) %></a></li>

<%



while(menuItems.hasNext()) {
	Page child = menuItems.next();
%>
        <li><a href="<%= FHUtilityFunctions.cleanLink(child.getPath(), resourceResolver) %>"><%= FHUtilityFunctions.displayTitle(child) %></a></li>
<%	
}
%>

      </ul>
    </div>
  </div>
  <div class="clear"></div>