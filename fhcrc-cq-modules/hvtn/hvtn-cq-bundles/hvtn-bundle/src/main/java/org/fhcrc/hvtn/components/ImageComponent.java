package org.fhcrc.hvtn.components;

import javax.servlet.ServletRequest;

import java.io.IOException;
import java.io.Writer;

import com.day.cq.wcm.api.components.DropTarget;
import com.day.cq.commons.Doctype;
import com.day.cq.wcm.foundation.Image;
import com.day.cq.wcm.api.designer.*;

import org.apache.sling.api.resource.*;
import org.fhcrc.tools.FHUtilityFunctions;

public class ImageComponent {
	
	private final String WRAPPER_CLASS = "imageAndCaption";
	private final String BORDER_CLASS = "frame";
	private final String NO_BORDER_CLASS = "noBorder";
	private final String ROLLOVER_CLASS = "rolloverImage";
	private final String HOVER_ATTRIBUTE = "data-hover";
	
	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	private Resource resource;
	private Style currentStyle;
	private Design currentDesign;
	private Design resourceDesign;
	private ServletRequest request;
	private Writer writer;
	
	private Image image;
	private Image hiRes;
	private Image mouseover;
	
	private String hiResSrc;
	private String mouseoverSrc;
	private String linkTarget;
	private String wrapperClass;
	private String imageAlign;
	
	private boolean externalLink;
	private boolean bordered;
	private boolean captionOrCredit;
	
	public ImageComponent() {
		
	}

	/**
	 * @return the componentProperties
	 */
	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	/**
	 * @param componentProperties the componentProperties to set
	 */
	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
		this.externalLink = properties.get("externalLink",false);
		this.bordered = properties.get("hasBorder",false);
		this.imageAlign = properties.get("imageAlign","leftAlign");
		this.image = new Image(this.resource, "image");
		setImage(image);
		setLinkTarget(properties.get("linkTarget",""));
		this.hiRes = new Image(this.resource, "hi-res");
		setHiRes(hiRes);
		this.mouseover = new Image(this.resource, "mouseover");
		setMouseover(mouseover);
		setWrapperClass(WRAPPER_CLASS);
		if (!properties.get("credit","").isEmpty() || !properties.get("jcr:caption","").isEmpty()) {
			this.captionOrCredit = true;
		} else {
			this.captionOrCredit = false;
		}
	}

	/**
	 * @return the resourceResolver
	 */
	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	/**
	 * @param resourceResolver the resourceResolver to set
	 */
	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	/**
	 * @return the resource
	 */
	public Resource getResource() {
		return resource;
	}

	/**
	 * @param resource the resource to set
	 */
	public void setResource(Resource resource) {
		this.resource = resource;
	}

	/**
	 * @return the currentStyle
	 */
	public Style getCurrentStyle() {
		return currentStyle;
	}

	/**
	 * @param currentStyle the currentStyle to set
	 */
	public void setCurrentStyle(Style currentStyle) {
		this.currentStyle = currentStyle;
	}

	/**
	 * @return the currentDesign
	 */
	public Design getCurrentDesign() {
		return currentDesign;
	}

	/**
	 * @param currentDesign the currentDesign to set
	 */
	public void setCurrentDesign(Design currentDesign) {
		this.currentDesign = currentDesign;
	}

	/**
	 * @return the resourceDesign
	 */
	public Design getResourceDesign() {
		return resourceDesign;
	}

	/**
	 * @param resourceDesign the resourceDesign to set
	 */
	public void setResourceDesign(Design resourceDesign) {
		this.resourceDesign = resourceDesign;
	}

	/**
	 * @return the request
	 */
	public ServletRequest getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(ServletRequest request) {
		this.request = request;
	}

	/**
	 * @return the out
	 */
	public Writer getWriter() {
		return writer;
	}

	/**
	 * @param out the out to set
	 */
	public void setWriter(Writer writer) {
		this.writer = writer;
	}

	/**
	 * @return the image
	 */
	public Image getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	@SuppressWarnings("deprecation")
	public void setImage(Image image) {
		this.image = image;
		this.image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
		this.image.loadStyleData(currentStyle);
	    this.image.setSelector(".img"); // use image script
	    this.image.setDoctype(Doctype.fromRequest(request));
	    // add design information if not default (i.e. for reference paras)
	    if (!currentDesign.equals(resourceDesign)) {
	        this.image.setSuffix(currentDesign.getId());
	    }
	    if (bordered) {
	    	this.image.addCssClass(BORDER_CLASS);
	    }
	}

	/**
	 * @return the hiRes
	 */
	public Image getHiRes() {
		return hiRes;
	}

	/**
	 * @param hiRes the hiRes to set
	 */
	public void setHiRes(Image hiRes) {
		this.hiRes = hiRes;
		if (this.hiRes.hasContent()) {
			this.hiResSrc = this.hiRes.getSrc();
			/* This allows users to use either images from the DAM or upload directly into the component */
			if (this.hiResSrc.contains("hi-res/file")) {
				this.hiResSrc = this.hiResSrc.replaceAll("hi-res/file\\.", "hi-res.img.");
			}
		}
	}

	/**
	 * @return the mouseover
	 */
	public Image getMouseover() {
		return mouseover;
	}

	/**
	 * @param mouseover the mouseover to set
	 */
	public void setMouseover(Image mouseover) {
		this.mouseover = mouseover;
		if (this.mouseover.hasContent()) {
			this.mouseoverSrc = this.mouseover.getSrc();
			/* This allows users to use either images from the DAM or upload directly into the component */
			if (this.mouseoverSrc.contains("mouseover/file")) {
				this.mouseoverSrc = this.mouseoverSrc.replaceAll("mouseover/file\\.", "mouseover.img.");
			}
			/* Add the class and attribute to the ORIGINAL IMAGE. These are hooks for the JavaScript that 
			 * actually makes the mouseover effect happen */
			this.image.addAttribute(HOVER_ATTRIBUTE, this.mouseoverSrc);
			this.image.addCssClass(ROLLOVER_CLASS);
		}
	}

	/**
	 * @return the hiResSrc
	 */
	public String getHiResSrc() {
		return hiResSrc;
	}

	/**
	 * @param hiResSrc the hiResSrc to set
	 */
	public void setHiResSrc(String hiResSrc) {
		this.hiResSrc = hiResSrc;
	}

	/**
	 * @return the mouseoverSrc
	 */
	public String getMouseoverSrc() {
		return mouseoverSrc;
	}

	/**
	 * @param mouseoverSrc the mouseoverSrc to set
	 */
	public void setMouseoverSrc(String mouseoverSrc) {
		this.mouseoverSrc = mouseoverSrc;
	}

	/**
	 * @return the linkTarget
	 */
	public String getLinkTarget() {
		return linkTarget;
	}

	/**
	 * @param linkTarget the linkTarget to set
	 */
	public void setLinkTarget(String linkTarget) {
		if(!linkTarget.equals("")){
	        this.linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
	    } else {
	    	this.linkTarget = "";
	    }
	}

	/**
	 * @return the isExternalLink
	 */
	public boolean isExternalLink() {
		return externalLink;
	}

	/**
	 * @param isExternalLink the isExternalLink to set
	 */
	public void setExternalLink(boolean isExternalLink) {
		this.externalLink = isExternalLink;
	}
	
	/**
	 * @return the hasBorder
	 */
	public boolean isBordered() {
		return bordered;
	}

	/**
	 * @param hasBorder the hasBorder to set
	 */
	public void setBordered(boolean hasBorder) {
		this.bordered = hasBorder;
	}

	public String getImageAlign() {
		return imageAlign;
	}

	public void setImageAlign(String imageAlign) {
		this.imageAlign = imageAlign;
	}

	/**
	 * @return the wrapperClass
	 */
	public String getWrapperClass() {
		return wrapperClass;
	}

	/**
	 * @param wrapperClass the wrapperClass to set
	 */
	public void setWrapperClass(String wrapperClass) {
		this.wrapperClass = wrapperClass;
		this.wrapperClass = this.wrapperClass.concat(" ").concat(this.imageAlign);
		if (!this.isBordered()) {
			this.wrapperClass = this.wrapperClass.concat(" ").concat(NO_BORDER_CLASS);
		}
	}

	/**
	 * @return the captionOrCredit
	 */
	public boolean isCaptionOrCredit() {
		return captionOrCredit;
	}

	/**
	 * @param captionOrCredit the captionOrCredit to set
	 */
	public void setCaptionOrCredit(boolean captionOrCredit) {
		this.captionOrCredit = captionOrCredit;
	}

	public void drawImage() {
		try {
			image.draw(writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}