<%--

  Storify component.
  
  This component displays an embedded timeline from Storify. Currently, it accepts only 2 variables:
  
  URL - The URL to the landing page of the Storify story
  Fallback Text - The text to be used if the Storify JavaScript cannot load

--%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page session="false" %>
<%

String url = properties.get("url","");
String fallbackText = properties.get("fallback","View this story on Storify");
String embedURL = "";
String jsURL = "";
if (!url.trim().equals("")) {
	
	url = url.replaceAll("http(s)?://", "");
	embedURL = url.concat("/embed");
	jsURL = url.concat(".js");

%>
<div class="storify">
	<iframe src="//<%= embedURL %>" width="100%" height=750 frameborder=no allowTransparency=true></iframe>
<!-- 	<script src="//<%= jsURL %>" type="text/javascript" language="javascript"></script>   -->
	<noscript>[<a href="//<%= url %>" target="_blank"><%= fallbackText %></a>]</noscript>
</div>

<script type="text/javascript">

	$(window).bind("load", function(){
		$.getScript("//<%= jsURL %>");
	});

</script>

<%

} else {

%>

<cq:include script="empty.jsp" />

<%

}

%>