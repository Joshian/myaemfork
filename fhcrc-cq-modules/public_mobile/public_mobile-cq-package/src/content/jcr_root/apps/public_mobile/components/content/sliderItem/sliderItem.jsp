<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
Image image = new Image(resource, "image");

//drop target css class = dd prefix + name of the drop target in the edit config
image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
//remove title attribute unless there is something explicitly in the dialog
if(properties.get("image/jcr:title","").trim().equals("")) {
    image.setTitle(" ");
}

image.loadStyleData(currentStyle);
image.setSelector(".img"); // use image script
image.setDoctype(Doctype.fromRequest(request));
// add design information if not default (i.e. for reference paras)
if (!currentDesign.equals(resourceDesign)) {
    image.setSuffix(currentDesign.getId());
}

/* First put the image (or the empty image placeholder) in the document */
if(image.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT){ 
%>

<div class="photoContainer <%= image.hasContent() ? "" : "cq-edit-only" %>">
<%
	image.draw(out);
%>

</div>

<% } %>

<div class="textContainer">

    <div class="theText">
    
        <cq:text property="shortText" tagClass="compressedText" />
        
        <cq:text property="longText" tagClass="expandedText" />
    
    </div>
    
    <div class="expandButton">+</div>

</div>