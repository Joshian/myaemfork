<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.security.User,
                 com.day.cq.security.UserManager,
                 com.day.cq.security.UserManagerFactory,
                 org.apache.sling.jcr.api.SlingRepository,
                 com.day.cq.wcm.api.WCMMode,
                 com.day.cq.wcm.api.components.DropTarget,
                 com.day.cq.security.Group,
                 com.day.cq.i18n.I18n,
                 java.util.ResourceBundle" %>
<%
//-------------------------------------------------------
// First capture the current WCM Mode so we can set it
// back at the end.
//-------------------------------------------------------
WCMMode originalMode = WCMMode.fromRequest(request); 
Session adminSession = null;
try {
	//ResourceBundle bundle = slingRequest.getResourceBundle(null);
    //log.info("test I18N message : " + I18n.get(bundle, "testText"));
    //log.info("test I18N message : " + I18n.get(bundle, "localWins"));
	
    // only want to protect when we are in author, not publish:
    String runModes = System.getProperty("sling.run.modes");
    log.info("RUNMODES = " + runModes);
    if (runModes.toLowerCase().contains("author")){
    	
	    boolean isProducer = false;
		User currentUser = resourceResolver.adaptTo(User.class); 
		SlingRepository repo = sling.getService(SlingRepository.class);
		adminSession = repo.loginAdministrative(null);
		UserManager userManager = sling.getService(UserManagerFactory.class).createUserManager(adminSession);
        if( userManager.hasAuthorizable("public-producer") ){
  		  Group group = (Group) userManager.get("public-producer"); 
		  isProducer = group.isMember(currentUser);
		} 
		//-------------------------------------------------------
		// If it is not a producer, disable editing for the
		// rest of this request.  Keep in mind, each component
		// is its own request so what we are doing only affects
		// this component.
		//-------------------------------------------------------
		if (!isProducer) {
			WCMMode.DISABLED.toRequest(request);
		}
		
    }
%>
    
    <cq:include path="producerProtectedPar" resourceType="foundation/components/parsys"/>
    
<%
} catch (Exception ex) {
	log.error("Exception", ex);
}
finally {
    //-------------------------------------------------------
    // Set the WCM Mode back to its original mode.
    //-------------------------------------------------------
	originalMode.toRequest(request); 
	if (adminSession != null) {
		adminSession.logout();
	}
}
 %>