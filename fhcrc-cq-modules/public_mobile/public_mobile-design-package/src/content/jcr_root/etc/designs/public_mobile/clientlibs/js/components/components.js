/* Here is the master component app. It should contain submodules for each 
 * component, then return each of those submodules as object properties of the
 * main module.
 */

var componentApp = (function(componentApp) {
	
	componentApp.utilityFunctions = (function(utilityFunctions) {
		
		utilityFunctions.lookUpForClassName = function lookUpForClassName(el, className) {
			
			if(el.tagName.toLowerCase() === "html") {
				
				return false;
				
			} else if(el.classList.contains(className)) {
				
				return el;
				
			} else {
				
				return lookUpForClassName(el.parentElement, className);
				
			}
			
		};
		
		return utilityFunctions;
	
	})(componentApp.utilityFunctions || {});
	
	return componentApp;
	
})(componentApp || {});