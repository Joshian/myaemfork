package org.fhcrc.importer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.polling.importer.Importer;
import com.day.cq.tagging.Tag;
import com.sun.syndication.feed.synd.SyndCategory;

public abstract class FHCRCImporter implements Importer {
	
    protected final Logger log = LoggerFactory.getLogger(getClass());

    /**
	 * Given a path, returns the list of tags under it.  Map is indexed by the tag title.
	 * @param path path to search under.
	 * @return 
	 * @throws LoginException
	 */
	protected Map<String, String> getChildTags(ResourceResolverFactory resolverFactory, String path) throws LoginException {
		ResourceResolver resolver = null;
		try {
		    resolver = resolverFactory.getAdministrativeResourceResolver(null);
			Resource tagResource = resolver.getResource(path);
			Map<String, String> childTags = new HashMap<String, String>();
			Iterator<Resource> children = tagResource.listChildren();
			while (children.hasNext()) {
				Tag aTag = children.next().adaptTo(Tag.class);
				if (aTag != null) {
					childTags.put(aTag.getTitle(), aTag.getTagID());
				}
			}
			return childTags;
		} finally {
			if (resolver != null) {
				resolver.close();
			}
		}
	}
	@SuppressWarnings("unchecked")
	protected Node updateTags(Node pageNode, List categories, Map<String, String> availableTags) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {
		  //------------------------------------------
		  // Build a list of TagIDs that should be 
		  // associated with this page.
		  //------------------------------------------
		  log.debug("Number of categories = " + categories.size());
		  List<String> tags = new ArrayList<String>();
		  for (int j = 0; j < categories.size(); j++) {
			  SyndCategory category = (SyndCategory)categories.get(j);
			  log.debug("category = " + category.getName());
			  String divisionTagId = availableTags.get(category.getName());
			  if (divisionTagId != null) {
				  tags.add(divisionTagId);
			  } else {
				  log.debug("Tag not found");
			  }
		  }
		  String[] tagArray = new String[tags.size()];
		  pageNode.setProperty(Constants.TAGS_PROPERTY, tags.toArray(tagArray));
		  return pageNode;
	}
	
	/**
	 * Create a node structure under the page which has a parsys and a text component named "text".
	 * The text component is of type public/components/content/text
	 * @param pageNode Page Node that is either being created or updated
	 * @param text Text value to be save.
	 * @return Updated, but not yet saved page node.
	 * @throws ItemExistsException
	 * @throws PathNotFoundException
	 * @throws VersionException
	 * @throws ConstraintViolationException
	 * @throws LockException
	 * @throws RepositoryException
	 */
	protected Node setTextComponent(Node pageNode, String text, String paragraphNodeName) throws ItemExistsException, PathNotFoundException, VersionException, ConstraintViolationException, LockException, RepositoryException {
		if (text != null && !text.trim().isEmpty()) {
			Node parNode = null;
			Node textNode = null;
			if (!pageNode.hasNode(paragraphNodeName)) {
				parNode = pageNode.addNode(paragraphNodeName);
				parNode.setProperty("sling:resourceType", "foundation/components/parsys");		
				textNode = parNode.addNode("text");
				textNode.setProperty("sling:resourceType", "public/components/content/text");
			} else {
				parNode = pageNode.getNode(paragraphNodeName);
				if (!parNode.hasNode("text")) {
					textNode = parNode.addNode("text");
					textNode.setProperty("sling:resourceType", "public/components/content/text");
				} else {
					textNode = parNode.getNode("text");
				}
			}
			textNode.setProperty("text", text);
		}
		return pageNode;
	}
	
	/**
	 * Create a node structure under the page which has a textarea component named "byline".
	 * The textarea component is of type public/components/content/textarea
	 * @param pageNode Page Node that is either being created or updated
	 * @param text Text value to be save (author)
	 * @return Updated, but not yet saved page node.
	 * @throws ItemExistsException
	 * @throws PathNotFoundException
	 * @throws VersionException
	 * @throws ConstraintViolationException
	 * @throws LockException
	 * @throws RepositoryException
	 */
	protected Node setBylineComponent(Node pageNode, String text, String bylineNodeName) throws ItemExistsException, PathNotFoundException, VersionException, ConstraintViolationException, LockException, RepositoryException {
		if (text != null && !text.trim().isEmpty()) {
			Node parNode = null;
			if (!pageNode.hasNode(bylineNodeName)) {
				parNode = pageNode.addNode(bylineNodeName);
				parNode.setProperty("sling:resourceType", "public/components/content/textarea");		
			} else {
				parNode = pageNode.getNode(bylineNodeName);
			}
			parNode.setProperty("text", text);
		}
		return pageNode;
	}
	
	/**
	 * Create a node structure under the page which has a parsys and a textimage component named "textimage".
	 * The text component is of type public/components/content/textimage
	 * @param pageNode Page Node that is either being created or updated
	 * @param text Text value to be save.
	 * @return Updated, but not yet saved page node.
	 * @throws ItemExistsException
	 * @throws PathNotFoundException
	 * @throws VersionException
	 * @throws ConstraintViolationException
	 * @throws LockException
	 * @throws RepositoryException
	 */
	protected Node setTextImageComponent(Node pageNode, String text, String paragraphNodeName) throws ItemExistsException, PathNotFoundException, VersionException, ConstraintViolationException, LockException, RepositoryException {
		if (text != null && !text.trim().isEmpty()) {
			Node parNode = null;
			Node textNode = null;
			if (!pageNode.hasNode(paragraphNodeName)) {
				parNode = pageNode.addNode(paragraphNodeName);
				parNode.setProperty("sling:resourceType", "foundation/components/parsys");		
				textNode = parNode.addNode("textimage");
				textNode.setProperty("sling:resourceType", "public/components/content/textimage");
				textNode.setProperty("imageAlign", "right");
			} else {
				parNode = pageNode.getNode(paragraphNodeName);
				if (!parNode.hasNode("textimage")) {
					textNode = parNode.addNode("textimage");
					textNode.setProperty("sling:resourceType", "public/components/content/textimage");
					textNode.setProperty("imageAlign", "right");
				} else {
					textNode = parNode.getNode("textimage");
				}
			}
			textNode.setProperty("text", text);			
		}
		return pageNode;
	}	

	/**
	 * Create a node under the page called "pubDate" of type public/components/content/date and whose "date" property is the date of publication.
	 * @param pageNode Page Node that is either being created or updated
	 * @param pubdate The date to be saved as a property of the pubDate node
	 * @return Updated, but not yet saved page node.
	 * @throws ItemExistsException
	 * @throws PathNotFoundException
	 * @throws VersionException
	 * @throws ConstraintViolationException
	 * @throws LockException
	 * @throws RepositoryException
	 */
	protected Node setPubdateComponent(Node pageNode, Date pubdate) throws ItemExistsException, PathNotFoundException, VersionException, ConstraintViolationException, LockException, RepositoryException {
		//SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT); // sld 12/7/2011
		Calendar cal = Calendar.getInstance(); // added, sld 12/7/2011
		if (pubdate != null ) {
			Node parNode = null;
			if (!pageNode.hasNode("pubDate")) {
				parNode = pageNode.addNode("pubDate");
				parNode.setProperty("sling:resourceType", "public/components/content/date");		
			}
			cal.setTime(pubdate); // sld 12/7/2011
			parNode.setProperty("date", cal); // sld 12/7/2011			
			//parNode.setProperty("date", dateFormat.format(pubdate)); // sld 12/7/2011			
		}
		return pageNode;
	}

}
