<%@include file="/apps/stripe/global.jsp" %>

<div class="container" id="utility_nav_container">
    <cq:include path="utilitynav" resourceType="stripe/components/modular/utilitynav"/>  
</div>
<div class="navbar navbar-inverse navbar-stripe-top">
  <div >
      <div class="logobar">
        <div class="container">
            <div class="row">
	            <span class="span12" >
	               <cq:include path="fhcrclogo" resourceType="stripe/components/modular/logo"/>
	            </span>
            </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="header-greybar">
        <div class="header-greybar-inner">
            <div class="row">
	            <span class="span12" >
	                <cq:include path="divisiontitle" resourceType="stripe/components/modular/divisiontitle" />
	            </span>
            </div>
        </div>
      </div>
      <div class="lab-title">
        <div class="container">
            <div class="row">
                <span class="span12 headertitle">
                   <cq:include path="labtitle" resourceType="stripe/components/modular/title" />   
                </span>
            </div>
        </div>
      </div>
    </div>
</div>
<div class="clearfix">
</div>
<cq:include script="../globalPage/header_style.jsp"/>