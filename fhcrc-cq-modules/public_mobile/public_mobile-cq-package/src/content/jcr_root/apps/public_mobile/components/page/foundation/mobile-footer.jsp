<%@include file="/libs/foundation/global.jsp" %>
   <div class="fh_footer">  
    <p>
      <span class="desktopsite"><a href="http://www.fhcrc.org">Desktop Site</a></span> |
      <span class="sitemap"><a href="http://www.fhcrc.org">Site map</a></span>
    </p>
    <span class="copyright">&copy; Fred Hutchinson Cancer Research Center</span>
   </div><!-- /fh_footer -->
