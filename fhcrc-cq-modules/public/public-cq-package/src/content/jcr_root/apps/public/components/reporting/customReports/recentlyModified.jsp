<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text, 
                 java.util.Iterator, 
                 org.apache.commons.lang.StringEscapeUtils, 
                 java.util.HashMap,
                 com.day.cq.search.QueryBuilder, 
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.result.Hit,
                 java.util.List,
                 java.util.ArrayList,
                 java.util.Date,
                 java.util.Calendar,
                 java.text.SimpleDateFormat,
                 com.day.cq.replication.ReplicationStatus" %>
<%
Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);
HashMap<String, String> map = new HashMap<String, String>();
String timeInt = slingRequest.getParameter("timeInt");
String timeIncrement = slingRequest.getParameter("timeIncrement");

map.put("type","cq:Page");
map.put("path","/content/public/en");
map.put("relativedaterange.property","jcr:content/cq:lastModified");
if(timeInt != null && timeIncrement != null && !timeInt.trim().equals("")) {
    map.put("relativedaterange.lowerBound","-" + timeInt + timeIncrement);
} else {
	map.put("relativedaterange.lowerBound","-1w");
}
map.put("p.limit","1000");

List<Hit> hitList = builder.createQuery(PredicateGroup.create(map), session).getResult().getHits();
ArrayList<Page> pageList = new ArrayList<Page>();

if(hitList.size() == 0){
%>
    <cq:include script="empty.jsp"/>
<%
} else {
	for(int i = 0; i < hitList.size(); i++){
	      Page testPage = hitList.get(i).getResource().adaptTo(Page.class);
	      pageList.add(testPage);
	}
	Iterator<Page> pageIterator = pageList.iterator();
    if(!pageIterator.hasNext()){
%>  
    	    <cq:include script="empty.jsp"/>
<%
    } else {
%>
<!-- Table of results -->
<table id="reportResults" class="sortable">
    <tr>
        <th>Page title</th>
        <th>Last Modified Date</th>
        <th>Last Modified by</th>
    </tr>


<%
    	while(pageIterator.hasNext()) {
    		Page nextPage = pageIterator.next();
    		Calendar lastModified = nextPage.getLastModified();
    		String lastModifiedBy = nextPage.getLastModifiedBy();
    		SimpleDateFormat f = new SimpleDateFormat("MMM dd, k:mm");
    		String lastModifiedDate = "Cannot determine";

    		if (lastModified != null) {
    			lastModifiedDate = f.format(lastModified.getTime()).toString();
    		}
    		
    		if (lastModifiedBy == null || lastModifiedBy.trim().equals("")) {
    		    lastModifiedBy = "Cannot determine";
    		}
    		
    		
%>
    <tr>
        <td><a href="<%= nextPage.getPath() %>.html" target="_blank"><%= nextPage.getTitle() != null ? nextPage.getTitle() : nextPage.getName() %></a></td>
        <td><%= lastModifiedDate %></td>
        <td><%= lastModifiedBy %></td>
    </tr>
<%
    	}
    }
}
%>
</table>
<script type="text/javascript">
var numResults = document.getElementById('reportResults').rows.length;
numResults -= 1;
var theTable = document.getElementById("reportResults");
var returnedResults = document.createElement("span");
returnedResults.setAttribute("id","numResults");
returnedResults.appendChild(document.createTextNode("Returned " + numResults + " results"));
theTable.parentNode.insertBefore(returnedResults,theTable);
</script>