<%@include file="/libs/foundation/global.jsp"%>
<% 
String eventName = currentPage.getPath();
%>

<div class="diseaserelated">
    <a href="http://www.nwsttr.org" target="_blank" onclick="_gaq.push(['_trackEvent','Disease Events','Disease STTR widget','<%= eventName %>']);"><img src="/etc/designs/public/img/logos/sttr_logo.gif" border="0" alt="Solid Tumor Translational Research" /></a>
    <p>Fred Hutch, UW Medicine and SCCA are working to bridge laboratory sciences and patient care.</p>
    <a href="http://www.nwsttr.org" target="_blank"><img class="rolloverImage" src="/etc/designs/public/img/buttons/button_learn_more_stat.gif" data-hover="/etc/designs/public/img/buttons/button_learn_more_over.gif" alt="Learn more" /></a>
</div>
