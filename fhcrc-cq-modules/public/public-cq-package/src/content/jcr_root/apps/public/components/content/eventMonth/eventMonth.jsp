<%@include file="/libs/foundation/global.jsp"%><%@ page import="java.util.Calendar" %>
<%
Calendar theTime = Calendar.getInstance();
int theMonth = theTime.get(Calendar.MONTH);
int theYear = theTime.get(Calendar.YEAR);
int componentMonth = Integer.parseInt(properties.get("month", "0"));
/* If the month of the component is in the past, increment the year by one so we aren't displaying events that occurred in the past */
if(componentMonth < theMonth){
	theYear++;
}
/* Get the correct month to display in the header */
String componentMonthString = "";
switch(componentMonth){
case 0: componentMonthString = "January"; break;
case 1: componentMonthString = "February"; break;
case 2: componentMonthString = "March"; break;
case 3: componentMonthString = "April"; break;
case 4: componentMonthString = "May"; break;
case 5: componentMonthString = "June"; break;
case 6: componentMonthString = "July"; break;
case 7: componentMonthString = "August"; break;
case 8: componentMonthString = "September"; break;
case 9: componentMonthString = "October"; break;
case 10: componentMonthString = "November"; break;
case 11: componentMonthString = "December"; break;
default: componentMonthString = "Invalid Month"; break;
}
/* Assuming one component per month, thisPar creates a unique parsys path for each month */
String thisPar = "par_" + componentMonthString;
%>
<h3><%= componentMonthString + " " + theYear %></h3>
<cq:include path="<%= thisPar %>" resourceType="foundation/components/parsys"/>