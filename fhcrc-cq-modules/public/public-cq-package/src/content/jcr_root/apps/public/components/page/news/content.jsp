<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text, java.util.Iterator, com.day.cq.wcm.api.WCMMode, org.fhcrc.publicsite.constants.Constants" %>


<%-- this is a page switcher.

    all news pages come through here. depending upon which news source you have,
    shunt to the appropriate page layout.

    shunt to the 

    (1) Center News - "fancy new newsroll" format, with rich right rail

    leave all others with the regular format 

    --%>


<% 

    String path = currentPage.getPath();

if(

   path.contains(Constants.PATH_TO_CENTERNEWS_FOLDER)

  ){

%>

<cq:include script="content-newsplus.jsp"/>

<%} else {%>

<cq:include script="content-classic.jsp"/>


<% } %>