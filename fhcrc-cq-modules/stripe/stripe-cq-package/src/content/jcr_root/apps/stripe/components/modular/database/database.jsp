<%@include file="/libs/foundation/global.jsp"%>
<%@page session="false"%>
<%@ page import="com.day.commons.datasource.poolservice.DataSourcePool" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet"%>
<html> 
<body> 
<%
DataSourcePool dspService = sling.getService(DataSourcePool.class);   
try 
{
    DataSource ds = (DataSource) dspService.getDataSource("mysqldb");
    if(ds != null) 
    {          

%>
    <p>Obtained the datasource!</p>
<%
    int id;
    String name = "";
    final Connection connection = ds.getConnection();
    final Statement statement = connection.createStatement();
    final ResultSet resultSet = statement.executeQuery("select * from test.my_table");
    int r=0;           
    while(resultSet.next())
    {
        r=r+1;
        id  = resultSet.getInt("id");
        name = resultSet.getString("name");
%>
        <div style="float: left; width:50px;"><strong></>ID:</strong> <%=id%></div> &nbsp; &nbsp;<div style="float: left;"><strong>NAME:</strong> <%=name%></div> <br>
<%        
    }            
    resultSet.close();    
    connection.close();
%>
    <p>Number of results: <%=r%></p>
<%       
    }
}catch (Exception e) 
{ 
%>
    <p>error! <%=e.getMessage()%></p>
<%     
}
%>
</body>
</html>    
