//**********************TODO*************************
// 1.  Only update if the publishedDate is newer than previous publishedDate.
// 2.  Create nodes(components) to hold the text.
//***************************************************
package org.fhcrc.importer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;


import com.day.cq.polling.importer.ImportException;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.Template;
import com.day.cq.wcm.api.WCMException;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.io.*;


import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.OsgiUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component(metatype=true, label="FHCRC RSS Feed - Science Spotlight Importer", immediate=true, description="Import Science Spotlight RSS Feed")
@Service(value=com.day.cq.polling.importer.Importer.class)
@Properties({
	@Property(name="importer.scheme", value="SpotlightImporter", propertyPrivate=true),
    @Property(name="service.description", value="Science Spotlight Importer"),
    @Property(name="service.vendor", value="FHCRC")
})
public class SpotlightImporter extends FHCRCImporter {
    private final Logger log = LoggerFactory.getLogger(getClass());
    
    @Reference
    private ResourceResolverFactory resolverFactory; 

    @Reference
    private Replicator replicator; 

    @Property(label="DisableOnPublishMode", boolValue=true, description="Disable on publishers")
    private static final String DISABLE_ON_PUBLISH_MODE = "disable.publish";
    protected boolean disableOnPublishMode;
    
    @Property(label="AutoActivateOnCreate", boolValue=false, description="Auto Activate Newly Created Pages")
    private static final String AUTO_ACTIVATE_ON_CREATE = "autoactivate.create";
    protected boolean autoActivateOnCreate;

    @Property(label="AutoActivateOnUpdate", boolValue=false, description="Auto Activate Updated Pages")
    private static final String AUTO_ACTIVATE_ON_UPDATE = "autoactivate.update";
    protected boolean autoActivateOnUpdate;
    
    @SuppressWarnings("unchecked")
	public void importData(String scheme, String dataSource, Resource target)
			throws ImportException {
    	
    	String runModes = System.getProperty("sling.run.modes");
    	if (runModes.toLowerCase().contains("author") ||
    	    (runModes.toLowerCase().contains("publish") && !disableOnPublishMode)) {
	    	log.info("RUNMODES = " + runModes);
			log.info("target = " + target);
			log.info("dataSource = " + dataSource);
	  	  
			//--------------------------------------------------------------
			// Use the Rome APIs to read in and parse the RSS Feed.
			// Once we have the feed we can create/update/delete CQ Pages.
	  	  	//--------------------------------------------------------------
			ResourceResolver resolver = null;
			try {
			    resolver = resolverFactory.getAdministrativeResourceResolver(null);			
	            PageManager pageManager = resolver.adaptTo(PageManager.class);            
				URL url = new URL(dataSource.toString());
				
			      XmlReader reader = null;
			      reader = new XmlReader(url);
			      SyndFeed feed = new SyndFeedInput().build(reader);
			      log.info("Feed Title: "+ feed.getTitle());
			      List entries = feed.getEntries();
		    	  Map<String, String> availableDivisionTags = getChildTags(resolverFactory, Constants.DIVISION_TAG_PATH);
		    	  log.debug("availableDivisionTags = " + availableDivisionTags);
			      if (entries != null && entries.size()>0) {
			    	  Page targetPage = target.adaptTo(Page.class);
			    	  Template targetTemplate = targetPage.getTemplate();
			    	  Iterator<Page> children = targetPage.listChildren();
			    	  Map<String, Page> importedPageMap = new HashMap<String, Page>();
			    	  while (children.hasNext()) {
			    		  Page child = children.next();
			    		  //----------------------------------------------------------
			    		  // Only keep track of pages that were created by the
			    		  // importer.  This will keep us from deleting any pages
			    		  // created by a user.
			    		  //----------------------------------------------------------
			    		  String id = child.getProperties().get(Constants.IDENTIFIER_PROPERTY, "");
			    		  if (!id.isEmpty()) {
			    			  importedPageMap.put(id, child);
			    		  }
			    	  }
				      for (int i = 0; i < entries.size(); i++) {
				    	  SyndEntry entry = (SyndEntry)entries.get(i);
				    	  log.debug("title = " + entry.getTitle());
				    	  log.debug("description = " + entry.getDescription().getValue());
				    	  log.debug("link = " + entry.getLink());
				    	  log.debug("publishedDate = " + entry.getPublishedDate());
				    	  log.debug("updatedDate = " + entry.getUpdatedDate());
				    	  String content="";
				    	  if(entry.getContents().size()>0){
				    		  SyndContent content_synd = (SyndContent)entry.getContents().get(0);
				    		  content = content_synd.getValue();
				    	  }
				    	  log.debug("author = " + entry.getAuthor());
				    	  log.debug("content = " + content);
				    	  log.debug("to string = " + entry.toString());
				    	  
				    	  if (entry.getLink()!=null && !entry.getLink().isEmpty()) {
					    	  //--------------------------------------------------------------
					    	  // Check to see if the item already exists as a page. This is
					    	  // done by using the link as the id.
					    	  // No - Create a page.
					    	  // Yes - Update it if pubDate is newer.
					          //--------------------------------------------------------------
					    	  Page existingPage = importedPageMap.get(entry.getLink());
			    			  SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
					    	  if (existingPage == null) {
					    		  //----------------------------------------------------------
					    		  // Create the new page.
					    		  //----------------------------------------------------------
					    		  log.info("Creating a new page with id:  " + entry.getLink());
					    		  Page createdPage = pageManager.create(targetPage.getPath(), null, targetTemplate.getPath(), entry.getTitle());
					    		  Node createdPageNode=createdPage.getContentResource().adaptTo(Node.class);
					    		  //------------------------------------------
					    		  // General Properties
					    		  //------------------------------------------
					    		  createdPageNode.setProperty(Constants.IDENTIFIER_PROPERTY,entry.getLink());
					    		  //createdPageNode.setProperty(Constants.REDIRECT_PROPERTY, entry.getLink());
					    		  createdPageNode.setProperty(Constants.DESCRIPTION_PROPERTY, entry.getDescription().getValue());
					    		  createdPageNode.setProperty(Constants.OMIT_SECTION_NAV_PROPERTY, true);
					    		  //if (entry.getDescription() != null) {
					    			//  createdPageNode = setTextComponent(createdPageNode, entry.getDescription().getValue(), "articletext");
					    		  //}
					    		  if (!content.equals("")){
					    			  createdPageNode = setTextImageComponent(createdPageNode, content, "articletext");
					    		  }
					    		  if (!entry.getAuthor().equals("")){
					    			  createdPageNode = setBylineComponent(createdPageNode, entry.getAuthor(), "byline");
					    		  }
					    		  
					    		  //------------------------------------------
					    		  // Published Date
					    		  //------------------------------------------
					    		  if (entry.getPublishedDate() != null) {
					    			  createdPageNode.setProperty(Constants.PUBLISHED_DATE_PROPERTY, dateFormat.format(entry.getPublishedDate()));
					    			  createdPageNode = setPubdateComponent(createdPageNode, entry.getPublishedDate());
					    		  } else {
					    			  String currentDate = dateFormat.format(new Date());
					    			  log.debug("Formatted Date = " + currentDate);
					    			  createdPageNode.setProperty(Constants.PUBLISHED_DATE_PROPERTY, currentDate);
					    		  }
					    		  createdPageNode = updateTags(createdPageNode, entry.getCategories(), availableDivisionTags);
					    		  createdPageNode.getSession().save(); 		
					    		  if (autoActivateOnCreate) {
						    		  log.info("Activating page now.");
						    		  replicator.replicate(createdPageNode.getSession(), ReplicationActionType.ACTIVATE, createdPage.getPath());
					    		  } else {
					    			  log.info("No Auto Activation Set.  Page not activated");
					    		  }
					    	  } else {
					    		  //----------------------------------------------------------
					    		  // Update the existing page.  When complete, remove it
					    		  // from the page map.  When we are done in this loop,
					    		  // the pages left in the map are the ones that should be
					    		  // deleted.
					    		  //----------------------------------------------------------
					    		  Node existingPageNode=existingPage.getContentResource().adaptTo(Node.class);
					    		  String existingPagePubDateAsString = existingPage.getProperties().get(Constants.PUBLISHED_DATE_PROPERTY,"");
					    		  Date existingPagePubDate = dateFormat.parse(existingPagePubDateAsString);
					    		  if (entry.getPublishedDate() == null || existingPagePubDate.before(entry.getPublishedDate())) {
						    		  log.info("Updating an existing page:  " + existingPage.getPath());
					    			  if (entry.getPublishedDate() != null) {
						    			  existingPageNode.setProperty(Constants.PUBLISHED_DATE_PROPERTY, dateFormat.format(entry.getPublishedDate()));
						    		  }
					    			  existingPageNode.setProperty(Constants.TITLE_PROPERTY, entry.getTitle());
						    		  existingPageNode.setProperty(Constants.DESCRIPTION_PROPERTY, entry.getDescription().getValue());
						    		  //if (entry.getDescription() != null) {
						    		  //  existingPageNode = setTextComponent(existingPageNode, entry.getDescription().getValue(), "articletext");
						    		  //}
						    		  if (!content.equals("")){
						    			  existingPageNode = setTextImageComponent(existingPageNode, content, "articletext");
						    		  }
						    		  if (!entry.getAuthor().equals("")){
						    			  existingPageNode = setBylineComponent(existingPageNode, entry.getAuthor(), "byline");
						    		  }
						    		  //existingPageNode = updateTags(existingPageNode, entry.getCategories(), availableDivisionTags);
						    		  existingPageNode.getSession().save(); 
						    		  if (autoActivateOnUpdate) {
							    		  log.info("Activating page now.");
							    		  replicator.replicate(existingPageNode.getSession(), ReplicationActionType.ACTIVATE, existingPageNode.getPath());
						    		  } else {
						    			  log.info("No Auto Activation Set.  Page not activated");
						    		  }
					    		  } else {
					    			  log.debug("Skipping page because it has not changed : " + existingPage.getPath());
					    		  }
					    		  importedPageMap.remove(entry.getLink());
					    	  }
					    	  log.debug("------------------------------------------------");
				    	  }
				      }
			    	  //--------------------------------------------------------------
			    	  // Now make a decision on any items that may have been deleted.
				      // Look through the entire set of pages.  If there are some
				      // that exist which are not in this current feed, delete them.
				      //--------------------------------------------------------------
				      Collection<Page> remainingPages = importedPageMap.values();
				      for (Page aPage : remainingPages) {
				    	  log.info("Deleting existing page because it was removed from the feed:  " + aPage.getPath());
				    	  pageManager.delete(aPage, false);
				      }
			      }
			} catch (MalformedURLException e) {
				log.error("Error caught", e);
			} catch (IOException e) {
				log.error("Error caught", e);
			} catch (IllegalArgumentException e) {
				log.error("Error caught", e);
			} catch (FeedException e) {
				log.error("Error caught", e);
			} catch (LoginException e) {
				log.error("Error caught", e);
			} catch (WCMException e) {
				log.error("Error caught", e);
			} catch (ValueFormatException e) {
				log.error("Error caught", e);
			} catch (VersionException e) {
				log.error("Error caught", e);
			} catch (LockException e) {
				log.error("Error caught", e);
			} catch (ConstraintViolationException e) {
				log.error("Error caught", e);
			} catch (RepositoryException e) {
				log.error("Error caught", e);
			} catch (ParseException e) {
				log.error("Error caught", e);
			} catch (ReplicationException e) {
				log.error("Error caught", e);
			} finally {
				if (resolver != null) {
					resolver.close();
				}
			}
    	} else {
    		log.info("Ignoring import for this runMode.");
    	}
	}
   
	@SuppressWarnings("unchecked")
    protected void activate(ComponentContext context ) throws Exception {
		//--------------------------------------------------------------
        // Retrieve configuration values from Felix
		//--------------------------------------------------------------
    	log.info("Getting OSGI Configuration");
        Dictionary<String, ?> props = context.getProperties();
		disableOnPublishMode = OsgiUtil.toBoolean(props.get(DISABLE_ON_PUBLISH_MODE), true);
		autoActivateOnCreate = OsgiUtil.toBoolean(props.get(AUTO_ACTIVATE_ON_CREATE), false);
		autoActivateOnUpdate = OsgiUtil.toBoolean(props.get(AUTO_ACTIVATE_ON_UPDATE), false);
    }

}
