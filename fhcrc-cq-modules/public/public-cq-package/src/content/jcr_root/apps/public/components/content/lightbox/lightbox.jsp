<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.api.WCMMode,
                 org.apache.commons.codec.digest.DigestUtils,
                 org.fhcrc.tools.FHUtilityFunctions" %><%

   boolean active = properties.get("activate",Boolean.FALSE); // user can disable the lightbox by unchecking a checkbox
   String width = properties.get("width","500");
   String height = properties.get("height","500");
   String scroll = properties.get("scroll","auto");
   String duration = properties.get("duration","session"); // each, session, user. default to session

   String randomID = FHUtilityFunctions.getRandomID();

   // type of lightbox
   String type = properties.get("linkType",""); // should never be blank because the component defaults to one choice right now.
   
   // iframe types have a url
   String url = properties.get("url","");
   
   /* normalize the prefix on the URL to avoid problems on Firefox */
   if (url.matches("http:.*") && request.getScheme().toLowerCase().equals("https")) {
	   url = url.replaceAll("http:", "https:");
   }
   
   // calculate a hash for this lightbox so when a new lightbox
   // is in play the cookie for the "old lightbox" doesn't prevent its display (under some durations).
   // this value is used as the name of the cookie, that's all.
   // when you add new types, e.g., inline, be sure to stuff any new md5-able 
   // into the parens below so when those things change, the cookie name changes.
   String md5 = DigestUtils.md5Hex( url + duration + type + currentPage.getPath() ); //out.print(currentPage.getPath());//out.print(md5);
   
   // note how we set the #fancybox-outer transparent using jquery (onComplete)
   // so inline content doesn't take on the #fff background specified in the
   // stock css.
   if (active && (WCMMode.fromRequest(request)!=WCMMode.EDIT)) { %>
     <cq:includeClientLib categories="public.components.lightbox" />	
     <div id="fancybox-<%= randomID %>"></div>
     <script type='text/javascript'>
      $(document).ready(function(){
           var duration = "<%=duration%>";
     	   var c_name = "fb-<%= md5 %>";
           var check_session = <%=(WCMMode.fromRequest(request)!=WCMMode.DISABLED)?"false":"true" %>; // set to true to debug
           if(check_session && document.cookie.indexOf(c_name + "=1")>-1 && duration!="each"){ return; }
	       $("#fancybox-<%= randomID %>").fancybox({
	         'width'        : <%=width%>,
	         'height'      : <%=height%>,
	         'aspectRatio'      : true,
	         'openEffect'    : 'none',
	         'closeEffect'    : 'none',
	         'type'        : 'iframe',
	         'href' : '<%=url%>',
	         'padding' : 0,
	         'scrolling': '<%=scroll%>',
	         'helpers' : {
	             'overlay' : {
	                 'css' : { 'background-color':'rgba(0,0,0,.5)' }
	             }
	         },
	         'afterLoad' : function(){ $('.fancybox-skin').css('background-color','transparent'); }       
	       });    
	       $("#fancybox-<%= randomID %>").trigger('click'); // trigger the lightbox
	       <% if(duration.equals("session")){ %>
	         document.cookie = escape(c_name) + "=1"; // session cookie
	       <% ;} %>
           <% if(duration.equals("user")){ %>
             var now = new Date();
             now.setMonth( now.getMonth()+12 ); // in this world, "permanent" equals 1 year.
             document.cookie = escape(c_name) + "=1;expires=" + now.toGMTString() + ";"; //alert( now.toGMTString()); // "permanent" cookie
           <% ;} %>
	       
      }); 
     </script>

<% } else { %>

   <% if(WCMMode.fromRequest(request)==WCMMode.EDIT){ %>
	   <cq:include script="empty.jsp"/>
   <% } %>
	    
<% } %>
