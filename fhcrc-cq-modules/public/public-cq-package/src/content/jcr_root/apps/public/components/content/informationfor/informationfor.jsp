<%@include file="/libs/foundation/global.jsp"%><%@ page import="java.util.Iterator,
com.day.cq.wcm.foundation.List, com.day.cq.wcm.api.PageFilter,com.day.cq.wcm.api.WCMMode" %><%
 int hashCode = resource.hashCode();
 boolean active = properties.get("activate",Boolean.FALSE); // user can disable the widget by unchecking a checkbox
%>

<% if (active) { %>

	<%-- all the important script content is located in a co-located js file: --%>
	<cq:includeClientLib categories="public.components.informationfor" />
	
	<div id="info4_<%=hashCode%>" class="infoforcontainer clearfix">
	  <div class="infolabel">
	    Information for:
	  </div>
	  <div class="infocontent clearfix">
	    <div class="more"><a href="#">More</a>&nbsp;&gt;</div>
	
	<%
	   List list = new List(slingRequest, new PageFilter()); // not sure if we want the PageFilter here?
	   if (!list.isEmpty()) {
	       Iterator<Page> items = list.getPages();
	       while ( items.hasNext() ) {
	           Page p = items.next();
	           String title = (String)p.getProperties().get("./footerNavigationTitle", "");
	           if (title.isEmpty()) {
	               title = p.getNavigationTitle();
	               if (title == null) {
	                   title = p.getPageTitle();
	                   if (title == null) {
	                       title = p.getTitle();
	                   }
	               }
	           }
	           
	           %>
	              <div class="item"><a href="<%= p.getPath() %>.html"><%= title %></a></div>           
	           <%           
	       }
	   }
	%>
	
	  </div>      
	</div>
	
	<script type="text/javascript">
	  $(document).ready( function(){ componentApp.informationFor.initInfoFor($("#info4_<%=hashCode%>")); } );
	</script>
	
	
<% } else { %>

   <% if(WCMMode.fromRequest(request)==WCMMode.EDIT){ %>
       <cq:include script="empty.jsp"/>
   <% } %>
        
<% } %>
	