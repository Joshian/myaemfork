<%@include file="/libs/foundation/global.jsp" %>
<div class="row-fluid midcontent">
    <div class="columnleft span4">
        <div class="logo">
              <img src="/content/dam/citn/logo_black.gif" id="home_logo" alt="Cancer Immunotherapy Trials Network" border="0"/>
        </div>
        <div class="leftpar">
            <cq:include path="par" resourceType="foundation/components/parsys"/>
        </div>
        <!-- logos. separate, so you can adjust them left and right a bit for fit. -->
        <div id="home_logos" class="home_logos">
            <a href="http://www.fhcrc.org" target="_blank"><img alt="Fred Hutchinson Cancer Research Center" border="0" hspace="10" src="/content/dam/citn/logo_fhcrc.gif"/></a> <a href="http://uwmedicine.washington.edu" target="_blank"><img alt="UW Medicine" border="0" src="/content/dam/citn/logo_um.gif"/></a>
        </div> 
    </div>
    <div class="maincontent span8">
        <div id="page_container_home" class="page_container_home">
      
        <!-- float the hero image right. not part of the page flow (absolute) -->
        <img src="/content/dam/citn/hero_home.jpg" id="hero_home" border="0" alt="T-lymphocyte (green) attacking a cancer cell (blue)." />
      </div> 
    </div>

</div>