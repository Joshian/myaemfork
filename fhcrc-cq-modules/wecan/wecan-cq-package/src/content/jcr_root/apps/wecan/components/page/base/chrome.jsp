<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="java.util.Calendar" %>
<%
Calendar theTime = Calendar.getInstance();
int theYear = theTime.get(Calendar.YEAR);
%>
  <!-- header appears on all pages incl. home page -->
  <div class="header" id="headerElement">
    
    <div class="headerContent">
    
      <div class="logo">
        
        <a href="<%= currentPage.getAbsoluteParent(1).getPath() %>/en.html"><img src="<%= currentDesign.getPath() %>/img/we-can-logo.png"></a>
          
      </div>
        
      <div class="searchAndTagline">
            
        <div class="search">
          <form action="<%= currentPage.getAbsoluteParent(2).getPath() %>/search.html" id="mainSearchForm">
            <input class="searchBox" maxlength="2048" name="q" placeholder="Search" type="text">
            <input class="searchButton" value="GO" type="submit" class="gradient">
          </form>
        </div>
          
        <div class="tagline">
          <img src="<%= currentDesign.getPath() %>/img/we-can-tagline.png">
        </div>
      </div>
    
<!-- MOBILE MENU -->        

      <div class="mobileMenu">
        
        <img src="<%= currentDesign.getPath() %>/img/menu.png" alt="menu" id="mobileMenuButton" title="Open Menu">
        <img src="<%= currentDesign.getPath() %>/img/search.png" alt="search" id="mobileSearchButton" title="Search Site">
        
      </div>

<!-- END MOBILE MENU -->    
    
      <div class="clear"></div>
    
    </div>

<!-- GLOBAL NAVIGATION -->
    <cq:include script="globalNav.jsp" />
    
<!-- MOBILE SEARCH AND NAVIGATION -->      
      
    <div class="mobileSearch" id="mobileSearch">
      
      <form action="<%= currentPage.getAbsoluteParent(2).getPath() %>/search.html" id="mobileSearchForm">
        <input class="searchBox" maxlength="2048" name="q" placeholder="Search this site..." type="text">
        <input class="searchButton" value="&raquo;" type="submit">
      </form>
      
    </div>
<!-- MOBILE NAVIGATION -->      
    <cq:include script="mobileNav.jsp" />

<!-- END MOBILE SEARCH AND NAVIGATION -->

  </div>
  
<!-- END HEADER -->

<!-- FOOTER -->

  <div class="footer" id="footerElement">
    
    <div class="footerContent">
      
      <div class="footerInfo" id="footerInfo">
        
        <div class="footerStatement">
            WE CAN Women's Empowerment Cancer Advocacy Network, a global network for change in breast and other women's cancers since 2003.
        </div>
          
        <div class="footerButtonsAndCopyright">
          
          <div class="footerButtonContainer leftButton">
            
            <a href="/content/wecan/en/contact-us.html" class="button">Contact&nbsp;Us</a>
            
          </div>
            
          <div class="footerCopyright">
            
            <p>Fred Hutchinson Cancer Research Center<br>
              1100 Fairview Ave. N., P.O. Box 19024, Seattle, WA 98109<br>
              &copy; <%= theYear %> Fred Hutchinson Cancer Research Center,<br>
              a 501(c)(3) nonprofit organization.</p>
              
              <p>Terms of use &amp; privacy policy
            
          </div>
            
          <div class="footerButtonContainer rightButton">
            
            <a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/donate.html" class="button">Donate</a>
            
          </div>
            
          <div class="clear"></div>
          
        </div>
        
      </div>
      
      <div class="footerLogos">
      
        <div class="footerLogo logo1">
          
          <a href="http://www.fredhutch.org" target="_blank"><img src="<%= currentDesign.getPath() %>/img/fhcrc_logo.png"></a>
          
        </div>
          
        <div class="footerLogo logo2">
          
          <a href="http://www.uwmedicine.org/Pages/default.aspx" target="_blank"><img src="<%= currentDesign.getPath() %>/img/UWMed_logo.png"></a>
          
        </div>
          
        <div class="footerLogo logo3">
          
          <a href="http://www.seattlecca.org/" target="_blank"><img src="<%= currentDesign.getPath() %>/img/SCCA_logo.png"></a>
          
        </div>      
      
      </div>
      
    </div>

  </div>

<!-- END FOOTER -->  