// javascript for Fred Hutchinson Cancer Research Center 


// page loaded functions...
$(document).ready(function()
{

  // set up any styling that you need to do via Jquery, etc.
  setDynamicStyles();
  
  // initialize any tabbed widgets that may be on this page
  //initTabbedWidgets(); // removing in favor of a per-tab instantiation. see initHeaderWidget().
  
  // initialize any header widgets that may be on this page
  initHeaderWidgets(); 
  
  // initialize the addtoany social share objects
  initSocialShare();
  
  // add Google Analytics click events to appropriate targets
  initGoogleAnalytics();

  // set up sitewide nav tracking
  initClickTrackingSitewideNav();

  // set up search box
  initUtilityNavSearch();
  
  // preload images
  var imageSrcPrefix = window.location.protocol + "//" + window.location.hostname;
  var daggerImageSrc = "/etc/designs/public/img/nav/dropdown-dagger-bar.png";
  FH_preloadImages(imageSrcPrefix + daggerImageSrc);
  
    
});

$(window).load(function(){
  // initialize any rollover images that may be on this page
  initRolloverImages();
  
  //even out the columns if this is the homepage
  if ($("body").hasClass("homepage") || $("body").hasClass("branchleader")) {
	  evenColumnHeights();
  }
});


/* Hides the search box and allows it to be revealed when search link in utility nav is clicked */
function initUtilityNavSearch() {
	
	var theSearchBox = $("form#utility_search"),
		theSearchLink = $("a#utility_search_link"),
		theSearchInputBox = $("form#utility_search input[name=q]");
		
	theSearchBox.hide();
	
	theSearchLink.click(function(e) {
		
		e.preventDefault();
		theSearchBox.fadeToggle(function(){
	        if(theSearchInputBox && theSearchBox.css('display')!='none'){
	            theSearchInputBox.focus();
	        }						
		});
		
	});
	
}


/* some styles need fancier selectors than provided by CSS.
 * 
 */
function setDynamicStyles(){
	
  // set up styles for SquareBullet, which gets placed on spans within the li tags, not the ul tag as we want:
  $("ul:has(span.SquareBullet)").addClass("SquareBullet")

}


/* set up tracking for site-wide navigation elements  
 * this is called on document ready. 
 */
function initClickTrackingSitewideNav(){
  var thisLabel="";
  var debug=false; // turn on to see the tracking labels

  // utility nav
  $('.utility_nav ul a').click(function(){
     thisLabel = $(this).text(); if(debug) alert(thisLabel);
     if(thisLabel && thisLabel.length>0){
       _gaq.push(['_trackEvent','Sitewide Navigation' ,'Utility Nav' , thisLabel]);
     }
  });
  
  // global nav
  $('#global_nav a').click(function(){
     thisLabel = $(this).text(); 
     if(!thisLabel || thisLabel.length<1) { thisLabel = $(this).attr('href'); if(thisLabel && thisLabel.indexOf('://')==-1){ thisLabel='http://www.fredhutch.org'+thisLabel; } } // home is a graphic, not text
     if(thisLabel && thisLabel.length>0){ if(debug) alert(thisLabel);
       _gaq.push(['_trackEvent','Sitewide Navigation' ,'Global Nav' , thisLabel]);
     }
  });
  
  // global nav dropdowns
  // model: <div class="nav_dropdown" id="dropdown-diseases">
  var dropdownLabel="";
  $('div.nav_dropdown a').click(function(){
      dropdownLabel = $(this).closest("div.nav_dropdown").attr('id'); if(!dropdownLabel) dropdownLabel='unknown';
      thisLabel = $(this).text(); 
      if(!thisLabel) { thisLabel = $(this).attr('href'); if(thisLabel && thisLabel.indexOf('://')==-1){ thisLabel='http://www.fredhutch.org'+thisLabel; } }
      thisLabel = dropdownLabel + ' - ' + thisLabel; if(debug) alert(thisLabel);    
      _gaq.push(['_trackEvent','Sitewide Navigation' ,'Global Nav Dropdown' , thisLabel]);
    
  });

  // fat footer
  $('.footer_container .footer_content a').click(function(){
     thisLabel = $(this).text(); 
     if(!thisLabel || thisLabel.length<1) { thisLabel = $(this).attr('href'); if(thisLabel.indexOf("://")==-1){ thisLabel = "http://www.fredhutch.org" + thisLabel; } }
     if(thisLabel){ if(debug) alert(thisLabel);
       _gaq.push(['_trackEvent','Sitewide Navigation' ,'Fat Footer' , thisLabel]);
     }
  });
      
}



/* set up the display and interaction of a single tabbed widget (tabComponent)
 * accommodates "edit" mode, where you want all the tabs visible for editing
 * send in any value for editmode to trigger that behavior
 */
function initTabbedWidget(widget_id,editmode){
    // if you are in edit mode, you want to make it so View all is showing
    // and disable the onclick handlers for all the tabs (reverts to the 
    // anchor behavior)
    if(editmode){
      // make the view all tab active
      $("ul.tabs li.viewAll","#"+widget_id).addClass('active');
      // disable just the view all tab
      $("ul.tabs li","#"+widget_id).click(function() {
        if($(this).hasClass('viewAll')) {return false;}
      });
      return;
    }
    
    var tabLabel; var tabPos; // used for GA tracking

	// remaining code is for preview and production rendering: 
	$(".tabContent", "#"+widget_id).hide(); 
	$("li:first", "#"+widget_id).addClass("active").show();
	$(".tabContent:first", "#"+widget_id).show();	
	$(".viewAllOnly", "#"+widget_id).addClass('hide'); //$(".viewAllOnly", "#"+widget_id).hide();
	
    // event handler for clicking on tabs
	$("ul.tabs li","#"+widget_id).click(function() { 
		var theContainer = $("#"+widget_id);
		if($(this).hasClass("viewAll")){ // if this is the view all tab...
			$(".tabContent", theContainer).show();
			$(".viewAllOnly", theContainer).removeClass('hide');
		} else { // otherwise....
		    $(".viewAllOnly", "#"+widget_id).addClass('hide');
			var theIndex = $(this).index();
			$(".tabContent", theContainer).hide();
			$(".tabContent", theContainer).eq(theIndex).fadeIn(75);
		}
		$("ul.tabs li", theContainer).removeClass("active");
		$(this).addClass("active");
		// add GA click tracking to improve user experience
		tabLabel = $(this).text(); tabPos = $(this).index(); //alert(tabLabel + " : " + tabPos);
		if(tabLabel) { _gaq.push(['_trackEvent','Tab Widget' ,'Tab Clicks' , tabLabel, tabPos+1]); } // option base 1
		return false; // don't follow the anchor link down the page
	});

	// event handler for hover over tabs
	$("ul.tabs li","#"+widget_id).hover(function(){
		if($(this).hasClass("active")){
			return;
		} else {
			$(this).addClass("hover");
		}
		}, function(){
			$(this).removeClass("hover");
	});
	//If the URL contains a hash of the form #tab\d, make tab number \d active
	var regex = new RegExp("#tab([1-7])");
	var results = regex.exec(window.location.href);
	if(results != null){
		$("ul.tabs li","#"+widget_id).eq(results[1] - 1).click();
	}
	listSplit();
}




//This sets up the minor hover effects on tabbed facade components
function initTabFacadeWidget(widget_id){ //alert(widget_id);
	$("ul.tabs li","#"+widget_id).hover(function(){
		if($(this).hasClass("active")){
			return;
		} else {
			$(this).addClass("hover");
		}
		}, function(){
			$(this).removeClass("hover");
	});
}




/* tabbed widget - set up all tabbed widgets. they are identified with .tabsContainer, may be multiple per page */
// NO LONGER USED: replaced with an instantiate-each approach, see initTabbedWidget().
function initTabbedWidgets(){
	$(".tabsContainer .tabContent").hide();
	$("li:first", ".tabsContainer").addClass("active").show();
	$(".tabContent:first", ".tabsContainer").show();
	$(".viewAllOnly", ".tabsContainer").hide();
	
	$(".tabsContainer ul.tabs li").click(function() {
		var theContainer = $(this).parents(".tabsContainer");
		if(this.id == "viewAll"){
			$(".tabContent", theContainer).show();
			$(".viewAllOnly", theContainer).show();
		} else {
		var theIndex = $(this).index();
		$(".tabContent", theContainer).hide();
		$(".viewAllOnly", theContainer).hide();
		$(".tabContent", theContainer).eq(theIndex).fadeIn(75);
		}
		$("ul.tabs li", theContainer).removeClass("active");
		$(this).addClass("active");
		return false;
	});
	$(".tabsContainer ul.tabs li").hover(function(){
		if($(this).hasClass("active")){
			return;
		} else {
			$(this).addClass("hover");
		}
		}, function(){
			$(this).removeClass("hover");
	});
	listSplit();
}

/* tabbed widget - split contents in two */
function listSplit() {
	$(".tabsContainer ul.splitMe").each(function(){
		/* First, add this div that we will put the content into */
		$('<div class="column2"></div>').insertAfter($(this));						  
		var amount = $(this).find("li").size();
		var halfway = Math.ceil(amount / 2);
		halfway++; // added, sld, 2/2012
		var halfwayElement = $(this).find("li").eq(halfway - 1); /* -1 since eq() starts at zero, while size() starts at 1 */
		/* If the halfway point is of class "subitem", march forward until we find one that is not */
		while(halfwayElement.hasClass("subitem")){ 
			halfway++; 
			halfwayElement = $(this).find("li").eq(halfway - 1);
		}
		/* Now find the halfwayElement in the original ul, tricky since there may also be additional ul's in there */
		for(var i = 0; i < $(this).children().size(); i++) {
			var tester = $(this).children().eq(i);
			if(tester[0] === halfwayElement[0]){
				break;
			}
		}
		/* Finally, grab all children after and including the halfwayElement, wrap them in a ul, and move them to the second div */
		$(this).children().slice(i).appendTo($(this).parent().find("div.column2")).wrapAll("<ul />");
	})
}



/* header component - reposition the "more" link (if there is one) to be on the same line
as the title text. model is:

	<div class="headerwidget addline">
	<h2 class="title">My Header</h2>
	<span class="more"><a href="#">Recent articles</a><span class="raquo">&raquo;</span></span>   
	</div>   
	   
 */
 
function initHeaderWidgets(){
	// pick up any header widgets that may be on this page
	$.each( $("div.headerwidget"), function(){ headerMorelinkPos(this) } );  
}
 
function headerMorelinkPos(el){ 

	var title_element = $(el).find(".title"); 
	var morelink_element = $(el).find(".more");
	if( title_element.length!=1 || morelink_element.length!=1 ) { return; } /* abort if the expected elements are not found */
	
    // create a nbsp node and append it to the text in the title if there's not one there already (multiple refreshes would do that)
    if( $(title_element).find(".marker").length==0 ){
      $(title_element).append("<span class='marker'>&nbsp;</span>");
    } 
	
	var d = parseInt( $(title_element).find(".marker").offset().top ); //alert(d);
	var e = parseInt( $(morelink_element).offset().left ); 
	var shim = 7; // fudge factor. feel free to adjust to work across browsers.
	$(morelink_element).offset({ top:d+shim, left:e }); // skootch the more link up by this amount    
	
	// add padding so the text doesn't cover up the more link
	$(title_element).css('padding-right',$(morelink_element).css('width') );
	$(title_element).css('padding-right', '+=10');  
	
	// equate the line heights. this might be unnecessary; adjust the shim if omitted.
	$(morelink_element).css('line-height', $(title_element).css('line-height') );       
    
}


/* set up the event handlers for a particular donation formlet object. 
send in the id of the enclosing div, e.g., 'MyId' of <div class="donateformlet" id="MyId"> */
function initDonateFormlet(df_id,design_id,default_amt){
	$('#'+df_id+'.donateformlet.colorTheme1').before('<div class="top rail gradient"></div>').after('<div class="bottom rail gradient"></div>'); // add rails to colorTheme1
	$('#'+df_id+'.donateformlet form').submit(function(){

      // in the standard mode there is a fill-out box and suggested value; in other modes these features don't exist. the init script will send -1 for the latter.
      var standard_mode = default_amt == -1 ? false : true; 

      if(standard_mode){
		  var arTmp = this.amount.value.replace( /,/g,'' ).match( /\d+\.?(\d\d)?/ );  // erase commas, grab digits only, e.g., 45 or 45. or 45.00
		  if(arTmp){ //alert(arTmp[0]);	   
		    this['set.Value'].value=parseInt(.5+arTmp[0]*100); //alert(this['set.Value'].value); // sends as integer number of cents, not dollars.
		  } else { //alert('Cannot determine amount');	   
		    this['set.Value'].value=0; // don't send an amount if you can't determine it.
		  }  	
	  }
	
	  /* analytics */
	  if(_gaq){
	    if(standard_mode){
	      _gaq.push(['_trackEvent','Donation Formlet', design_id, "Default amount = " + default_amt, parseInt(this['set.Value'].value/100)]); //alert("tracked " + parseInt(this['set.Value'].value/100));
	    } else {
	      _gaq.push(['_trackEvent','Donation Formlet', design_id, "Default amount = n/a", 0]); 
	    }
	  } else {
	    //alert("no tracking");
	  }
	  
	});	 
}


/* set up the addtoany social share widgets. */
function initSocialShare(){
    // reveal the widgets if user has js (they start out hidden) 
	$(".share_options").show();
	// add click handling so users get a pop-up window vs. a new tab when using facebook, twitter and email tabs 
    $("a.a2a_button_facebook,a.a2a_button_twitter,a.a2a_button_email,a.a2a_button_google_plus").click( function(){  sharewinobj=window.open('','sharewin','height=500,width=800,scrollbars,resizable');sharewinobj.location.href=$(this).attr('href');sharewinobj.focus();return false; } );
}




// general-use function to add handlers. use like this:
//    if(document.getElementById('ehs.form')){
//      addEvent(document.getElementById('ehs.form'), 'click', handleRadioClick);
//    }
// JS Bible 6th ed.
function addEvent(elem,evtType,func){
  if(elem.addEventListener){ elem.addEventListener(evtType,func,false); }
  else if(elem.attachEvent){ elem.attachEvent("on"+evtType, func); }
  else { elem["on"+evtType] = func; }
}

// adapted from code from http://www.selfcontained.us/2008/03/08/simple-jquery-image-rollover-script/
function initRolloverImages(){
	$(".rolloverImage").each(function(){
		$(this).attr("width",this.clientWidth);
		$(this).attr("height",this.clientHeight);
	});
	  $('.rolloverImage').mouseenter(function() {
	      var currentImg = $(this).attr('src');
/*	      var theWidth = $(this).outerWidth(); alert(theWidth); alert($(this).width());
	      var theHeight = $(this).outerHeight();
	      */
	      $(this).attr('src', $(this).attr('data-hover'));//.width(theWidth).height(theHeight);
	      $(this).attr('data-hover', currentImg);
	  });
	  $('.rolloverImage').mouseleave(function() {
	      var currentImg = $(this).attr('src');
	      $(this).attr('src', $(this).attr('data-hover'));
	      $(this).attr('data-hover', currentImg);
	  });	  
}

/* googleAnalytics is an object containing methods and properties to add click
 * events to links for capturing in Google Analytics
 * 
 * @property  linkedImages   A jQuery object containing links that themselves
 *                           contain images
 * @property  externalLinks  A jQuery object containing text links whose href is
 *                           an external site
 * @property  pdfLinks       A jQuery object containing text links whose href is
 *                           a PDF file, internal to the site
 * @method    addClickEvents Takes a jQuery group and one or two arguments. A
 *                           click event is placed on each member of the group
 *                           This handler is a call to _gaq.push using the
 *                           arguments passed 
 */

function initGoogleAnalytics() {
//Creating a module to avoid polluting the global namespace
	var googleAnalytics = (function($, undefined){
/* We only care about the body content and related content divs */
		var scope = $('div.fh_body_content, div.relatedcontentarea');
/* A jQuery object containing any links that contain images */
		var linkedImages = $("a:has(img), area[href]", scope);
/* A jQuery object containing any external links, not including those that
contain images */
		var externalLinks = $("a[href^='http']", scope).not(":has(img)");
/* A jQuery object containing any links to PDF files, not including PDFs
hosted on external sites */
		var pdfLinks = $("a[href$='.pdf']", scope).not("[href^='http']");
/* A jQuery object containing any internal links that contain anchor suffixes 
(e.g. #video) */
		var anchorLinks = $("a[href*='#']", scope).not("[href^='http']");
/* A testing method that simply highlights the elements of the various
jQuery objects, allowing you to make sure there is no overlap/omissions */ 
		var highlightSelected = function() {
			linkedImages.css({'border':'2px solid red'});
			externalLinks.css({'border':'2px solid blue'});
			pdfLinks.css({'border':'2px solid orange'});
			anchorLinks.css({'border' : '2px solid green'});
		};
/* A function that returns a function that can be used as an event handler.
 *
 * @param arg3    A string representing the genre of event being tracked
 *                by Google Analytics
 * @param arg4    A string representing the specific event being tracked
 *                by Google Analytics
 * @param arg5    Optional integer that can be passed into Google Analytics
 * @return theGAQ A function ready to be attached to an event handler
 *                that calls the Google Analytics engine 
 */
		var giveMeGAQ = function(arg3, arg4, arg5) {
			var theGAQ;
			if(arg5 === undefined) {
				theGAQ = function() {
					_gaq.push(['_trackEvent','Click tracking', arg3, arg4]);
				};
			} else {
				theGAQ = function() {
					_gaq.push(['_trackEvent','Click tracking', arg3, arg4, arg5]);
				};
			}
			return theGAQ;
		};
/* A function that adds the click events to the elements of a jQuery objects.
 *
 * @param group  The jQuery object containing the elements you want the 
 *               events added to
 * @param arg3   A string representing the genre of event being tracked
 *               by Google Analytics
 * @param arg5   Optional integer that can be passed into Google Analytics
 * @return       void
  */
		var addClickEvents = function(group, arg3, arg5) {
			group.each( function() {
/* If the element already has an onclick attribute, then that is a Google
Analytics push, so we don't want to add another one. Check against null
and the empty string, as either could be returned if there is no attribute */
				if(
				this.getAttribute('onclick') === null || 
				this.getAttribute('onclick') === "") {
/* Next, check to see if there are event handlers on the element using
jQuery's $.data('events') method. */
					if(
					$(this).data('events') === undefined ||
					$(this).data('events').click === undefined) {
						var arg4 = this.getAttribute('href');
						var theGAQ;
						if(arg5 === undefined) {
							theGAQ = giveMeGAQ(arg3, arg4);
						} else {
							theGAQ = giveMeGAQ(arg3, arg4, arg5);
						}
						$(this).click(theGAQ);
					}
				}
			});
		};
/* Return the methods and properties that I want exposed to the world */	
		return {
//			highlightSelected : highlightSelected,
			addClickEvents : addClickEvents,
			linkedImages : linkedImages,
			externalLinks : externalLinks,
			pdfLinks : pdfLinks,
			anchorLinks : anchorLinks
		};
	})(jQuery);
	
//	googleAnalytics.highlightSelected();
	googleAnalytics.addClickEvents(googleAnalytics.linkedImages, "Image Link");
	googleAnalytics.addClickEvents(googleAnalytics.externalLinks, "External Text Link");
	googleAnalytics.addClickEvents(googleAnalytics.pdfLinks, "Internal PDF Link");
	googleAnalytics.addClickEvents(googleAnalytics.anchorLinks, "Internal Anchored Text Link");
	
}

function evenColumnHeights() {
	
	var COLUMN_CLASS_NAMES = ".cq-colctrl-lt0, .cq-colctrl-lt0w, .cq-colctrl-lt0x, .cq-colctrl-lt1";
	var theColumns = $(COLUMN_CLASS_NAMES);

	/* Run through each columns div, find the components inside those divs (should only have one component per column),
	 * find the maximum height among those components, and set all components to that height. */
	
	theColumns.each(function(){
		
		var maxHeight = 0;
		var that = $(this);
		var theComponents = that.find(".parsys_column div[class*='colorTheme']");
		
		for(var i = 0; i < theComponents.size(); i++) {
			
			var tempHeight = theComponents.eq(i).outerHeight(false);
			if (tempHeight > maxHeight) {
				
				maxHeight = tempHeight;
				
			}
			
		}
		
		theComponents.css("height", maxHeight+"px");
		
	});
	
}

/* jQuery image pre-loader from http://css-tricks.com/snippets/jquery/image-preloader/ */
/* This was a method of the $ variable, but we are getting conflicts with multiple versions of jQuery running
 * on the same page (unavoidable), so I am making this a namespaced global variable instead. - IS*/

var FH_preloadImages = function() {
	for (var i = 0; i < arguments.length; i++) {
		$("<img />").attr("src", arguments[i]);
	}
}