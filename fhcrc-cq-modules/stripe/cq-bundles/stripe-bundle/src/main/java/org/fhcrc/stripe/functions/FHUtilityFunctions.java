package org.fhcrc.stripe.functions;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.*;

import org.apache.sling.api.resource.*;

import java.util.Date;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.foundation.Image;

public class FHUtilityFunctions {

/* rev. July 15, 2013 */    
    
/** 
 * Returns a String containing the correctly styled (by AP guidelines) date when
 * passed a Date object.
 * 
 * @param  theDate a Date object representing the date you want styled
 * @return         the Month, Day and Year, correctly styled by AP standards
 */ 
    public static String APStylize(Date theDate) {
        
        DateFormat f = new SimpleDateFormat("MMM d, yyyy");
        //DateFormat f_test = f; // testing SVN plug-in; erase me if you want. sld. 10/9/2012
/* Converting to AP date style */
        StringBuffer d = new StringBuffer(f.format(theDate));
        String dstring = d.toString();
/* Insert a period after any of the 3-character months */
        if(dstring.matches("Jan.*|Feb.*|Aug.*|Oct.*|Nov.*|Dec.*")) {
            d.insert(3,"."); 
        } else if(dstring.matches("Mar.*")) {
            d.insert(3,"ch");
        } else if(dstring.matches("Apr.*")) {
            d.insert(3,"il");
        } else if(dstring.matches("Jun.*")) {
            d.insert(3,"e");
        } else if(dstring.matches("Jul.*")) {
            d.insert(3,"y");
        } else if(dstring.matches("Sep.*")) {
            d.insert(3,"t.");
        }
        return d.toString();
    }
    
/**
 * Returns a String representing the title of the Page passed in. The hierarchy
 * is as follows: Navigation Title -> Page Title -> Title -> Name   
 * 
 * @param  thePage a Page object whose title you want
 * @return a String, representing the title of the Page
 */
    
    public static String displayTitle(Page thePage) {
        String theTitle = thePage.getNavigationTitle();
        if(theTitle == null) {
            theTitle = thePage.getPageTitle();
            if(theTitle == null) {
                theTitle = thePage.getTitle();
                if(theTitle == null) {
                    theTitle = thePage.getName();
                }
            }
        }
        return theTitle;
    }

/**
 * Returns a String representing a URI. 
 * @param thePath a String, usually retrieved from a pathfield in a component dialog
 * @param resourceResolver a ResourceResolver, needed to check if the path String points to a cq:Page object in the system
 * @return a String, representing a URI, usually to be placed in an <a> tag as its href attribute
 */
    
    public static String cleanLink(String thePath, ResourceResolver resourceResolver) {
        String[] allowedPrefixes = {"https?:.*", "mailto:.*", "tel:.*"};
        Resource r;
        
        /* If the path passed in starts with any of the allowed prefixes, then just return back the path */
        for(int i = 0; i < allowedPrefixes.length; i++) {
            if(thePath.matches(allowedPrefixes[i])) {
                return thePath;
            }
        }
        
        /* Otherwise, if the path starts with a slash, then we assume it to be an asset in the system. If that asset
         * is of type cq:Page, then we have to append a ".html" to make the link understandable by the browser. */
        if(thePath.matches("^/.*")){
        	
        	/* If there is a fragment in the URL, we need to insert the ".html" in front of it */
        	if (thePath.matches("^/.*#.*")) {
        		
        		thePath = thePath.replaceAll("^/([^#]+)#(.*)", "/$1.html#$2");
        		
        	} else {
        	
                r = resourceResolver.getResource(thePath); //log.info(r.getResourceType());
                if(r!=null && r.isResourceType("cq:Page")){ // added, 2/06/2012 sld
                    thePath += ".html";
                }

        	}

        } else {
        /* If the path starts with anything other than a slash, we assume it to be an external link and we prepend
         * "http://" to it to make the link understandable by the browser. */
            thePath = ("http://").concat(thePath);
        }
        
        return thePath;
        
    }

/**
 * Returns an Image who src attribute should be prefix-nuked if the image resides on a server that uses prefix nuking. 
 * @param image, an Image. This is the image you want to display
 * @param resourceResolver a ResourceResolver, needed to use the map method to actually nuke the prefix
 * @return an Image that can be drawn using the draw method of the Image object
 * @throws UnsupportedEncodingException 
 */    
    
    public static Image nukeURLPrefix(Image image, ResourceResolver resourceResolver) throws UnsupportedEncodingException {
    	
    	final String UPLOADED_IMAGE_REGEX = "(.*)?/file\\.(.*)?";
    	String imagePath = image.getSrc();
    	
    	if (imagePath.matches(UPLOADED_IMAGE_REGEX)) {
    		imagePath = imagePath.replaceAll(UPLOADED_IMAGE_REGEX, "$1.img.$2");
    	}
    	
    	image.setSrc(URLDecoder.decode(resourceResolver.map(imagePath), "UTF-8"));
    	
    	return image;
    	
    }    
    
}