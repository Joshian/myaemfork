var componentApp = (function(componentApp) {
	
	componentApp.profileFinder = (function(profileFinder) {
		
		$(document).ready(function() {
			$("div#alphaSelect a").each(function(){
				if($(".profileresult[data-alpha=" + this.id + "]").length != 0 || this.id == "showAll"){
					$(this).addClass("active");
					$(this).removeClass("inactive");
				}
			});
			$("div#alphaSelect a.inactive").click(function(){
				return false;
			});
			$("div#alphaSelect a.active").click(function(){
				if(this.id == "showAll"){
					$(".profileresult").show();
				} else {
					$(".profileresult").hide().filter("[data-alpha=" + this.id + "]").show();
				}
				return false;
			});
		});
		
		return profileFinder;
		
	})(componentApp.profileFinder || {});
	
	return componentApp;
	
})(componentApp || {});

