package org.fhcrc.hvtn.components;

import org.apache.sling.api.resource.ValueMap;
import org.fhcrc.tools.FHUtilityFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.api.resource.*;
import java.lang.Integer;

public class TwitterFeed {
	
	private static final Logger log = LoggerFactory.getLogger(TwitterFeed.class);
	
	private final String DEFAULT_TWITTER_LINK = "http://twitter.com";
	private final String DEFAULT_LINK_TEXT = "Our Tweets";
	private final String LINK_CLASS = "twitter-timeline";
	
	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	
	private String twitterLink;
	private String widgetID;
	private String linkColor;
	private String chromeOptions;
	private String dataAttributes;
	private String linkText;
	
	private Integer tweetLimit;
	private Integer width;
	private Integer height;
	
	private boolean header;
	private boolean footer;
	private boolean transparent;
	
	public TwitterFeed() {
		
	}

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
		setTwitterLink(properties.get("twitterLink",""));
		this.widgetID = properties.get("widgetID","");
		this.linkColor = properties.get("linkColor","");
		this.linkText = properties.get("linkText",DEFAULT_LINK_TEXT);
		this.tweetLimit = properties.get("tweetLimit",Integer.class);
		this.width = properties.get("width",Integer.class);
		this.height = properties.get("height",Integer.class);
		this.header = properties.get("hasHeader",false);
		this.footer = properties.get("hasFooter",false);
		this.transparent = properties.get("isTransparent",false);
		setChromeOptions();
		setDataAttributes("");
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public String getTwitterLink() {
		return twitterLink;
	}


	public void setTwitterLink(String twitterLink) {
		try {
			
			if(!twitterLink.equals("")){
		        this.twitterLink = FHUtilityFunctions.cleanLink(twitterLink, resourceResolver);
		    } else {
		    	this.twitterLink = DEFAULT_TWITTER_LINK;
		    }
		} catch(Exception e) {
			log.error("Error setting Twitter Link", e);
		}
	}


	public String getWidgetID() {
		return widgetID;
	}


	public void setWidgetID(String widgetID) {
		this.widgetID = widgetID;
	}


	public String getLinkColor() {
		return linkColor;
	}


	public void setLinkColor(String linkColor) {
		this.linkColor = linkColor;
	}


	public Integer getTweetLimit() {
		return tweetLimit;
	}


	public void setTweetLimit(Integer tweetLimit) {
		this.tweetLimit = tweetLimit;
	}


	public Integer getWidth() {
		return width;
	}


	public void setWidth(Integer width) {
		this.width = width;
	}


	public Integer getHeight() {
		return height;
	}


	public void setHeight(Integer height) {
		this.height = height;
	}


	public String getLinkText() {
		return linkText;
	}

	public void setLinkText(String linkText) {
		this.linkText = linkText;
	}

	public boolean isHeader() {
		return header;
	}


	public void setHeader(boolean header) {
		this.header = header;
	}


	public boolean isFooter() {
		return footer;
	}


	public void setFooter(boolean footer) {
		this.footer = footer;
	}


	public boolean isTransparent() {
		return transparent;
	}


	public void setTransparent(boolean transparent) {
		this.transparent = transparent;
	}
	
	public String getDataAttributes() {
		return dataAttributes;
	}
	
	public void setDataAttributes(String dataAttributes) {
		
		try {

			if (dataAttributes == null) {
				dataAttributes = "";
			}
			
			if (!(linkColor == null || linkColor.equals(""))) {
				dataAttributes = dataAttributes.concat("data-link-color=\"").concat(linkColor).concat("\"");
			}
			if (tweetLimit != null && tweetLimit != 0) {
				dataAttributes = appendWithSpace("data-tweet-limit=\"".concat(tweetLimit.toString()).concat("\""), dataAttributes);
			}
			if (!(widgetID == null || widgetID.equals(""))) {
				dataAttributes = appendWithSpace("data-widget-id=\"".concat(widgetID).concat("\""), dataAttributes);
			}
			if (!(chromeOptions == null || chromeOptions.equals(""))) {
				dataAttributes = appendWithSpace("data-chrome=\"".concat(chromeOptions).concat("\""), dataAttributes);
			}
			if (width != null && width != 0) {
				dataAttributes = appendWithSpace("width=\"".concat(width.toString()).concat("\""), dataAttributes);
			}
			if (height != null && height != 0) {
				dataAttributes = appendWithSpace("height=\"".concat(height.toString()).concat("\""), dataAttributes);
			}
			
			this.dataAttributes = dataAttributes;
			
		} catch (Exception e) {
			log.error("Error setting Data Attributes", e);
		}
		
	}
	
	public String getLinkClass() {
		return LINK_CLASS;
	}
	
	private void setChromeOptions() {
		
		try {
			chromeOptions = "";
			
			if (!header) {
				chromeOptions = chromeOptions.concat("noheader");
			}
			if (!footer) {
				chromeOptions = appendWithSpace("nofooter",chromeOptions);
			}
			if (transparent) {
				chromeOptions = appendWithSpace("transparent",chromeOptions);
			}
		} catch (Exception e) {
			log.error("Error setting Chrome Options", e);
		}
	}
	
	public String getChromeOptions() {
		return chromeOptions;
	}

	/* A helper function to append strings, adding spaces if the string to append to starts off not empty */
	private String appendWithSpace(String toAppend, String appendTo) {
		
		try {
			
			if (!appendTo.equals("")) {
				appendTo = appendTo.concat(" ");
			}
			appendTo = appendTo.concat(toAppend);	
			
		} catch (Exception e) {
			log.error("Error in appending helper function",e);
		}
		
		return appendTo;
		
	}
	
	
}