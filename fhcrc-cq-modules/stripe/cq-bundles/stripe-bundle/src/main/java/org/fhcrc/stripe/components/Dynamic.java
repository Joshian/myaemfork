
/**
 * Dynamic class is in charge of handling dynamic component properties and logic
 *  
 */
package org.fhcrc.stripe.components;

import java.util.ArrayList;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

public class Dynamic {

	private final static String DEFAULT_LABEL = "Section Label";
	
	private ValueMap componentProperties;
	
	private ResourceResolver resourceResolver;
	
	private ArrayList<Section> sections;
	
	private int slideNumber;
	private Float pointerPos;
	
	/** Constructor of the class
	 */
	public Dynamic() {
		this.sections = new ArrayList<Section>();
	}	
	
	
	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;	
		slideNumber = Integer.valueOf((String)componentProperties.get("section_number","0")).intValue();
		
		switch(slideNumber){
			case 1: pointerPos = 456f; break;
			case 2: pointerPos = 274f; break;
			case 3: pointerPos = 175f; break;
			case 4: pointerPos = 116f; break;
			case 5: pointerPos = 77f; break;
			case 6: pointerPos = 69.5f; break;
		}
		
		for(int i = 1; i <= slideNumber; i++){
			String sectionLabel = (String)properties.get("section_" + i + "_label",DEFAULT_LABEL);
			String sectionImage = (String)properties.get("section_" + i + "_image","");
			String sectionThumbnail = (String)properties.get("section_" + i + "_thumb","");
			Section section = new Section (sectionImage,sectionLabel, sectionThumbnail);
			this.sections.add(section);
		}
		
	}

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}


	public ArrayList<Section> getSections() {
		return sections;
	}


	public void setSections(ArrayList<Section> sections) {
		this.sections = sections;
	}
	

	public int getSlideNumber() {
		return slideNumber;
	}


	public void setSlideNumber(int slideNumber) {
		this.slideNumber = slideNumber;
	}

	public Float getPointerPos() {
		return pointerPos;
	}


	public void setPointerPos(Float pointerPos) {
		this.pointerPos = pointerPos;
	}


	public class Section{
		private String imagePath;
		private String imageLabel;
		private String imageThumbnail;
		public Section(String imagePath, String imageLabel, String imageThumbnail){
			this.imagePath = imagePath;
			this.imageLabel = imageLabel;
			this.imageThumbnail = imageThumbnail;
		}
		public String getImagePath() {
			return imagePath;
		}
		public void setImagePath(String imagePath) {
			this.imagePath = imagePath;
		}
		public String getImageLabel() {
			return imageLabel;
		}
		public void setImageLabel(String imageLabel) {
			this.imageLabel = imageLabel;
		}
		public String getImageThumbnail() {
			return imageThumbnail;
		}
		public void setImageThumbnail(String imageThumbnail) {
			this.imageThumbnail = imageThumbnail;
		}	
		
	}
}