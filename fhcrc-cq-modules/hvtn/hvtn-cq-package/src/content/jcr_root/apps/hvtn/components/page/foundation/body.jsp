<%@include file="/libs/foundation/global.jsp"%><%
StringBuffer cls = new StringBuffer();

for (String c: componentContext.getCssClassNames()) {
   	cls.append(c).append(" ");
}
	
if (!properties.get("relatedContent","related-color-0").equals("none")){
	cls.append("with_related_content").append(" ");
	
	if (!properties.get("relatedContent","").trim().equals("")) {
		cls.append(properties.get("relatedContent","")).append(" ");
	}
	
}

if (properties.get("omitSectionNav","false").equals("false")){
	cls.append("with_section_navigation").append(" ");
}

%>

<body class="<%= cls %>">
	<cq:include script="redirect-notice.jsp"/>
	<section>
		<cq:include script="header.jsp"/>
		<cq:include script="content.jsp"/>
		<cq:include script="footer.jsp"/>
	</section>
	<cq:includeClientLib js="apps.hvtn"/>
</body>