<%@include file="/libs/foundation/global.jsp"%>

<%
	/*
    you'll need to add the possible layouts to the gradientcolumns in design
    mode for each template available for author use. use this syntax:
	
	#cols;cssbaseclass<tab>Display Choice Text
	
	e.g.,    
	1;fh-colctrl-lt10   1 column (100%)
	2;fh-colctrl-lt20   2 columns (50% 50%)
	3;fh-colctrl-lt30   3 columns (33% 33% 33%)
	
	refer to components.css for the possible choices for cssbaseclass
	
	*/
%>


<% 
	String colstyles = properties.get("layout","4;cq-colctrl-lt2");
	String[] colArray = colstyles.split(";");
	int num_cols = Integer.parseInt(colArray[0]); // number of columns to iterate over
	String col_base_css = colArray[1];            // the base css class
   
	String colorthemes = properties.get("colortheme","0"); // default is theme 0
	//out.write(colorthemes);
	String[] c = colorthemes.split("\t");
	String color_theme_number = c[0];
	String colorTheme = "";
	if(!color_theme_number.isEmpty()){
		colorTheme = "colorTheme" + color_theme_number;
	}
%>

<div class="fh_panel_grid clearfix <%= colorTheme %>">
 <div class="fh_parsys_column <%= col_base_css %>">
  <%
  for( int i=0; i < num_cols; i++){ 
	 String thisPar = "par" + (i+1); 
	 String thisCss = col_base_css + "-c" + Integer.toString(i);
	 %>
    <div class="fh_parsys_column <%= thisCss %>">
     <div class="fh_section"><cq:include path="<%= thisPar %>" resourceType="foundation/components/parsys"/></div>
    </div>
  <% } %>
 </div>
</div>

<cq:includeClientLib js="public.components.panelGrid"/>