<%@include file="/libs/foundation/global.jsp" %><%
%><%@ page import="java.util.Date,
                   java.text.SimpleDateFormat,
                   com.day.cq.commons.Doctype,
                   org.apache.commons.lang.StringEscapeUtils,
                   org.apache.commons.lang.StringUtils" %><%
String xs = Doctype.isXHTML(request) ? "/" : "";

if(!properties.get("cq:lastModified", "").equals("")) {
    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy HH:mm:ss z");
    String date = sdf.format(properties.get("cq:lastModified", Date.class) );
    %><meta http-equiv="Last-Modified" content="<%= StringEscapeUtils.escapeHtml(date) %>"<%=xs%>><%
}

if(!properties.get("cq:lastModifiedBy", "").equals("")) {
    %><meta name="author" content="<%= StringEscapeUtils.escapeHtml(properties.get("cq:lastModifiedBy", "")) %>"<%=xs%>><%
}

if(!properties.get("jcr:title", "").equals("")) {
    %><meta name="title" content="<%= StringEscapeUtils.escapeHtml(properties.get("jcr:title", "")) %>"<%=xs%>><%
}

if(!properties.get("subtitle", "").equals("")) {
    %><meta name="subtitle" content="<%= StringEscapeUtils.escapeHtml(properties.get("subtitle", "")) %>"<%=xs%>><%
}

if(properties.get("cq:tags", new String[0]).length > 0) {
    %><meta name="tags" content="<%= StringEscapeUtils.escapeHtml( StringUtils.join(properties.get("cq:tags", new String[0]), ",") ) %>"<%=xs%>><%
}


%>
<!-- CSS dependencies -->
<cq:includeClientLib css="apps.citn" />
<!-- 
<link rel="stylesheet" type="text/css" href="<%= currentDesign.getPath() %>/clientlibs/bootstrap/css/bootstrap-responsive.css" media="all" >
<link rel="stylesheet" type="text/css" href="<%= currentDesign.getPath() %>/clientlibs/bootstrap/css/bootstrap-responsive.min.css" media="all" >
<link rel="stylesheet" type="text/css" href="<%= currentDesign.getPath() %>/clientlibs/bootstrap/css/bootstrap.css" media="all" >
<link rel="stylesheet" type="text/css" href="<%= currentDesign.getPath() %>/clientlibs/bootstrap/css/bootstrap.min.css" media="all" >
<link rel="stylesheet" type="text/css" href="<%= currentDesign.getPath() %>/clientlibs/css/static.css" media="all" >     
<link rel="stylesheet" type="text/css" href="<%= currentDesign.getPath() %>/clientlibs/css/mobile.css" media="all"/> 
-->
<!-- /CSS dependencies -->
 