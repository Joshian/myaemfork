var componentApp = (function(componentApp) {
	
	/*---------------- EXPANDER COMPONENT ----------------*/
	/* Publicly accessible methods:
	 * 
	 * expanderInit() - Initializes all expander components on the page
	 * hasExpanders - Boolean. Set to true if there are expanders on the page
	 */
	componentApp.expanderComponent = (function(expanderComponent) {
		
		/* Public variable that gauges whether the initialization function
		 * should be run. */
		
		expanderComponent.hasExpanders = false;
		
		/* expand is created to be the onclick handler for the .title element in
		 * the expander component. It takes the .title element as input and toggles
		 * the "visuallyHidden" class on the appropriate text container and/or
		 * closing line. Returns void. */
		
		var expand = function(el) {
			
			var textContainer, closingLine, domWalker;
			domWalker = el;
			
			/* Run through the DOM starting at the <h2> to find the text
			 * container and/or the closing line */
			
			while (domWalker.nextSibling) {
				domWalker = domWalker.nextSibling;
				
				/* Some browsers include textNodes as siblings, so make sure we
				 * are dealing with an elementNode */
				
				if (domWalker.nodeType === 1) {
					
					if (domWalker.classList.contains("textContainer")) {
						
						textContainer = domWalker;
						
					} else if (domWalker.classList.contains("closingLine")) {
						
						closingLine = domWalker;
						
					}
					
				}
			}
			
			if (textContainer !== undefined) {
				
				textContainer.classList.toggle("visuallyHidden");
				
			}
			if (closingLine !== undefined) {
				
				closingLine.classList.toggle("visuallyHidden");
				
			}
			
			/* Send the .title element to the expanderIconToggle function to 
			 * change the + to a - or vice versa */
			
			expanderIconToggle(el);
			
		};
		
		/* expanderIconToggle takes in an element (.title) and searches its
		 * children for an element with class="expanderIcon". It then toggles 
		 * the innerHTML of that element and its data-status attribute. 
		 * Returns void */
		
		var expanderIconToggle = function(el) {
			
			var iconNode = el.querySelector(".expanderIcon")
			
			if (iconNode.dataset.status === "closed") {
				
				iconNode.innerHTML = "&ndash;";
				iconNode.dataset.status = "open";
				
			} else {
				
				iconNode.innerHTML = "+";
				iconNode.dataset.status = "closed";
				
			}
			
		};
		
		/* Initialization function. Hides all text/closing line elements that 
		 * should be hidden at page load and adds all expanderIcon spans to the
		 * <h2> of each expander component. Returns void */
		
		expanderComponent.expanderInit = function() {
			
			/* If the author has declared that an expander component be open
			 * by default, make sure you don't include those ones. The ones that
			 * are open by default have data-default-status="open" on the
			 * expanderContainer div */
			
			var expanderTexts = document.querySelectorAll(".expanderContainer[data-default-status=\"closed\"] > .textContainer");
			var expanderHeaders = document.querySelectorAll(".expanderContainer .title");
			var closingLines = document.querySelectorAll(".expanderContainer[data-default-status=\"closed\"] .closingLine");
			
			/* Set isInitialized to true so that we don't accidentally run this
			 * function multiple times if there are multiple expander components
			 * on one page */
			
			isInitialized = true;
			
			for (var i = 0; i < expanderTexts.length; i++) {
				
				expanderTexts[i].classList.add("visuallyHidden");
				
			}
			
			for (var i = 0; i < closingLines.length; i++) {
				
				closingLines[i].classList.add("visuallyHidden");
				
			}
			
			for (var i = 0; i < expanderHeaders.length; i++) {
				
				var theHeader = expanderHeaders[i],
				theIcon = theHeader.querySelector(".expanderIcon");
				
				theHeader.querySelector(".iconContainer").style.display = "table-cell";
			
				if (theHeader.parentNode.dataset.defaultStatus !== theIcon.dataset.status) {
				
					expanderIconToggle(theHeader);
				
				} 
						
				theHeader.addEventListener("click", function() {
					expand(this);
				}, false);
				
			}
			
		}
		
		return expanderComponent;
	
	})(componentApp.expanderComponent || {});
	
	return componentApp;
	
})(componentApp || {});