<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode,
    org.fhcrc.tools.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%>

<%
  String mode = properties.get("mode","default"); // whether this is the default or the "banner" mode
  Image image = new Image(resource, "image");
  String linkTarget = properties.get("linkTarget","");
  String linkTargetEscaped;
%>

<% if(mode.equals("default") || (!mode.equals("default")&&!image.hasContent())){%>

  <cq:include script="/apps/public/components/page/homepage/actionbar.jsp"/>

<% } else {

  Resource r;     
  if(!linkTarget.equals("")){
      linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
  }

  linkTargetEscaped = org.apache.commons.lang.StringEscapeUtils.escapeJavaScript(linkTarget);  
  
  if (image.hasContent()) {
	  image = FHUtilityFunctions.nukeURLPrefix(image, resourceResolver);
  }
  
  //remove title attribute unless there is something explicitly in the dialog
  if(properties.get("image/jcr:title","").trim().equals("")) {
      image.setTitle("");
  }  
  image.addAttribute("alt", "");
  image.loadStyleData(currentStyle);
  image.setSelector(".img"); // use image script
  image.setDoctype(Doctype.fromRequest(request));
  // add design information if not default (i.e. for reference paras)
  if (!currentDesign.equals(resourceDesign)) {
      image.setSuffix(currentDesign.getId());
  }  %>
  
  <div id="hero_actionbar" class="hero_actionbar bannermode clearfix"><div class="sizer"><% if(!linkTarget.equals("")){ 
     %><a href="<%=linkTarget%>"><% image.draw(out); %></a><script type="text/javascript">
     $(document).ready( function(){
         $("#hero_actionbar a").click( function(){ 
             var linkTarget = $(this).attr('href');
             _gaq.push(['_trackEvent','Home Page Click Tracking','Action Bar Banner',linkTarget]);
              });
         });
     </script><% } else {  image.draw(out); } %></div></div>

<% } %>



