<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode,
    org.fhcrc.stripe.functions.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
    Image image = new Image(resource, "image");
    String linkTarget = properties.get("linkTarget","");
 	String shadowAffect = properties.get("shadowAffect","outer");
 	boolean hasShadow = Boolean.parseBoolean(properties.get("showShadow","false"));
 	
 	String shadowClass = "";
	if (hasShadow) {
		
		if(shadowAffect.equals("outer")) {
			
			shadowClass = "shadowed";
			
		} else {
			
			shadowClass = "inner_shadowed";
			
		}
		
	}
    
    Resource r;
    if(!linkTarget.trim().equals("")) {
        linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
    }

//drop target css class = dd prefix + name of the drop target in the edit config
    image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
    image.loadStyleData(currentStyle);
    image.setSelector(".img"); // use image script
    image.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        image.setSuffix(currentDesign.getId());
    }
    %>

    <%
    String addClass = (!image.hasContent() && !(WCMMode.fromRequest(request) == WCMMode.EDIT)) ? " noimage" : ""; // add a class if there is no image, css can hide it.
    %>
     
        
    <div class="fh_textimage clearfix<%=addClass%>">
    
    <%
    
    if(image.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT){
    	
    	%>
    	<div class="textimage_image imageplus<%
                if(properties.get("imageAlign","left").equals("right")){
                    out.print(" imageAlignRight");
                }
        %>" <%
    	
    	if(image.hasContent()) {
	        //--- RB ------------------------------------------------------------------------
	        // Removed the original method of getting image width and replaced it with
	        // the one in the out.print() line.  Seems to have improved performance greatly.
	        //-------------------------------------------------------------------------------
	        //int imgWidth = image.getLayer(true,true,true).getWidth();
	        if(image.get(Image.PN_WIDTH) == null || image.get(Image.PN_WIDTH).trim().equals("")) {
	            int styleWidth = image.getLayer(true,true,true).getWidth();
	            out.print("style=\"width:" + styleWidth + "px\"");
	            log.warn("No Image.PN_WIDTH. StyleWidth = " + Integer.toString(styleWidth));
	        } else {
	            int styleWidth = Integer.parseInt(image.get(Image.PN_WIDTH));
	            out.print("style=\"width:" + styleWidth + "px\"");
	            log.info("StyleWidth = " + Integer.toString(styleWidth));
	        }
	        // log.info(image.get(Image.PN_WIDTH));
	        
	        image = FHUtilityFunctions.nukeURLPrefix(image, resourceResolver);
    	}
        
    %>
    
        ><dl><dt class="<%= shadowClass %>">    
        <% 
        if(!linkTarget.equals("")){ %>
            <a href="<%= linkTarget %>" <%= properties.get("externalLink","false").equals("true") ? "target=\"_blank\"" : "" %>>
        <% }
        image.draw(out);
        if(!linkTarget.equals("")){ %></a><% }%>
        </dt>
        <dd>
        <cq:text property="jcr:caption" tagName="small" tagClass="caption" placeholder="" />
        </dd></dl></div>
        
    <%
    } /* END OF if(image.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT) STATEMENT*/
    %>
        <cq:text property="text" tagName="div" tagClass="text" />
    </div>