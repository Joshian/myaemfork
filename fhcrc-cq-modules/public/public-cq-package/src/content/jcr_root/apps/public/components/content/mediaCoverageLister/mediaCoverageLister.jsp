<%@include file="/libs/foundation/global.jsp"%><%@ page import="com.day.cq.wcm.api.WCMMode, java.util.Iterator, java.lang.Integer" %>
<div class="mediaCoverageList">
<%
Integer maxNum = new Integer(properties.get("displayNumber",3));
Integer countUp = new Integer(0);
/* Null pointer check: do we have a Media Coverage page? */ 
if(resourceResolver.getResource("/content/public/en/news/media-coverage") != null){
    Page mediaPage = resourceResolver.getResource("/content/public/en/news/media-coverage").adaptTo(Page.class);
/* Null pointer check: do we have a Paragraph System on the Media Coverage page? */    
    if(mediaPage.getContentResource("par") != null){
        Iterator<Resource> nodeIterator = mediaPage.getContentResource("par").listChildren();
/* Loop through the elements in the par until we hit the last one or get the maximum number of media coverage items */        
        while(nodeIterator.hasNext() && !countUp.equals(maxNum)){
            Resource theNode = nodeIterator.next();
            if(theNode.getResourceType().equals("public/components/content/mediaCoverageItem")){
/* Check to see if this is the first one, and open a UL tag if so */
            	if(countUp==0){
%>
                    <ul>
<%
            	}
%>
                <li><sling:include path="<%= theNode.getPath() %>" /></li>
<%
                countUp++;
/* Check to see if that was the last one, and close the UL tag if so */
                if(countUp.equals(maxNum) || nodeIterator.hasNext() == false){
%>
                    </ul>
<%
                }
            }
        }
    } else {
/* The media coverage page's parsys is returning a null pointer. Include a script to display this error. */
%> <cq:include script="noPar.jsp" /><%
    }
} else {
/* The media coverage page is returning a null pointer. Include a script to display this error. */
%> <cq:include script="noPage.jsp" /><%
}
%>
</div>