<%@include file="/libs/foundation/global.jsp"%><%@ page import="
                 java.net.URLEncoder,com.day.cq.commons.Doctype,
    com.day.cq.wcm.foundation.Image" %>
<%
Image overrideImage = new Image(resource, "image");
%>



<% 
String disease = properties.get("disease","");
String custom_disease_name = properties.get("custom_disease_name","");
String custom_search_text = properties.get("custom_search_text","");

String diseaseText = "";
String diseaseLink = "";
if(disease.equals("")){
%>
<cq:include script="empty.jsp" />
<%  
} else {
if(disease.equals("ALL")){
    diseaseText = "Acute Lymphoblastic Leukemia";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.1.html";
} else if(disease.equals("AML")){
    diseaseText = "Acute Myeloid Leukemia";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.2.html";
} else if(disease.equals("Brain Tumors")){
    diseaseText = "Brain Tumors";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.12.html";
} else if(disease.equals("Breast Cancer")){
    diseaseText = "Breast Cancer";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.13.html";
} else if(disease.equals("Cervical Cancer")){
    diseaseText = "Cervical Cancer";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.17.html";
} else if(disease.equals("CLL")){
    diseaseText = "Chronic Lymphoblastic Leukemia";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.18.html";
} else if(disease.equals("CML")){
    diseaseText = "Chronic Myeloid Leukemia";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.19.html";
} else if(disease.equals("Colorectal Cancer")){
    diseaseText = "Colorectal Cancer";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.20.html";
} else if(disease.equals("Crohns")){
    diseaseText = "Crohn's Disease";
    diseaseLink = "http://studies.fhcrc.org/cats/";
} else if(disease.equals("Cystic Fibrosis")){
    diseaseText = "NO TREATMENT";
    diseaseLink = "";
} else if(disease.equals("Esophageal Cancer")){
    diseaseText = "NO TREATMENT";
    diseaseLink = "";
} else if(disease.equals("HIV")){
    diseaseText = "NO TREATMENT";
    diseaseLink = "";
} else if(disease.equals("Liver Cancer")){
    diseaseText = "Liver Cancer";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.45.html";
} else if(disease.equals("Lung Cancer")){
    diseaseText = "Lung Cancer";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.46.html";
} else if(disease.equals("Lupus")){
    diseaseText = "NO TREATMENT";
    diseaseLink = "";
} else if(disease.equals("Lymphoma - Hodgkins")){
    diseaseText = "Hodgkin Lymphoma";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.38.html";
} else if(disease.equals("Lymphoma - Non-Hodgkins")){
    diseaseText = "Non-Hodgkin Lymphoma";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.64.html";
} else if(disease.equals("MDS")){
    diseaseText = "Myelodysplastic Syndrome";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.61.html";
} else if(disease.equals("Melanoma")){
    diseaseText = "Melanoma";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.52.html";
} else if(disease.equals("Multiple Myeloma")){
    diseaseText = "Multiple Myeloma";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.57.html";
} else if(disease.equals("Multiple Sclerosis")){
    diseaseText = "NO TREATMENT";
    diseaseLink = "";
} else if(disease.equals("Oral Cancer")){
    diseaseText = "Oral Cancer";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.44.html";
} else if(disease.equals("Ovarian Cancer")){
    diseaseText = "Ovarian Cancer";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.65.html";
} else if(disease.equals("Pancreatic Cancer")){
    diseaseText = "Pancreatic Cancer";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.66.html";
} else if(disease.equals("Prostate Cancer")){
    diseaseText = "Prostate Cancer";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.71.html";
} else if(disease.equals("Rheumatoid Arthritis")){
    diseaseText = "NO TREATMENT";
    diseaseLink = "";
} else if(disease.equals("Sarcoma")){
    diseaseText = "Sarcoma";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.77.html";
} else if(disease.equals("Scleroderma")){
    diseaseText = "Scleroderma";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.78.html";
} else if(disease.equals("Wilms Tumor")){
    diseaseText = "Wilms Tumor";
    diseaseLink = "/content/public/en/treatment/clinical-trials/list.95.html";
} else if(disease.equals("CUSTOM")){
    diseaseText = custom_disease_name;
    diseaseLink = "/content/public/en/treatment/clinical-trials/search.html?atn=search&amp;search_type=text&amp;searchTerms=" + URLEncoder.encode(custom_search_text.trim(),"UTF-8");
}

if(diseaseText.equals("NO TREATMENT")){
%>
<cq:include script="noTrial.jsp" />
<%
} else {
%>
<div class="diseaserelated">

	<a href="<%= diseaseLink %>">
		<%
		if(overrideImage.hasContent()){
			/* Design no longer has frame class as default, so removing */
//		    overrideImage.addCssClass("frame");
		    overrideImage.addAttribute("width","174px");
		    overrideImage.loadStyleData(currentStyle);
		    overrideImage.setSelector(".img"); // use image script
		    overrideImage.setDoctype(Doctype.fromRequest(request));
		    // add design information if not default (i.e. for reference paras)
		    if (!currentDesign.equals(resourceDesign)) {
		        overrideImage.setSuffix(currentDesign.getId());
		    }
		    overrideImage.draw(out);
		} else {
		%>    
		    <img src="<%= currentDesign.getPath() %>/img/defaults/clinical_trials.jpg" alt="Clinical trials unit patient lounge" />
		<% } %>    
	</a>

    <p>Clinical trials are vital to the development of innovative treatments for <%= diseaseText %>.</p>
    <a href="<%= diseaseLink %>"><img class="rolloverImage" src="/etc/designs/public/img/buttons/button_sm_find_clinical_trial_stat.gif" data-hover="/etc/designs/public/img/buttons/button_sm_find_clinical_trial_over.gif" alt="Find a Clinical Trial" /></a>
</div>
<%
}
}
%>