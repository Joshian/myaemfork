<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="java.util.Iterator,
        com.day.text.Text,
        com.day.cq.wcm.api.PageFilter,
        com.day.cq.wcm.api.NameConstants,
        org.apache.commons.lang.StringEscapeUtils,
        com.day.cq.wcm.api.Page,
        org.fhcrc.publicsite.constants.Constants,
        org.fhcrc.tools.FHUtilityFunctions" %>
<div id="breadcrumbs" class="breadcrumbs"><ul>
<%

    // get starting point of trail
    int level = 2;
    int currentLevel = currentPage.getDepth() - 1;
    String currentTitle = FHUtilityFunctions.displayTitle(currentPage);

    while (level < currentLevel) {
        Page trail = currentPage.getAbsoluteParent(level);
        if (trail == null) {
            break;
        }
        /* Certain pages should be skipped in the breadcrumbs, so we skip them here. */
        /* News articles are separated by year and month, but those year/month pages have no content, so skip them */
        if (trail.getPath().contains(Constants.PATH_TO_NEWSRELEASES_FOLDER) || trail.getPath().contains(Constants.PATH_TO_CENTERNEWS_FOLDER)) {
            if (trail.getName().matches("\\d{4}") || trail.getName().matches("\\d{2}")) {
            	level++;
            	continue;
            }
        }
        /* Starting 10.14.2014, authors can editorially skip any page in breadcrumbs using the omitFromBreadcrumbs flag. 
         * Check for that flag here. */
        if (trail.getProperties().get("omitFromBreadcrumbs","false").equals("true")) {
        	level++;
        	continue;
        }
        String title = FHUtilityFunctions.displayTitle(trail);
        %><li><a href="<%= Text.escape(trail.getPath(), '%', true) %>.html"><%
        %><%= StringEscapeUtils.escapeHtml(title) %><%
        %></a></li><%

        level++;
    }
%>
<li><%= currentTitle %></li>
</ul><!-- <br class="clear"/>--></div>