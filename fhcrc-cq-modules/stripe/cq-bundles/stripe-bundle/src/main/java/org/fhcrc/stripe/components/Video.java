
/**
 * Footer class is in charge of handling footer properties and logic
 *  
 */
package org.fhcrc.stripe.components;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import javax.servlet.*;

public class Video {
	
	private final static String PATH_PROPERTY = "videoPath";
	private final static String WIDTH_PROPERTY = "width";
	private final static String HEIGHT_PROPERTY = "height";
	private final static String OMIT_RELATED_PROPERTY = "omitRelated";
	private final static String SHADOWED_PROPERTY = "shadowed";
	private final static String SHADOWED_DEFAULT = "false";
	private final static String OMIT_RELATED_DEFAULT = "false";
	private final static String WIDTH_PROPERTY_DEFAULT = "560px"; // Default YouTube video size
	private final static String HEIGHT_PROPERTY_DEFAULT = "315px";

	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	private ServletRequest request;
	
	private String path;
	private String width;
	private String height;
	private String defaultMessage = "Please configure the component.";
	private Boolean omitRelated;
	private Boolean shadowed;
	
	/** Adds the "rel=0" to the end of a YouTube link
	 * 
	 * @param String thePath
	 * @return String thePath
	 */
	private String addOmitRelated(String thePath) {
		
		if (thePath.contains("youtube") || thePath.contains("youtu.be")) {
			
			if (thePath.contains("?")) {
				
				thePath = thePath.concat("&rel=0");
				
			} else {
				
				thePath = thePath.concat("?rel=0");
				
			}
			
		}
		
		return thePath;
	}
	
	/** If the current page is being requested via https, this changes the video URL to https as well. This is to avoid a mixed content
	 *  error in Firefox.
	 *  
	 * @param String thePath
	 * @return String thePath 
	 */
	private String normalizeURL(String thePath) {
		
		if (request.getScheme().toLowerCase().equals("https")) {
			
			thePath = thePath.replaceAll("http:", "https:");
			
		}
		
		return thePath;
	}

	/** Constructor of the class
	 * TODO: Logic in constructor
	 * @param properties the page properties
	 * @param resourceResolver the resource resolver
	 */
	public Video() {
	}	

	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
        this.path = componentProperties.get(PATH_PROPERTY, String.class);
        this.width = componentProperties.get(WIDTH_PROPERTY, WIDTH_PROPERTY_DEFAULT);
        this.height = componentProperties.get(HEIGHT_PROPERTY, HEIGHT_PROPERTY_DEFAULT);
        String omitString = componentProperties.get(OMIT_RELATED_PROPERTY, OMIT_RELATED_DEFAULT);
        String shadowedString = componentProperties.get(SHADOWED_PROPERTY, SHADOWED_DEFAULT);
        
        if (path != null && !path.trim().equals("")) {
        	if (omitString.equals("true")) {
        		setPath(addOmitRelated(this.path));
        		this.omitRelated = true;
        	} else {
        		this.omitRelated = false;
        	}
        	if (shadowedString.equals("true")) {
        		this.shadowed = true;
        	} else {
        		this.shadowed = false;
        	}
        	if (request != null) {
        		setPath(normalizeURL(this.path));
        	}
        }
        
	}

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public ServletRequest getRequest() {
		return request;
	}
	
	public void setRequest(ServletRequest request) {
		this.request = request;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}
	
	public Boolean getOmitRelated() {
		return omitRelated;
	}

	public void setOmitRelated(Boolean omitRelated) {
		this.omitRelated = omitRelated;
	}

	public Boolean getShadowed() {
		return shadowed;
	}

	public void setShadowed(Boolean shadowed) {
		this.shadowed = shadowed;
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}

	public void setDefaultMessage(String defaultMessage) {
		this.defaultMessage = defaultMessage;
	}
	
	

	
}
