<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text, java.util.Iterator,
    com.day.cq.wcm.api.WCMMode,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.commons.Doctype" %>
 <!-- page content -->
 <div id="fh_body" class="fh_body container_12 clearfix"><a name="content"></a>

  <div class="grid_12">

    <!-- breadcrumbs appear in the content flow in case they wrap to two lines -->
    <cq:include script="breadcrumbs.jsp"/>

    <cq:include script="clusterTitle.jsp" />   
    
  </div>  
    
  <!-- content column -->
  <div id="fh_body_content" class="fh_body_content grid_9 push_3">  

    <% //log.info(currentPage.getProperties().get("./isClusterHead","--not set--"));
       // if this is a cluster head, then do not display a field for title, since the cluster title will take on that role
       if(!properties.get("isClusterHead",Boolean.FALSE)){ %>
       <cq:include path="title" resourceType="foundation/components/title"/>
    <% } %>
    
    <!-- event main page has a top-level image first -->
    <div class="eventmain-banner"><cq:include path="eventmainbanner" resourceType="foundation/components/parsys"/></div>
    
    
    <!-- followed by a two-column layout -->
    <div class="clear"></div>
    <div class="eventmain-leftcol forceSquareBullet grid_6 alpha">
      <cq:include path="title" resourceType="foundation/components/title"/>
      <cq:include path="leftcol" resourceType="foundation/components/parsys"/></div>
    <div class="eventmain-rightcol grid_3 omega"><cq:include path="rightcol" resourceType="foundation/components/parsys"/></div>  
    
    <!--  handle color styling of the title of this page -->
    <% String themecolor = properties.get("themecolor","");
       if(!"".equals(themecolor)){ %>
        <script type="text/javascript">
         $(".eventmain-leftcol h1").css("color","#<%=themecolor%>");
        </script>
    <% } %>
  
    <!--  print out the banner image for fun (will appear on sub pages, not this page): -->
    <%
    Image b = new Image(resource, "bannerimage" );
    b.loadStyleData(currentStyle);
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        b.setSuffix(currentDesign.getId());
    }
    //drop target css class = dd prefix + name of the drop target in the edit config
    b.addCssClass(DropTarget.CSS_CLASS_PREFIX + "bannerimage");
    /* Design no longer has bordered images as default */
//  b.addCssClass("frame");
    b.setSelector(".img");
    b.setDoctype(Doctype.fromRequest(request));
    //b.draw(out);    
    %> 
     
  </div>
  <!-- /content column -->

    

  <!-- left column (sidebar) -->
  <div id="fh_sidebar" class="fh_sidebar grid_3 pull_9"><div id="fh_sidebar_content" class="fh_sidebar_content">
    
    <cq:include script="clusterNav_page.jsp" />    
    
    <!-- protected parsys: -->
    <div class="relatedcontentarea">
      <cq:include path="parRelated" resourceType="public/components/content/protectedparsys"/>
    </div>
    
    <cq:include script="related-content.jsp" />
  
   </div><!-- /fh_sidebar_content -->
  
  </div>
  <!-- /left column -->
 
 </div><!-- /fh_body -->
