<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.api.WCMMode, 
                 java.util.*,
                 java.lang.Integer, 
                 org.apache.sling.api.resource.ValueMap,
                 java.text.SimpleDateFormat,
                 java.text.DateFormat" %>

<%
Integer maxNum = new Integer(properties.get("displayNumber",3));
Integer countUp = new Integer(0);
String pagePath = properties.get("pagePath","");
DateFormat g = new SimpleDateFormat("MM/dd/yy");
DateFormat f = new SimpleDateFormat("EEEE, MMMM d");
Date theCurrentDate = g.parse(g.format(Calendar.getInstance().getTime()));

Comparator<ValueMap> dateCompare = new Comparator<ValueMap>() {
    public int compare(ValueMap a, ValueMap b){
    	DateFormat g = new SimpleDateFormat("MM/dd/yy");
    	String dateStringA = a.get("date","");
    	String dateStringB = b.get("date","");
    	
    	try {
    	    Date dateA = g.parse(dateStringA);
    	    Date dateB = dateStringB.equals("") ? Calendar.getInstance().getTime() : g.parse(b.get("date", Calendar.getInstance().getTime().toString()));
    	    
    	    if(dateA.equals(dateB)){
                return a.get("title","A").compareTo(b.get("title","B"));
            }
            return dateA.compareTo(dateB);
            
    	} catch (Exception e) {
    		return a.get("title","A").compareTo(b.get("title","B"));
    	}
        
    }
};


/* Null pointer check: do we have an Events page? */ 
if(!pagePath.equals("") && resourceResolver.getResource(pagePath) != null){
    Page eventsPage = resourceResolver.getResource(pagePath).adaptTo(Page.class);
/* Null pointer check: do we have a Paragraph System on the Events page? */    
    if(eventsPage.getContentResource("par") != null){
        Iterator<Resource> nodeIterator = eventsPage.getContentResource("par").listChildren();
        /* Just grab the event items, as I need to sort them by date */
        ArrayList<ValueMap> justTheEvents = new ArrayList<ValueMap>();
        while(nodeIterator.hasNext()) {
        	Resource theNode = nodeIterator.next();
        	if(theNode.getResourceType().equals("public/components/content/VIDDEventItem")){
        		justTheEvents.add(theNode.adaptTo(ValueMap.class));
        	}
        }
        
        ValueMap[] eventsArray = new ValueMap[justTheEvents.size()];
        eventsArray = justTheEvents.toArray(eventsArray);
/*        
        for(int i = 0; i < eventsArray.length; i++) {
        	out.print("Date: ".concat(eventsArray[i].get("date","NO DATE")).concat(" || "));
        }
*/        
        Arrays.sort(eventsArray, dateCompare);
        
        
/* Loop through the elements in the par until we hit the last one or get the maximum number of media coverage items */        
        for (int eventLoop = 0; eventLoop < eventsArray.length; eventLoop++) {

        	ValueMap theMap = eventsArray[eventLoop];
        	if (g.parse(theMap.get("date","")).before(theCurrentDate)) {
        		continue;
        	}
           	String override = theMap.get("categoryOverride","");
           	String category = theMap.get("category","");
           	String categoryDisplay = "";
           	if (!override.equals("")) {
           		categoryDisplay = override;
           	} else if (!category.equals("")) {
           		categoryDisplay = category;
           	}
            	
%>

<div class="eventListItem">

    <p>
    <%= categoryDisplay.equals("") ? "" : "<strong>".concat(categoryDisplay).concat("</strong><br />") %>
    <%= theMap.get("title","").equals("") ? "" : "<a href=\"".concat(pagePath).concat(".html\">").concat(theMap.get("title","")).concat("</a>") %><br />
    <%= f.format(g.parse(theMap.get("date",""))) %><br />
    <%= theMap.get("alert","").equals("") ? "" : "<span class=\"alertText\">".concat(theMap.get("alert","")).concat("</span>") %>
    </p>

</div>

<%          
            countUp++;
            /* If we just printed the maximum number of items, break the loop */
            if (countUp.equals(maxNum)) {
                break;
            }
            	
        }

    } else {
/* The media coverage page's parsys is returning a null pointer. Include a script to display this error. */
%> <cq:include script="noPar.jsp" /><%
    }
} else {
/* The media coverage page is returning a null pointer. Include a script to display this error. */
%> <cq:include script="noPage.jsp" /><%
}
%>