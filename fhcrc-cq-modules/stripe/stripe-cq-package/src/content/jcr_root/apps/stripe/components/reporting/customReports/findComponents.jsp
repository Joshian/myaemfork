<%@include file="/libs/foundation/global.jsp"%>
<%
String componentType = slingRequest.getParameter("componentType");
if(componentType == null || componentType.trim().equals("")) {
%>
<p>Temporary Error Message</p>
<%
} else if(componentType.equals("button")) {
%>
<cq:include script="componentFinders/button.jsp" />
<%
} else if(componentType.equals("carousel")) {
%>
<cq:include script="componentFinders/carousel.jsp" />
<%
} else if(componentType.equals("header")) {
%>
<cq:include script="componentFinders/header.jsp" />
<%
} else if(componentType.equals("image")) {
%>
<cq:include script="componentFinders/image.jsp" />
<%
} else if(componentType.equals("staffHeadshot")) {
%>
<cq:include script="componentFinders/staffHeadshot.jsp" />
<%
} else if(componentType.equals("video")) {
%>
<cq:include script="componentFinders/video.jsp" />
<%
} 
%>
<script type="text/javascript">
var numResults = document.getElementById('reportResults').rows.length;
numResults -= 1;
var theTable = document.getElementById("reportResults");
var returnedResults = document.createElement("span");
returnedResults.setAttribute("id","numResults");
returnedResults.appendChild(document.createTextNode("Returned " + numResults + " results"));
theTable.parentNode.insertBefore(returnedResults,theTable);
</script>