<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode,
    org.fhcrc.tools.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
    Image image = new Image(resource, "image");
    String linkTarget = properties.get("linkTarget",""),
        caption = properties.get("jcr:caption",""),
        credit = properties.get("credit",""),
        classString = "";
    Boolean hasCaption = false,
        hasCredit = false;
        
    // Check to see if we do have caption/credit. This is important for HTML
    // class assignment later
    
    if (!caption.trim().equals("")) {
        
        hasCaption = true;
        
    }
    
    if (!credit.trim().equals("")) {
        
        hasCredit = true;
        
    }
    
    if (hasCaption) {
        
        if (hasCredit) {
            
            classString = "captionCreditContainer";
            
        } else {
            
            classString = "captionContainer";
            
        }
        
    } else {
        
        if (hasCredit) {
            
            classString = "creditContainer";
            
        }
    }

    // If the linkTarget is an internal page, we need to concat a ".html"
    
    Resource r;     
    if(!linkTarget.equals("")){
        linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
    }
    
    //drop target css class = dd prefix + name of the drop target in the edit config
    image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
    
    if (properties.get("image/jcr:title","").trim().equals("")) {
        image.setTitle(" ");
    }

    image.loadStyleData(currentStyle);
    image.setSelector(".img"); // use image script
    image.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        image.setSuffix(currentDesign.getId());
    }
    
    if (image.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT) {
    
        // Open the div tag, adding the cq-edit-only class if there is no image
        // Add the imageOnly class if there is no caption/credit
    %>
      
        <div class="imageContainer<%= image.hasContent() ? "" : " cq-edit-only" %><%= classString.equals("") ? " imageOnly" : "" %>">
    <%    
        // If there is a link, add in the <a> tag (with target="blank" if necessary
                
        if(!linkTarget.equals("")){ 
        %>
        <a href="<%= linkTarget %>" <%= properties.get("externalLink","false").equals("true") ? "target=\"_blank\"" : "" %>>
        <% 
          image.draw(out);
        %>
        </a>
        <%
        } else {
            // If there was no link, just draw the image
            
            image.draw(out);
        }
        
        // Next is the caption/credit container. First check to see if it is needed,
        // if so, add the classString to its class attribute
        
        if (hasCaption || hasCredit) {
    %>    
    
        <div class="<%= classString %>">
        <% 
          if (hasCaption) {
        %>
          <cq:text property="jcr:caption" tagName="span" tagClass="imageCaption" placeholder="" />
        <%
            if (hasCredit) {
        %>
          <i><span class="imageCredit">(<%= credit %>)</span></i>
        <%
            }
          } else {
            if (hasCredit) {
        %>
          <i><cq:text property="imageCredit" tagName="span" tagClass="caption" /></i>
        <%
            }
          }
        %>
        </div>
    <%
        }
    // Close the imageContainer div
    %>
    </div> 
    <%
    
    // End if (image.hasContent()...)
    }
    
    // Now we need the textContainer
    
    %>
    
    <cq:text property="text" tagName="div" tagClass="textContainer" />