<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.io.IOException,
                 java.net.URLEncoder,
                 javax.xml.parsers.DocumentBuilderFactory,
                 javax.xml.parsers.DocumentBuilder,
                 org.w3c.dom.Document,
                 org.w3c.dom.NodeList,
                 org.w3c.dom.Node,
                 org.w3c.dom.Element,
                 org.xml.sax.SAXException,
                 com.day.cq.wcm.api.WCMMode" %>
<%
Boolean edit_mode = WCMMode.fromRequest(request) == WCMMode.EDIT ? true: false;

// NOTE: In responsive world, you don't want to allow sizes, so I'm omitting
//       the sizefield from this component. The CSS will stretch to fit a 16:9 aspect
//       ratio (or some other ratio you define in the css), and you can also use
//       css to define a maximum width. Beyond that, you should not be asking YouTube/Vimeo
//       for certain sizes. Sld. Jul/2013.

String videoURL = properties.get("videoURL", "");
String videoSource = properties.get("videoSource","");

switch(videoSource) {

   case "youtube":
%>   
       <cq:include script="youtubeVideo.jsp"/>
<%   

       break;

   case "vimeo":
%>   
       <cq:include script="vimeoVideo.jsp"/>
<%   

       break;

   case "facebook":
%>   
       <cq:include script="facebookVideo.jsp"/>
<%   

       break;

/* Default should be YouTube video as that is what most components were before this refactoring - 3/18/16 ICS */
        
   default:
%>
       <cq:include script="youtubeVideo.jsp" />
<% 
}
%>