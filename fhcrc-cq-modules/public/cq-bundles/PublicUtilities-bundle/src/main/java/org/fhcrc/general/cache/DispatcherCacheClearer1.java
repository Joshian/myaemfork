package org.fhcrc.general.cache;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;

@Component(metatype=true, immediate=true, label="FHCRC Scheduled Dispatcher Cache Clearing1", description="Clear Dispatcher Cache on a scheduled basis.")
@Service(value=java.lang.Runnable.class)
@Properties({
    @Property(name="service.description", value="Dispatcher Cache Clearer1"),
    @Property(name="service.vendor", value="FHCRC")
})
public class DispatcherCacheClearer1 extends DispatcherCacheClearer {

	public void run() {
		super.run();
	}
}
