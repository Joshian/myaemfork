<%@include file="/libs/foundation/global.jsp"%>
<jsp:useBean id="slideBean" scope="request" class="org.fhcrc.publicsite.pageComponents.Slide" />
<jsp:setProperty name="slideBean" property="currentPage" value="<%= currentPage %>"/>
<jsp:setProperty name="slideBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="slideBean" property="resource" value="<%= resource %>"/>
<jsp:setProperty name="slideBean" property="currentStyle" value="<%= currentStyle %>"/>
<jsp:setProperty name="slideBean" property="currentDesign" value="<%= currentDesign %>"/>
<jsp:setProperty name="slideBean" property="resourceDesign" value="<%= resourceDesign %>"/>
<jsp:setProperty name="slideBean" property="request" value="<%= request %>"/>
<jsp:setProperty name="slideBean" property="writer" value="<%= out %>"/>
<jsp:setProperty name="slideBean" property="componentProperties" value="<%= properties %>"/>

<!-- TITLE BAR -->

	<div class="titlebar">
  
  	<div class="titlebar_container outer_grid_container table">
    
    	<div class="table_row">
    
        <div class="title_container table_cell">
        
          <h1><c:out value="${ slideBean.slideshowTitle }" /></h1>
        
        </div>
        
        <div class="controls_container table">
        
        	<div class="table_row">
        
            <img id="left_arrow" class="control_arrow table_cell" src="<%= currentDesign.getPath() %>/img/slideshow/left_arrow.png">
            <span class="controls_text table_cell"><span id="current_slide">2</span>/<span id="total_slides">15</span></span>
            <img id="right_arrow table_cell" class="control_arrow" src="<%= currentDesign.getPath() %>/img/slideshow/right_arrow.png">
            
          </div>
        
        </div>
        
        <div class="sharing_container table_cell">
        
        	<!-- Lockerz Share BEGIN -->
          <span class="share_options">
            <span class="a2a_kit a2a_default_style">
              <a class="a2a_button_facebook"></a>
              <a class="a2a_button_twitter"></a>
              <a class="a2a_button_google_plus"></a>          
              <a class="a2a_button_email"></a>
              <a class="a2a_button_printfriendly" onclick="window.print();return false;"></a>
              <span class="a2a_divider"></span>
              <a class="a2a_dd" href="http://www.addtoany.com/share_save">Share</a>
            </span>
          </span>
          <script type="text/javascript">
          <!--
            a2a_config = {};
            a2a_config.onclick=1;
            a2a_config.show_title=0;
            a2a_config.num_services=10;
            /*a2a_config.linkname=document.title;
            a2a_config.linkurl=location.href;*/
            a2a_config.prioritize=["facebook","twitter","linkedin","digg","delicious","myspace","read_it_later","squidoo","technorati_favorites","care2_news"];
            /*a2a_config.color_main="D7E5ED";*/
            /*a2a_config.color_border="AECADB";*/
            a2a_config.color_link_text="276a9f";
            a2a_config.color_link_text_hover="348dd1";
          //-->
          </script>
          <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
          <!-- Lockerz Share END -->
        
        </div>
        
      </div>
    
    </div>
    
  </div>
  
<!-- END TITLE BAR -->