<%@include file="/apps/stripe/global.jsp" %>

<body>   
    <cq:include script="header.jsp"/>
    <cq:include script="content.jsp"/>
    <div id="footer">
        <cq:include script="footer.jsp"/>
    </div>    
    <cq:includeClientLib js="apps.nwbiotrust" />  
    <cq:include script="overlay.jsp"/>
</body>
