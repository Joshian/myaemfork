<%@include file="/libs/foundation/global.jsp" %>
 <!-- page content -->
 <div id="fh_body" class="fh_body ar_homepage container_12 clearfix"><a name="content"></a>

  <div class="grid_12">

    <!-- breadcrumbs appear in the content flow in case they wrap to two lines -->
    <cq:include script="breadcrumbs.jsp"/>

    <!--  mobile menu -->
    <cq:include script="mobile-menu.jsp"/>  

    <!-- all pages have the cluster title -->
    <cq:include script="clusterTitle.jsp" /> 
      
  </div>
    
  <!-- content column -->
  <div id="fh_body_content" class="fh_body_content grid_12">
    <cq:include path="hpslider" resourceType="public/components/content/omnislider"/>
    
    <div class="ar_text">
      <cq:include path="hptext" resourceType="shared/components/content/textimage"/>
    </div>
    
    <div class="ar_panels">
      <cq:include path="panel_01" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_02" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_03" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_04" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_05" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_06" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_07" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_08" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_09" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_10" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_11" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_12" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_13" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_14" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_15" resourceType="shared/components/content/imageWithTextOverlay"/>
      <cq:include path="panel_16" resourceType="shared/components/content/imageWithTextOverlay"/>
      <div class="clear"></div>
    </div>
    
    <!-- three "action" tabs -->   
    <div class="ar_actionbuttons">
     <ul>
       <li class="financial"><div class="CSSButton"><a class="button colorTheme0" href="<%=currentPage.getPath()%>/financial-summary.html"><span class="piechart"></span>Financial Summary&nbsp;&gt;</a></div></li>
       <li class="benefactors"><div class="CSSButton"><a class="button colorTheme2" href="<%=currentPage.getPath()%>/benefactors.html">Our Benefactors&nbsp;&gt;</a></div></li>
       <li class="donatenow gradient">
         <a class="actiontext" href="<%=currentPage.getPath()%>/donate.html" target="_blank"><span class="label droptext"><cq:text property="subtitle"/></span></a>
         <div class="CSSButton"><a target="_blank" href="<%=currentPage.getPath()%>/donate.html" class="button colorTheme0">Donate Now&nbsp;&gt;</a></div>
       </li>
    </ul>
    <div class='clear'></div>
   </div>
    
    
    <!-- remote-wrap-api-marker -->
    <div class="ar_footer">
      <cq:include script="social-options.jsp" />
    </div>
    <span class="ar_credits"><a href="<%= currentPage.getPath() %>/credits.html">CREDITS</a></span>
    <div class="ar_archive">
      <cq:include path="archivetext" resourceType="shared/components/content/textComponent"/>
    </div>
    <div class="clear"></div>
  </div>
  <!-- /content column -->
    
  <br class="clear"/>
   
 </div><!-- /fh_body, page content -->
