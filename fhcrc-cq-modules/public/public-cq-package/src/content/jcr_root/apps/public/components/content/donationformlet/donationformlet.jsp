<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.fhcrc.tools.FHUtilityFunctions,
                 org.fhcrc.publicsite.constants.Constants,
    			 org.apache.commons.httpclient.*, 
                 org.apache.commons.httpclient.methods.*,
                 org.apache.commons.httpclient.params.HttpMethodParams,
                 java.io.IOException" %>
<%

  // This component runs in one of two modes.

    // Mode 1. Standard ("with suggested donation")
    // This mode was our original incarnation, where the user sees a fillout form with suggested gift value, they enter what they
    // want, hit the button, and go to the Fred Hutch main donation form (a Convio donation form). In this mode, the author
    // need only fill in requested fields, including source and subsource values.

    // One additional, fun complication is that Design/UX also wanted to be able to run in Mode 1 but send the user to ANY Convio donation form, not just the main one.
    // This was tricky, because previously I could hard-code df_id, and level_id of the "other" giving level, for the main donation
    // form. Why is level_id necessary? Because in Mode 1 I send the user to the Donation form and pre-select the "other" giving level (which has its own, particular
    // level id), pre-filled out with their donation entry. So the solution is to switch this from hard-coding the df_id and level_id to getting
    // this value of level_id from Convio API. I wrote a new API (see below) that does the job. Given a user-entered df_id, the API returns the level_id associated with
    // the "other" giving level for that form. If the component cannot find an appropriate level_id, the component is not written out (harsh, 
    // but necessary).

    // Mode 2. Flexible ("without suggested donation")
    // In this mode, the author does NOT want a fill-out field for amount. And they want to also custom craft the target URL that a user gets
    // following a button press. Design/UX wanted this option so they could drive users to ANY URL, donation form or not. In this
    // mode, the author is responsible for generating the target URL with whatever df_id, source, subsource, etc. values they want.
    // They don't get the easy "fill out" fields as offered in Mode 1, and none of the Mode 1 options are used.


  // ------------------------------
  // Defaults:
  // ------------------------------

  // set the default Convio donation form ID. 
  final String df_id_default = Integer.toString(Constants.CONVIO_MAIN_DONATION_FORM_DF_ID);   //"5980";

  // the default value printed on the button 
  final String action_button_text_default = "Give Now";

  // URL to the Convio API to use for gaining the level_id (see above)
  //final String convio_level_api = "http://is.fhcrc.org/users/sdowning/convio-api/convio-get-form-other-level-id.php"; // omit the querystring (?form_id=#### is added later)
  final String convio_level_api = "http://is.fhcrc.org/open/convio/api/1/convio-get-form-other-level-id.php";

  // ------------------------------
  // Mode:
  // ------------------------------

  // the "mode" that this component is running in
  String mode = properties.get("mode","").equals("targetoptions") ? "mode2" : "mode1"; // mode1 (default) or mode2 (see explanation, above)


  // MODE 1 variables:

  // Convio-related options used for campaign tracking. http://help.convio.net/site/PageServer?pagename=Admin_Origin_SourceSubSource_Understanding#special

// the "source"
  String s_src = properties.get("src", "");

  // the "sub source"
  String s_subsrc = properties.get("subsrc", "");

  // the "df_id" value for the Convio donation form
  String df_id = properties.get("df_id", df_id_default);


  // MODE 2 variables:

  // the custom target URL
  String target_url = properties.get("target_url", "");

  // open in a new window option
  String newwin = properties.get("externalLink", "");



  // ------------------------------
  // Design/format output variables:
  // ------------------------------

  // component's H3 (title)
  String title = properties.get("title", ""); // title can be blank

  // component's body (message)
  String message = properties.get("message", ""); // message can be blank

  // default value presented to the user (integer)
  int default_value = properties.get("default_value", 0); // default is 0 = do not display a default amount
  String default_display = default_value==0? "" : "$ " + java.text.NumberFormat.getIntegerInstance().format( default_value );

  // action button text (the "go" button)
  String action_text = properties.get("action_button_text", action_button_text_default); // this must have a default

  // color and design themes

  String colorthemes = properties.get("colortheme","0"); // default is theme 0
  //out.write(colorthemes);
  String[] c = colorthemes.split("\t");
  String color_theme_number = c[0];
  String colorTheme = "";
  if(!color_theme_number.isEmpty()){
    colorTheme = "colorTheme" + color_theme_number ;
  }

  String designthemes = properties.get("designtheme","0"); // default is theme 0
  //out.write(designthemes);
  c = designthemes.split("\t");
  String design_theme_number = c[0];
  String designTheme = "";
  if(!design_theme_number.isEmpty()){
    designTheme = "designTheme" + design_theme_number ;
  }

  // random md5 hash for ID purposes
  final String randomID = FHUtilityFunctions.getRandomID();


  // COMPUTE the level_id when in mode1.

  // When running under mode1, the author has entered the df_id, and we need to use the Convio API to grab the level_id of that form's
  // "other" giving level.
  // Model: https://is.fhcrc.org/open/convio/api/1/convio-get-form-other-level-id.php?form_id=5980
  // Returns: OK\n8207 or ERROR\nError-message on error

  String level_id = "";

  if(mode.equals("mode1")){

    String levelidAPILink = convio_level_api + "?form_id=" + df_id;  

    HttpClient client = new HttpClient();
    HttpMethod method = new GetMethod(levelidAPILink);
	try {

        int statusCode = client.executeMethod(method);
        if (statusCode != HttpStatus.SC_OK) {
          log.error("Method failed: " + method.getStatusLine());
        }
        String apiResponse = method.getResponseBodyAsString(); //log.info(apiResponse);

        // only when the first line returned = "OK" is the api call successful:
        String lines[] = apiResponse.split("\\r?\\n");
        if(lines[0]!=null&&lines[0].equals("OK")){
            level_id = lines[1];
			log.debug("Convio Level ID API responded successfully. level_id = "+ lines[1]);
        } else {
            log.warn("Convio Level ID API returned ERROR. Perhaps the form (df_id="+ df_id +") does not exist, or there is no 'other' giving level for this form. Response = " + apiResponse);
        }

	} 

    catch (HttpException e) {
        log.error("Fatal protocol violation: " + e.getMessage());
        response.setStatus(404);
        } 
    catch (IOException e) {
        log.error("Fatal transport error: " + e.getMessage());
        response.setStatus(404);
        } 
    finally {
        method.releaseConnection();
  	}        

  } // if(mode.equals("mode1"))


  // note the extra wrapped <div>. this is because when this component is used in columns,
  // the column control tells the first child, "no top margin, no top padding!"
  // so, because this element is in a shaded box, we DO want to preserve that top padding: therefore
  // have the first child be an empty <div>.
%>

<% if(mode.equals("mode1") && 
      (df_id.equals("") || level_id.equals(""))
     ){%>

    <cq:include script="misconfigured.jsp"/>


<% } else if(mode.equals("mode2") && 
      (target_url.equals(""))
     ){%>

    <cq:include script="misconfigured.jsp"/>

<% } else if(mode.equals("mode1")){ %>

  <div><div class="donateformlet <%=colorTheme%> gradient" id="df-<%= randomID %>">
    <% if(!title.equals("")){%><h3><%= title %></h3><% } %>
    <% if(!message.equals("")){%><p class="message"><%= message %></p><% } %>
    <form method="get" action="https://secure2.convio.net/fhcrc/site/Donation2" class="clearfix">
      <input type="hidden" name="df_id" value="<%= df_id %>" />
      <input type="hidden" name="set.DonationLevel" value="<%= xssAPI.encodeForHTMLAttr(level_id) %>" />
      <input type="hidden" name="set.Value" value="">
      <% if(!s_src.equals("")){%><input type="hidden" name="s_src" value="<%= xssAPI.encodeForHTMLAttr(s_src) %>" /><% } %>
      <% if(!s_subsrc.equals("")){%><input type="hidden" name="s_subsrc" value="<%= xssAPI.encodeForHTMLAttr(s_subsrc) %>" /><% } %>
      <input type="submit" value="<%= xssAPI.encodeForHTMLAttr(action_text) %>" class="stdform df_button" />
      <span class="df_amount_container"><input type="text" name="amount" class="stdform df_amount" value="<%= xssAPI.encodeForHTMLAttr(default_display) %>" /></span>
    </form>
  </div></div> 
  <script type='text/javascript'>
     $(document).ready(initDonateFormlet("df-<%= randomID %>", "<%= colorTheme %>", <%= default_value %>));
  </script>

<% } else { %>

  <div><div class="donateformlet flexible <%=colorTheme%> gradient" id="df-<%= randomID %>">
    <div class="text_container">
      <% if(!title.equals("")){%><h3><%= title %></h3><% } %>
      <% if(!message.equals("")){%><p class="message"><%= message %></p><% } %>
    </div>
    <form method="post" action="<%= xssAPI.getValidHref(target_url) %>" class="clearfix"<%= newwin.equals("true")?" target='_blank'":"" %>>
      <input type="submit" value="<%= xssAPI.encodeForHTMLAttr(action_text) %>" class="stdform df_button" />
    </form>
  </div></div> 
  <script type='text/javascript'>
     $(document).ready(initDonateFormlet("df-<%= randomID %>", "<%= colorTheme %>", -1));
  </script>

<% } %>
