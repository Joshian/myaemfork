<%--

 ADOBE CONFIDENTIAL
 __________________

  Copyright 2011 Adobe Systems Incorporated
  All Rights Reserved.

 NOTICE:  All information contained herein is, and remains
 the property of Adobe Systems Incorporated and its suppliers,
 if any.  The intellectual and technical concepts contained
 herein are proprietary to Adobe Systems Incorporated and its
 suppliers and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material
 is strictly forbidden unless prior written permission is obtained
 from Adobe Systems Incorporated.

  ==============================================================================

     Blog: Break component / repurposed for newsplus work, sld May/2014.

  If this component is encountered in a blog entry parsys, the entry list
  component will stop rendering the subsequent paragraphs and display a
  "read more" link instead.

  ==============================================================================

--%><%@ page session="false" import="com.adobe.cq.social.blog.Blog,
                     com.adobe.cq.social.blog.BlogManager,
                     com.day.cq.i18n.I18n,
                     com.day.cq.wcm.api.WCMMode" %><%
%><%@include file="/libs/foundation/global.jsp" %><%

                         //BlogManager blogMgr = resource.getResourceResolver().adaptTo(BlogManager.class);
                         //Blog blog = blogMgr.getBlog(slingRequest);

    if (WCMMode.fromRequest(request) == WCMMode.EDIT) {
        String readmoretext = properties.get("text", "Continue reading");
        %><p class="cq-blog-placeholder cq-edit-only"><%= I18n.get(slingRequest, "---[ Break here in newsroll and show a \""+ readmoretext +"\" link ]---") %></p><%
    }

%>
