<%-- 
    Employer Match 

    Searches HEP's database for employer matching gift options.
 --%>
<%@include file="/libs/foundation/global.jsp"%><%
    String placeholder = properties.get("placeholder", "Type employer or company name here");
	String searchButtonLabel = properties.get("searchButtonLabel", "Search");
	String iframeWidth = properties.get("iframeWidth", "100%");
	String iframeHeight = properties.get("iframeHeight", "730");
%>
<script type="text/javascript">
(function($) {
  $(document).ready(function() {
    var $form = $('form[target="employer-match"]');

    $('input[name="INPUT_ORGNAME"]').keyup(function() {
      if (this.value !== this.lastValue && this.value.length >= 3) {
        if (this.timer) {
          clearTimeout(this.timer);
        }

        this.timer = setTimeout(function() {
          $form.submit();
        }, 200);

        this.lastValue = this.value;
      }
    });
  }); 
})(jQuery);
</script>
<div class="employer_match_container">
  <form target="employer-match" action="https://www.matchinggifts.com/search/fredhutch_iframe" method="post" >

    <label class="visuallyHidden" for="INPUT_ORGNAME">Employer/Company Name</label>
    <input class="search-text stdform" type="text" name="INPUT_ORGNAME" placeholder="<%= placeholder %>" size="50" />
    <input type="submit" value="<%= searchButtonLabel %>" class="search-button stdform" />

    <input type="hidden" name="INPUT_ORGNAME_required" value="You must input a company name" />
    <input type="hidden" NAME="eligible" VALUE="ALL" selected/>

  </form>
</div>

<iframe name="employer-match" width="<%= iframeWidth %>" height="<%= iframeHeight %>" scrolling="auto" frameborder="0"></iframe>