<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.commons.Doctype,
        java.util.ArrayList,
        java.util.Iterator,
        com.day.cq.wcm.api.PageFilter,
        org.apache.commons.lang.StringEscapeUtils,
        com.day.text.Text" %>
<%!
public String recursiveNav(Page current, PageFilter filter, Page finalPage, ArrayList trail, JspWriter out) throws Exception {
    Iterator<Page> children = current.listChildren(filter);
    String htmlOut = "";
    Boolean onTrail, isHidden;
    while(children.hasNext()){
        Page child = children.next();
        onTrail = false; //Reset the booleans just in case.
        isHidden = false;
        for(int i=0; i<trail.size(); i++){ //Check to see if this page is on the navigational trail of currentPage
            if(child.getPath().equals(((Page) trail.get(i)).getPath())) { //If the page we are looking at's path equals the path of any page on the trail, then it is on the trail
                onTrail = true;
                break; //No need to check any other items
            }
        }
        if("true".equals(child.getProperties().get("hideInNav", "false"))){ //Check to see if the page is marked to be hidden in Navigation
            isHidden = true;
        }
        if(isHidden && !onTrail) { //If the page is supposed to be hidden in nav AND is NOT on the trail, then we can skip it
            continue;
        }
        htmlOut += "<li";
        if(onTrail){
            htmlOut += " class=\"onTrail"; //Add a class declaration for pages on the trail
            if(child.equals(finalPage)){
                htmlOut += " currentPage"; //And an extra class for the current page
            }
            htmlOut += "\"";
        }
        htmlOut += "><a href=\"" + child.getPath() + ".html\">";
        if(child.getNavigationTitle() != null){
            htmlOut += child.getNavigationTitle(); //If the page has a Navigation Title, use that
        } else if(child.getTitle() != null){
            htmlOut += child.getTitle(); //If not, use the Page Title if that exists
        } else {
            htmlOut += child.getName(); //If not, use the Page Name, which MUST exist
        }
        htmlOut += "</a>";
        if(onTrail && child.listChildren(filter).hasNext()){ //If you are on the trail AND you have children, then call the function recursively to add another level of navigation
            htmlOut += "<ul>";
            htmlOut += recursiveNav(child, filter, finalPage, trail, out);
            htmlOut += "</ul>";
        }
        htmlOut += "</li>";
    }
    return htmlOut;
}
%>

<%
  String omitSectionNav = currentPage.getProperties().get("omitSectionNav", "false");
  if("true".equals(omitSectionNav)){ return; }
%>


<%
Page clusterHead = currentPage;
String test = clusterHead.getProperties().get("isClusterHead", "false");
ArrayList<Page> al = new ArrayList<Page>();
al.add(clusterHead);
while(!"true".equals(test)){
    if(clusterHead.getParent() == null){
        // If we have hit the top of the tree, then use the branch leader as the clusterHead
        clusterHead = currentPage.getAbsoluteParent(3);
        break;
    }
    clusterHead = clusterHead.getParent();
    test = clusterHead.getProperties().get("isClusterHead", "false");
    al.add(clusterHead);
}
PageFilter filter = new PageFilter(false, true); //pageFilter removes invalid pages but keeps hidden ones

%>

    <a name="section-navigation" class="clear"></a>
    <div class="section-nav"><ul>
      <% // adds the "Section Home" element to the top of every navigation system. if the current page is the section-home page (=clusterHead), then add currentPage class. %>
      <li class="sectionhome<%= clusterHead.getPath().equals(currentPage.getPath())?" currentPage":"" %>"><div><a href="<%= clusterHead.getPath()+".html" %>"><%= clusterHead.getTitle() %></a></div></li>
    
<%
String toPrint = recursiveNav(clusterHead, filter, currentPage, al, out);
out.println(toPrint);
%> 
</ul></div><!-- /section-nav -->   

