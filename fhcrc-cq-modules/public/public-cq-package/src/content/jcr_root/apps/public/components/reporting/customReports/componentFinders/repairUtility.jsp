<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.util.Iterator, 
                 java.util.HashMap,
                 com.day.cq.search.QueryBuilder, 
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.result.Hit,
                 java.util.List,
                 java.util.ArrayList,
                 java.util.regex.*,
                 java.util.Calendar,
                 java.util.Date,
                 java.text.SimpleDateFormat,
                 javax.jcr.*,
				 org.fhcrc.tools.FHUtilityFunctions" %>
<%
Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);
HashMap<String, String> map = new HashMap<String, String>();
Page previousPage = null;

SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy - HH:mm");

/* Here is where we construct the query. We are looking for components (which are nt:unstructured nodes)
with a sling:resourceType of the videoEmbed component, and which are located in the public website. */

map.put("type","nt:unstructured");
map.put("path","/content/stripe");
map.put("group.p.or","true");
map.put("group.property","sling:resourceType");
map.put("group.property.1_value","stripe/components/modular/image");
map.put("group.property.2_value","stripe/components/modular/member");
map.put("orderby","path");
map.put("p.limit","10000");

/* Execute the query and create an ArrayList of Nodes to hold the resulting component nodes */

List<Hit> hitList = builder.createQuery(PredicateGroup.create(map), session).getResult().getHits();
ArrayList<Node> nodeList = new ArrayList<Node>();

/* Check to make sure we got any hits back and if not, divert to an empty case */

if(hitList.size() == 0){
%>
    <cq:include script="empty.jsp"/>
<%
} else {
/* If we did get hits, we adapt each hit to a Node and add that Node to our ArrayList */
	for(int i = 0; i < hitList.size(); i++){
	      Node testNode = hitList.get(i).getResource().adaptTo(Node.class);
	      nodeList.add(testNode);
	}
/* And now turn that ArrayList into an Iterator of Nodes so we can print them out */
	Iterator<Node> nodeIterator = nodeList.iterator();
/* Double-check that we have any items to even iterate through */
    if(!nodeIterator.hasNext()){
%>  
    	    <cq:include script="empty.jsp"/>
<%
    } else {
/* Start printing out the table of results. Start with table headers. The class is used by
tablesort.js to make the table sortable by clicking the headers. The id is for creating the
"<X> results" span at the top of the page. */
%>
<!-- Table of results -->
<table id="reportResults" class="sortable">
<thead>
    <tr>
        <th>Containing Page</th>
        <th>Date of last Replication</th>
        <th>Date of last Modification</th>
        <th>Outstanding Modification?</th>
    </tr>
</thead>
<tbody>
<%
    	while(nodeIterator.hasNext()) {

    		Node nextNode = nodeIterator.next();

/* Since we want a page, we take the Node of interest and chop off all of its path from 
"/jcr:content" on, leaving its containing Page's path, then adapt the resource at that
path to a Page. */

    		String pagePath = nextNode.getPath().replaceAll("/jcr:content.*","");
            Page containingPage = resourceResolver.getResource(pagePath).adaptTo(Page.class);
            
            if (previousPage != null && containingPage.equals(previousPage)) {
            	
            	continue;
            	
            }

            Node contentNode = containingPage.getContentResource().adaptTo(Node.class);
            
            try {
            	
            	Calendar replicatedDate = contentNode.getProperty("cq:lastReplicated").getDate();
            	Calendar modifiedDate = containingPage.getLastModified();
            	
            	%>
                	<tr>
                    	<td><a href="<%= pagePath %>.html" target="_blank"><%= FHUtilityFunctions.displayTitle(containingPage) %></a></td>
                    	<td><%= df.format(replicatedDate.getTime()) %></td>
                    	<td><%= df.format(modifiedDate.getTime()) %></td>
                    	<td><%= replicatedDate.compareTo(modifiedDate) < 0 ? "Yes" : "No" %>
                	</tr>
            	<%
            	
            } catch(PathNotFoundException e) {
            	
            	//out.print("PROBLEM WITH NODE ".concat(contentNode.getPath()).concat("<br>"));
            	
            }
            
            previousPage = containingPage;

    	}
    }
}
%>
</tbody>
</table>