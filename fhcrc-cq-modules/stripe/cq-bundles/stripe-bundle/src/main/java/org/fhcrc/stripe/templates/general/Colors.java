/**
 * Colors class is in charge retrieving colors for dynamic page styles
 *  
 */

package org.fhcrc.stripe.templates.general;

import com.day.cq.wcm.api.Page;

public class Colors 
{
	public static final String DEFAULT_COLOR = "FFFFFF";

	public static String getColorProperty(Page page, String propertyName){
	     String color = (String)page.getProperties().get(propertyName,DEFAULT_COLOR);

	     Page root = page;
	     while (color.equals(DEFAULT_COLOR)){
	         if (root.getParent()!=null){
	              root = root.getParent();
	              color = (String)root.getProperties().get(propertyName,DEFAULT_COLOR);
	         }
	         else{
	             break;
	         }
	     }
	     
	     return color;
	}
}