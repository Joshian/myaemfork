<%@include file="/libs/foundation/global.jsp" %>
<div class="hpdiseasesweresearch">
     <h2>Diseases We Research</h2>
     <p class="learnmore">Learn more about our research on specific diseases:</p>   
     <cq:include path="hpdiseasesweresearchtext" resourceType="public/components/content/text"/>
     
     <%--  
     <div class="tabsContainer homepage clearfix" id="hpdiseasetabs">
          <ul class="tabs">
            <li class="emphasize"><a href="#cancers">Cancers</a></li>
            <li x-class="breaktext"><a href="#infectious">Infectious &amp;<br/>Autoimmune Diseases</a></li>            
          </ul>
          <div class="tabContentsContainer"><div class="tabContentsContainerBorder">
            <div id="cancers" class="tabContent">
                <ul class="splitMe">
                    <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/brain-tumors.html">Brain Tumors</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/breast-cancer.html">Breast Cancer</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/cervical-cancer.html">Cervical Cancer</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/colorectal-cancer.html">Colorectal (Colon) Cancer</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/esophageal-cancer.html">Esophageal Cancer (Barrett's Esophagus)</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/sarcoma.html">Kaposi Sarcoma</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/leukemia.html">Leukemia</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/liver-cancer.html">Liver Cancer</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/lung-cancer.html">Lung Cancer</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/lymphoma.html">Lymphoma</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/multiple-myeloma.html">Multiple Myeloma</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/myelodysplastic-syndrome.html">Myelodysplastic Syndrome (MDS)</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/oral-cancer.html">Oral Cancer</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/ovarian-cancer.html">Ovarian Cancer</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/pancreatic-cancer.html">Pancreatic (Pancreas) Cancer</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/prostate-cancer.html">Prostate Cancer</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/skin-cancer.html">Skin Cancer (Melanoma)</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/wilms-tumor.html">Wilms Tumor</a></li>
                  </ul>
              </div>
              <div id="infectious" class="tabContent">
                <ul>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/crohns-disease.html">Crohn's Disease</a></li>
                      <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/cystic-fibrosis.html">Cystic Fibrosis</a></li>
                      <li class="header"><span>Autoimmune Diseases</span>
                      <ul class="nobreak">
                      <li class="subitem"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/multiple-sclerosis.html">Multiple Sclerosis</a></li>
                      <li class="subitem"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/rheumatoid-arthritis.html">Rheumatoid Arthritis</a></li>
                      <li class="subitem"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/scleroderma.html">Scleroderma</a></li>
                      </ul></li>
                      <li class="header"><span>Infectious Diseases</span>
                      <ul class="nobreak">
                      <li class="subitem"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/hiv-aids.html">HIV/AIDS</a></li>
                      <li class="subitem"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/influenza.html">Influenza</a></li>
                      <li class="subitem"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases/opportunistic-infections.html">Opportunistic Infections</a></li>
                      </ul></li>
                  </ul>
              </div>
          </div>
      </div></div>
      --%>
      
      
</div>
<%--<script type='text/javascript'>
   initTabbedWidget("hpdiseasetabs",'');
</script>
--%>
      