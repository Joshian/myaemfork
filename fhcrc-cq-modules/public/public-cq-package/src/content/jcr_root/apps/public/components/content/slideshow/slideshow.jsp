<%@ page import="com.day.cq.wcm.api.WCMMode" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
int slideNumber = Integer.parseInt(properties.get("slideNumber","1"));
int dataHash = (int)Math.floor(Math.random() * Integer.MAX_VALUE);
if(WCMMode.fromRequest(request) != WCMMode.EDIT) {
%>
<cq:includeClientLib categories="public.components.slideshow" />
<%
}
%>
<div class="slideshowSuperContainer" data-hash="<%= dataHash %>">
<cq:text property="title" tagName="h3" tagClass="slideTitle" placeholder="" />
<cq:text property="subtitle" tagName="span" tagClass="instructions" placeholder="" />
<%
for(int i = 1; i <= slideNumber; i++) {
%>
    <cq:include path="<%= Integer.toString(i) %>" resourceType="/apps/public/components/content/textimage" />
<%
}
%>
</div>
<%
if(properties.get("hr","false").equals("true")){
%>
<hr class="slideshowHR"></hr>
<%
}
%>
<script type="text/javascript">
/* Create an instance of the slideshow object */
var ss_<%= dataHash %> = new FH_slideshowWidget(<%= dataHash %>, {});
</script>