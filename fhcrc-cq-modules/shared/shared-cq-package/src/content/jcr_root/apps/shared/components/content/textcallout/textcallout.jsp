<%@include file="/libs/foundation/global.jsp"%><%@ page import="com.day.cq.wcm.api.WCMMode"%><%
%>

<%        

//COLOR THEME:
// colorTheme0, colorTheme1, etc. are controlled
// via design dialog. In the design dialog configure
// the allowed set of text & background colors like this:
//
//        0<tab>Normal (default)
//        1<tab>Black text on white background
//        2<tab>White text on yellow background
//        #<tab>Text the user will see in the dropdown
//        etc.
//
// you do not have to go sequentially, and can use multiple digits if you want.
// now the user will be able to select a background color of your choosing.
// this component places a class, colorTheme#, around the text div.
// add the appropriate styling to your css for the colorTheme# choices you provide.


// DESIGN THEME:
// for complete generality, we add the ability to select a second class.
// you can use this however you want. e.g., 
//
//        0<tab>None (default)
//        1<tab>Narrow margins
//        2<tab>Blinking text
//        #<tab>Text the user will see in the dropdown
//        etc.

 String colorthemes = properties.get("colortheme","0"); // default is theme 0
 //out.write(colorthemes);
 String[] c = colorthemes.split("\t");
 String color_theme_number = c[0];
 String colorTheme = "";
 if(!color_theme_number.isEmpty()){
     colorTheme = "colorTheme" + color_theme_number ;
 }

 String designthemes = properties.get("designtheme","0"); // default is theme 0
 //out.write(designthemes);
 c = designthemes.split("\t");
 String design_theme_number = c[0];
 String designTheme = "";
 if(!design_theme_number.isEmpty()){
   designTheme = "designTheme" + design_theme_number ;
 }    
 
%>

    <div class="clearfix align<%= properties.get("calloutAlign","Right") %><%= colorTheme.equals("") ? "" : " ".concat(colorTheme) %><%= designTheme.equals("") ? "" : " ".concat(designTheme) %>">
      <% if(properties.get("calloutText","").equals("")){ %>
        <div class="cq-edit-only"><cq:text property="calloutText" tagClass="callout" tagName="div"/></div>
      <% } else { %>
        <cq:text property="calloutText" tagClass="callout" tagName="div"/>
      <% } %>
      <cq:text property="text" tagName="div" tagClass="text" />
    </div>