<%@include file="/libs/foundation/global.jsp"%><%

/* This file includes retargeting pixels on all pages in the tree
 * /content/public/en/news. The pixels to be included are Facebook/MarComm,
 * Facebook/Marketing, and Twitter. See the Retargeting Pixel component
 * for the definition of these pixels. */

final String NEWS_PATH = "content/public/en/news";
Boolean onPath = false;

if (currentPage.getPath().contains(NEWS_PATH)) {

    onPath = true;

}

if (onPath) {
%>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '229798047358683');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=229798047358683&amp;ev=PageView&amp;noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Facebook Pixel Code - Marketing -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1705204039721587');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1705204039721587&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code - Marketing -->

<!-- Twitter single-event website tag code -->
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">twttr.conversion.trackPid('nueg0', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nueg0&amp;p_id=Twitter&amp;tw_sale_amount=0&amp;tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nueg0&amp;p_id=Twitter&amp;tw_sale_amount=0&amp;tw_order_quantity=0" />
</noscript>
<!-- End Twitter single-event website tag code -->

<%
}

%>