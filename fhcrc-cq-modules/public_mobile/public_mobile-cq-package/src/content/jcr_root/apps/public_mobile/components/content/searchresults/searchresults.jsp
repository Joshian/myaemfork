<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.apache.commons.httpclient.*, 
                 org.apache.commons.httpclient.methods.*,
                 org.apache.commons.httpclient.params.HttpMethodParams,
                 java.io.IOException" %>
<%
//------------------------------------------------------
// Grab the query parameters froom the querystring sent
// in to this page.  Use the search API link provided.
// Make sure to attach the "key" to the querystring of
// the search API.  
//------------------------------------------------------
String queryString = request.getQueryString();
String searchAPILink = properties.get("./searchAPILink", "http://is.fhcrc.org/sites/public_mobile/apis/search-api/search-api.php");
String searchKey = "key=public";
queryString = searchKey + "&" + queryString;
searchAPILink = searchAPILink + "?" + queryString;
log.info("Using Google Search Appliance with the following url:  " + searchAPILink);

HttpClient client = new HttpClient();
HttpMethod method = new GetMethod(searchAPILink);
try {
    int statusCode = client.executeMethod(method);
    if (statusCode != HttpStatus.SC_OK) {
      log.error("Method failed: " + method.getStatusLine());
    }

    String searchResponse = method.getResponseBodyAsString();
    //--------------------------------------------------------------------
    // Perform some string replacements on the results.  The result uses
    // a url with a path to the search results such as /search?q=cancer.  
    // We need it to be /search.html?q=cancer.
    // Also, on the advanced search page there are buttons with an action="search".
    // We need to make that action="search.html".
    //--------------------------------------------------------------------
    searchResponse = searchResponse.replaceAll("search\\?", "search.html?");
    searchResponse = searchResponse.replaceAll("action=\"search\"", "action=\"search.html\"");
    searchResponse = searchResponse.replaceAll("/user_help.html", currentPage.getPath() + "/user_help.html");
    log.info("searchResponse = " + searchResponse);
%>
    <div id="googleapisearchresults">
       <%= searchResponse %>
    </div>
<%
} catch (HttpException e) {
    log.error("Fatal protocol violation: " + e.getMessage());
    response.setStatus(404);
  } catch (IOException e) {
    log.error("Fatal transport error: " + e.getMessage());
    response.setStatus(404);
  } finally {
    method.releaseConnection();
  }  

%>