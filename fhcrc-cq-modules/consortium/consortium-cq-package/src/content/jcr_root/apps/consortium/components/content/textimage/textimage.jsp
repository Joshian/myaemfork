<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode,
    org.fhcrc.tools.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
    Image image = new Image(resource, "image");
    Image hires = new Image(resource, "hi-res");
    Image mouseover = new Image(resource, "mouseover");
    String hiresSrc = "";
    String mouseoverSrc = "";
    String linkTarget = properties.get("linkTarget","");
    String backgroundTheme = properties.get("backgroundTheme","");
    String imageAlign = properties.get("imageAlign","");
    
    Resource r;
    if(!linkTarget.equals("")){
        linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
    }
    if(hires.hasContent()){
        hiresSrc = hires.getSrc();
    
        // If the hi-res image is not in the dam, we need to mess with the Src string to get hi-res.img.<file extension> rather than hi-res/file.<file extension>
        if(hiresSrc.contains("hi-res/file")){
            hiresSrc = hiresSrc.replaceAll("hi-res/file\\.", "hi-res.img.");
        }
    }
    if(mouseover.hasContent()){
        mouseoverSrc = mouseover.getSrc();

        // If the mouseover image is not in the dam, we need to mess with the Src string to get mouseover.img.<file extension> rather than mouseover/file.<file extension>
        if(mouseoverSrc.contains("mouseover/file")){
            mouseoverSrc = mouseoverSrc.replaceAll("mouseover/file\\.", "mouseover.img.");
        }
    }

    //drop target css class = dd prefix + name of the drop target in the edit config
    //image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
/*    
    if(!"true".equals(properties.get("noBorder","false"))) {
        image.addCssClass("frame"); // Add the frame class unless omitted
    } else {
        image.addCssClass("noborder");      
    }
*/
    if(mouseover.hasContent()){
        image.addAttribute("data-hover", mouseoverSrc);
        image.addCssClass("rolloverImage");
    }
    //remove title attribute unless there is something explicitly in the dialog
    if(properties.get("image/jcr:title","").trim().equals("")) {
        image.setTitle(" ");
    }
    
    image.loadStyleData(currentStyle);
    image.setSelector(".img"); // use image script
    image.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        image.setSuffix(currentDesign.getId());
    }

//    String addClass = (!image.hasContent() && !(WCMMode.fromRequest(request) == WCMMode.EDIT)) ? " noimage" : ""; // add a class if there is no image, css can hide it.
    %>
     
    <div class="textimageContainer clear
        <%= backgroundTheme.equals("") ? "" : (" ").concat(backgroundTheme) %>
        <%= imageAlign.equals("") ? "" : (" ").concat(imageAlign) %>">
    
        <div class="imageAndCaption<%= (!image.hasContent() && !(WCMMode.fromRequest(request) == WCMMode.EDIT)) ? " visuallyHidden" : "" %>">
        
            <div class="imageContainer<%= properties.get("noBorder","").equals("true") ? " noBorder" : "" %>">
            
<%

                if(!linkTarget.equals("")){ %>
                <a href="<%= linkTarget %>" <%= properties.get("externalLink","false").equals("true") ? "target=\"_blank\"" : "" %>>
                <% }
                image.draw(out);
                if(!linkTarget.equals("")){ %></a><% }

%>
            
            
            </div>
            
            <div class="captionAndCredit">
            
                <cq:text property="jcr:caption" tagName="span" placeholder="" tagClass="caption" />
                <cq:text property="credit" tagName="span" placeholder="" tagClass="credit" />
                
<%
                if(hires.hasContent()) {
%>
                
                <span class="hiRes"><a href="<%= hiresSrc %>" target="_blank">Click for high-res version</a></span>
                
<%
                }
%>
            </div>
        
        </div>
        
        <cq:text property="text" tagName="div" tagClass="textContainer" />
    
        <div class="clear"></div>
    
    </div>
    
    <cq:includeClientLib js="consortium.components.textimage" />
