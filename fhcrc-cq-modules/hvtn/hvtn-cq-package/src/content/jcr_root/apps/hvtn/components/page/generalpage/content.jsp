<%@include file="/libs/foundation/global.jsp"%>

<div id="slideshow"><div class="slideshow-contents"></div></div><!-- used for page layout -->

<section>

	<div id="wrapper-content-outer">
	            
		<div id="wrapper-content">
	
			<div id="content-primary" class="content-primary">
			
				<nav>
				
					<cq:include script="breadcrumbs.jsp"/>
				
					<cq:include script="back-to-parent.jsp"/>
			
				</nav>
				
				<section>

					<cq:include path="title" resourceType="foundation/components/title"/>
					
					<div class="main-content-well">
		               
						<cq:include path="par" resourceType="foundation/components/parsys"/>
					
					</div>
				
				</section>
				
				<aside>
				
					<!-- right column -->
					<div class="right-column">
						<nav>
						
							<cq:include script="sectionNavigation.jsp"/>
							
						</nav>
						
						<section>
						
							<cq:include script="relatedContent.jsp"/>
						
						</section>
						
					</div>
					
					<div class="clear"></div>
					
				</aside>
				<!-- remote-wrap-api-marker -->
				
				<nav>
					<cq:include script="mobileSectionNav.jsp"/>
				</nav>
	              
			</div>
	            
		</div>
	    
	</div>

</section>