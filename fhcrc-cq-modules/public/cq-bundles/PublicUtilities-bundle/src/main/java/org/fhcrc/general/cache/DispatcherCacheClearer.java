package org.fhcrc.general.cache;

import java.util.Dictionary;

import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.OsgiUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.Agent;
import com.day.cq.replication.AgentFilter;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.Replicator;


@Component(metatype=true, immediate=true, label="FHCRC Scheduled Dispatcher Cache Clearing", description="Clear Dispatcher Cache on a scheduled basis.")
@Service(value=java.lang.Runnable.class)
@Properties({
    @Property(name="service.description", value="Dispatcher Cache Clearer"),
    @Property(name="service.vendor", value="FHCRC")
})
public class DispatcherCacheClearer implements Runnable {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Property(label="Enabled", boolValue=false, description="Check to Enable this scheduler")
    private static final String ENABLED = "scheduler.enabled";
    private boolean enabled;

    @Property(label="Job schedule", value="30 * * * * ?", description= "CRON Syntax for Quartz Scheduler can be found at http://www.quartz-scheduler.org/docs/tutorials/crontrigger.html")
    public static final String EXPRESSION = "scheduler.expression";

    @Property(cardinality=-1000, label="Paths", description="A list of paths that the dispatcher will clear.", value={})
    private static final String PROP_PATHS = "content.paths";
    String[] paths;
     
     @Reference
	private ResourceResolverFactory resolverFactory;

    @Reference
    private Replicator replicator;
   
    public void run() {
		//--------------------------------------------------------------
    	// Only run the code if the configuration has been checked on.
		//--------------------------------------------------------------
    	if (enabled) {
	        log.info("Executing a Scheduled Dispatcher Cache Flushing");

	        //--------------------------------------------------------------
			// Since this is only for dispatcher flushing, filter the 
	        // Replication agents to only those that are enabled and are
	        // Flush agents.
	        //--------------------------------------------------------------
	        ReplicationOptions optionsForDispatcherFlush = new ReplicationOptions();
	   	    optionsForDispatcherFlush.setFilter(new AgentFilter() {
	            public boolean isIncluded(Agent agent) {
	                return agent.isEnabled() && agent.isCacheInvalidator();
	            }
	        });
	    	
			//--------------------------------------------------------------
	   	    // Get the paths to clear from the configuration.  Drop a
	   	    // replication event on the queue for each path.
			//--------------------------------------------------------------
	    	Session adminSession = null;
			try {
				ResourceResolver resolver = resolverFactory.getAdministrativeResourceResolver(null);
				adminSession = resolver.adaptTo(Session.class);
				for (String path : paths) {
					if (path != null) {
						path = path.trim();
						if (!path.isEmpty()) {
							log.info("Sending Dispatcher Cache Clearing for path : " + path);
							replicator.replicate(adminSession, ReplicationActionType.ACTIVATE, path, optionsForDispatcherFlush);
						}
					}
				}
			} catch (ReplicationException e) {
				log.error("", e);
			} catch (LoginException e) {
				log.error("", e);
			} finally {
				//--------------------------------------------------------------
				// Always make sure to logout of the session.
				//--------------------------------------------------------------
				if (adminSession != null) {
					adminSession.logout();
				}
			}
    	}
    }
    
    @SuppressWarnings("unchecked")
    protected void activate(ComponentContext context ) throws Exception {
		//--------------------------------------------------------------
        // Retrieve configuration values from Felix
		//--------------------------------------------------------------
    	log.info("Getting OSGI Configuration");
        Dictionary<String, ?> props = context.getProperties();
		paths = OsgiUtil.toStringArray(props.get(PROP_PATHS));
		enabled = OsgiUtil.toBoolean(props.get(ENABLED), false);
    }

}
