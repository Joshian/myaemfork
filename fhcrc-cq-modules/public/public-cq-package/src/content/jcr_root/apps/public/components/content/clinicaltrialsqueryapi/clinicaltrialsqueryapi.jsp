<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.apache.commons.httpclient.*, 
                 org.apache.commons.httpclient.methods.*,
                 org.apache.commons.httpclient.params.HttpMethodParams,
                 java.io.IOException,
                 java.net.URLEncoder" %>

<%! 
  //a convenience function. parses a string for non-digits, nukes those, and returns the integer as a string
  private String getint(String s){
	  String num="";
	  if(s!=null){
	    num = s.replaceAll("[^\\d]", "");
	  }
	  return num;
  }
%>

<% 

//------------------------------------------------------
// Grab the query parameters froom the querystring sent
// in to this page.  
//------------------------------------------------------
String queryString = request.getQueryString();

// user can specify what kind of component (what kind of page) this is (search|intro|all|detail)
String type = properties.get("./type","");

// set the api-host to the PHP page of the API service for resolving CT requests:
String api_host = properties.get("./apiLink", "http://is-ext.fhcrc.org/sites/public/apis/clinical-trials/index.php");

//String api_host = "http://is.fhcrc.org/sites/public/apis/clinical-trials/index.php";

// retrieve the possible expected parameters (either through selectors or querystring)
String age= request.getParameter("age");
String search_type= request.getParameter("search_type");
String atn= request.getParameter("atn");
String searchTerms= request.getParameter("searchTerms");
String rss= request.getParameter("rss");
String id= request.getParameter("id");

// de-nullify strings for convenience, and trim them
// TODO: limit the amount of data accepted.
if(age==null) { age=""; } else { age=age.trim(); }
if(search_type==null) { search_type=""; } else { search_type=search_type.trim(); }
if(atn==null) { atn=""; } else { atn=atn.trim(); }
if(searchTerms==null) { searchTerms=""; } else { searchTerms=searchTerms.trim(); }
if(rss==null) { rss=""; } else { rss=rss.trim(); }
if(id==null) { id=""; } else { id=id.trim(); }

// age must be one of three things, de-fang it:
if(!(age.equals("")||age.equals("adult")||age.equals("pediatric"))){ age=""; }
// on keyword searches, searchTerms must be a number
if(search_type.equals("keyword")){ searchTerms = getint(searchTerms); }
// id is always a number
id=getint(id);

// ******** Switch to the cached versions of pages that are available for caching ***************** */

// if the request is for a detail page, switch to the preferred method to handle that (cached):
if(atn.equals("detail")&&!id.equals("")){	
	response.sendRedirect("/content/public/en/treatment/clinical-trials/detail."+id+".html");
}
//if the request is for a view all page, switch to the preferred method to handle that (cached):
else if(atn.equals("search")&&search_type.equals("all")){   
    response.sendRedirect("/content/public/en/treatment/clinical-trials/all."+(age.equals("")?"":age+".")+"html");
}
//if the request is for a disease list page, switch to the preferred method to handle that (cached):
else if(atn.equals("search")&&search_type.equals("keyword")&&!searchTerms.equals("")){   
    response.sendRedirect("/content/public/en/treatment/clinical-trials/list."+searchTerms+"."+(age.equals("")?"":age+".")+"html");
}

else {

	// ********* Handle the request, per type ********************** */
	
	// if the component is a view all type, then hard-code that here
	if(type.equals("all")){
		// model: page.age.html
		// age is optional
	    String selector = slingRequest.getRequestPathInfo().getSelectorString();
	    if(selector!=null&&(selector.equals("adult")||selector.equals("pediatric"))){ age=selector; }
	    atn="search";
	    search_type="all";
	}
	if(type.equals("details")){
		// model: page.388.html
		// the id is required
	    String selector = slingRequest.getRequestPathInfo().getSelectorString();
	    if(selector!=null){ id=selector; }
	    atn="detail";
	    id=getint(selector);
	}
	if(type.equals("diseaselist")){
	    // model: page.55.adult.html
	    // the id is required, the age is optional
	    String[] selectors = slingRequest.getRequestPathInfo().getSelectors();
	    if(selectors.length>0) { searchTerms=getint(selectors[0]); }
	    if(selectors.length>1&&selectors[1]!=null&&(selectors[1].equals("adult")||selectors[1].equals("pediatric"))) { age=selectors[1]; }
	    atn="search";
	    search_type="keyword";    
	}
	
	// prepare them to be sent to the api
	String params = "age=" + (age!=null?URLEncoder.encode(age.trim(),"UTF-8"):"") + "&" + 
	                "search_type=" + (search_type!=null?URLEncoder.encode(search_type.trim(),"UTF-8"):"") + "&" +
	                "atn=" + (atn!=null?URLEncoder.encode(atn.trim(),"UTF-8"):"") + "&" +
	                "searchTerms=" + (searchTerms!=null?URLEncoder.encode(searchTerms.trim(),"UTF-8"):"") + "&" +
	                "id=" + (id!=null?URLEncoder.encode(id.trim(),"UTF-8"):"") + "&" +
	                "rss=" + (rss!=null?URLEncoder.encode(rss.trim(),"UTF-8"):"");
	
	String api_url = api_host + "?" + params;
	log.info("Clinical trials API called:  " + api_url);
	
	
	
	HttpClient client = new HttpClient();
	HttpMethod method = new GetMethod(api_url);
	try {
	    int statusCode = client.executeMethod(method);
	    if (statusCode != HttpStatus.SC_OK) {
	      log.error("Method failed: " + method.getStatusLine());
	    }
	
	    String searchResponse = method.getResponseBodyAsString();
	    //--------------------------------------------------------------------
	    // Perform some string replacements on the results.  The result uses
	    // a url with a path to the search results such as /search?q=cancer.  
	    // We need it to be /search.html?q=cancer.
	    // Also, on the advanced search page there are buttons with an action="search".
	    // We need to make that action="search.html".
	    //--------------------------------------------------------------------
	    //searchResponse = searchResponse.replaceAll("search\\?", "search.html?");
	    //searchResponse = searchResponse.replaceAll("action=\"search\"", "action=\"search.html\"");
	    //searchResponse = searchResponse.replaceAll("/user_help.html", currentPage.getPath() + "/user_help.html");
	    
	    searchResponse = searchResponse.replaceAll("##viewallpage##", "/content/public/en/treatment/clinical-trials/all.html"); // always cached
	    searchResponse = searchResponse.replaceAll("##newsearch##", "/content/public/en/treatment/clinical-trials/search.html"); // might be cached
	    searchResponse = searchResponse.replaceAll("##diseaselist##", "/content/public/en/treatment/clinical-trials/list.html"); // always cached
	    searchResponse = searchResponse.replaceAll("##searchpage##", "/content/public/en/treatment/clinical-trials/search.html"); // never cached
	    searchResponse = searchResponse.replaceAll("##detailpageprefix##", "/content/public/en/treatment/clinical-trials/detail."); // never cached
	    //log.info("searchResponse = " + searchResponse);
	%>
	  <div class="clinicaltrialsquery">
	       <%= searchResponse %>
	  </div>
	
	<%
	} catch (HttpException e) {
	    log.error("Fatal protocol violation: " + e.getMessage());
	    response.setStatus(404);
	  } catch (IOException e) {
	    log.error("Fatal transport error: " + e.getMessage());
	    response.setStatus(404);
	  } finally {
	    method.releaseConnection();
	  }  

}	  
	  
%>