
/** 
 * Custom JS code for the STRIPE project - Top Design
 */

/* ----------sticky footer ------------------*/

jQuery(document).ready(function()
{
	   var docHeight = jQuery(window).height();
	   var footerHeight = jQuery('#footer').height();
	   var footerTop = jQuery('#footer').position().top + footerHeight;
	   jQuery('#footer').css('margin-top', '0px');
	   if (footerTop < docHeight) {
	    jQuery('#footer').css('margin-top', 15 + (docHeight - footerTop) + 'px');
	    //jQuery('#sidenv').css('height', sidebarHeight + 20 + (docHeight - footerTop) + 'px');
	   }
});