<%@ page import="com.day.text.Text, 
                 java.util.*,
                 com.day.cq.search.QueryBuilder, 
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.result.Hit,
                 java.text.SimpleDateFormat,
                 java.text.DateFormat,
                 com.day.cq.wcm.api.WCMMode,
                 org.apache.commons.lang.StringEscapeUtils,
                 org.fhcrc.publicsite.constants.Constants,
                 org.fhcrc.tools.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
String source = properties.get("source", Constants.PATH_TO_VIDD_NEWS_FOLDER);
String outputType = properties.get("output", "terse");
Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);

/* Begin building the predicateGroup for the search */ 
HashMap<String, String> map = new HashMap<String, String>();
map.put("path", source);
/* We only care about pages that are News Page templates */
map.put("property","cq:template");
map.put("property.value","/apps/public/templates/news");
map.put("p.limit","-1");
map.put("p.guessTotal","true"); // Added to fix Oak 1.0.18 problem
/* Sort by reverse chron */
map.put("orderby","@pubDate/date");
map.put("orderby.sort","desc");
map.put("orderby.index","true");

List<Hit> hitList = builder.createQuery(PredicateGroup.create(map), session).getResult().getHits();

if(hitList.size() == 0){
%>  
    <cq:include script="empty.jsp"/>
<%
} else {
    
    Page mostRecentArticle = null, testPage = null;
    int len = hitList.size();

    for (int i = 0; i < len; i++) {
    
        testPage = hitList.get(i).getResource().getParent().adaptTo(Page.class);
        if (testPage.isValid()) {
            mostRecentArticle = testPage;
            break;
        }
        
    }
    
    if (mostRecentArticle != null) {
        
        /* Set the article headline. Not using FHUtilityFunctions because we don't want the Nav Title */
        String articleHeadline = mostRecentArticle.getPageTitle();
        if (articleHeadline == null) {
            articleHeadline = mostRecentArticle.getTitle();
        } 
        if (articleHeadline == null) {
            articleHeadline = mostRecentArticle.getName();
        } 
        
        if (outputType.equals("terse")) {
        
            /* Set the article type. Keyed off the path given by the user */
            String articleType = "VIDD News"; /* Default value */
            if (source.contains("faculty_stories")) {
                articleType = "VIDD Faculty Stories";
            } else if (source.contains("featured-news")) {
                articleType = "VIDD Featured News";
            } else if (source.contains("publications")) {
                articleType = "Publication Spotlights";
            }

    %>
    <p><a href="<%= mostRecentArticle.getPath() %>.html"><%= articleHeadline %></a><br />
    <%= mostRecentArticle.getDescription() != null ? mostRecentArticle.getDescription() : "" %></p>
    <p><a href="<%= source %>.html">More <%= articleType %></a>&nbsp;&gt;</p>
    <%
        } /* end outputType == "terse" */
        else {
        /* We know that mostRecentArticle must have a ContentResource, since it passed the .isValid() test */
            Resource articleText = mostRecentArticle.getContentResource().getChild("articletext");
            if (articleText != null) {
                Iterator<Resource> articleComponents = articleText.listChildren();
                if (articleComponents != null) {
    %>
       <h2><%= StringEscapeUtils.escapeHtml(articleHeadline) %></h2>
    <%
                    while (articleComponents.hasNext()) {
                
                        Resource nextComponent = articleComponents.next();
    %>
       <cq:include path="<%= nextComponent.getPath() %>" resourceType="<%= nextComponent.getResourceType() %>" />
    <%
                    }
                }
            }
        } /* end outputType != "terse" */
    
    } else if(WCMMode.fromRequest(request) == WCMMode.EDIT) {
    %>
    <p><span class="error">No articles were found in the directory specified.</span></p>
    <%  
    } else {
    %>
    <cq:include script="empty.jsp"/>
    <%
    }
           
}
%>