<%--
	Job Search Widget component.

	For the next version that doesnt require compositing the caption and credit into the image.
    .job-search-widget-gradient {
    	opacity: 0.68;
        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#000000+0,000000+51,000000+100,000000+100&amp;1+1,0.25+51,0+100 */
        background: -moz-linear-gradient(left,  rgba(0,0,0,1) 0%, rgba(0,0,0,1) 1%, rgba(0,0,0,0.25) 51%, rgba(0,0,0,0) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(0,0,0,1)), color-stop(1%,rgba(0,0,0,1)), color-stop(51%,rgba(0,0,0,0.25)), color-stop(100%,rgba(0,0,0,0))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(left,  rgba(0,0,0,1) 0%,rgba(0,0,0,1) 1%,rgba(0,0,0,0.25) 51%,rgba(0,0,0,0) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(left,  rgba(0,0,0,1) 0%,rgba(0,0,0,1) 1%,rgba(0,0,0,0.25) 51%,rgba(0,0,0,0) 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(left,  rgba(0,0,0,1) 0%,rgba(0,0,0,1) 1%,rgba(0,0,0,0.25) 51%,rgba(0,0,0,0) 100%); /* IE10+ */
        background: linear-gradient(to right,  rgba(0,0,0,1) 0%,rgba(0,0,0,1) 1%,rgba(0,0,0,0.25) 51%,rgba(0,0,0,0) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#000000', endColorstr='#00000000',GradientType=1 ); /* IE6-9 */
    }
	<div class="job-search-widget-gradient">
    </div>

--%><%@ page import="org.fhcrc.tools.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %><%
%><%
    String headerText = properties.get("headerText", "Careers start here");
	String searchPlaceholder = properties.get("searchPlaceholder", "Search by keyword");
	String buttonText = properties.get("buttonText", "Search Jobs");
	// Random md5 hash for ID purposes.
	final String scope = FHUtilityFunctions.getRandomID();
	String[] images = properties.get("images", String[].class);
	String[] includeCategories = properties.get("includeCategories", String[].class);
    Boolean omitAlertSignUp = properties.get("omitAlertSignUp","false").equals("true") ? true : false;                                                                                                   
%>
<div id="<%= scope %>" class="job-search-widget">
    <h3><%= headerText %></h3>
    <form class="job-search-widget-form" action="https://hub-fhcrc.icims.com/jobs/search" method="get">
        <div class="keyword-search-container">
            <label for="searchKeyword">Keyword:</label>
            <img src="<%= currentDesign.getPath() %>/img/backgrounds/magnifying-glass.png" class="magnifying-glass" />
            <input placeholder="<%= xssAPI.encodeForHTMLAttr(searchPlaceholder) %>" type="text" id="searchKeyword" name="searchKeyword" class="stdform search_term keyword-search" />
        </div>
        <div class="category-container">
            <label for="searchCategory">Category:</label>
            <select class="stdform category" name="searchCategory">
                <option value="">All categories</option>
            </select>
        </div>
        <div class="submit-container">
            <input type="submit" id="submit" class="stdform submit" value="<%= xssAPI.encodeForHTMLAttr(buttonText) %>" />
        </div>
        <input type="hidden" value="1" name="ss" id="ss" />
    </form>
    <% if (omitAlertSignUp == false) { %>                                                                                                         
      <a class="alert-signup" href="https://careers-fhcrc.icims.com/connect">Sign up for job alerts</a>    
    <% } %>                                                                                                   
</div>

<script type="text/javascript"><!--
    (function($) {
        // Randomize the background image.
        var scope = '#<%= scope %>';
        var backgroundImages = [];
        <%
        	if (images != null) {
                for (int i = 0; i < images.length; i++) {
                    out.write("backgroundImages.push('" + images[i] + "');");
                }
            }
        %>
        if ( backgroundImages.length ) {
            var activeBackgroundImage = backgroundImages[ Math.floor( Math.random() * backgroundImages.length ) ];
            $( scope + '.job-search-widget' ).css( 'background' , 'url(' + activeBackgroundImage + ') no-repeat 0 0' );
        }
        // Category filtering. Display only the ones entered.
		var includeCategories = [];
        <%
            if (includeCategories != null) {
                for (int i = 0; i < includeCategories.length; i++) {
                    out.write("includeCategories.push('" + includeCategories[i] + "');");
                }
        	}
        %>
        // Generate the select (category) options. Source = "https://api.icims.com/customers/3669/lists/lists.position".
            var endPoint = '/libs/fhcrc/ProxyServlet.html?url=http://is-ext.fhcrc.org/open/icims/data.json'; // Refreshes every 5 minutes.
        $.getJSON( endPoint ).done(function( data ) {
            var categories = [];
            for ( var i = 0; i < data.children.length; i++ ) {
                var category = data.children[i];
                if ( typeof category !== 'undefined' && category.hidden !== true && ( includeCategories.length === 0 || includeCategories.indexOf( $.trim( category.value ) ) !== -1 ) ) {
                    categories.push({
                        'listnodeid' : category.listnodeid,
                        'value' : category.value
                    });
                }
            }
            var $categorySelect = $( scope + '.job-search-widget select[name="searchCategory"]' );
            // If only one category is entered, we default to it as the selected option. 
            if ( categories.length === 1 ) {
                $categorySelect.append( '<option selected="selected" value="' + categories[0].listnodeid + '">' + categories[0].value + '</option>' );
            }
            else {
                for ( var i = 0; i < categories.length; i++ ) {
                    $categorySelect.append( '<option value="' + categories[i].listnodeid + '">' + categories[i].value + '</option>' );
                }
            }
        });

    })(jQuery);
//-->    
</script>
