<%@include file="/apps/stripe/global.jsp" %>
<%@page import="com.day.cq.dam.api.Asset,
                org.fhcrc.stripe.utilities.AssetUtil"%>

<jsp:useBean id="numberedListWithLinkBean" scope="request" class="org.fhcrc.stripe.components.NumberedListWithLink" />
<jsp:setProperty name="numberedListWithLinkBean" property="componentProperties" value="<%= properties %>"/>

 <!-- Numbered List Items -->
 <div>
   <div style="width: ${numberedListWithLinkBean.columnWidth}px;">
       <div style="width: 15px; float: left;"><c:out value="${numberedListWithLinkBean.startNumber}"/>.</div>
       <div style="display: inline; width: ${numberedListWithLinkBean.columnWidth-30}px; float: left; margin-left: 1em; padding-bottom:5px;">
           <c:out value="${numberedListWithLinkBean.textItem}"/>&nbsp;<a href="<c:out value='${numberedListWithLinkBean.linkUrl}'/>" target="_blank"><c:out value="${numberedListWithLinkBean.linkName}"/></a>
       </div>
   </div>    
</div>