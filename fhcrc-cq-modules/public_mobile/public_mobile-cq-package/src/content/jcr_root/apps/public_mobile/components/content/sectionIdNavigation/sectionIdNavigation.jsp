<%@include file="/libs/foundation/global.jsp"%><%@ page import="org.fhcrc.tools.FHUtilityFunctions" %><%


  // the only design configuration is whether this element is disabled. this is set on a template basis.
  String disabled_template = currentStyle.get("disabled","");

  // but the regular dialog which is available on a per page basis (see template dialog) should override if set to disable
  String disabled_page = currentPage.getProperties().get("./omitBackToSectionNav","");
  
  // determine which wins.
  String disabled = "";
  if(!disabled_page.equals("")){ disabled = disabled_page; } // page, if set to disable, should win.
  else{ disabled = disabled_template; }                      // otherwise the template type dictates.
  
  // but skip this component altogether if you aren't three levels down
  if( currentPage.getDepth() <= 4 ) { disabled = "true"; } // out.write("depth = " + currentPage.getDepth());
  
  // compute the title you are going to show
  Page aPage = currentPage.getAbsoluteParent(3); // this returns null or a Page object
  String show_title = "Untitled"; // should never be displayed
  String show_url = "#";          // ....ditto....
  String tooltip = "Return to this section";
  if(aPage!=null){
      show_title = FHUtilityFunctions.displayTitle(aPage);   
      show_url = aPage.getPath() + ".html";
  }
  
  if(!disabled.equals("true")){%>

    <div class="section_id">
      <p><a href="<%= show_url %>" title="<%= tooltip %>"><%= show_title %></a></p>
      <div class="gradient"></div>
    </div>

  <% }  %>



