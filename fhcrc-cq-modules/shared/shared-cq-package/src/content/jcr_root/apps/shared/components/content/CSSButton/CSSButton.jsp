<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.fhcrc.tools.FHUtilityFunctions" %>
<%@page session="false" %><%

String linkTarget = properties.get("linkTarget","");
if (!linkTarget.equals("")) {
    linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
}

// COLOR THEME:
// colorTheme0, colorTheme1, etc. are controlled
// via design dialog. In the design dialog configure
// the allowed set of text & background colors like this:
//
//        0<tab>Normal (default)
//        1<tab>Black text on white background
//        2<tab>White text on yellow background
//        #<tab>Text the user will see in the dropdown
//        etc.
//
// you do not have to go sequentially, and can use multiple digits if you want.
// now the user will be able to select a background color of your choosing.
// this component places a class, colorTheme#, around the text div.
// add the appropriate styling to your css for the colorTheme# choices you provide.


// DESIGN THEME:
// for complete generality, we add the ability to select a second class.
// you can use this however you want. e.g., 
//
//        0<tab>None (default)
//        1<tab>Narrow margins
//        2<tab>Blinking text
//        #<tab>Text the user will see in the dropdown
//        etc.

// DECORATION THEME:
//
//        &raquo;<tab>Right angle quote (default)
//        &gt;<tab>Greater than
//        HTML ENTITY<tab>Text the user will see in the dropdown
//        etc.

 String colorthemes = properties.get("colortheme","0"); // default is theme 0
 //out.write(colorthemes);
 String[] c = colorthemes.split("\t");
 String color_theme_number = c[0];
 String colorTheme = "";
 if(!color_theme_number.isEmpty()){
     colorTheme = "colorTheme" + color_theme_number ;
 }

 String designthemes = properties.get("designtheme","0"); // default is theme 0
 //out.write(designthemes);
 c = designthemes.split("\t");
 String design_theme_number = c[0];
 String designTheme = "";
 if(!design_theme_number.isEmpty()){
   designTheme = "designTheme" + design_theme_number ;
 }    

 String decorations = properties.get("decorations",""); // default is blank
 //out.write(designthemes);
 c = decorations.split("\t");
 String decoration_code = c[0];
 if (decoration_code == null || decoration_code.trim().equals("")) {
	 decoration_code = "";
 }
 
%>

<a href="<%= linkTarget %>" <%= properties.get("externalLink","false").equals("true") ? "target=\"_blank\"" : "" %> class="button<%= colorTheme.equals("") ? "" : " ".concat(colorTheme) %><%= designTheme.equals("") ? "" : " ".concat(designTheme) %>">
<%= properties.get("text","DEFAULT BUTTON TEXT") %><%= decoration_code.equals("") ? "" : "&nbsp;".concat(decoration_code) %></a>