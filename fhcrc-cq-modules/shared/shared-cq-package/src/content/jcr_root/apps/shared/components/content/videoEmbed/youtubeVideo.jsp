<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.io.IOException,
                 java.net.URLEncoder,
                 javax.xml.parsers.DocumentBuilderFactory,
                 javax.xml.parsers.DocumentBuilder,
                 org.w3c.dom.Document,
                 org.w3c.dom.NodeList,
                 org.w3c.dom.Node,
                 org.w3c.dom.Element,
                 org.xml.sax.SAXException,
                 com.day.cq.wcm.api.WCMMode" %>
<%
Boolean edit_mode = WCMMode.fromRequest(request) == WCMMode.EDIT ? true: false;

// NOTE: In responsive world, you don't want to allow sizes, so I'm omitting
//       the sizefield from this component. The CSS will stretch to fit a 16:9 aspect
//       ratio (or some other ratio you define in the css), and you can also use
//       css to define a maximum width. Beyond that, you should not be asking YouTube/Vimeo
//       for certain sizes. Sld. Jul/2013.

String videoURL = properties.get("videoURL", "");
String omitRelated = properties.get("omitRelated", "false");

if (videoURL == null || videoURL.trim().equals("")) {
%>
    <cq:include script="empty.jsp" />
<%   
} else {
   
    /* URI encode the video URL for use in the oembed API */
    videoURL = URLEncoder.encode(videoURL, "UTF-8");
    String apiURL = "";
    String apiPrefix = request.getScheme();

    apiURL = apiPrefix.concat("://www.youtube.com/oembed?url=").concat(videoURL).concat("&format=xml");	

    String theHTML = "";

    /* Limit the size of the widget if necessary */
    String maxwidth = properties.get("maxwidth","");
    String maxheight = properties.get("maxheight","");
    if(!maxwidth.trim().equals("")){
        apiURL += "&maxwidth=" + maxwidth;
    }
    if(!maxheight.trim().equals("")){
        apiURL += "&maxheight=" + maxheight;
    }

    log.info("Embedding YouTube video using oembed:  " + apiURL);

    try {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(apiURL);
        doc.getDocumentElement().normalize(); // Convenience method so we dont have to manually grab the Document node

        /* Grab all the nodes named "html" (there should be only one) and grab its contents and print it out. */   
                                                                              
        NodeList theList = doc.getElementsByTagName("html");
        if(theList.getLength() > 0){
            Node htmlNode = theList.item(0);
                                                                              
            /* Check to make sure the node we are working with is an element node, then get its first child(the text)'s value and print it out */    	
   
            if(htmlNode.getNodeType() == Node.ELEMENT_NODE){
                theHTML = htmlNode.getChildNodes().item(0).getNodeValue();
                if(theHTML != null) {
                    /* Change http requests to https if we are under https to avoid mixed content warnings */
                    if (apiPrefix.equals("https")) {
                        theHTML = theHTML.replace("http://","https://");
                    }

                    if(omitRelated.equals("true")) {
                        theHTML = theHTML.replace("feature=oembed", "feature=oembed&rel=0");
                    }

                    out.print("<div class='embeddedVideo'>" + theHTML + "</div>");
%>
                    <cq:text tagClass="videoCaption" property="caption" placeholder="" />
                    <cq:text tagClass="videoCredit" property="credit" placeholder="" />  
<%    

                } else {
                    if(edit_mode) { out.print("<span class='cq-edit-only'>Error. Could not find any embed code.</span>"); } // I need a better failover here.
                }
            }
        }

    } catch (SAXException e) {
        log.error("Fatal protocol violation: " + e.getMessage());
        if(edit_mode) { out.print("<span class='cq-edit-only'>The URL you entered returned non-XML content. " + e.getMessage() + "</span>"); }
    } catch (IOException e) {
        log.error("Fatal transport error: " + e.getMessage());
        if(edit_mode) { out.print("<span class='cq-edit-only'>Missing video, bad URL or some other problem. The video might have been removed or its permissions changed. I tried using the following URL to obtain the embed code: " + e.getMessage() + "</span>"); }
    }
    
}
%>