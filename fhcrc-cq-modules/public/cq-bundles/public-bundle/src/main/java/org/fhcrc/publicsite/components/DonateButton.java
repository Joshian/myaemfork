package org.fhcrc.publicsite.components;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.fhcrc.tools.FHUtilityFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DonateButton {
	
	private final String DEFAULT_DONATION_LINK = "/content/public/en/redirect-stubs/donate";
	protected final Logger log = LoggerFactory.getLogger(getClass());
	
	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	
	private String donationLink;

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public void setComponentProperties(ValueMap componentProperties) {
		this.componentProperties = componentProperties;
		setDonationLink(componentProperties.get("linkLocation", DEFAULT_DONATION_LINK));
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public String getDonationLink() {
		return donationLink;
	}

	public void setDonationLink(String donationLink) {
	
		if (donationLink != null && !donationLink.trim().equals("")) {
//			log.info("DONATE BUTTON: Raw donation link: " + donationLink);
			this.donationLink = FHUtilityFunctions.cleanLink(donationLink, resourceResolver);
//			log.info("DONATE BUTTON: Clean donation link: " + this.donationLink);
		} else {
			this.donationLink = "";
		}
		
	}
	
}