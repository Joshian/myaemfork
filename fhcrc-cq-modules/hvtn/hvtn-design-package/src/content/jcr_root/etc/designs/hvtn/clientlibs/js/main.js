var HVTNApp = (function($, HVTNApp) {
	
/* "Private" variables and methods */	
	var searchActive = 0;
	var mobileMenuIcon = $('img#mobile-menu-icon');
	
	var toggleMenuIcon = function(theIcon) {
	
		var closedIconLocation = '/etc/designs/hvtn/img/icons/icon-menu-isClosed.png';
		var openIconLocation = '/etc/designs/hvtn/img/icons/icon-menu-isOpen.png';
		
		if (theIcon.attr('src') === closedIconLocation) {
			
			theIcon.attr('src',openIconLocation);
			theIcon.attr('alt','close menu');
			
		} else {
			
			theIcon.attr('src',closedIconLocation);
			theIcon.attr('alt','open menu');
			
		}
		
	};

/* "Public" variables and methods */	
	HVTNApp.navigationAnimate = function() {

    /* Handle navigation when in full screen mode */
		if ($('div#navigation-main-wrapper').css('display') !== 'none') {
	
      /* fade in/out the navigation on hover: */
			$('li.navigation-main-item').hover(function() {
				$(this).find('ul.navigation-sub').fadeIn(150);
			}, function() {
				$(this).find('ul.navigation-sub').fadeOut(150);
			});

      /* close the hover navigation immediately on click of a hyperlink: */
  		$('a.navigation-main-link').click(function() {
  			$(this).next('ul.navigation-sub').fadeToggle(0);
  		});
  		$('li.navigation-sub-item a').click(function() {
  			$(this).parents('ul.navigation-sub').fadeToggle(0);
  		});
  		
  		/* add a chevron */
        $('ul.navigation-sub').prepend("<li class='chevron'></li>");

			
		}
    
    /* Handle navigation when in mobile mode */
    if ($('div#navigation-main-wrapper').css('display') == 'none') {
    
      $('li.navigation-main-item').click(function(){
      
        /* close all other subnavs */
        $('li.navigation-main-item').not(this).removeClass("active").find("ul.navigation-sub").hide();  

        /* open/close this subnav: */
        $(this).find('ul.navigation-sub').slideToggle(150, function(){
          
          // add the "active" class to container when the subnav is in open state:
          $(this).parents('li.navigation-main-item').toggleClass( 'active', $(this).css("display") !== "none" ? 1: 0 );
          
        });

      });
      
      /* clicks on nav hyperlinks should not trigger menu activity of overlying li handler.
         plus should simulate a click on the menu system toggle to reset menu state for history.go(-1) */
      $('a.navigation-main-link,li.navigation-sub-item a').click(function(event){
        event.stopPropagation();
        $('div#header-mobile-menu-toggle').click();
      });
      
      /* clicks on the subnav's li should trigger a click of the hyperlink within it */
      $('li.navigation-sub-item').click(function(event){
        //$(this).children('a').click();alert($(this).children('a').attr('href'));
        window.location.href= $(this).children('a').attr('href');
        event.stopPropagation();
      });
      
      /* menu systems should close when user clicks outside of them */
      $("#wrapper-content-outer, #wrapper-footer-outer").bind('touchstart click',function(){
      
        var navSystem = $('div#navigation-main-wrapper'); /* nav system */
        if(navSystem.css("display") !== "none"){
          navSystem.hide();
          // and update the mobile menu toggle icon:
          toggleMenuIcon(mobileMenuIcon);
        }
        var theSearch = $('div#header-mobile-search');	  /* search menu */
  		if (theSearch.css('display') !== "none") {
  			theSearch.hide();
  		}
                        
      });
    
    }
    


	
	/* Mobile menu reveal animation */
		
		$('div#header-mobile-menu-toggle').click(function() {
			
			// close all the subs for next time 
      //$("ul.navigation-sub").fadeOut(0);
      $("ul.navigation-sub").hide();
      $("li.navigation-main-item").removeClass("active");
      
      // now hide the search mobile menu if it's active:
      var theSearch = $('div#header-mobile-search');			
			if (theSearch.css('display') !== "none") {
				theSearch.slideToggle(150);
			}
      
      // now show or hide the navigation system:
			$('div#navigation-main-wrapper').slideToggle(150);
			
      // and update the mobile menu toggle icon:
			toggleMenuIcon(mobileMenuIcon);
			
		});
	
	};

	
	// add the graphical "back" image to the mobile form factor's "back to parent" navigation
	HVTNApp.setMobileBackToParentImage = function(){
	    var imageNode = $("<img src='/etc/designs/hvtn/img/nav/arrow-back-grey-highres.png' alt=''>");
	    $("div#back-to-parent p a").prepend(imageNode);
	}	
	
	
	HVTNApp.searchAnimate = function() {
				
		$('a.header-search').click(function() {
			
			$('div#header-search').slideToggle(550, function(){ //HVTNApp.setPageView(); 
				if($(this).css('display')=="block"){
					$("#header-search input[type='text']").trigger("focus");
				} else {
					$("#header-search input[type='text']").trigger("blur");        
				}
			}); 			

			/* return false in order to prevent the a tag from executing its default behavior (scroll to top of page) */
			return false;
	
		});
		
		$('#mobile-search-icon').click(function() {
			
			var theNav = $('div#navigation-main-wrapper');
			if (theNav.css('display') !== "none") {
				theNav.slideToggle(150);
				toggleMenuIcon(mobileMenuIcon);
			}
			
			$('div#header-mobile-search').slideToggle(150, function(){
				if($(this).css('display')=="block"){
					$("#header-mobile-search input[type='text']").trigger("focus");
				} else {
					$("#header-mobile-search input[type='text']").trigger("blur");        
				}        
			});
							
		});
		
	};

  /**
     adjust the positioning of elements on the page depending upon the 
     heights of header and global navigation elements.
     called at page load, resize and "search animation"
     **/
	HVTNApp.setPageView = function() {
    // determine the height of the header
    var PageHeaderHeight = $("div#wrapper-header-fixed").height(); //alert(PageHeaderHeight);
    // and the height of the navigation strip
    var PageGlobalNavigationStripHeight = $("div#navigation-main-wrapper").height(); //alert(PageGlobalNavigationStripHeight);
    if(PageHeaderHeight&&PageGlobalNavigationStripHeight) {
      // the top-page bumper is...
      var PageShimHeight = PageHeaderHeight - PageGlobalNavigationStripHeight; //alert(PageShimHeight)
      // ...except under mobile where the global nav strip is not visible
      if( $("div#navigation-main-wrapper").css("display")=='none' ){
        PageShimHeight = PageHeaderHeight;
      }//alert(PageShimHeight);      
      // now set the top-margin of the slideshow div. the rest of the page is pushed down.
      $("div#slideshow").css("margin-top",PageShimHeight);

    }
    
		//var PageViewSlideHeight = $("div#slideshow img.slide:first").height();	//alert(PageViewSlideHeight);
		//$('div#selector-slide-wrapper-01').css("top", (PageViewSlideHeight + 97) )
		//$('div#wrapper-content-outer').css("top", (PageViewSlideHeight + 50) );	/* guess-ti-mate */
		//$('div#wrapper-footer-outer').css("top", (PageViewSlideHeight + 50) );		
	};
	
	HVTNApp.addAnchorClass = function() {
		
		var theAnchors = $("a[name]:empty");
		var wrapperHeight;
		
		wrapperHeight = $("div#wrapper-header-fixed").outerHeight();
		
		theAnchors.css({
			"display":"block",
			"position":"relative",
			"visibility":"hidden",
			"top": (wrapperHeight * -1) + "px"
		});
		
	}
	
	HVTNApp.adjustForFixedHeader = function() {
		
		var hashName = window.location.hash;
		hashName = hashName.replace(/#/,"");
		var theAnchor = $("a[name=" + hashName + "]");
		if (theAnchor.length > 0) {
			var offset = theAnchor.offset();
			$('html body').animate({scrollTop:offset.top},100);
		}		
	}

	// defeat the inconsistent handling of form text input placeholder attributes
	HVTNApp.fixInputPlaceholders = function(){  
		$('input[type="text"]').focus( function(){
			$(this).attr("data-placeholder",$(this).attr('placeholder')).removeAttr("placeholder");
		});
		$('input[type="text"]').blur( function(){
			$(this).attr("placeholder",$(this).attr('data-placeholder'));
		});      
	}	
	
	
	return HVTNApp;
	
})(jQuery, HVTNApp || {});

function initGoogleAnalytics() {
//Creating a module to avoid polluting the global namespace
	var googleAnalytics = (function($, undefined){
/* We only care about the body content and related content divs */
		var scope = $('div.content-primary, div.related-content');
/* A jQuery object containing any links that contain images */
		var linkedImages = $("a:has(img), area[href]", scope);
/* A jQuery object containing any external links, not including those that
contain images */
		var externalLinks = $("a[href^='http']", scope).not(":has(img)");
/* A jQuery object containing any links to PDF files, not including PDFs
hosted on external sites */
		var pdfLinks = $("a[href$='.pdf']", scope).not("[href^='http']");
/* A jQuery object containing any internal links that contain anchor suffixes 
(e.g. #video) */
		var anchorLinks = $("a[href*='#']", scope).not("[href^='http']");
/* A testing method that simply highlights the elements of the various
jQuery objects, allowing you to make sure there is no overlap/omissions */ 
		var highlightSelected = function() {
			linkedImages.css({'border':'2px solid red'});
			externalLinks.css({'border':'2px solid blue'});
			pdfLinks.css({'border':'2px solid orange'});
			anchorLinks.css({'border' : '2px solid green'});
		};
/* A function that returns a function that can be used as an event handler.
 *
 * @param arg3    A string representing the genre of event being tracked
 *                by Google Analytics
 * @param arg4    A string representing the specific event being tracked
 *                by Google Analytics
 * @param arg5    Optional integer that can be passed into Google Analytics
 * @return theGAQ A function ready to be attached to an event handler
 *                that calls the Google Analytics engine 
 */
		var giveMeGAQ = function(arg3, arg4, arg5) {
			var theGAQ;
			if(arg5 === undefined) {
				theGAQ = function() {
					_gaq.push(['_trackEvent','Click tracking', arg3, arg4]);
				};
			} else {
				theGAQ = function() {
					_gaq.push(['_trackEvent','Click tracking', arg3, arg4, arg5]);
				};
			}
			return theGAQ;
		};
/* A function that adds the click events to the elements of a jQuery objects.
 *
 * @param group  The jQuery object containing the elements you want the 
 *               events added to
 * @param arg3   A string representing the genre of event being tracked
 *               by Google Analytics
 * @param arg5   Optional integer that can be passed into Google Analytics
 * @return       void
  */
		var addClickEvents = function(group, arg3, arg5) {
			group.each( function() {
/* If the element already has an onclick attribute, then that is a Google
Analytics push, so we don't want to add another one. Check against null
and the empty string, as either could be returned if there is no attribute */
				if(
				this.getAttribute('onclick') === null || 
				this.getAttribute('onclick') === "") {
/* Next, check to see if there are event handlers on the element using
jQuery's $.data('events') method. */
					if(
					$(this).data('events') === undefined ||
					$(this).data('events').click === undefined) {
						var arg4 = this.getAttribute('href');
						var theGAQ;
						if(arg5 === undefined) {
							theGAQ = giveMeGAQ(arg3, arg4);
						} else {
							theGAQ = giveMeGAQ(arg3, arg4, arg5);
						}
						$(this).click(theGAQ);
					}
				}
			});
		};
/* Return the methods and properties that I want exposed to the world */	
		return {
//			highlightSelected : highlightSelected,
			addClickEvents : addClickEvents,
			linkedImages : linkedImages,
			externalLinks : externalLinks,
			pdfLinks : pdfLinks,
			anchorLinks : anchorLinks
		};
	})(jQuery);
	
//	googleAnalytics.highlightSelected();
	googleAnalytics.addClickEvents(googleAnalytics.linkedImages, "Image Link");
	googleAnalytics.addClickEvents(googleAnalytics.externalLinks, "External Text Link");
	googleAnalytics.addClickEvents(googleAnalytics.pdfLinks, "Internal PDF Link");
	googleAnalytics.addClickEvents(googleAnalytics.anchorLinks, "Internal Anchored Text Link");
		
}

$(document).ready(function () {
	HVTNApp.navigationAnimate();
	HVTNApp.searchAnimate();
	HVTNApp.fixInputPlaceholders();
	initGoogleAnalytics();
	
});	


$(window).load(function () {
	HVTNApp.setPageView();	
	HVTNApp.addAnchorClass();
	if (window.location.hash) {
		HVTNApp.adjustForFixedHeader();
	}
});	

$(window).resize(function (){
	HVTNApp.setPageView();
});

$(window).load(function () {
    HVTNApp.setMobileBackToParentImage();
});