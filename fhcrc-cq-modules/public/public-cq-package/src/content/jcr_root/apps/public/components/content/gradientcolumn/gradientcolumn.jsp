<%@include file="/libs/foundation/global.jsp"%>

<%
	/*
    you'll need to add the possible layouts to the gradientcolumns in design
    mode for each template available for author use. use this syntax:
	
	#cols;cssbaseclass<tab>Display Choice Text
	
	e.g.,    
	1;fh-colctrl-lt10   1 column (100%)
	2;fh-colctrl-lt20   2 columns (50% 50%)
	3;fh-colctrl-lt30   3 columns (33% 33% 33%)
	
	refer to components.css for the possible choices for cssbaseclass
	
	*/
%>


<% String colstyles = properties.get("layout","2;fh-colctrl-lt20"); // default is currently 2-col Layout 20
   String[] c = colstyles.split(";");
   int num_cols = Integer.parseInt(c[0]); // number of columns to iterate over
   String col_base_css = c[1];            // the base css class
%>

<div class="fh_gradient_columns clearfix">
 <div class="fh_parsys_column <%= col_base_css %>">
  <%
  for( int i=0; i < num_cols; i++){ 
	 String thisPar = "par" + (i+1); 
	 String thisCss = col_base_css + "-c" + Integer.toString(i);
	 %>
    <div class="fh_parsys_column <%= thisCss %>">
     <div class="fh_section"><cq:include path="<%= thisPar %>" resourceType="foundation/components/parsys"/></div>
    </div>
  <% } %>
 </div>
</div>
