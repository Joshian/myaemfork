var componentApp = (function(componentApp) {
	
	componentApp.expanderComponent = (function(expanderComponent) {
		
		/* DEPENDENCIES: querySelectorAll, classList */
		
		/* This variable is used in the JSP to avoid initializing the components multiple times */
		expanderComponent.isInitialized = false;
		var hiddenClassName = "visuallyHidden",
			hasDataset = true;

		/* Check to see if the browser handles dataset. */
		var el = document.createElement("div");
		if (!el.dataset) {
			hasDataset = false;
		}
		
		
		/* Workhorse function. Toggles the "visuallyHidden" class on the text parsys, the "closed" class on the header, and the
		 * unicode character used as an indicator icon. */
		var toggleExpander = function(e) {
			
			var expander;
			
			/* This is some trickery so I can use the same function for the initialization (function is passed a DOM Element) and
			 * for event handling (function is passed an event). So check to see if e is an event or a DOMElement */
			
			if (e.target || e.srcElement) {
				
				/* If e is an event, then we need expander to be the header tag that contains the text and icon. */
				
				expander = e.target || e.srcElement;
				
				/* If you clicked on either of the SPAN tags in the header, then that is the target, so we need to move up to the header level */
				if (expander.tagName) {
					
					var tagName = expander.tagName,
						headerRegex = /^h\d/i;
					while( !tagName.match(headerRegex) ) {
						if (expander.parentNode == null) {
							break;
						}
						expander = expander.parentNode;
						tagName = expander.tagName;
					}
					
				}
				
			} else {
				
				/* Otherwise, e is a DOM Element, and it is the header tag that we care about. */
				expander = e;
				
			}	
			
			var textContainer,
				targetClassName = "expanderParsys";
			
			/* See the consortiumApp JavaScript for the findChildWithClassName function and its associated Exception that can be
			 * thrown. */	
			try {
				
				textContainer = consortiumApp.findChildWithClassName(expander.parentNode, targetClassName);
				
				if (textContainer !== undefined) {
				
					textContainer.classList.toggle(hiddenClassName);
								
					expanderIconToggle(expander);
				
				}
				
			} catch(e) {
				
				consortiumApp.handleChildException(e);
				
			}
			
		};
		
		var expanderIconToggle = function(expander) {
			
			var targetClassName = "expanderIcon",
				iconToggleClass = "closed";
			
			try {
				
				var theIcon = consortiumApp.findChildWithClassName(expander, targetClassName);

				if (theIcon !== null) {
					
					if (!hasDataset) {
						
						if (expanderComponent.checkDataset(expander, "isOpen", "true")) {
							
							theIcon.innerHTML = "&#9654;";
							expanderComponent.setDataset(expander, "isOpen", "false");
							
						} else {
							
							theIcon.innerHTML = "&#9660;";
							expanderComponent.setDataset(expander, "isOpen", "true");
							
						}
						
					} else {
						
						if (expander.dataset.isOpen === "true") {
							
							theIcon.innerHTML = "&#9654;";
							expander.dataset.isOpen = "false";
							
						} else {
							
							theIcon.innerHTML = "&#9660;";
							expander.dataset.isOpen = "true";
							
						}
						
					}
					
					theIcon.classList.toggle(iconToggleClass);
					
				}
				
			} catch(e) {
				
				consortiumApp.handleChildException(e);
				
			}
			
		};
		
		/* checkDataset and setDataset are fallbacks in case dataset is not supported. */
		expanderComponent.checkDataset = function(el, attr, value) {
			
			/* Took this part from the jQuery source. Converts camel case attributes into dash-separated, all-lowercase names, 
			 * as required in the data- attribute specs. */
			var rmultiDash = /([a-z])([A-Z])/g;
			var attributeName = "data-" + attr.replace(rmultiDash, "$1-$2").toLowerCase();
			
			return el.getAttribute(attributeName) === value;
			
		};
		
		expanderComponent.setDataset = function(el, attr, value) {
			
			/* Took this part from the jQuery source. Converts camel case attributes into dash-separated, all-lowercase names,
			 * as required in the data- attribute specs. */
			var rmultiDash = /([a-z])([A-Z])/g;
			var attributeName = "data-" + attr.replace(rmultiDash, "$1-$2").toLowerCase();
			
			el.setAttribute(attributeName, value);
						
		};
		
		var addIcon = function(expander) {
			
			/* Use JavaScript to add the icons, as if there is no JavaScript, there is no need for the icons. */
			var html = expander.innerHTML,
				triangleUnicode = "&#9660;";
			
			html = "<span class=\"expanderIcon\">" + triangleUnicode + "</span>" + html;
			expander.innerHTML = html;

		};
		
		expanderComponent.initializeExpanders = function() {
			
			if (expanderComponent.hasInitializedExpanders === true || !document.querySelectorAll) {
				
				return;
				
			}
			
			var theExpanders = document.querySelectorAll(".expanderHeader");
			
			/* This helper function is for adding the click events to each of the expanderHeaders.
			 * Without this, all click events toggle the final expander. */
			var expanderHelper = function() {
				
				return function() {
					toggleExpander(this);
				};
				
			};
			
			for(var expCount = 0, expLen = theExpanders.length; expCount < expLen; expCount++) {
				
				var that = theExpanders[expCount];
				addIcon(that);
				
				if (!hasDataset) {
					
					if (!expanderComponent.checkDataset(that, "defaultOpen", "true")) {
						
						toggleExpander(that);
						
					}
					
				} else {
					
					if (that.dataset.defaultOpen !== "true") {
						
						toggleExpander(that);
						
					}
					
				}
				
				consortiumApp.addEvent(that, "click", toggleExpander); 
				
			}
			
			/* Set the boolean so we don't accidentally initialize more than once. */
			expanderComponent.hasInitializedExpanders = true;
			
		};
		
		consortiumApp.addEvent(window, "load", expanderComponent.initializeExpanders);
		
		return expanderComponent;
		
	})(componentApp.expanderComponent || {});
	
	return componentApp;
	
})(componentApp || {});