<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.fhcrc.tools.FHUtilityFunctions" %>

<%

  String link = ""; //default
  String linkName = ""; // default

  Page parentNode = currentPage.getParent();
  if(parentNode!=null){
    link = parentNode.getPath() + ".html";  
    linkName = FHUtilityFunctions.displayTitle(parentNode);
  }

  if(!link.equals("") && !linkName.equals("")){
%>

               <!-- back to parent only on responsive pages: -->
               <div id="back-to-parent">
                 <p><a href="<%= link %>">Back to <span class="parent"><%= linkName %></span></a></p>
               </div>
               
<% } else {

     log.warn("Unable to determine the parent node for this page. Unable to write back-to-parent link.");
     
     }

 %>