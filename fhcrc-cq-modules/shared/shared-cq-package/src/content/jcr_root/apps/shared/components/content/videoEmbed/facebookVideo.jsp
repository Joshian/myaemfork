<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.io.IOException,
                 java.net.URLEncoder,
                 javax.xml.parsers.DocumentBuilderFactory,
                 javax.xml.parsers.DocumentBuilder,
                 org.w3c.dom.Document,
                 org.w3c.dom.NodeList,
                 org.w3c.dom.Node,
                 org.w3c.dom.Element,
                 org.xml.sax.SAXException,
                 com.day.cq.wcm.api.WCMMode" %>
<%
Boolean edit_mode = WCMMode.fromRequest(request) == WCMMode.EDIT ? true: false;

String videoURL = properties.get("videoURL_facebook","");
String width = properties.get("width_facebook","");
Boolean hasWidth = width != null && !width.trim().equals("");

if (videoURL == null || videoURL.trim().equals("")) {
%>
   <cq:include script="empty.jsp"/>
<%   
} else {

    /* URI encode the video URL for use in the oembed API */
    String apiPrefix = request.getScheme();
    
    /* Change http requests to https if we are under https to avoid mixed content warnings */
    if (apiPrefix.equals("https")) {
        videoURL = videoURL.replace("http://","https://");
    }
    
%>

    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : 'your-app-id',
          xfbml      : true,
          version    : 'v2.5'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
    
    <div class="fb-video" data-href="<%= videoURL %>" <%= hasWidth ? "data-width=\"".concat(width).concat("\"") : "" %>></div>

    <cq:text tagClass="videoCaption" property="caption" placeholder="" />
    <cq:text tagClass="videoCredit" property="credit" placeholder="" />
    
<%
    
}

%>