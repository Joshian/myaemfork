<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.WCMMode,
    com.day.cq.wcm.foundation.Image,
    java.lang.Integer,
    java.util.Comparator,
    java.util.Arrays" %>
<%
/* An arbitrary number representing the most slides allowed in the slider. */
int maxSlides = 6;
/* 
 * Start off by reordering the slides as to their order property (set by the author)
 * The comparator orders by the order property of the slide. If the slide has no order
 * property, it is granted an order of 100, effectively placing it at the end. If two
 * slides have the same order, they are compared by their tab numbers, with lower numbered
 * tabs coming first.
 */
Comparator<Float[]> arrayCompare = new Comparator<Float[]>() {
    public int compare(Float[] a, Float[] b){
        Float orderA = a[0];
        Float orderB = b[0];
        if(orderA==null){
            orderA = (float)100;
        }
        if(orderB==null){
            orderB = (float)100;
        }
    
        if(orderA.equals(orderB)){
            return a[1].compareTo(b[1]);
        }
        return orderA.compareTo(orderB);
    }
};

/* The metadata is a series of Float arrays, maxSlides number of arrays that each contain 2 points of data. */
Float[][] slideMetadata = new Float[maxSlides][2];
Float tmpOrder;

for(int metacounter = 0; metacounter < maxSlides; metacounter++) {
    /* If the user doesn't enter an order, set the order at 100 (reasonably large enough that it should come last) */
    tmpOrder = Float.parseFloat(properties.get("highlight_"+(metacounter+1)+"/order","100"));
    slideMetadata[metacounter][0] = tmpOrder;
    slideMetadata[metacounter][1] = (float)(metacounter+1);
}

Arrays.sort(slideMetadata, arrayCompare);

/* Now the slides should be in the order desired by the author. */

String[] textContent = new String[maxSlides];
Image[] imageArray = new Image[maxSlides];
Boolean[] invalidArray = new Boolean[maxSlides];
String speed = properties.get("speed","7500");
int slideNumber;

for(int i = 0; i < maxSlides; i++) {
    
    slideNumber = slideMetadata[i][1].intValue();
        
    /* First put the text associated with each slide into its own array */
    textContent[i] = properties.get("highlight_"+(slideNumber)+"/text","");
        
    /* Then the images */
    imageArray[i] = new Image(resource, "image"+(slideNumber));
    
    /* Finally, whether or not the slides are invalid. Invalid slides are displayed
     * in author mode, but not on the live site. */
    if(properties.get("highlight_"+(slideNumber)+"/isInvalid","false").equals("true")) {
        invalidArray[i] = true;
    } else {
        invalidArray[i] = false;
    }
    
}

%>

<div class="royalSlider rsMinW fullWidth">

<%

for(int i = 0; i < imageArray.length; i++) {
    if(imageArray[i].hasContent()){
/* If the current highlight has been marked invalid, skip it */
        if(invalidArray[i] == true && WCMMode.fromRequest(request) != WCMMode.EDIT){
            continue;
        }
        imageArray[i].loadStyleData(currentStyle);
        imageArray[i].setSelector(".img"); // use image script
        imageArray[i].setDoctype(Doctype.fromRequest(request));
        // add design information if not default (i.e. for reference paras)
        if (!currentDesign.equals(resourceDesign)) {
            imageArray[i].setSuffix(currentDesign.getId());
        }
/* Set the title attribute to a single space to avoid defaulting to the filename */             
        if(properties.get("image"+(slideMetadata[i][1])+"/jcr:title","").trim().equals("")) {
            imageArray[i].setTitle(" ");
        }

/* The class="hero" is needed for only the first item, for IE7 compatibility. */
%>

    <div class="rsContent <%= i==0 ? " hero" : "" %>">
        <div class="imageslide">
            <% imageArray[i].draw(out); %>
        </div>
        <% if (!textContent[i].trim().equals("")) { %>
        <div class="infoBlock">
            <%= textContent[i] %>    
        </div>
        <% } %>
    </div>

<%

    }
}

%>  

</div>  
<div class='clear'></div>      

<cq:includeClientLib js="wecan.libs.royalslider" />  
  
<script type="text/javascript">
    jQuery(document).ready(function($) {//return;
        $(".royalSlider").royalSlider({
            keyboardNavEnabled: true,
            autoScaleSlider: true,
            loop: true,
            transitionType: 'fade',
            globalCaption: true,
            imageScaleMode: 'fill',
            autoScaleSliderWidth: 1024,     
            autoScaleSliderHeight: 440,
            autoPlay: {
                enabled: true,
                pauseOnHover: true,
                delay: <%= speed %>
            },
            arrowsNav: true,
            arrowsNavAutoHide: false,
            navigateByClick: false,
            deeplinking: {
              enabled: true
            }    
        });
    });
</script>