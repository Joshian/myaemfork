<%@include file="/apps/stripe/global.jsp" %>
<%@ page import="org.fhcrc.stripe.utilities.PageUtil,
				 org.fhcrc.stripe.functions.FHUtilityFunctions" %>
<%
	String title = properties.get("title", "");
	String linkURL = properties.get("linkURL", "");
	
	if (linkURL != null && !linkURL.trim().equals("")) {
		
		linkURL = FHUtilityFunctions.cleanLink(linkURL, resourceResolver);
		
	}
	
	request.setAttribute("linkURL",linkURL);
	String headerSize = properties.get("headerSize","h2");
	
	String target = "";
	boolean linkLocation = properties.get("linkLocation", false);
	if(linkLocation)
	{
	    target = "_blank";
	}
%>

<c:choose>
    <c:when test="${!empty linkURL}">
        <a href="<%=linkURL%>" target="<%=target%>">
        	<cq:text property="title" tagName="<%= headerSize %>" />
        </a>
     </c:when>
     <c:otherwise>
       <cq:text property="title" tagName="<%= headerSize %>" />
    </c:otherwise>
</c:choose>