<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text,
        org.apache.commons.lang.StringEscapeUtils,
        com.day.cq.wcm.api.Page,
        org.fhcrc.tools.FHUtilityFunctions" %>
<%
Page clusterHead = currentPage;
String test = clusterHead.getProperties().get("isClusterHead", "false");
while(!"true".equals(test)){
    if(clusterHead.getParent() == null){
        // If we have hit the top of the tree, then use the branch leader as the clusterHead
        clusterHead = currentPage.getAbsoluteParent(3);
        break;
    }
    clusterHead = clusterHead.getParent();
    test = clusterHead.getProperties().get("isClusterHead", "false");
}

/*
String clusterTitle = clusterHead.getNavigationTitle();
if(clusterTitle == null || clusterTitle.equals("")){
    clusterTitle = clusterHead.getTitle();
}
if(clusterTitle == null || clusterTitle.equals("")){
    clusterTitle = clusterHead.getName();
}
*/
if(clusterHead.equals(currentPage)){
/* If you are on the cluster head, we want the more verbose title of the page,
 * rather than the navigation title */  
       String clusterTitle = clusterHead.getPageTitle();
        if (clusterTitle == null) {
            clusterTitle = clusterHead.getTitle();
        }
        if (clusterTitle == null) {
            clusterTitle = clusterHead.getName();
        }
%>
  <h1 class="cluster_title"><%= StringEscapeUtils.escapeHtml(clusterTitle) %></h1>
<%
} else { 
%>
  <p class="cluster_title"><%= StringEscapeUtils.escapeHtml(FHUtilityFunctions.displayTitle(clusterHead)) %></p>
<%
}
%>

<% // store the cluster head object for other scripts to use.
   // for example, check out general/eventsubpage.jsp
   request.setAttribute("clusterHead", clusterHead);
%>