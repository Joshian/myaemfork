<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text, java.util.Iterator" %>
 <!-- page content -->
 <div id="fh_body" class="fh_body container_12 clearfix"><a name="content"></a>

  <div class="grid_12">

    <!-- breadcrumbs appear in the content flow in case they wrap to two lines -->
    <cq:include script="breadcrumbs.jsp"/>
    
    <cq:include script="clusterTitle.jsp" /> 

  </div>  


  <!-- content column -->
  <div class="grid_12 fh_rightcol_container fh_body_content clearfix">
    <div class="grid_8 fh_body_column alpha">
        <% // if this is a cluster head, then do not display a field for title, since the cluster title will take on that role
	       if(!properties.get("isClusterHead",Boolean.FALSE)){ %>
	       <cq:include path="title" resourceType="foundation/components/title"/>
        <% } %>
        <cq:include path="par" resourceType="foundation/components/parsys"/>
        <!-- remote-wrap-api-marker -->
        <cq:include path="disSocialShare" resourceType="public/components/content/socialshare"/>
    </div>    
    <div class="grid_4 omega">
        <div class="fh_rightcol_content">
            <cq:include path="parcolright" resourceType="foundation/components/parsys"/>
        </div>
    </div>
  </div>    
  <div style="padding-bottom:0px;">&nbsp;</div>
  <!-- /content column -->

  <br class="clear"/>    


 </div><!-- /fh_body -->
