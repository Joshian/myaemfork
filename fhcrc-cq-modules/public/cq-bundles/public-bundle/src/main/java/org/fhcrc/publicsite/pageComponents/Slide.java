package org.fhcrc.publicsite.pageComponents;

import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.io.Writer;
import java.lang.Exception;

import com.day.cq.wcm.api.components.DropTarget;
import com.day.cq.commons.Doctype;
import com.day.cq.wcm.foundation.Image;
import com.day.cq.wcm.api.designer.*;
import com.day.cq.wcm.api.Page;

import org.apache.sling.api.resource.*;
import org.fhcrc.tools.FHUtilityFunctions;

import java.util.ArrayList;
import java.util.Iterator;

public class Slide {
	
	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	private Resource resource;
	private Style currentStyle;
	private Design currentDesign;
	private Design resourceDesign;
	private HttpServletRequest request;
	private Writer writer;
	private Page currentPage;
	
	private Image slideImage;
	
	private String slideshowTitle;
	private String slideTitle;
	private String caption;
	private String photoCredit;
	
	private String[] relatedList;
	private ArrayList<Page> relatedContent;
	
	private boolean related;
	private boolean photoContent;

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public void setComponentProperties(ValueMap componentProperties) {
		this.componentProperties = componentProperties;
		this.slideshowTitle = FHUtilityFunctions.displayTitle(currentPage.getParent());
		setSlideTitle(componentProperties.get("headline",""));
		setCaption(componentProperties.get("caption",""));
		setPhotoCredit(componentProperties.get("photoCredit",""));
		
		if (componentProperties.get("related",String[].class) != null) {
			setRelatedList(componentProperties.get("related",String[].class));
		} else {
			this.related = false;
		}
		this.slideImage = new Image(this.resource, "image");
		setSlideImage(this.slideImage);
		
		if (slideImage.hasContent()) {
			this.photoContent = true;
		} else {
			this.photoContent = false;
		}
		
	}

	public Page getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public Style getCurrentStyle() {
		return currentStyle;
	}

	public void setCurrentStyle(Style currentStyle) {
		this.currentStyle = currentStyle;
	}

	public Design getCurrentDesign() {
		return currentDesign;
	}

	public void setCurrentDesign(Design currentDesign) {
		this.currentDesign = currentDesign;
	}

	public Design getResourceDesign() {
		return resourceDesign;
	}

	public void setResourceDesign(Design resourceDesign) {
		this.resourceDesign = resourceDesign;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public Writer getWriter() {
		return writer;
	}

	public void setWriter(Writer writer) {
		this.writer = writer;
	}

	public Image getSlideImage() {
		return slideImage;
	}

	public void setSlideImage(Image slideImage) {
		this.slideImage = slideImage;
		prepImage(slideImage);
	}

	public String getSlideshowTitle() {
		return slideshowTitle;
	}

	public void setSlideshowTitle(String slideshowTitle) {
		this.slideshowTitle = slideshowTitle;
	}

	public String getSlideTitle() {
		return slideTitle;
	}

	public void setSlideTitle(String slideTitle) {
		this.slideTitle = slideTitle;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getPhotoCredit() {
		return photoCredit;
	}

	public void setPhotoCredit(String photoCredit) {
		this.photoCredit = photoCredit;
	}

	public String[] getRelatedList() {
		return relatedList;
	}

	public void setRelatedList(String[] relatedList) {
		this.relatedList = relatedList;
		if (relatedList.length > 0) {
			this.related = true;
			createRelatedContent(relatedList);
		} else {
			this.related = false;
		}
	}

	public ArrayList<Page> getRelatedContent() {
		return relatedContent;
	}

	public void setRelatedContent(ArrayList<Page> relatedContent) {
		this.relatedContent = relatedContent;
	}

	public boolean isRelated() {
		return related;
	}

	public boolean isPhotoContent() {
		return photoContent;
	}

	public void setPhotoContent(boolean photoContent) {
		this.photoContent = photoContent;
	}

	public void setRelated(boolean related) {
		this.related = related;
	}
	
	private void createRelatedContent(String[] relatedList) {
		
		ArrayList<Page> pageList = new ArrayList<Page>();
		
		for (int i = 0; i < relatedList.length; i++) {
			
			if (relatedList[i] != null && !relatedList[i].trim().equals("")) {
				
				if (this.resourceResolver.getResource(relatedList[i]) != null) {
					Page thePage = this.resourceResolver.getResource(relatedList[i]).adaptTo(Page.class);
					
					if (thePage.isValid()) {
						pageList.add(thePage);
					}
					
				}

			}
			
		}
		
		if (!pageList.isEmpty()) {
			setRelatedContent(pageList);
		}
		
	}
	
	public void drawRelatedContent() throws Exception {
		
		if (relatedContent.isEmpty()) {
			throw new Exception("The related content arrayList is empty.");
		} else {
			Iterator<Page> pageIterator = relatedContent.iterator();
			while (pageIterator.hasNext()) {
				
				Page thePage = pageIterator.next();
				
				/* If the page doesn't exist, skip it. This can happen if a page listed as related content
				 * on the Author instance has also been deactivated or has reached its Off Time*/
				if (thePage == null || !thePage.isValid()) {
					
					continue;
					
				}				
				
				Image pageImage = new Image(thePage.getContentResource("image"));
				StringBuilder htmlStringBuilder = new StringBuilder();
				String pageTitle = FHUtilityFunctions.displayTitle(thePage);
				String pageHref = thePage.getPath().concat(".html");
				
				htmlStringBuilder.append("<div class=\"related_content_item\"><a href=\"");
				htmlStringBuilder.append(pageHref);
				htmlStringBuilder.append("\" target=\"_blank\">");
				htmlStringBuilder.append("<h4 class=\"related_content_item_title\">".concat(pageTitle).concat("</h4>"));
				
				if (pageImage.hasContent()) {
					
					prepImage(pageImage);
					pageImage.addCssClass("related_content_item_photo");
					String img;
					img = thePage.getPath() + ".img.png" + pageImage.getSuffix(); // e.g.,: /content/public/en/events/events/hutch-award.img.png/1323194909406.gif
		            // prepend context path to img
		            img = request.getContextPath().concat(img);
		            img = resourceResolver.map(img);
					
		            htmlStringBuilder.append("<img src=\"".concat(img).concat("\" class=\"related_content_item_photo\""));
		            htmlStringBuilder.append(" alt=\"".concat(pageImage.getAlt()).concat("\">"));								
										
				}
					
				htmlStringBuilder.append("</a></div>");
					
				writer.write(htmlStringBuilder.toString());
				
			}
		}
		
	}
	
	public void drawSlideImage() {
		
		try {
			slideImage.draw(writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@SuppressWarnings("deprecation")
	private void prepImage(Image image) {
		
		image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
		image.loadStyleData(currentStyle);
		image.setSelector(".img"); // use image script
		image.setDoctype(Doctype.fromRequest(request));
	    // add design information if not default (i.e. for reference paras)
	    if (!currentDesign.equals(resourceDesign)) {
	    	image.setSuffix(currentDesign.getId());
	    }
		
	}

}