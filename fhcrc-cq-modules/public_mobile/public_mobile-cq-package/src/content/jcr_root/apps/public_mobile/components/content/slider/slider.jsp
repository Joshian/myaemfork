<%@ page import="com.day.cq.wcm.api.WCMMode" %>
<%@include file="/libs/foundation/global.jsp"%>
<%
int dataHash = (int)Math.floor(Math.random() * Integer.MAX_VALUE);
%>

<div class="slideContainer" id="slider_<%= dataHash %>">

<cq:include path="slidePar" resourceType="foundation/components/parsys"/>

</div>

<%
if(WCMMode.fromRequest(request) != WCMMode.EDIT) {
%>

<cq:includeClientLib js="mobile.components.slider" />

<script type="text/javascript">
var slider_<%= dataHash %> = new Swipe(document.getElementById("slider_<%= dataHash %>"));
slider_<%= dataHash %>.setupMobileSlider();
</script>
<% } %>

<% if(WCMMode.fromRequest(request) == WCMMode.EDIT){%>
<div class="cq-edit-only" style="font-style:italic; background-color:#ddd; text-align:center;">
  This is the end of the slider component. This text will not appear on your webpage.
</div>
<% } %>