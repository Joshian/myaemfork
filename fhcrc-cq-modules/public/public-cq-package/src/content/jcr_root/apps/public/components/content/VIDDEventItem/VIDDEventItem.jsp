<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.text.SimpleDateFormat,
                 java.text.DateFormat,
                 com.day.cq.wcm.foundation.Download,
                 com.day.text.Text" %>
<%
String eventDate = properties.get("date","");
String href = "";

DateFormat g = new SimpleDateFormat("MM/dd/yy");
DateFormat f = new SimpleDateFormat("EEEE, MMMM d");
if(!eventDate.equals("")){
	eventDate = f.format(g.parse(eventDate));
}

Download dld = new Download(resource);
if (dld.hasContent()) {
    href = Text.escape(dld.getHref(), '%', true);
}

%>

<div class="eventItemContainer<%= properties.get("text","").equals("") ? " noDescription" : "" %>">

    <p class="eventDetails"><em><%= eventDate %></em>&nbsp;&ndash;&nbsp;<%= href.equals("") ? "" : "<a href=\"".concat(href).concat("\" target=\"_blank\">") %><cq:text tagName="span" property="title" /><%= href.equals("") ? "" : "</a>" %><br />
    <cq:text property="alert" tagName="span" tagClass="alertText" placeholder="" /></p>
    <cq:text property="text" tagClass="eventDescription" placeholder="" />

</div>