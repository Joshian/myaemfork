<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text, java.util.Iterator, com.day.cq.wcm.api.WCMMode, org.fhcrc.publicsite.constants.Constants,                 com.day.cq.tagging.Tag,
                 com.day.cq.tagging.TagManager" %><%

    /* This script is invoked for a Center News article (other news get the "classic" layout, see content-classic.jsp). 
       This news type is the "rich" or "newsplus" format that has a full right column,
       taglist and related content that inherits down from the news landing page. */

    %>

 <!-- page content -->
 <div id="fh_body" class="fh_body container_12 clearfix"><a name="content"></a>

  <div class="grid_12">

    <!-- breadcrumbs appear in the content flow in case they wrap to two lines -->
    <cq:include script="breadcrumbs.jsp"/>

    <cq:include script="clusterTitle.jsp" /> 
    
  </div>  

  <!-- fh_body_content (main content area, right column) -->
  <div id="fh_body_content" class="fh_body_content fh_rightcol_container newsplus grid_12 clearfix">  

      <div class="fh_body_column grid_8 alpha">

        <!--  TITLE -->
        <cq:include path="title" resourceType="foundation/components/title"/>
        
        <!--  SUBTITLE -->
        <% String subtitle = properties.get("newssubtitle/text",""); %>
        <% if( "".equals(subtitle) && WCMMode.fromRequest(request) == WCMMode.EDIT){%>
          <div class="cq-edit-only" style="font-style:italic">Subtitle (if any):</div>
        <% } %>
        <h2 class="news-subhead"><cq:include path="newssubtitle" resourceType="public/components/content/textarea"/></h2>
        
        <!--  PUBLICATION DATE -->
        <%  if(WCMMode.fromRequest(request) == WCMMode.EDIT){%>
          <div class="cq-edit-only"><i>Publication date, required:</i></div>
        <% } %>
          <span class="newspage-date"><cq:include path="pubDate" resourceType="public/components/content/date"/></span>
        <% if( WCMMode.fromRequest(request) == WCMMode.EDIT ){ %>
           <!-- Pub date is currently: <cq:text property="pubDate/date"/> -->
        <% } %>
    
        <!--  BYLINE -->
        <% if(WCMMode.fromRequest(request) == WCMMode.EDIT){%>
          <div class="cq-edit-only" style="margin-top:12px; font-style:italic;clear:both">Byline (if any):</div>
        <% } %>
        <% String byline = currentPage.getProperties().get("byline/text", ""); 
           String pipes = byline.equals("") ? "" : "<span class='pipes'>&nbsp;|&nbsp;</span>";
         %>
         <span class="newspage-byline"><%= pipes %><cq:include path="byline" resourceType="public/components/content/textarea"/></span>   
    
        <div class="clear"></div>
    
        <!-- TEXT -->        
        <% if( WCMMode.fromRequest(request) == WCMMode.EDIT ){%>
        <p class="cq-edit-only" style="font-style:italic">Story text:</p>
        <% } %>
        <cq:include path="articletext" resourceType="foundation/components/parsys"/>


        <!-- FEATURES -->  
        <cq:include script="news-features.jsp"/>

      </div>
      <!-- /fh_body_column -->




      <!-- fh_rightcol_column -->
      <div class="fh_rightcol_column omega grid_4">

		  <div class="fh_rightcol_content">

              <!--  iparsys -->
              <cq:include path="newsparcolright" resourceType="foundation/components/iparsys"/>

          </div>
          <!-- /fh_rightcol_content -->


      </div>
      <!-- /fh_rightcol_column -->




  </div>
  <!-- /fh_body_content -->

    

 </div><!-- /fh_body -->
