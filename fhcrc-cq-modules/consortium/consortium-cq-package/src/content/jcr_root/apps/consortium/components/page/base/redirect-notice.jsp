<%@include file="/libs/foundation/global.jsp"%><%@ 
  page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.WCMMode,
    com.day.cq.wcm.foundation.ELEvaluator" %><%

// if this page has a redirectTarget and you are in WCM mode
// create a little banner message so the user knows this
// while they are editing in CQ.

// SLD Sep/11.




String location = properties.get("redirectTarget", "");
location = ELEvaluator.evaluate(location, slingRequest, pageContext);
Page target = pageManager.getPage(location);
String title = target == null ? location : target.getTitle();

if (WCMMode.fromRequest(request) != WCMMode.DISABLED && location.length() > 0) {
    // check for recursion
    if (currentPage != null && !location.equals(currentPage.getPath()) && location.length() > 0) {
        // check for absolute path
        final int protocolIndex = location.indexOf(":/");
        final int queryIndex = location.indexOf('?');
        final String redirectPath;
        if (  protocolIndex > -1 && (queryIndex == -1 || queryIndex > protocolIndex) ) {
            redirectPath = location;
        } else {
            redirectPath = request.getContextPath() + location + ".html";
        }       
        %><div class="cq-redirect-notice"><div>This page redirects to <a href="<%= redirectPath %>"><%= title %></a>. Use Page Properties to adjust or remove the redirect target. <br/><span class="smaller">(This notice does not appear on the published page.)</span></div></div><%
    } 
}

%>