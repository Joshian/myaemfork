<%@ page import="java.util.Calendar" %><%@include file="/libs/foundation/global.jsp"%><%

Calendar theTime = Calendar.getInstance();
int theYear = theTime.get(Calendar.YEAR);

%>

 <div class="container_12">
 
 	<div class="grid_12 horizontal_rule"></div>
 
 	<div class="grid_fifths footer_column_1">
 		<cq:include path="footer_text_column_1" resourceType="/apps/public/components/content/text"/>
 	</div>
 	
 	<div class="grid_fifths footer_column_2">
 		<cq:include path="footer_text_column_2" resourceType="/apps/public/components/content/text"/>
 	</div>
 	
 	<div class="grid_fifths footer_column_3">
 		<cq:include path="footer_text_column_3" resourceType="/apps/public/components/content/text"/>
 	</div>
 	
 	<div class="grid_fifths footer_column_4">
 		<p><a href="http://www.cancer.gov/" target="_blank"><img id="ccc_logo_footer" src="<%= currentDesign.getPath() %>/img/logos/fred_hutch_ccc_logo_2016.png" alt="A Comprehensive Cancer Center Designated by the National Cancer Institute"></a></p>
 		<p><a href="http://www.nccn.org/" target="_blank"><img id="nci_logo_footer" src="<%= currentDesign.getPath() %>/img/logos/fred_hutch_nci_logo.png" alt="National Comprehensive Cancer Network"></a></p>
		<p><a href="http://www.aahrpp.org/" target="_blank"><img id="aahrpp_logo_footer" src="<%= currentDesign.getPath() %>/img/logos/aahrpp_logo_footer.png" alt="AAHRPP logo"></a></p>
		<p id="aahrpp_accreditation">Accredited by the Association for the Accreditation of Human Research Protection Programs</p>
 	</div>
 	
 	<div class="grid_fifths footer_column_5">
 		<h3>Follow Us</h3>
 		<img id="social_media_icons_footer" src="<%= currentDesign.getPath() %>/img/icons/socialmedia_footer.png" alt="Social media icons" usemap="#socialmap">
 		<map name="socialmap">
     		<area shape="rect" coords="0,0,28,28" href="http://www.facebook.com/HutchinsonCenter" target="_blank" alt="Like Fred Hutch on Facebook"></area>
     		<area shape="rect" coords="28,0,56,28" href="http://twitter.com/fredhutch" target="_blank" alt="Follow Fred Hutch on Twitter"></area>
     		<area shape="rect" coords="56,0,83,28" href="https://www.youtube.com/channel/UCnWBh22UKMHF4VRKdsQvZqg/featured" target="_blank" alt="View the Fred Hutch YouTube channel"></area>
     		<area shape="rect" coords="83,0,111,28" href="http://www.pinterest.com/fredhutch/" target="_blank" alt="Pin from the Fred Hutch boards on Pinterest"></area>
     		<area shape="rect" coords="111,0,139,28" href="http://instagram.com/FredHutch" target="_blank" alt="Check out Fred Hutch photos on Instagram"></area>
    		<area shape="rect" coords="139,0,166,28" href="https://plus.google.com/+fredhutch" target="_blank" alt="Follow Fred Hutch on Google+"></area>
   		</map>
   		<cq:include path="footer_donate_button" resourceType="/apps/public/components/content/donateButton"/>
 	</div>
 	
 </div>
 
 <div class="container_12 clearfix"></div>
 
 <div id="copyright" class="copyright">
  Fred Hutchinson Cancer Research Center | 1100 Fairview Ave. N., Seattle, WA 98109<br/>
  &copy; <%= theYear %> Fred Hutchinson Cancer Research Center, <span class="nobreak">a 501(c)(3) nonprofit organization.</span><br/> 
  <span class="terms_privacy"><a href="/content/public/en/util/terms-privacy.html">Terms of use &amp; privacy policy</a>&nbsp;|&nbsp;<a href="/content/public/en/util/conflict-of-interest.html">Conflict of interest</a></span>
 </div>
 
 <br class="clear"/>