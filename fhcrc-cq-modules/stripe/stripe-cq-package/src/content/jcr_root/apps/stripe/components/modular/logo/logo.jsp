<%--

  Logo component.

--%>
<%@include file="/apps/stripe/global.jsp"%>
<%@page session="false" %><%
%><%@page import="com.day.cq.wcm.foundation.Image,
                  org.fhcrc.stripe.components.Logo"%><%
%>
    <jsp:useBean id="logoBean" scope="request" class="org.fhcrc.stripe.components.Logo" />
    <jsp:setProperty name="logoBean" property="currentStyle" value="<%= currentStyle %>"/>
    <jsp:setProperty name="logoBean" property="currentDesign" value="<%= currentDesign %>"/>    
    <jsp:setProperty name="logoBean" property="currentPage" value="<%= currentPage %>"/>
    
    <a href="${logoBean.link}">
        <img alt="${logoBean.altText}" src="${logoBean.sourceImage}"/>
    </a>