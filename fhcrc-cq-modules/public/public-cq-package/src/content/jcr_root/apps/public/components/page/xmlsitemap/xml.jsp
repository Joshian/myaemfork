<%--

  XMLSitemap component XML view.

--%><%@include file="/libs/foundation/global.jsp"%><%--
--%><%@include file="/apps/public/components/page/xmlsitemap/xmlsitemap.class.jsp"%><%--
--%><%@page session="false"%><%--
--%><%
String rootPath = properties.get("rootPath", "");
String publicationName = properties.get("publicationName", "Fred Hutchinson Cancer Research Center");
Page rootPage = slingRequest.getResourceResolver().adaptTo(PageManager.class).getPage(rootPath);
XMLSitemapFactory xmlSitemapFactory = new XMLSitemapFactory();
XMLSitemap xmlSitemap = xmlSitemapFactory.buildXMLSitemap(rootPage, resourceResolver, request, "news");
List<Map<String, String>> mapValues = xmlSitemap.getMapValues();
%><?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
	xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"><%
  for (int i = 0; i < mapValues.size(); i++) {
    Map<String, String> row = mapValues.get(i);
    String genres = row.get("genres");
    String keywords = row.get("keywords");
%>
    <url>
      <loc><%= xmlSitemap.absolutePath(row.get("loc")) %></loc>
      <news:news>
         <news:publication>  
           <news:name><%= StringEscapeUtils.escapeXml(publicationName) %></news:name>
           <news:language><%= row.get("language") %></news:language>
         </news:publication><%
        if (!genres.equals("")) {
%>
          <news:genres><%= genres %></news:genres><%
      	}
%>
        <news:publication_date><%= row.get("pubDate") %></news:publication_date>
        <news:title><%= StringEscapeUtils.escapeXml(row.get("title")) %></news:title><%
        if (!keywords.equals("")) {
%>
          <news:keywords><%= keywords %></news:keywords><%
        }
%>
      </news:news>
    </url><%
  }
%>
</urlset>