<%@include file="/libs/foundation/global.jsp"%><%
String dirResultsPath = currentPage.getAbsoluteParent(2).getPath() + "/util/directory.html";
//log.info("dirResultsPath = " + dirResultsPath);

// determine presets
// company may be sent: company=fhcrc|scca
String company= request.getParameter("company");
if(null==company){ company=""; }
String fh_company_preset = (company.equals("fhcrc")) ? "checked='checked'" : "";
String scca_company_preset = (company.equals("scca")) ? "checked='checked'" : "";
String both_company_preset = (!company.equals("fhcrc")) && (!company.equals("scca")) ? "checked='checked'" : "";

// query might be sent (anything)
String type= request.getParameter("type"); log.info("mytype = " + type); if(null==type){ type=""; }
String q= request.getParameter("q");  if(null==q){ q=""; }
String query_preset = !(type.equals("peoplebydeptid") || type.equals("relateddeptsid")) ? q : "";

%>

<div class="directorysearchboxcontainer">
<div class="borderbox">

  <!-- here is the general search -->
  <form method="get" name="mainqsearch" action="<%= dirResultsPath %>">
  <input type="hidden" name="short" value="true">
	
	  <p>Enter your search terms:</p>
	  
	  <input type="text" name="q" size="35" maxlength="124" value="<%=xssAPI.filterHTML(query_preset)%>" class="stdform" />
	  <input type="submit" value="Search" class="stdform">
	  
	  <p class="filters">
		  Try names, departments, mail stops, phone numbers or email addresses.<br/>
		  <input type="radio" name="company" id="company1" value="fhcrc" <%=fh_company_preset%>><label for="company1" title="Limit results to Fred Hutch">Fred Hutch</label>
		  <input type="radio" name="company" id="company2" value="scca" <%=scca_company_preset%>><label for="company2" title="Limit results to Seattle Cancer Care Alliance">Seattle Cancer Care Alliance</label>
		  <input type="radio" name="company" id="company3" value="" <%=both_company_preset%>><label for="company3" title="Show results from both Fred Hutch and Seattle Cancer Care Alliance">Both</label>
	  </p>
	
	  <p class="startover"><a href="<%=dirResultsPath%>">Start over</a></p>
  </form>

  <!-- added focus 2/09 -->
  <script type='text/javascript' language='javascript'><!--
    if(document['mainqsearch']&&document['mainqsearch'].q){
      document['mainqsearch'].q.focus();
    }
    //-->
  </script> 
  
</div>
</div>  