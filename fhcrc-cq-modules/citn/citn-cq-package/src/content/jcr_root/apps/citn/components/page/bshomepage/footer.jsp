<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="java.util.Calendar" %>
<%
Calendar theTime = Calendar.getInstance();
int theYear = theTime.get(Calendar.YEAR);
%>
        <div class="row-fluid footer">
            <div class="span12">
                <!-- footer including chunky grey stripe -->
                <div id="footer_home" class="footer_home">
                  &copy; <%= theYear %> Cancer Immunotherapy Trials Network &nbsp;|&nbsp;
                    <a href="/content/citn/en/about/contact.html">Contact Us</a> &nbsp;|&nbsp; 
                    <a href="/content/citn/en/about/privacy.html">Privacy Notice</a>
                </div>
            </div> 
       </div>
</div> <!-- /container -->