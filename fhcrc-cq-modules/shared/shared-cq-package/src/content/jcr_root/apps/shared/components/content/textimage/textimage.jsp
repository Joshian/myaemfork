<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode,
    org.fhcrc.tools.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
    Image image = new Image(resource, "image");
    Image hires = new Image(resource, "hi-res");
    Image mouseover = new Image(resource, "mouseover");
    String hiresSrc = "";
    String mouseoverSrc = "";
    String linkTarget = properties.get("linkTarget","");
    String imageAlign = properties.get("imageAlign","");

    
    
	// COLOR THEME:
	// colorTheme0, colorTheme1, etc. are controlled
	// via design dialog. In the design dialog configure
	// the allowed set of text & background colors like this:
	//
	//        0<tab>Normal (default)
	//        1<tab>Black text on white background
	//        2<tab>White text on yellow background
	//        #<tab>Text the user will see in the dropdown
	//        etc.
	//
	// you do not have to go sequentially, and can use multiple digits if you want.
	// now the user will be able to select a background color of your choosing.
	// this component places a class, colorTheme#, around the text div.
	// add the appropriate styling to your css for the colorTheme# choices you provide.
	
	
	// DESIGN THEME:
	// for complete generality, we add the ability to select a second class.
	// you can use this however you want. e.g., 
	//
	//        0<tab>None (default)
	//        1<tab>Narrow margins
	//        2<tab>Blinking text
	//        #<tab>Text the user will see in the dropdown
	//        etc.
	
	 String colorthemes = properties.get("colortheme","0"); // default is theme 0
	 //out.write(colorthemes);
	 String[] c = colorthemes.split("\t");
	 String color_theme_number = c[0];
	 String colorTheme = "";
	 if(!color_theme_number.isEmpty()){
	     colorTheme = "colorTheme" + color_theme_number ;
	 }
	
	 String designthemes = properties.get("designtheme","0"); // default is theme 0
	 //out.write(designthemes);
	 c = designthemes.split("\t");
	 String design_theme_number = c[0];
	 String designTheme = "";
	 if(!design_theme_number.isEmpty()){
	   designTheme = "designTheme" + design_theme_number ;
	 }    
        
    Resource r;
    if(!linkTarget.equals("")){
    	linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
    }
    if(hires.hasContent()){
        hiresSrc = hires.getSrc();
    
        // If the hi-res image is not in the dam, we need to mess with the Src string to get hi-res.img.<file extension> rather than hi-res/file.<file extension>
        if(hiresSrc.contains("hi-res/file")){
            hiresSrc = hiresSrc.replaceAll("hi-res/file\\.", "hi-res.img.");
        }
    }
    if(mouseover.hasContent()){
        mouseoverSrc = mouseover.getSrc();

        // If the mouseover image is not in the dam, we need to mess with the Src string to get mouseover.img.<file extension> rather than mouseover/file.<file extension>
        if(mouseoverSrc.contains("mouseover/file")){
            mouseoverSrc = mouseoverSrc.replaceAll("mouseover/file\\.", "mouseover.img.");
        }
    }

    //drop target css class = dd prefix + name of the drop target in the edit config
    //image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
/*    
    if(!"true".equals(properties.get("noBorder","false"))) {
        image.addCssClass("frame"); // Add the frame class unless omitted
    } else {
    	image.addCssClass("noborder");    	
    }
*/
    if(mouseover.hasContent()){
        image.addAttribute("data-hover", mouseoverSrc);
        image.addCssClass("rolloverImage");
    }
    //remove title attribute unless there is something explicitly in the dialog
    if(properties.get("image/jcr:title","").trim().equals("")) {
        image.setTitle(" ");
    }
    
    image.loadStyleData(currentStyle);
    image.setSelector(".img"); // use image script
    image.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        image.setSuffix(currentDesign.getId());
    }

//    String addClass = (!image.hasContent() && !(WCMMode.fromRequest(request) == WCMMode.EDIT)) ? " noimage" : ""; // add a class if there is no image, css can hide it.
    %>
          
    <div class="textimageContainer clearfix
        <%= colorTheme.equals("") ? "" : (" ").concat(colorTheme) %>
        <%= designTheme.equals("") ? "" : (" ").concat(designTheme) %>
        <%= imageAlign.equals("") ? "" : (" ").concat(imageAlign) %>">
    
        <div class="imageAndCaption<%= (!image.hasContent() && !(WCMMode.fromRequest(request) == WCMMode.EDIT)) ? " visuallyHidden" : "" %>">
        
            <div class="imageContainer<%= properties.get("addBorder","").equals("true") ? "" : " noBorder" %>">
            
<%

				if(!linkTarget.equals("")){ %>
				<a href="<%= linkTarget %>" <%= properties.get("externalLink","false").equals("true") ? "target=\"_blank\"" : "" %>>
				<% }
				image.draw(out);
				if(!linkTarget.equals("")){ %></a><% }

%>
            
            
            </div>
            
            <% if(!properties.get("credit","").isEmpty()||!properties.get("jcr:caption","").isEmpty()||!hiresSrc.isEmpty()){ %>
            <div class="captionAndCredit">
            
                <cq:text property="jcr:caption" tagName="span" placeholder="" tagClass="caption" />
                <cq:text property="credit" tagName="span" placeholder="" tagClass="credit" />
                <% if(hires.hasContent()) { %>                
                  <span class="hiRes"><a href="<%= hiresSrc %>" target="_blank">Click for high-res version</a></span>
                <% } %>

            </div>
            <% } %>
        
        </div>
        
        <cq:text property="text" tagName="div" tagClass="textContainer" />
    
        <div class="clear"></div>
    
    </div>
    
    <cq:includeClientLib js="apps.shared.textimage" />
