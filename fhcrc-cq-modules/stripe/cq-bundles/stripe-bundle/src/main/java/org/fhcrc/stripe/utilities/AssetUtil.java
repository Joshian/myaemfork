package org.fhcrc.stripe.utilities;

import com.day.cq.dam.api.Asset;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AssetUtil{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(AssetUtil.class);
	
	public static String getAssetProperty(SlingHttpServletRequest request, String assetPath, String propertyName)
	{		
		try
		{
			ResourceResolver resourceResolver = request.getResourceResolver();
	        Resource res = resourceResolver.getResource(assetPath);
	        Asset imageAsset = res.adaptTo(Asset.class);
	        String imageDesc = imageAsset.getMetadataValue(propertyName); 
	        return imageDesc;
		}
		catch (Exception exception)
		{
			LOGGER.error("There is an exception getting AssetProperty :" + exception);
			return "" ;
		}
	}
}