<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text, 
                 java.util.*,
                 com.day.cq.search.QueryBuilder, 
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.result.Hit,
                 com.day.cq.tagging.Tag,
                 com.day.cq.tagging.TagManager,
                 java.text.SimpleDateFormat,
                 java.text.DateFormat,
                 com.day.cq.wcm.api.WCMMode,
                 org.fhcrc.publicsite.constants.Constants,
                 org.fhcrc.tools.FHUtilityFunctions,
                 org.fhcrc.tools.FieldUtility" %>
<%
Date defaultDate = new Date();
boolean omitHeader = properties.get("omitHeader",Boolean.FALSE);

Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);
TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
/* searchTerm is the name of the variable that should be passed to the component via the URL */
String queryString = slingRequest.getParameter("searchTerm");
String[] tags = properties.get("searchTags", new String[0]);
String[] excludeTagStrings = properties.get("excludeTags", new String[0]);
Tag[] excludeTags = new Tag[excludeTagStrings.length];
if(excludeTags.length > 0) {
    for(int i=0; i<excludeTags.length; i++){
        excludeTags[i] = tagManager.resolve(excludeTagStrings[i]);
    }
}
/* Begin building the predicateGroup for the search */ 
HashMap<String, String> map = new HashMap<String, String>();
/* 1_group contains the different news sources that can be selected via checkbox in the component dialog */
map.put("1_group.p.or","true");
if(properties.get("centernews","false").equals("true")){
    map.put("1_group.1_path",Constants.PATH_TO_CENTERNEWS_FOLDER); // "/content/public/en/news/center-news"
}
if(properties.get("petridish","false").equals("true")){
    map.put("1_group.2_path",Constants.PATH_TO_PETRIDISHIMPORTER_FOLDER); // "/content/public/en/news/petridish-import"
}
if(properties.get("pressreleases","false").equals("true")){
    map.put("1_group.3_path",Constants.PATH_TO_NEWSRELEASES_FOLDER); //"/content/public/en/news/releases"
}
if(properties.get("hutchMagazine","false").equals("true")){
    map.put("1_group.4_path",Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER); //"/content/public/en/news/hutch-magazine"
}
/* new news source Mar/2012 */
if(properties.get("spotlight","false").equals("true")){
    map.put("1_group.5_path",Constants.PATH_TO_SCIENCESPOTLIGHT_FOLDER); 
}
/* new news source Feb 2013 */
if(properties.get("vidd","false").equals("true")){
    map.put("1_group.6_path",Constants.PATH_TO_VIDD_NEWS_FOLDER); //"/content/public/en/labs/vaccine-and-infectious-disease/news"
}/* If no sources are set, use the base news path */
/* but never include spotlight in this mix per Gordie. Mar/2012 */
/* whoops...doing it this way created enormous latency. let's
   go back to the old way, and just not tag the spotlight articles if 
   you don't want them found. Apr/2012 */
/* Since VIDD news doesn't live in the news directory, no need to change anything here, just add the check for the VIDD checkbox - Feb 2013 */
if(properties.get("centernews","false").equals("false") && properties.get("petridish","false").equals("false") && properties.get("pressreleases","false").equals("false") && properties.get("hutchMagazine","false").equals("false") && properties.get("spotlight","false").equals("false") && properties.get("vidd","false").equals("false")){
    map.put("path",Constants.PATH_TO_NEWS_FOLDER); // "/content/public/en/news"
}
/* If we have any search tags, add them to the search IN THEIR OWN GROUP, 2_group */
if(tags.length > 0){
    if("true".equals(properties.get("tagsAnd","false"))){
        map.put("2_group.p.and","true");
    } else {
        map.put("2_group.p.or","true");
    }
    for(int i=1; i<=tags.length; i++) {
        map.put("2_group."+Integer.toString(i)+"_tagid",tags[i-1]);
        map.put("2_group."+Integer.toString(i)+"_tagid.property","jcr:content/cq:tags");
    }
}
/* We only care about pages that are News Page templates */
map.put("type","cq:Page");
map.put("property","jcr:content/cq:template");
map.put("property.value","/apps/public/templates/news");
map.put("p.limit","100000"); //Temp fix to display all results. Change when/if pagination is implemented.
map.put("p.guessTotal","true"); // Added to fix Oak 1.0.18 problem

/* Handle any date filters. Mar/2012 */
Date startdate = properties.get("startdate",Date.class);
Date enddate = properties.get("enddate",Date.class);
if(startdate!=null){
    DateFormat f1= new SimpleDateFormat("yyyy-MM-dd'T00:00:00.000-06:00'"); // assets will have daylight savings time
    map.put("4_group.1_daterange.lowerBound",f1.format(startdate)); //f1.format(startdate)//Integer.toString(startdate.getYear())+'-'+Integer.toString(startdate.getMonth())+'-'+Integer.toString(startdate.getDay())
    map.put("4_group.1_daterange.lowerOperation",">=");
    map.put("4_group.1_daterange.property","@jcr:content/pubDate/date");
    log.debug("start date = " + f1.format(startdate));
} else {
	String relative_startdate = properties.get("startdateRelative","");
    if( !relative_startdate.equals("") ){
		map.put("7_relativedaterange.lowerBound", relative_startdate);
        map.put("7_relativedaterange.property","@jcr:content/pubDate/date");
        log.debug("relative start date = " + relative_startdate);
    }
}
if(enddate!=null){
    DateFormat f2= new SimpleDateFormat("yyyy-MM-dd'T23:59:59.999-09:00'"); //   built in...so cover the worst case scenario.
    map.put("4_group.2_daterange.upperBound",f2.format(enddate)); //f1.format(enddate)//Integer.toString(startdate.getYear())+'-'+Integer.toString(startdate.getMonth())+'-'+Integer.toString(startdate.getDay())
    map.put("4_group.2_daterange.upperOperation","<=");
    map.put("4_group.2_daterange.property","@jcr:content/pubDate/date");
    log.debug("stop date = " + f2.format(enddate));
}
//map.put("4_group.p.or","false");
// http://dev.day.com/discussion-groups/content/lists/cq-google/2010-11/2010-11-30__day_communique____operator_in_daterange_query_sharat.html/2?q=Querybuilder



/* the default is to sort by reverse chron... */
map.put("orderby","@jcr:content/pubDate/date"); // changed to date sorting, sld.
map.put("orderby.sort","desc"); // added reverse chron, sld.
map.put("orderby.index","true");

/* ...unless the user has expressed a choice: */
String orderby = properties.get("orderBy",""); log.info("order by = " + orderby);
if(orderby.equals("alpha")){
    map.put("orderby","@jcr:content/jcr:title"); 
    map.put("orderby.sort","asc"); 
    map.put("orderby.index","true");    
}

/* but if there is a query string, then do a fulltext search on that string and order results by score */   
if(queryString!=null && !queryString.trim().equals("")){
    queryString = queryString.replaceAll(" ", " OR ");
    map.put("3_group.p.or","true");
    map.put("3_group.1_fulltext",queryString);
//    map.put("3_group.1_fulltext.relPath", "jcr:content");
    map.put("3_group.2_fulltext",queryString);
    map.put("3_group.2_fulltext.relPath", "@cq:tags");
    map.put("orderby","@jcr:score"); /* override whatever defaults or choices were made */
    map.put("orderby.sort","desc");
} 

%>

<div class="newsfindercontainer">
  <%-- optional header widget. note this precludes you from programmatically hooking up the more button to a specific search. --%>
<% 
if (!omitHeader) { 
%>
  <cq:include resourceType="/apps/public/components/content/header" path="includedHeaderWidget" />
<%
}

List<Hit> hitList = builder.createQuery(PredicateGroup.create(map), session).getResult().getHits();
List<Page> pageList = new ArrayList<Page>();
if(properties.get("searchBox","false").equals("true")){
    %>
        <cq:include resourceType="/apps/public/components/content/searchWidget" path="includedSearchBox" />
    <%
}
if(hitList.size() == 0){
%>  
    <cq:include script="empty.jsp"/>
<%  
} else {
    for(int i = 0; i < hitList.size(); i++){
      Page testPage = hitList.get(i).getResource().adaptTo(Page.class);
      Boolean skip = false;
      // omit the import pages themselves (which are news pages too)
      if(testPage.getPath().equals(Constants.PATH_TO_SCIENCESPOTLIGHTIMPORTER_FOLDER)||
         testPage.getPath().equals(Constants.PATH_TO_PETRIDISHIMPORTER_FOLDER)||
         testPage.getPath().equals(Constants.PATH_TO_HUTCH_MAG_IMPORTER_FOLDER)||
         testPage.getPath().equals(Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER)){ /* there has to be a better way.... */
        skip=true;
      }
      if(excludeTags.length > 0){ //Check to see if the page we are looking at should not be added to the list
          Tag[] pageTags = testPage.getTags();
          for(Tag s : pageTags){
              for(Tag t : excludeTags){
                 if(t!=null &&  t.equals(s)){
                    skip = true;
                    break; // No need to continue the loop if we get a match
                 }
              }
              if(skip == true){
                  break; // No need to continue the loop if we get a match
              }
          }
      }
      if(skip == false){
          pageList.add(testPage); 
      }
    }

    Iterator<Page> pageIterator = pageList.iterator();

    if(!pageIterator.hasNext()){
%>  
    <cq:include script="empty.jsp"/>
<%      
    } else {
%>
<div class="related-news">
<%

boolean omitSource = properties.get("omitSource",Boolean.FALSE);
boolean omitDate = properties.get("omitDate",Boolean.FALSE);
boolean showSubtitle = properties.get("showSubtitle",Boolean.FALSE);
boolean showDescription = properties.get("showDescription",Boolean.FALSE);
boolean showThumbnails = properties.get("showThumbnails",Boolean.FALSE);
Integer maxnum = properties.get("maxnum",3);

Integer ct = 0;
while(pageIterator.hasNext() && (maxnum.equals(0)||(ct++<maxnum))){
    Page nextPage = pageIterator.next();
    String pageDescription = nextPage.getDescription();

    String source="";
    if(nextPage.getPath().contains(Constants.PATH_TO_CENTERNEWS_FOLDER)){        source = " in <a href='"+Constants.PATH_TO_NEWS_FOLDER+".html'>Hutch News</a>"; }
    if(nextPage.getPath().contains(Constants.PATH_TO_PETRIDISHIMPORTER_FOLDER)){ source = " in <a href='http://questmagazine.wordpress.com' target='_blank'>Petri Dish</a>"; } 
    if(nextPage.getPath().contains(Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER)){    source = " in <a href='"+ Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER+".html'>Hutch Magazine</a>"; } 
    if(nextPage.getPath().contains(Constants.PATH_TO_NEWSRELEASES_FOLDER)){      source = " in <a href='"+ Constants.PATH_TO_NEWSRELEASES_FOLDER+".html'>News Releases</a>"; } 
    if(nextPage.getPath().contains(Constants.PATH_TO_SCIENCESPOTLIGHT_FOLDER)){  source = " in <a href='"+ Constants.PATH_TO_SCIENCESPOTLIGHT_FOLDER+".html'>Science Spotlight</a>"; } 
    if(nextPage.getPath().contains(Constants.PATH_TO_VIDD_NEWS_FOLDER)){         source = " in <a href='"+ Constants.PATH_TO_VIDD_NEWS_FOLDER+".html'>VIDD News</a>"; } 
    
    Date pubdate = nextPage.getProperties().get("pubDate/date", nextPage.getProperties().get("cq:lastModified", Date.class)); 

    String description = nextPage.getProperties().get("jcr:description","");
    String subtitle = nextPage.getProperties().get("newssubtitle/text","");
    String imagehtml = "<img src='"+ resourceResolver.map(nextPage.getPath()) +".pagethumb.140.105.fit.png' class='thumbnail frame' alt='' title=''>";

%>
    <div<%= showThumbnails ? " class=\"thumbnails\"" : "" %>>
     <% if (showThumbnails) { %>
     <div class="thumbnailContainer">
     	<%= imagehtml %>
     </div>
     <% } %>
     <%= showThumbnails ? "<div class=\"textContainer\">" : "" %>
     <span class="news-title"><a href="<%= nextPage.getPath() %>.html"><%= FHUtilityFunctions.displayTitle(nextPage) %></a></span><br />
     <% if(showSubtitle && !"".equals(subtitle)) { %><span class="news-subtitle"><%= FieldUtility.cleanField(subtitle) %></span><br /><% } %>     
     <% if(showDescription && !"".equals(description)){ %><span class="news-description"><%=FieldUtility.cleanField(description)%></span><br /><% } %>
     <% if(!omitDate){ %><span class="news-date"><%=FHUtilityFunctions.APStylize(pubdate)%></span><% } %>
     <% if(!omitSource){ %><span class="news-source"><%=source%></span><% } %>
     <%= showThumbnails ? "</div>" : "" %>
    </div>
<%
}
%>
</div>
<%
}
}
%>

</div>