<%@ page import="com.day.cq.wcm.foundation.Search,com.day.cq.tagging.TagManager,org.fhcrc.tools.FieldUtility" %>
<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Search component

  Draws the search form and evaluates a query.
  
  Comment to see if SVN is working with Eclipse/Maven/SVN. Sld. Oct 27/2013

--%><%@include file="/libs/foundation/global.jsp" %><%
%><cq:setContentBundle/><%
    Search search = new Search(slingRequest);

//search.setHitsPerPage(3);
//out.print(search.getSearchProperties()); // default: ., jcr:title, jcr:description 
//out.print(search.getRelatedQueries());
//out.print(search.getTrends().getMinSize()); // default: 12 (getMaxSize = 18 default)
//out.print(search.getTrends().getMaxQueries()); // default: 20

	// set the minimum and maximum "trends" font sizes. your css needs to match the options you choose here. see span.sizeNN
	search.getTrends().setMinSize(12);
	search.getTrends().setMaxSize(18);
	
	// set the max number of "trends" results you will see. these will be the most popular N queries:
	search.getTrends().setMaxQueries(25);

    String searchIn = (String) properties.get("searchIn");
    String requestSearchPath = request.getParameter("path");
    if (searchIn != null) {
        // only allow the "path" request parameter to be used if it
        // is within the searchIn path configured
        if (requestSearchPath != null && requestSearchPath.startsWith(searchIn)) {
            search.setSearchIn(requestSearchPath);
        } else {
            search.setSearchIn(searchIn);
        }
    } else if (requestSearchPath != null) {
        search.setSearchIn(requestSearchPath);
    }
    
    pageContext.setAttribute("search", search);
    TagManager tm = resourceResolver.adaptTo(TagManager.class);
%><c:set var="trends" value="${search.trends}"/><c:set var="result" value="${search.result}"/><%
%>
  <form action="${currentPage.path}.html" 
  	record="'search', {'keyword': '<c:out value="${search.query}"/>', 'results':'${result.totalMatches}', 'executionTime':'${result.executionTime}'}">
    <input class="searchbox" maxlength="2048" name="q" placeholder="Search this site..." value="${fn:escapeXml(search.query)}" type="text"/>
    <input class="searchbutton gradient" value="<fmt:message key="searchButtonText"/>" type="submit"/>    
  </form>

<br/>
<c:choose>
  <c:when test="${empty result && empty search.query}">
  </c:when>
  <c:when test="${empty result.hits}">
    ${result.trackerScript}
    <c:if test="${result.spellcheck != null}">
      <p><fmt:message key="spellcheckText"/> <a href="<c:url value="${currentPage.path}.html"><c:param name="q" value="${result.spellcheck}"/></c:url>"><b><c:out value="${result.spellcheck}"/></b></a></p>
    </c:if>
    <fmt:message key="noResultsText">
      <fmt:param value="${fn:escapeXml(search.query)}"/>
    </fmt:message>
    <span record="'noresults', {'keyword': '<c:out value="${search.query}"/>', 'results':'${result.totalMatches}', 'executionTime':'${result.executionTime}'}"></span>
  </c:when>
  <c:otherwise>
    ${result.trackerScript}
    
    <%--
    <fmt:message key="statisticsText">
      <fmt:param value="${result.startIndex + 1}"/>
      <fmt:param value="${result.startIndex + fn:length(result.hits)}"/>
      <fmt:param value="${result.totalMatches}"/>
      <fmt:param value="${fn:escapeXml(search.query)}"/>
      <fmt:param value="${result.executionTime}"/>        
    </fmt:message> 
     --%>
    
    Results ${result.startIndex + 1} - ${result.startIndex + fn:length(result.hits)} 
    of <fmt:formatNumber value="${result.totalMatches}" type="number" pattern="###,###,###,###,###"/> 
    for <b>${fn:escapeXml(search.query)}</b>.

    
<%--  # commenting out the whole searchRight div because we aren't using any of its features right now. 
      <div class="searchRight">  --%>
 
<%--
          <c:if test="${fn:length(trends.queries) > 0}">
              <p><fmt:message key="searchTrendsText"/></p>
              <div class="searchTrends">
                  <c:forEach var="query" items="${trends.queries}">
                      <a href="<c:url value="${currentPage.path}.html"><c:param name="q" value="${query.query}"/></c:url>"><span class="size${query.size}"><c:out value="${query.query}"/></span></a>
                  </c:forEach>
              </div>
          </c:if>
--%>
<%-- 
          <c:if test="${result.facets.languages.containsHit}">
              <p>Languages</p>
              <c:forEach var="bucket" items="${result.facets.languages.buckets}">
                  <c:set var="bucketValue" value="${bucket.value}"/>
                  <c:set var="label" value='<%= new java.util.Locale((String) pageContext.getAttribute("bucketValue")).getDisplayLanguage(request.getLocale())'/>
                  <c:choose>
                      <c:when test="${param.language != null}">${label} (${bucket.count}) - <a href="<cq:requestURL><cq:removeParam name="start"/><cq:removeParam name="language"/></cq:requestURL>">remove filter</a></c:when>
                      <c:otherwise><a title="filter results" href="<cq:requestURL><cq:removeParam name="start"/><cq:addParam name="language" value="${bucket.value}"/></cq:requestURL>">${label} (${bucket.count})</a></c:otherwise>
                  </c:choose><br/>
              </c:forEach>
          </c:if>
 --%>
<%--
          <c:if test="${result.facets.tags.containsHit}">
              <p>Tag explorer</p>
              <div class="tagexplorer">
	              <c:forEach var="bucket" items="${result.facets.tags.buckets}">
	                  <c:set var="bucketValue" value="${bucket.value}"/>
	                  <c:set var="tag" value="<%= tm.resolve((String) pageContext.getAttribute("bucketValue")) %>"/>
	                  <c:if test="${tag != null}">
	                      <c:set var="label" value="${tag.title}"/>
	                      <c:choose>
	                          <c:when test="<%= request.getParameter("tag") != null && java.util.Arrays.asList(request.getParameterValues("tag")).contains(pageContext.getAttribute("bucketValue")) %>">${label} (${bucket.count}) - <a href="<cq:requestURL><cq:removeParam name="start"/><cq:removeParam name="tag" value="${bucket.value}"/></cq:requestURL>">remove filter</a></c:when>
	                          <c:otherwise><a title="filter results" href="<cq:requestURL><cq:removeParam name="start"/><cq:addParam name="tag" value="${bucket.value}"/></cq:requestURL>">${label} (${bucket.count})</a></c:otherwise>
	                      </c:choose><br/>
	                  </c:if>
	              </c:forEach>
              </div>
          </c:if>
--%>        
<%-- 
          <c:if test="${result.facets.mimeTypes.containsHit}">
              <jsp:useBean id="fileTypes" class="com.day.cq.wcm.foundation.FileTypes"/>
              <p>File types</p>
              <c:forEach var="bucket" items="${result.facets.mimeTypes.buckets}">
                  <c:set var="bucketValue" value="${bucket.value}"/>
                  <c:set var="label" value="${fileTypes[bucket.value]}"/>
                  <c:choose>
                      <c:when test="<%= request.getParameter("mimeType") != null && java.util.Arrays.asList(request.getParameterValues("mimeType")).contains(pageContext.getAttribute("bucketValue")) %>">${label} (${bucket.count}) - <a href="<cq:requestURL><cq:removeParam name="start"/><cq:removeParam name="mimeType" value="${bucket.value}"/></cq:requestURL>">remove filter</a></c:when>
                      <c:otherwise><a title="filter results" href="<cq:requestURL><cq:removeParam name="start"/><cq:addParam name="mimeType" value="${bucket.value}"/></cq:requestURL>">${label} (${bucket.count})</a></c:otherwise>
                  </c:choose><br/>
              </c:forEach>
          </c:if>
 --%>
<%--
          <c:if test="${result.facets.lastModified.containsHit}">
              <p>Last Modified</p>
              <c:forEach var="bucket" items="${result.facets.lastModified.buckets}">
                  <c:choose>
                      <c:when test="${param.from == bucket.from && param.to == bucket.to}">${bucket.value} (${bucket.count}) - <a href="<cq:requestURL><cq:removeParam name="start"/><cq:removeParam name="from"/><cq:removeParam name="to"/></cq:requestURL>">remove filter</a></c:when>
                      <c:otherwise><a title="filter results" href="<cq:requestURL><cq:removeParam name="start"/><cq:removeParam name="from"/><cq:removeParam name="to"/><c:if test="${bucket.from != null}"><cq:addParam name="from" value="${bucket.from}"/></c:if><c:if test="${bucket.to != null}"><cq:addParam name="to" value="${bucket.to}"/></c:if></cq:requestURL>">${bucket.value} (${bucket.count})</a></c:otherwise>
                  </c:choose><br/>
              </c:forEach>
          </c:if>
 --%>

<%--      </div> --%>

      <c:if test="${fn:length(search.relatedQueries) > 0}">
        <br/><br/>
        <fmt:message key="relatedSearchesText"/>
        <c:forEach var="rq" items="${search.relatedQueries}">
            <a style="margin-right:10px" href="${currentPage.path}.html?q=${rq}"><c:out value="${rq}"/></a>
        </c:forEach>
      </c:if>
      <br/>

      <dl class="hitresults">
      <c:forEach var="hit" items="${result.hits}" varStatus="status">
        <dt>
        <c:if test="${hit.extension != \"\" && hit.extension != \"html\"}">
            <span class="icon type_${hit.extension}"><img src="/etc/designs/default/0.gif" alt="*"></span>
        </c:if>
        <a href="${hit.URL}" onclick="trackSelectedResult(this, ${status.index + 1})">${hit.title}</a>
        </dt>
        <dd>
        
        <%-- use resolver mapping to nuke /content/wecan/ from the display of the hit url: --%>
        <c:set var="mappedurl" value="${hit.URL}"/> 
        <% String mappedurl = (String) pageContext.getAttribute("mappedurl"); %>
        
        <div class="hiturl">www.womenscanceradvocacy.net<%= resourceResolver.map( mappedurl ) %></div>
        <c:set var="excerpt" value="${hit.excerpt}"/>
        <%
            String excerpt = (String) pageContext.getAttribute("excerpt");
            out.write(FieldUtility.cleanField(excerpt));
        %>
        </dd>
      </c:forEach>
      </dl>

      <c:if test="${fn:length(result.resultPages) > 1}">
         <div class="result_paging">
	        <span class="resultPagesText"><fmt:message key="resultPagesText"/></span>
	        <c:if test="${result.previousPage != null}">
	          <span class="pagetile previouspage"><a href="${result.previousPage.URL}"><fmt:message key="previousText"/></a></span>
	        </c:if>

            <% // locate the index of the current page: --%>
            <c:set var="index_of_current_page" value="-1"/>
            <c:forEach var="page" items="${result.resultPages}">
              <c:choose>
                <c:when test="${page.currentPage}"><c:set var="index_of_current_page" value="${page.index}"></c:set></c:when>
              </c:choose>
            </c:forEach>
            
	        <c:forEach var="page" items="${result.resultPages}"> 
	          <c:choose>
	            <c:when test="${page.currentPage}"><span class="pagetile currentpage">${page.index + 1}</span></c:when>
	            <c:otherwise>
	              <c:if test="${page.index>index_of_current_page}">
	                <c:choose>
	                  <c:when test="${index_of_current_page < 2}">
                         <c:if test="${page.index<5}">
                           <span class="pagetile"><a href="${page.URL}">${page.index + 1}</a></span>
                         </c:if>	                    
	                  </c:when>
                      <c:otherwise>
                         <c:if test="${page.index-index_of_current_page<3}">
                           <span class="pagetile"><a href="${page.URL}">${page.index + 1}</a></span>
                         </c:if>
                      </c:otherwise>
	                </c:choose>
                  </c:if>
                  <c:if test="${page.index<index_of_current_page}">
                    <c:if test="${page.index-index_of_current_page>-3}">
                      <span class="pagetile"><a href="${page.URL}">${page.index + 1}</a></span>
                    </c:if>
                  </c:if>
	            </c:otherwise>
	          </c:choose>
	        </c:forEach>

	        <c:if test="${result.nextPage != null}">
	          <span class="pagetile nextpage"><a href="${result.nextPage.URL}"><fmt:message key="nextText"/></a></span>
	        </c:if>
	     </div>
      </c:if>
  </c:otherwise>
</c:choose>
