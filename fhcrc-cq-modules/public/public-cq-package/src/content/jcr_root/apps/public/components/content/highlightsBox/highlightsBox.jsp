<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.WCMMode,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    java.lang.Integer,
    java.util.Comparator,
    java.util.Arrays,
    org.fhcrc.tools.FHUtilityFunctions" %>
<%

/* The arrays are of length 2: x[0] is the order of the slide (as given by user input)
 * x[1] is the number of the slide (1-6). If the order is not given by the user, the 
 * number of the slide is used as the order. 
*/

final int NUMBER_OF_SLIDES = 10;

Comparator<Float[]> arrayCompare = new Comparator<Float[]>() {
    public int compare(Float[] a, Float[] b){
        Float orderA = a[0];
        Float orderB = b[0];
        if(orderA==null){
            orderA = (float)100;
        }
        if(orderB==null){
            orderB = (float)100;
        }
    
        if(orderA.equals(orderB)){
            return a[1].compareTo(b[1]);
        }
        return orderA.compareTo(orderB);
    }
};

/* The metadata is a series of Float arrays, 6 arrays that each contain 2 points of data. */
Float[][] slideMetadata = new Float[NUMBER_OF_SLIDES][2];
Float tmpOrder;

for(int metacounter = 0; metacounter < NUMBER_OF_SLIDES; metacounter++) {
    /* If the user doesn't enter an order, set the order at 100 (reasonably large enough that it should come last) */
    tmpOrder = Float.parseFloat(properties.get("highlight_"+(metacounter+1)+"/order","100"));
    slideMetadata[metacounter][0] = tmpOrder;
    slideMetadata[metacounter][1] = (float)(metacounter+1);
}

Arrays.sort(slideMetadata, arrayCompare);

int maxSlides = NUMBER_OF_SLIDES;
String[] linkTargets = new String[maxSlides];
String[] textContent = new String[maxSlides];
String[] linkTexts = new String[maxSlides];
Image[] imageArray = new Image[maxSlides];
String temp = "";
String textProperty = "";
String linkTextProperty  = "";
int hashCode = resource.hashCode();
String speed = properties.get("speed","0");
String restrictWidth = properties.get("width","0");
int slideNumber;

for(int i = 0; i < maxSlides; i++){
	
	slideNumber = slideMetadata[i][1].intValue();
	/* First put the linkTargets into their array */
    temp = properties.get("highlight_"+(slideNumber)+"/linkTarget","");
    if(!"".equals(temp)){
        temp = FHUtilityFunctions.cleanLink(temp, resourceResolver);
    }
    linkTargets[i] = temp;
    /* Then the text content */
    textContent[i] = properties.get("highlight_"+(slideNumber)+"/text","");
    /* Then the text for the links */
    linkTexts[i] = properties.get("highlight_"+(slideNumber)+"/linkText","");
    /* And finally the images */
    imageArray[i] = new Image(resource, "image"+(slideNumber));

}

%>
<cq:includeClientLib categories="public.components.highlightsBox" />
<div class="highlightsBoxContainer" id="HB<%= hashCode %>">
    <cq:text property="title" tagName="h3" placeholder="Title" tagClass="underline" />
    <div class="panels">
        <dl>
<%
for(int i = 0; i < maxSlides; i++){
	if(!textContent[i].equals("") || !linkTexts[i].equals("")){  // was: || WCMMode.fromRequest(request) == WCMMode.EDIT
		slideNumber = slideMetadata[i][1].intValue();
	    textProperty = "highlight_"+(slideNumber)+"/text";
	    linkTextProperty = "highlight_"+(slideNumber)+"/linkText";
%>
            <dt>Highlight <%= i+1 %></dt>
            <dd class="<%
            if( !(imageArray[i].hasContent() )){    
            	out.print("noimage ");
            }
            if( i != 0 ) {
            	out.print("hiddenSlide");
            }
            %>"><%
                if (imageArray[i].hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT) {
                	
                	/* Set the image src attribute to avoid 301 redirects */
                	if (imageArray[i].hasContent()) {
	                	imageArray[i] = FHUtilityFunctions.nukeURLPrefix(imageArray[i], resourceResolver);
                	}
/*
					String imageSrc = imageArray[i].getSrc();
                	imageSrc = FHUtilityFunctions.addImageSelector(imageSrc);
                	imageArray[i].setSrc(resourceResolver.map(imageSrc));
*/                	
                	
				    imageArray[i].loadStyleData(currentStyle);
				    // add design information if not default (i.e. for reference paras)
				    if (!currentDesign.equals(resourceDesign)) {
				    	imageArray[i].setSuffix(currentDesign.getId());
				    }
				    //drop target css class = dd prefix + name of the drop target in the edit config
				    imageArray[i].addCssClass(DropTarget.CSS_CLASS_PREFIX + "image" + (i+1));
				    /* Design no longer has frame class as default, so removing */
//				    imageArray[i].addCssClass("frame");
				    imageArray[i].setSelector(".img");
				    imageArray[i].setDoctype(Doctype.fromRequest(request));
				
				    // div around image for additional formatting
				    %><div class="image"><%
				    if( !"".equals(linkTargets[i]) ){
				    %><a href="<%= linkTargets[i] %>">
				    <% imageArray[i].draw(out); %></a>
				    <% } else {
				    imageArray[i].draw(out);
				    } %>
				    </div>
                <% } %>
                <div class="text">
                    <cq:text property="<%= textProperty %>" /><br />
                    <% if( !"".equals(linkTargets[i]) ){ %>
                    <a href="<%= linkTargets[i] %>"><cq:text property="<%= linkTextProperty %>" placeholder="" /></a>&nbsp;<span class="raquo">&gt;</span>
                    <% } else { %>
                    <cq:text property="<%= linkTextProperty %>" placeholder="" />                    
                    <% } %>
                </div>
            </dd>
<%
	}
} 
%>
        </dl>
        <div class="status">
            <span class="currentPanel">1</span> of <span class="totalPanels">1</span> | <a href="#" class="nextButton">Next</a>&nbsp;<span class="raquo">&gt;</span>
        </div>
    </div>
</div>
<script type="text/javascript">
$(window).load(function(){
	var theScope = $("#HB<%= hashCode %>");
    componentApp.highlightsBox.instantiateHighlightsBox(theScope, <%= speed %>, <%= restrictWidth %>);
    
});
</script>