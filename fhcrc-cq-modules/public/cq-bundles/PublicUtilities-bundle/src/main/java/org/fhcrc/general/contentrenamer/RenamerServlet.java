/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhcrc.general.contentrenamer;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
@Service
@Properties({
    @Property(name="service.description", value="Content Renaming Servlet"),
    @Property(name="service.vendor", value="FHCRC"),
    @Property(name="sling.servlet.paths",value="/libs/fhcrc/RenameServlet"),
    @Property(name="sling.servlet.extensions",value="html")
})
public class RenamerServlet extends SlingSafeMethodsServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Logger log = LoggerFactory.getLogger(getClass());
    private String startPath = "/content/public/en/news";
    private String findNodeName =  "subtitle";
    private String replacementName = "newssubtitle";
    private boolean isTest = false;
    @Reference
    private Renamer renamer;
    
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException {
       List<String> results = renamer.rename(startPath, findNodeName, replacementName, isTest);
   	   try {
	       if (!results.isEmpty()) {
		       for (String result : results) {
						response.getOutputStream().println(result + "<br>");
		       } 
	       } else {
				response.getOutputStream().println("No Results Found");
	       }
	   } catch (IOException e) {
		   log.error("", e);
	   }
    }
}