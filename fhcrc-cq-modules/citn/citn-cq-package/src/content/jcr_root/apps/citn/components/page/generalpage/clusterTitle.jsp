<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text,
        com.day.cq.wcm.api.NameConstants,
        org.apache.commons.lang.StringEscapeUtils,
        com.day.cq.wcm.api.Page" %>
<%
Page clusterHead = currentPage;
String test = clusterHead.getProperties().get("isClusterHead", "false");
while(!"true".equals(test)){
    if(clusterHead.getParent() == null){
        // If we have hit the top of the tree, then use the branch leader as the clusterHead
        clusterHead = currentPage.getAbsoluteParent(3);
        break;
    }
    clusterHead = clusterHead.getParent();
    test = clusterHead.getProperties().get("isClusterHead", "false");
}
String clusterTitle = clusterHead.getNavigationTitle();
if(clusterTitle == null || clusterTitle.equals("")){
    clusterTitle = clusterHead.getTitle();
}
if(clusterTitle == null || clusterTitle.equals("")){
    clusterTitle = clusterHead.getName();
}
if(clusterHead.equals(currentPage)){
%>
  <h1 class="cluster_title"><cq:include script="share_options.jsp"/><%= StringEscapeUtils.escapeHtml(clusterTitle) %></h1>
<%
} else { %>
  <p class="cluster_title"><cq:include script="share_options.jsp"/><%= StringEscapeUtils.escapeHtml(clusterTitle) %></p>
<%
}
%>

<% // store the cluster head object for other scripts to use.
   // for example, check out general/eventsubpage.jsp
   request.setAttribute("clusterHead", clusterHead);
%>