<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Download component

  Draws a download link with icons.

--%><%@ page import="com.day.cq.wcm.foundation.Download,
        com.day.cq.wcm.api.components.DropTarget,
        com.day.cq.wcm.api.WCMMode,
        com.day.cq.wcm.commons.WCMUtils,
        com.day.text.Text,
        org.apache.commons.lang.StringEscapeUtils,
        java.util.Map" %><%
%><%@include file="/libs/foundation/global.jsp"%><%
    //drop target css class = dd prefix + name of the drop target in the edit config
    String ddClassName = DropTarget.CSS_CLASS_PREFIX + "file";

    Download dld = new Download(resource);
    if (dld.hasContent()) {
        dld.addCssClass(ddClassName);
        String title = dld.getTitle(true);
        String linkText = dld.getDescription(true);
        String href = Text.escape(dld.getHref(), '%', true);
        %><div>
            <span class="icon type_<%= dld.getIconType() %>"><img src="/etc/designs/default/0.gif" alt="*"></span>
            <a href="<%= href%>" target="_blank" title="<%=title%>"<%
                Map<String,String> attrs = dld.getAttributes();
            if ( attrs!= null) {
                for (Map.Entry e : attrs.entrySet()) {
                    out.print(StringEscapeUtils.escapeHtml(e.getKey().toString()));
                    out.print("=\"");
                    out.print(StringEscapeUtils.escapeHtml(e.getValue().toString()));
                    out.print("\"");
                }
            }%>
        ><%= (linkText == null || linkText.trim().equals("")) ? "File Description" : linkText %></a><br />
        <cq:text property="instructions" tagClass="downloadInstructions" tagName="span" placeholder="" />
        </div><div class="clear"></div><%
        
    } else if (WCMMode.fromRequest(request) == WCMMode.EDIT) {
        %><img src="/libs/cq/ui/resources/0.gif" class="cq-file-placeholder <%= ddClassName %>" alt=""><%
    }
%>