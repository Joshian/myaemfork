<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode,
    org.fhcrc.publicsite.functions.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
String colorClass = properties.get("color","none");
String isArrow = properties.get("arrow","true");
String linkTarget = properties.get("linkTarget","");
String valign = properties.get("valign", "");
String noImageClassName = "noImage";
String noArrowClassName = "noArrow";

//If the linkTarget is an internal page, we need to concat a ".html"
Resource r;     
if(!linkTarget.equals("")){
	linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
}

Image image = new Image(resource, "image");
//drop target css class = dd prefix + name of the drop target in the edit config
image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");

if (properties.get("image/jcr:title","").trim().equals("")) {
    image.setTitle(" ");
}

image.loadStyleData(currentStyle);
image.setSelector(".img"); // use image script
image.setDoctype(Doctype.fromRequest(request));
// add design information if not default (i.e. for reference paras)
if (!currentDesign.equals(resourceDesign)) {
    image.setSuffix(currentDesign.getId());
}

%>


<a href="<%= linkTarget %>">

    <div class="mainContainer<%= colorClass.equals("none") ? "" : " ".concat(colorClass) %>
    <%= image.hasContent() ? "" : " ".concat(noImageClassName) %>
    <%= isArrow.equals("true") ? "" : " ".concat(noArrowClassName) %>">

    <%
    if (image.hasContent()) {
    %>    
        <div class="photoContainer tableCell">
            <% image.draw(out); %>
        </div>
    <%
    }
    %>
    
        <div class="textContainer tableCell<%= valign.equals("") ? "" : " ".concat(valign) %>">
            <cq:text property="title" tagName="h2" />
            <cq:text property="subtitle" tagName="h3" placeholder="" />
        </div>
        
    <%
    if (isArrow.equals("true")) {
    %>
    
        <div class="arrowContainer tableCell<%= valign.equals("") ? "" : " ".concat(valign) %>">
            <div class="arrow">&gt;</div>
        </div>
    <%
    }
    %>
    
    </div>

</a>