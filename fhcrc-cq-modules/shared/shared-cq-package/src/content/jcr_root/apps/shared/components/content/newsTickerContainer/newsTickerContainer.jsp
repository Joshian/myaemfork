<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.fhcrc.tools.FHUtilityFunctions,
                 java.util.Iterator,
                 java.lang.Integer,
                 com.day.cq.wcm.api.designer.*" %>
<%@page session="false" %><%

final Integer HEIGHT_DEFAULT = new Integer(220);
final Integer OFFSET_DEFAULT = new Integer(2);
final Integer MAXWORDS_DEFAULT = new Integer(64);
final Integer MAXNUM_DEFAULT = new Integer(2);
final String BGCOLOR_DEFAULT = "#ffffff";
final String BGHOVER_DEFAULT = "#ffffff";
final Integer SPEED_DEFAULT = new Integer(5000);

Style componentStyle = currentDesign.getStyle(resource);

String pageLocation = properties.get("pageLocation","");
Integer speed = new Integer(properties.get("speed",SPEED_DEFAULT));
Integer maxNum = new Integer(componentStyle.get("maxNum",MAXNUM_DEFAULT));
Integer height = new Integer(componentStyle.get("height",HEIGHT_DEFAULT));
Integer offset = new Integer(componentStyle.get("offset",OFFSET_DEFAULT));
Integer maxWords = new Integer(componentStyle.get("maxWords",MAXWORDS_DEFAULT));
String bgColor = componentStyle.get("bgColor",BGCOLOR_DEFAULT);
String bgHover = componentStyle.get("bgHover",BGHOVER_DEFAULT);

%>

<div id="fancy-news">

<%

if (!pageLocation.trim().equals("") && resourceResolver.getResource(pageLocation) != null) {
	
	Page newsPage = resourceResolver.getResource(pageLocation).adaptTo(Page.class);
	
/* Null pointer check: do we have a Paragraph System on the Media Coverage page? */    
    if(newsPage.getContentResource("mainPar") != null){
        Iterator<Resource> nodeIterator = newsPage.getContentResource("mainPar").listChildren();
/* Loop through the elements in the par until we hit the last one or get the maximum number of media coverage items */        
        while(nodeIterator.hasNext()){
            Resource theNode = nodeIterator.next();
            if(theNode.getResourceType().contains("newsTickerItem")){
%>
            	<sling:include path="<%= theNode.getPath() %>" />
<%
            }
            
        }

    } else {
/* The media coverage page's parsys is returning a null pointer. Include a script to display this error. */
%> <cq:include script="noPar.jsp" /><%
    }
	
} else {
%>
	<cq:include script="noPage.jsp"/>
<%	
}
%>
<!-- End of fancy-news container -->
</div>

<cq:includeClientLib js="apps.shared.fancyNews"/>
<script type="text/javascript">

var intwidth = '100%';
var intheight = <%= height %>;
var intoffset = <%= offset %>;
var intmaxWords = <%= maxWords %>;
var intpreviewsPerPage = <%= maxNum %>;
var intslideTime = <%= speed %>;
// feed:'URL_OF_RSS_FEED'	  
var strbackgroundColor = '<%= bgColor %>'; // '#ffffff';
var strbackgroundOverColor = '<%= bgHover %>'; // '#ffffff';
var booluseLinks = true;
var strtargetWindow = '_blank';
var boolinfiniteLoop = true;
var boolarrows = true;
var boolvertical = false;

if (window.matchMedia) {

	if (window.matchMedia("(min-width: 960px)").matches) {
		intheight = <%= height %>;
		intpreviewsPerPage = <%= maxNum %>;
	
	} else if (window.matchMedia("(min-width: 720px)").matches) {
		intheight = 240;
		intpreviewsPerPage = 2;

	} else if (window.matchMedia("(min-width: 540px)").matches) {
		intheight = 140;
		intpreviewsPerPage = 1;
	
	} else if (window.matchMedia("(min-width: 480px)").matches) {
		intheight = 160;
		intpreviewsPerPage = 1;

	} else if (window.matchMedia("(min-width: 360px)").matches) {
		intheight = 180;
		intpreviewsPerPage = 1;
	}

}

function setParameters() {
	$('#fancy-news').fancyNews({
		width: intwidth, 
		height: intheight,
		offset: intoffset,
		maxWords: intmaxWords,
		previewsPerPage: intpreviewsPerPage,
		slideTime: intslideTime,
		// feed:'URL_OF_RSS_FEED'	  
		backgroundColor: strbackgroundColor,
		backgroundOverColor: strbackgroundOverColor,
		useLinks: booluseLinks,
		targetWindow: strtargetWindow,
		infiniteLoop: boolinfiniteLoop,
		arrows: boolarrows,
		vertical: boolvertical	
	});
}

$(document).ready(function(){
	setParameters();
});

</script>