<%@ page import="com.day.cq.commons.Doctype,
    java.util.regex.*,
    com.day.cq.wcm.foundation.Image,
    com.day.cq.wcm.api.WCMMode" %>
<%@include file="/libs/foundation/global.jsp"%><%@ page import="java.util.Calendar" %>
<%
String source = properties.get("source",""),
  videoId = "";
Image image = new Image(resource, "image");
Pattern videoIdPattern = Pattern.compile("https?:\\/\\/(?:[0-9A-Z-]+\\.)?(?:youtu\\.be\\/|youtube\\.com\\S*[^\\w\\-\\s])([\\w\\-]{11})(?=[^\\w\\-]|$)(?![?=&+%\\w]*(?:[\'\"][^<>]*>|<\\/a>))[?=&+%\\w-]*", Pattern.CASE_INSENSITIVE);
/*Pattern videoIdPattern = Pattern.compile("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\\/)[^&\n]+(?=\\?)|(?<=v=)[^&\n]+|(?<=youtu\\.be/)[^&\n]+#");*/
Matcher myMatcher;

if (!source.trim().equals("")) {
	
	myMatcher = videoIdPattern.matcher(source);
	
	if (myMatcher.find()) {
		
		videoId = myMatcher.group(1);
		
	} else {
		
		videoId = "";
		
	}
}

/* Prep the image for insertion */
/* remove title attribute unless there is something explicitly in the dialog */
if(properties.get("image/jcr:title","").trim().equals("")) {
    image.setTitle(" ");
}
image.loadStyleData(currentStyle);
image.setSelector(".img"); // use image script
image.setDoctype(Doctype.fromRequest(request));
/* add design information if not default (i.e. for reference paras) */
if (!currentDesign.equals(resourceDesign)) {
    image.setSuffix(currentDesign.getId());
}



%>

<div class="originalSlideContainer" data-source="<%= videoId %>">

<div class="videoThumbnail">

    <a href="<%= source %>" target="_blank">

<%
image.draw(out);
%>

    </a>
    
</div>

<a class="videoTitle" href="<%= source %>" target="_blank">
    <cq:text property="title" tagName="h4" />
</a>

<cq:text property="description" tagName="p" tagClass="videoDescription" />

</div>