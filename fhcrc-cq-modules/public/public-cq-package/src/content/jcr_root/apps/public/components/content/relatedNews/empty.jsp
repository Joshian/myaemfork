<%--
  Copyright 1997-2009 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  List component sub-script

--%><%@ page session="false" import="com.day.cq.wcm.api.WCMMode"%><%
%><%@include file="/libs/foundation/global.jsp"%><%
      out.print("<p class='cq-edit-only'>Related news articles - Cannot determine news articles related to this one or author has disabled the component on this page. Solutions: Uncheck the 'disable' checkbox, manually add articles you want to see, or add tags to this page using Page Properties.</p>");
%>