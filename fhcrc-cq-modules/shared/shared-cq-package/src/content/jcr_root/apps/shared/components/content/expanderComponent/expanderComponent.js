/*
 * Expander component
 * Creation date 02/08/2016
 * ICS
 **/
"use strict";

use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js"], function (AuthoringUtils) {
   
    var CONST = {
        PROP_TITLE: "title",
        PROP_INTRO_TEXT: "intro",
        PROP_OPEN_BY_DEFAULT: "defaultOpen",
        PROP_COLOR_THEME: "colortheme",
        TITLE_DEFAULT_TEXT: "Please configure the component to add a title"
    };

    var expander = {};

    expander.title = granite.resource.properties[CONST.PROP_TITLE] 
        || "";

    expander.intro = granite.resource.properties[CONST.PROP_INTRO_TEXT]
        || "";

    expander.defaultOpen = granite.resource.properties[CONST.PROP_OPEN_BY_DEFAULT]
        || false;

    expander.colorTheme = granite.resource.properties[CONST.PROP_COLOR_THEME]
        || "";
    
    expander.CONST = CONST;

    if (!expander.title) {
        
        expander.title = AuthoringUtils.isTouch
            ? ""
            : CONST.TITLE_DEFAULT_TEXT;
        
    }

    if (expander.colorTheme) {

        expander.colorTheme = "colorTheme" + expander.colorTheme;

    }

    return expander;
    
});