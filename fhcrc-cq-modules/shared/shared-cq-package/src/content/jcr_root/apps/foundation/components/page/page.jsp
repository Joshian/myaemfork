<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default page component.

  Is used as base for all "page" components. It basically includes the "head"
  and the "body" scripts.

  ==============================================================================
  
  This is an overlay of the page.jsp located in /libs/foundation/components/page
  
  I needed to do this overlay in order to detect whether redirects where internal
  (original page.jsp) or external (http://) (this page.jsp).
  
  I'd prefer to do this within /apps/public (or wherever, on a per-domain basis)
  but couldn't figure out how to accomplish that.
  
  SLD Sep/11.
  
  ==============================================================================

--%><%@page session="false"
            contentType="text/html; charset=utf-8"
            import="com.day.cq.commons.Doctype,
                    com.day.cq.wcm.api.WCMMode,
                    com.day.cq.wcm.foundation.ELEvaluator" %><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %><%
%><cq:defineObjects/><%

    // read the redirect target from the 'page properties' and perform the
    // redirect if WCM is disabled.
    String location = properties.get("redirectTarget", "");
    // resolve variables in path
    location = ELEvaluator.evaluate(location, slingRequest, pageContext);
    boolean wcmModeIsDisabled = WCMMode.fromRequest(request) == WCMMode.DISABLED;
    boolean wcmModeIsPreview = WCMMode.fromRequest(request) == WCMMode.PREVIEW;

    // test to see if a selector "noredirect" was sent, and if so do not send back the redirect. 
    // (this is for our webapps that may need to grab the contents of this page, even though it's a redirect)
    // in other words, requests for a cq redirect page file.html won't redirect if you ask for file.noredirect.html 
    boolean overrideRedirect = false;
    String selector = slingRequest.getRequestPathInfo().getSelectorString();
    if(selector!=null&&(selector.equals("noredirect"))){ overrideRedirect = true; }
    log.info("Selector= " + selector);
    if(!overrideRedirect) { log.info("(Selector) NOT overriding redirect"); }
    else { log.info("(Selector) OVERRIDING redirect"); }
   
    // the following mode check (WCMMode.EDIT) is probably too harsh,
    // you may want to use   == WCMMode.DISABLED    instead, 
    // which would be in force only on the Publish server.
    // why? because in author if you're in preview mode, you can't 
    // open the file for editing (!) since you'll be redirected.
    if (WCMMode.fromRequest(request) == WCMMode.DISABLED && location.length() > 0 && !overrideRedirect) {
        // check for recursion
        if (currentPage != null && !location.equals(currentPage.getPath()) && location.length() > 0) {
            // check for absolute path
            final int protocolIndex = location.indexOf(":/");
            final int queryIndex = location.indexOf('?');
            final String redirectPath;
            if ( protocolIndex > -1 && (queryIndex == -1 || queryIndex > protocolIndex) ) {
                redirectPath = location;
            } else {
                //redirectPath = request.getContextPath() + location + ".html"; // 5.4
                redirectPath = slingRequest.getResourceResolver().map(request, location) + ".html"; // 5.6.1
            }
            response.sendRedirect(redirectPath);
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        return;
    }
    
    // set doctype
    if (currentDesign != null) {
        currentDesign.getDoctype(currentStyle).toRequest(request);
    }

%><%= Doctype.fromRequest(request).getDeclaration() %>
<html <%= wcmModeIsPreview ? "class=\"preview\"" : ""%>>
<cq:include script="head.jsp"/>
<cq:include script="body.jsp"/>
</html>
