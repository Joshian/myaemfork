/* Here is the master component app. It should contain submodules for each 
 * component, then return each of those submodules as object properties of the
 * main module.
 */

var componentApp = (function(componentApp) {
	
	componentApp.utilityFunctions = (function(utilityFunctions) {
		
		return utilityFunctions;
	
	})(componentApp.utilityFunctions || {});
	
	return componentApp;
	
})(componentApp || {});