/**
 * Carousel class is in charge of handling Carousel properties and logic
 *  
 */
package org.fhcrc.stripe.components;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.fhcrc.stripe.utilities.AssetUtil;

public class Carousel 
{
	private final static String IMAGES_PROPERTY = "images";
	private final static String DESCRIPTION_LOCATION = "descriptionLocation";
	
	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	
		
	private String[] images;
	private String descriptionLocation;
	
	/** Constructor of the class
	 * @param properties the page properties
	 * @param resourceResolver the resource resolver
	 */
	public Carousel() {

	}	

	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
        this.images = properties.get(IMAGES_PROPERTY, String[].class);
        this.descriptionLocation = properties.get(DESCRIPTION_LOCATION, String.class);
	}

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}
	
	public String[] getImages() {
		return images;
	}

	public void setImages(String[] images) {
		this.images = images;
	}
	
    public String getImageDescription(SlingHttpServletRequest slingRequest,String imageName) 
    {
 		return AssetUtil.getAssetProperty(slingRequest,imageName,"dc:description");
    }
    
    public String getDescriptionLocation() {
		return descriptionLocation;
	}

	public void setDescriptionLocation(String descriptionLocation) {
		this.descriptionLocation = descriptionLocation;
	}

}