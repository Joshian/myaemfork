
/**
 * Footer class is in charge of handling footer properties and logic
 *  
 */
package org.fhcrc.stripe.components;

import java.text.SimpleDateFormat;

import javax.jcr.Node;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;


public class NWBioFooter {
    
	private final Logger log = LoggerFactory.getLogger(Footer.class);

    public static final String DEFAULT_VALUE = "";
    public static final String CONTACTUS = "contactus";
    public static final String STAFFLOGIN = "stafflogin";
    public static final String HUTCH = "hutch";
    public static final String SCCA = "scca";
    public static final String UW = "uw";
    public static final String CONTACTUS_URL = "contactus";
    public static final String STAFFLOGIN_URL = "staffloginurl";
    public static final String SEATTLE_CHILDREN = "schildrens";
    
	private Page page;
	
	private ValueMap componentProperties;
	
	private ValueMap designProperties;
	
	private ResourceResolver resourceResolver;
	
		
	private SimpleDateFormat sdf;
	
	private String contactus;
	private String contactusurl;
	private String stafflogin;
	private String staffloginurl;
	private String hutch;
	private String scca;
	private String uw;
	private String schildrens;


	/** Constructor of the class
	 * @param properties the page properties
	 * @param resourceResolver the resource resolver
	 */
	public NWBioFooter(Page page, ValueMap componentProperties,ValueMap designProperties, ResourceResolver resourceResolver) {
		this.setPage(page);
		this.setComponentProperties(componentProperties);
		this.setDesignProperties(designProperties);
		this.setResourceResolver(resourceResolver);
		sdf = new SimpleDateFormat();
		sdf.applyPattern("MM/dd/yyyy");
	}
	
	/** Constructor of the class
	 * @param properties the page properties
	 * @param resourceResolver the resource resolver
	 */
	public NWBioFooter() {

	}	

	public void setPage(final Page page) {
		this.page = page;
	}

	public Page getPage() {
		return page;
	}
	
	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
		this.contactus = componentProperties.get(CONTACTUS, String.class);
        this.stafflogin = componentProperties.get(STAFFLOGIN, String.class);
        this.hutch = componentProperties.get(HUTCH, String.class);
        this.scca = componentProperties.get(SCCA, String.class);
        this.uw = componentProperties.get(UW, String.class);
        this.contactusurl = componentProperties.get(CONTACTUS_URL, "#");
        this.staffloginurl = componentProperties.get(STAFFLOGIN_URL, "#");
        this.schildrens = componentProperties.get(SEATTLE_CHILDREN, "#");
	}

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public void setDesignProperties(ValueMap properties) {
//		this.designProperties = properties;
//		this.contactus = designProperties.get(CONTACTUS, String.class);
//        this.stafflogin = designProperties.get(STAFFLOGIN, String.class);
//        this.hutch = designProperties.get(HUTCH, String.class);
//        this.scca = designProperties.get(SCCA, String.class);
//        this.uw = designProperties.get(UW, String.class);
//        this.contactusurl = designProperties.get(CONTACTUS_URL, "#");
//        this.staffloginurl = designProperties.get(STAFFLOGIN_URL, "#");
	}

	public ValueMap getDesignProperties() {
		return designProperties;
	}
	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public String getInheritedProperty(Page page, String componentName, String propertyName)
	{
	     String value = "";
	     Page root = page;
	     while (value.equals(DEFAULT_VALUE))
	     {
	         if (root.getParent()!=null)
	         {
	              root = root.getParent();
	              Resource parentResource = resourceResolver.getResource(root.getPath()+ "/jcr:content/" + componentName);
	              if(parentResource != null)
	              {
	            	  try
	            	  {
		            	  Node parentNode = parentResource.adaptTo(Node.class);
		            	  value = (String)parentNode.getProperty(propertyName).getString();
		            	  break;
	            	  }
	            	catch(Exception exception)
	            	{
	            		log.warn("Error getting getInheritedProperty in footer", exception);
	            	}
	              }
	         }
	         else{
	             break;
	         }
	     }
	     return value;
	}
	
	
	/**
	 * @return the contactus
	 */
	public String getContactus() {
		return contactus;
	}

	/**
	 * @param contactus the contactus to set
	 */
	public void setContactus(String contactus) {
		this.contactus = contactus;
	}

	/**
	 * @return the stafflogin
	 */
	public String getStafflogin() {
		return stafflogin;
	}

	/**
	 * @param stafflogin the stafflogin to set
	 */
	public void setStafflogin(String stafflogin) {
		this.stafflogin = stafflogin;
	}

	/**
	 * @return the hutch
	 */
	public String getHutch() {
		return hutch;
	}

	/**
	 * @param hutch the hutch to set
	 */
	public void setHutch(String hutch) {
		this.hutch = hutch;
	}

	/**
	 * @return the scca
	 */
	public String getScca() {
		return scca;
	}

	/**
	 * @param scca the scca to set
	 */
	public void setScca(String scca) {
		this.scca = scca;
	}

	/**
	 * @return the uw
	 */
	public String getUw() {
		return uw;
	}

	/**
	 * @param uw the uw to set
	 */
	public void setUw(String uw) {
		this.uw = uw;
	}
	
	/**
	 * @return the contactusurl
	 */
	public String getContactusurl() {
		return contactusurl;
	}

	/**
	 * @param contactusurl the contactusurl to set
	 */
	public void setContactusurl(String contactusurl) {
		this.contactusurl = contactusurl;
	}

	/**
	 * @return the staffloginurl
	 */
	public String getStaffloginurl() {
		return staffloginurl;
	}

	/**
	 * @param staffloginurl the staffloginurl to set
	 */
	public void setStaffloginurl(String staffloginurl) {
		this.staffloginurl = staffloginurl;
	}
	
	/**
	 * @return the schildrens
	 */
	public String getSchildrens() {
		return schildrens;
	}

	/**
	 * @param schildrens the schildrens to set
	 */
	public void setSchildrens(String schildrens) {
		this.schildrens = schildrens;
	}
}