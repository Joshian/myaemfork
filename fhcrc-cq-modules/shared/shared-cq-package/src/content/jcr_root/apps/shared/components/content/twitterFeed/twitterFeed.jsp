<%--

  Twitter Feed component.
  
  This component displays an embedded timeline from Twitter. The behavior of the
  timeline must be created using Twitter's Widgets Configurator
  https://twitter.com/settings/widgets/
  Here are the things that can be altered on the client side:
  # Theme - light vs. dark
  # Link color - color hex code
  # Width - in pixels
  # Height - in pixels
  # Chrome - noheader, nofooter, noborders, noscrollbar, transparent
  # Border color - color hex code
  # Language - HTML lang attribute
  # Tweet Limit - int between 1 and 20. NOTE THAT SETTING THIS WILL NOT ALLOW THE
  WIDGET TO POLL THE SERVER FOR NEW TWEETS, IT WILL BE STATIC ON PAGELOAD
  # Web Intent Related Users - twitter @-names of suggested followers after reply
  # ARIA politeness - polite, assertive
  
  CODE FREEZE NOTES: This widget is currently static, no dialogue options. Once
  the code freeze is over, this widget should allow the user to make some/all of
  the client-side overrides mentioned above as well as specify the widget ID.

--%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page session="false" %>
<%

final String LINK_CLASS = "twitter-timeline";
final String TWITTER_LINK_HREF = "https://twitter.com/search?q=%23hutchhiv";

/* CODE FREEZE STATIC VALUES */
final String WIDGET_ID = "390518734927831040";
final String TWEET_LIMIT = "2";
final String CHROME_OPTIONS = "noheader transparent";
final String HEIGHT = "200";

%>
<div>
<a class="<%= LINK_CLASS %>"  href="<%= TWITTER_LINK_HREF %>"  data-widget-id="<%= WIDGET_ID %>" height="<%= HEIGHT %>" data-chrome="<%= CHROME_OPTIONS %>">Tweets about "#hutchhiv"</a>
</div>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>