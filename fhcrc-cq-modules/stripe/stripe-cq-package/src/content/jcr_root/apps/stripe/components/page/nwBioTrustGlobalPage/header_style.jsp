<%@include file="/apps/stripe/global.jsp" %>
<%@page import="com.day.cq.wcm.api.Page,
                org.fhcrc.stripe.templates.general.Colors, 
                org.fhcrc.stripe.utilities.PageUtil" %>
                
<%
    String home = Text.getAbsoluteParent(currentPage.getPath(), 3) + "/jcr:content";

    String logoTopColor = Colors.getColorProperty(currentPage,"logo_top");
    String logoBottomColor = Colors.getColorProperty(currentPage,"logo_bottom");

    String labTopColor = Colors.getColorProperty(currentPage,"lab_top");
    String labBottomColor = Colors.getColorProperty(currentPage,"lab_bottom");
    String labtextColor = Colors.getColorProperty(currentPage,"lab_text_color");
     
    String navActiveTextColor = Colors.getColorProperty(currentPage,"nav_active_text"); 
    String navBackgroundColor = Colors.getColorProperty(currentPage,"nav_background");
    String navSelectedSubMenuColor = Colors.getColorProperty(currentPage,"nav_selected_sub_menu");
    String navSelectedMenuColor = Colors.getColorProperty(currentPage,"nav_selected_menu");
    String navTextColor = Colors.getColorProperty(currentPage,"nav_text");
    String carouselCurrentIconColor = Colors.getColorProperty(currentPage,"carousel_current_icon");
     
     
    String hyperlinkActiveColor = Colors.getColorProperty(currentPage,"hyperlink_active_color"); 
    String hyperlinkColor = Colors.getColorProperty(currentPage,"hyperlink_color");
     
    String thumbnailCurrentColor = Colors.getColorProperty(currentPage,"thumbnail_current"); 
    String thumbnailHoverColor = Colors.getColorProperty(currentPage,"thumbnail_hover"); 

    
    String navbarHeight = PageUtil.getSiteConfigProperty(slingRequest, home, "nav_bar_height"); 
    if(navbarHeight.equals(""))
    {
        navbarHeight = "50";
    }
    
    String homeIconPaddingLeft = PageUtil.getSiteConfigProperty(slingRequest, home, "home_icon_padding_left");
    if(homeIconPaddingLeft.equals(""))
    {
        homeIconPaddingLeft = "12";
    }
     
    String homeIconPaddingTop = PageUtil.getSiteConfigProperty(slingRequest, home, "home_icon_padding_top");
     
    if(homeIconPaddingTop.equals(""))
    {
        homeIconPaddingTop = "10";
    }
     
    String homeIconMarginTop = PageUtil.getSiteConfigProperty(slingRequest, home, "home_icon_margin_top");
     
    if(homeIconMarginTop.equals(""))
    {
        homeIconMarginTop = "-5";
    }
     
    String homeIconWidth = PageUtil.getSiteConfigProperty(slingRequest, home, "home_icon_width");
     
    if(homeIconWidth.equals(""))
    {
        homeIconWidth = "60";
    }
     
    String homeIconHeight = PageUtil.getSiteConfigProperty(slingRequest, home, "home_icon_height");
     
    if(homeIconHeight.equals(""))
    {
        homeIconHeight = "60"; 
    }
%>
<style type="text/css">

/*----------general start ------------*/
a{
    color: #<%=hyperlinkColor%>;
}

a:hover{
    color: #<%=hyperlinkActiveColor%>;
}
/*----------general end ------------*/

/*----------- labtitle start --------*/
.navbar-inverse .lab-title{
  background-color: #<%=labBottomColor%>;
  background-image: -moz-linear-gradient(top, #<%=labTopColor%>, #<%=labBottomColor%>);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#<%=labTopColor%>), to(#<%=labBottomColor%>));
  background-image: -webkit-linear-gradient(top, #<%=labTopColor%>, #<%=labBottomColor%>);
  background-image: -o-linear-gradient(top, #<%=labTopColor%>, #<%=labBottomColor%>);
  background-image: linear-gradient(to bottom, #<%=labTopColor%>, #<%=labBottomColor%>);
  background-repeat: repeat-x;
  border-color: #252525;
  filter: progid:dximagetransform.microsoft.gradient(startColorstr='#ff<%=labTopColor%>', endColorstr='#ff<%=labBottomColor%>', GradientType=0);
}

.labtitle h1 {
    color: #<%=labtextColor%>;
}
/*----------- labtitle end --------*/

/*---------- logobar start ----------*/

.logobar{
  height: 65px;
  border-width: 0 0 1px;
  background-image: none;
  background-color: #<%=logoBottomColor%>;
}

.navbar-inverse .logobar{
  background-color: #<%=logoBottomColor%>;
  background-image: -moz-linear-gradient(top, #<%=logoTopColor%>, #<%=logoBottomColor%>);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#<%=logoTopColor%>), to(#<%=logoBottomColor%>));
  background-image: -webkit-linear-gradient(top, #<%=logoTopColor%>, #<%=logoBottomColor%>);
  background-image: -o-linear-gradient(top, #<%=logoTopColor%>, #<%=logoBottomColor%>);
  background-image: linear-gradient(to bottom, #<%=logoTopColor%>, #<%=logoBottomColor%>);
  background-repeat: repeat-x;
  border-color: #252525;
  filter: progid:dximagetransform.microsoft.gradient(startColorstr='#ff<%=logoTopColor%>', endColorstr='#ff<%=logoBottomColor%>', GradientType=0);
  
}
/*---------- logobar end ----------*/


/*---------- navbar ----------*/
.navbar-inverse .nav li.dropdown.selected{
  color: #<%=navActiveTextColor%>;
  background-color: #<%=navSelectedMenuColor%>;
  margin-top: -18px !important;
  padding-bottom: 18px !important;
}

.navbar-inverse .nav li a:hover
{
  background-color: #<%=navSelectedMenuColor%>;
  margin-top: 0px !important;
  padding-bottom: -18px !important;
}

.navbar-inverse .brand, .navbar-inverse .nav  li  a {
    color: #<%=navTextColor%>;
    text-shadow: none;
}

.navbar-inverse .nav  ul.dropdown-menu li  a {
    color: #<%=navActiveTextColor%>;
    text-shadow: none;
}

.navbar-inverse .nav  ul.dropdown-menu li  a:hover {
    color: #<%=navTextColor%>;
    text-shadow: none;
}


.navbar-inverse .brand, .navbar-inverse .nav  li  a:hover {
    color: #<%=navActiveTextColor%>;
    text-shadow: none;
}

.navbar .nav li .dropdown-menu:after {
    border-bottom: 6px solid #<%=navSelectedSubMenuColor%>;
    color: #<%=navActiveTextColor%>;
    
}

/* ----dropdown -----*/

.dropdown-menu {
  background-color: #<%=navSelectedMenuColor%>;
}

.navbar-inverse .nav li.dropdown.selected{
  background-color: #<%=navSelectedMenuColor%>;
}
.navbar-inverse .nav li.dropdown.selected a p{
  color: #<%=navActiveTextColor%>;
}

.navbar-inverse .nav li.dropdown > a p {
    color: #<%=navTextColor%>;
}

.navbar-inverse .nav li.dropdown > a:hover p,
.navbar-inverse .nav li.dropdown.open > a p {
    color: #<%=navActiveTextColor%>;
}

/*** PASTE HERE ***/
.navbar .nav  li  .dropdown-menu:after {
    border-bottom: 6px solid #<%=navSelectedMenuColor%>;
}

.navbar .navitems {
    background-color: #<%=navBackgroundColor%>
}

/* ----dropdown -----*/

.dropdown-menu {
  background-color: #<%=navSelectedMenuColor%>;
}

.navbar-inverse .nav li.dropdown.selected{
  background-color: #<%=navSelectedMenuColor%>;
}
.navbar-inverse .nav li.dropdown.selected a p{
  color: #<%=navActiveTextColor%>;
}

.navbar-inverse .nav li.dropdown > a p {
    color: #<%=navTextColor%>;
}

.navbar-inverse .nav li.dropdown > a:hover p,
.navbar-inverse .nav li.dropdown.open > a p {
    color: #<%=navActiveTextColor%>;
}

/*** PASTE HERE ***/
.navbar .nav  li  .dropdown-menu:after {
    border-bottom: 6px solid #<%=navSelectedMenuColor%>;
}

.navbar .navitems {
    background-color: #<%=navBackgroundColor%>
}


/** ---- carousel -----*/ 

.carousel-icons.current{
    background-color: #<%=carouselCurrentIconColor%>;
}

.bottom_description{  
    background-color: #FFFFFF; 
}

/*------ horizontal thumbnail design --------------*/

.slider-thumbs li.current a .thumb-image {
    /*** maincolor1 **/
    border: 4px solid #<%=thumbnailCurrentColor%>;
}
.slider-thumbs li a .thumb-image:hover,
.slider-thumbs li a .thumb-image:focus,
.slider-thumbs li a .thumb-image:active {
    /*** maincolor1 **/
    border: 4px solid #<%=thumbnailHoverColor%>;
}

.navbar-inverse .nav li.dropdown {
    height: <%=navbarHeight%>px;
}

.home {
    height: <%=homeIconHeight%>px; 
    width: <%=homeIconWidth%>px;
    margin-top: <%=homeIconMarginTop%>px; 
    
}

.home img {
    padding-left:<%=homeIconPaddingLeft%>px; 
    padding-top:<%=homeIconPaddingTop%>px;
}
</style>
<script type='text/javascript'>
jQuery(document).ready(function(){
	
    jQuery('th').css('background-color', '#<%=labBottomColor%>');
    jQuery('th').css('color', '#<%=labtextColor%>');
    jQuery('th').find('p').css('color', '#<%=labtextColor%>');     
  });   
</script>