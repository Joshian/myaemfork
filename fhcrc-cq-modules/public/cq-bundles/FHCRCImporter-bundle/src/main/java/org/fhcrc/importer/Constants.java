package org.fhcrc.importer;

public class Constants {

	public final static String DIVISION_TAG_PATH = "/etc/tags/divisions/";
	public final static String DATE_FORMAT = "EEE, d MMM yyyy HH:mm:ss Z";
	public final static String PUBLISHED_DATE_PROPERTY = "published";
	public final static String IDENTIFIER_PROPERTY = "id";
	public final static String DESCRIPTION_PROPERTY = "jcr:description";
	public final static String REDIRECT_PROPERTY = "redirectTarget";
	public final static String OMIT_SECTION_NAV_PROPERTY = "omitSectionNav";
	public final static String TITLE_PROPERTY = "jcr:title";
	public final static String TAGS_PROPERTY = "cq:tags";
	
	// these are for Petri Dish
	//public final static String IDENTIFIER_PROPERTY_PETRI = "guid";
}
