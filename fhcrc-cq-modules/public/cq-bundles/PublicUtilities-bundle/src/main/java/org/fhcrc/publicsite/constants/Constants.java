package org.fhcrc.publicsite.constants;

public class Constants {
	
    /* folders related to news. no trailing slash */
	public final static String PATH_TO_NEWS_FOLDER              = "/content/public/en/news";
    public final static String PATH_TO_NEWSRELEASES_FOLDER      = "/content/public/en/news/releases";
    public final static String PATH_TO_CENTERNEWS_FOLDER        = "/content/public/en/news/center-news";
    public final static String PATH_TO_HUTCH_MAGAZINE_FOLDER    = "/content/public/en/news/hutch-magazine";
    public final static String PATH_TO_HUTCH_MAG_IMPORTER_FOLDER     = "/content/public/en/news/hutch-magazine/imports";
    public final static String PATH_TO_PETRIDISHIMPORTER_FOLDER = "/content/public/en/news/petridish-import";
    public final static String PATH_TO_SCIENCESPOTLIGHT_FOLDER  = "/content/public/en/news/spotlight";
    public final static String PATH_TO_SCIENCESPOTLIGHTIMPORTER_FOLDER  = "/content/public/en/news/spotlight/imports";
    public final static String PATH_TO_VIDD_NEWS_FOLDER         = "/content/public/en/labs/vaccine-and-infectious-disease/news";
    
    
    /* folders related to labs. no trailing slash */
    public final static String PATH_TO_LABSIMPORTER_FOLDER = "/content/public/en/labs/labs-import";
    
    /* 3rd-level domain part of the regex that allows certain URLs to be exposed via ProxyServlet. see PublicUtilities > org.fhcrc.general.proxy.ProxyServlet.java */
    public final static String PROXYSERVLET_REGEX = "(extranet|is-ext|quest)"; //.fhcrc.org
    
    // newsroll system uses a delimiter for tag IDs (replaces the / which is part of the tag ID)
    public final static String NEWSROLL_TAG_DELIMITER     = "~";
    
    // the df_id value (form id) of the main Convio donation form. this is used in the donationformlet to send donors to this form and obtain its level_id. you'll need to update this value when you make a new "default" main giving form.
    public final static Integer CONVIO_MAIN_DONATION_FORM_DF_ID = 5980;
	
}
