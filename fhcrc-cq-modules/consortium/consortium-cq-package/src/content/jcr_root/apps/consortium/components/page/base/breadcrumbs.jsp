<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="java.util.Iterator,
        com.day.text.Text,
        com.day.cq.wcm.api.PageFilter,
        com.day.cq.wcm.api.NameConstants,
        org.apache.commons.lang.StringEscapeUtils,
        com.day.cq.wcm.api.Page,
        org.fhcrc.publicsite.constants.Constants,
        org.fhcrc.tools.FHUtilityFunctions" %>
        
      <div class="breadcrumbs noprint">
        <ul>
          <li<%= (currentPage.getDepth()==4?" class='parent'": "") %>><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>.html">Home</a></li>
<%
int level = 3;
int currentLevel = currentPage.getDepth() - 1;
String currentTitle = FHUtilityFunctions.displayTitle(currentPage);

while (level < currentLevel) {
    Page trail = currentPage.getAbsoluteParent(level);
    if (trail == null) {
        break;
    }
    String title = FHUtilityFunctions.displayTitle(trail);
    String cls = (currentLevel-level==1)? " class='parent'" : "";
    %><li<%=cls%>><a href="<%= Text.escape(trail.getPath(), '%', true) %>.html"><%
    %><%= StringEscapeUtils.escapeHtml(title) %><%
    %></a></li><%

    level++;
}
%>
          
          <li><%= currentTitle %></li>
        </ul>
      </div>