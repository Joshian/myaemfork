
<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.util.Iterator,
        com.day.cq.wcm.api.PageFilter,
        org.fhcrc.tools.FHUtilityFunctions" %>
<%

Page sectionHead = resourceResolver.getResource(currentPage.getAbsoluteParent(3).getPath()).adaptTo(Page.class);
PageFilter filter = new PageFilter(false, true); // Keeps hidden pages, since we have to display the page we are on even if hidden
Iterator<Page> navElements = sectionHead.listChildren(filter);

%>
<div class="section-navigation">
	<h3>In <%= FHUtilityFunctions.displayTitle(sectionHead) %>:</h3>
	<ul>

<%

while (navElements.hasNext()) {
    
    Page child = navElements.next();
    String currentPagePath = currentPage.getPath();
    String navPath = child.getPath();
    Boolean isOnTrail = currentPagePath.contains(navPath),
    		isHidden = child.getProperties().get("hideInNav","false").equals("true"),
    		isCurrentPage = currentPage.equals(child);
    
    /* If the page in question is hidden and NOT an ancestor of the current page, skip it */
    if (isHidden && !isOnTrail) {
    	continue;
    }

%>

		<li class="<%= isCurrentPage ? "currentPage " : ""%><%= isOnTrail ? "onTrail" : ""%>"><a href="<%= navPath %>.html"><%= FHUtilityFunctions.displayTitle(child) %></a>
	
<%

	if (isOnTrail && child.listChildren(filter).hasNext()) {
		
		Iterator<Page> subNavElements = child.listChildren(filter);
		out.print("<ul>");
		while (subNavElements.hasNext()) {
			
			Page subNavChild = subNavElements.next();
			String subNavPath = subNavChild.getPath();
			isCurrentPage = currentPage.equals(subNavChild);
			isHidden = subNavChild.getProperties().get("hideInNav","false").equals("true");
			isOnTrail = currentPagePath.contains(subNavPath);
			
			if (isHidden && !(isCurrentPage || isOnTrail)) {
				continue;
			}

%>

				<li class="<%= isCurrentPage ? "currentPage " : ""%><%= isOnTrail ? "onTrail" : ""%>"><a href="<%= subNavChild.getPath() %>.html"><%= FHUtilityFunctions.displayTitle(subNavChild) %></a>

<%			
	
			if (isOnTrail && subNavChild.listChildren(filter).hasNext()) {
				
				Iterator<Page> subSubNavElements = subNavChild.listChildren(filter);
				out.print("<ul>");
				while (subSubNavElements.hasNext()) {
					
					Page subSubNavChild = subSubNavElements.next();
					isCurrentPage = currentPage.equals(subSubNavChild);
					isHidden = subSubNavChild.getProperties().get("hideInNav","false").equals("true");
					
					if (isHidden && !isCurrentPage) {
						continue;
					}

		%>

						<li class="<%= isCurrentPage ? "currentPage " : ""%>"><a href="<%= subSubNavChild.getPath() %>.html"><%= FHUtilityFunctions.displayTitle(subSubNavChild) %></a>

		<%			
		
				}
				
				out.print("</ul>");
				
			}
			
		}
		
		out.print("</ul>");
		
	}

}

%>	

	</ul>
</div>