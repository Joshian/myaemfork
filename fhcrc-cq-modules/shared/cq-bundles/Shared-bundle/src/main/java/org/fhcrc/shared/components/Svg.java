package org.fhcrc.shared.components;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import javax.servlet.ServletRequest;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.resource.Resource;

import com.day.cq.commons.Doctype;
import com.day.cq.wcm.api.components.DropTarget;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.foundation.Image;
import com.day.cq.wcm.foundation.Download;
import com.adobe.granite.xss.XSSAPI;

import org.fhcrc.tools.FHUtilityFunctions;

public class Svg {
	
	/* PRIVATE VARIABLES */
	
	private ValueMap componentProperties;
	private Style currentStyle;
	private Design currentDesign;
	private Design resourceDesign;
	private ServletRequest request;
	private Writer writer;
	private Resource resource;
	private XSSAPI xssApi;
	private ResourceResolver resourceResolver;
	
	private Download svg;
	private String svgHref;
	private Image fallbackImage;
	
	/* GETTERS AND SETTERS */
	
	public ValueMap getComponentProperties() {
		return componentProperties;
	}
	public void setComponentProperties(ValueMap componentProperties) throws UnsupportedEncodingException {
		this.componentProperties = componentProperties;
		Download tempDownload = new Download(resource);
		if (tempDownload != null && tempDownload.hasContent()) {
			setSvg(tempDownload);
		} 
		this.fallbackImage = new Image(this.resource, "image");
		if (this.fallbackImage != null && this.fallbackImage.hasContent()) {
			setFallbackImage(this.fallbackImage);
		}
	}

	public Style getCurrentStyle() {
		return currentStyle;
	}
	public void setCurrentStyle(Style currentStyle) {
		this.currentStyle = currentStyle;
	}
	
	public Design getCurrentDesign() {
		return currentDesign;
	}
	public void setCurrentDesign(Design currentDesign) {
		this.currentDesign = currentDesign;
	}
	
	public Design getResourceDesign() {
		return resourceDesign;
	}
	public void setResourceDesign(Design resourceDesign) {
		this.resourceDesign = resourceDesign;
	}
	
	public ServletRequest getRequest() {
		return request;
	}
	public void setRequest(ServletRequest request) {
		this.request = request;
	}
	
	public XSSAPI getXssApi() {
		return xssApi;
	}
	public void setXssApi(XSSAPI xssApi) {
		this.xssApi = xssApi;
	}
	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}
	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}
	public Writer getWriter() {
		return writer;
	}
	public void setWriter(Writer writer) {
		this.writer = writer;
	}
	
	public Resource getResource() {
		return resource;
	}
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	public Download getSvg() {
		return svg;
	}
	public void setSvg(Download svg) {
		this.svg = svg;
		String href = xssApi.getValidHref(resourceResolver.map(svg.getHref()));
		if (href != null) {
			setSvgHref(href);
		} else {
			setSvgHref("");
		}
	}
	public String getSvgHref() {
		return svgHref;
	}
	public void setSvgHref(String svgHref) {
		this.svgHref = svgHref;
	}
	
	public Image getFallbackImage() {
		return fallbackImage;
	}
	@SuppressWarnings("deprecation")
	public void setFallbackImage(Image image) throws UnsupportedEncodingException {
		
		this.fallbackImage = image;
		this.fallbackImage.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
		this.fallbackImage.loadStyleData(currentStyle);
	    this.fallbackImage.setSelector(".img"); // use image script
	    this.fallbackImage.setDoctype(Doctype.fromRequest(request));
	    // add design information if not default (i.e. for reference paras)
	    if (!currentDesign.equals(resourceDesign)) {
	        this.fallbackImage.setSuffix(currentDesign.getId());
	    }
		this.fallbackImage = FHUtilityFunctions.nukeURLPrefix(this.fallbackImage, resourceResolver);

	}
	
	/* OTHER PUBLIC METHODS */
	
	public void drawImage() {
		try {
			fallbackImage.draw(writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}