      <!-- Lockerz Share BEGIN -->
      <span class="share_options">
        <span class="a2a_kit a2a_default_style">
          <a class="a2a_button_facebook"></a>
          <a class="a2a_button_twitter"></a>
          <a class="a2a_button_email"></a>
          <a class="a2a_button_printfriendly" onclick="window.print();return false;"></a>
          <span class="a2a_divider"></span>
          <a class="a2a_dd" href="http://www.addtoany.com/share_save">Share</a>
        </span>
      </span>
      <script type="text/javascript">
      <!--
        a2a_config = {};
        a2a_config.onclick=1;
        a2a_config.show_title=0;
        a2a_config.num_services=10;
        /*a2a_config.linkname=document.title;
        a2a_config.linkurl=location.href;*/
        a2a_config.prioritize=["facebook","twitter","linkedin","digg","delicious","myspace","read_it_later","squidoo","technorati_favorites","care2_news"];
        /*a2a_config.color_main="D7E5ED";*/
        /*a2a_config.color_border="AECADB";*/
        a2a_config.color_link_text="276a9f";
        a2a_config.color_link_text_hover="348dd1";
      //-->
      </script>
      <!-- <script type="text/javascript" src="https://static.addtoany.com/menu/page.js"></script> -->
      <script type="text/javascript">
         // for some reason the addtoany.com code needs to load last in the edit environment,
         // so use Modernizr to do that. Also execute an a2a.init in case there are other
         // share elements on the same page that have been placed after the first one (since their
         // a2a.init() invocation will have failed...a2a isn't yet an object for them).
         addEvent(window,"load",function(){
	         Modernizr.load([
	                         {
	                           load: 'https://static.addtoany.com/menu/page.js',
	                           complete: function () { if( typeof a2a == 'object' ) { a2a.init('page'); } }
	                         },
	                         {
	                           // This will wait for the fallback to load and
	                           // execute if it needs to.
	                           //load: 'needs-jQuery.js'
	                         }
	                       ]);
	         });   
      </script>
      
      <!-- Lockerz Share END -->   