<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.apache.commons.httpclient.*, 
                 org.apache.commons.httpclient.methods.*,
                 org.apache.commons.httpclient.params.HttpMethodParams,
                 java.io.IOException" %>
<%
//------------------------------------------------------
// Grab the query parameters froom the querystring sent
// in to this page.  Use the search API link provided.
// Make sure to attach the "key" to the querystring of
// the search API.  
//------------------------------------------------------
String queryString = request.getQueryString();
String dirAPILink = properties.get("./dirAPILink", "http://is.fhcrc.org/users/sdowning/projects/mobile/directory-api/directory.php");
dirAPILink = dirAPILink + "?" + queryString;
log.info("Performing Directory API with the following url:  " + dirAPILink);
boolean force_expanded = properties.get("./forceExpanded",Boolean.FALSE);

HttpClient client = new HttpClient();
HttpMethod method = new GetMethod(dirAPILink);
try {
    int statusCode = client.executeMethod(method);
    if (statusCode != HttpStatus.SC_OK) {
      log.error("Method failed: " + method.getStatusLine());
    }

    String searchResponse = method.getResponseBodyAsString();
    //--------------------------------------------------------------------
    // Perform some string replacements on the results.  The result uses
    // a url with a path to the search results such as /search?q=cancer.  
    // We need it to be /search.html?q=cancer.
    // Also, on the advanced search page there are buttons with an action="search".
    // We need to make that action="search.html".
    //--------------------------------------------------------------------
    searchResponse = searchResponse.replaceAll("search\\?", "search.html?");
    searchResponse = searchResponse.replaceAll("action=\"search\"", "action=\"search.html\"");
    searchResponse = searchResponse.replaceAll("/user_help.html", currentPage.getPath() + "/user_help.html");
    //log.info("searchResponse = " + searchResponse);
%>
    <div id="directorysearchapiresults" class="directorysearchapiresults">
       <%= searchResponse %>
    </div>
<%
} catch (HttpException e) {
    log.error("Fatal protocol violation: " + e.getMessage());
    response.setStatus(404);
  } catch (IOException e) {
    log.error("Fatal transport error: " + e.getMessage());
    response.setStatus(404);
  } finally {
    method.releaseConnection();
  }  

%>

<br />
<div <% if(!force_expanded){ %>id="directoryContactInfoContainer"<% } %>>

	<div class="fh_gradient_columns clearfix">
	 <div class="fh_parsys_column fh-colctrl-lt10">
      <div class="fh_parsys_column fh-colctrl-lt10-c0">
       <div class="fh_section">	 
	    <cq:include path="directoryContactInfo" resourceType="public/components/content/text" />
	   </div>
	  </div>
	 </div>
	</div>

</div>