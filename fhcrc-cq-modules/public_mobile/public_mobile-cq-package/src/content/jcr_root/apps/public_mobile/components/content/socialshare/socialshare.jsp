<%@ page import = "java.net.URLEncoder,
                   com.day.cq.wcm.api.WCMMode" 
%><%@include file="/libs/foundation/global.jsp"%>
<%

    boolean disabled = properties.get("disable",Boolean.FALSE); // user can disable the widget by checking a checkbox
    int hashCode = resource.hashCode();   
    String title = properties.get("title","Share this webpage:");
    String share_url = "http://m.fhcrc.org" + currentPage.getPath().replace(currentPage.getAbsoluteParent(1).getPath(),"") + ".html";
    String share_url_enc = URLEncoder.encode(share_url, "UTF-8")
        .replaceAll("\\+", "%20")
        .replaceAll("\\%21", "!")
        .replaceAll("\\%27", "'")
        .replaceAll("\\%28", "(")
        .replaceAll("\\%29", ")")
        .replaceAll("\\%7E", "~"); // http://stackoverflow.com/questions/607176/java-equivalent-to-javascripts-encodeuricomponent-that-produces-identical-outpu
%>

<% if(!disabled){ %>

<div class="socialsharewidget">
<p class="directive"><%=title %></p>
<a rel="nofollow" id="share-fb-<%=hashCode%>" 
   data-url="<%= share_url %>" 
   href="http://www.facebook.com/share.php?u=<%= share_url_enc %>"  
   target="_blank"><img src="<%= currentDesign.getPath().toString() %>/img/icons/facebook.jpg" alt="Share to Facebook" /></a>
</div>

<cq:includeClientLib js="mobile.components.socialshare" />
<script type="text/javascript">
  addEvent( document.getElementById("share-fb-<%=hashCode%>"), 'click', componentApp.socialshareComponent.clickFB );
</script>

<% } else { %>

   <% if(WCMMode.fromRequest(request)==WCMMode.EDIT){ %>
       <cq:include script="empty.jsp"/>
   <% } %>
        
<% } %>