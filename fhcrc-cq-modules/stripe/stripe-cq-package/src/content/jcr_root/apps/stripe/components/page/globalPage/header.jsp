<%@include file="/apps/stripe/global.jsp" %>
<%
String edit = (editMode.equals("true"))?"editmode":"";
String home = Text.getAbsoluteParent(currentPage.getPath(), 3);
boolean removeSubHeader = false;
if(home.contains("youyifong"))
{
    removeSubHeader = true;
}
request.setAttribute("removeSubHeader",removeSubHeader);
%>
<jsp:useBean id="pageUtilBean" scope="request" class="org.fhcrc.stripe.utilities.PageUtil" />
<div class="container" id="utility_nav_container">
    <cq:include path="utilitynav" resourceType="stripe/components/modular/utilitynav"/>  
</div>
<div class="navbar navbar-inverse navbar-stripe-top">
  <div >
      <div class="logobar">
        <div class="container">
          <div class="row">
            <span class="span12">
               <cq:include path="fhcrclogo" resourceType="stripe/components/modular/logo"/>
            </span>
          </div>
        </div>
      </div>
      <div class="header-greybar">
        <div class="header-greybar-inner">
            <div class="row">
	           <span class="span12">
	                 <cq:include path="divisiontitle" resourceType="stripe/components/modular/divisiontitle" />
	           </span>
            </div>
        </div>
      </div>
      <div class="lab-title">
        <div class="container">
          <div class="row">
            <span class="span12">
                <cq:include path="labtitle" resourceType="stripe/components/modular/title" />
            </span>
          </div>
        </div>
      </div>
      <div class="navitems">
        <div class="clearfix"></div>
        <div class="container">
          <div class="row">
	          <div class="span12">
	              <div class="home">
                        <a href="<%=home%>.html"><img src="/content/dam/stripe/logos/Home_35x35.png"></a>
	              </div>
		          <div class="nav-collapse collapse" style="float:left; padding-left:50px;" >
		            <ul class="nav">
		                <cq:include path="navitemsiparsys" resourceType="foundation/components/iparsys" />
		            </ul>
		          </div><!--/.nav-collapse -->
	          </div>
          </div>
        </div>
      </div>
    </div>
</div>
<c:if test="${removeSubHeader == false}">
    <cq:include script="subheader.jsp" />
</c:if>
<cq:include script="header_style.jsp"/>