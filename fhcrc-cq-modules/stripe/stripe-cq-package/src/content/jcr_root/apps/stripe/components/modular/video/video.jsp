<%@include file="/apps/stripe/global.jsp" %>

<jsp:useBean id="videoBean" scope="request" class="org.fhcrc.stripe.components.Video" />
<jsp:setProperty name="videoBean" property="request" value="<%= request %>" />
<jsp:setProperty name="videoBean" property="componentProperties" value="<%= properties %>"/>

<c:choose>
	<c:when test="${!empty videoBean.path}" >
	
		<c:choose>
			
			<c:when test="${ videoBean.shadowed == true }" >
			
				<div id="video" class="video shadowed">
			
			</c:when>
			
			<c:otherwise>
			
				<div id="video" class="video">
			
			</c:otherwise>
		
		</c:choose>
		
		    <div id="oembedcont" style="display: none;">
		        <input id="olink" type="text">
		    </div>
		    <iframe width="${videoBean.width}" height="${videoBean.height}" frameborder="0" allowfullscreen="" 
		        src="${videoBean.path}">
		    </iframe>
		</div>

	</c:when>

	<c:otherwise>

		<div style="margin: 20px 0;">
		   <span><c:out value="${videoBean.defaultMessage}"/></span>
		</div>

	</c:otherwise>

</c:choose>