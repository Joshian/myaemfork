
/**
 * Footer class is in charge of handling footer properties and logic
 *  
 */
package org.fhcrc.stripe.components;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.resource.Resource;

import com.day.cq.wcm.api.Page;

import org.fhcrc.stripe.utilities.PageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Footer {
	private final static String LOCATION = "location";
	private final static String TITLE = "header";
//	private final static String COPYRIGHT = "copyright";
    private final static String FACEBOOK = "facebook";
    private final static String TWITTER = "twitter";
    private final static String YOUTUBE = "youtube";
    private final static String BUTTON1_TEXT = "button1_text";
    private final static String BUTTON2_TEXT = "button2_text";
    private final static String SHOW_BUTTON1 = "showButon1";
    private final static String SHOW_BUTTON2 = "showButton2";
    public static final String DEFAULT_VALUE = "";
    public static final String DONATE = "DONATE";
    public static final String CENTERNET = "CenterNet";
    private static final int theYear = Calendar.getInstance().get(Calendar.YEAR);
	
	private Page page;
	
	private ValueMap componentProperties;
	
	private ValueMap designProperties;
	
	private ResourceResolver resourceResolver;
	
	private final Logger log = LoggerFactory.getLogger(Footer.class);
		
	private SimpleDateFormat sdf;
	
	private String title;
	private String phone;
	private String fax;
	private String location;
	private String copyright;
	private String facebook;
	private String twitter;
	private String youtube;
	private String button1Text;
	private String button2Text;
	private String showButton1;
	private String showButton2;
	


	/** Constructor of the class
	 * @param properties the page properties
	 * @param resourceResolver the resource resolver
	 */
	public Footer(Page page, ValueMap componentProperties,ValueMap designProperties, ResourceResolver resourceResolver) {
		this.setPage(page);
		this.setComponentProperties(componentProperties);
		this.setDesignProperties(designProperties);
		this.setResourceResolver(resourceResolver);
		sdf = new SimpleDateFormat();
		sdf.applyPattern("MM/dd/yyyy");
	}
	
	/** Constructor of the class
	 * @param properties the page properties
	 * @param resourceResolver the resource resolver
	 */
	public Footer() {

	}	

	public void setPage(final Page page) {
		this.page = page;
	}

	public Page getPage() {
		return page;
	}
	
	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
		this.title = componentProperties.get(TITLE, String.class);
		if (title==null || (title!=null && title.equals(""))){
			this.title = PageUtil.getPageTitle(page, "");
		}
		this.location = componentProperties.get(LOCATION, String.class);
		this.copyright = "© ".concat(String.valueOf(theYear)).concat(" Fred Hutchinson Cancer Research Center, a 501(c)(3) nonprofit organization.");
//      this.copyright = componentProperties.get(COPYRIGHT, String.class);
        this.button1Text = componentProperties.get(BUTTON1_TEXT, DONATE);
        this.button2Text = componentProperties.get(BUTTON2_TEXT, CENTERNET);
        this.showButton1 = componentProperties.get(SHOW_BUTTON1, String.class);
        this.showButton2 = componentProperties.get(SHOW_BUTTON2, String.class);
	}

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public void setDesignProperties(ValueMap properties) {
		this.designProperties = properties;
		//	this.title = designProperties.get(_TITLE_, String.class);
		//		
		//	if (title==null || (title!=null && title.equals(""))){
		//		this.title = PageUtil.getPageTitle(page, "");
		//	}
		//this.location = componentProperties.get(_LOCATION_, String.class);
		//this.copyright = componentProperties.get(_COPYRIGHT_, String.class);
		
        this.facebook = designProperties.get(FACEBOOK, String.class);
        this.youtube = designProperties.get(YOUTUBE, String.class);
        this.twitter = designProperties.get(TWITTER, String.class);
        this.button1Text = componentProperties.get(BUTTON1_TEXT, DONATE);
        this.button2Text = componentProperties.get(BUTTON2_TEXT, CENTERNET);
        this.showButton1 = componentProperties.get(SHOW_BUTTON1, String.class);
        this.showButton2 = componentProperties.get(SHOW_BUTTON2, String.class);

	}

	public ValueMap getDesignProperties() {
		return designProperties;
	}
	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCopyright() {
		return copyright;
	}

	public void setCopyright(String copyright) {
		this.copyright = copyright;

	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getYoutube() {
		return youtube;
	}

	public void setYoutube(String youtube) {
		this.youtube = youtube;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getButton1Text() {
		return button1Text;
	}

	public void setButton1Text(String button1Text) {
		this.button1Text = button1Text;
	}

	public String getButton2Text() {
		return button2Text;
	}

	public void setButton2Text(String button2Text) {
		this.button2Text = button2Text;
	}

	public String isShowButton1() {
		return showButton1;
	}

	public void setShowButton1(String showButton1) {
		this.showButton1 = showButton1;
	}

	public String isShowButton2() {
		return showButton2;
	}

	public void setShowButton2(String showButton2) {
		this.showButton2 = showButton2;
	}

	public String getInheritedProperty(Page page, String componentName, String propertyName)
	{
	     String value = "";
	     Page root = page;
	     while (value.equals(DEFAULT_VALUE))
	     {
	         if (root.getParent()!=null)
	         {
	              root = root.getParent();
	              Resource parentResource = resourceResolver.getResource(root.getPath()+ "/jcr:content/" + componentName);
	              if(parentResource != null)
	              {
	            	  try
	            	  {
		            	  Node parentNode = parentResource.adaptTo(Node.class);
		            	  value = (String)parentNode.getProperty(propertyName).getString();
		            	  break;
	            	  }
	            	catch (PathNotFoundException pnfe) 
	            	{
	            		// IF THE PATH IS NOT FOUND THEN JSP WILL TAKE CARE OF SHOWING THE DEFAULT'S
					}
	            	catch(Exception exception)
	            	{
	            		log.warn("Error getting getInheritedProperty in footer", exception);
	            	}
	              }
	         }
	         else{
	             break;
	         }
	     }
	     return value;
	}
}