<%@include file="/libs/foundation/global.jsp"%>

<!-- HEADER -->

	<div class="menubar">
  
  	<div class="menubar_container outer_grid_container">
  
      <div class="FH_logo inner_grid_container">
      
        <a href="<%= currentPage.getAbsoluteParent(2).getPath() %>.html"><img src="<%= currentDesign.getPath() %>/img/logos/fred_hutch_logo.png"></a>
        
      </div>
      
      <div class="menu_container inner_grid_container">
      
        <nav role="navigation">
        
          <div class="navigation_link_container">
	          <a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases.html">Diseases / Research</a>
          </div>
          <div class="navigation_link_container">
	          <a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/news.html">News</a>
          </div>
          <div class="navigation_link_container">
	          <a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/about.html">About Us</a>
          </div>
          <div class="navigation_link_container">
              <cq:include path="header_donate_button" resourceType="/apps/public/components/content/donateButton" />
	          <!-- 
	          <a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/redirect-stubs/donate.html">Donate Now</a>
	           -->
          </div>
               
         </nav>
       
       </div>
       
       <div class="clear"></div>
     
     </div>
  
  </div>

<!-- END HEADER -->
