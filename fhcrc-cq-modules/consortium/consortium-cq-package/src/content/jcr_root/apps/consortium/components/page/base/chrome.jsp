<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="java.util.Calendar" %>
<%
Calendar theTime = Calendar.getInstance();
int theYear = theTime.get(Calendar.YEAR);
%>
  <!-- header appears on all pages incl. home page -->
  <div class="header" id="header_element">
    <div class="header_contents">
      <!-- utility nav -->
      <div class="utility_nav noprint">
        <ul>
          <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/patient-care.html">Patient <span class="noverbose">care</span></a></li>
          <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/research.html">Research</a></li>
          <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/education-training.html">Education <span class="noverbose">&amp; training</span></a></li>
          <li class="search"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/util/search.html" id="utility_nav_search_icon">Search</a></li>
        </ul>
        <!-- show/hide search widget -->
        <div class="utility_search" id="utility_search">
          <div class="sitesearch">
             <form action="<%= currentPage.getAbsoluteParent(2).getPath() %>/util/search.html" id="utility_search_form">
               <input class="searchbox" maxlength="2048" name="q" placeholder="Search this site..." value="" type="text"/>
               <input class="searchbutton" value="&raquo;" type="submit"/>    
             </form>
          </div>
        </div>
      </div>
      <!-- cc logo -->
      <div class="logo">
        <div class="logo_contents">
          <a href="<%= currentPage.getAbsoluteParent(1).getPath() %>/en.html"><img src="<%= currentDesign.getPath() %>/img/logos/cancer-consortium.png" alt="Fred Hutch/University of Washington Cancer Consortium" /></a>
        </div>
      </div>
      <!-- nav system for smallest mobile only -->
      <div class="mobile_nav">
        <img src="<%= currentDesign.getPath() %>/img/icons/menu.png" alt="Menu" id="mobile_menu_button" />
        <img src="<%= currentDesign.getPath() %>/img/icons/search.png" alt="Search" id="mobile_search_button" />        
      </div>
      <div class="clear"></div>
      <!-- global nav: for desktop and tablets only -->
      <cq:include script="global_nav.jsp" />
    </div>
    <!-- push-down search, only for mobile -->
    <div class="pushdown_search" id="pushdown_search">
      <div class="pushdown_search_contents">
        <div class="sitesearch">
           <form action="<%= currentPage.getAbsoluteParent(2).getPath() %>/util/search.html">
             <input class="searchbox" maxlength="2048" name="q" placeholder="Search this site..." value="" type="text"/>
             <input class="searchbutton" value="&raquo;" type="submit"/>    
           </form>
        </div>
      </div>
    </div>
    <!-- push-down menu, only for mobile -->
    <cq:include script="pushdown-menu.jsp"/>
    <!-- blue band for desktop, blue line mobile -->
    <div class="bluebar noprint"></div>
  </div>


  <!-- large blue footer hugs bottom of every page -->
  <div class="footer" id="footer_element">
    <div class="footer_content">
      <div class="partners noprint">
        <p><strong>Consortium partner institutions</strong></p>
        <a href="http://www.fredhutch.org"><img src="<%= currentDesign.getPath() %>/img/logos/fhcrc.png" alt="Fred Hutchinson Cancer Research Center" /></a>
        <a href="http://www.washington.edu"><img src="<%= currentDesign.getPath() %>/img/logos/uw.png" alt="University of Washington" /></a>
        <a href="http://www.seattlechildrens.org"><img src="<%= currentDesign.getPath() %>/img/logos/seattle-childrens.png" alt="Seattle Children's" /></a>
        <a href="http://www.seattlecca.org"><img src="<%= currentDesign.getPath() %>/img/logos/scca.png" alt="Seattle Cancer Care Alliance" /></a>
      </div>
      <div class="copyright">
        <div class="copyright_content">
          <p class="nci noprint"><a href="http://www.cancer.gov"><img src="<%= currentDesign.getPath() %>/img/logos/nci-ccc_sm.png" alt="NCI CCC - A Comprehensive Cancer Center Designated by the National Cancer Institute" /></a></p>
          <div class="text_elements">
            <p class="year">&copy; <%= theYear %> Fred Hutch/University of Washington <span class="nobreak">Cancer Consortium</span></p>
            <p class="address">1100 Fairview Ave. N., Seattle, WA 98109</p>
            <p class="terms noprint"><a href="http://www.fredhutch.org/en/util/terms-privacy.html" target="_blank">Terms of use &amp; privacy policy</a> <span class="pipe">|</span> <a href="mailto:cancerconsortium@fredhutch.org">Contact an administrator</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

