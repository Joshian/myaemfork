<%@include file="/libs/foundation/global.jsp"%>
<jsp:useBean id="bucketBean" scope="request" class="org.fhcrc.hvtn.components.Bucket" />
<jsp:setProperty name="bucketBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="bucketBean" property="resource" value="<%= resource %>"/>
<jsp:setProperty name="bucketBean" property="currentStyle" value="<%= currentStyle %>"/>
<jsp:setProperty name="bucketBean" property="currentDesign" value="<%= currentDesign %>"/>
<jsp:setProperty name="bucketBean" property="resourceDesign" value="<%= resourceDesign %>"/>
<jsp:setProperty name="bucketBean" property="request" value="<%= request %>"/>
<jsp:setProperty name="bucketBean" property="writer" value="<%= out %>"/>
<jsp:setProperty name="bucketBean" property="componentProperties" value="<%= properties %>"/>


<div class="${ bucketBean.imageContainerClass }">

	<c:choose>
		<c:when test="${!empty bucketBean.linkTarget }">
			<c:choose>
				<c:when test="${ bucketBean.externalLink }">
					<a href="${ bucketBean.linkTarget }" target="_blank">
				</c:when>
				<c:otherwise>
					<a href="${ bucketBean.linkTarget }">
				</c:otherwise>
			</c:choose>
			<% bucketBean.drawImage(); %></a>
		</c:when>
		<c:otherwise>
			<% bucketBean.drawImage(); %>
		</c:otherwise>
	</c:choose>

</div>

<cq:text property="title" tagName="h2" tagClass="bucketHeader" />
	
<cq:text property="text" tagClass="bucketText" />