(function($) {
	$(document).ready(function() {
		  if ( $().fancybox ) {
			  
			  var autoplayID = location.hash;
			  /* Need to remove the hash from the beginning of the string */
			  autoplayID = autoplayID.substring(1); 
			  var hasAutoplay = autoplayID != 0;
			  
			  if (hasAutoplay) {
				  
				  var $autoplay = $('div.fh_panel_grid div.imageWithTextOverlay a[name=\"' + autoplayID + '\"]');
				  if ($autoplay.length !== 0) {
					  
					  $(window).load(function(){
						  $autoplay.click();
					  })
					  
				  }
				  
			  }
			  
			  /* Reset background-image of any panels that contain youtube links */
			  var youTubeLinks = $('div.fh_panel_grid div.imageWithTextOverlay a[href*="youtube.com"], div.fh_panel_grid div.imageWithTextOverlay a[href*="youtu.be"]');
			  youTubeLinks.find('div.textContainer').css({
				  'background-image' : 'url(/etc/designs/public/img/icons/play_arrow_icon.png)'
			  });
			  
			  
			  var mediaLinks = $('div.fh_panel_grid div.imageWithTextOverlay a'); // fancybox 2 has media handling with auto-detect
			  //var YOUTUBE_OPTIONS = "?rel=0&autoplay=1&showinfo=0&wmode=transparent";
			  var cleanUp = function() {
				
				  $(".textOverlayBackground").stop().hide().css({
					  'opacity' : 1
				  });
				  
			  };
			  
			  //youTubeLinks.each(function(){
				  
				//  /* Find the YouTube URL, grab the 11-digit video ID, and construct an embed URL using it.*/
				//  var $this = $(this);
				//  var linkHref = $this.attr("href");
				//  var regex = /(?:youtube\.com\/watch\?v=|youtu.be\/)([\w-]{11})/ig;
				//  var results = regex.exec(linkHref);
				//  
				//  if (results != null) {
				//	  
				//	  if (results[1]) {
				//		  
				//		  var embedURL = "//www.youtube.com/embed/" + results[1];
				//		  embedURL += YOUTUBE_OPTIONS;
				//		  
				//		  $this.attr("href", embedURL);
				//		  
				//	  } 
				//	  
				// }
				  
			  //});
			  
			  mediaLinks.fancybox({
				  'openEffect'       : 'none',
				  'closeEffect'      : 'none',
				  'width'            : 800,
				  'height'           : 450,
				  'aspectRatio'      : true,
				  'scrolling'        : 'no',
				  'padding'          : 10,
				  'helpers'          : {
					                        'media' : {
					                        	'youtube' : {
					                        		'params' : {
					                        			'autoplay' : 1,
					                        			'rel'      : 0,
					                        			'showinfo' : 0
					                        		}					                        		
					                        	}					                        	
					                        }					  
				                       },
				  'afterLoad'        : cleanUp
			  });
	      }
    });
})(jQuery);