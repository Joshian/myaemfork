<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.WCMMode,
    com.day.cq.wcm.foundation.Image,
    java.lang.Integer,
    java.util.Comparator,
    java.util.Arrays,
    org.fhcrc.tools.FHUtilityFunctions" %>

<%

/* An arbitrary number representing the most slides allowed in the slider. */
int maxSlides = 10;
/* 
 * Start off by reordering the slides as to their order property (set by the author)
 * The comparator orders by the order property of the slide. If the slide has no order
 * property, it is granted an order of 100, effectively placing it at the end. If two
 * slides have the same order, they are compared by their tab numbers, with lower numbered
 * tabs coming first.
 */
Comparator<Float[]> arrayCompare = new Comparator<Float[]>() {
    public int compare(Float[] a, Float[] b){
        Float orderA = a[0];
        Float orderB = b[0];
        if(orderA==null){
            orderA = (float)100;
        }
        if(orderB==null){
            orderB = (float)100;
        }
    
        if(orderA.equals(orderB)){
            return a[1].compareTo(b[1]);
        }
        return orderA.compareTo(orderB);
    }
};

/* The metadata is a series of Float arrays, maxSlides number of arrays that each contain 2 points of data. */
Float[][] slideMetadata = new Float[maxSlides][2];
Float tmpOrder;

for(int metacounter = 0; metacounter < maxSlides; metacounter++) {
    /* If the user doesn't enter an order, set the order at 100 (reasonably large enough that it should come last) */
    tmpOrder = Float.parseFloat(properties.get("highlight_"+(metacounter+1)+"/order","100"));
    slideMetadata[metacounter][0] = tmpOrder;
    slideMetadata[metacounter][1] = (float)(metacounter+1);
}

Arrays.sort(slideMetadata, arrayCompare);

/* Now the slides should be in the order desired by the author. */

String[] textContent = new String[maxSlides];
Image[] imageArray = new Image[maxSlides];
Boolean[] invalidArray = new Boolean[maxSlides];
String[] linkTargets = new String[maxSlides];
String[] videoURLs = new String[maxSlides]; // for optional YouTube- and Vimeo-based videos.
String speed = properties.get("speed","0"); // seconds to wait before auto-advancing. 0 means do not auto-advance.
String transitionType = properties.get("transitionType","fade"); // type of movement between slides, a "move" (slide) or a "fade"
String controlNavigation = properties.get("controlNavigation","bullets"); // choices are bullets, thumbnails, tabs, none
String imageScaleMode = properties.get("imageScaleMode","fill"); // choices are fit, fill, fit-if-smaller (default), none See http://dimsemenov.com/private/forum.php?to=kb/royalslider-jquery-plugin-faq/how-to-change-image-scaling
String theme = properties.get("theme","rsUni"); // which royal slider theme you want. choices are rsUni, rsMinW, and two others (when programmed)
int slideNumber;
String temp = "";
Boolean makeLink;

for(int i = 0; i < maxSlides; i++) {
    
    slideNumber = slideMetadata[i][1].intValue();
        
    /* First put the text associated with each slide into its own array */
    textContent[i] = properties.get("highlight_"+(slideNumber)+"/text","");
        
    /* Then the images */
    imageArray[i] = new Image(resource, "image"+(slideNumber));
    
    /* Then the link targets */
    temp = properties.get("highlight_"+(slideNumber)+"/linkTarget","");
    if(!temp.equals("")){
        linkTargets[i] = FHUtilityFunctions.cleanLink(temp, resourceResolver);
    }    
    
    /* Then the videos */
    videoURLs[i] = properties.get("highlight_"+(slideNumber)+"/videoURL",""); 
    
    /* Finally, whether or not the slides are invalid. Invalid slides are displayed
     * in author mode, but not on the live site. */
    if(properties.get("highlight_"+(slideNumber)+"/isInvalid","false").equals("true")) {
        invalidArray[i] = true;
    } else {
        invalidArray[i] = false;
    }
    
}

/* determine the height and width (native) of the first image to be displayed.
   this is for royal slider which needs the aspect ratio in order to resize
   itself correctly. */
   
int nativeSlideWidth=0;
int nativeSlideHeight=0;

for(int i = 0; i < imageArray.length; i++){
	if(nativeSlideWidth==0 && imageArray[i].hasContent()){
		
        if(imageArray[i].get(Image.PN_WIDTH) == null || imageArray[i].get(Image.PN_WIDTH).trim().equals("")) {
        	nativeSlideWidth = imageArray[i].getLayer(true,true,true).getWidth();
            nativeSlideHeight = imageArray[i].getLayer(true,true,true).getHeight();
        } else {
        	nativeSlideWidth = Integer.parseInt(imageArray[i].get(Image.PN_WIDTH));
            nativeSlideHeight = Integer.parseInt(imageArray[i].get(Image.PN_HEIGHT));
        }
        //log.info(image.get(Image.PN_WIDTH));
		
	}
}



%>

<div class="royalSlider <%=theme%> fullWidth">

<%

for(int i = 0; i < imageArray.length; i++) {
    if(imageArray[i].hasContent()){
/* If the current highlight has been marked invalid, skip it */
        if(invalidArray[i] == true && WCMMode.fromRequest(request) != WCMMode.EDIT){
            continue;
        }
		imageArray[i] = FHUtilityFunctions.nukeURLPrefix(imageArray[i], resourceResolver);
        imageArray[i].loadStyleData(currentStyle);
        imageArray[i].setSelector(".img"); // use image script
        imageArray[i].setDoctype(Doctype.fromRequest(request));
        // add design information if not default (i.e. for reference paras)
        if (!currentDesign.equals(resourceDesign)) {
            imageArray[i].setSuffix(currentDesign.getId());
        }
/* Set the title attribute to a single space to avoid defaulting to the filename */             
        if(properties.get("image"+(slideMetadata[i][1])+"/jcr:title","").trim().equals("")) {
            imageArray[i].setTitle("");
        }

/* Whether there is a link target (there may not be) */
        if( linkTargets[i] != null && !linkTargets[i].trim().equals("") ){
        	makeLink = true;
        } else {
        	makeLink = false;
        }

/* Add the rsImg class expected by RoyalSlider so the image can be resized to fit container */
        imageArray[i].addCssClass("rsImg");
        // if this is video slide...
        if(!videoURLs[i].equals("")){
            imageArray[i].addAttribute("data-rsVideo",videoURLs[i]);
            makeLink = false; // would be confusing to have the video both playable and clickable
        }


/* The class="hero" is needed for only the first item, for IE7 compatibility. */
%>

    <div class="rsContent <%= i==0 ? " hero" : "" %>">
        <div class="imageslide">
          <%= makeLink? "<a href='" + linkTargets[i] + "'>"   : "" %>
            <% imageArray[i].draw(out); %>
          <%= makeLink? "</a>"   : "" %>
        </div>
        <% if (!textContent[i].trim().equals("")) { %>
        <div class="infoBlock">
            <%= textContent[i] %>    
        </div>
        <% } %>
    </div>

<%

    }
}

%>  

</div>  
<div class='clear'></div>      

<cq:includeClientLib js="public.libs.royalslider" />  
  
<script type="text/javascript">
    jQuery(document).ready(function($) {//return;
        $(".royalSlider").royalSlider({
            keyboardNavEnabled: true,
            autoScaleSlider: true,
            loop: true,
            transitionType: '<%=transitionType%>',
            globalCaption: true,
            imageScaleMode: '<%= imageScaleMode %>',
            autoScaleSliderWidth: <%= nativeSlideWidth %>,     
            autoScaleSliderHeight: <%= nativeSlideHeight %>,
            autoPlay: {
                enabled: <%= speed=="0"?"false":"true" %>,
                pauseOnHover: true,
                delay: <%= !speed.equals("0")?speed+"000":"7500" %>
            },
            arrowsNav: true,
            arrowsNavAutoHide: false,
            navigateByClick: false,
            deeplinking: {
              enabled: true
            },
            controlNavigation: '<%=controlNavigation%>',
            video: {
               autoHideControlNav: true,
               autoHideArrows: true,
               vimeoCode: '<iframe src="//player.vimeo.com/video/%id%?byline=0&amp;portrait=0&amp;autoplay=1" frameborder="no" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>',
               youTubeCode: '<iframe src="//www.youtube.com/embed/%id%?rel=1&autoplay=1&showinfo=0&wmode=transparent" frameborder="no"></iframe>'                 
            }
        });
    });
</script>