var componentApp = (function(componentApp) {
	
	componentApp.profileAlphaList = (function(profileAlphaList) {
		
		$(document).ready(function() {
			$("div#alphaSelect a").each(function(){
				if($(".profileListItem[data-alpha=" + this.id + "]").length != 0 || this.id == "showAll"){
					$(this).addClass("active");
					$(this).removeClass("inactive");
					//alert(this.id);
				}
			});
			$("div#alphaSelect a.inactive").click(function(){
				return false;
			});
			$("div#alphaSelect a.active").click(function(){
				if(this.id == "showAll"){
					$(".profileListItem").show();
				} else {
					$(".profileListItem").hide().filter("[data-alpha=" + this.id + "]").show();
				}
				return false;
			});
			$("#topicSelector").change(function(){
				if(this.value == "" || this.value == "showAll"){
					$(".profileListItem").show();
				} else {
					$(".profileListItem").hide();
					$(".profileListItem[data-topics*=" + this.value + "]").show();
				}
			});
		});
		
		return profileAlphaList;
		
	})(componentApp.profileAlphaList || {});
	
	return componentApp;
	
})(componentApp || {});