<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text, 
                 java.util.Iterator, 
                 com.day.cq.tagging.TagManager, 
                 com.day.cq.tagging.Tag, 
                 com.day.cq.commons.RangeIterator, 
                 org.apache.commons.lang.StringEscapeUtils, 
                 java.util.TreeSet, 
                 java.util.Comparator, 
                 com.day.cq.wcm.api.WCMMode,
                 org.fhcrc.tools.FHUtilityFunctions" %>
<%
/* Variable Declaration */
TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
Tag[] allTags = tagManager.getTags(resource);
Tag facultyTag = null;
int tagCount = 0;
/* String[] basePaths = {"/content/public/en/labs/clinical/projects","/content/public/en/labs/phs/projects","/content/public/en/labs/humanbio/projects","/content/public/en/labs/basic-sciences/projects","/content/public/en/labs/vidd/projects","/content/public/en/labs/labs-import", "/content/public/en/labs/science-projects"}; */
String[] basePaths = {"/content/public/en/labs"}; // use this higher-up level to get better performance of tagManager.find()
/* This TreeSet will eventually contain all the pages I want to list in the UL tag, ordered alphabetically by Page Title (or Page Name if there is no Title) */
TreeSet<Page> programsTree = new TreeSet<Page>(new Comparator<Page>(){
	public int compare(Page a, Page b){
		String titleA = a.getTitle();
		String titleB = b.getTitle();
		if(titleA == null || titleA.equals("")) {
			titleA = a.getName();
		}
		if(titleB == null || titleB.equals("")) {
            titleB = b.getName();
        }
		return titleA.compareTo(titleB);
	}
});
/* I only care about tags in the web-faculty namespace, and I want to make sure there is one and only one */
for(Tag t : allTags){
	if(t.getNamespace().getName().equals("web-faculty")){
		tagCount++;
		facultyTag = t;
	}
}

/* Don't print out anything if there is no Faculty tag on this page */
if(facultyTag != null || WCMMode.fromRequest(request) == WCMMode.EDIT){
%>
<h3>Related Labs &amp; Projects</h3>
<%
if(tagCount == 0){
	out.print("<p>Unable to determine related labs &amp; projects.<br/><br/>Please add the faculty tag for this page using Page Properties. If the appropriate faculty tag is missing, contact webadm@fhcrc.org</p>");
} else if(tagCount == 1){
/* 
Here's the workhorse. I plow through all the resources under each of the basePaths, and grab all the ones that are tagged
with the same faculty tag as this page. Then, I add any of those resources that are Pages to the TreeSet.
*/
	String[] facultyTagArray = {facultyTag.getTagID()};
    for(String s : basePaths){
        RangeIterator<Resource> taggedProjects = tagManager.find(s, facultyTagArray, true);
        while(taggedProjects.hasNext()){
            Resource project = taggedProjects.next().getParent(); //Since the jcr:content is the thing that is tagged, and we want the Page
            if(project.isResourceType("cq:Page")){ // I only care about elements that are Pages
                Page projectPage = resourceResolver.getResource(project.getPath()).adaptTo(Page.class);
                if(projectPage.getProperties().get("cq:template","").equals("/apps/public/templates/project")){ // I only want to list pages of the "Project page" type
                	programsTree.add(projectPage);
                }
            }
        }
    }
	Iterator<Page> printIterator = programsTree.iterator();
%>
    <ul class="SquareBullet">
<%
    while(printIterator.hasNext()){
    	Page printMe = printIterator.next();
    	out.print("<li><a href=\"" + printMe.getPath() + ".html\">");
    	out.print(FHUtilityFunctions.displayTitle(printMe));
    	out.print("</a></li>");
    }
%>    

    </ul>
    <a href="/content/public/en/labs.html">View all labs &amp; projects</a> &gt;
<%	
} else {
	out.print("There are multiple faculty tags on this page.");
}
}
%>
    