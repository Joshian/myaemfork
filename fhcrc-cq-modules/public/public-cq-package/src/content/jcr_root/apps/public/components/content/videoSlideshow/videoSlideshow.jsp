<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.api.WCMMode" %>
<%
int dataHash = (int)Math.floor(Math.random() * Integer.MAX_VALUE);
%>
<div id="videoSlide_<%= dataHash %>">
<cq:text property="title" tagName="h3" tagClass="mainTitle" />
<cq:text property="additionalTitle" tagName="h3" tagClass="additionalTitle" />
<cq:include path="videoPar" resourceType="foundation/components/parsys"/>
</div>

<%
if(WCMMode.fromRequest(request) != WCMMode.EDIT) {
%>
<cq:includeClientLib categories="public.components.videoSlideshow" />
<script type="text/javascript">
var videoSlideshow_<%= dataHash %> = new FH_videoSlideshow("<%= dataHash %>");

/* Add the slideshow to the array of all slideshows on this page */
if (typeof slideShowArray !== "undefined") {
    
    slideShowArray.push(videoSlideshow_<%= dataHash %>);
    
} else {
    
    var slideShowArray = [videoSlideshow_<%= dataHash %>];
    
}
</script>
<%
}
%>
<% if(WCMMode.fromRequest(request) == WCMMode.EDIT){%>
  <div class="cq-edit-only" style="font-family:'Geogrotesque-RegularIt',Arial,Helvetica,Sans-serif; background-color:#ddd; text-align:center;">
    This is the end of the video slideshow. This text will not appear on your webpage.
  </div>
<% } %>