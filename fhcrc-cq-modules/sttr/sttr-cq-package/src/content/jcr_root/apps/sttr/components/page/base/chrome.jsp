<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="java.util.Calendar" %>
<%
Calendar theTime = Calendar.getInstance();
int theYear = theTime.get(Calendar.YEAR);
%>

  <!-- header -->
 <div id="header_element" class="header">
  <div class="header_contents">

   <nav><div class="utility_nav noprint">
    <ul>
     <li class="utility_home"><a href="<%= currentPage.getAbsoluteParent(1).getPath() %>/en.html">Home</a></li>
     <li><a href="https://share.fhcrc.org/sites/cancer-center/sttr/" target="_blank" title="Link to the members site opens in a new browser window">Members</a></li>
     <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/contact-us.html">Contact <span class="droptext">Us</span></a></li>
     <li class="search">
      <a id="utility_nav_search_icon" href="<%= currentPage.getAbsoluteParent(2).getPath() %>/util/search.html">Search</a>     
     </li>      
     <li class="utility_donate_button"><a href="https://secure2.convio.net/fhcrc/site/Donation2?df_id=5340&5340.donation=form1&s_src=WEB&s_subsrc=aeLio33" target="_blank"><img class="rolloverImage" src="<%= currentDesign.getPath() %>/img/sttr_donate_button_static.png" data-hover="<%= currentDesign.getPath() %>/img/sttr_donate_button_hover.png"></a></li>
    </ul>
    <div id="utility_search" class="utility_search">
     <div class="sitesearch">
      <form id="utility_search_form" action="<%= currentPage.getAbsoluteParent(2).getPath() %>/util/search.html">
       <input class="searchbox" type="text" value="" placeholder="Search this site..." name="q" maxlength="2048">
       <input class="searchbutton" type="image" src="<%= currentDesign.getPath() %>/img/sttr_search_magnifying_glass_icon.png" alt="Search" name="submit">
      </form>
     </div>
    </div>
   </div></nav><!-- /utility_nav -->
   
   <div class="logo">
    <div class="logo_contents">
      <a href="<%= currentPage.getAbsoluteParent(1).getPath() %>/en.html">
        <img src="<%= currentDesign.getPath() %>/img/logo-seattle-translational.png" alt="Seattle Translational Tumor Research" /><img src="<%= currentDesign.getPath() %>/img/logo-tumor-research.png" alt="" />
        <img src="<%= currentDesign.getPath() %>/img/sttr_logos_header.png" id="logo-partners" alt="" />
      </a>
    </div><!-- /logo_contents-->
   </div><!-- /logo -->
   
   <nav><div class="global_nav noprint" id="global_nav">
     <!-- mobile menu button(s) -->
     <div class="mobile_nav">
       <div class="mobile_menu_button">
         <img id="mobile_menu_button" src="<%= currentDesign.getPath() %>/img/icon-menu-dropdown.png" alt="Menu" />
       </div>
     </div>
     <!-- ornamentation -->
     <div class="mobile_texture" id="mobile_texture"></div>
     <!-- global menu -->
     <div class="global_menu" id="global_menu">
       <div class="global_menu_contents">
         <ul class="mobile_search">
           <li>
             <div class="sitesearch">
               <form action="<%= currentPage.getAbsoluteParent(2).getPath() %>/util/search.html">
                 <input class="searchbox" type="text" value="" placeholder="Search this site..." name="q" maxlength="2048">
                 <input class="searchbutton" type="image" src="<%= currentDesign.getPath() %>/img/sttr_search_magnifying_glass_icon_mobile.png" alt="Search" name="submit">
               </form>           
             </div>  
           </li>
         </ul>
         <cq:include script="globalNav.jsp"/>
         <ul class="mobile_utilities">
           <li><a href="https://share.fhcrc.org/sites/cancer-center/sttr/" target="_blank" title="Link to the members site opens in a new browser window">Members</a></li>
           <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/contact-us.html">Contact us</a></li>         
         </ul>
       </div>
     </div><!-- /global_menu -->          
   </div></nav><!-- /global_nav -->
  
  </div><!-- /header_contents -->
 </div>
 <!-- /header --> 
 
 <!-- footer -->
 <footer><div id="footer_element" class="footer">
   <div class="footer_content">
      
      <!-- donate feature -->
      <div id="footer-donate">
        <a href="https://secure2.convio.net/fhcrc/site/Donation2?df_id=5340&5340.donation=form1&s_src=WEB&s_subsrc=aeLio43" target="_blank"><img class="rolloverImage" data-hover="<%= currentDesign.getPath() %>/img/STTR_footer_donate_button_over.png" src="<%= currentDesign.getPath() %>/img/STTR_footer_donate_button_stat.png" alt="Donate" /></a>
      </div>

      <!-- social media icons -->
      <div id="socialmedia">
        <a href="https://www.facebook.com/STTRCancer" target="_blank"><img class="rolloverImage" data-hover="<%= currentDesign.getPath() %>/img/sttr-facebook-over.png" src="<%= currentDesign.getPath() %>/img/sttr-facebook-out.png" alt="Facebook" /></a>
        <a href="https://twitter.com/sttrcancer" target="_blank"><img class="rolloverImage" data-hover="<%= currentDesign.getPath() %>/img/sttr-twitter-over.png" src="<%= currentDesign.getPath() %>/img/sttr-twitter-out.png" alt="Twitter" /></a>
        <a href="https://www.youtube.com/channel/UCnWBh22UKMHF4VRKdsQvZqg/featured" target="_blank"><img class="rolloverImage" data-hover="<%= currentDesign.getPath() %>/img/sttr-youtube-over.png" src="<%= currentDesign.getPath() %>/img/sttr-youtube-out.png" alt="YouTube" /></a>
        <a href="http://instagram.com/FredHutch" target="_blank"><img class="rolloverImage" data-hover="<%= currentDesign.getPath() %>/img/sttr-instagram-over.png" src="<%= currentDesign.getPath() %>/img/sttr-instagram-out.png" alt="Instagram" /></a>
        <a href="http://www.linkedin.com/company/fred-hutchinson-cancer-research-center/careers?trk=top_nav_careers" target="_blank"><img class="rolloverImage" data-hover="<%= currentDesign.getPath() %>/img/sttr-linkedin-over.png" src="<%= currentDesign.getPath() %>/img/sttr-linkedin-out.png" alt="LinkedIn" /></a>
        <a href="https://plus.google.com/116048577052683099154/posts" target="_blank"><img class="rolloverImage" data-hover="<%= currentDesign.getPath() %>/img/sttr-google-over.png" src="<%= currentDesign.getPath() %>/img/sttr-google-out.png" alt="Google+" /></a>
      </div>
      <script type="text/javascript" src="/etc/designs/shared/clientlibs/components/textimage.js"></script>        
   
      <div id="footer-logos">
        <a href="http://www.fredhutch.org" target="_blank"><img width="235" height="95" src="<%= currentDesign.getPath() %>/img/logo-fred-hutchinson-cancer-research-center.png" alt="Fred Hutchinson Cancer Research Center" /></a>
        <a href="http://www.uwmedicine.org" target="_blank"><img width="236" height="95" src="<%= currentDesign.getPath() %>/img/logo-university-of-washington-medicine.png" alt="UW Medicine" /></a>
        <a href="http://www.seattlecca.org" target="_blank"><img width="219" height="95" src="<%= currentDesign.getPath() %>/img/logo-seattle-cancer-care-alliance.png" alt="Seattle Cancer Care Alliance" /></a>
      </div>
      <p class="copy">
      <span class="nobreak">&copy;<%= theYear %> Fred Hutchinson Cancer Research Center</span>
      <span class="nobreak">1100 Fairview Ave. N.,</span>
      <span class="nobreak">P.O. Box 19024,</span> 
      <span class="nobreak">Seattle, WA 98109</span>
      <br />
      <a href="http://www.fredhutch.org/en/util/terms-privacy.html">Terms of Use &amp; Privacy Policy</a>
      </p>     
   </div><!-- /footer_content -->
 </div></footer>
 <!-- /footer -->
 
