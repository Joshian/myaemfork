<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="org.fhcrc.tools.FHUtilityFunctions" %>
<%

String text = properties.get("./title","");
String colorClass = properties.get("color","default");
String isArrow = properties.get("arrow","true");
String linkTarget = properties.get("linkTarget","");

Resource r;     
if(!linkTarget.equals("")){
    linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
}

%>

<div class="cssbutton<%= colorClass.equals("default") ? "" : " ".concat(colorClass) %>"><a href="<%=linkTarget.equals("")?"#":linkTarget%>"><%=text %> <%=isArrow.equals("true")?"&raquo;":""%></a></div>