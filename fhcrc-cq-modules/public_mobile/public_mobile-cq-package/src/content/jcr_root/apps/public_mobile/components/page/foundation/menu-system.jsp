<%@include file="/libs/foundation/global.jsp" %>

  <!-- drop-downs. everything below me is absolutely positioned. -->

  <!-- campus drop-down -->
  <div class="campus_menu" id="campus_menu"><a name="campus_menu_items"></a>
    <div class="campus_dropdown">
      <ul>
        <!-- i need the text labels to be on two lines, so add <br/>&nbsp; if they are shorties -->
        <li class="maps"><a href="#1">Maps &amp;<br/> Directions</a></li>
        <li class="cafes"><a href="#2">Caf&eacute;s &amp;<br/>Menus</a></li>
        <li class="directory"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/util/directory.html">Directory<br/>&nbsp;</a></li>
        <li class="bus"><a href="#4">Bus<br/> Schedule</a></li>
        <li class="streetcar"><a href="#5">Streetcar<br/> Schedule</a></li>
        <li class="shuttle"><a href="#6">Shuttle<br/> Schedule</a></li>
        <li class="employees"><a href="#7">Employees<br/> (Email)</a></li>
        <li class="campus"><a href="http://centernet.fhcrc.org">Campus<br/>&nbsp;</a></li>
      </ul>
      <div class="clear"></div>
    </div>
  </div>          

  <!-- menu drop-down -->
  <div class="menu_menu" id="menu_menu"><a name="menu_menu_items" id="menu_menu_items"></a>
    <div class="menu_dropdown_container">
      <div class="menu_dropdown">
        <ul>
          <li class="tl"><a href="#">Donate &amp; Volunteer</a></li>
          <li class="tr"><a href="#">Campus</a></li>
        </ul>
        <ul>
          <li class="ml"><a href="#">Events</a></li>
          <li class="mr"><a href="#">Diseases &amp; Treatment</a></li>
        </ul>
        <ul>
          <li class="ml"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/news.html">News</a></li>
          <li class="mr"><a href="#">Careers</a></li>
        </ul>
        <ul>
          <li class="bl"><a href="#">Education</a></li>
          <li class="br"><a href="#">About Us</a></li>
        </ul>
      </div>
      <div class="clear"></div>
    </div>
    <!--<div class="clear"></div>-->
  </div>

  <!-- search/find drop-down -->
  <div class="search_menu" id="search_menu"><a name="search_menu_items"></a>
    <div class="search_dropdown">
      <!-- search the website -->
      <div class="website">
        <form action="<%= currentPage.getAbsoluteParent(2).getPath() %>/util/search.html" method="GET">
          <input type="input" name="q" class="typein" placeholder="Search mobile site" /><input type="submit" class="submitButton" value="Go"/>
        </form>
      </div>
      <!-- other options -->
      <p class="options">Looking for people or depts? <strong><span class="nobreak"><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/util/directory.html">Use our online directory</a></span></strong></p>
    </div>
  </div>   

