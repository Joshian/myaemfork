<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.util.Iterator,
        com.day.cq.wcm.api.PageFilter,
        org.fhcrc.tools.FHUtilityFunctions,
        org.fhcrc.publicsite.constants.Constants" %>

<%
Page homepage = currentPage.getAbsoluteParent(2);
PageFilter filter = new PageFilter();
Iterator<Page> navElements = homepage.listChildren(filter);
%>



<!--  Ian, the first ul below can build from content tree: -->

    <div class="pushdown_menu" id="pushdown_menu">
      <div class="pushdown_menu_contents">
        <!-- mirror the global nav elements: -->
        <ul>
<%

while (navElements.hasNext()) {
    
    Page child = navElements.next();
%>
          <li><a href="<%= child.getPath() %>.html"><%= FHUtilityFunctions.displayTitle(child) %></a></li>
<%
}
%>
        </ul>
        <hr>
        <!-- add on the utility navigation: -->
        <ul>
          <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/patient-care.html">Patient Care</a></li>
          <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/research.html">Research</a></li>
          <li><a href="<%= currentPage.getAbsoluteParent(2).getPath() %>/education-training.html">Education &amp; Training</a></li>        
        </ul>
      </div>
    </div>