<%@ page session="false"  import="com.day.cq.wcm.foundation.List" %><%

List list = (List)request.getAttribute("list");

%><div class="pagination"><%
    if (list.getPreviousPageLink() != null) {
        %><div class="previous"><%
            %><span class="raquo">&laquo;</span><a href="<%= list.getPreviousPageLink() %>">Previous</a><%
        %></div><%
    }
    if (list.getNextPageLink() != null) {
        %><div class="next"><%
            %><a href="<%= list.getNextPageLink() %>">Next</a><span class="raquo">&gt;</span><%
        %></div><%
    }
%></div>