<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.WCMMode,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.foundation.Image,
    java.lang.Integer,
    org.apache.commons.lang.StringEscapeUtils,
    java.util.ArrayList,
    java.util.TreeSet,
    java.util.Comparator,
    java.util.Iterator,
    java.util.Arrays" %>
<%

/* The arrays are of length 2: x[0] is the order of the slide (as given by user input)
 * x[1] is the number of the slide (1-6). If the order is not given by the user, the 
 * number of the slide is used as the order. 
TreeSet<Float[]> slideOrder = new TreeSet<Float[]>(new Comparator<Float[]>() {
    public int compare(Float[] a, Float[] b){
        Float orderA = a[0];
        Float orderB = b[0];
        if(orderA==null){
            orderA = (float)0;
        }
        if(orderB==null){
            orderB = (float)0;
        }
    
        if(orderA.equals(orderB)){
            return a[1].compareTo(b[1]);
        }
        return orderA.compareTo(orderB);
    }
});  
*/

Comparator<Float[]> arrayCompare = new Comparator<Float[]>() {
    public int compare(Float[] a, Float[] b){
        Float orderA = a[0];
        Float orderB = b[0];
        if(orderA==null){
            orderA = (float)100;
        }
        if(orderB==null){
            orderB = (float)100;
        }
    
        if(orderA.equals(orderB)){
            return a[1].compareTo(b[1]);
        }
        return orderA.compareTo(orderB);
    }
};

/* The metadata is a series of Float arrays, 6 arrays that each contain 2 points of data. */
Float[][] slideMetadata = new Float[6][2];
Float tmpOrder;

for(int metacounter = 0; metacounter < 6; metacounter++) {
	/* If the user doesn't enter an order, set the order at 100 (reasonably large enough that it should come last) */
	tmpOrder = Float.parseFloat(properties.get("highlight_"+(metacounter+1)+"/order","100"));
	slideMetadata[metacounter][0] = tmpOrder;
	slideMetadata[metacounter][1] = (float)(metacounter+1);
}

Arrays.sort(slideMetadata, arrayCompare);


String[] linkTargets = new String[6];
String[] textContent = new String[6];
String[] titles = new String[6];
String[] linkTexts = new String[6];
String temp = "";
String textProperty = "";
String linkTextProperty  = "";
String speed = properties.get("speed","0");
Image[] imageArray = new Image[6];
Boolean[] invalidArray = new Boolean[6];
int slideNumber;

for(int i = 0; i < 6; i++) {
	
	slideNumber = slideMetadata[i][1].intValue();
	/* put the linkTargets into their array */
	temp = properties.get("highlight_"+(slideNumber)+"/linkTarget","");
    if(!"".equals(temp)){
        if(resourceResolver.getResource(temp)!=null && resourceResolver.getResource(temp).isResourceType("cq:Page")) {
            temp += ".html"; //If this is a page, add the .html to the end of the path
        }
    }
    linkTargets[i] = temp;
    
    /* Next is the textContent */
    textContent[i] = properties.get("highlight_"+(slideNumber)+"/text","");
	
    /* The titles */
    titles[i] = properties.get("highlight_"+(slideNumber)+"/title","");
    
    /* The link texts */
    linkTexts[i] = properties.get("highlight_"+(slideNumber)+"/linkText","");
    
    /* The images */
    imageArray[i] = new Image(resource, "image"+(slideNumber));
    
    /* Whether or not the slides are invalid */
    if(properties.get("highlight_"+(slideNumber)+"/isInvalid","false").equals("true")) {
    	invalidArray[i] = true;
    } else {
    	invalidArray[i] = false;
    }
    
}
/*
for(int i = 0; i < linkTargets.length; i++){
    temp = properties.get("highlight_"+(i+1)+"/linkTarget","");
    if(!"".equals(temp)){
        if(resourceResolver.getResource(temp)!=null && resourceResolver.getResource(temp).isResourceType("cq:Page")) {
            temp += ".html"; //If this is a page, add the .html to the end of the path
        }
    }
    linkTargets[i] = temp;
}
for(int i = 0; i < textContent.length; i++){
    textContent[i] = properties.get("highlight_"+(i+1)+"/text","");
}
for(int i = 0; i < titles.length; i++){
    titles[i] = properties.get("highlight_"+(i+1)+"/title","");
}
for(int i = 0; i < linkTexts.length; i++){
    linkTexts[i] = properties.get("highlight_"+(i+1)+"/linkText","");
}
Image[] imageArray = new Image[6];
for(int i = 0; i < imageArray.length; i++){
    imageArray[i] = new Image(resource, "image"+(i+1));
}
*/
%>
<div id="homepageSliderContainer">
<%        
    for(int i = 0; i < imageArray.length; i++) {
      	if(imageArray[i].hasContent()){
/* If the current highlight has been marked invalid, skip it */
      		if(invalidArray[i] == true && WCMMode.fromRequest(request) != WCMMode.EDIT){
      			continue;
      		}
       		imageArray[i].loadStyleData(currentStyle);
       		imageArray[i].setSelector(".img"); // use image script
       	    imageArray[i].setDoctype(Doctype.fromRequest(request));
       	    // add design information if not default (i.e. for reference paras)
            if (!currentDesign.equals(resourceDesign)) {
                imageArray[i].setSuffix(currentDesign.getId());
            }
/* Set the title attribute to a single space to avoid defaulting to the filename */       	    
       	    if(properties.get("image"+(slideMetadata[i][1])+"/jcr:title","").trim().equals("")) {
       	        imageArray[i].setTitle(" ");
       	    }
%>
    <div <%= i==0 ? "id=\"firstContainer\" " : "" %>class="homepageFadeContainer">
        <div class="homepagePhoto">
            <a href="<%= linkTargets[i] != null && !linkTargets[i].trim().equals("") ? linkTargets[i] : "#" %>" 
            onclick="_gaq.push(['_trackEvent','Home Page Click Tracking','Slider Hero','<%= titles[i] != null && !titles[i].trim().equals("") ? StringEscapeUtils.escapeJavaScript(titles[i]) + "_image" : "--no title--" %>']);">
                <% imageArray[i].draw(out); %>
            </a>
   	    </div>
   	    <div class="homepageText">
   	        <h2><%= titles[i] != null && !titles[i].trim().equals("") ? titles[i] : "MISSING TITLE TEXT" %></h2>
            <div class="textline"><%= textContent[i] != null && !textContent[i].trim().equals("") ? textContent[i] : "" %></div>
            <div class="linkline">
              <a href="<%= linkTargets[i] != null && !linkTargets[i].trim().equals("") ? linkTargets[i] : "#" %>" onclick="_gaq.push(['_trackEvent','Home Page Click Tracking','Slider Hero','<%= titles[i] != null && !titles[i].trim().equals("") ? StringEscapeUtils.escapeJavaScript(titles[i]) : "--no title--" %>']);"><%= linkTexts[i] != null && !linkTexts[i].trim().equals("") ? linkTexts[i] : "Learn more" %></a><% if(linkTexts[i] != null && !linkTexts[i].trim().equals("")){ %>&nbsp;<span class="raquo">&raquo;</span> <% } %>
              
            </div>
        </div>
    </div>
<%
        }
    }
%>
    <div id="homepageSliderDots">
<%
    for(int i = 0; i < imageArray.length; i++) {
/* Don't want to display the dot if it doesn't have a matching image */
        if(imageArray[i].hasContent()){ 
/* If the current highlight has been marked invalid, we don't want a dot either */
            if(invalidArray[i] == true && WCMMode.fromRequest(request) != WCMMode.EDIT){
                continue;
            }
%>	            	
	    <div class="homepageDot">
	        <img src="<%= currentDesign.getPath() %>/img/bullets/dot_white.gif" alt="" />
	    </div>
<%	            	
	    }
    }
%>
    </div>
</div>