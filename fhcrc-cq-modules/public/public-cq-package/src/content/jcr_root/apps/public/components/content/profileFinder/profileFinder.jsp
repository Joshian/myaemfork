<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text, 
                 java.util.Iterator, 
                 org.apache.commons.lang.StringEscapeUtils, 
                 java.util.HashMap,
                 com.day.cq.search.QueryBuilder, 
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.result.Hit,
                 com.day.cq.tagging.Tag,
                 com.day.cq.tagging.TagManager,
                 com.day.cq.wcm.foundation.Image,
                 com.day.cq.commons.Doctype,
                 java.util.List,
                 java.util.ArrayList,
                 org.fhcrc.tools.FHUtilityFunctions" %>
<%
Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);
TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
/* searchTerm is the name of the variable that should be passed to the component via the URL */
String queryString = slingRequest.getParameter("searchTerm");
Boolean isQueried = false;
if(!(queryString==null || queryString.trim().equals(""))){
    isQueried = true;
    queryString = queryString.trim(); 
    /* This avoids a bug later where we add wildcard characters to every word in the query string */
}
String[] tags = properties.get("searchTags", new String[0]);
String[] excludeTagStrings = properties.get("excludeTags", new String[0]);
Tag[] excludeTags = new Tag[excludeTagStrings.length];
if(excludeTags.length > 0) {
    for(int i=0; i<excludeTags.length; i++){
        excludeTags[i] = tagManager.resolve(excludeTagStrings[i]);
    }
}
/* Begin building the predicateGroup for the search */ 
HashMap<String, String> map = new HashMap<String, String>();
/* 1_group contains the different divisions that can be selected via checkbox in the component dialog */
map.put("1_group.p.or","true");
if(properties.get("clinical","false").equals("true")){
    map.put("1_group.1_tagid","divisions:clinical");
    if(isQueried){
        map.put("1_group.1_tagid.property","cq:tags");
    } else {
        map.put("1_group.1_tagid.property","jcr:content/cq:tags");
    }
    
}
if(properties.get("phs","false").equals("true")){
    map.put("1_group.2_tagid","divisions:phs");
    if(isQueried){
        map.put("1_group.2_tagid.property","cq:tags");
    } else {
        map.put("1_group.2_tagid.property","jcr:content/cq:tags");
    }
}
if(properties.get("humanbio","false").equals("true")){
    map.put("1_group.3_tagid","divisions:human-bio");
    if(isQueried){
        map.put("1_group.3_tagid.property","cq:tags");
    } else {
        map.put("1_group.3_tagid.property","jcr:content/cq:tags");
    }
}
if(properties.get("basic","false").equals("true")){
    map.put("1_group.4_tagid","divisions:basic-sciences");
    if(isQueried){
        map.put("1_group.4_tagid.property","cq:tags");
    } else {
        map.put("1_group.4_tagid.property","jcr:content/cq:tags");
    }
}
if(properties.get("vidd","false").equals("true")){
    map.put("1_group.5_tagid","divisions:vidd");
    if(isQueried){
        map.put("1_group.5_tagid.property","cq:tags");
    } else {
        map.put("1_group.5_tagid.property","jcr:content/cq:tags");
    }
}
/* If we have any search tags, add them to the search IN THEIR OWN GROUP, 2_group */
if(tags.length > 0){
    if("true".equals(properties.get("tagsAnd","false"))){
        map.put("2_group.p.and","true");
    } else {
        map.put("2_group.p.or","true");
    }
    for(int i=1; i<=tags.length; i++) {
        map.put("2_group."+Integer.toString(i)+"_tagid",tags[i-1]);
        if(isQueried){
            map.put("2_group."+Integer.toString(i)+"_tagid.property","cq:tags");
        } else {
            map.put("2_group."+Integer.toString(i)+"_tagid.property","jcr:content/cq:tags");
        }
    }
}

/* We only care about pages that are Profile Page templates located in the profiles directory */
map.put("path","/content/public/en/labs/profiles");
map.put("p.limit","500"); //Temp fix to display all results. Change when/if pagination is implemented.
map.put("p.guessTotal","true"); // Added to fix Oak 1.0.18 problem

/* Branch point: do we have a query string? This turns out to be a big deal for this finder */
if(!isQueried){
/* If we have no query string, we want to sort by nodename, which requires our hits to be cq:Pages */   
    map.put("orderby","nodename");
    map.put("orderby.index","true");
    map.put("type","cq:Page");
    map.put("property","jcr:content/cq:template");
    map.put("property.value","/apps/public/templates/profile");
} else {
/* If there is a query string, then do a fulltext search on that string and order results by score, which requires our hits to be jcr:content nodes */
    map.put("property","cq:template");
    map.put("property.value","/apps/public/templates/profile");
/* First, we need to add wildcard characters to the end of each word in the search to enable partial string matches */
/* Also, we are building a string specifically for searching tags, whose only difference is that leading slash that prepends the Tag Name in the tagID */
    String[] qArray = queryString.split("\\s+");
    String qString = "";
    String qStringTags = "*";
    for(int i = 0; i < qArray.length; i++){
        qArray[i] += "*";
        qString += qArray[i];
        qStringTags += qArray[i];
        if(i+1 < qArray.length) {
/* JCR searches interpret whitespace as implicit AND, so change that to explicit ORs */
            qString += " OR ";
            qStringTags += " OR ";
        }
    }
/* We want to search both the content of the page and the tags */
    map.put("3_group.p.or","true");
    map.put("3_group.1_fulltext",qString);
//    map.put("3_group.1_fulltext.relPath", "jcr:content");
    map.put("3_group.2_fulltext",qStringTags);
    map.put("3_group.2_fulltext.relPath", "@cq:tags");
//    map.put("3_group.3_tagid",qStringTags);
//    map.put("3_group.3_tagid.property","@cq:tags");
    map.put("orderby","@jcr:score");
    map.put("orderby.sort","desc");
}
List<Hit> hitList = builder.createQuery(PredicateGroup.create(map), session).getResult().getHits();
List<Page> pageList = new ArrayList<Page>();
if(properties.get("searchBox","false").equals("true")){
    %>
        <cq:include resourceType="/apps/public/components/content/searchWidget" path="includedSearchBox" />
    <%
}
if(hitList.size() == 0){
%>  
    <cq:include script="empty.jsp"/>
<%  
} else {
    Page testPage;
    for(int i = 0; i < hitList.size(); i++){
/* If there was a queryString, then we have jcr:content nodes in our hitList, if not, then we have cq:Pages in there */
      if(isQueried==true) {
          testPage = hitList.get(i).getResource().getParent().adaptTo(Page.class);
      } else {
          testPage = hitList.get(i).getResource().adaptTo(Page.class);  
      }
      Boolean skip = false;
      if(excludeTags.length > 0){ //Check to see if the page we are looking at should not be added to the list
          Tag[] pageTags = testPage.getTags();
          for(Tag s : pageTags){
              if(s != null){ //Double-check that this tag exists (possible discrepancy from Author to Publish, for instance)
              for(Tag t : excludeTags){
                 if(t != null){ //Another double-check that this tag exists before attempting to match on it
                    if(t.equals(s)){
                        skip = true;
                        break; // No need to continue the loop if we get a match
                    }
                 }
              }
              }
              if(skip == true){
                  break; // No need to continue the loop if we get a match
              }
          }
       }
      if(skip == false){
          pageList.add(testPage); 
      }
    }

    Iterator<Page> pageIterator = pageList.iterator();

    if(!pageIterator.hasNext()){
%>  
    <cq:include script="empty.jsp"/>
<%      
    } else {
/* Include the alphabetical list of links and associated JavaScript if requested */
        if(properties.get("alphaList","false").equals("true")){
%>
<div class="profileListContainer">
    <cq:includeClientLib categories="public.components.profileFinder" />
    <h3>Alphabetical Listing:</h3>
    <span class="note">Select a letter to display a list of Faculty members</span>
    <div id="alphaSelect" class="alphaSelect">
        <a href="#a" id="a" class="inactive">A</a>&nbsp;
        <a href="#b" id="b" class="inactive">B</a>&nbsp;
        <a href="#c" id="c" class="inactive">C</a>&nbsp;
        <a href="#d" id="d" class="inactive">D</a>&nbsp;
        <a href="#e" id="e" class="inactive">E</a>&nbsp;
        <a href="#f" id="f" class="inactive">F</a>&nbsp;
        <a href="#g" id="g" class="inactive">G</a>&nbsp;
        <a href="#h" id="h" class="inactive">H</a>&nbsp;
        <a href="#i" id="i" class="inactive">I</a>&nbsp;
        <a href="#j" id="j" class="inactive">J</a>&nbsp;
        <a href="#k" id="k" class="inactive">K</a>&nbsp;
        <a href="#l" id="l" class="inactive">L</a>&nbsp;
        <a href="#m" id="m" class="inactive">M</a>&nbsp;
        <a href="#n" id="n" class="inactive">N</a>&nbsp;
        <a href="#o" id="o" class="inactive">O</a>&nbsp;
        <a href="#p" id="p" class="inactive">P</a>&nbsp;
        <a href="#q" id="q" class="inactive">Q</a>&nbsp;
        <a href="#r" id="r" class="inactive">R</a>&nbsp;
        <a href="#s" id="s" class="inactive">S</a>&nbsp;
        <a href="#t" id="t" class="inactive">T</a>&nbsp;
        <a href="#u" id="u" class="inactive">U</a>&nbsp;
        <a href="#v" id="v" class="inactive">V</a>&nbsp;
        <a href="#w" id="w" class="inactive">W</a>&nbsp;
        <a href="#x" id="x" class="inactive">X</a>&nbsp;
        <a href="#y" id="y" class="inactive">Y</a>&nbsp;
        <a href="#z" id="z" class="inactive">Z</a>&nbsp;
        [<a href="#showAll" id="showAll" class="inactive">Show All</a>]
    </div>
</div>
<%
        }
while(pageIterator.hasNext()){
/* nextPage is the Profile Page of the faculty member we are working with */    
    Page nextPage = pageIterator.next();
/* We need the first letter of the Faculty member's last name. Fortunately, by design, all profile pages are built with name = <Last Name>_<First Name> */
    char alphaData = nextPage.getName().charAt(0);
    String pageDescription = nextPage.getDescription();
/* Print out the opening div complete with all the display options chosen by the user */    
    out.print("<div class=\"profileresult clearfix");
    if(properties.get("omitPhoto","false").equals("true")){
        out.print(" noimage");
    }
    if(properties.get("omitAllAppt","false").equals("true")){
        out.print(" noappt");
    }
    if(properties.get("omitSecondaryAppt","false").equals("true")){
        out.print(" nosecondaryappt");
    }
    if(properties.get("omitDescription","false").equals("true")){
        out.print(" nodescription");
    }
    if(properties.get("omitContact","false").equals("true")){
        out.print(" nocontact");
    }
    if(properties.get("layout","horizontal").equals("vertical")){
        out.print(" vertical");
    }
    out.print("\" data-alpha=\"" + alphaData + "\">");

/* Grab the faculty member's profile image (placed on the profile page in the upper left) and print it out */
    Resource r = nextPage.getContentResource("profileImage");
    String img = "";
    if (r != null) {
        Image image = new Image(r);
        img = nextPage.getPath() + "/_jcr_content/profileImage.img.png" + image.getSuffix(); 
        // prepend context path to img
        img = request.getContextPath() + img;
    }

/*
    Image profilePic = new Image(nextPage.getContentResource(), "profileImage");
    profilePic.loadStyleData(currentStyle);
    profilePic.setSelector(".img"); // use image script
    profilePic.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        profilePic.setSuffix(currentDesign.getId());
    }
*/    
if(!properties.get("omitPhoto","false").equals("true")){
%>
        <div class="profile_image">
<% 
    if(!img.equals("")){
        out.print("<img src='"+img+"'>");
    } else {
/* If they don't have a profile image, use a default one */
    out.print("<img src=\"" + currentDesign.getPath()  +"/img/defaults/profile-default.gif\" alt=\"Researcher silhouette\" />");
    }
%>
        </div>
<%
}
%>
        <div class="profile_text">
<% 
/* Print out the faculty member's name, which should be the title of the faculty profile page */
    if(nextPage.getTitle() != null) {
        out.print("<div class=\"name\"><a href=\"" + nextPage.getPath() + ".html\">" + nextPage.getTitle() + "</a></div>");
    } else {
        out.print("<div class=\"name\">" + nextPage.getName() + "</div>");
    }
/* Print out all the listed appointments. There are a maximum of 5 allowed on a profile page. */
    for(int i = 1; i <= 5; i++) {
        String apptTitle = nextPage.getProperties().get("titleorg"+i+"/title", "NULL");
        String linkTitle = "";
        String linkPath = "";
/* If any of the possible outputs come back null, we skip that titleorg pair */     
        if(apptTitle.equals("NULL")){
            continue;
        }
/* Everything else is dependent on whether the titleorg property linkType is internal or external */
/* If the appointment linkType is internal, then use the page properties of the pagePath to return the text to print out */
        if(nextPage.getProperties().get("titleorg"+i+"/linkType", "NULL").equals("internal")){
            String pagePath = nextPage.getProperties().get("titleorg"+i+"/internalPagePath","NULL");
            if(pagePath.equals("NULL") || resourceResolver.getResource(pagePath) == null){
                continue;
            } else {
                /*linkTitle = resourceResolver.getResource(pagePath).adaptTo(Page.class).getPageTitle(); // try the page's h1 title first...
                if(linkTitle == null){
                    linkTitle = resourceResolver.getResource(pagePath).adaptTo(Page.class).getTitle(); // but use the asset's title otherwise
                }
                if(linkTitle == null){
                    linkTitle = resourceResolver.getResource(pagePath).getName();
                }*/
                linkPath = pagePath + ".html";
                linkTitle = FHUtilityFunctions.displayTitle(resourceResolver.getResource(pagePath).adaptTo(Page.class));
            }
/* If the linkType is external, then everything we need is stored in the properties of the titleorg in question */          
        } else if(nextPage.getProperties().get("titleorg"+i+"/linkType", "NULL").equals("external")){
            linkTitle = nextPage.getProperties().get("titleorg"+i+"/linkTitle", "NULL");
            linkPath = nextPage.getProperties().get("titleorg"+i+"/url", "NULL");
            if(linkTitle.equals("NULL") || linkPath.equals("NULL")){
                continue;
            }
/* If the linkType is neither internal nor external, then skip this titleorg */
        } else {
            continue;
        }
/* The primary appointment gets a special class, so check to see if this is the first one */
        if(i == 1){
            out.print("<div class=\"appt\">" + apptTitle + ", <a href=\"" + linkPath + "\"");
            if(nextPage.getProperties().get("titleorg"+i+"/linkType", "NULL").equals("external")){
                out.print(" target=\"_blank\"");
            }
            out.print(">" + linkTitle + "</a></div>");
        } else {
            out.print("<div class=\"appt2\">" + apptTitle + ", <a href=\"" + linkPath + "\"");
            if(nextPage.getProperties().get("titleorg"+i+"/linkType", "NULL").equals("external")){
                out.print(" target=\"_blank\"");
            }
            out.print(">" + linkTitle + "</a></div>");
        }
    }
/* If there is a page description, print it out in its own div */
    if(pageDescription != null && !pageDescription.trim().equals("")){
        out.print("<div class=\"description\">" + pageDescription + "</div>");
    }
/* If there is a phone number, print it out in its own div */
    if(!nextPage.getProperties().get("phone/text","").equals("")){
        out.print("<div class=\"phone\">Phone:&nbsp;" + nextPage.getProperties().get("phone/text","") + "</div>");
    }
/* If there is an email address, print it out in its own div */
    if(!nextPage.getProperties().get("email/text","").equals("")){
        out.print("<div class=\"phone\">Email:&nbsp;<a href=\"mailto:" + nextPage.getProperties().get("email/text","") + "\">" + nextPage.getProperties().get("email/text","") + "</a></div>");
    }
    if(!nextPage.getProperties().get("fax/text","").equals("")){
        out.print("<div class=\"phone\">Fax:&nbsp;" + nextPage.getProperties().get("fax/text","") + "</div>");
    }
%>
        </div>
    </div>   
<%
}
}
}
%>