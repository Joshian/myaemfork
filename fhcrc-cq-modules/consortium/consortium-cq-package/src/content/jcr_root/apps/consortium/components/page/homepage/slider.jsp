  <%@include file="/libs/foundation/global.jsp"%>
  
  <div class="royalSlider rsMinW fullWidth">
  
      <!-- image and content -->
      <div class="rsContent caption_no_background hero"><!-- hero is needed for the first item, ie7 -->
          <div class="imageslide"><!-- can encapsulate the image if you want -->
            <a href="/content/consortium/en/about/partner-institutions.html"><img class="rsImg" src="<%= currentDesign.getPath() %>/img/slider/CC_home_slider_venn.jpg" /></a>
          </div>
          <div class="infoBlock">
            <h4>FRED HUTCH / UNIVERSITY OF WASHINGTON CANCER CONSORTIUM</h4>
            <p>A National Cancer Institute-designated Comprehensive Cancer Center and a world-class partnership in cancer research, patient care and education.</p>
          </div>
      </div>
  
      <!-- image and content -->
      <div class="rsContent">
          <img class="rsImg" src="<%= currentDesign.getPath() %>/img/slider/CC_home_slider_clinical.jpg" />
          <div class="infoBlock">
            <h4>CLINICAL / TRANSLATIONAL SCIENCE</h4>
            <p>ADVANCES the understanding of diseases and translates it into the development of new cancer treatments.</p>
          </div>
      </div>
      
      <!-- image and content -->
      <div class="rsContent">
          <img class="rsImg" src="<%= currentDesign.getPath() %>/img/slider/CC_home_slider_basic.jpg" />
          <div class="infoBlock">
            <h4>BASIC SCIENCES </h4>
            <p>DEVELOPS the foundation for understanding the causes of diseases leading to the development of new treatments.</p>
          </div>
      </div>    
      
      <!-- image and content -->
      <div class="rsContent">
          <img class="rsImg" src="<%= currentDesign.getPath() %>/img/slider/CC_home_slider_phs.jpg" />
          <div class="infoBlock">
            <h4>PUBLIC HEALTH SCIENCES</h4>
            <p>IDENTIFIES strategies that will reduce the incidence of and mortality from cancer.</p>
          </div>
      </div>    
  
      <!-- image and content -->
      <div class="rsContent">
          <img class="rsImg" src="<%= currentDesign.getPath() %>/img/slider/CC_home_slider_global_health.jpg" />
          <div class="infoBlock">
            <h4>GLOBAL HEALTH</h4>
            <p>CREATES new, low-cost prevention and treatment strategies that benefit millions of people worldwide.</p>
          </div>
      </div>

      <!-- image and content -->
      <div class="rsContent">
          <a href="/content/consortium/en/about/collaborative-clinical-research.html"><img class="rsImg" src="<%= currentDesign.getPath() %>/img/slider/CC_home_slider_info.png" /></a>
          <!--<div class="infoBlock">
             <h4>GLOBAL HEALTH</h4>
            <p>CREATES new, low-cost prevention and treatment strategies that benefit millions of people worldwide.</p>
          </div>-->
      </div>

  
  </div>
  <div class='clear'></div>

  <cq:includeClientLib js="consortium.libs.royalslider" />  
  
  <script type="text/javascript">
    jQuery(document).ready(function($) {//return;
        $(".royalSlider").royalSlider({
            keyboardNavEnabled: true,
            autoScaleSlider: true,
            loop: true,
            transitionType: ' move',
            globalCaption: true,
            imageScaleMode: 'fill',
            autoScaleSliderWidth: 980,     
            autoScaleSliderHeight: 370,
            autoPlay: {
                enabled: true,
                pauseOnHover: true,
                delay: 7500
            },
            arrowsNav: true,
            arrowsNavAutoHide: false,
            navigateByClick: false,
            deeplinking: {
              enabled: true
            }    
        });
    });
  </script>

<!-- mission for smallest mobile -->
<div class="mission">
  <div class="mission_content">           
    <cq:include path="homepagemobilemission" resourceType="consortium/components/content/textComponent"/>
  </div>
</div>


