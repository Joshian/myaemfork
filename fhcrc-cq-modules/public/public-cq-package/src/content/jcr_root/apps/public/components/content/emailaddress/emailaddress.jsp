<%@include file="/libs/foundation/global.jsp"%><%
   String text = properties.get("text", "");
   if (!text.isEmpty()) {
%>
    <a href="mailto:<%= text %>"><%= text %></a>
<% 
    } else {
%>   
    <cq:include script="empty.jsp"/>
<%
    }
%>   