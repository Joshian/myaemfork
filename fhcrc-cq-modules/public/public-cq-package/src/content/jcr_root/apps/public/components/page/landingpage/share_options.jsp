<%@include file="/libs/foundation/global.jsp" %>
      <!-- Lockerz Share BEGIN -->
      <span class="share_options">
        <a class="a2a_dd" href="http://www.addtoany.com/share_save">
    		<img src="<%= currentDesign.getPath() %>/img/buttons/plus_icon_static.png" class="rolloverImage" 
    		data-hover="<%= currentDesign.getPath() %>/img/buttons/plus_icon_hover.png"><span class="share_link_text">Share</span>
    	</a>
      </span>
      <script type="text/javascript">
      <!--
        a2a_config = {};
        a2a_config.onclick=1;
        a2a_config.show_title=0;
        a2a_config.num_services=10;
        /*a2a_config.linkname=document.title;
        a2a_config.linkurl=location.href;*/
        a2a_config.prioritize=["facebook","twitter","linkedin","digg","delicious","myspace","read_it_later","squidoo","technorati_favorites","care2_news"];
        /*a2a_config.color_main="D7E5ED";*/
        /*a2a_config.color_border="AECADB";*/
        a2a_config.color_link_text="123054";
        a2a_config.color_link_text_hover="24548d";
      //-->
      </script>
      <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
      <!-- Lockerz Share END -->   