<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.WCMMode" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
String pubTitle; 

final String apiUrl = "http://is.ext.fhcrc.org/open/mobile/enews/confirm.php";


%>
<h2>Subscribe to Enews from Fred Hutch</h2>

<div class="no-js">

  <p>To subscribe to Enews from Fred Hutch, please <a href="http://getinvolved.fhcrc.org/site/Survey?SURVEY_ID=1360&ACTION_REQUIRED=URI_ACTION_USER_REQUESTS" target="_blank">click here</a>.</p>
  
</div>

<div class="js visuallyHidden">

  <form action="">
  
    <label for="fname">First name:</label>
    <input type="text" name="first-name" id="fname" required>
    
    <label for="lname">Last name:</label>
    <input type="text" name="last-name" id="lname" required>
    
    <label for="email">Email:</label>
    <input type="email" name="email" id="email" required>
    
    <input class="submitButton" type="submit" value="Subscribe">  
  
  </form>
  
</div>

<div class="spinner visuallyHidden">
  <img src="loader64.gif">
</div>

<div class="successMessage visuallyHidden">
  <h3>Thank you for your interest in our publications</h3>
  <p>An email has been sent to <span class="userEmail">the email address you supplied</span>. Please check your email and follow the instructions to confirm your subscription.</p>
  <p><a href="" class="formReset">Subscribe another email address</a>.</p>
</div>

<div class="errorMessage visuallyHidden">
  <h3>Error</h3>
  <p class="theError">An error has occurred: <span class="errorType">UNKNOWN ERROR</span>. Please <a href="" class="formReset">try again</a>. If the error persists, please contact <a href="mailto:webadm@fhcrc.org">webadm@fhcrc.org</a>.</p>
</div>

<cq:includeClientLib js="mobile.components.enewsSubscriber" />
<script type="text/javascript">
componentApp.subscriptionComponent.initializeSubscription();
</script>