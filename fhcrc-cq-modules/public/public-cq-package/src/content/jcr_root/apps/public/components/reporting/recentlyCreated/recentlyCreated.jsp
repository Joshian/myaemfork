<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text, 
                 java.util.Iterator, 
                 org.apache.commons.lang.StringEscapeUtils, 
                 java.util.HashMap,
                 com.day.cq.search.QueryBuilder, 
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.result.Hit,
                 com.day.cq.tagging.Tag,
                 com.day.cq.tagging.TagManager,
                 java.util.List,
                 java.util.ArrayList,
                 java.util.TreeSet,
                 java.util.Comparator,
                 java.util.AbstractCollection,
                 java.util.Date,
                 java.text.SimpleDateFormat" %>
<%
Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);
HashMap<String, String> map = new HashMap<String, String>();

map.put("type","cq:Page");
map.put("path","/content/public/en");
map.put("relativedaterange.property","jcr:created");
map.put("relativedaterange.lowerBound","-1w");
map.put("orderby","path");
map.put("p.limit","1000");

List<Hit> hitList = builder.createQuery(PredicateGroup.create(map), session).getResult().getHits();
ArrayList<Page> pageList = new ArrayList<Page>();

if(hitList.size() == 0){
%>
    <cq:include script="empty.jsp"/>
<%
} else {
    for(int i = 0; i < hitList.size(); i++){
          Page testPage = hitList.get(i).getResource().adaptTo(Page.class);
          pageList.add(testPage);
    }
    Iterator<Page> pageIterator = pageList.iterator();
    if(!pageIterator.hasNext()){
%>  
            <cq:include script="empty.jsp"/>
<%
    } else {
%>
<!-- Table of results -->
<table>
    <tr>
        <th>Page title</th>
        <th>Created on</th>
        <th>Created by</th>
    </tr>


<%
        while(pageIterator.hasNext()) {
            Page nextPage = pageIterator.next();
            Date creationDate = nextPage.getProperties().get("jcr:created",Date.class);
            SimpleDateFormat f = new SimpleDateFormat("MMM dd, k:mm");
%>
    <tr>
        <td><a href="<%= nextPage.getPath() %>.html" target="_blank"><%= nextPage.getTitle() != null ? nextPage.getTitle() : nextPage.getName() %></a></td>
        <td><%= f.format(creationDate) %></td>
        <td><%= nextPage.getProperties().get("jcr:createdBy","Cannot determine") %></td>
    </tr>
<%
        }
    }
}
%>
</table>
