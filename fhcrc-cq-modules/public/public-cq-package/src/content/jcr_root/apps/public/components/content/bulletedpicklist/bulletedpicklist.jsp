<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="org.fhcrc.tools.FHUtilityFunctions" %>

<div class="bulleted_picklist">

<%
String displayTitle; 
String[] pagePaths = properties.get("pages", String[].class);
if (pagePaths != null && pagePaths.length > 0) {
    boolean bullets = properties.get("bullets", Boolean.TRUE);
    log.info("bullets = " + bullets);
    if (bullets) { 
    %>
        <ul class="SquareBullet">
    <%
    } else {
    %>
        <ul class="nobullets">
    <%
    }
    for (String pagePath : pagePaths) {
        Page aPage = pageManager.getPage(pagePath);
        if (aPage != null) {
        %>
              <li><a href="<%= aPage.getPath() + ".html" %>"><%= FHUtilityFunctions.displayTitle(aPage) %></a></li>
        <%
        }
    }
    %>
    </ul>
<%
} else {
%>
    <cq:include script="empty.jsp"/>
<%
}
%>

</div><!-- /bulleted_picklist -->