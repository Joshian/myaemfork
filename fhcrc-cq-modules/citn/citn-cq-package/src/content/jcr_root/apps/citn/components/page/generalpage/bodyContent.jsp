<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.cq.wcm.api.WCMMode" %>
<div class="dropshadow"></div>
<div class="row-fluid midcontent">
    <div class="columnleft span4">
        <div class="logo">
              <a href="/content/citn/en.html"><img src="/content/dam/citn/logo_white_bg.gif" id="home_logo" alt="Cancer Immunotherapy Trials Network" border="0"/></a>
        </div>
        <div id="sidebar_divider"></div>
        <div class="leftpar">
              <!-- left column (sidebar) -->
              <div id="fh_sidebar" class="fh_sidebar">
                  <div id="fh_sidebar_content" class="fh_sidebar_content">
                    <cq:include script="clusterNav_page.jsp" /> 
                  </div>
<%
/* Search component is causing issues in Firefox 23+ in author environment */
                  if (WCMMode.fromRequest(request) == WCMMode.DISABLED) {
%>
                  <cq:include path="search" resourceType="/apps/citn/components/content/search"/>
<%
                	  
                  }
%>
                  
                  <cq:include path="leftpar" resourceType="foundation/components/parsys"/>
              </div>
        </div><!-- /leftpar -->

    </div>
    <div class="maincontent span8">
       <cq:include path="breadcrumb" resourceType="foundation/components/breadcrumb"/>
       <cq:include path="title" resourceType="foundation/components/title"/>
       <cq:include path="mainpar" resourceType="foundation/components/parsys"/>
    </div>

</div>