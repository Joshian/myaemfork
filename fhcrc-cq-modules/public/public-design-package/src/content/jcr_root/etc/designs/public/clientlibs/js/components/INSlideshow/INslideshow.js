var componentApp = (function(componentApp) {
	
	componentApp.INslider = (function(INslider) {
		
		INslider.INslideshow = function() {
			
			var $active = $('.alternateSlider .slideContainer div.active');

		    if ($active.length == 0) $active = $('.alternateSlider .slideContainer div:last');

		    var $next = $active.next().length ? $active.next()
		        : $('.alternateSlider .slideContainer div:first');

		    $active.addClass('last-active');

		    $next.css({ opacity: 0.0 })
		        .addClass('active')
		        .animate({ opacity: 1.0 }, 1500, function () {
		            $active.removeClass('active last-active');
		    });
			
		};
		
		return INslider;
		
	})(componentApp.INslider || {});
	
	return componentApp;
	
})(componentApp || {});