<%@ page import="java.util.*" %>
<%@include file="/libs/foundation/global.jsp"%>
<%

/* Initialize to -1 and the empty string as failure cases */
int nextFHCRCHour = -1, nextFHCRCMinute = -1, nextSCCAHour = -1, nextSCCAMinute = -1;
String nextFHCRCShuttle = "", nextSCCAShuttle = "";

/* These schedules don't tend to change, but potentially change at the beginning of the calendar year. 
Check with Transportation department for most current schedule */

int[] FHCRC_HOURS = {6,7,8,8,9,10,10,11,12,12,13,14,14,15,16,16,17,18,18};
int[] FHCRC_MINUTES = {40,20,0,40,20,0,40,20,0,40,20,0,40,20,0,40,20,0,40};
int[] SCCA_HOURS = {7,7,8,9,9,10,11,11,12,13,13,14,15,15,16,17,17,18};
int[] SCCA_MINUTES = {0,40,20,0,40,20,0,40,20,0,40,20,0,40,20,0,40,20};

Calendar currentTime = Calendar.getInstance();

int currentHour = currentTime.get(Calendar.HOUR_OF_DAY);
int currentMinute = currentTime.get(Calendar.MINUTE);

for (int i = 0; i < FHCRC_HOURS.length; i++) {
	
	if (currentHour < FHCRC_HOURS[i] || (currentHour == FHCRC_HOURS[i] && currentMinute < FHCRC_MINUTES[i])) {
			
		nextFHCRCHour = FHCRC_HOURS[i];
		nextFHCRCMinute = FHCRC_MINUTES[i];
		break;
		
	}
	
}

for (int i = 0; i < SCCA_HOURS.length; i++) {
    
    if (currentHour < SCCA_HOURS[i] || (currentHour == SCCA_HOURS[i] && currentMinute < SCCA_MINUTES[i])) {
        
        nextSCCAHour = SCCA_HOURS[i];
        nextSCCAMinute = SCCA_MINUTES[i];
        break;
        
    }
    
}

/* Convert back from 24-hour time to 12-hour time */

if (nextFHCRCHour > 12) {
	
	nextFHCRCHour %= 12;
	
}

if (nextSCCAHour > 12) {
    
    nextSCCAHour %= 12;
    
}

/* Construct the time strings */

if (nextFHCRCHour >= 0 && nextFHCRCMinute >= 0) {
	
	nextFHCRCShuttle = Integer.toString(nextFHCRCHour);
	nextFHCRCShuttle = nextFHCRCShuttle.concat(":");
	
	if (nextFHCRCMinute == 0) {
	    nextFHCRCShuttle = nextFHCRCShuttle.concat("00");
	} else {
		nextFHCRCShuttle = nextFHCRCShuttle.concat(Integer.toString(nextFHCRCMinute));
	}
	
}

if (nextSCCAHour >= 0 && nextSCCAMinute >= 0) {
    
    nextSCCAShuttle = Integer.toString(nextSCCAHour);
    nextSCCAShuttle = nextSCCAShuttle.concat(":");
    
    if (nextSCCAMinute == 0) {
        nextSCCAShuttle = nextSCCAShuttle.concat("00");
    } else {
        nextSCCAShuttle = nextSCCAShuttle.concat(Integer.toString(nextSCCAMinute));
    }
    
}

%>

<div class="nextShuttleWidget">

<%

/* Print out the next shuttle times if they exist */

if (!nextFHCRCShuttle.equals("")) {

%>
<div class="nextShuttleContainer FHCRC">
<p>The next shuttle will leave the Arnold Building at: </p>
<p class="shuttleTime"><%= nextFHCRCShuttle %></p>
</div>
<%

} else {
	
%>
<div class="nextShuttleContainer FHCRC">
<p>There are no shuttles leaving from the Arnold Building scheduled for the remainder of the day.</p>
</div>
<%

}

if (!nextSCCAShuttle.equals("")) {

%>
<div class="nextShuttleContainer SCCA">
<p>The next shuttle will leave the <abbr title="Seattle Cancer Care Alliance">SCCA</abbr> Building at:</p>
<p class="shuttleTime"><%= nextSCCAShuttle %></p>
</div>
<%

} else {
    
%>
<div class="nextShuttleContainer SCCA">
<p>There are no shuttles leaving from the <abbr title="Seattle Cancer Care Alliance">SCCA</abbr> Building scheduled for the remainder of the day.</p>
</div>
<%

}

/* If requested, show the full shuttle schedule in a table */
if (!properties.get("fullSchedule","").equals("")) {
%>

<table id="shuttleScheduleTable" dir="ltr" summary="Fred Hutch and SCCA shuttle schedules" border="1">
    <caption>Fred Hutch/SCCA shuttle schedules</caption>
    <thead>
        <tr>
            <th scope="col"><abbr title="Fred Hutchinson Cancer Research Center">FHCRC</abbr> Arnold</th>
            <th scope="col"><abbr title="Seattle Cancer Care Alliance">SCCA</abbr></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>6:40</td>
            <td>7:00</td>
        </tr>
        <tr>
            <td>7:20</td>
            <td>7:40</td>
        </tr>
        <tr>
            <td>8:00</td>
            <td>8:20</td>
        </tr>
        <tr>
            <td>8:40</td>
            <td>9:00</td>
        </tr>
        <tr>
            <td>9:20</td>
            <td>9:40</td>
        </tr>
        <tr>
            <td>10:00</td>
            <td>10:20</td>
        </tr>
        <tr>
            <td>10:40</td>
            <td>11:00</td>
        </tr>
        <tr>
            <td>11:20</td>
            <td>11:40</td>
        </tr>
        <tr>
            <td>12:00</td>
            <td>12:20</td>
        </tr>
        <tr>
            <td>12:40</td>
            <td>1:00</td>
        </tr>
        <tr>
            <td>1:20</td>
            <td>1:40</td>
        </tr>
        <tr>
            <td>2:00</td>
            <td>2:20</td>
        </tr>
        <tr>
            <td>2:40</td>
            <td>3:00</td>
        </tr>
        <tr>
            <td>3:20</td>
            <td>3:40</td>
        </tr>
        <tr>
            <td>4:00</td>
            <td>4:20</td>
        </tr>
        <tr>
            <td>4:40</td>
            <td>5:00</td>
        </tr>
        <tr>
            <td>5:20</td>
            <td>5:40</td>
        </tr>
        <tr>
            <td>6:00</td>
            <td>6:20</td>
        </tr>
        <tr>
            <td>6:40</td>
            <td>&mdash;</td>
        </tr>
    </tbody>
</table>

<%

}

%>
</div>