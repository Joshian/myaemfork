var componentApp = (function(componentApp) {
	
	componentApp.textimageComponent = (function(textimageComponent) {
		
		textimageComponent.hasInitializedMouseovers = textimageComponent.hasInitializedMouseovers || false;
		
		var initializeMouseovers = function() {
			
			if(!textimageComponent.hasInitializedMouseovers) {
				$(window).load(function(){ addMouseovers(); });
			} 
			textimageComponent.hasInitializedMouseovers = true;
			return;
			
		};
		
		var addMouseovers = function() {

			var theImages = $(".rolloverImage");
			
			theImages.each( function() {
				
				var that = $(this);
				var offsetWidth = that.outerWidth();
				var offsetHeight = that.outerHeight();
				
				that.attr({
					width: offsetWidth + "px",
					height: offsetHeight + "px"
				});
				
				that.mouseover(toggleMouseover);
				that.mouseout(toggleMouseover);
				
			});
						
		};
		
		var toggleMouseover = function(e) {

			var that = e.target || e.srcElement;
			var currentSrc = that.getAttribute("src");
			
			if(that.dataset) {
				that.setAttribute("src", that.dataset.hover);
				that.dataset.hover = currentSrc;
			} else {
				that.setAttribute("src", that.getAttribute("data-hover"));
				that.setAttribute("data-hover", currentSrc);
			}
			
		}
		
		initializeMouseovers();
		
		return textimageComponent;

	})(componentApp.textimageComponent || {});
	
	return componentApp;
	
})(componentApp || {});