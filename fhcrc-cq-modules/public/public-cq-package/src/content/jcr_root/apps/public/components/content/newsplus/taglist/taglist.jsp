<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text, 
	java.util.*,
	org.fhcrc.general.search.ISearchCacheService,
	org.fhcrc.general.search.impl.SearchCacheService,
    com.day.cq.search.QueryBuilder, 
    com.day.cq.search.PredicateGroup,
	com.day.cq.search.result.SearchResult,
    com.day.cq.search.result.Hit,    
    com.day.cq.wcm.api.WCMMode, 
    org.fhcrc.publicsite.constants.Constants,
    com.day.cq.tagging.Tag,
    com.day.cq.tagging.TagManager, 
    java.net.URLEncoder, 
    java.util.Arrays, 
    java.util.Comparator, 
    java.lang.StringBuilder, 
    java.util.List, 
    java.util.ArrayList" %>
<% 
        /*
                     News tag lister. Use this component to print out a clickable list of tags for this news page.
                     Interrogates the current page, grabs its tags, and for EACH tag:

                     1. calculates how many other pages are also tagged
                     2. if more than 1 other page is tagged, print out this tag, clickable to the news landing page newsroll "view by tag" feature

                     User can supply a list of tags to "omit" from the display. If the page has an omitted tag, don't display that tag
                     in the taglist.

Enhancement:

It's convenient to use this component directly IN ANOTHER COMPONENT to display a taglist for this page. For example,
the NewsRoll component needs to do this: it prints out an excerpt followed by the taglist **of each page**.
So, you'll need a way to pass in the "target page" into this component. To do this, use the solution provided by 
Daycare under ticket https://daycare.day.com/home/fhcrc/fhcrc_us/customer_services/59092.html
You are passing in the path of the page in question in an attribute.

*/ 
Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);

// check whether I should use currentPage or some other page to grab the list of tags
Page p = currentPage; // default is to use the current page
String targetPagePath = (String)request.getAttribute("targetPageForTagList"); // null if there is no attribute sent
if(targetPagePath!=null){
    Resource r = resourceResolver.getResource(targetPagePath);
    if( r!=null ){
        Page pg = r.adaptTo(Page.class);
        if( pg!=null ){
		   p = pg;
        }
    }
}

//out.println(p.getPath());



                     //out.print("I am the component. <b>Current page is " + currentPage.getPath() + "</b>");


TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
Tag[] pageTags = p.getTags(); 
String taglabel = properties.get("text",String.class); // default label.
List<String> tagList = new ArrayList<String>();


// Here are the tags to exclude from the taglist.
String[] excludeTagStrings = properties.get("excludeTags", new String[0]);
Tag[] excludeTags = new Tag[excludeTagStrings.length];
if(excludeTags.length > 0) {
    for(int i=0; i<excludeTags.length; i++){
        excludeTags[i] = tagManager.resolve(excludeTagStrings[i]);
    }
}


// we want sorted tags by tag title (case insensitive)
Comparator<Tag> c = new Comparator<Tag>() {
    public int compare(Tag a, Tag b) {
       String titleA = a.getTitle();
       String titleB = b.getTitle();
       if( titleA.equals(titleB) ){
         return a.getTagID().compareTo( b.getTagID() );
       } else {
         return titleA.compareToIgnoreCase(titleB);
       }
    }
};
Arrays.sort( pageTags, c );

List<String> taglist = new ArrayList<String>();

String taglist_root = resourceResolver.map("/content/public/en/news.tag."); // allow prefix nuking to work

ISearchCacheService cacheService = sling.getService(ISearchCacheService.class);
for(Tag s : pageTags){
    if( Arrays.asList(excludeTags).contains(s) ) { continue; }
    String[] arr = {s.getTagID()}; 
    log.debug("Starting tag = " + s.getTagID());
    
    //--------------------------------------------------------------------
    // Instead of searching for pages with tags every time, call the 
    // cache.  It will refresh on a TTL set in OSGI configuration.  This
    // will keep from hammering the lucene engine with the same requests
    // over and over.
    //--------------------------------------------------------------------
    Long ct0 = cacheService.getPageCountbyPathAndTag(session, builder, Constants.PATH_TO_CENTERNEWS_FOLDER, s.getTagID());
    Long ct1 = cacheService.getPageCountbyPathAndTag(session, builder, Constants.PATH_TO_HUTCH_MAGAZINE_FOLDER, s.getTagID());
	
    Long ct = 0L;
    if( ct0==-1 || ct1==-1 ){
        ct = -1L; // if EITHER branch returns -1 you don't really know how many articles have this tag
    } else {
      ct = ct0 + ct1;
    }
    //if(ct0==-1||ct1==-1) ct = s.getCount(); // i've noticed that tagManager.find().getSize() can return -1, so if that happens, use <tag>.getCount() which is accurate but opens it up to the full jcr content
    if(ct>1||ct==-1){
        String tmp = "";
        //String title= ct>1 ? "title='View "+Long.toString(ct)+" stories with this tag'": "";
        String title= "title='View "+ (ct==-1 ? "more" : Long.toString(ct)) +" stories with this tag'";
        tmp+="<a x-cq-linkchecker='skip' href='"+ taglist_root +URLEncoder.encode(s.getTagID(), "UTF-8").replace("%2F","%60").replace("+","%20")+".html' "+title+">"; // because %2F (/) does not pass correctly, I use %60 (`) in the encoding; I also need to encode "+" as %20 for some reason
    	tmp+=s.getTitle();
    	tmp+="</a>";
        //out.write(", ");
        taglist.add(tmp);
    }
}

if(taglist.size()>0){
    if(taglabel==null){ out.write("<p><span class='label'>Tag"+ (taglist.size()==1?"":"s") +":</span> "); }
    else { out.write("<p><span class='label'>" + taglabel + "</span> " ); }
    for( int i=0; i<taglist.size(); i++ ){
  		out.write( taglist.get(i) );
  		if(i<taglist.size()-1){ out.write(", "); }
	}
    out.write("</p>");
} else {%>
   <cq:include script="empty.jsp"/>
<% }


%>



<%  //this code might be useful for debugging:

    /*

pageTags = currentPage.getTags();
for(Tag s : pageTags){
    out.write("<li>Tag");
    out.write("<ul>");
    out.write("<li>.getName="+s.getName()+"</li>");
    out.write("<li>.getTitle="+s.getTitle()+"</li>");
    out.write("<li>.getTitlePath="+s.getTitlePath()+"</li>");
    out.write("<li>.getPath="+s.getPath()+"</li>");
    out.write("<li>.getTagID="+s.getTagID()+"</li>");
    out.write("<li>.getTagID encoded="+URLEncoder.encode(s.getTagID(),"UTF-8")+"</li>");
    out.write("<li>.getTagID encoded 2F fixed="+URLEncoder.encode(s.getTagID(),"UTF-8").replace("%2F","%60")+"</li>");


    out.write("<li>.find()="+s.find()+"</li>");
    out.write("<li>.getCount()="+s.getCount()+"</li>");
    //out.write("<li>.getGQLSearchExpression()="+s.getGQLSearchExpression("cq:tags")+"</li>");
    out.write("<li>News pages that are tagged with this tag: ");
    String[] ar = {s.getTagID()};
    String count = Long.toString(tagManager.find("/content/public/en/news", ar).getSize());
    out.write( count.equals("-1")?"unavailable":count);
    //out.write(" "+tagManager.find("/content/public/en/news", ar).getSize());

    out.write("</ul>");
    out.write("</li>");
}


*/
 %>