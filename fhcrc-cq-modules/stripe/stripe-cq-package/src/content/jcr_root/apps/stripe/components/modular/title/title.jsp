<%@include file="/apps/stripe/global.jsp"%><%
%><%@ page import="com.day.cq.commons.Doctype,
        com.day.cq.commons.DiffInfo,
        com.day.cq.commons.DiffService,
        org.apache.sling.api.resource.ResourceUtil,
        org.fhcrc.stripe.utilities.PageUtil" %>


	      <jsp:useBean id="pageUtilBean" scope="request" class="org.fhcrc.stripe.utilities.PageUtil" />
	      <jsp:setProperty name="pageUtilBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<%
           String title = properties.get("title", pageUtilBean.getInheritedProperty(currentPage,"labtitle","title"));
           String link = properties.get("link", pageUtilBean.getInheritedProperty(currentPage,"labtitle","link"));
         
        
          
        
    // first calculate the correct title - look for our sources if not set in paragraph
    /**String title = currentStyle.get(NameConstants.PN_TITLE, String.class);
    if (title == null || title.equals("")) {
        title = resourcePage.getPageTitle();
    }
    if (title == null || title.equals("")) {
        title = resourcePage.getTitle();
    }
    if (title == null || title.equals("")) {
        title = resourcePage.getName();
    }**/

    
    // check if we need to compute a diff
    String diffOutput = null;
    DiffInfo diffInfo = resource.adaptTo(DiffInfo.class);
    if (diffInfo != null) {
        DiffService diffService = sling.getService(DiffService.class);
        ValueMap map = ResourceUtil.getValueMap(diffInfo.getContent());
        String diffText = map.get(NameConstants.PN_TITLE, "");
        // if the paragraph has no own title, we use the current page title(!)
        if (diffText == null || diffText.equals("")) {
            diffText = title;
        } else {
            diffText = diffText;
        }
        diffOutput = diffInfo.getDiffOutput(diffService, title, diffText, false);
        if (title.equals(diffOutput)) {
            diffOutput = null;
        }
    }


    if (diffOutput == null) {
        %><a href="<%=link%>.html" style="text-decoration: none"><h1><%= title %></h1></a><%
    // we need to display the diff output
    } else {
        // don't escape diff output
        %><p><%= diffOutput %></p><%
    }
%>