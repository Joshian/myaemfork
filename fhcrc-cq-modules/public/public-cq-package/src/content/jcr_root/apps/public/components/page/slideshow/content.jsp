<%@include file="/libs/foundation/global.jsp"%>

<jsp:useBean id="slideshowBean" scope="request" class="org.fhcrc.publicsite.pageComponents.Slideshow" />
<jsp:setProperty name="slideshowBean" property="currentPage" value="<%= currentPage %>"/>
<jsp:setProperty name="slideshowBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="slideshowBean" property="resource" value="<%= resource %>"/>
<jsp:setProperty name="slideshowBean" property="currentStyle" value="<%= currentStyle %>"/>
<jsp:setProperty name="slideshowBean" property="currentDesign" value="<%= currentDesign %>"/>
<jsp:setProperty name="slideshowBean" property="resourceDesign" value="<%= resourceDesign %>"/>
<jsp:setProperty name="slideshowBean" property="request" value="<%= request %>"/>
<jsp:setProperty name="slideshowBean" property="writer" value="<%= out %>"/>
<jsp:setProperty name="slideshowBean" property="componentProperties" value="<%= properties %>"/>

 <!-- CONTENT AREA -->

	<div class="content_container outer_grid_container">

<%

if (slideshowBean.getNumberOfSlides() == 0) {

%>

		<cq:include script="empty.jsp" />

<%

} else {

%>

 	<!-- PRIMARY CONTENT AREA -->
  
	  	<div class="primary_content_container inner_grid_container">
    
    		<div class="slide_photos_container">
    		
    			<div class="slide_control" id="left_slide_control">
        
        			<div class="control_image" id="left_control_image">Move slideshow back</div>
        
       			</div>
    		
    		<%
    		
    		for (int i = 0; i < slideshowBean.getNumberOfSlides(); i++) { 
    				
    		%>		
				
				<div class="slide_photo<%= i == 0 ? " active" : "" %>">
						
					<% slideshowBean.drawSlideImage(i); %>

				</div>

			<%
    		
    		} /* End for loop */
    		
    		%>
    		
    			<div class="slide_control" id="right_slide_control">
        
        			<div class="control_image" id="right_control_image">Move slideshow forward</div>
        
        		</div>
        		
        		<div id="final_slide">
        
        			<div class="final_slide_button_container">
        
            			<img class="final_slide_button" id="replay_slideshow_button" src="<%= currentDesign.getPath() %>/img/slideshow/replay_button_static.png" alt="Replay slideshow" title="Replay slideshow" data-hover="<%= currentDesign.getPath() %>/img/slideshow/replay_button_hover.png">
			        <%--
			        	Only show the next slideshow button if someone has entered a value into the next slideshow dialog widget.
			         --%>    
			        <c:if test="${ slideshowBean.nextContent }">
			            
			            <a href="<%= slideshowBean.getNextContentLink() %>" title="<%= slideshowBean.getNextContentTitle() %>">
			            	<img class="final_slide_button" id="next_slideshow_button" src="<%= currentDesign.getPath() %>/img/slideshow/next_slideshow_button_static.png" alt="Next slideshow" data-hover="<%= currentDesign.getPath() %>/img/slideshow/next_slideshow_button_hover.png">
			            </a>
			            
			        </c:if>
			            
			            <a href="http://www.facebook.com/sharer.php?p[url]=<%= slideshowBean.getFacebookLink() %>&p[title]=<%= slideshowBean.getFacebookTitle() %>" target="_blank" title="Share on Facebook">
			            	<img class="final_slide_button" id="share_slideshow_button" src="<%= currentDesign.getPath() %>/img/slideshow/share_button_static.png" alt="Share this slideshow" data-hover="<%= currentDesign.getPath() %>/img/slideshow/share_button_hover.png">
			            </a>
			            <a href="https://secure2.convio.net/fhcrc/site/Donation2?df_id=3440&3440.donation=form1" target="_blank" title="Donate to Fred Hutch">
			            	<img class="final_slide_button" id="donate_slideshow_button" src="<%= currentDesign.getPath() %>/img/slideshow/donate_button_static.png" alt="Donate to Fred Hutch" data-hover="<%= currentDesign.getPath() %>/img/slideshow/donate_button_hover.png">
			            </a> 
            
	        		</div>
	          
	        		<div class="final_slide_shader"></div>         
        
        		</div>

			</div>
		
		</div>
		
		<!-- SIDEBAR / SECONDARY CONTENT AREA -->
	  
	  	<div class="secondary_content_container inner_grid_container">
	    
	    <c:if test="${ slideshowBean.thumbnails }">
	    
	    <!-- THUMBNAIL NAVIGATION -->
	    	
			<div id="slideshow_thumbnail_navigation_container" class="thumbnail_navigation_container">
	      
		      	<div class="thumbnail_navigation_control">
		        
		        	<img id="thumbnail_left_arrow" src="<%= currentDesign.getPath() %>/img/slideshow/thumbnail_left_arrow_blk.png">
		        
		        </div>
		        
		        <div class="thumbnail_viewport">
		        
		        	<div class="thumbnail_container">
		          
		          		<div class="thumbnail_image thumbnail_placeholder">
			            	<img class="thumbnail" src="<%= currentDesign.getPath() %>/img/slideshow/empty_thumbnail.jpg">
			            	<div class="thumbnail_filter"></div>
		            	</div>
		          		
	          		<% 
	          		for (int i = 0; i < slideshowBean.getNumberOfSlides(); i++) { 
	          		%>

			            <div class="thumbnail_image<%= i == 0 ? " active_thumbnail" : ""%>">
			            	<%= slideshowBean.getThumbnail(i) %>
			            	<div class="thumbnail_filter"></div>
			            </div>
			            
			        <%
	          		} // END THUMBNAIL FOR LOOP
	          		%>
			            
			            <div class="thumbnail_image thumbnail_placeholder">
			            	<img class="thumbnail" src="<%= currentDesign.getPath() %>/img/slideshow/empty_thumbnail.jpg">
			            	<div class="thumbnail_filter"></div>
		            	</div>
		          
		        	</div>
		          
		        </div>
		        
		        <div class="thumbnail_navigation_control">
		        
		        	<img id="thumbnail_right_arrow" src="<%= currentDesign.getPath() %>/img/slideshow/thumbnail_right_arrow_blk.png">
		        
		        </div>
	      
			</div>
	    	 
		<!-- END THUMBNAIL NAVIGATION -->
		
		</c:if>
	    
			<div class="slide_information_container">
			
			<%
			
			for (int i = 0; i < slideshowBean.getNumberOfSlides(); i++) {
			
			%>
	      
		      	<div class="slide_information<%= i == 0 ? " active" : "" %>">
		        
		        <!-- HEADLINE -->
		        
		        	<div class="headline_container">
		        
		            	<h2><%= slideshowBean.getSlideHeadline(i) %></h2>
		            
		        	</div>
		          
		        <!-- END HEADLINE -->
		          
		        <!-- CAPTION -->
		        
		        	<div class="caption_container">
		          
		            	<%= slideshowBean.getSlideCaption(i) %>
		            
		        	</div>
		          
		        <!-- END CAPTION -->
		          
		        <!-- PHOTO CREDIT -->
		        
		        	<div class="photo_credit_container">
		          
		            	<span class="photo_credit"><%= slideshowBean.getSlidePhotoCredit(i) %></span>
		          
		        	</div>
		          
		        <!-- END PHOTO CREDIT -->
		        
		        <%
		        
		        if (slideshowBean.hasSlideRelatedContent(i)) {
		        
		        %>
		        <!-- RELATED CONTENT -->
		        	
		        	<div class="related_content_container">
		          
		            	<h3 class="related_content_header">Related</h3>
		            
		            	<% slideshowBean.drawSlideRelatedContent(i); %>
		          
		        	</div>
		        	
		          
		        <!-- END RELATED CONTENT -->
		        
		        <%
		        
		        } /* End if statement */
		        
		        %>
	        
	        	</div>
	        	
	        <%
	        
	        } /* End of for loop */
	        
	        %>
	      
	    	</div>
	    
	    </div>
	
	<!-- END SECONDARY CONTENT -->
	  
	<div class="clear"></div>
	
<%

}

%>

	</div>
	
	<cq:includeClientLib js="public.components.slideshowPage" />