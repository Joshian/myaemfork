<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text, 
                 java.util.Iterator, 
                 org.apache.commons.lang.StringEscapeUtils, 
                 java.util.HashMap,
                 com.day.cq.search.QueryBuilder, 
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.result.Hit,
                 com.day.cq.tagging.Tag,
                 com.day.cq.tagging.TagManager,
                 java.util.List,
                 java.util.ArrayList,
                 java.util.TreeSet,
                 java.util.Comparator,
                 java.util.AbstractCollection" %>
<%
Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
QueryBuilder builder = resource.getResourceResolver().adaptTo(QueryBuilder.class);
TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
/* searchTerm is the name of the variable that should be passed to the component via the URL */
String queryString = slingRequest.getParameter("searchTerm");
String[] tags = properties.get("searchTags", new String[0]);
String[] excludeTagStrings = properties.get("excludeTags", new String[0]);
Tag[] excludeTags = new Tag[excludeTagStrings.length];
AbstractCollection<Page> pageList;
if(excludeTags.length > 0) {
	for(int i=0; i<excludeTags.length; i++){
		excludeTags[i] = tagManager.resolve(excludeTagStrings[i]);
	}
}
/* Begin building the predicateGroup for the search */ 
HashMap<String, String> map = new HashMap<String, String>();
/* 1_group contains the different divisions that can be selected via checkbox in the component dialog */
map.put("1_group.p.or","true");
if(properties.get("clinical","false").equals("true")){
    map.put("1_group.1_tagid","divisions:clinical");
    map.put("1_group.1_tagid.property","cq:tags");
}
if(properties.get("phs","false").equals("true")){
    map.put("1_group.2_tagid","divisions:phs");
    map.put("1_group.2_tagid.property","cq:tags");
}
if(properties.get("humanbio","false").equals("true")){
    map.put("1_group.3_tagid","divisions:human-bio");
    map.put("1_group.3_tagid.property","cq:tags");
}
if(properties.get("basic","false").equals("true")){
    map.put("1_group.4_tagid","divisions:basic-sciences");
    map.put("1_group.4_tagid.property","cq:tags");
}
if(properties.get("vidd","false").equals("true")){
    map.put("1_group.5_tagid","divisions:vidd");
    map.put("1_group.5_tagid.property","cq:tags");
}
/* If we have any search tags, add them to the search IN THEIR OWN GROUP, 2_group */
if(tags.length > 0){
    if("true".equals(properties.get("tagsAnd","false"))){
        map.put("2_group.p.and","true");
    } else {
        map.put("2_group.p.or","true");
    }
	for(int i=1; i<=tags.length; i++) {
		map.put("2_group."+Integer.toString(i)+"_tagid",tags[i-1]);
		map.put("2_group."+Integer.toString(i)+"_tagid.property","cq:tags");
	}
}
/* We only care about pages that are Project Page templates located in the labs directory */
map.put("path","/content/public/en/labs");
//map.put("type","cq:Page"); <-- removed to fix @jcr:score problem
map.put("property","cq:template");
map.put("property.value","/apps/public/templates/project");
map.put("p.limit","500"); //Temp fix to display all results. Change when/if pagination is implemented.
map.put("p.guessTotal","true"); // Added to fix Oak 1.0.18 problem

/* If there is no query string or the query string is blank, then just list everything by title */
if(queryString==null || queryString.trim().equals("")){
    map.put("orderby","@jcr:title");
    map.put("orderby.index","true");
} else {
/* If there is a query string, then do a fulltext search on that string and order results by score */	
/* First, we need to add wildcard characters to the end of each word in the search to enable partial string matches */
    queryString = queryString.trim(); 
    /* Get rid of leading spaces, as they generate searches on just the wildcard character which is useless */
    String[] qArray = queryString.split("\\s+");
    String qString = "";
    for(int i = 0; i < qArray.length; i++){
        qArray[i] += "*";
        qString += qArray[i];
        if(i+1 < qArray.length) {
/* JCR searches interpret whitespace as implicit AND, so change that to explicit ORs */
            qString += " OR ";
        }
    }
/* We want to search both the content of the page and the tags */
    map.put("3_group.p.or","true");
    map.put("3_group.1_fulltext",qString);
//    map.put("3_group.1_fulltext.relPath", "jcr:content");
    map.put("3_group.2_fulltext",qString);
    map.put("3_group.2_fulltext.relPath", "@cq:tags");
    map.put("orderby","@jcr:score");
    map.put("orderby.sort","desc");
}

List<Hit> hitList = builder.createQuery(PredicateGroup.create(map), session).getResult().getHits();
if(queryString==null || queryString.trim().equals("")){
	pageList = new TreeSet<Page>(new Comparator<Page>(){
	    public int compare(Page a, Page b){
	        String titleA = a.getTitle();
	        String titleB = b.getTitle();
	        if(titleA.equals(titleB)){
	            return a.getPath().compareTo(b.getPath());
	        } else {
	            return titleA.compareToIgnoreCase(titleB);
	        }
	    }
	});
} else {
    pageList = new ArrayList<Page>();
}
if(properties.get("searchBox","false").equals("true")){
    %>
        <cq:include resourceType="/apps/public/components/content/searchWidget" path="includedSearchBox" />
    <%
}
if(hitList.size() == 0){
%>  
    <cq:include script="empty.jsp"/>
<%  
} else {
    for(int i = 0; i < hitList.size(); i++){
      Page testPage = hitList.get(i).getResource().getParent().adaptTo(Page.class); //We need the getParent() since we are now returning jcr:content nodes as hits
      Boolean skip = false;
      if(excludeTags.length > 0){ //Check to see if the page we are looking at should not be added to the list
          Tag[] pageTags = testPage.getTags();
          for(Tag s : pageTags){
        	  if(s != null){ //Double-check that this tag exists (possible discrepancy from Author to Publish, for instance)
              for(Tag t : excludeTags){
            	 if(t != null){ //Another double-check that this tag exists before attempting to match on it
                 if(t.equals(s)){
                    skip = true;
                    break; // No need to continue the loop if we get a match
                 }
            	 }
              }
              if(skip == true){
                  break; // No need to continue the loop if we get a match
              }
        	  }
          }
       }
      if(skip == false){
          pageList.add(testPage); 
      }
    }

    Iterator<Page> pageIterator = pageList.iterator();

    if(!pageIterator.hasNext()){
%>  
    <cq:include script="empty.jsp"/>
<%      
    } else {
%>
<ul class="SquareBullet">
<%
while(pageIterator.hasNext()){
	Page nextPage = pageIterator.next();
	String pageDescription = nextPage.getDescription();
	Tag[] pageTags = nextPage.getTags();
	int tagCount = 0;
	out.print("<li><a href=\"" + nextPage.getPath() + ".html\">");
	if(nextPage.getTitle() == null){
        out.print(nextPage.getName());
    } else {
        out.print(nextPage.getTitle());
    }
	out.print("</a>");
	if(properties.get("omitDivision","false").equals("false")){
/* Run through the tags and print out the tag titles of any division tags on the page */
    	for(Tag t : pageTags){
    	    if(t.getNamespace().getName().equals("divisions")){
    	    	if(tagCount == 0){
    	    		out.print("<br /><em>" + t.getTitle());
    	    	} else {
    	    	    out.print(", " + t.getTitle());
    	    	}
    	    	tagCount++;
    	    }
	    }
	
/* Close the <em> tag if any tags were printed */
	    if(tagCount != 0){ 
	    	out.print("</em>");
	    }
	}
	if(properties.get("omitDescription","false").equals("false")){
/* Print the description if there is one */
        if(pageDescription != null && !pageDescription.trim().equals("")){ 
            out.print("<br />" + StringEscapeUtils.unescapeXml(pageDescription));
        }
	}
    out.print("</li>");
}
%>
</ul>
<%
}
}
%>