var sttrApp = (function(sttrApp, $) {
	
	/* "Private" variables */
	
	sttrApp.addEvent = function(elem, evtType, func) {
	    if (elem.addEventListener) {
	      elem.addEventListener(evtType, func, false);
	    } else if (elem.attachEvent) {
	      elem.attachEvent("on" + evtType, func);
	    } else {
	      elem["on" + evtType] = func;
	    }
    };
	
	sttrApp.adjustBodyPosition = function() {
		
		var headerId = "header_element",
			bodyId = "body_element";
	    /* adjust the position of the body element so it appears
	     perfectly under the header. */
	    var o = document.getElementById(headerId); //alert(o.offsetHeight);
	    var y;
	    if (o) {
	      y = o.offsetHeight;
	    }
	    if (y) {
	      var o = document.getElementById(bodyId);
	      if (o) {
					var s = 0; 
					/* Check to see if the subNav is active */
					/*if($("#globalNav>ul>li.active").size() > 0) {
						s += $("li.active ul.subNav").first().outerHeight();
					}*/
	        
	                o.style.marginTop = (y + s) + "px";
	              }
	           }
		
	}
	
	/* set up click events for the mobile menu button */
	sttrApp.setupClickEventsMobileMenu = function(){
	    var o_menu = document.getElementById('global_menu');
	    var o_button = document.getElementById('mobile_menu_button');
	    var o_texture = document.getElementById('mobile_texture');
	    if( o_menu && o_button ){
		  sttrApp.addEvent(o_button,"click",function(){//alert(o_menu.style.display);
		      if(o_menu.style.display=='none'||!o_menu.style.display) {
		    	  o_menu.style.display='block';
		    	  if(o_texture) { o_texture.style.height="16px"; } /* arbitrary. shrinks the texture height a bit. */
		      }
		      else {
		    	  o_menu.style.display='none';
		    	  if(o_texture) { o_texture.style.height=""; } /* returns the texture height to normal */
		      }
		      sttrApp.adjustBodyPosition();
	      });
	    }			
	}
	
	sttrApp.resetMobileMenus = function(){
	   /* remove any js-placed styling (element.style) from mobile menus, so they can operate fresh. used for page resizing. */
	   var o = document.getElementById('global_menu');
	   if(o) o.style.display='';  
	}	

	
    sttrApp.equallySpaceGlobalNav = function(){
	    /* make each global nav element (desktop form factors) the same width */
	    if($){
	      if( $("div.mobile_nav").css('display')!='block' ){ /* when running under full size (no mobile menu)... */
	        var v =  100 / $("div.global_nav div.global_menu_contents > ul.core > li").size();    
	        $("div.global_nav div.global_menu_contents > ul.core > li").css('width',v+'%'); /* ... make them equally spaced .... */
	      } else {
	        $("div.global_nav div.global_menu_contents > ul.core > li").css('width',''); /* ...otherwise remove this setting in case they are browser resizing */
	      }
	    }
	};	
    
    sttrApp.setupClickEventsGlobalNav = function(){
    	/* click events on the Global Nav for mobile platforms */
    	if($) {
    		$("#global_menu ul.core>li").click(function(){
    			$("#global_menu ul.core>li").removeClass("active");
    			$(this).addClass("active");
    		}).mouseleave(function() {
    			$(this).removeClass("active");
    		});
    	}
    	/* click adds the active class to the current global nav (this) after removing the active class from all other global nav items */
    	
    };
	
	sttrApp.setupClickEventsUtilitySitesearch = function(){
	    /* set up click events for the utility nav search button */
	    var o_search = document.getElementById('utility_search');
	    var o_button = document.getElementById('utility_nav_search_icon');
	    var o_form   = document.getElementById('utility_search_form');
	    if( o_search && o_button && o_form ){
	  	  sttrApp.addEvent(o_button,"click",function(e){
	        if(o_search.style.display=='block') {
	          if(o_form.q && o_form.q.value){
	            o_form.submit();
	          }
	          o_search.style.display='none';
	        } else o_search.style.display='block';
	        sttrApp.adjustBodyPosition();
	        var evt = e ? e:window.event;
	        if (evt.preventDefault) evt.preventDefault();
	        evt.returnValue = false;
	        return false;
	      });
	    }    		
	}	

	return sttrApp;
	
})(sttrApp || {}, jQuery);