<%@include file="/libs/foundation/global.jsp"%><%
String searchPath = currentPage.getAbsoluteParent(2).getPath() + "/util/search.html";
log.debug("searchPath = " + searchPath);
%>
<form id="utility_search" class="utility_search" action="<%= searchPath %>"><input type="text" name="q" placeholder="Search for faculty, disease research and news..." class="search_term"></input><input type="image" class="search_button" src="<%= currentDesign.getPath() %>/img/buttons/fred_hutch_search_icon.png" alt="GO"></input></form>
