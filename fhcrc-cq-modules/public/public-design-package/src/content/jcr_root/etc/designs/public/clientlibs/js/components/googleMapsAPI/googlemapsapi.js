var componentApp = (function(componentApp) {
	
	componentApp.googleMapsAPI = (function(googleMapsAPI) {
		
		googleMapsAPI.initializeFHGooglemap = function() {
			
			// two fields come in via global variables:
		    //   googleMapRes = the path of the "resources" folder for this component (for images, etc.)
		    //   googleMapID  = the DOM ID of the element to be used to draw the map.
		  
		    // the location of this component's images
	        //var imgPath = '/apps/public/components/content/googlemapsapi/img/';
		    var imgPath = googleMapRes + "img/";
		  
	  	    //map latLng
	        var latlng = new google.maps.LatLng(47.6277, -122.33065);
	      	
	      	//marker latLng
	      	var markerLatLng = new google.maps.LatLng(47.62752, -122.33089);
	      	
	      //main map image coordinates
	      	var imageBounds1 = new google.maps.LatLngBounds(
	      		new google.maps.LatLng(47.62530,-122.33265), //southeast corner?  increase 2nd # to go left, decrease 2nd # to go right (1st number moves bottom, 2nd number moves left
	      		new google.maps.LatLng(47.62990,-122.32775)); //northwest corner?  increase 2nd # to go left, decrease 2nd # to go right (1st number moves top, 2nd number moves right
	      	
	      	//1616 building map image coordinates
	      	var imageBounds2 = new google.maps.LatLngBounds(
	      		new google.maps.LatLng(47.63393,-122.32520), //southeast corner?  increase 2nd # to go left, decrease 2nd # to go right (1st number moves bottom, 2nd number moves left
	      		new google.maps.LatLng(47.63483,-122.32425)); //northwest corner?  increase 2nd # to go left, decrease 2nd # to go right (1st number moves top, 2nd number moves right
	      		
	      	//pete gross house/hutch school building map image coordinates
	      	var imageBounds3 = new google.maps.LatLngBounds(
	      		new google.maps.LatLng(47.62352,-122.33365), //southeast corner?  increase 2nd # to go left, decrease 2nd # to go right (1st number moves bottom, 2nd number moves left
	      		new google.maps.LatLng(47.62394,-122.33305)); //northwest corner?  increase 2nd # to go left, decrease 2nd # to go right (1st number moves top, 2nd number moves right	

	        //SCCA House building map image coordinates
	      	var imageBounds4 = new google.maps.LatLngBounds(
	      		new google.maps.LatLng(47.61973,-122.33177), //northwest corner?  increase 2nd # to go left, decrease 2nd # to go right (1st number moves top, 2nd number moves right
	            new google.maps.LatLng(47.62008,-122.33234));  //southeast corner?  increase 2nd # to go left, decrease 2nd # to go right (1st number moves bottom, 2nd number moves left)
	      
	        var myOptions = {
	          zoom: 17, //16 
	          center: latlng,
	          mapTypeId: google.maps.MapTypeId.ROADMAP
	        };

	        var map = new google.maps.Map(document.getElementById(googleMapID), myOptions); // references the element id here
	      	
	      	//set variable to turn off transit icons
	      	var transitStyle = [ { featureType: "transit.station", elementType: "all", stylers: [ { visibility: "on" }, { hue: "#FF0000" } ] } ];
	      	
	      	//turns off transit icons
	      	map.setOptions({styles: transitStyle});
	      	
	      //main map image
	      	 var fhcrcoverlay1 = new google.maps.GroundOverlay(
	      	  imgPath + "map_mainCampus-2015.png",
	      	  imageBounds1);
	          //alert(imgPath + "map_mainCampus.png");
	      	 
	      	 //1616 building image
	      	 var fhcrcoverlay2 = new google.maps.GroundOverlay(
	      	  imgPath + "01_1616.png",
	      	  imageBounds2); 
	      	 
	      	 //Pete Gross/Hutch School image
	      	 var fhcrcoverlay3 = new google.maps.GroundOverlay(
	      	  imgPath + "02_HutchSchool_PeteGross.png",
	      	  imageBounds3); 
	      
	         //SCCA House image
	      	 var fhcrcoverlay4 = new google.maps.GroundOverlay(
	      	  imgPath + "03_SCCA_House.png",
	      	  imageBounds4); 
	      	 
	      	 //--------------- marker and info window stuff
	      	 var contentString = '<div class="googlemaplocator">'+
	          '<div id="siteNotice">'+
	          '</div>'+
	          '<h1 class="firstHeading"><span class="nobreak">Fred Hutchinson</span> Cancer Research Center</h1>'+
	          '<div">'+
	          '<p><img src="'+imgPath+'FHCRC_vesselSculpture.png" width="105" height="150" alt="Vessel Sculpture" align="left" hspace="10"></p>'+
	          '<p><span class="nobreak">1100 Fairview Avenue North</span><br/>' +
	          'Seattle, WA 98109-4433<br/>'+
	          '(206) 667-5000</p>'+
	      	'<p>&nbsp;</p>'+
	      	'<p class="directions"><a href="https://maps.google.com/maps/ms?msid=212457292778404937524.0004aea5cd161df69022f&msa=0&ll=47.627136,-122.330967&spn=0.002983,0.003846" target="_blank">Directions</a></p>'+
	      	'<p>&nbsp;</p>'+
	          '<p><img src="'+imgPath+'Logo_BLUE_BLACK.png" width="195" height="49" alt="FHCRC Logo"></p>'+
	          '</div>'+
	          '</div>';
	      
	      var infowindow = new google.maps.InfoWindow({
	          content: contentString
	      });
	      
	      var marker = new google.maps.Marker({
	          position: markerLatLng,
	          map: map,
	          title:"Fred Hutchinson Cancer Research Center",
	      	icon:imgPath+"fhcrc_marker.png"
	      });
	      
	      google.maps.event.addListener(marker, 'click', function() {
	        infowindow.open(map,marker);
	      });
	      	 
	      	 //-------------
	      	 
	      	 // To add the overlay images to the map, call setMap();
	        fhcrcoverlay1.setMap(map);  
	        fhcrcoverlay2.setMap(map); 
	        fhcrcoverlay3.setMap(map);
	        fhcrcoverlay4.setMap(map);
			
		};
		
		googleMapsAPI.loadGoogleMapScript = function() {
			
			var script = document.createElement("script");
		    script.type = "text/javascript";
		    script.src = "https://maps.googleapis.com/maps/api/js?sensor=false&callback=componentApp.googleMapsAPI.initializeFHGooglemap";
		    document.body.appendChild(script);
			
		};
		
		addEvent(window, 'load', googleMapsAPI.loadGoogleMapScript);
		
		return googleMapsAPI;
		
	})(componentApp.googleMapsAPI || {});
	
	return componentApp;
	
})(componentApp || {});