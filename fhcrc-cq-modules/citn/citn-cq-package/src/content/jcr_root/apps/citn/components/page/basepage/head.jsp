<%@include file="/libs/foundation/global.jsp" %><%
%><%@ page import="com.day.cq.commons.Doctype,
                   org.apache.commons.lang.StringEscapeUtils" %><%
    String xs = Doctype.isXHTML(request) ? "/" : "";
    String favIcon = currentDesign.getPath() + "/favicon.ico";
    if (resourceResolver.getResource(favIcon) == null) {
        favIcon = null;
    }
%><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"<%=xs%>>
    <meta http-equiv="keywords" content="<%= StringEscapeUtils.escapeHtml(WCMUtils.getKeywords(currentPage, false)) %>"<%=xs%>>
    <meta http-equiv="description" content="<%= StringEscapeUtils.escapeHtml(properties.get("jcr:description", "")) %>"<%=xs%>>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <cq:include script="headlibs.jsp"/>
    <cq:include script="/libs/wcm/core/components/init/init.jsp"/>
    <cq:includeClientLib js="apps.shared.jquery-1.10" />
    <% if (favIcon != null) { %>
    <link rel="icon" type="image/vnd.microsoft.icon" href="<%= favIcon %>"<%=xs%>>
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<%= favIcon %>"<%=xs%>>
    <% } %>
    <script type="text/javascript">
    <!--

      if(location.hostname=='citninfo.org' || location.hostname=='www.citninfo.org'){   

    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', 'UA-528883-16']);
    	  _gaq.push(['_trackPageview']);

    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();

      } 
    //-->
    </script>     
    <title><%= currentPage.getTitle() == null ? StringEscapeUtils.escapeHtml(currentPage.getName()) : StringEscapeUtils.escapeHtml(currentPage.getTitle()) %></title>
    

</head>
