<%@include file="/libs/foundation/global.jsp" %>
  <!-- logo area, top-most element -->
  <div class="logobar">
    <a href="<%= currentPage.getAbsoluteParent(2).getPath() %>.html"><img src="<%= currentDesign.getPath() %>/img/fhcrc_logo.png" alt="Fred Hutchinson Cancer Research Center" /></a>
  </div>
  
  <!-- site-wide nav element. three main buttons, campus, search and menu -->
  <div class="sitewidenav" title="Site-wide navigation - helps get you around the mobile website. ">
    <ul>
      <li class="campus"><span><a href="#campus_menu_items" onclick="return false;" title="Campus menu"><img src="<%= currentDesign.getPath() %>/img/icon_campus.png" alt="" />Campus</a></span></li>
      <li class="search"><span><a href="#search_menu_items" onclick="return false;" ><img src="<%= currentDesign.getPath() %>/img/icon_search.png" alt="" />Search</a></span></li>
      <li class="menu"><span><a href="#menu_menu_items" onclick="return false;" ><img src="<%= currentDesign.getPath() %>/img/icon_menu.png" alt="" />Menu</a></span></li>
    </ul>
    <div class="clear"></div>
  </div>

  <!-- insert the menu systems here, below the top chrome, above the content -->
  <div id="menu_anchor"></div>  
  