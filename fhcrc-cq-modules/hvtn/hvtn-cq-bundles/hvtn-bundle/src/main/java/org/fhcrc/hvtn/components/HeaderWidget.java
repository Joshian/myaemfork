package org.fhcrc.hvtn.components;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.fhcrc.tools.FHUtilityFunctions;


public class HeaderWidget {
	
	private final String HEADER_CLASS = "";
	private final String UNDERLINE_CLASS = "isUnderlined";
	private final String NO_PADDING_CLASS = "omitPadding";
	
	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	
	private String headerSize;
	private String linkTarget;
	private String colorTheme;
	private String designTheme;
	private String headerClass;
	
	private Boolean externalLink;
	private Boolean omitUnderline;
	private Boolean omitPadding;
	
	public HeaderWidget() {
		
	}
	
	public void setComponentProperties(ValueMap properties) {
		
		this.componentProperties = properties;
		this.headerSize = properties.get("headerSize","h2");
		setLinkTarget(properties.get("linkTarget",""));
		setColorTheme(properties.get("colortheme","0"));
		setDesignTheme(properties.get("designtheme","0"));
		this.externalLink = properties.get("externalLink",false);
		this.omitUnderline = properties.get("omitUnderline",false);
		this.omitPadding = properties.get("omitPadding",false);
		setHeaderClass(HEADER_CLASS);
		
	}
	
	public ValueMap getComponentProperties() {
		return componentProperties;
	}
	
	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}
	
	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}
	
	public void setHeaderSize(String headerSize) {
		this.headerSize = headerSize;
	}
	
	public String getHeaderSize() {
		return headerSize;
	}
	
	public void setLinkTarget(String linkTarget) {
		if(!linkTarget.equals("")){
	        this.linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
	    } else {
	    	this.linkTarget = "";
	    }
	}
	
	public String getLinkTarget() {
		return linkTarget;
	}
	
	public void setColorTheme(String colorString) {
		 String[] c = colorString.split("\t");
		 String color_theme_number = c[0];
		 this.colorTheme = "";
		 if(!color_theme_number.isEmpty()){
		     this.colorTheme = "colorTheme" + color_theme_number ;
		 }
	}
	
	public String getColorTheme() {
		return colorTheme;
	}
	
	public void setDesignTheme(String designString) {
		String[] c = designString.split("\t");
		String design_theme_number = c[0];
		this.designTheme = "";
		if(!design_theme_number.isEmpty()){
			this.designTheme = "designTheme" + design_theme_number ;
		}
	}
	
	public String getDesignTheme() {
		return designTheme;
	}

	public String getHeaderClass() {
		return headerClass;
	}

	public void setHeaderClass(String headerClass) {
		this.headerClass = headerClass.concat(" ").concat(colorTheme).concat(" ").concat(designTheme);
		if (!omitUnderline) {
			this.headerClass = this.headerClass.concat(" ").concat(UNDERLINE_CLASS);
		}
		if (omitPadding) {
			this.headerClass = this.headerClass.concat(" ").concat(NO_PADDING_CLASS);
		}
		
	}

	public Boolean getExternalLink() {
		return externalLink;
	}

	public void setExternalLink(Boolean externalLink) {
		this.externalLink = externalLink;
	}

	public Boolean getOmitUnderline() {
		return omitUnderline;
	}

	public void setOmitUnderline(Boolean omitUnderline) {
		this.omitUnderline = omitUnderline;
	}

	public Boolean getOmitPadding() {
		return omitPadding;
	}

	public void setOmitPadding(Boolean omitPadding) {
		this.omitPadding = omitPadding;
	}
	
}