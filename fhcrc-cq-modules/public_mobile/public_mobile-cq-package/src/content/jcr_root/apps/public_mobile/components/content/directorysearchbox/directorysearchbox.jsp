<%@include file="/libs/foundation/global.jsp"%><%
String dirResultsPath = currentPage.getAbsoluteParent(2).getPath() + "/util/directory.html";
//log.info("dirResultsPath = " + dirResultsPath);

// determine presets
// company may be sent: company=fhcrc|scca
String company= request.getParameter("company");
if(null==company){ company=""; }
String fh_company_preset = (company.equals("fhcrc")) ? "checked='checked'" : "";
String scca_company_preset = (company.equals("scca")) ? "checked='checked'" : "";
String both_company_preset = (!company.equals("fhcrc")) && (!company.equals("scca")) ? "checked='checked'" : "";

// query might be sent (anything)
String type= request.getParameter("type"); log.info("mytype = " + type); if(null==type){ type=""; }
String q= request.getParameter("q");  if(null==q){ q=""; }
String query_preset = !(type.equals("peoplebydeptid") || type.equals("relateddeptsid")) ? q : "";

%>

<div class="directorysearchboxcontainer">

  <!-- here is the general search -->
  <form method="get" name="mainqsearch" action="<%= dirResultsPath %>">
	  <input type="hidden" name="short" value="true">	
	  Search by name, email, department or phone number:<br/>
	  <div class="searchfield"><input type="text" name="q" class="q" maxlength="124" value="<%=query_preset%>"/><input type="submit" class="submitButton" value="Go"></div>
	  <input type="hidden" name="company" value=""/>
	  <!--<p class="startover"><a href="<%=dirResultsPath%>">New search</a></p>-->
  </form>

  <!-- added focus 2/09 -->
  <script type='text/javascript' language='javascript'><!--
    if(document['mainqsearch']&&document['mainqsearch'].q){
      document['mainqsearch'].q.focus();
    }
    //-->
  </script> 
  <!--  added onblur 12/12 -->
  <script type='text/javascript' language='javascript'><!-- 
    /*if(document.querySelector){
      addEvent(document.querySelector("div.directorysearchboxcontainer form input[name='q']"),"blur",function(){
         if(this.value){this.form.submit();};
      });
    }*/
    //-->
  </script>
  
</div>  