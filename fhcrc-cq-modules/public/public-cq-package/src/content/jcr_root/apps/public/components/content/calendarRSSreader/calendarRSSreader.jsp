<%@include file="/libs/foundation/global.jsp"%><%@ page import="com.day.cq.wcm.api.WCMMode" %>
<%
String eventType = properties.get("eventType","All");
String maxNum = properties.get("maxnum","0");

%>
<cq:includeClientLib categories="public.components.calendarRSSreader"/>
<div class="calendarContainer">
    <div class="rssTarget"></div>
</div>
<script type="text/javascript">
    $(function(){
        $('div.calendarContainer').each(function(){ componentApp.calendarRSSReader.loadRSS($(this), "<%= eventType %>", <%= maxNum %>); });
    });
</script>