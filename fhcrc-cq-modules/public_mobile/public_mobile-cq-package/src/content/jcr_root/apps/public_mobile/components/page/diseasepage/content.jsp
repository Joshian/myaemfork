<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.text.Text, java.util.Iterator, com.day.cq.wcm.api.WCMMode, org.fhcrc.publicsite.constants.Constants" %>
   <div class="fh_content"><a name="jump_content"></a>
    <!-- content begins here: -->

    <cq:include path="title" resourceType="foundation/components/title"/>
    <cq:include path="par" resourceType="foundation/components/parsys"/>
    <cq:include path="parRelated" resourceType="foundation/components/parsys"/>
    <cq:include path="socialshare_template" resourceType="public_mobile/components/content/socialshare"/>    
    <!-- content ends here -->
    <!-- remote-wrap-api-marker -->
    
   </div><!-- /fh_content -->
     