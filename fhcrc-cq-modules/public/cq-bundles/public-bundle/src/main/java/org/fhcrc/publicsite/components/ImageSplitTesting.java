package org.fhcrc.publicsite.components;

import javax.servlet.ServletRequest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.components.DropTarget;
import com.day.cq.commons.Doctype;
import com.day.cq.wcm.foundation.Image;
import com.day.cq.wcm.api.designer.*;
import com.adobe.granite.xss.XSSAPI;

import org.apache.sling.api.resource.*;
import org.fhcrc.tools.FHUtilityFunctions;

public class ImageSplitTesting {
	
	private final String B_IMAGE_ATTRIBUTE = "data-img-b";
	private final String CAMPAIGN_LABEL_ATTRIBUTE = "data-campaign-label";
	private final String A_IMAGE_DESCRIPTION_ATTRIBUTE = "data-image-a-label";
	private final String B_IMAGE_DESCRIPTION_ATTRIBUTE = "data-image-b-label";
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	private Resource resource;
	private Style currentStyle;
	private Design currentDesign;
	private Design resourceDesign;
	private ServletRequest request;
	private Writer writer;	
	private XSSAPI xss;
	
	private Image image;	
	private Image imageB;
	
	private String imageB_SRC;
	private String linkTargetA;
  	private String linkTargetB;
  	private String campaignLabel;
  	private String imageDescriptionA;
  	private String imageDescriptionB;
  	private boolean externalLink;
/*	
	private Boolean hasTwoImages;
*/	
	public ImageSplitTesting() {
		
	}

	/**
	 * @return the componentProperties
	 */
	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	/**
	 * @param componentProperties the componentProperties to set
	 */
	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
		setCampaignLabel(properties.get("campaignLabel",""));
		setImageDescriptionA(properties.get("imageDescriptionA",""));
		setImageDescriptionB(properties.get("imageDescriptionB",""));
		this.externalLink = componentProperties.get("externalLink",false);
		
		this.imageB = new Image(this.resource, "imageB");
		setImageB(imageB);
		
		if (imageB.hasContent()) {
			
			setImageB_SRC(imageB.getSrc());
			
		} else {
			
			setImageB_SRC("");
			
		}

		this.image = new Image(this.resource, "image");
		setImage(image);		
	
		setLinkTargetA(properties.get("linkTargetA",""));
		setLinkTargetB(properties.get("linkTargetB",""));

	}

	/**
	 * @return the resourceResolver
	 */
	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	/**
	 * @param resourceResolver the resourceResolver to set
	 */
	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	/**
	 * @return the resource
	 */
	public Resource getResource() {
		return resource;
	}

	/**
	 * @param resource the resource to set
	 */
	public void setResource(Resource resource) {
		this.resource = resource;
	}

	/**
	 * @return the currentStyle
	 */
	public Style getCurrentStyle() {
		return currentStyle;
	}

	/**
	 * @param currentStyle the currentStyle to set
	 */
	public void setCurrentStyle(Style currentStyle) {
		this.currentStyle = currentStyle;
	}

	/**
	 * @return the currentDesign
	 */
	public Design getCurrentDesign() {
		return currentDesign;
	}

	/**
	 * @param currentDesign the currentDesign to set
	 */
	public void setCurrentDesign(Design currentDesign) {
		this.currentDesign = currentDesign;
	}

	/**
	 * @return the resourceDesign
	 */
	public Design getResourceDesign() {
		return resourceDesign;
	}

	/**
	 * @param resourceDesign the resourceDesign to set
	 */
	public void setResourceDesign(Design resourceDesign) {
		this.resourceDesign = resourceDesign;
	}

	/**
	 * @return the request
	 */
	public ServletRequest getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(ServletRequest request) {
		this.request = request;
	}

	/**
	 * @return the out
	 */
	public Writer getWriter() {
		return writer;
	}

	/**
	 * @param out the out to set
	 */
	public void setWriter(Writer writer) {
		this.writer = writer;
	}

	public XSSAPI getXss() {
		return xss;
	}

	public void setXss(XSSAPI xss) {
		this.xss = xss;
	}

	/**
	 * @return the image
	 */
	public Image getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	@SuppressWarnings("deprecation")
	public void setImage(Image image) {
		this.image = image;
		if (this.image.hasContent()) {
			
			try {
			
				this.image = FHUtilityFunctions.nukeURLPrefix(this.image, resourceResolver);
			
			} catch(UnsupportedEncodingException e) {
			
				log.error("ERROR: Unsupported Encoding Exception - ".concat(e.getMessage()));
			
			}
		
		}
		this.image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
		this.image.loadStyleData(currentStyle);
	    this.image.setSelector(".img"); // use image script
	    this.image.setDoctype(Doctype.fromRequest(request));
	    // add design information if not default (i.e. for reference paras)
	    if (!currentDesign.equals(resourceDesign)) {
	        this.image.setSuffix(currentDesign.getId());
	    }
	    
	    if (imageB_SRC != null && imageB_SRC != "") {
	    	
	    	if (xss != null) {
	    		
	    		this.image.addAttribute(B_IMAGE_ATTRIBUTE, xss.getValidHref(imageB_SRC));
	    		
	    	} else {
	    	
	    		this.image.addAttribute(B_IMAGE_ATTRIBUTE, imageB_SRC);
	    	
	    	}
	    	
	    }
	    
	    if (campaignLabel != null && campaignLabel != "") {
	    	
    		this.image.addAttribute(CAMPAIGN_LABEL_ATTRIBUTE, campaignLabel);
	    	
	    }
	    
	    if (imageDescriptionA != null && imageDescriptionA != "") {
	    	
    		this.image.addAttribute(A_IMAGE_DESCRIPTION_ATTRIBUTE, imageDescriptionA);
	    	
	    }
	    
	    if (imageDescriptionB != null && imageDescriptionB != "") {
	    	
    		this.image.addAttribute(B_IMAGE_DESCRIPTION_ATTRIBUTE, imageDescriptionB);
	    	
	    }

	}

	/**
	 * @return the linkTarget
	 */
	public String getLinkTargetA() {
		return linkTargetA;
	}

	/**
	 * @param linkTarget the linkTarget to set
	 */
	public void setLinkTargetA(String linkTargetA) {
		if(!linkTargetA.equals("")){
	        this.linkTargetA = FHUtilityFunctions.cleanLink(linkTargetA, resourceResolver);
	    } else {
	    	this.linkTargetA = "";
	    }
	}
	
	public String getLinkTargetB() {
		return linkTargetB;
	}

	public void setLinkTargetB(String linkTargetB) {
		if(!linkTargetB.equals("")){
	        this.linkTargetB = FHUtilityFunctions.cleanLink(linkTargetB, resourceResolver);
	    } else {
	    	this.linkTargetB = "";
	    }
	}

	public Image getImageB() {
		return imageB;
	}

	@SuppressWarnings("deprecation")
	public void setImageB(Image imageB) {
		this.imageB = imageB;
		this.imageB.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
		this.imageB.loadStyleData(currentStyle);
	    this.imageB.setSelector(".img"); // use image script
	    this.imageB.setDoctype(Doctype.fromRequest(request));
	    // add design information if not default (i.e. for reference paras)
	    if (!currentDesign.equals(resourceDesign)) {
	        this.imageB.setSuffix(currentDesign.getId());
	    }

	}

//	public Boolean getHasTwoImages() {
//		return hasTwoImages;
//	}
//
//	public void setHasTwoImages(Boolean hasTwoImages) {
//		this.hasTwoImages = hasTwoImages;
//	}

	public String getImageB_SRC() {
		return imageB_SRC;
	}

	public void setImageB_SRC(String imageB_SRC) {
		this.imageB_SRC = imageB_SRC;
		
		if (this.imageB_SRC.contains("imageB/file")) {
			
			this.imageB_SRC = this.imageB_SRC.replaceAll("imageB/file\\.", "imageB.img.");
			
		}
		
	}

	public String getCampaignLabel() {
		return campaignLabel;
	}

	public void setCampaignLabel(String campaignLabel) {
		this.campaignLabel = campaignLabel;
	}

	public String getImageDescriptionA() {
		return imageDescriptionA;
	}

	public void setImageDescriptionA(String imageDescriptionA) {
		this.imageDescriptionA = imageDescriptionA;
	}

	public String getImageDescriptionB() {
		return imageDescriptionB;
	}

	public void setImageDescriptionB(String imageDescriptionB) {
		this.imageDescriptionB = imageDescriptionB;
	}

	public boolean isExternalLink() {
		return externalLink;
	}

	public void setExternalLink(boolean externalLink) {
		this.externalLink = externalLink;
	}

	public void drawImage() {
		try {
			image.draw(writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}