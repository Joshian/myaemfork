/* This function is called when a YouTube player is set up. Must be named this
 * for YouTube's API to work. */
function onYouTubePlayerReady(playerId) {
	
	var dataHash = playerId.replace("player", ""),
		theSlideshow,
		thePlayer = document.getElementById(playerId.replace("player", "video-"));
	
	/* Run through all the slideshows on the page and find the one whose ID matches */
	for (var i = 0; i < slideShowArray.length; i++) {
		
		if (slideShowArray[i].ID === dataHash) {
			
			theSlideshow = slideShowArray[i];
			break;
			
		}
		
	}
	
	if (theSlideshow && thePlayer) {
		
		theSlideshow.ytplayer = thePlayer;
		theSlideshow.moveMainVideoContent();
		
	}
	
}

/* Again, this is namespaced enough that I don't feel like it needs to be contained
 * within the componentApp */
function FH_videoSlideshow(dataHash) {
	
	/* Object variables */
	this.theScope = $("#videoSlide_" + dataHash);
	this.ID = dataHash;
	this.firstVisibleSlide = 0;
	this.lastVisibleSlide = 3;
	this.numberOfSlides = $(".originalSlideContainer", this.theScope).size();
	this.ytplayer;
	
	/* See individual functions for explanations of what these do */
	createSkeleton.call(this);
	setVideoHeight.call(this);
	setAdditionalContainerWidth.call(this);
	createYouTubePlayer.call(this);
	moveVideoSlides.call(this);
	setVideoSlideWidth.call(this);
	bindEvents.call(this);
	
	/* Hide the left arrow since we start at the leftmost slide */
	this.toggleArrows();
	
	/********************************
	******* Private methods *********
	*********************************/
	
	/* A function to create the necessary HTML structure and add it to the DOM */	
	function createSkeleton() {

		/* Create all the elements with the relevant classes/ids */
		var mainContainer = $("<div></div>",
			{
				"class" : "mainContainer"
			}),
				mainVideoContainer = $("<div></div>",
					{
						"class" : "mainVideoContainer"
					}),
				mainVideo = $("<div></div>",
					{
						"id" : "mainVideo_" + this.ID
					}),
				mainVideoTitle = $("<h4></h4>",
					{
						"class" : "mainVideoTitle"
					}),
				mainVideoDescription = $("<p></p>",
					{
						"class" : "mainVideoDescription"
					}),
				additionalVideoContainer = $("<div></div>",
					{
						"class" : "additionalVideoContainer"
					}),
				leftArrow = $("<div></div>",
					{
						"class" : "leftArrow"
					}),
				additionalVideoWindow = $("<div></div>",
					{
						"class" : "additionalVideoWindow"
					}),
				videoSlideContainer = $("<div></div>",
					{
						"class" : "videoSlideContainer"
					}),
				rightArrow = $("<div></div>",
					{
						"class" : "rightArrow"
					});
		
		/* Now append the skeleton to the page. First the main video container */
		
		var target = $(".mainTitle", this.theScope);
		
		target.after(mainContainer.append(mainVideoContainer.append(mainVideo))
			.append(mainVideoTitle).append(mainVideoDescription));
		
		/* And now append the additional video slides */
		target = $(".additionalTitle", this.theScope);
		
		target.after(additionalVideoContainer.append(leftArrow)
			.append(additionalVideoWindow.append(videoSlideContainer))
			.append(rightArrow));
			
		/* Finally, set a local variable since it now exists */
		
		this.theContainer = $(".videoSlideContainer", this.theScope);
		
	}
	
	/* A function to set the height of the main video container, proportional to 
	its width */
	function setVideoHeight() {

		var videoContainer = $(".mainVideoContainer", this.theScope),
			videoWidth = videoContainer.width(),
			
			/* Since standard YouTube widget is 640x390, set the aspect ratio */
			videoHeight = videoWidth * 0.609375;
		
		videoContainer.height(videoHeight);
		
	}
	
	/* A function to set the width of the additional video slides container. It
	needs to be proportional to the number of videos it contains. */
	function setAdditionalContainerWidth() {
		
		var containerWidth = "100%";
		
		if (this.numberOfSlides > 3) {
			
			containerWidth = this.numberOfSlides * 26 + "%";
			
		}
		
		this.theContainer.width(containerWidth);
		
	}

	function createYouTubePlayer() {
		
		/* prepare and embed the SWF YouTube player */
		var videoVariables = {
			"video_id" : 'dx06Ci96IB4',
			"playerapiid" : 'player' + this.ID,
			"params" : { allowScriptAccess: "always", bgcolor: "#000000", wmode: "opaque" },
			"atts" : { id: "video-" + this.ID },
			"domain" : ('https:' === document.location.protocol) ? 'https://www.youtube.com' : 'http://www.youtube.com'
		};
	
		swfobject.embedSWF(videoVariables.domain + "/v/"+ videoVariables.video_id + 
			"?modestbranding=1&amp;title=&amp;enablejsapi=1&amp;playerapiid="+ videoVariables.playerapiid +
			"&amp;version=3&amp;rel=0", "mainVideo_" + this.ID, "100%", "100%", "8", null, null, 
			videoVariables.params, videoVariables.atts);
		
	}

	/* A function that moves the content from the old, CQ output HTML into the newly
	created skeleton */
	function moveVideoSlides() {
		
		var that = this,
			theOriginals = $(".originalSlideContainer", this.theScope),
			theClone = theOriginals.first().clone(true),
			size = theOriginals.size();
			
		theOriginals.slice(1).add(theClone).each( function(index) {
			
			/* Create the new slide div, binding the YouTube video ID to it */
			var newSlide = $("<div></div>", {
				"class" : "videoSlide",
				"data-source" : $(this).data("source")
			});
			
			/* If this is the final slide, give it the active class */	
			if (index === size - 1) {
				
				newSlide.addClass("active");
				
			}
			
			/* Add the click event handler that loads the video into the main player */ 
			newSlide.click( function(e) {
				
				/* Bear in mind that throughout this function, "that" refers to the video slideshow
				object, while "this" refers to the element we are adding the click event to. */
				var videoId = $(this).data("source");
				e.preventDefault();
				
				if (that.ytplayer) {
					
					/* If this is already the loaded video, don't reload it */
					if ($(this).hasClass("active")) {
						
						return;
						
					} else {
					
						that.ytplayer.loadVideoById(videoId, 0, "default");
						
					}
					
					/* Change the main video description and title*/
					$(".mainVideoTitle", that.theScope).text($("h4", $(this)).text());
					$(".mainVideoDescription", that.theScope).html($(".videoDescription", 
						$(this)).html());
					
					/* Make this the active slide */
					$(".videoSlide", that.theScope).removeClass("active");
					$(this).addClass("active");
					
				} else {
					
					alert("ERROR - There is no YouTube player available");
					
				}
				/* END ANONYMOUS FUNCTION */	
			});
			/* Add the newly made slide into the DOM, just before the rightArrow div */
			newSlide.append($(this).children()).appendTo(that.theContainer);
			
			/* END JQUERY EACH STATEMENT */
		});
		
		/* Remove the cloned item from the DOM */
		
		theOriginals.first().detach();
				
		/* Finally, set a local variable since it now exists */
		
		this.theSlides = $(".videoSlide", this.theScope);

	}
	
	/* A function that sets the width, padding, and right margin on each video slide
	proportional to the number of slides in the video slide container */
	function setVideoSlideWidth() {

		/* Each slide is 85% content width, 10% margin and 2.5% padding on each side */
		var grossSize = 100 / this.numberOfSlides,
			slideRatio = 0.85,
			marginRatio = 0.1,
			paddingRatio = 0.025,
			slideWidth = grossSize * slideRatio + "%",
			marginWidth = grossSize * marginRatio + "%",
			paddingWidth = grossSize * paddingRatio + "%";
		
		/* Set the width, padding, and margin for each slide */	
		this.theSlides.css({
			"width" : slideWidth,
			"padding-left" : paddingWidth,
			"padding-right" : paddingWidth,
			"margin-right" : marginWidth
		});
		
		/* Remove the margin from the final slide */
		this.theSlides.last().css({
			"margin-right" : 0
		});
		
	}
	
	function bindEvents() {
		
		var that = this;
		
		/* Add slide advance/regress events to the right/left arrows */
		$(".rightArrow", this.theScope).click($.proxy(that.slideAdvance, that));
		$(".leftArrow", this.theScope).click($.proxy(that.slideRegress, that));
		
	}

	
}

/* Public methods and variables */

FH_videoSlideshow.prototype.toggleArrows = function() {
	
	var rightArrow = $(".rightArrow", this.theScope),
		leftArrow = $(".leftArrow", this.theScope),
		bgImagePath = "/etc/designs/public/img/buttons/videoSlideshowArrows.gif";
	
	/* If we are at the end of the slides, remove the right arrow */
	if (this.lastVisibleSlide === this.numberOfSlides - 1) {
		
		rightArrow.css({"background-image" : "none"});
	
	/* Otherwise, replace it */	
	} else if (rightArrow.css("backgroundImage") === "none") {
	
		rightArrow.css({"background-image" : "url(" + bgImagePath + ")"});
		
	}
	
	/* If we are at the beginning of the slides, remove the left arrow */
	if (this.firstVisibleSlide === 0) {
		
		leftArrow.css({"background-image" : "none"});
		
	/* Otherwise, replace it */	
	} else if (leftArrow.css("backgroundImage") === "none") {
	
		leftArrow.css({"background-image" : "url(" + bgImagePath + ")"});
		
	}

};

FH_videoSlideshow.prototype.slideAdvance = function() {
	
	var lastAbsoluteSlide = this.numberOfSlides - 1,
		slidesToSlide = 3,
		slideAmount,
		grossSize = 100 / this.numberOfSlides,
		percentageFactor = this.numberOfSlides * 26 / 100,
		that = this;

	/* No need to do anything if we are already at the end */		
	if (this.lastVisibleSlide === lastAbsoluteSlide) {
		
		return;
		
	} else if (this.lastVisibleSlide + slidesToSlide > lastAbsoluteSlide) {
		
		slidesToSlide = lastAbsoluteSlide - this.lastVisibleSlide;
		
	}
	
	slideAmount = slidesToSlide * grossSize * percentageFactor;
	
	/* Update the first and last visible slide variables */
	this.lastVisibleSlide += slidesToSlide;
	this.firstVisibleSlide += slidesToSlide;
	
	this.theContainer.animate({"left" : "-=" + slideAmount + "%"}, 1000, $.proxy(that.toggleArrows, that));
	
};

FH_videoSlideshow.prototype.slideRegress = function() {
	
	var slidesToSlide = 3,
		slideAmount,
		grossSize = 100 / this.numberOfSlides,
		percentageFactor = this.numberOfSlides * 26 / 100,
		that = this;

	/* No need to do anything if we are already at the beginning */		
	if (this.firstVisibleSlide === 0) {
		
		return;
		
	} else if (this.firstVisibleSlide - slidesToSlide < 0) {
		
		slidesToSlide = this.firstVisibleSlide;
		
	}
	
	slideAmount = slidesToSlide * grossSize * percentageFactor;
	
	/* Update the first and last visible slide variables */
	this.lastVisibleSlide -= slidesToSlide;
	this.firstVisibleSlide -= slidesToSlide;
	
	/* Animate the container and then toggle the arrows if necessary */
	this.theContainer.animate({"left" : "+=" + slideAmount +"%"}, 1000, $.proxy(that.toggleArrows, that));

};

FH_videoSlideshow.prototype.moveMainVideoContent = function() {
	
	var theFirstVideo = this.theSlides.last(),
		theTitle = theFirstVideo.find("h4"),
		theDescription = theFirstVideo.find("p"),
		targetTitle = $(".mainVideoTitle", this.theScope),
		targetDescription = $(".mainVideoDescription", this.theScope),
		videoId = theFirstVideo.data("source");
		
	targetTitle.text(theTitle.text());
	targetDescription.html(theDescription.html());
	
	if (this.ytplayer) {
		
		/* Load the appropriate video into the player */
		this.ytplayer.cueVideoById(videoId);
		
	} else {
		
		alert("no player");
		
	}
	
}