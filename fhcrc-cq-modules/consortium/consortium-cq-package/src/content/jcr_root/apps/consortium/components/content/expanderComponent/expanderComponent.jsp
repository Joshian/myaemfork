<%@include file="/libs/foundation/global.jsp"%><%@ page import="com.day.cq.wcm.api.WCMMode" %>
<%
String headerTag = properties.get("headerSize","h2");
String headerText = properties.get("title","");


if (headerText.trim().equals("")) {
%>
<div><span class="cq-text-placeholder">&para;</span></div>
<%
} else {
%>
<<%= headerTag %> class="expanderHeader" data-is-open="true" data-default-open="<%= properties.get("defaultOpen","false") %>"><span class="expanderHeaderText"><%= headerText %></span></<%= headerTag %>>
<%
}

%>

<cq:text property="intro" tagClass="expanderIntro" placeholder="" />

<div class="expanderParsys">

    <cq:include path="expanderPar" resourceType="foundation/components/parsys" />

<% if(WCMMode.fromRequest(request) == WCMMode.EDIT){%>
  <div class="cq-edit-only" style="font-style:italic; background-color:#ddd; text-align:center;">
    This is the end of the expander widget. This text will not appear on your webpage.
  </div>
<% } %>

</div>

<cq:includeClientLib js="consortium.components.expanderComponent" />