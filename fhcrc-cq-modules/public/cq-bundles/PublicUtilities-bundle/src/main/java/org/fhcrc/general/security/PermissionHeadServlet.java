package org.fhcrc.general.security;


import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.api.resource.Resource;

/**
* @scr.component metatype="false"
* @scr.service
* @scr.property name="sling.servlet.paths" value="/bin/permissioncheck"
*
*/
public class PermissionHeadServlet extends SlingSafeMethodsServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(PermissionHeadServlet.class);

    public void doHead(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String uri = request.getParameter("uri");
        Resource test = request.getResourceResolver().getResource(uri);
        if (test != null) {
            response.setStatus(SlingHttpServletResponse.SC_OK);
        } else {
            
            response.setStatus(SlingHttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}