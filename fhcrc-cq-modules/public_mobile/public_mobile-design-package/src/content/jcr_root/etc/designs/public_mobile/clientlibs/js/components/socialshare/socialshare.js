

var componentApp = (function(componentApp) {
	
	/*---------------- SOCIALSHARE COMPONENT ----------------*/
	/* Publicly accessible methods:
	 * 
	 * clickFB() - Handles the click you have made on the Facebook icon.
	 */
	componentApp.socialshareComponent = (function(socialshareComponent) {
		
		
		socialshareComponent.clickFB = function(e) {
			
          if (!e) var e = window.event;//var evt = e ? e : window.event;
          if (e.stopPropagation)    e.stopPropagation(); // http://www.javascripter.net/faq/canceleventbubbling.htm
          if (e.cancelBubble!=null) e.cancelBubble = true;
          if (e.preventDefault) e.preventDefault(); // http://www.javascripter.net/faq/eventpreventdefault.htm
          e.returnValue = false;
      
          var targ, u, t;
        	if (e.target) targ = e.target;
        	else if (e.srcElement) targ = e.srcElement;
        	if (targ.nodeType == 33) // defeat Safari bug  - http://www.quirksmode.org/js/events_properties.html#target
        		targ = targ.parentNode;
          //alert("data-url is " + targ.parentNode.getAttribute('data-url'));
      
          //u=location.href; //u="http://www.fhcrc.org/en/about.html";
          u=targ.parentNode.getAttribute('data-url'); // the url is stored in the wrapping a tag. the event target is the facebook icon.
          t=document.title;
          window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');
      
          return false;    
    
		}
		
		return socialshareComponent;
	
	})(componentApp.socialshareComponent || {});
	
	return componentApp;
	
})(componentApp || {});


