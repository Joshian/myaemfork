<%@include file="/libs/foundation/global.jsp" %>
 <!-- page content -->
 <div id="fh_body" class="fh_body ar_contentpage container_12 clearfix"><a name="content"></a>

  <div class="grid_12">

    <!-- breadcrumbs appear in the content flow in case they wrap to two lines -->
    <cq:include script="breadcrumbs.jsp"/>

    <!--  mobile menu -->
    <cq:include script="mobile-menu.jsp"/>  

    <!-- all pages have the cluster title -->
    <cq:include script="clusterTitle.jsp" /> 
  
    
  </div>
    
  <!-- content column -->
  
  

  <% if("true".equals(properties.get("hasRightColumn","false"))){ %>
    <div id="fh_body_content" class="grid_12 fh_rightcol_container fh_body_content clearfix">
      <cq:include path="mainimage" resourceType="public/components/content/imagePlus"/>
      <div class="grid_8 fh_body_column alpha">
        <header>
           <cq:include path="maintitle" resourceType="foundation/components/title"/>
           <cq:include path="mainsubtitle" resourceType="public/components/content/textarea"/>
        </header>
        <cq:include path="par" resourceType="foundation/components/parsys"/>
        <cq:include path="mainColNav" resourceType="public/components/content/inPageSectionNav" />
        <!-- remote-wrap-api-marker -->
      </div>    
      <div class="grid_4 omega"><div class="fh_rightcol_content">
        <cq:include path="rightColNav" resourceType="public/components/content/inPageSectionNav" />
        <cq:include script="social-options.jsp" />
        <cq:include path="parcolright" resourceType="foundation/components/parsys"/>
      </div></div>
      <div class="ar_footer grid_12 alpha omega">
        <cq:include script="social-options.jsp" />
      </div>
    </div>    
    <div style="padding-bottom:0px;">&nbsp;</div>
  <% } else { %>
    <div id="fh_body_content" class="fh_body_content grid_12">
      <cq:include path="mainimage" resourceType="public/components/content/imagePlus"/>
      <header>
        <cq:include path="maintitle" resourceType="foundation/components/title"/>
        <cq:include path="mainsubtitle" resourceType="public/components/content/textarea"/>
      </header>
      <cq:include path="par" resourceType="foundation/components/parsys"/>
      <!-- remote-wrap-api-marker -->
      <div class="ar_footer grid_12 alpha omega">
        <cq:include script="social-options.jsp" />
      </div>      
    </div>
  <% } %>
  <!-- /content column -->
    
  <br class="clear"/>
   
 </div><!-- /fh_body, page content -->
