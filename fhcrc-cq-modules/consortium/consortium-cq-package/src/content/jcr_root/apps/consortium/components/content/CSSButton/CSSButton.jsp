<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.wcm.api.WCMMode,
                 org.fhcrc.tools.FHUtilityFunctions" %>
<%
String brandColor = properties.get("color", "brandColor1");
String textSize = properties.get("textSize", "smallText");
String linkTarget = properties.get("linkTarget","");
Resource r;
Boolean hasLink = !linkTarget.trim().equals("");
String buttonClass = textSize.concat(" buttonText");
String buttonText = properties.get("text","Button text");

if (!properties.get("raquo","").equals("")) {
    
    buttonText = buttonText.concat("&nbsp;&raquo;");
    
}

if(hasLink){
    linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);

%>    
<a href="<%= linkTarget %>" <%= properties.get("externalLink","false").equals("true") ? "target=\"_blank\"" : "" %>>
<%    
}

%>
<div class="CSSButtonContainer <%= brandColor %>">

    <div class="buttonTextContainer">

        <span class="<%= buttonClass %>"><%= buttonText %></span>
    
    </div>

</div>
<%
if(hasLink) {
%>
</a>
<%
}
%>