<%@include file="/apps/stripe/global.jsp" %>
<%@page import="com.day.cq.wcm.api.Page,
                org.fhcrc.stripe.templates.general.Colors" %>
<%
     String footerTopColor = Colors.getColorProperty(currentPage,"footer_top");
     String footerBottomColor = Colors.getColorProperty(currentPage,"footer_bottom");
     String footerTextColor = Colors.getColorProperty(currentPage,"footer_text");
     
     String footerButtonTopColor = Colors.getColorProperty(currentPage,"footer_button_top");
     String footerButtonBottomColor = Colors.getColorProperty(currentPage,"footer_button_bottom");
     String footerButtonTextColor = Colors.getColorProperty(currentPage,"footer_button_text");
     String footerButtonSelectionColor = Colors.getColorProperty(currentPage,"footer_button_selection");
%>

<style type="text/css">

/* ------footer colorbar start -------*/
.footer-colorbar{
    color: #<%=footerTextColor%>;
    background-color: #<%=footerBottomColor%>;
    background-image: -moz-linear-gradient(top, #<%=footerTopColor%>, #<%=footerBottomColor%>);
    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#<%=footerTopColor%>), to(#<%=footerBottomColor%>));
    background-image: -webkit-linear-gradient(top, #<%=footerTopColor%>, #<%=footerBottomColor%>);
    background-image: -o-linear-gradient(top, #<%=footerTopColor%>, #<%=footerBottomColor%>);
    background-image: linear-gradient(to bottom, #<%=footerTopColor%>, #<%=footerBottomColor%>);
    filter: progid:dximagetransform.microsoft.gradient(startColorstr='#ff<%=footerTopColor%>', endColorstr='#ff<%=footerBottomColor%>', GradientType=0);
}
/* ------footer colorbar end -------*/

.footer .footer-text p{
    color: #<%=footerTextColor%>;
}
/* ------ footer buttons start -------*/
.btn-login {
  color: #<%=footerButtonTextColor%>;
  background-color: #<%=footerButtonBottomColor%>;
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#<%=footerButtonTopColor%>), to(#<%=footerButtonBottomColor%>));
  background-image: -webkit-linear-gradient(top, #5824<%=footerButtonTopColor%>00, #<%=footerButtonBottomColor%>);
  background-image: -o-linear-gradient(top, #<%=footerButtonTopColor%>, #<%=footerButtonBottomColor%>);
  background-image: linear-gradient(to bottom, #<%=footerButtonTopColor%>, #<%=footerButtonBottomColor%>);
  background-image: -moz-linear-gradient(top, #<%=footerButtonTopColor%>, #<%=footerButtonBottomColor%>);
  filter: progid:dximagetransform.microsoft.gradient(startColorstr='#ff<%=footerButtonTopColor%>', endColorstr='#ff<%=footerButtonBottomColor%>', GradientType=0);
}

.btn-login:hover,
.btn-login:active,
.btn-login.active,
.btn-login.disabled,
.btn-login[disabled] {
  color: #<%=footerButtonTextColor%>;
  background-color: #<%=footerButtonSelectionColor%>;
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#<%=footerButtonTopColor%>), to(#<%=footerButtonSelectionColor%>));
  background-image: -webkit-linear-gradient(top, #5824<%=footerButtonTopColor%>00, #<%=footerButtonSelectionColor%>);
  background-image: -o-linear-gradient(top, #<%=footerButtonTopColor%>, #<%=footerButtonSelectionColor%>);
  background-image: linear-gradient(to bottom, #<%=footerButtonTopColor%>, #<%=footerButtonSelectionColor%>);
  background-image: -moz-linear-gradient(top, #<%=footerButtonTopColor%>, #<%=footerButtonSelectionColor%>);
  filter: progid:dximagetransform.microsoft.gradient(startColorstr='#ff<%=footerButtonTopColor%>', endColorstr='#ff<%=footerButtonSelectionColor%>', GradientType=0);
}
/* ------ footer buttons end -------*/
</style>