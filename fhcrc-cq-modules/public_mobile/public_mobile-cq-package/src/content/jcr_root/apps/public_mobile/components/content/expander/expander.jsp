<%@include file="/libs/foundation/global.jsp"%>
<%
String defaultStatus = properties.get("defaultStatus","closed"),
    closingLine = properties.get("closingLine","false");
int hashCode = resource.hashCode();    
%>
<div class="expanderContainer" data-default-status="<%= defaultStatus %>">

    <div class="title">
    
        <div class="titleContainer tableCell">
        
            <cq:text property="title" tagName="h2" />
        
        </div>
        
        <div class="iconContainer tableCell">
        
            <span class="expanderIcon" data-status="closed">+</span>
        
        </div>
    
    </div>
    
    <div class="textContainer">
    
        <cq:include path="par_<%= hashCode %>" resourceType="foundation/components/parsys"/>
    
    </div>

<%

if (closingLine.equals("true")) {
    
%>

    <div class="closingLine"></div>

<%

}

%>

</div>
<cq:includeClientLib js="mobile.components.expander" />
<script type="text/javascript">
/* If we haven't already added the event listener to initialize the expander
 * components, switch the hasExpanders boolean and add the initialization
 * function to the DOMContentLoaded event.
 */
if (!componentApp.expanderComponent.hasExpanders) {

    componentApp.expanderComponent.hasExpanders = true;
    document.addEventListener("DOMContentLoaded", 
      componentApp.expanderComponent.expanderInit, false);
      
}
</script>