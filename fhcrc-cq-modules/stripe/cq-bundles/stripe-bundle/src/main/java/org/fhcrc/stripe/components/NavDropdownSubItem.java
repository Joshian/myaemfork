/**
 * NavDropdownItem class is in charge of handling NavDropdownItem properties and logic
 *  
 */
package org.fhcrc.stripe.components;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

public class NavDropdownSubItem 
{
	private final static String SUB_ITEM_PATH_PROPERTY = "subitempath";
	private final static String SUB_ITEM_LABEL_PROPERTY = "subitemlabel";

	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	
	private String subitempath;
	private String subitemlabel;
	
	/** Constructor of the class
	 * @param properties the page properties
	 * @param resourceResolver the resource resolver
	 */
	public NavDropdownSubItem() {

	}	

	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;
        this.subitempath = properties.get(SUB_ITEM_PATH_PROPERTY, String.class);
        this.subitemlabel = properties.get(SUB_ITEM_LABEL_PROPERTY, String.class);
	}

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public String getSubitempath() {
		return subitempath;
	}

	public void setSubitempath(String subitempath) {
		this.subitempath = subitempath;
	}

	public String getSubitemlabel() {
		return subitemlabel;
	}

	public void setSubitemlabel(String subitemlabel) {
		this.subitemlabel = subitemlabel;
	}
}