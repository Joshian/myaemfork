/* Module for Cancer Consortium functions and variables */

var sharedApp = (function(sharedApp) {

	sharedApp.addEvent = function(elem,evtType,func){
		if(elem.addEventListener){ elem.addEventListener(evtType,func,false); }
		else if(elem.attachEvent){ elem.attachEvent("on"+evtType, func); }
		else { elem["on"+evtType] = func; }
	};

	sharedApp.adjustBodyPosition = function(){
	  /* adjust the position of the body element so it appears
		 perfectly under the header. */
	  var o = document.getElementById('header_element'); //alert(o.offsetHeight);
	  var y;
	  if(o) { y = o.offsetHeight; }
	  if(y){
	    var o = document.getElementById('body_element');
	    if(o){
	      var s = 0; //20; // vertical offset from the bottom of the bluebar/mission, in pixels. you can adjust this to your liking.
	      o.style.marginTop = (y+s) + "px";
	    }
	  }
	};
	
	sharedApp.findChildWithClassName = function(el, className) {
		
		/* Runs through the passed element's children and looks for the first one that has the passed string
		 * as one of its classes. 
		 * Returns a DOM Element if successful and null otherwise. */
		var children = el.children,
			len = children.length,
			target;
			
		if (len === 0 || len === undefined) {
			
			throw new ElementChildrenException("Passed element contains no children");
			
		} else {
			
			for (var i = 0; i < len; i++) {
				
				if (children[i].classList.contains(className)) {
						
					target = children[i];
					break;
					
				}
				
			}
			
		}
		
		if (target !== undefined) {
			
			return target;
			
		} else {
			
			throw new ElementChildrenException("No child contains the class \"" + className + "\"");
			return null;
			
		}
		
	};
	
	sharedApp.handleChildException = function(e) {
		
		if (console) {
					
			console.log(e.name + ": " + e.message);
					
		} else {
					
			alert(e.name + ": " + e.message);
					
		}
		
	};

	sharedApp.closeMobileMenus = function(){
	/* hide any mobile or other menus that may be open. you
       invoke this on browser resize, e.g. */
	  var o_menu1 = document.getElementById('pushdown_menu');
	  var o_menu2 = document.getElementById('pushdown_search');
	  var o_search = document.getElementById('utility_search');
	  if( o_menu1 && o_menu2 && o_search ){
	      if(o_menu1.style.display=='block') o_menu1.style.display='none';
	      if(o_menu2.style.display=='block') o_menu2.style.display='none';
	      if(o_search.style.display=='block') o_search.style.display='none';
	  }
	};	
	
	return sharedApp;

})(sharedApp || {});

var ElementChildrenException = function(message) {
	
	this.message = message;
	this.name = "Element Child Finder Exception";
	
}