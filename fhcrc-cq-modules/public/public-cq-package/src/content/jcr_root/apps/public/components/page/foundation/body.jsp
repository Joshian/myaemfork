<%@include file="/libs/foundation/global.jsp"%><%

StringBuffer cls = new StringBuffer();

for (String c: componentContext.getCssClassNames()) {
   	cls.append(c).append(" ");
}

/* add in 'griddedcolumns' class for pages of specified templates.
   column controls are specially crafted to work to make a "gridded" effect when placed one next to the other.
   specifying which templates are eligible for this treatment targets
   the special grid handling css applied to column controls, allowing
   pages of other templates to skate by unmolested. there may come a day
   when you want ALL pages to render column controls as grids, so you can just
   hard-code 'griddedcolumns' into the class list.  */

   if( currentPage.getProperties().get("cq:template","").contains("homepage") ||
       currentPage.getProperties().get("cq:template","").contains("landingpage") ||
       currentPage.getProperties().get("cq:template","").contains("branchleader") ) {
         cls.append("griddedcolumns").append(" ");
   }

%>

<body class="<%= cls.toString().trim() %>">
<cq:include script="redirect-notice.jsp"/>

<a href="#content" title="Jump to content"></a>
<a href="#site-wide-navigation" title="Jump to site-wide navigation"></a>
<a href="#section-navigation" title="Jump to navigation for this section"></a>


<div id="fh_container" class="fh_container">
 <cq:include script="content.jsp"/>
 <cq:include script="chrome1.jsp"/><!--  mission, global nav and logo -->
</div><!-- /fh_container -->  

<!-- helps some browsers block position the footer: -->
<div class="clear"></div>
 
 <cq:include script="footer.jsp"/><!--  fat footer -->
 <cq:include script="chrome2.jsp"/><!--  utility nav -->
 
 <cq:include script="pixels.jsp"/>

</body>