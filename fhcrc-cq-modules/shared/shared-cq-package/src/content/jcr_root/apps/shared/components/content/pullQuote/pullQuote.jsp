<%--

  Pull Quote component.

  This component adds a blockquote tag to the page, with the text surrounded by curly quotes.

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %><%

// COLOR THEME:
// colorTheme0, colorTheme1, etc. are controlled
// via design dialog. In the design dialog configure
// the allowed set of text & background colors like this:
//
//        0<tab>Normal (default)
//        1<tab>Black text on white background
//        2<tab>White text on yellow background
//        #<tab>Text the user will see in the dropdown
//        etc.
//
// you do not have to go sequentially, and can use multiple digits if you want.
// now the user will be able to select a background color of your choosing.
// this component places a class, colorTheme#, around the text div.
// add the appropriate styling to your css for the colorTheme# choices you provide.


// DESIGN THEME:
// for complete generality, we add the ability to select a second class.
// you can use this however you want. e.g., 
//
//        0<tab>None (default)
//        1<tab>Narrow margins
//        2<tab>Blinking text
//        #<tab>Text the user will see in the dropdown
//        etc.

 String colorthemes = properties.get("colortheme","0"); // default is theme 0
 //out.write(colorthemes);
 String[] c = colorthemes.split("\t");
 String color_theme_number = c[0];
 String colorTheme = "";
 if(!color_theme_number.isEmpty()){
     colorTheme = "colorTheme" + color_theme_number ;
 }

 String designthemes = properties.get("designtheme","0"); // default is theme 0
 //out.write(designthemes);
 c = designthemes.split("\t");
 String design_theme_number = c[0];
 String designTheme = "";
 if(!design_theme_number.isEmpty()){
   designTheme = "designTheme" + design_theme_number ;
 }    

String quote = properties.get("quote","");

if (quote.trim().equals("")) {
	
%>

<div><span class="cq-text-placeholder-ipe">&para;</span></div>

<%

} else { %>

<blockquote class="
    <%= colorTheme.equals("") ? "" : colorTheme %>
    <%= designTheme.equals("") ? "" : (" ").concat(designTheme) %>">

    &ldquo;<%= quote %>&rdquo;

</blockquote>

<%

}

%>
