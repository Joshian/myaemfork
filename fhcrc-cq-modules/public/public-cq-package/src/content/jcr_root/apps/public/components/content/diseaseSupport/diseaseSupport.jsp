<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.foundation.Image" %>
<%
Image overrideImage = new Image(resource, "image");
%>
<div class="diseaserelated">    
    <a href="/content/public/en/how-to-help.html">
<%
if(overrideImage.hasContent()){
	/* Design no longer has frame class as default, so removing */
//	overrideImage.addCssClass("frame");
	overrideImage.addAttribute("width","174px");
	overrideImage.loadStyleData(currentStyle);
	overrideImage.setSelector(".img"); // use image script
	overrideImage.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
    	overrideImage.setSuffix(currentDesign.getId());
    }
    overrideImage.draw(out);
} else {
%>    
    <img src="<%= currentDesign.getPath() %>/img/defaults/research_1.jpg" alt="Researcher in lab" />
<% } %>    
    </a>
    <p>Fred Hutch scientists are producing some of the most important breakthroughs in the prevention, early detection and treatment of cancer, HIV and other diseases.</p>
    <a href="https://secure2.convio.net/fhcrc/site/Donation2?df_id=3440&amp;3440.donation=form1" target="_blank"><img class="rolloverImage" src="/etc/designs/public/img/buttons/button_sm_donate_research_stat.gif" data-hover="/etc/designs/public/img/buttons/button_sm_donate_research_over.gif" alt="Donate to our Research" /></a>
</div>