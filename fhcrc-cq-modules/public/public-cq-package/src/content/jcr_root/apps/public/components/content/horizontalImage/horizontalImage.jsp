<%@ page import="com.day.cq.commons.Doctype,
    com.day.cq.wcm.api.components.DropTarget,
    com.day.cq.wcm.api.WCMMode,
    com.day.cq.wcm.foundation.Image,
    org.fhcrc.tools.FHUtilityFunctions" %><%
%><%@include file="/libs/foundation/global.jsp"%>
<%
    Image image = new Image(resource, "image");
    Image mouseover = new Image(resource, "mouseover");
    String mouseoverSrc = "";
    String linkTarget = properties.get("linkTarget","");
    Boolean isBordered = properties.get("bordered","false").equals("false") ? false : true;
    String hz_imageBorderClass = isBordered ? "" : " noborder";
    
    if(!linkTarget.equals("")){
        linkTarget = FHUtilityFunctions.cleanLink(linkTarget, resourceResolver);
    }
    
    if(mouseover.hasContent()){
        mouseoverSrc = mouseover.getSrc();
    
        // If the mouseover image is not in the dam, we need to mess with the Src string to get mouseover.img.<file extension> rather than mouseover/file.<file extension>
        if(mouseoverSrc.contains("mouseover/file")){
            mouseoverSrc = mouseoverSrc.replaceAll("mouseover/file\\.", "mouseover.img.");
        }
    }

    //drop target css class = dd prefix + name of the drop target in the edit config
    image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
    if(isBordered) {
        image.addCssClass("frame"); // Add the frame class unless omitted
    }
    if(mouseover.hasContent()){
        image.addAttribute("data-hover", mouseoverSrc);
        image.addCssClass("rolloverImage");
    }
    //remove title attribute unless there is something explicitly in the dialog
    if(properties.get("image/jcr:title","").trim().equals("")) {
        image.setTitle(" ");
    }
    image.loadStyleData(currentStyle);
    image.setSelector(".img"); // use image script
    image.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        image.setSuffix(currentDesign.getId());
    }
    %>
<div class="horizontalimagetext
<% 
    if(!image.hasContent()) { 
        out.print(" noimage");
    }
%>
">
    <div class="hz_container">
        <cq:text property="title" tagName="h3" placeholder="Title" />
        <div class="hz_image<%=hz_imageBorderClass%>">
<%
if(image.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT){
	if (image.hasContent()) {
		image = FHUtilityFunctions.nukeURLPrefix(image, resourceResolver);
	}
    
	if(!linkTarget.equals("")){ %>
    <a href="<%= linkTarget %>" <%= properties.get("externalLink","false").equals("true") ? "target=\"_blank\"" : "" %>>
    <% } %>
    <% image.draw(out); %>
    <% if(!linkTarget.equals("")){ %></a><% }
}
%>        
        </div>
        <cq:text property="text" tagName="div" tagClass="hz_text forceSquareBullet" />
    </div>
</div>
