<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="java.util.Calendar" %>
<%
Calendar theTime = Calendar.getInstance();
int theYear = theTime.get(Calendar.YEAR);
%>
    <div class="row-fluid footer">
            <div class="span6" id="footer_text">
                 &copy; Cancer Immunotherapy Trials Network, <%= theYear %> | <a href="/content/citn/en/about/contact.html">Contact Us</a> | <a href="/content/citn/en/about/privacy.html">Privacy Notice</a>
             </div>
             <div class="span6" id="footer_logo">
                <img alt="" src="/content/dam/citn/logo_bw.gif">
             </div>
       </div> <!-- /row-fluid -->
</div> <!-- /container-fluid -->    