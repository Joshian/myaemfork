<%--
  Copyright 1997-2009 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Login component

--%><%
%><%@ page import="com.day.cq.i18n.I18n,
                   com.day.cq.wcm.api.WCMMode,
                   com.day.cq.wcm.foundation.forms.FormsHelper,
                   com.day.cq.xss.ProtectionContext,
                   com.day.cq.xss.XSSProtectionService,
                   com.day.text.Text,
                   java.util.ResourceBundle" %><%
%><%@include file="/libs/foundation/global.jsp"%><%
    String id = Text.getName(resource.getPath());
    final ResourceBundle bundle = slingRequest.getResourceBundle(null);

    String action = currentPage.getPath() + "/j_security_check";
    action = resourceResolver.map(request, action);

    String validationFunctionName = "cq5forms_validate_" + id;

    String usernameLabel = properties.get("./usernameLabel","Username");
    String passwordLabel = properties.get("./passwordLabel","Password");

    String redirectTo = request.getParameter("resource");
    if (redirectTo == null || redirectTo.isEmpty()) {
    	redirectTo = properties.get("./redirectTo",currentPage.getAbsoluteParent(2).getPath());
    } 
    if( !redirectTo.endsWith(".html")) {
        redirectTo += ".html";
    }

    boolean isDisabled = WCMMode.fromRequest(request).equals(WCMMode.DISABLED);
%>
<script type="text/javascript">
    function <%=validationFunctionName%>() {
        <% if ( !isDisabled ) { %>
            if( CQ_Analytics && CQ_Analytics.ProfileDataMgr) {
                var u = document.forms['<%=id%>']['j_username'].value;
                if( u ) {
                    var loaded = CQ_Analytics.ProfileDataMgr.loadProfile(u);
                    if (loaded) {
                        CQ.shared.Util.load(CQ.HTTP.externalize("<%= redirectTo %>"));
                    } else {
                        alert("<%=I18n.get(bundle, "The user could not be found.")%>");
                    }
                    return false;
                }
            }
            return true;
        <% } else { %>
            if( CQ_Analytics && CQ_Analytics.ProfileDataMgr) {
                CQ_Analytics.ProfileDataMgr.clear();
            }
            return true;
        <% } %>
    }
</script>

<%
    String jReason = request.getParameter("j_reason");

    if (null != jReason) {

        final XSSProtectionService xss = sling.getService(XSSProtectionService.class);
        if (null != xss) {
            jReason = xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT, jReason);

%><div class="loginerror"><%=jReason%></div>
<%
        }

    }
%>

<form method="POST"
      action="<%=action%>"
      id="<%=id%>"
      name="<%=id%>"
      enctype="multipart/form-data"
      onsubmit="return <%=validationFunctionName%>();">
    <input type="hidden" name="resource" value="<%= resourceResolver.map(redirectTo)%>">
    <input type="hidden" name="_charset_" value="UTF-8"/>
    <table class="login-form">
        <tr>
            <td class="label"><%= bundle.getString(usernameLabel) %></td>
            <td><input
                   class="<%= FormsHelper.getCss(properties, "form_field form_field_text form_" + id + "_username") %>"
                   name="j_username"/></td>
        </tr>
            <tr>
                <td class="label"><%= bundle.getString(passwordLabel) %></td>
                <td><input
                   class="<%= FormsHelper.getCss(properties, "form_field form_field_text form_" + id + "_password") %>"
                   type="password" name="j_password"/></td>
            </tr>
        <tr>
            <td></td>
            <td>
                <input class="form_button_submit" type="submit" value="Login">
            </td>
        </tr>
    </table>
</form>