<%@include file="/libs/foundation/global.jsp"%>
<jsp:useBean id="twoImageBean" scope="request" class="org.fhcrc.publicsite.components.ImageSplitTesting" />
<jsp:setProperty name="twoImageBean" property="resourceResolver" value="<%= resourceResolver %>"/>
<jsp:setProperty name="twoImageBean" property="resource" value="<%= resource %>"/>
<jsp:setProperty name="twoImageBean" property="currentStyle" value="<%= currentStyle %>"/>
<jsp:setProperty name="twoImageBean" property="currentDesign" value="<%= currentDesign %>"/>
<jsp:setProperty name="twoImageBean" property="resourceDesign" value="<%= resourceDesign %>"/>
<jsp:setProperty name="twoImageBean" property="request" value="<%= request %>"/>
<jsp:setProperty name="twoImageBean" property="writer" value="<%= out %>"/>
<jsp:setProperty name="twoImageBean" property="xss" value="<%= xssAPI %>"/>
<jsp:setProperty name="twoImageBean" property="componentProperties" value="<%= properties %>"/>

<%
	final String LINK_DATA_ATTRIBUTE = "data-href-b";
%>

<div class="imageplus">

	<dl>
	
		<dt>
			<c:choose>
			
				<c:when test="${!empty twoImageBean.linkTargetA }">

					<a class="twoImageLink" href="${ twoImageBean.linkTargetA }" <%= LINK_DATA_ATTRIBUTE %>="${ twoImageBean.linkTargetB }"<%= twoImageBean.isExternalLink() ? " target=\"_blank\"" : "" %>>
			
						<% twoImageBean.drawImage(); %>	
			
					</a>
					
				</c:when>
				
				<c:otherwise>
				
					<% twoImageBean.drawImage(); %>
					
				</c:otherwise>
				
			</c:choose>
		
		</dt>
	
	</dl>

</div>

<cq:includeClientLib js="public.components.imageSplitTesting"/>
