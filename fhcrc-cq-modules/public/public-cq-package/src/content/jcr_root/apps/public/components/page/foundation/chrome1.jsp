<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="java.util.HashMap" %> 

 <!-- global nav -->
 <% 
    // determine which branch of the tree you are in by polling the name of the asset at level 3.
    // because this might be the home page, exclude for that.
    Page current_branch = currentPage.getAbsoluteParent(3); // will be null for home page. will be same page for branch pages.
    String branch_name = "";
    if(null != current_branch){
       branch_name = current_branch.getProperties().get("branchname",""); // reads the property set on the branch top to determine which branch you are in
    }    
    // set up a hashmap so you can easily mark the current branch with a class for printing, next.
    HashMap<String,String> branches = new HashMap<String, String>();
    branches.put( branch_name, "current_branch" );

 %>

<div id="navigation-main-container"><a name="site-wide-navigation" class="clear"></a>
  <div id="navigation-main-wrapper">
    <ul id="navigation-main" class="navigation-main">
       <li class="navigation-main-item menu-diseases <%= branches.get("diseases") %>"><a class="navigation-main-link" href="<%= currentPage.getAbsoluteParent(2).getPath() %>/diseases.html">Diseases / Research</a><%= generateDropDown(currentPage.getAbsoluteParent(2).getPath() + "/diseases",pageManager) %></li>
       <li class="navigation-main-item menu-treatment <%= branches.get("treatment") %>"><a class="navigation-main-link" href="<%= currentPage.getAbsoluteParent(2).getPath() %>/treatment.html">Treatment / Support</a><%= generateDropDown(currentPage.getAbsoluteParent(2).getPath() + "/treatment",pageManager) %></li>
       <li class="navigation-main-item menu-about <%= branches.get("about") %>"><a class="navigation-main-link" href="<%= currentPage.getAbsoluteParent(2).getPath() %>/about.html">About Us</a><%= generateDropDown(currentPage.getAbsoluteParent(2).getPath() + "/about",pageManager) %></li>
       <li class="navigation-main-item menu-labs <%= branches.get("labs") %>"><a class="navigation-main-link" href="<%= currentPage.getAbsoluteParent(2).getPath() %>/labs.html">Labs / Projects</a><%= generateDropDown(currentPage.getAbsoluteParent(2).getPath() + "/labs",pageManager) %></li>
       <li class="navigation-main-item menu-events <%= branches.get("events") %>"><a class="navigation-main-link" href="<%= currentPage.getAbsoluteParent(2).getPath() %>/events.html">Events / Programs</a><%= generateDropDown(currentPage.getAbsoluteParent(2).getPath() + "/events",pageManager) %></li>
       <li class="navigation-main-item menu-give <%= branches.get("help") %>"><a class="navigation-main-link" href="<%= currentPage.getAbsoluteParent(2).getPath() %>/ways-to-give.html">Ways To Give</a><%= generateDropDown(currentPage.getAbsoluteParent(2).getPath() + "/ways-to-give",pageManager) %></li>
    </ul>
  </div>
</div>


 <%! 
     //private String generateDropDown(String dropdownId, String pagePath, PageManager pm){
  private String generateDropDown(String pagePath, PageManager pm){
    String DROPDOWNLINKHTML = "dropdownLinkHTML";
     //String DROPDOWNTEXT = "dropdownAbstract";
     //String DROPDOWNMORELINKTEXT = "morelinkText";
     //String DROPDOWNIMAGE = "dropdownImage"; 
    String buf = "";    
    Page branchPage = pm.getPage(pagePath);
    if (branchPage != null) {
        String dropDownLinkHTML = branchPage.getProperties().get(DROPDOWNLINKHTML, "");
        //String abstractText = branchPage.getProperties().get(DROPDOWNTEXT, "");
        //String moreLinkText = branchPage.getProperties().get(DROPDOWNMORELINKTEXT, "");
        //String thumbnail = branchPage.getProperties().get(DROPDOWNIMAGE, "");       

        buf+= "<div class='dropdown_menu'>";
        buf+= dropDownLinkHTML;
        buf+= "</div>";


        //buf+= "<div class='nav_dropdown' id='" + dropdownId + "'>";
        //buf+=  "<div class='leftcol'>" + dropDownLinkHTML + "</div>";
        //buf+=  "<div class='rightcol'>";
        //buf+=  "<img src='" + thumbnail + "' alt='' class='frame' />";
        //buf+=  "<p>" + abstractText + "</p>";
        //if(!"".equals(moreLinkText)){ buf+=  "<p><a href='" + pagePath + ".html'>" + moreLinkText + "</a>&nbsp;<span class='raquo'>&gt;</span></p>"; }
        //buf+=  "</div><br class='clear' />";
        //buf+= "</div>";        
    }
    return buf;
 }
 %>


 <!-- logo -->
 <div id="fh_logo"><a href="<%= currentPage.getAbsoluteParent(1).getPath() %>/en.html"><img src="<%= currentDesign.getPath() %>/img/logos/fred_hutch_logo.png" alt="Fred Hutchinson Cancer Research Center" border="0"/></a></div>
