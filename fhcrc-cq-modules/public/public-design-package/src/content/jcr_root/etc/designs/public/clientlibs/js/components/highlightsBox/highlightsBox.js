var componentApp = (function(componentApp) {
	
	componentApp.highlightsBox = (function(highlightsBox) {
		
		// Code based on a post found here: http://www.queness.com/post/923/create-a-simple-infinite-carousel-with-jquery
		highlightsBox.instantiateHighlightsBox = function(containerSet, speed, restrictWidth) {
			
			if(speed != 0){
				speed *= 1000;
				var run = setInterval(function(){containerSet.find('.nextButton').click();},speed);
			 	containerSet.hover(
			 		function() {
			 			clearInterval(run);
			 		},
			 		function() {
			 			run = setInterval(function(){containerSet.find('.nextButton').click();},speed);
			 		}
			 	);		
			}
			var currentPanel = 1;
			var theDL = containerSet.find('.panels dl');
			var theDD = containerSet.find('.panels dd');
			var totalPanels = theDD.size();
			
			if(restrictWidth != 0){
				containerSet.width(restrictWidth);
			}
			
			var itemWidth = theDL.outerWidth();
			theDD.width(itemWidth);
			theDD.each(function(){
				var tempDD = $(this);
				if(tempDD.hasClass('noimage')){
					tempDD.find('.text').width(itemWidth);
//					console.log("no image")
				} else {
//					console.log("Image outerwidth = " + tempDD.find('.image').outerWidth(true));
					tempDD.find('.text').width(itemWidth - tempDD.find('.image').outerWidth(true));
//					console.log("Text width = " + (itemWidth - tempDD.find('.image').outerWidth(true)));
				}
			});
//			theDD.find('.text').width(itemWidth - theDD.find('.image').outerWidth(true));
			var leftValue = itemWidth * (-1);
			
			//Set the dl's width dynamically, depending on the number of panels
			theDL.width(containerSet.width() * totalPanels);
			
		    // set up link tracking. this adds ga tracking on all links (images and hyperlinks)
		    var thisUrl="";
		    containerSet.find('.panels dl a').click(function(){
		      thisUrl = $(this).attr('href'); 
		      if(thisUrl && thisUrl.length>0){
		        if(thisUrl.indexOf("://")==-1){ thisUrl = "http://www.fredhutch.org" + thisUrl; }
		        //alert(thisUrl);
		        _gaq.push(['_trackEvent','Highlights Widget' ,'Clickthroughs' , thisUrl]);
		      }
		    });		
			
			if(totalPanels == 1){ //Degrade to a simple text/image box if there is only one highlight
				return;
			} else {
				containerSet.find('div.status').show();
			}
			
			containerSet.find('.panels dt:last,.panels dd:last').insertBefore(containerSet.find('.panels dt:first'));
			theDL.css({'left' : leftValue});
			
			containerSet.find('.totalPanels').text(totalPanels);
			
			containerSet.find('.nextButton').click(function() {
				var leftIndent = parseInt(theDL.css('left')) - itemWidth;
				
				theDL.animate({'left' : leftIndent}, 200, function(){
					containerSet.find('.panels dt:first,.panels dd:first').insertAfter(containerSet.find('.panels dd:last'));
					theDL.css({'left' : leftValue});
					if(currentPanel == theDD.size()){
						currentPanel = 1;
					} else {
						currentPanel++;
					}
					containerSet.find('.currentPanel').text(currentPanel);
				});
				return false;
			});
			
			$("dd", containerSet).removeClass("hiddenSlide");
			
		};
		
		return highlightsBox;
		
	})(componentApp.highlightsBox || {});
	
	return componentApp;
	
})(componentApp || {});