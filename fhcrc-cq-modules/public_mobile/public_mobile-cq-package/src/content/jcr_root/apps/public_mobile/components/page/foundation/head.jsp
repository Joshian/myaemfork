<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default head script.

  Draws the HTML head with some default content:
  - initialization of the WCM
  - initialization of the WCM Mobile Emulator and Device Group CSS
  - includes the current design CSS
  - sets the HTML title

  ==============================================================================

--%><%@include file="/libs/foundation/global.jsp" %><%
%><%@ page import="com.day.cq.commons.Doctype,
                   com.day.cq.wcm.mobile.api.device.DeviceGroup" %><%
    String xs = Doctype.isXHTML(request) ? "/" : "";
    String favIcon = currentDesign.getPath() + "/img/favicon.ico";
    if (resourceResolver.getResource(favIcon) == null) {
        favIcon = null;
    }
    String webclipIcon = currentDesign.getPath() + "/webclip.png";
    if (resourceResolver.getResource(webclipIcon) == null) {
        webclipIcon = null;
    }
    String webclipIconPre = currentDesign.getPath() + "/webclip-precomposed.png";
    if (resourceResolver.getResource(webclipIconPre) == null) {
        webclipIconPre = null;
    }
%><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"<%=xs%>>
    <meta http-equiv="keywords" content="<%= WCMUtils.getKeywords(currentPage) %>"<%=xs%>>

	<!-- disable iPhone initial scale -->
	<!-- http://stackoverflow.com/questions/5434656/ipad-layout-scales-up-when-rotating-from-portrait-to-landcape --> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0" />

	<!-- some friendly metas for different handhelds: -->
	<meta name="HandheldFriendly" content="True" /><!-- Blackberry -->
	<meta name="MobileOptimized" content="width" /><!-- Internet Explorer Mobile (5) -->
	 
    <cq:include script="/libs/wcm/core/components/init/init.jsp"/>
    <%
        /*
        Retrieve the current mobile device group from the request in the following order:
        1) group defined by <path>.<groupname-selector>.html
        2) if not found and in author mode, get default device group as defined in the page properties
           (the first of the mapped groups in the mobile tab)

        If a device group is found, use the group's drawHead method to include the device group's associated
        emulator init component (only in author mode) and the device group's rendering CSS.
        */
        final DeviceGroup deviceGroup = slingRequest.adaptTo(DeviceGroup.class);
        if (null != deviceGroup) {
            deviceGroup.drawHead(pageContext);
        }
        Session session = slingRequest.getResourceResolver().adaptTo(Session.class);
    %>
    <cq:include script="stats.jsp"/>
    <% if (favIcon != null) { %>
    <link rel="icon" type="image/vnd.microsoft.icon" href="<%= favIcon %>"<%=xs%>>
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<%= favIcon %>"<%=xs%>>
    <% } %>
    <% if (webclipIcon != null) { %>
    <link rel="apple-touch-icon" href="<%= webclipIcon %>"<%=xs%>>
    <% } %>
    <% if (webclipIconPre != null) { %>
    <link rel="apple-touch-icon-precomposed" href="<%= webclipIconPre %>"<%=xs%>>
    <% } %>
    <%// currentDesign.writeCssIncludes(pageContext); %>
    <% if(currentDesign !=null & currentDesign.getPath().length()>1){
       Node designNode=session.getNode(currentDesign.getPath());
       if(designNode.hasNode("clientlibs") && designNode.getNode("clientlibs").hasProperty("categories")){    
       Value[] categories=designNode.getNode("clientlibs").getProperty("categories").getValues();%>
       <cq:includeClientLib css="<%=categories[0].getString()%>" />
       <cq:includeClientLib js="<%=categories[0].getString()%>" />    
<%    }
  } %>
    
    <title><%= currentPage.getTitle() == null ? currentPage.getName() : currentPage.getTitle() %></title>
</head>
