<%@include file="/apps/stripe/global.jsp" %>

<%

StringBuffer cls = new StringBuffer();

for (String c: componentContext.getCssClassNames()) {
   	cls.append(c).append(" ");
}

%>

<body class="<%= cls %>">   
    <cq:include script="header.jsp"/>
    <cq:include script="content.jsp"/>
    <div id="footer">
        <cq:include script="footer.jsp"/>
    </div>    
    <cq:includeClientLib js="apps.stripetop" />  
    <cq:include script="overlay.jsp"/>
</body>
