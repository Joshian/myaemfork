<%--

  Retargeting Pixel component.

  This component adds a script tag to the page that then adds AdRoll.coms targeting pixel code to the page.

--%>
<%@include file="/libs/foundation/global.jsp"%>
<%@page session="false" %>
<%@page import="com.day.cq.wcm.api.WCMMode" %><%

Boolean hasAG = properties.get("annualGiving","false").equals("false")? false : true;
Boolean hasSM = properties.get("specificMedia","false").equals("false")? false : true;
Boolean hasFB = properties.get("facebook","false").equals("false")? false : true;
Boolean hasFB_marketing = properties.get("facebookMarketing","false").equals("false")? false : true;
Boolean hasTW = properties.get("twitter","false").equals("false")? false : true;

/* 
 * Check if we need to add the Grizzard/Annual Giving pixel.
 * This is no longer set by default if no boxes are checked.
 */
if (hasAG) {

%>

<!-- element added 11/19/2015 -->
<script async src="https://i.simpli.fi/dpx.js?cid=40781&amp;action=100&amp;segment=grizzardfredhutch&amp;m=1&amp;sifi_tuid=20119"></script>

<%

}
/* End Grizzard/Annual Giving */

/* Check if we need to add the Specific Media targeting pixel */
if (hasSM) {
%>

<img src="//mpp.vindicosuite.com/mpp/?y=2&amp;t=i&amp;tp=1&amp;clid=22219&amp;pixid=99090351" width='1' height='1' border='0' alt='' style="position:absolute;left:-10000px;" />

<%
}
/* End Specific Media */

/* Check if we need to add the Facebook targeting pixel */
if (hasFB) {
%>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '229798047358683');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=229798047358683&amp;ev=PageView&amp;noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<%
}
/* End Facebook/MarComm */

/* Check if we need to add the Facebook Marketing pixel */
if (hasFB_marketing) {
%>

<!-- Facebook Pixel Code - Marketing -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1705204039721587');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1705204039721587&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code - Marketing -->

<%
}
/* End Facebook/Marketing */

/* Check if we need to add the Twitter targeting pixel */
if (hasTW) {
%>

<!-- Twitter single-event website tag code -->
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">twttr.conversion.trackPid('nueg0', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nueg0&amp;p_id=Twitter&amp;tw_sale_amount=0&amp;tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nueg0&amp;p_id=Twitter&amp;tw_sale_amount=0&amp;tw_order_quantity=0" />
</noscript>
<!-- End Twitter single-event website tag code -->

<%
}
/* End Twitter */

if (WCMMode.fromRequest(request) == WCMMode.EDIT) {
%>

<p>This is the targeting pixel component. Please place this as near to the bottom of your page as possible. 
This text will not appear on your page when published.</p>

<%
}
%>