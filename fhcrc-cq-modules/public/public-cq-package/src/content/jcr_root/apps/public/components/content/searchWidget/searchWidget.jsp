<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.day.text.Text,
                 com.day.cq.wcm.api.WCMMode,
                 org.fhcrc.tools.FieldUtility,
                 org.apache.commons.lang.StringEscapeUtils,
                 com.day.cq.xss.ProtectionContext,
                     com.day.cq.xss.XSSProtectionException,
                     com.day.cq.xss.XSSProtectionService,
                     org.apache.sling.api.scripting.SlingBindings,
                     org.apache.sling.api.scripting.SlingScriptHelper" %><%

String actionPath = properties.get("resultsPage","");
if(!actionPath.trim().equals("")){
	actionPath = actionPath + ".html";
}
String linkPath = properties.get("linkTarget","");
if(!linkPath.trim().equals("")){
	linkPath = linkPath + ".html";
}
String searchType = properties.get("searchType","none");
String staticLinkTarget = "";
String staticLinkText = "";
if(searchType.equals("faculty")){
	staticLinkTarget = "/content/public/en/labs/profiles.html";
	staticLinkText = "Hutch faculty";
	
} else if(searchType.equals("projects")){
	staticLinkTarget = "/content/public/en/labs/science-projects.html";
	staticLinkText = "Hutch labs and projects";
}
String value = "Search by keyword...";
String value_safe = "";
if(slingRequest.getParameter("searchTerm") != null) {
	value = slingRequest.getParameter("searchTerm");
    //value_safe = FieldUtility.cleanField(value);	// i actually don't want any html at all: this method allows some.
    //value_safe = StringEscapeUtils.escapeHtml(value); // another approach: just escape everything.

    SlingScriptHelper scriptHelper = bindings.getSling(); // here's the CQ-recommended approach
    XSSProtectionService xssService = scriptHelper.getService(XSSProtectionService.class);
    try {
      value_safe = xssService.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT, value);
    } catch (XSSProtectionException xe) {
      value_safe = ""; // protect page from danger
      log.error("failed to protect comment from xss", xe);
    }

}
%>
<cq:includeClientLib categories="public.components.searchWidget" />
<div class="profinderentry">
    <cq:text property="title" tagName="h3" />
    <!-- Search box goes here -->
    <div class="search_box">
        <form action="<%= actionPath %>" method="get">
            <div>
                <div class="search_term_container"><div><input type="text" name="searchTerm" value="<%= value_safe %>" class="search_term" /></div></div>
                <div class="search_button_container"><input type="image" class="search_button" src="/etc/designs/public/img/buttons/fred_hutch_search_icon.png" alt="GO" /></div>
            </div>
        </form>
        <cq:text property="text" tagName="p" placeholder="" />
<%
if(!linkPath.trim().equals("") && !properties.get("linkText","").trim().equals("")){
%>
        <p class="linktext"><a href="<%= linkPath %>"><cq:text property="linkText" placeholder="" /></a></p>
<%
}
if(!searchType.equals("none")){
%>
        <p class="linktext"><a href="<%= staticLinkTarget %>"><%= staticLinkText %></a></p>
<%
}
%>
    </div>
</div>