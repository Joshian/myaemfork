/**
 * Dynamic class is in charge of handling dynamic component properties and logic
 *  
 */
package org.fhcrc.stripe.components;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.jcr.Node;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;


public class DivisionTitle 
{

    /**
     * default logger
     */
    private static final Logger log = LoggerFactory.getLogger(DivisionTitle.class);
	
	private final static String DEFAULT_LABEL = "Division Title Label";
	public static final String DEFAULT_VALUE = "";
	private ValueMap componentProperties;
	private List<DivisionTitleDetails> divisionTitles;
	private int slideNumber = 0;
	private Float pointerPos;
	private ResourceResolver resourceResolver;

	
	/** Constructor of the class
	 */
	public DivisionTitle() {
		this.divisionTitles = new ArrayList<DivisionTitleDetails>();
	}	
	
	public void setComponentProperties(ValueMap properties) {
		this.componentProperties = properties;	
		
		slideNumber = Integer.valueOf((String)componentProperties.get("division_number","0")).intValue();
		for(int i = slideNumber; i >= 1; i--){
			String divisionLabel = (String)properties.get("division_title_" + i + "_label",DEFAULT_LABEL);
			String divisionLink = (String)properties.get("division_title_" + i + "_link","");
			String faculty = (String)properties.get("division_title_" + i + "_faculty","");
			if(faculty.equals(""))
			{
				faculty = "Member of";
			}
			DivisionTitleDetails divisionTitleDetails = new DivisionTitleDetails(divisionLabel,divisionLink,faculty);
			this.divisionTitles.add(divisionTitleDetails );
		}
	}

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}


	public int getSlideNumber() {
		return slideNumber;
	}


	public void setSlideNumber(int slideNumber) {
		this.slideNumber = slideNumber;
	}

	public Float getPointerPos() {
		return pointerPos;
	}


	public void setPointerPos(Float pointerPos) {
		this.pointerPos = pointerPos;
	}

	public List<DivisionTitleDetails> getDivisionTitles() {
		return divisionTitles;
	}


	public void setDivisionTitles(ArrayList<DivisionTitleDetails> divisionTitles) {
		this.divisionTitles = divisionTitles;
	}

	public List<DivisionTitleDetails> getInheritedProperty(Page page, String componentName)
	{
		Page root = page;
			if (root.getParent() != null) 
			{
				root = root.getParent();
				StringBuffer resourcePath = new StringBuffer();
				StringTokenizer stringTokenizer = new StringTokenizer(root.getPath(), "/");
				StringBuffer pathBuffer = new StringBuffer();
				while (stringTokenizer.hasMoreElements()) {
					String element = (String) stringTokenizer.nextElement();
					pathBuffer.append("/")
					.append(element);
					if(element.equalsIgnoreCase("en"))
					{
						break;
					}
				}
				resourcePath.append(pathBuffer.toString())
				.append("/jcr:content/")
				.append(componentName);
				
				Resource parentResource = resourceResolver.getResource(resourcePath.toString());
				
				if (parentResource != null)
				{
					try 
					{
						Node parentNode = parentResource.adaptTo(Node.class);
						slideNumber = Integer.valueOf(parentNode.getProperty("division_number").getString()).intValue();
						for(int i = slideNumber; i >= 1; i--)
						{
							String divisionLabel = (String)parentNode.getProperty("division_title_" + i + "_label").getString();
							String divisionLink = (String)parentNode.getProperty("division_title_" + i + "_link").getString();
							String faculty = "";
							
							try
							{
								faculty = (String)parentNode.getProperty("division_title_" + i + "_faculty").getString();	
							}
							catch(Exception exception)
							{
								faculty = "Member of";
							}
							
							if(faculty.equals(""))
							{
								faculty = "Member of";
							}
							
							DivisionTitleDetails divisionTitleDetails = new DivisionTitleDetails(divisionLabel,divisionLink, faculty);
							this.divisionTitles.add(divisionTitleDetails );
						}
					} 
					catch (Exception exception) 
					{
						log.error("ERROR GETTING DIVISION TITLE []", exception);
					}
				}
			} 
		return this.divisionTitles;
	}
	
	public class DivisionTitleDetails{
		private String divisionTitle;
		private String divisionLink;
		private String faculty;

		public DivisionTitleDetails(String divisionTitle, String divisionLink,
				String faculty)
		{
			this.divisionTitle = divisionTitle;
			this.divisionLink = divisionLink;
			this.faculty = faculty;
		}
		
		public String getDivisionTitle() {
			return divisionTitle;
		}

		public void setDivisionTitle(String divisionTitle) {
			this.divisionTitle = divisionTitle;
		}

		public String getDivisionLink() {
			return divisionLink;
		}

		public void setDivisionLink(String divisionLink) {
			this.divisionLink = divisionLink;
		}
		
		public String getFaculty() {
			return faculty;
		}

		public void setFaculty(String faculty) {
			this.faculty = faculty;
		}
	}
}