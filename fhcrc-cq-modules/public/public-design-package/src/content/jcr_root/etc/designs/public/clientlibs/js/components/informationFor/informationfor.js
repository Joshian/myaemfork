var componentApp = (function(componentApp) {
	
	componentApp.informationFor = (function(informationFor) {
		
		informationFor.initInfoFor = function(el) {
			
			var h1=$(el).innerHeight(); // gets the natural height of this container for use later
		    $(el).css('height','1.3em'); // narrows up the display to one line visible only
		    $(el).find(".more").css('visibility','visible');
		    var h2=$(el).innerHeight(); // gets the one-line height of this container for use later
		    $(el).find(".more").toggle(
		      function(){ $(el).animate({height:h1},500,'swing'); $(el).find('.more a').html('Less'); },
		      function(){ $(el).animate({height:h2},500,'swing'); $(el).find('.more a').html('More'); }        
		    );

		      // set up link tracking
		    var thisLabel="";
		    $(el).find('.item a').click(function(){
		       thisLabel = $(this).text(); 
		       if(thisLabel){ //alert(thisLabel);
		         _gaq.push(['_trackEvent','Home Page Click Tracking' ,'Information For' , thisLabel]);
		       }
		    });  
			
		};
		
		return informationFor;
		
	})(componentApp.informationFor || {});
	
	return componentApp;
	
})(componentApp || {});