package org.fhcrc.publicsite.pageComponents;

import com.day.cq.commons.Doctype;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;
import com.day.cq.wcm.api.components.DropTarget;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.foundation.Image;

import org.apache.sling.api.resource.*;
import org.fhcrc.tools.FHUtilityFunctions;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Slideshow {
	
	private ValueMap componentProperties;
	private ResourceResolver resourceResolver;
	private Resource resource;
	private Style currentStyle;
	private Design currentDesign;
	private Design resourceDesign;
	private HttpServletRequest request;
	private Writer writer;
	private Page currentPage;
	protected final Logger log = LoggerFactory.getLogger(getClass());
	private PageFilter filter = new PageFilter();
	
	private int numberOfSlides;
	private ArrayList<Page> slides;
	private String nextContentLink;
	private String nextContentTitle;
	private String facebookLink;
	private String facebookTitle;
	
	private boolean thumbnails;
	private boolean nextContent;

	public ValueMap getComponentProperties() {
		return componentProperties;
	}

	public void setComponentProperties(ValueMap componentProperties) {
		this.componentProperties = componentProperties;
		setNextContentLink(componentProperties.get("nextContent",""));
		
		/* Set facebook link/name */
		String tempFBLink = "http://www.fredhutch.org" + resourceResolver.map(currentPage.getPath())+".html";
		String tempFBTitle = componentProperties.get("jcr:title","");
		if (tempFBTitle.trim().equals("")) {
			tempFBTitle = FHUtilityFunctions.displayTitle(currentPage);
		}
		
		try {
			tempFBLink = URLEncoder.encode(tempFBLink, "UTF-8");
			tempFBTitle = URLEncoder.encode(tempFBTitle, "UTF-8");
			log.info("SLIDESHOW: FB Link = " + tempFBLink);
			log.info("SLIDESHOW: FB Title = " + tempFBTitle);
		} catch (UnsupportedEncodingException e) {
			log.info("SLIDESHOW: URL Encoding error: Unsupported Encoding Exception: " + e.getMessage());
		}
		setFacebookLink(tempFBLink);
		setFacebookTitle(tempFBTitle);
		
		setSlides();
		if (slides != null) {
			this.numberOfSlides = slides.size();
		} else {
			this.numberOfSlides = 0;
		}
		
		if (componentProperties.get("hideThumbnails","false").equals("false")) {
			this.thumbnails = true;
		} else {
			this.thumbnails = false;
		}
		
	}

	public ResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setResourceResolver(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public Style getCurrentStyle() {
		return currentStyle;
	}

	public void setCurrentStyle(Style currentStyle) {
		this.currentStyle = currentStyle;
	}

	public Design getCurrentDesign() {
		return currentDesign;
	}

	public void setCurrentDesign(Design currentDesign) {
		this.currentDesign = currentDesign;
	}

	public Design getResourceDesign() {
		return resourceDesign;
	}

	public void setResourceDesign(Design resourceDesign) {
		this.resourceDesign = resourceDesign;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public Writer getWriter() {
		return writer;
	}

	public void setWriter(Writer writer) {
		this.writer = writer;
	}

	public Page getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Page currentPage) {
		this.currentPage = currentPage;
	}

	public int getNumberOfSlides() {
		return numberOfSlides;
	}

	public void setNumberOfSlides(int numberOfSlides) {
		this.numberOfSlides = numberOfSlides;
	}

	public ArrayList<Page> getSlides() {
		return slides;
	}

	/* A version of the setSlides function that takes no input variables.
	 * This version uses the currentPage to set the slides as the children of that page. */
	
	public void setSlides() {
		
		if (currentPage != null && currentPage.getProperties().get("jcr:primaryType","").equals("cq:PageContent")) {

			Iterator<Page> children = currentPage.listChildren(filter);
			ArrayList<Page> tempList = new ArrayList<Page>();
			
			while (children.hasNext()) {
				
				tempList.add(children.next());
				
			}
			
			if (!tempList.isEmpty()) {
				
				setSlides(tempList);
				
			}
					
		}
		
	}
	
	public void setSlides(ArrayList<Page> slides) {
		this.slides = slides;
	}

	public String getNextContentLink() {
		return nextContentLink;
	}

	public void setNextContentLink(String nextContentLink) {
		if (!nextContentLink.trim().equals("")) {
			
			if (resourceResolver.getResource(nextContentLink) != null) {

				Page nextContentPage = resourceResolver.getResource(nextContentLink).adaptTo(Page.class);
				setNextContentTitle(FHUtilityFunctions.displayTitle(nextContentPage));
				
			} else {

				setNextContentTitle("");
				
			}
			
			nextContentLink = FHUtilityFunctions.cleanLink(nextContentLink, resourceResolver);
			setNextContent(true);
			
		} else {
			
			setNextContent(false);
			
		}
		
		this.nextContentLink = nextContentLink;

	}

	public String getNextContentTitle() {
		return nextContentTitle;
	}

	public void setNextContentTitle(String nextContentTitle) {
		this.nextContentTitle = nextContentTitle;
	}

	public String getFacebookLink() {
		return facebookLink;
	}

	public void setFacebookLink(String facebookLink) {
		this.facebookLink = facebookLink;
	}

	public String getFacebookTitle() {
		return facebookTitle;
	}

	public void setFacebookTitle(String facebookTitle) {
		this.facebookTitle = facebookTitle;
	}

	public boolean isThumbnails() {
		return thumbnails;
	}

	public void setThumbnails(boolean thumbnails) {
		this.thumbnails = thumbnails;
	}
	
	/* PUBLIC NON-GETTER/SETTER METHODS */
	
	public boolean isNextContent() {
		return nextContent;
	}

	public void setNextContent(boolean nextContent) {
		this.nextContent = nextContent;
	}

	/**
	 * A method to draw out the slide image of a particular slide. The int parameter represents
	 * which slide's image you want to output. The parameter should be somewhere between 1 and 
	 * numberOfImages for this Slideshow.
	 * 
	 * @param i  an integer that determines which slide to access
	 */
	public void drawSlideImage(int i) {
		
		Resource imageResource = slides.get(i).getContentResource("image");
		String noImageHtml = "<h2>Missing slide photo, please add a photo to the slide</h2>";
		
		if (imageResource != null) {
			
			Image slideImage = new Image(imageResource);
			prepImage(slideImage);
			
			try {
				slideImage.draw(writer);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} else {
			try {
				writer.write(noImageHtml);
			} catch (IOException e) {
				e.printStackTrace();
			}
			log.info("SLIDESHOW: Slide image resource is null");
		}
		
	}
	
	public String getSlideHeadline(int i) {
		
		String slideHeadline = slides.get(i).getProperties().get("headline","");
		return slideHeadline;
		
	}
	
	public String getSlideCaption(int i) {
		
		String slideCaption = slides.get(i).getProperties().get("caption","");
		return slideCaption;
		
	}
	
	public String getSlidePhotoCredit(int i) {
		
		String slidePhotoCredit = slides.get(i).getProperties().get("photoCredit","");
		return slidePhotoCredit;
		
	}
	
	public boolean hasSlideRelatedContent(int i) {
		
		String[] testArray = slides.get(i).getProperties().get("related",String[].class);
		
		/* Test to see if any of the related content items are present on the server */
		
		if (testArray != null && testArray.length > 0) {
			
			int arrayLength = testArray.length;
			int validItems = arrayLength;
			
			for (int j = 0; j < arrayLength; j++) {
				
				if (this.resourceResolver.getResource(testArray[j]) == null || !this.resourceResolver.getResource(testArray[j]).adaptTo(Page.class).isValid()) {
					
					validItems--;
					
				}
				
			}
			
			if (validItems > 0) {
			
				return true;
				
			} else {
				
				return false;
				
			}
			
		} else {
			
			return false;
			
		}
		
	}
	
	public void drawSlideRelatedContent(int i) throws Exception {
		
		String[] relatedContentPaths = slides.get(i).getProperties().get("related",String[].class);
		
		if (relatedContentPaths == null || relatedContentPaths.length < 1) {
			
			log.info("SLIDESHOW: The related content path array is empty.");
			throw new Exception("The related content path array is empty.");
		
		} else {
			
			ArrayList<Page> relatedContent = createRelatedContent(relatedContentPaths);
			
			if (!relatedContent.isEmpty()) {
				
				Iterator<Page> pageIterator = relatedContent.iterator();
				while (pageIterator.hasNext()) {
					
					Page thePage = pageIterator.next();
					
					/* If the page doesn't exist, skip it. This can happen if a page listed as related content
					 * on the Author instance has also been deactivated or has reached its Off Time*/
					if (thePage == null || !thePage.isValid()) {
						
						continue;
						
					}
					
					Image pageImage = new Image(thePage.getContentResource("image"));
					StringBuilder htmlStringBuilder = new StringBuilder();
					String pageTitle = FHUtilityFunctions.displayTitle(thePage);
					String pageHref = thePage.getPath().concat(".html");
					
					htmlStringBuilder.append("<div class=\"related_content_item\"><a href=\"");
					htmlStringBuilder.append(pageHref);
					htmlStringBuilder.append("\" target=\"_blank\">");
					htmlStringBuilder.append("<h4 class=\"related_content_item_title\">".concat(pageTitle).concat("</h4>"));
					
					if (pageImage.hasContent()) {
						
//						prepImage(pageImage);
//						pageImage.addCssClass("related_content_item_photo");
						String img;
						img = thePage.getPath() + ".img.png" + pageImage.getSuffix(); // e.g.,: /content/public/en/events/events/hutch-award.img.png/1323194909406.gif
			            // prepend context path to img
			            img = request.getContextPath().concat(img);
			            img = resourceResolver.map(img);
						
			            htmlStringBuilder.append("<img src=\"".concat(img).concat("\" class=\"related_content_item_photo\""));
			            htmlStringBuilder.append(" alt=\"".concat(pageImage.getAlt()).concat("\">"));								
											
					}
						
					htmlStringBuilder.append("</a></div>");
						
					writer.write(htmlStringBuilder.toString());
					
				}
				
			} else {
				
				log.info("SLIDESHOW: The relatedContent arrayList is empty");
				
			}
			
		}
		
	}
	
	public String getThumbnail(int i) {
		
		String THUMBNAIL_CLASS = "thumbnail";
		String THUMBNAIL_WIDTH = "62"; /* Width in pixels of the desired thumbnail */
		String THUMBNAIL_HEIGHT = "62";
		String PAGETHUMB_MODE = "fit"; /* From Pagethumb docs, this can be "fit", "crop", or "resize" */
		String IMAGE_SELECTOR = "png"; /* Currently, pagethumb only outputs PNG files */
		
		StringBuffer html = new StringBuffer();
		Page slidePage = slides.get(i);
		String thumbAltText = "";
		String thumbTitle = "";
		Image thumbImage = null;
		if (slidePage.getContentResource("image") != null) {

			thumbImage = new Image(slidePage.getContentResource("image"));
			thumbAltText = thumbImage.getAlt();
			thumbTitle = thumbImage.getTitle();
			
		}
		
		
		html.append("<img class=\"").append(THUMBNAIL_CLASS).append("\"");
		
		if (thumbImage != null && thumbImage.hasContent()) {
		
			/* Here we use the pagethumb servlet to generate the image src attribute */
			html.append(" src=\"").append(slidePage.getPath()).append(".pagethumb");
			html.append(".").append(PAGETHUMB_MODE);
			html.append(".").append(THUMBNAIL_WIDTH);
			html.append(".").append(THUMBNAIL_HEIGHT);
			html.append(".").append(IMAGE_SELECTOR);
			html.append("\"");
			/* Add the alt attribute */
			html.append(" alt=\"").append(thumbAltText).append("\"");
			/* Add the title attribute if there is one */
			if (!thumbTitle.trim().equals("")) {
				
				html.append(" title=\"").append(thumbTitle).append("\"");
				
			}
			/* Close the img tag */
			html.append(">");
			
		} else {
			html.append(" src=\"\">");
		}
		
		return html.toString();
		
	}
	
	/* PRIVATE METHODS */
	
	/**
	 * A series of methods to be performed on an Image object before that Image uses its
	 * draw() method.
	 * 
	 * @param image  the image to be prepared for drawing.
	 */
	@SuppressWarnings("deprecation")
	private void prepImage(Image image) {
		
//		image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
		image.loadStyleData(currentStyle);
		image.setSelector(".img"); // use image script
		image.setDoctype(Doctype.fromRequest(request));
	    // add design information if not default (i.e. for reference paras)
	    if (!currentDesign.equals(resourceDesign)) {
	    	image.setSuffix(currentDesign.getId());
	    }
		
	}
	
	private ArrayList<Page> createRelatedContent(String[] relatedList) {
		
		ArrayList<Page> pageList = new ArrayList<Page>();
		
		for (int i = 0; i < relatedList.length; i++) {
			
			if (relatedList[i] != null && !relatedList[i].trim().equals("")) {
				
				if (this.resourceResolver.getResource(relatedList[i]) != null) {
					Page thePage = this.resourceResolver.getResource(relatedList[i]).adaptTo(Page.class);
					
					if (thePage.isValid()) {
						pageList.add(thePage);
					}
					
				}

			}
			
		}
		
		return pageList;
		
	}
	
}