function appendFormFields() {
	if(document.getElementById("dataCollectionContainer")) {
		var toRemove = document.getElementById("dataCollectionContainer");
		toRemove.parentNode.removeChild(toRemove);
	}
	var toAppend = document.getElementById("formContainer");
	switch(document.getElementById("reportType").value) {
		case "recentlyCreated" :
			var theContainer = document.createElement("div");
			theContainer.setAttribute("id", "dataCollectionContainer");
			var theLabel = document.createElement("label");
			theLabel.setAttribute("for","timeInt");
			theLabel.appendChild(document.createTextNode("How far back would you like to look?"));
			theLabel.appendChild(document.createElement("br"));
			var small = document.createElement("small");
			small.appendChild(document.createTextNode("(default is one week)"));
			theLabel.appendChild(small);
			var timeInt = document.createElement("input");
			timeInt.setAttribute("type","num");
			timeInt.setAttribute("id","timeInt");
			timeInt.setAttribute("name","timeInt");
			var dropdown = document.createElement("select");
			dropdown.setAttribute("name","timeIncrement");
			var days = document.createElement("option");
			days.setAttribute("value","d");
			days.appendChild(document.createTextNode("days"));
			var weeks = document.createElement("option");
			weeks.setAttribute("value","w");
			weeks.appendChild(document.createTextNode("weeks"));
			var months = document.createElement("option");
			months.setAttribute("value","M");
			months.appendChild(document.createTextNode("months"));
			dropdown.appendChild(days);
			dropdown.appendChild(weeks);
			dropdown.appendChild(months);
			theContainer.appendChild(theLabel);
			theContainer.appendChild(timeInt);
			theContainer.appendChild(dropdown);
			toAppend.appendChild(theContainer);
			document.getElementById("customReportSubmit").removeAttribute("disabled");
			break;
        case "recentlyModified" :
			var theContainer = document.createElement("div");
			theContainer.setAttribute("id", "dataCollectionContainer");
			var theLabel = document.createElement("label");
			theLabel.setAttribute("for","timeInt");
			theLabel.appendChild(document.createTextNode("Find pages that have been modified in the last:"));
			theLabel.appendChild(document.createElement("br"));
			var small = document.createElement("small");
			small.appendChild(document.createTextNode("(default is one week)"));
			theLabel.appendChild(small);
			var timeInt = document.createElement("input");
			timeInt.setAttribute("type","num");
			timeInt.setAttribute("id","timeInt");
			timeInt.setAttribute("name","timeInt");
			var dropdown = document.createElement("select");
			dropdown.setAttribute("name","timeIncrement");
			var days = document.createElement("option");
			days.setAttribute("value","d");
			days.appendChild(document.createTextNode("days"));
			var weeks = document.createElement("option");
			weeks.setAttribute("value","w");
			weeks.appendChild(document.createTextNode("weeks"));
			var months = document.createElement("option");
			months.setAttribute("value","M");
			months.appendChild(document.createTextNode("months"));
			dropdown.appendChild(days);
			dropdown.appendChild(weeks);
			dropdown.appendChild(months);
			theContainer.appendChild(theLabel);
			theContainer.appendChild(timeInt);
			theContainer.appendChild(dropdown);
			toAppend.appendChild(theContainer);
			document.getElementById("customReportSubmit").removeAttribute("disabled");
			break;
		case "staleContent" :
			var theContainer = document.createElement("div");
			theContainer.setAttribute("id", "dataCollectionContainer");
			var theLabel = document.createElement("label");
			theLabel.setAttribute("for","timeInt");
			theLabel.appendChild(document.createTextNode("Find content that has not been edited in how long?"));
			theLabel.appendChild(document.createElement("br"));
			var small = document.createElement("small");
			small.appendChild(document.createTextNode("(default is 3 months)"));
			theLabel.appendChild(small);
			var timeInt = document.createElement("input");
			timeInt.setAttribute("type","num");
			timeInt.setAttribute("id","timeInt");
			timeInt.setAttribute("name","timeInt");
			var dropdown = document.createElement("select");
			dropdown.setAttribute("name","timeIncrement");
			var days = document.createElement("option");
			days.setAttribute("value","d");
			days.appendChild(document.createTextNode("days"));
			var weeks = document.createElement("option");
			weeks.setAttribute("value","w");
			weeks.appendChild(document.createTextNode("weeks"));
			var months = document.createElement("option");
			months.setAttribute("value","M");
			months.appendChild(document.createTextNode("months"));
			var years = document.createElement("option");
			years.setAttribute("value","y");
			years.appendChild(document.createTextNode("years"));
			dropdown.appendChild(days);
			dropdown.appendChild(weeks);
			dropdown.appendChild(months);
			dropdown.appendChild(years);
			theContainer.appendChild(theLabel);
			theContainer.appendChild(timeInt);
			theContainer.appendChild(dropdown);
			toAppend.appendChild(theContainer);
			document.getElementById("customReportSubmit").removeAttribute("disabled");
			break;
		case "findComponents" :
			var theContainer = document.createElement("div");
			theContainer.setAttribute("id", "dataCollectionContainer");
			var theLabel = document.createElement("label");
			theLabel.setAttribute("for","componentType");
			theLabel.appendChild(document.createTextNode("Which component type are you looking for?"));
			theLabel.appendChild(document.createElement("br"));
			var small = document.createElement("small");
			small.appendChild(document.createTextNode("(default is Video)"));
			theLabel.appendChild(small);

            var dropdown = document.createElement("select");
			dropdown.setAttribute("name","componentType");
			dropdown.setAttribute("id","componentType");

            var embeddedVideo = document.createElement("option");
			embeddedVideo.setAttribute("value","video");
			embeddedVideo.appendChild(document.createTextNode("Video"));

            var button = document.createElement("option");
			button.setAttribute("value","button");
			button.appendChild(document.createTextNode("Button"));

            var carousel = document.createElement("option");
			carousel.setAttribute("value","carousel");
			carousel.appendChild(document.createTextNode("Carousel"));

            var header = document.createElement("option");
			header.setAttribute("value","header");
			header.appendChild(document.createTextNode("Header"));

            var staffHeadshot = document.createElement("option");
			staffHeadshot.setAttribute("value","staffHeadshot");
			staffHeadshot.appendChild(document.createTextNode("Staff Headshot"));

            var image = document.createElement("option");
			image.setAttribute("value","image");
			image.appendChild(document.createTextNode("Image"));

			dropdown.appendChild(button);
			dropdown.appendChild(carousel);
			dropdown.appendChild(header);
            dropdown.appendChild(image);
			dropdown.appendChild(staffHeadshot);
            dropdown.appendChild(embeddedVideo);

			theContainer.appendChild(theLabel);
			theContainer.appendChild(dropdown);
			toAppend.appendChild(theContainer);
			document.getElementById("customReportSubmit").removeAttribute("disabled");
			break;
		case "newUsers" :
			var theContainer = document.createElement("div");
			theContainer.setAttribute("id", "dataCollectionContainer");
			var theLabel = document.createElement("label");
			theLabel.setAttribute("for","timeInt");
			theLabel.appendChild(document.createTextNode("How far back would you like to look?"));
			theLabel.appendChild(document.createElement("br"));
			var small = document.createElement("small");
			small.appendChild(document.createTextNode("(default is one week)"));
			theLabel.appendChild(small);
			var timeInt = document.createElement("input");
			timeInt.setAttribute("type","num");
			timeInt.setAttribute("id","timeInt");
			timeInt.setAttribute("name","timeInt");
			var dropdown = document.createElement("select");
			dropdown.setAttribute("name","timeIncrement");
			var days = document.createElement("option");
			days.setAttribute("value","d");
			days.appendChild(document.createTextNode("days"));
			var weeks = document.createElement("option");
			weeks.setAttribute("value","w");
			weeks.appendChild(document.createTextNode("weeks"));
			var months = document.createElement("option");
			months.setAttribute("value","M");
			months.appendChild(document.createTextNode("months"));
			dropdown.appendChild(days);
			dropdown.appendChild(weeks);
			dropdown.appendChild(months);
			theContainer.appendChild(theLabel);
			theContainer.appendChild(timeInt);
			theContainer.appendChild(dropdown);
			toAppend.appendChild(theContainer);
			document.getElementById("customReportSubmit").removeAttribute("disabled");
			break;
		default :
			document.getElementById("customReportSubmit").setAttribute("disabled", "disabled");
			break;
	}
}

if(document.getElementById("reportType")) {
	document.addEventListener ? 
			document.getElementById("reportType").addEventListener("change",appendFormFields, false) 
			: document.getElementById("reportType").attachEvent("onchange",appendFormFields);
}