<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.util.Iterator,
                 com.day.cq.wcm.foundation.Image,
                 com.day.cq.commons.Doctype" %>
<%
String rootPath = "/content/public/en/diseases/featured-researchers";
Iterator<Page> pageIterator = resourceResolver.getResource(rootPath).adaptTo(Page.class).listChildren();
%>
<cq:includeClientLib categories="public.components.profileAlphaList" />
<div class="profileListContainer clearfix">
    <!-- <h2 id="profileListTitle">Faculty Profiles</h2>-->
    <h3>View by topic:</h3>
    <select id="topicSelector" class="topicSelector">
        <option value="">Search by topic</option>
        <option value="autoimmune">Autoimmune Diseases</option>
        <option value="detection">Early Detection</option>
        <option value="fundamental">Fundamental Science</option>
        <option value="immunotherapy">Immunotherapy</option>
        <option value="infectious">Infectious Diseases</option>
        <option value="leukemia">Leukemia &amp; Lymphoma</option>
        <option value="prevention">Prevention</option>
        <option value="tumors">Solid Tumor Cancers</option>
        <option value="showAll">Show All</option>
    </select>
    <h3>Alphabetical Listing:</h3>
    <span class="note">Select a letter to display a list of Faculty members</span>
    <div id="alphaSelect" class="alphaSelect">
        <a href="#a" id="a" class="inactive">A</a>&nbsp;
        <a href="#b" id="b" class="inactive">B</a>&nbsp;
        <a href="#c" id="c" class="inactive">C</a>&nbsp;
        <a href="#d" id="d" class="inactive">D</a>&nbsp;
        <a href="#e" id="e" class="inactive">E</a>&nbsp;
        <a href="#f" id="f" class="inactive">F</a>&nbsp;
        <a href="#g" id="g" class="inactive">G</a>&nbsp;
        <a href="#h" id="h" class="inactive">H</a>&nbsp;
        <a href="#i" id="i" class="inactive">I</a>&nbsp;
        <a href="#j" id="j" class="inactive">J</a>&nbsp;
        <a href="#k" id="k" class="inactive">K</a>&nbsp;
        <a href="#l" id="l" class="inactive">L</a>&nbsp;
        <a href="#m" id="m" class="inactive">M</a>&nbsp;
        <a href="#n" id="n" class="inactive">N</a>&nbsp;
        <a href="#o" id="o" class="inactive">O</a>&nbsp;
        <a href="#p" id="p" class="inactive">P</a>&nbsp;
        <a href="#q" id="q" class="inactive">Q</a>&nbsp;
        <a href="#r" id="r" class="inactive">R</a>&nbsp;
        <a href="#s" id="s" class="inactive">S</a>&nbsp;
        <a href="#t" id="t" class="inactive">T</a>&nbsp;
        <a href="#u" id="u" class="inactive">U</a>&nbsp;
        <a href="#v" id="v" class="inactive">V</a>&nbsp;
        <a href="#w" id="w" class="inactive">W</a>&nbsp;
        <a href="#x" id="x" class="inactive">X</a>&nbsp;
        <a href="#y" id="y" class="inactive">Y</a>&nbsp;
        <a href="#z" id="z" class="inactive">Z</a>&nbsp;
        [<a href="#showAll" id="showAll" class="inactive">Show All</a>]
    </div>
<%
while(pageIterator.hasNext()){
	Page nextPage = pageIterator.next();
/* If it isn't a public profile, skip it */
	if(!nextPage.getProperties().get("cq:template","").equals("/apps/public/templates/publicprofile")){ 
		continue; 
	}
/* Get the profile photo */

    Resource r = nextPage.getContentResource("listimage");
    String img = "";
    if (r != null) {
        Image image = new Image(r);
        img = nextPage.getPath() + "/_jcr_content/listimage.img.png" + image.getSuffix(); 
        // prepend context path to img
        img = request.getContextPath() + img;
        img = resourceResolver.map(img);
    }


/*  
    Image profilePhoto = new Image(nextPage.getContentResource(), "listimage");
	if(profilePhoto.hasContent()){
		profilePhoto.addCssClass("frame");
		profilePhoto.loadStyleData(currentStyle);
		profilePhoto.setSelector(".img"); // use image script
		profilePhoto.setDoctype(Doctype.fromRequest(request));
		// add design information if not default (i.e. for reference paras)
		if (!currentDesign.equals(resourceDesign)) {
		    profilePhoto.setSuffix(currentDesign.getId());
		}
	}
*/

/* We need the first letter of the Faculty member's last name. Fortunately, by design, all profile pages are built with name = <Last Name>_<First Name> */
	char alphaData = nextPage.getName().charAt(0);
/* We need the topics that the authors have associated with this person */
    String[] topics = nextPage.getProperties().get("topics", new String[0]);
    String topicsString = "";
    if(topics != null && topics.length > 0){
    	for(int i = 0; i < topics.length; i++){
    		topicsString += topics[i];
    		if(i != topics.length-1){
    			topicsString += "|";
    		}
    	}
    }
/* Get the name that we will display */
    String nameString = nextPage.getProperties().get("nameDisplay","");
    if(nameString.trim().equals("")){
    	if(nextPage.getTitle() != null && !nextPage.getTitle().trim().equals("")){
    		nameString = nextPage.getTitle();
    	} else {
    		nameString = nextPage.getName();
    	}
    }
%>
    <div class="profileListItem clearfix" data-alpha="<%= alphaData %>" data-topics="<%= topicsString %>">
        <div class="profileListPhoto">
            <a href="<%= nextPage.getPath() %>.html">
<%
    if(!img.equals("")){
    	out.print("<img src='"+img+"'>");
    	out.print("</a>");
    } else {
%>
            <img src="<%= currentDesign.getPath() %>/img/defaults/profile-default.gif" alt="Default profile image" /></a>
<% } %>
        </div>
        <div class="profileListText">
            <div class="profileName"><a href="<%= nextPage.getPath() %>.html" class="title"><%= nameString %></a></div>
            <div class="profileBusinessRole"><%= nextPage.getProperties().get("businessRole","") %></div>
            <div class="profileSummary"><%= nextPage.getProperties().get("summary","") %></div>
            <div class="profileMoreLink"><a href="<%= nextPage.getPath() %>.html">Read more</a>&nbsp;&gt;</div>
        </div>
    </div>
<%	
}
%>
</div>