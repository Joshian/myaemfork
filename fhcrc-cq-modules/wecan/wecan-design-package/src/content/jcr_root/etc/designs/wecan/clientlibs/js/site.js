var weCanApp = (function(weCanApp, $) {
	
	/* "Private" variables */
	

	
	weCanApp.addEvent = function(elem, evtType, func) {
    if (elem.addEventListener) {
      elem.addEventListener(evtType, func, false);
    } else if (elem.attachEvent) {
      elem.attachEvent("on" + evtType, func);
    } else {
      elem["on" + evtType] = func;
    }
  };
	
	weCanApp.adjustBodyPosition = function() {
		
		var headerId = "headerElement",
			bodyId = "bodyContent";
    /* adjust the position of the body element so it appears
     perfectly under the header. */
    var o = document.getElementById(headerId); //alert(o.offsetHeight);
    var y;
    if (o) {
      y = o.offsetHeight;
    }
    if (y) {
      var o = document.getElementById(bodyId);
      if (o) {
				var s = 0; 
				/* Check to see if the subNav is active */
				/*if($("#globalNav>ul>li.active").size() > 0) {
					s += $("li.active ul.subNav").first().outerHeight();
				}*/
        
        o.style.marginTop = (y + s) + "px";
      }
    }
		
	}
	
	weCanApp.adjustFooterHeight = function() {
		
		var theFooter = $("#footerInfo");
		theFooter.css("height","auto");
		if ($("#footerInfo .footerCopyright").css("position") != "absolute") {
			return;
		}
		
		if (theFooter.size() > 0) {
			var footerButtonsAndCopyright = $(".footerCopyright", theFooter).add(".footerButtonContainer a", theFooter);
			if (footerButtonsAndCopyright.size() > 0) {
				var footerHeight = theFooter.outerHeight();
				footerButtonsAndCopyright.each(function() {
					footerHeight += $(this).outerHeight(true);
				});
				
				theFooter.css("height", footerHeight+"px");
	
			}
			
		}
		
	}
		
	/* Global Nav object to hold all relevant objects */
	weCanApp.globalNav = (function(globalNav) {
		
		globalNav.activateItem = function(el) {
			
			/* el is the li that is being rolled over */
			var el = $(el);
			el.toggleClass("active");
			weCanApp.adjustBodyPosition();
			
		};
		
		globalNav.toggleMobileNav = function() {
			
			var mobileNav = $("#mobileNav");
			if (mobileNav.css("display") == "block") {
				mobileNav.css("display", "none");
			} else {
				mobileNav.css("display", "block");
			}
			
			weCanApp.adjustBodyPosition();
			
		}
		
		globalNav.toggleMobileSearch = function() {
			
			var mobileSearch = $("#mobileSearch");
			if (mobileSearch.css("display") == "block") {
				mobileSearch.css("display", "none");
			} else {
				mobileSearch.css("display", "block");
			}
			
			weCanApp.adjustBodyPosition();
			
		}
		
		return globalNav;
		
	})(weCanApp.globalNav || {});
	
	return weCanApp;
	
})(weCanApp || {}, jQuery);